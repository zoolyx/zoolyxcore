<?php

namespace spec\Zoolyx\CoreBundle\Model\Report\Lister;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Zoolyx\CoreBundle\Entity\Parameter;
use Zoolyx\CoreBundle\Entity\ParameterGroup;
use Zoolyx\CoreBundle\Entity\ReportVersion;
use Zoolyx\CoreBundle\Entity\Sample;

class ParameterListerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Zoolyx\CoreBundle\Model\Report\Lister\ParameterLister');
    }

    function it_returns_an_array(Sample $sample
    ) {
        $sample->getParameterGroups()->willReturn(array());

        $this->getParameters($sample)->shouldBeArray();
    }

    function it_returns_a_parameter(Sample $sample,
                                    ParameterGroup $parameterGroup,
                                    Parameter $parameter
    ) {
        $parameterGroup->getParameters()->willReturn(array($parameter));
        $sample->getParameterGroups()->willReturn(array($parameterGroup));

        $parameters = $this->getParameters($sample);

        $parameters->shouldHaveCount(1);
        $parameters[0]->shouldBe($parameter);
    }

    function it_returns_multiple_parameters(
                                    Sample $sample,
                                    ParameterGroup $parameterGroup1,
                                    ParameterGroup $parameterGroup2,
                                    Parameter $parameter1,
                                    Parameter $parameter2,
                                    Parameter $parameter3,
                                    Parameter $parameter4
    ) {
        $parameterGroup1->getParameters()->willReturn(array($parameter1, $parameter2));
        $parameterGroup2->getParameters()->willReturn(array($parameter3, $parameter4));
        $sample->getParameterGroups()->willReturn(array($parameterGroup1, $parameterGroup2));

        $parameters = $this->getParameters($sample);

        $parameters->shouldHaveCount(4);
        $parameters[0]->shouldBe($parameter1);
        $parameters[1]->shouldBe($parameter2);
        $parameters[2]->shouldBe($parameter3);
        $parameters[3]->shouldBe($parameter4);
    }



}
