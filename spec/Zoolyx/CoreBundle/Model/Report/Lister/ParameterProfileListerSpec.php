<?php

namespace spec\Zoolyx\CoreBundle\Model\Report\Lister;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Zoolyx\CoreBundle\Entity\Parameter;
use Zoolyx\CoreBundle\Entity\ParameterProfile as ParameterProfileEntity;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileRepository;
use Zoolyx\CoreBundle\Model\Report\Lister\ParameterProfileLister;

/** @var ParameterProfileLister */
class ParameterProfileListerSpec extends ObjectBehavior
{
    private $reference1 = "H3201";
    private $reference2 = "H3202";
    private $reference3 = "DO_NOT_EXIST";

    function let(ParameterProfileRepository $parameterProfileRepository, ParameterProfileEntity $parameterProfileEntity1, ParameterProfileEntity $parameterProfileEntity2) {
        $this->beConstructedWith($parameterProfileRepository);
        $parameterProfileRepository->findOneBy(array('reference'=>$this->reference1))->willReturn($parameterProfileEntity1);
        $parameterProfileRepository->findOneBy(array('reference'=>$this->reference2))->willReturn($parameterProfileEntity2);
        $parameterProfileRepository->findOneBy(array('reference'=>$this->reference3))->willReturn(null);
    }


    function it_is_initializable()
    {
        $this->shouldHaveType('Zoolyx\CoreBundle\Model\Report\Lister\ParameterProfileLister');
    }

    function it_returns_an_array() {
        $parameters = array();
        $this->getParameterProfiles($parameters)->shouldBeArray();
    }

    function it_returns_an_empty_array() {
        $parameters = array();
        $this->getParameterProfiles($parameters)->shouldHaveCount(0);
    }

    function it_returns_a_profile(Parameter $parameter) {
        $parameter->getParameterP()->willReturn($this->reference1);
        $parameter->getPrice()->willReturn('4.5');
        $parameters = array($parameter);
        $parameterProfiles = $this->getParameterProfiles($parameters);


        $parameterProfiles->shouldHaveCount(1);
        $parameterProfiles->offsetExists($this->reference1);
        $parameterProfiles[$this->reference1]->shouldHaveType('Zoolyx\CoreBundle\Model\Report\Lister\ParameterProfile');
        $parameterProfiles[$this->reference1]->getPrice()->shouldBeLike(4.5);
        $parameterProfiles[$this->reference1]->getReference()->shouldBeLike($this->reference1);
    }

    function it_returns_no_profile_for_a_parameter_without_parameter_profile(Parameter $parameter) {
        $parameters = array($parameter);
        $this->getParameterProfiles($parameters)->shouldHaveCount(0);
    }

    function it_returns_no_profile_for_a_parameter_with_a_null_parameter_profile(Parameter $parameter) {
        $parameters = array($parameter);
        $parameter->getParameterP()->willReturn(null);
        $this->getParameterProfiles($parameters)->shouldHaveCount(0);
    }

    function it_returns_no_profile_for_a_parameter_with_an_empty_parameter_profile(Parameter $parameter) {
        $parameters = array($parameter);
        $parameter->getParameterP()->willReturn('');
        $this->getParameterProfiles($parameters)->shouldHaveCount(0);
    }


    function it_returns_no_profile_for_a_parameter_with_a_non_existing_parameter_profile(Parameter $parameter) {
        $parameters = array($parameter);
        $parameter->getParameterP()->willReturn($this->reference3);
        $parameter->getPrice()->willReturn('0');
        $this->getParameterProfiles($parameters)->shouldHaveCount(0);
    }

    function it_returns_a_profile_for_each_parameter(Parameter $parameter1, Parameter $parameter2) {
        $parameter1->getParameterP()->willReturn($this->reference1);
        $parameter1->getPrice()->willReturn('0');
        $parameter2->getParameterP()->willReturn($this->reference2);
        $parameter2->getPrice()->willReturn('0');
        $parameters = array($parameter1,$parameter2);
        $parameterProfiles = $this->getParameterProfiles($parameters);

        $parameterProfiles->shouldHaveCount(2);
    }

    function it_return_a_profile_only_once_and_add_prices(Parameter $parameter1, Parameter $parameter2) {
        $parameter1->getParameterP()->willReturn($this->reference1);
        $parameter1->getPrice()->willReturn('4.30');
        $parameter2->getParameterP()->willReturn($this->reference1);
        $parameter2->getPrice()->willReturn('5,69');
        $parameters = array($parameter1,$parameter2);

        $parameterProfiles = $this->getParameterProfiles($parameters);

        $parameterProfiles->shouldHaveCount(1);
        $parameterProfiles[$this->reference1]->getPrice()->shouldBeLike(9.99);
    }

}
