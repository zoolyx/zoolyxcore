<?php

namespace spec\Zoolyx\CoreBundle\Model\Report\Lister;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ParameterProfileSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Zoolyx\CoreBundle\Model\Report\Lister\ParameterProfile');
    }

    function it_can_add_a_string_to_the_price() {

        $this->addPrice('4.05');

        $this->getPrice()->shouldBeLike(4.05);
    }


    function it_can_add_a_string_with_comma_to_the_price() {

        $this->addPrice('4,05');

        $this->getPrice()->shouldBeLike(4.05);
    }

    function it_can_add_an_empty_string() {

        $this->addPrice('');

        $this->getPrice()->shouldBeLike(0);
    }

    function it_can_add_a_null() {

        $this->addPrice(null);

        $this->getPrice()->shouldBeLike(0);
    }
}
