<?php

namespace Zoolyx\NotificationBundle\Entity;

use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;
use Zoolyx\CoreBundle\Entity\User;

/**
 * Notification
 *
 * @Serializer\ExclusionPolicy("all")
 */
abstract class Notification
{

    const NEW_NOTIFICATION = 0;
    const NOTIFICATION_SENT = 1;
    const NOTIFICATION_CONFIRMED = 0;
    const NOTIFICATION_ERROR = 2;

    /** @var integer
     *  @Serializer\Expose
     */
    private $id;

    /** @var int */
    private $status = self::NEW_NOTIFICATION;

    /** @var User */
    private $user;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Notification
     */
    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }
    /**
     * @return User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param User $user
     * @return ReportNotification
     */
    public function setUser($user) {
        $this->user = $user;
        return $this;
    }

}

