<?php

namespace Zoolyx\NotificationBundle\Entity;

use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;

/**
 * ReportNotification
 *
 * @Serializer\ExclusionPolicy("all")
 */
class ReportNotification extends Notification
{
    const REPORT_PARTIAL_COMPLETE = 0;
    const REPORT_COMPLETE = 1;

    /** @var Report */
    private $report;

    /** @var int */
    private $reportStatus = self::REPORT_PARTIAL_COMPLETE;
    /** @var VeterinaryPractice */
    private $veterinaryPractice = null;
    /** @var bool */
    private $isComplete = false;
    /** @var bool */
    private $isNew = false;
    /** @var bool */
    private $isUrgent = false;
    /** @var bool */
    private $sendMail = false;

    /**
     * @return Report
     */
    public function getReport() {
        return $this->report;
    }

    /**
     * @param Report $report
     * @return ReportNotification
     */
    public function setReport($report) {
        $this->report = $report;
        return $this;
    }

    /**
     * @return int
     */
    public function getReportStatus() {
        return $this->reportStatus;
    }

    /**
     * @param int $reportStatus
     * @return ReportNotification
     */
    public function setReportStatus($reportStatus) {
        $this->reportStatus = $reportStatus;
        return $this;
    }

    /**
     * @return VeterinaryPractice
     */
    public function getVeterinaryPractice()
    {
        return $this->veterinaryPractice;
    }

    /**
     * @param VeterinaryPractice $veterinaryPractice
     */
    public function setVeterinaryPractice($veterinaryPractice)
    {
        $this->veterinaryPractice = $veterinaryPractice;
    }

    /**
     * @return boolean
     */
    public function isComplete()
    {
        return $this->isComplete;
    }

    /**
     * @param boolean $isComplete
     * @return $this
     */
    public function setIsComplete($isComplete)
    {
        $this->isComplete = $isComplete;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isNew()
    {
        return $this->isNew;
    }

    /**
     * @param boolean $isNew
     * @return $this
     */
    public function setIsNew($isNew)
    {
        $this->isNew = $isNew;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isUrgent()
    {
        return $this->isUrgent;
    }

    /**
     * @param boolean $isUrgent
     * @return $this
     */
    public function setIsUrgent($isUrgent)
    {
        $this->isUrgent = $isUrgent;
        return $this;
    }

    /**
     * @return boolean
     */
    public function sendMail()
    {
        return $this->sendMail;
    }

    /**
     * @param boolean $sendMail
     * @return $this
     */
    public function setSendMail($sendMail)
    {
        $this->sendMail = $sendMail;
        return $this;
    }

}

