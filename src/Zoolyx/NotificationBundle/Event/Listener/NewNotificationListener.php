<?php

namespace Zoolyx\NotificationBundle\Event\Listener;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Entity\VeterinaryPracticeReport;
use Zoolyx\CoreBundle\Event\ReportEvent;
use Zoolyx\CoreBundle\Events;
use Zoolyx\CoreBundle\Model\Report\VeterinaryPracticeReport\VeterinaryPracticeReportRoles;
use Zoolyx\NotificationBundle\Entity\Notification;
use Zoolyx\NotificationBundle\Entity\ReportNotification;
use Zoolyx\NotificationBundle\Entity\Repository\ReportNotificationRepository;
use Zoolyx\NotificationBundle\Model\Report\NotificationDetector;


/**
 * @DI\Service
 */
class NewNotificationListener
{
    /** @var ObjectManager */
    private $entityManager;

    /** @var  ReportNotificationRepository */
    private $reportNotificationRepository;

    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /** @var  NotificationDetector */
    private $notificationDetector;
    /**
     * Constructor.
     *
     * @param ObjectManager $entityManager
     * @param ReportNotificationRepository $reportNotificationRepository
     * @param EventDispatcherInterface $eventDispatcher
     * @param NotificationDetector $notificationDetector
     *
     * @DI\InjectParams({
     *  "entityManager" = @DI\Inject("doctrine.orm.entity_manager"),
     *  "reportNotificationRepository" = @DI\Inject("zoolyx_notification.repository.report_notification"),
     *  "eventDispatcher" = @DI\Inject("event_dispatcher"),
     *  "notificationDetector" = @DI\Inject("zoolyx_notification.report.notification_detector")
     * })
     */
    public function __construct( ObjectManager $entityManager, ReportNotificationRepository $reportNotificationRepository, EventDispatcherInterface $eventDispatcher, NotificationDetector $notificationDetector)
    {
        $this->entityManager = $entityManager;
        $this->reportNotificationRepository = $reportNotificationRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->notificationDetector = $notificationDetector;
    }

    /**
     * @DI\Observe(Events::REPORT_IMPORTED)
     *
     * @param ReportEvent $reportEvent
     */
    public function onResult(ReportEvent $reportEvent)
    {
        /* @var Report $report */
        $report = $reportEvent->getReport();

        $this->notificationDetector->setReport($report);

        $organisations = array();
        $veterinaries = array();
        $owner = null;

        // check if notification is required for organization
        /** @var VeterinaryPracticeReport $vpr */
        foreach ($report->getVeterinaryPracticeReports() as $vpr) {
            $veterinaryPractice = $vpr->getVeterinaryPractice();
            $veterinary = $veterinaryPractice->getVeterinary();
            $practice = $veterinaryPractice->getPractice();
            if( $veterinary->getAccount() &&
                $practice->isSendNotifications() &&
                !is_null($practice->getNotificationEndpoint()) &&
                (
                    !$practice->getNotificationEndpoint()->usesNotificationId() ||
                    (
                        $practice->getNotificationEndpoint()->usesNotificationId() &&
                        !is_null($veterinaryPractice->getNotificationId()) &&
                        $veterinaryPractice->getNotificationId()!=''
                    )
                )
            )
            {
                $veterinaryAccount = $veterinary->getAccount();
                if ($veterinaryAccount->hasRole("ROLE_ORGANISATION")) {
                    $organisations[] = $veterinaryPractice;
                } elseif ($veterinaryAccount->hasRole("ROLE_VETERINARY") ) {
                    $veterinaries[] = $veterinaryPractice;
                }
            }
        }

        /** @var VeterinaryPractice $organisation */
        foreach ($organisations as $organisation) {
            $account = $organisation->getVeterinary()->getAccount();
            //check if notification is pending
            $reportNotification = $this->reportNotificationRepository->findOneBy(array(
                'user' => $account,
                'report' => $report,
                'status' => Notification::NEW_NOTIFICATION
            ));

            if (!$reportNotification) {
                $reportNotification = new ReportNotification();
                $reportNotification->setUser($account);
                $reportNotification->setVeterinaryPractice($organisation);
                $reportNotification->setReport($report);
            }
            $reportStatus = $this->notificationDetector->isComplete() ? ReportNotification::REPORT_COMPLETE : ReportNotification::REPORT_PARTIAL_COMPLETE;
            $reportNotification->setReportStatus($reportStatus);

            $this->entityManager->persist($reportNotification);
        }

        /** @var VeterinaryPractice $veterinary */
        foreach ($veterinaries as $veterinary) {
            $account = $veterinary->getVeterinary()->getAccount();
            //check if notification is pending
            $reportNotification = $this->reportNotificationRepository->findOneBy(array(
                'user' => $account,
                'report' => $report,
                'status' => Notification::NEW_NOTIFICATION
            ));

            if (!$reportNotification) {
                $reportNotification = new ReportNotification();
                $reportNotification->setUser($account);
                $reportNotification->setVeterinaryPractice($veterinary);
                $reportNotification->setReport($report);
            }
            $reportStatus = $this->notificationDetector->isComplete() ? ReportNotification::REPORT_COMPLETE : ReportNotification::REPORT_PARTIAL_COMPLETE;
            $reportNotification->setReportStatus($reportStatus);

            $this->entityManager->persist($reportNotification);

            //$this->eventDispatcher->dispatch(Events::REPORT_IMPORTED_FOR_USER, new ReportUserEvent($report,$veterinary));

        }

        //check to send mails
        $accounts = array();

        $mainVeterinaryAccount = null;
        $mainOrganisationAccount = null;

        /** @var VeterinaryPracticeReport $vpr */
        foreach ($report->getVeterinaryPracticeReports() as $vpr) {
            if (is_null($vpr->getVeterinaryPractice()) ||
                is_null($vpr->getVeterinaryPractice()->getVeterinary()) ||
                is_null($vpr->getVeterinaryPractice()->getVeterinary()->getAccount())
            ) {
                continue;
            }

            $veterinaryAccount = $vpr->getVeterinaryPractice()->getVeterinary()->getAccount();
            if ($this->notificationDetector->needsNotification($veterinaryAccount)) {
                $accounts[$veterinaryAccount->getId()] = $veterinaryAccount;
            }

            if ($vpr->getRole() == VeterinaryPracticeReportRoles::VPR_VETERINARY) {
                $mainVeterinaryAccount = $veterinaryAccount;
            }
            if ($veterinaryAccount->hasRole('ROLE_ORGANISATION')) {
                $mainOrganisationAccount = $veterinaryAccount;
            }
        }

        // check if owner can see the report
        $sharingAccount = is_null($mainOrganisationAccount) ? $mainVeterinaryAccount : $mainOrganisationAccount;
        if (!is_null($sharingAccount) && $sharingAccount->isAutoShareOwners()) {
            // the owner is allowed to see the report
            $report->setOwnerHasAccess(true);

            $ownerAccount = $report->getOwner() ? $report->getOwner()->getAccount() : null;
            if ($ownerAccount && $this->notificationDetector->needsNotification($ownerAccount)) {
                $accounts[$ownerAccount->getId()] = $ownerAccount;
            }
        } else {
            $report->setOwnerHasAccess(false);
        }

        // send the mails
        foreach ($accounts as $account) {
            $notification = new ReportNotification();
            $notification
                ->setUser($account)
                ->setReport($report)
                ->setIsNew($this->notificationDetector->isNew())
                ->setIsComplete($this->notificationDetector->isComplete())
                ->setIsUrgent($this->notificationDetector->isUrgent())
                ->setSendMail(true)
                ->setStatus(Notification::NEW_NOTIFICATION);
            $this->entityManager->persist($notification);
        }

        $this->entityManager->flush();
    }
}
