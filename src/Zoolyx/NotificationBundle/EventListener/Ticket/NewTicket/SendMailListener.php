<?php

namespace Zoolyx\NotificationBundle\EventListener\Ticket\NewTicket;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Swift_Mailer;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Zoolyx\CoreBundle\Events;
use Zoolyx\CoreBundle\Model\Url\AbsoluteUrlGenerator;
use Zoolyx\TicketBundle\Entity\Ticket;
use Zoolyx\TicketBundle\Event\NewTicketEvent;


/**
 * @DI\Service
 */
class SendMailListener
{
    /** @var ObjectManager */
    private $entityManager;

    /** @var Swift_Mailer */
    private $mailer;

    /** @var TwigEngine */
    private $templating;

    /** @var AbsoluteUrlGenerator */
    private $absoluteUrlGenerator;

    /** @var string */
    private $from;

    /**
     * Constructor.
     *
     * @param ObjectManager $entityManager
     * @param Swift_Mailer $mailer
     * @param TwigEngine $templating
     * @param AbsoluteUrlGenerator $absoluteUrlGenerator
     * @param string $from
     *
     * @DI\InjectParams({
     *  "entityManager" = @DI\Inject("doctrine.orm.entity_manager"),
     *  "mailer" = @DI\Inject("mailer"),
     *  "templating" = @DI\Inject("templating"),
     *  "absoluteUrlGenerator" = @DI\Inject("zoolyx_core.generator.absolute_url"),
     *  "from" = @DI\Inject("%ticket_from%")
     * })
     */
    public function __construct( ObjectManager $entityManager, Swift_Mailer $mailer, TwigEngine $templating, AbsoluteUrlGenerator $absoluteUrlGenerator, $from)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->absoluteUrlGenerator = $absoluteUrlGenerator;
        $this->from = $from;
    }

    /**
     * @DI\Observe(Events::NEW_TICKET)
     *
     * @param NewTicketEvent $newTicketEvent
     */
    public function onResult(NewTicketEvent $newTicketEvent)
    {
        /* @var Ticket $ticket */
        $ticket = $newTicketEvent->getTicket();
        $creator = $newTicketEvent->getCreator();
        $lastMessage = $newTicketEvent->getLastMessage();

        //we versturen enkel al admin zelf een bericht voor de dierenarts heeft aangemaakt
        if ($creator->getId() != $ticket->getOwner()->getId()) {
            $parameters = array(
                'reportQuestionUrl' => $this->absoluteUrlGenerator->generate('zoolyx_report_question',array('hash'=> $ticket->getReport()->getHash())),
                'user' => $ticket->getOwner(),
                'requestId' => $ticket->getReport()->getRequestId(),
                'lastMessage' => $lastMessage
            );
            $htmlContent = $this->templating->render('ZoolyxTemplateBundle:Mail/Ticket:new_admin.html.twig', $parameters);
            $plainContent = $this->templating->render('ZoolyxTemplateBundle:Mail/Ticket:new_admin.plain.twig', $parameters);

            $message = new \Swift_Message("[ticket #".$ticket->getId()."] new");
            $message
                ->setFrom($this->from)
                ->setTo($ticket->getOwner()->getEmail())
                ->setBody($htmlContent,'text/html')
                ->addPart($plainContent,'text/plain')
            ;
            $this->mailer->send($message);
        }
    }

}
