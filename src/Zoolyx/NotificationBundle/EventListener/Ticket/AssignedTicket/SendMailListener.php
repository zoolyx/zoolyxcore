<?php

namespace Zoolyx\NotificationBundle\EventListener\Ticket\AssignedTicket;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Swift_Mailer;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Zoolyx\CoreBundle\Events;
use Zoolyx\CoreBundle\Model\Url\AbsoluteUrlGenerator;
use Zoolyx\TicketBundle\Entity\MailEvent;
use Zoolyx\TicketBundle\Entity\MessageEvent;
use Zoolyx\TicketBundle\Entity\Ticket;
use Zoolyx\TicketBundle\Entity\WatchTicket;
use Zoolyx\TicketBundle\Event\AssignedTicketEvent;
use Zoolyx\TicketBundle\Event\UpdatedTicketEvent;


/**
 * @DI\Service
 */
class SendMailListener
{
    /** @var ObjectManager */
    private $entityManager;

    /** @var Swift_Mailer */
    private $mailer;

    /** @var TwigEngine */
    private $templating;

    /** @var AbsoluteUrlGenerator */
    private $absoluteUrlGenerator;

    /** @var string */
    private $from;

    /**
     * Constructor.
     *
     * @param ObjectManager $entityManager
     * @param Swift_Mailer $mailer
     * @param TwigEngine $templating
     * @param AbsoluteUrlGenerator $absoluteUrlGenerator
     * @param string $from
     *
     * @DI\InjectParams({
     *  "entityManager" = @DI\Inject("doctrine.orm.entity_manager"),
     *  "mailer" = @DI\Inject("mailer"),
     *  "templating" = @DI\Inject("templating"),
     *  "absoluteUrlGenerator" = @DI\Inject("zoolyx_core.generator.absolute_url"),
     *  "from" = @DI\Inject("%from%")
     * })
     */
    public function __construct( ObjectManager $entityManager, Swift_Mailer $mailer, TwigEngine $templating, AbsoluteUrlGenerator $absoluteUrlGenerator, $from)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->absoluteUrlGenerator = $absoluteUrlGenerator;
        $this->from = $from;
    }

    /**
     * @DI\Observe(Events::ASSIGNED_TICKET)
     *
     * @param AssignedTicketEvent $assignedTicketEvent
     */
    public function onResult(AssignedTicketEvent $assignedTicketEvent)
    {
        /* @var Ticket $ticket */
        $ticket = $assignedTicketEvent->getTicket();
        $email = $assignedTicketEvent->getEmail();


        $parameters = array(
            'ticketUrl' => $this->absoluteUrlGenerator->generate('zoolyx_ticket_ticket',array('id'=> $ticket->getId())),
            'ticketId' => $ticket->getId()
        );
        $htmlContent = $this->templating->render('ZoolyxTemplateBundle:Mail/Ticket:assigned.html.twig', $parameters);
        $plainContent = $this->templating->render('ZoolyxTemplateBundle:Mail/Ticket:assigned.plain.twig', $parameters);
        $message = new \Swift_Message("[ticket #".$ticket->getId()."] assignment");
        $message
            ->setFrom($this->from)
            ->setTo($email)
            ->setBody($htmlContent,'text/html')
            ->addPart($plainContent,'text/plain')
        ;
        $this->mailer->send($message);
    }

}
