<?php

namespace Zoolyx\NotificationBundle\EventListener\Ticket\UpdatedTicket;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Swift_Mailer;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Zoolyx\CoreBundle\Events;
use Zoolyx\CoreBundle\Model\Url\AbsoluteUrlGenerator;
use Zoolyx\TicketBundle\Entity\MessageEvent;
use Zoolyx\TicketBundle\Entity\TicketCopy;
use Zoolyx\TicketBundle\Entity\WatchTicket;
use Zoolyx\TicketBundle\Event\UpdatedTicketEvent;


/**
 * @DI\Service
 */
class SendMailListener
{
    /** @var ObjectManager */
    private $entityManager;

    /** @var Swift_Mailer */
    private $mailer;

    /** @var TwigEngine */
    private $templating;

    /** @var AbsoluteUrlGenerator */
    private $absoluteUrlGenerator;

    /** @var string */
    private $from;

    /**
     * Constructor.
     *
     * @param ObjectManager $entityManager
     * @param Swift_Mailer $mailer
     * @param TwigEngine $templating
     * @param AbsoluteUrlGenerator $absoluteUrlGenerator
     * @param string $from
     *
     * @DI\InjectParams({
     *  "entityManager" = @DI\Inject("doctrine.orm.entity_manager"),
     *  "mailer" = @DI\Inject("mailer"),
     *  "templating" = @DI\Inject("templating"),
     *  "absoluteUrlGenerator" = @DI\Inject("zoolyx_core.generator.absolute_url"),
     *  "from" = @DI\Inject("%ticket_from%")
     * })
     */
    public function __construct( ObjectManager $entityManager, Swift_Mailer $mailer, TwigEngine $templating, AbsoluteUrlGenerator $absoluteUrlGenerator, $from)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->absoluteUrlGenerator = $absoluteUrlGenerator;
        $this->from = $from;
    }

    /**
     * @DI\Observe(Events::UPDATED_TICKET)
     *
     * @param UpdatedTicketEvent $updatedTicketEvent
     */
    public function onResult(UpdatedTicketEvent $updatedTicketEvent)
    {
        /* @var MessageEvent $ticketEvent */
        $ticketEvent = $updatedTicketEvent->getTicketEvent();
        $ticket = $ticketEvent->getTicket();
        $lastMessage =
            $ticketEvent instanceof MessageEvent ?
            $ticketEvent->getContent() :
            "";

        /** @var WatchTicket $watcher */
        foreach ($ticket->getWatchers() as $watcher) {
            $user = $watcher->getUser();
            if ($ticketEvent->isInternal() && !$user->hasRole('ROLE_ADMINISTRATOR')) {
                continue;
            }

            if ($watcher->hasNew())
            {
                $routeName = $user->hasRole('ROLE_ADMINISTRATOR') || $user->hasRole('ROLE_SUPER_ADMIN') ? 'zoolyx_ticket_ticket' : 'zoolyx_user_ticket';
                $twigName = $user->hasRole('ROLE_ADMINISTRATOR') || $user->hasRole('ROLE_SUPER_ADMIN') ? 'update_admin' : 'update';

                $parameters = array(
                    'ticketUrl' => $this->absoluteUrlGenerator->generate($routeName,array('id'=> $ticket->getId())),
                    'user' => $user,
                    'ticketId' => $ticket->getId(),
                    'lastMessage' => $lastMessage
                );
                $htmlContent = $this->templating->render('ZoolyxTemplateBundle:Mail/Ticket:'.$twigName.'.html.twig', $parameters);
                $plainContent = $this->templating->render('ZoolyxTemplateBundle:Mail/Ticket:'.$twigName.'.plain.twig', $parameters);

                $message = new \Swift_Message("[ticket #".$ticket->getId()."] update");
                $message
                    ->setFrom($this->from)
                    ->setTo($user->getEmail())
                    ->setBody($htmlContent,'text/html')
                    ->addPart($plainContent,'text/plain')
                ;
                $this->mailer->send($message);
            }
        }

        if ($lastMessage != "" && !$ticketEvent->isInternal()) {
            /** @var TicketCopy $ticketCopy */
            foreach ($ticket->getCopied() as $ticketCopy)
            {
                $twigName = 'update_copy';

                $parameters = array(
                    'ticketId' => $ticket->getId(),
                    'lastMessage' => $lastMessage
                );
                $htmlContent = $this->templating->render('ZoolyxTemplateBundle:Mail/Ticket:'.$twigName.'.html.twig', $parameters);
                $plainContent = $this->templating->render('ZoolyxTemplateBundle:Mail/Ticket:'.$twigName.'.plain.twig', $parameters);

                $message = new \Swift_Message("[ticket #".$ticket->getId()."] update");
                $message
                    ->setFrom($this->from)
                    ->setTo($ticketCopy->getEmail())
                    ->setBody($htmlContent,'text/html')
                    ->addPart($plainContent,'text/plain')
                ;
                $this->mailer->send($message);
            }
        }
    }

}
