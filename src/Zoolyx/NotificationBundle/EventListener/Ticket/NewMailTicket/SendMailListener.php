<?php

namespace Zoolyx\NotificationBundle\EventListener\Ticket\NewMailTicket;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Swift_Mailer;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Zoolyx\CoreBundle\Events;
use Zoolyx\CoreBundle\Model\Url\AbsoluteUrlGenerator;
use Zoolyx\TicketBundle\Entity\Ticket;
use Zoolyx\TicketBundle\Event\NewMailTicketEvent;


/**
 * @DI\Service
 */
class SendMailListener
{
    /** @var ObjectManager */
    private $entityManager;

    /** @var Swift_Mailer */
    private $mailer;

    /** @var TwigEngine */
    private $templating;

    /** @var AbsoluteUrlGenerator */
    private $absoluteUrlGenerator;

    /** @var string */
    private $from;

    /**
     * Constructor.
     *
     * @param ObjectManager $entityManager
     * @param Swift_Mailer $mailer
     * @param TwigEngine $templating
     * @param AbsoluteUrlGenerator $absoluteUrlGenerator
     * @param string $from
     *
     * @DI\InjectParams({
     *  "entityManager" = @DI\Inject("doctrine.orm.entity_manager"),
     *  "mailer" = @DI\Inject("mailer"),
     *  "templating" = @DI\Inject("templating"),
     *  "absoluteUrlGenerator" = @DI\Inject("zoolyx_core.generator.absolute_url"),
     *  "from" = @DI\Inject("%from%")
     * })
     */
    public function __construct( ObjectManager $entityManager, Swift_Mailer $mailer, TwigEngine $templating, AbsoluteUrlGenerator $absoluteUrlGenerator, $from)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->absoluteUrlGenerator = $absoluteUrlGenerator;
        $this->from = $from;
    }

    /**
     * @DI\Observe(Events::NEW_MAIL_TICKET)
     *
     * @param NewMailTicketEvent $newMailTicketEvent
     */
    public function onResult(NewMailTicketEvent $newMailTicketEvent)
    {
        /* @var Ticket $ticket */
        $ticket = $newMailTicketEvent->getTicket();
        $creator = $newMailTicketEvent->getCreator();
        
        if ( !preg_match_all('/^.+@zoolyx\.be$/m', $creator->getEmail(), $matches) ) {
			$originalSubject = $newMailTicketEvent->getOriginalSubject();
			$originalMessage = $newMailTicketEvent->getOriginalMessage();

			$parameters = array(
				'ticketUrl' => $this->absoluteUrlGenerator->generate('zoolyx_user_ticket',array('id'=> $ticket->getId())),
				'originalMessage' => $originalMessage,
				'user' => $creator
			);
			$htmlContent = $this->templating->render('ZoolyxTemplateBundle:Mail/Ticket:confirm_mail_received.html.twig', $parameters);
			$plainContent = $this->templating->render('ZoolyxTemplateBundle:Mail/Ticket:confirm_mail_received.plain.twig', $parameters);

			$message = new \Swift_Message("[ticket #".$ticket->getId()."] " . $originalSubject);
			$message
				->setFrom($this->from)
				->setTo($creator->getEmail())
				->setBody($htmlContent,'text/html')
				->addPart($plainContent,'text/plain')
			;

			$this->mailer->send($message);
        }
    }

}
