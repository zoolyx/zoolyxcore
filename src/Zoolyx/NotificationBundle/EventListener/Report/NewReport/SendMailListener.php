<?php

namespace Zoolyx\NotificationBundle\EventListener\Report\NewReport;

use JMS\DiExtraBundle\Annotation as DI;
use Swift_Mailer;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Event\ReportUserEvent;
use Zoolyx\CoreBundle\Events;
use Zoolyx\CoreBundle\Model\Url\AbsoluteUrlGenerator;
use Zoolyx\NotificationBundle\Model\Mail\MailMessage;


/**
 * @DI\Service
 */
class SendMailListener
{
    /** @var Swift_Mailer */
    private $mailer;

    /** @var TwigEngine */
    private $templating;

    /** @var AbsoluteUrlGenerator */
    private $absoluteUrlGenerator;

    /** @var string */
    private $stubTo;
    /** @var string */
    private $from;
    /**
     * Constructor.
     *
     * @param Swift_Mailer $mailer
     * @param TwigEngine $templating
     * @param AbsoluteUrlGenerator $absoluteUrlGenerator
     * @param string $stubTo
     * @param string $from
     *
     * @DI\InjectParams({
     *  "mailer" = @DI\Inject("mailer"),
     *  "templating" = @DI\Inject("templating"),
     *  "absoluteUrlGenerator" = @DI\Inject("zoolyx_core.generator.absolute_url"),
     *  "stubTo" = @DI\Inject("%stub_to%"),
     *  "from" = @DI\Inject("%from%")
     * })
     */
    public function __construct(Swift_Mailer $mailer, TwigEngine $templating, AbsoluteUrlGenerator $absoluteUrlGenerator, $stubTo, $from) {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->absoluteUrlGenerator = $absoluteUrlGenerator;
        $this->stubTo = $stubTo;
        $this->from = $from;
    }

    /**
     * @DI\Observe(Events::REPORT_IMPORTED_FOR_USER)
     *
     * @param ReportUserEvent $reportUserEvent
     */
    public function onResult(ReportUserEvent $reportUserEvent)
    {
        /* @var Report $trace */
        $report = $reportUserEvent->getReport();
        /** @var User $user */
        $user = $reportUserEvent->getUser();
        $language = $user->getLanguage();

        $htmlContent = $this->templating->render('ZoolyxTemplateBundle:Mail/Report:new_'.$language.'.html.twig', array(
            'report' => $report,
            'user' => $user
        ));
        $plainContent = $this->templating->render('ZoolyxTemplateBundle:Mail/Report:new_'.$language.'.plain.twig', array(
            'report' => $report,
            'user' => $user
        ));

        $mailMessage = new MailMessage($this->stubTo);
        $mailMessage
            ->setSubject("Rapport [request #".$report->getRequestId()."]")
            ->setFrom($this->from)
            ->setTo($user->getEmail())
            ->setHtml($htmlContent,'text/html')
            ->setPlain($plainContent ,'text/plain')
        ;
        $this->mailer->send($mailMessage->getSwiftMessage());
    }
}
