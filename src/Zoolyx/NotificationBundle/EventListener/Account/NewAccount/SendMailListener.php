<?php

namespace Zoolyx\NotificationBundle\EventListener\Account\NewAccount;

use JMS\DiExtraBundle\Annotation as DI;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Event\NewAccountEvent;
use Zoolyx\CoreBundle\Event\ReportUserEvent;
use Zoolyx\CoreBundle\Events;
use Zoolyx\CoreBundle\Model\Url\AbsoluteUrlGenerator;
use Zoolyx\NotificationBundle\Model\Mail\MailMessage;


/**
 * @DI\Service
 */
class SendMailListener
{
    /** @var Swift_Mailer */
    private $mailer;

    /** @var TwigEngine */
    private $templating;

    /** @var AbsoluteUrlGenerator */
    private $absoluteUrlGenerator;

    /** @var string */
    private $from;
    /** @var string */
    private $baseUrl;
    /** @var string */
    private $scheme;

    /**
     * Constructor.
     *
     * @param Swift_Mailer $mailer
     * @param TwigEngine $templating
     * @param AbsoluteUrlGenerator $absoluteUrlGenerator
     * @param string $from
     * @param string $baseUrl
     * @param string $scheme
     *
     * @DI\InjectParams({
     *  "mailer" = @DI\Inject("mailer"),
     *  "templating" = @DI\Inject("templating"),
     *  "absoluteUrlGenerator" = @DI\Inject("zoolyx_core.generator.absolute_url"),
     *  "from" = @DI\Inject("%from%"),
     *  "baseUrl" = @DI\Inject("%base_url%"),
     *  "scheme" = @DI\Inject("%scheme%"),
     * })
     */
    public function __construct(
        Swift_Mailer $mailer,
        TwigEngine $templating,
        AbsoluteUrlGenerator $absoluteUrlGenerator,
        $from, $baseUrl, $scheme)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->absoluteUrlGenerator = $absoluteUrlGenerator;
        $this->from = $from;
        $this->baseUrl = $baseUrl;
        $this->scheme = $scheme;
    }

    /**
     * @DI\Observe(Events::NEW_ACCOUNT)
     *
     * @param NewAccountEvent $newAccountEvent
     */
    public function onResult(NewAccountEvent $newAccountEvent)
    {
        /* @var User $user */
        $user = $newAccountEvent->getUser();

        $confirmationToken =  $user->getConfirmationToken();
        $language = $user->getLanguage();
        $parameters = array(
            'user' => $user,
            'changePasswordUrl' => $user->getLanguage() != '' ?
                $this->absoluteUrlGenerator->generate('zoolyx_account_password_reset_with_language',array(
                    'token'=>$confirmationToken,
                    'language'=>$language
                )):
                $this->absoluteUrlGenerator->generate('zoolyx_account_password_reset',array(
                    'token'=>$confirmationToken
                ))
        );
        $htmlContent = $this->templating->render('ZoolyxTemplateBundle:Mail/AccountActivation:activate_'.$language.'.html.twig', $parameters);
        $plainContent = $this->templating->render('ZoolyxTemplateBundle:Mail/AccountActivation:activate_'.$language.'.plain.twig', $parameters);

        $message = new \Swift_Message("Activate account");
        $message
            ->setFrom($this->from)
            ->setTo($user->getEmail())
            ->setBody($htmlContent,'text/html')
            ->addPart($plainContent,'text/plain')
        ;
        $this->mailer->send($message);
    }

}
