<?php

namespace Zoolyx\NotificationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ZoolyxNotificationBundle:Default:index.html.twig', array('name' => $name));
    }
}
