<?php

namespace Zoolyx\NotificationBundle\Model;

use DOMDocument;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use XSLTProcessor;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Zoolyx\CoreBundle\Entity\NotificationEndpoint;
use Zoolyx\CoreBundle\Entity\Owner;
use Zoolyx\CoreBundle\Entity\Practice;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\Sample;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Entity\VeterinaryPracticeReport;
use Zoolyx\CoreBundle\Model\Url\AbsoluteUrlGenerator;
use Zoolyx\NotificationBundle\Entity\ReportNotification;

/**
 * NotificationGenerator
 *
 * @DI\Service("zoolyx_notification.generator.xml")
 */
class NotificationGenerator
{
    /** @var AbsoluteUrlGenerator */
    protected $absoluteUrlGenerator;

    /**
     * Constructor.
     *
     * @param AbsoluteUrlGenerator $absoluteUrlGenerator
     *
     * @DI\InjectParams({
     *  "absoluteUrlGenerator" = @DI\Inject("zoolyx_core.generator.absolute_url")
     * })
     */
    public function __construct(AbsoluteUrlGenerator $absoluteUrlGenerator) {
        $this->absoluteUrlGenerator = $absoluteUrlGenerator;
    }

    /**
     * @param ReportNotification $notification
     * @return string
     */
    public function getXml(ReportNotification $notification) {
        /** @var NotificationEndpoint $notificationEndpoint */
        $notificationEndpoint = $notification->getVeterinaryPractice()->getPractice()->getNotificationEndpoint();

        if (is_null($notificationEndpoint)) {
            return null;
        }

        /** @var DOMDocument $baseDoc */
        $baseDoc = $this->getBaseXml($notification);

        if (is_null($notificationEndpoint->getXslt())) {
            return $baseDoc->saveXml();
        }

        $xslDoc = new DOMDocument();
        $xslDoc->loadXml($notificationEndpoint->getXslt());

        $xsltProcessor = new XSLTProcessor();
        $xsltProcessor->importStylesheet($xslDoc);
        /** @var DOMDocument $doc */
        $doc = $xsltProcessor->transformToXML($baseDoc);
        return $doc;
    }

    /**
     * @param ReportNotification ReportNotification $notification
     * @return string
     */
    private function getBaseXml(ReportNotification $notification) {
        /** @var Report $report */
        $report = $notification->getReport();

        /** @var User $veterinaryAccount */
        $veterinaryAccount = $notification->getUser();

        /** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = null;
        /** @var Veterinary $veterinary */
        $veterinary = null;
        /** @var Practice $practice */
        $practice = null;

        /** @var VeterinaryPracticeReport $vpr */
        foreach ($report->getVeterinaryPracticeReports() as $vpr) {
            if ($vpr->getRole()==1) {
                $veterinaryPractice = $vpr->getVeterinaryPractice();
                if ($veterinaryPractice) {
                    $veterinary = $veterinaryPractice->getVeterinary();
                    $practice = $veterinaryPractice->getPractice();
                }
            }
        }

        $doc = new DOMDocument("1.0","UTF-8");
        $requestDoc = $doc->createElement('rq');
        $doc->appendChild($requestDoc);

        $requestDoc->appendChild($this->getDoc($doc,'practiceRef',$report->getPracticeReference()));
        $requestDoc->appendChild($this->getDoc($doc,'dateCreated',$report->getCreatedOn()->format('Y-m-d H:i:s')));
        $requestDoc->appendChild($this->getDoc($doc,'requestId',$report->getRequestId()));
        $requestDoc->appendChild($this->getDoc($doc,'reportHash',$report->getHash()));

        $requestDoc->appendChild($this->getDoc($doc,'reportUrl',$this->absoluteUrlGenerator->generate('zoolyx_report',array('hash'=>$report->getHash()))));
        $requestDoc->appendChild($this->getDoc($doc,'completed',$notification->getReportStatus() ? 'VOLLEDIG' : 'PARTIEEL'));
        $requestDoc->appendChild($this->getDoc($doc,'completedCode',$notification->getReportStatus() ? '1' : '0'));

        $veterinaryDoc = $doc->createElement('da');
        $veterinaryDoc->setAttribute('id',$veterinaryPractice ? $veterinaryPractice->getLimsId():'');
        $veterinaryDoc->setAttribute('extId',$notification->getVeterinaryPractice()->getNotificationId());
        $requestDoc->appendChild($veterinaryDoc);
        $veterinaryDoc->appendChild($this->getDoc($doc,'lastName',$veterinary ? $veterinary->getLastName() : $veterinaryAccount->getLastName()));
        $veterinaryDoc->appendChild($this->getDoc($doc,'firstName',$veterinary ? $veterinary->getFirstName() : $veterinaryAccount->getFirstName()));
        $veterinaryDoc->appendChild($this->getDoc($doc,'street', $practice ? $practice->getStreet() : $veterinaryAccount->getStreet()));
        $veterinaryDoc->appendChild($this->getDoc($doc,'zipCode',$practice ? $practice->getZipCode() : $veterinaryAccount->getZipCode()));
        $veterinaryDoc->appendChild($this->getDoc($doc,'city',$practice ? $practice->getCity() : $veterinaryAccount->getCity()));
        $veterinaryDoc->appendChild($this->getDoc($doc,'country',$practice ? $practice->getCountry() : $veterinaryAccount->getCountry()));

        /** @var Owner $owner */
        $owner = $report->getOwner();
        $ownerDoc = $doc->createElement('ad');
        $ownerDoc->setAttribute('id',$owner->getLimsId());
        $requestDoc->appendChild($ownerDoc);
        $ownerDoc->appendChild($this->getDoc($doc,'lastName',$owner->getLastName()));
        $ownerDoc->appendChild($this->getDoc($doc,'firstName',$owner->getFirstName()));
        $ownerDoc->appendChild($this->getDoc($doc,'street',$owner->getStreet()));
        $ownerDoc->appendChild($this->getDoc($doc,'zipCode',$owner->getZipCode()));
        $ownerDoc->appendChild($this->getDoc($doc,'city',$owner->getCity()));
        $ownerDoc->appendChild($this->getDoc($doc,'country',$owner->getCountry()));

        $reportVersion = $report->getDefaultVersion();
        /** @var Sample $sample */
        foreach ($reportVersion->getSamples() as $sample) {
            $sampleDoc = $doc->createElement('sc');
            $sampleDoc->setAttribute('id', $sample->getCode());
            $sampleDoc->appendChild($this->getDoc($doc,'dateReceived',$sample->getDateReceived() ? $sample->getDateReceived()->format('Y-m-d') : ''));
            $sampleDoc->appendChild($this->getDoc($doc,'dateCompleted',$sample->getDateCompleted() ? $sample->getDateCompleted()->format('Y-m-d') : ''));

            $requestDoc->appendChild($sampleDoc);

            $pet = $sample->getPet();
            $petDoc = $doc->createElement('pe');
            $petDoc->setAttribute('id',$pet->getPetId());
            $sampleDoc->appendChild($petDoc);
            $petDoc->appendChild($this->getDoc($doc,'name',$pet->getName()));
            $petDoc->appendChild($this->getDoc($doc,'speciesCode',$pet->getSpeciesCode()));
            $petDoc->appendChild($this->getDoc($doc,'gender',$pet->getGender()));
            $petDoc->appendChild($this->getDoc($doc,'birthDate',$pet->getBirthDate() ? $pet->getBirthDate()->format('Y-m-d') : ''));
            $petDoc->appendChild($this->getDoc($doc,'breed',$pet->getBreed()));
            $petDoc->appendChild($this->getDoc($doc,'chipId',$pet->getChipNbr()));
        }

        return $doc;
    }

    /**
     * @param DOMDocument $doc
     * @param string $name
     * @param string $value
     * @return mixed
     */
    private function getDoc($doc,$name,$value) {
        $elementDoc = $doc->createElement($name);
        $elementDoc->nodeValue = str_replace("&","&amp;",$value);
        return $elementDoc;
    }

}

