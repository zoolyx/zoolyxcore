<?php

namespace Zoolyx\NotificationBundle\Model\Report;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\AccountBundle\Forms\Account\Account;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\Sample;
use Zoolyx\CoreBundle\Entity\User;

/**
 * NotificationGenerator
 *
 * @DI\Service("zoolyx_notification.report.notification_detector")
 */
class NotificationDetector
{
    /** @var Report */
    private $report;
    private $isComplete = false;
    private $isUrgent = false;
    private $isNew = false;

    /**
     * @param mixed $report
     */
    public function setReport($report)
    {
        $this->report = $report;
        $this->isComplete = true;
        $this->isUrgent = false;
        $this->isNew = $report->isNew();
        /** @var Sample $sample */
        foreach ($report->getDefaultVersion()->getSamples() as $sample) {
            if ($sample->getCompleted() == 0) {
                $this->isComplete = false;
            }
            if ($sample->isUrgent()) {
                $this->isUrgent = true;
            }
        }
    }

    public function isComplete() {
        return $this->isComplete;
    }
    public function isNew() {
        return $this->isNew;
    }
    public function isUrgent() {
        return $this->isUrgent;
    }
    public function needsNotification(User $user) {
        if ($this->isUrgent) {
            return true;
        }

        if ($user->getReportFrequency() == Account::FREQUENCY_EVERY_VERSION)
            return true;

        if (
            $user->getReportFrequency() == Account::FREQUENCY_ONLY_COMPLETED &&
            $this->isComplete
        )
            return true;

        if (
            $user->getReportFrequency() == Account::FREQUENCY_FIRST_AND_COMPLETED &&
            ($this->isComplete || $this->isNew)
        )
            return true;

        return false;
    }


}

