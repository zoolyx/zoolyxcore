<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 09.07.18
 * Time: 14:53
 */
namespace Zoolyx\NotificationBundle\Model\Mail;


class MailMessage {


    /** @var string */
    private $subject;

    /** @var string */
    private $to;

    /** @var string */
    private $stubTo;

    /** @var string */
    private $from;

    /** @var string */
    private $html;

    /** @var string */
    private $plain;


    public function __construct($stubTo) {
        $this->stubTo = $stubTo;
    }

    /**
     * @param string $from
     * @return $this
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @param string $html
     * @return $this
     */
    public function setHtml($html)
    {
        $this->html = $html;
        return $this;
    }

    /**
     * @param string $mailer
     * @return $this
     */
    public function setMailer($mailer)
    {
        $this->mailer = $mailer;
        return $this;
    }

    /**
     * @param string $plain
     * @return $this
     */
    public function setPlain($plain)
    {
        $this->plain = $plain;
        return $this;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param string $to
     * @return $this
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return \Swift_Message
     */
    public function getSwiftMessage() {
        return $this->getMessage();
    }

    private function getStubMessage() {
        $message = new \Swift_Message($this->subject . "(to: ".$this->to.")");
        $message
            ->setFrom($this->from)
            ->setTo($this->stubTo)
            ->setBody($this->html,'text/html')
            ->addPart($this->plain,'text/plain')
        ;
        return $message;
    }

    private function getMessage() {
        $message = new \Swift_Message($this->subject);
        $message
            ->setFrom($this->from)
            ->setTo($this->to)
            ->setBody($this->html,'text/html')
            ->addPart($this->plain,'text/plain')
        ;
        return $message;
    }

} 