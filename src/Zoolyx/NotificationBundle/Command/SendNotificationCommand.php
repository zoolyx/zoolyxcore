<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 13.03.17
 * Time: 16:28
 */

namespace Zoolyx\NotificationBundle\Command;

use Doctrine\Common\Persistence\ObjectManager;
use DOMDocument;
use Exception;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zoolyx\CoreBundle\Entity\NotificationEndpoint;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\NotificationBundle\Entity\Notification;
use Zoolyx\NotificationBundle\Entity\ReportNotification;
use Zoolyx\NotificationBundle\Entity\Repository\ReportNotificationRepository;
use Zoolyx\NotificationBundle\Model\Mail\MailMessage;
use Zoolyx\NotificationBundle\Model\NotificationGenerator;

class SendNotificationCommand extends ContainerAwareCommand {


    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:send-notifications')

            // the short description shown while running "php bin/console list"
            ->setDescription('Sends new notifications.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command will send a notification to all veterinaries and organisations the received an update of a report...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var NotificationGenerator $notificationGenerator */
        $notificationGenerator = $this->getContainer()->get('zoolyx_notification.generator.xml');
        /** @var ObjectManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var ReportNotificationRepository $reportNotificationRepository */
        $reportNotificationRepository = $this->getContainer()->get('zoolyx_notification.repository.report_notification');

        /** @var Swift_Mailer $mailer */
        $mailer = $this->getContainer()->get('mailer');
        /** @var TwigEngine $templating */
        $templating = $this->getContainer()->get('templating');
        /** @var string $stubTo */
        $stubTo = $this->getContainer()->getParameter('stub_to');
        /** @var string $from */
        $from = $this->getContainer()->getParameter('from');

        $notifications = $reportNotificationRepository->findBy(array('status' => Notification::NEW_NOTIFICATION), null, 10);


        $output->writeln(date("Y-m-d H:i:s",time()).' - Notification service started');
        /** @var ReportNotification $notification */
        foreach ($notifications as $notification)
        {
            /** @var User $user */
            $user = $notification->getUser();
            /** @var Report $report */
            $report = $notification->getReport();

            if ($notification->sendMail())
            {
                $output->writeln('will send mail sent for notification '.$notification->getId().' to '.$user->getEmail() . " for report ".$report->getRequestId());

                $htmlContent = $templating->render('ZoolyxTemplateBundle:Mail/Report:new_'.$user->getLanguage().'.html.twig', array(
                    'report' => $report,
                    'notification' => $notification,
                    'user' => $user
                ));
                $plainContent = $templating->render('ZoolyxTemplateBundle:Mail/Report:new_'.$user->getLanguage().'.plain.twig', array(
                    'report' => $report,
                    'notification' => $notification,
                    'user' => $user
                ));

                $subject = "Rapport ";
                if ($report->getOwner()) {
                    $subject .= $report->getOwner()->getLastName();
                }
                $subject.= "[request #".$report->getRequestId()."]";

                $mailMessage = new MailMessage($stubTo);
                $mailMessage
                    ->setSubject($subject)
                    ->setFrom($from)
                    ->setTo($user->getEmail())
                    ->setHtml($htmlContent,'text/html')
                    ->setPlain($plainContent ,'text/plain')
                ;
                $mailer->send($mailMessage->getSwiftMessage());

                $notification->setStatus(Notification::NOTIFICATION_SENT);
                $entityManager->persist($notification);
                $entityManager->flush();
                $output->writeln('mail sent for notification '.$notification->getId().' to '.$user->getEmail() . " for report ".$report->getRequestId());
            } else {
                $xml = null;
                try {
                    /** @var DomDocument $xml */
                    $xml = $notificationGenerator->getXml($notification);
                } catch (Exception $e) {
                    $notification->setStatus(Notification::NOTIFICATION_ERROR);
                    $entityManager->persist($notification);
                    $entityManager->flush();
                    $output->writeln('Caught exception: ' . $e->getMessage());
                    $output->writeln('notification was not sent');
                }

                if (is_null($xml)) {
                    continue;
                }

                /** @var NotificationEndpoint $notificationEndpoint */
                $notificationEndpoint = $notification->getVeterinaryPractice()->getPractice()->getNotificationEndpoint();

                $output->writeln('will send notification '.$notification->getId().' to '.$notificationEndpoint->getName() . " for user ".$user->getUsername());
                $output->writeln($xml);

                // send the notification
                $options = array(
                    'http' => array(
                        'header'  => "Content-type: application/xml; charset=utf-8",
                        'method'  => 'POST',
                        'content' => $xml
                    )
                );
                $context  = stream_context_create($options);

                $response = @file_get_contents($notificationEndpoint->getUrl(), false, $context);
                $responseCode = null;
                foreach ($http_response_header as $header) {
                    if ( preg_match( '~^'."HTTP/1.(?'version'\d) (?'code'\d+) .*".'$~i', $header, $responseCode ) )
                    {
                        break;
                    }
                }

                if (isset($responseCode["code"]) && $responseCode["code"]=="200") {
                    $notification->setStatus(Notification::NOTIFICATION_SENT);
                    /** @var ObjectManager $entityManager */
                    $entityManager->persist($notification);
                    $entityManager->flush();
                    $output->writeln('notification was sent successfully: '.$response);
                } else {
                    $notification->setStatus(Notification::NOTIFICATION_ERROR);
                    $entityManager->persist($notification);
                    $entityManager->flush();
                    $output->writeln('ERROR - notification was not sent: '.$response);
                }
            }
        }
    }
}