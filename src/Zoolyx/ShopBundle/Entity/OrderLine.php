<?php

namespace Zoolyx\ShopBundle\Entity;
use Zoolyx\CoreBundle\Entity\ParameterProfile;

/**
 * Parameter
 */
class OrderLine
{
    /** @var int */
    private $id;

    /** @var Order  */
    private $order;

    /** @var Product */
    private $product = null;

    /** @var ParameterProfile */
    private $parameterProfile = null;

    /** @var int */
    private $items;

    /** @var int */
    private $price;

    /** @var boolean */
    private $isAnalyse = false;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Cart
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return int
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param int $items
     * @return $this
     */
    public function setItems($items)
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return ParameterProfile
     */
    public function getParameterProfile()
    {
        return $this->parameterProfile;
    }

    /**
     * @param ParameterProfile $parameterProfile
     * @return $this
     */
    public function setParameterProfile($parameterProfile)
    {
        $this->parameterProfile = $parameterProfile;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isAnalyse()
    {
        return $this->isAnalyse;
    }

    /**
     * @param boolean $isAnalyse
     * @return $this
     */
    public function setIsAnalyse($isAnalyse)
    {
        $this->isAnalyse = $isAnalyse;
        return $this;
    }


}

