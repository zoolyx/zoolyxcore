<?php

namespace Zoolyx\ShopBundle\Entity;
use Zoolyx\CoreBundle\Entity\ProductLongDescription;
use Zoolyx\CoreBundle\Entity\ProductNameDescription;
use Zoolyx\CoreBundle\Entity\ProductShortDescription;

/**
 * Parameter
 */
class Product
{
    /** @var int */
    private $id;
    /** @var string */
    private $imageName;
    /** @var array */
    private $nameDescriptions;
    /** @var array */
    private $shortDescriptions;
    /** @var array */
    private $longDescriptions;
    /** @var string */
    private $reference;
    /** @var int */
    private $price;
    /** @var bool */
    private $visible=true;
    /** @var bool  */
    private $quantifiable = true;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param string $imageName
     * @return $this
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
        return $this;
    }

    /**
     * @return array
     */
    public function getLongDescriptions()
    {
        return $this->longDescriptions;
    }

    /**
     * @param string $language
     * @return string
     */
    public function getLongDescription($language = "nl") {
        /** @var ProductLongDescription $longDescription */
        foreach ($this->getLongDescriptions() as $longDescription) {
            if ($longDescription->getLanguage() == $language) {
                return $longDescription->getDescription();
            }
        }
        return "";
    }

    /**
     * @param array $longDescriptions
     * @return $this
     */
    public function setLongDescriptions($longDescriptions)
    {
        $this->longDescriptions = $longDescriptions;
        return $this;
    }

    /**
     * @return array
     */
    public function getNameDescriptions()
    {
        return $this->nameDescriptions;
    }

    /**
     * @param string $language
     * @return string
     */
    public function getName($language = "nl") {
        /** @var ProductNameDescription $nameDescription */
        foreach ($this->getNameDescriptions() as $nameDescription) {
            if ($nameDescription->getLanguage() == $language) {
                return $nameDescription->getDescription();
            }
        }
        return "";
    }
    /**
     * @param array $nameDescriptions
     * @return $this
     */
    public function setNameDescriptions($nameDescriptions)
    {
        $this->nameDescriptions = $nameDescriptions;
        return $this;
    }

    /**
     * @return array
     */
    public function getShortDescriptions()
    {
        return $this->shortDescriptions;
    }

    /**
     * @param string $language
     * @return string
     */
    public function getShortDescription($language = "nl") {
        /** @var ProductShortDescription $shortDescription */
        foreach ($this->getShortDescriptions() as $shortDescription) {
            if ($shortDescription->getLanguage() == $language) {
                return $shortDescription->getDescription();
            }
        }
        return "";
    }

    /**
     * @param array $shortDescriptions
     * @return $this
     */
    public function setShortDescriptions($shortDescriptions)
    {
        $this->shortDescriptions = $shortDescriptions;
        return $this;
    }


    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }


    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isQuantifiable()
    {
        return $this->quantifiable;
    }

    /**
     * @param boolean $quantifiable
     * @return $this
     */
    public function setQuantifiable($quantifiable)
    {
        $this->quantifiable = $quantifiable;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isVisible()
    {
        return $this->visible;
    }

    /**
     * @param boolean $visible
     * @return $this
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
        return $this;
    }


}

