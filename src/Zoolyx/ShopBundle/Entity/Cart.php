<?php

namespace Zoolyx\ShopBundle\Entity;

/**
 * Parameter
 */
class Cart
{
    /** @var int */
    private $id;

    /** @var int */
    private $items = 0;

    /** @var int */
    private $price = 0;

    /** @var array  */
    private $cartLines = array();

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getCartLines()
    {
        return $this->cartLines;
    }

    /**
     * @param CartLine $cartLine
     * @return $this
     */
    public function addCartLine($cartLine)
    {
        $this->cartLines[] = $cartLine;
        return $this;
    }

    /**
     * @return int
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param int $items
     * @return $this
     */
    public function setItems($items)
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }


}

