<?php

namespace Zoolyx\ShopBundle\Entity;
use Zoolyx\CoreBundle\Entity\User;

/**
 * Parameter
 */
class Order
{
    /** @var int */
    private $id;

    /** @var int */
    private $items;

    /** @var int */
    private $price;

    /** @var array  */
    private $orderLines = array();

    /** @var User */
    private $user;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getOrderLines()
    {
        return $this->orderLines;
    }

    /**
     * @param OrderLine $orderLine
     * @return $this
     */
    public function addOrderLine($orderLine)
    {
        $this->orderLines[] = $orderLine;
        return $this;
    }

    /**
     * @return int
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param int $items
     * @return $this
     */
    public function setItems($items)
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }


}

