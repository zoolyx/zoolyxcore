<?php

namespace Zoolyx\ShopBundle\Model\Address;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Practice;
use Zoolyx\CoreBundle\Entity\Repository\PracticeRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;

/**
 * @DI\Service("zoolyx_shop.provider.address")
 */

class AddressProvider
{
    /** @var PracticeRepository */
    private $practiceRepository;

    /**
     * Constructor.
     *
     * @param PracticeRepository $practiceRepository
     *
     * @DI\InjectParams({
     *  "practiceRepository" = @DI\Inject("zoolyx_core.repository.practice")
     * })
     */
    public function __construct(PracticeRepository $practiceRepository) {
        $this->practiceRepository = $practiceRepository;
    }

    /**
     * @param User $user
     * @return array
     */
    public function getAddresses($user) {
        $addresses = array();

        if ($user->hasRole('ROLE_VETERINARY')) {
            /** @var Veterinary $veterinary */
            $practices = $this->practiceRepository->searchForAccount($user);

            /** @var Practice $practice */
            foreach ($practices as $practice) {
                $address = new Address();
                $address
                    ->setPracticeId($practice->getId())
                    ->setName($practice->getName())
                    ->setStreet($practice->getStreet())
                    ->setZipCode($practice->getZipCode())
                    ->setCity($practice->getCity())
                    ->setCountry($practice->getCountry());

                $addresses[] = $address;
            }
        }
        return $addresses;
    }
}

