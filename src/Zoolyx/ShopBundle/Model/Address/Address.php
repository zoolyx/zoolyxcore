<?php

namespace Zoolyx\ShopBundle\Model\Address;



class Address
{
    /** @var string */
    private $name;

    /** @var string */
    private $street;

    /** @var string */
    private $city;

    /** @var string */
    private $zipCode;

    /** @var string */
    private $country;

    /** @var int */
    private $practiceId;

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return $this
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     * @return $this
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getPracticeId()
    {
        return $this->practiceId;
    }

    /**
     * @param int $practiceId
     * @return $this
     */
    public function setPracticeId($practiceId)
    {
        $this->practiceId = $practiceId;
        return $this;
    }


}

