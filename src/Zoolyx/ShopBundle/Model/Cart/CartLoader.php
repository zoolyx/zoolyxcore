<?php

namespace Zoolyx\ShopBundle\Model\Cart;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\Persistence\ObjectManager;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\ShopBundle\Entity\Cart;

/**
 * @DI\Service("zoolyx_shop.loader.cart")
 */
class CartLoader
{
    /** @var ObjectManager $entityManager */
    private $entityManager;

    /**
     * Constructor.
     *
     * @param ObjectManager $entityManager
     *
     * @DI\InjectParams({
     *  "entityManager" = @DI\Inject("doctrine.orm.entity_manager")
     * })
     */
    public function __construct(ObjectManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function getCart(User $user)
    {
        $cart = $user->getCart();

        if (is_null($cart)) {
            $cart = new Cart();
            $user->setCart($cart);
            $this->entityManager->persist($cart);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        return $cart;
    }
}

