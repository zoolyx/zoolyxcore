<?php

namespace Zoolyx\ShopBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Zoolyx\ShopBundle\Entity\Repository\ProductRepository;

class DefaultController extends Controller
{
    //http://www.opt.localhost/repositories/bart/elcodi/bamboo5/web/app_dev.php/shop/
    /**
     * @var ProductRepository
     *
     * @DI\Inject("zoolyx_shop.repository.product")
     */
    private $productRepository;

    public function indexAction()
    {
        $products = $this->productRepository->findBy(array("visible"=>true));
        return $this->render('ZoolyxShopBundle:Default:index.html.twig', array(
            'products' => $products
        ));
    }

    public function productAction($id) {
        $product = $this->productRepository->find($id);
        return $this->render('ZoolyxShopBundle:Default:product.html.twig', array(
            'product' => $product
        ));
    }
}
