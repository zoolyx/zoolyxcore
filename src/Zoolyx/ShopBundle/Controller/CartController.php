<?php

namespace Zoolyx\ShopBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Zoolyx\CoreBundle\Entity\ParameterProfile;
use Zoolyx\CoreBundle\Entity\ParameterProfileSampleType;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileRepository;
use Zoolyx\CoreBundle\Entity\Repository\SampleRepository;
use Zoolyx\CoreBundle\Entity\Sample;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Model\Lims\ParameterProfile as LimsParameterProfile;
use Zoolyx\CoreBundle\Model\Request\Object\RequestSampleType;
use Zoolyx\CoreBundle\Model\Request\RequestManager;
use Zoolyx\ShopBundle\Entity\Cart;
use Zoolyx\ShopBundle\Entity\CartLine;
use Zoolyx\ShopBundle\Entity\Order;
use Zoolyx\ShopBundle\Entity\OrderLine;
use Zoolyx\ShopBundle\Entity\Product;
use Zoolyx\ShopBundle\Entity\Repository\CartLineRepository;
use Zoolyx\ShopBundle\Entity\Repository\ProductRepository;
use Zoolyx\ShopBundle\Model\Address\AddressProvider;
use Zoolyx\ShopBundle\Model\Cart\CartLoader;

class CartController extends Controller
{
    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var CartLineRepository
     *
     * @DI\Inject("zoolyx_shop.repository.cart_line")
     */
    private $cartLineRepository;
    /**
     * @var ProductRepository
     *
     * @DI\Inject("zoolyx_shop.repository.product")
     */
    private $productRepository;
    /**
     * @var ParameterProfileRepository
     *
     * @DI\Inject("zoolyx_core.repository.parameter_profile")
     */
    private $parameterProfileRepository;
    /**
     * @var SampleRepository
     *
     * @DI\Inject("zoolyx_core.repository.sample")
     */
    private $sampleRepository;

    /**
     * @var RequestManager $requestManager
     *
     *  @DI\Inject("zoolyx_core.manager.request")
     */
    private $requestManager;

    /**
     * @var AddressProvider $addressProvider
     *
     *  @DI\Inject("zoolyx_shop.provider.address")
     */
    private $addressProvider;

    /**
     * @var CartLoader $cartLoader
     *
     * @DI\Inject("zoolyx_shop.loader.cart")
     */
    private $cartLoader;

    public function indexAction()
    {
        $cart = $this->cartLoader->getCart($this->getUser());

        $totalPrice = 0;
        $lines = 0;
        /** @var CartLine $cartLine */
        foreach ($cart->getCartLines() as $cartLine) {
            $totalPrice+= $cartLine->getPrice();
            $lines++;
        }
        $cart->setPrice($totalPrice)->setItems($lines);
        $this->entityManager->persist($cart);
        $this->entityManager->flush();

        return $this->render('ZoolyxShopBundle:Cart:index.html.twig', array(
            'cart' => $cart,
            'totalPrice' => $totalPrice,
            'lines' => $lines
        ));
    }

    public function updateAction() {
        return $this->redirectToRoute('shop_cart_material');
    }
    public function materialAction(Request $request)
    {
        $cart = $this->cartLoader->getCart($this->getUser());

        $cartLines = $cart->getCartLines();

        $pets = array();
        /** @var CartLine $cartLine */
        foreach ($cartLines as $cartLine) {
            if ($cartLine->isAnalyse()) {
                $profile = $cartLine->getParameterProfile();

                $sample = $cartLine->getSample();
                $pet = $sample ? $sample->getPet() : null;
                $petIndex = $pet ? $pet->getId() : "default";

                if (!isset($pets[$petIndex])) {
                    $pets[$petIndex] = array(
                        "pet" => $pet,
                        "profiles" => array(),
                        "sampleTypes" => array()
                    );
                }

                $pets[$petIndex]["profiles"][$profile->getId()] = array(
                    "cartLine" => $cartLine,
                    "sampleTypes" => array()
                );

                /** @var ParameterProfileSampleType $sampleType */
                foreach ($profile->getSampleTypes() as $sampleType) {
                    if ($sampleType->getDefinition()->ownerCanOrder()) {
                        $pets[$petIndex]["profiles"][$profile->getId()]['sampleTypes'][] = $sampleType;
                    }
                }

            }
        }
        // find sampletypes per pet
        foreach ($pets as $petIndex => $pet) {
            $sampleTypes = array();
            foreach ($pet["profiles"] as $profile) {
                /** @var ParameterProfileSampleType $sampleType */
                foreach ($profile["sampleTypes"] as $sampleType) {
                    $sampleTypeDefinition =  $sampleType->getDefinition();
                    if (!isset($sampleTypes[$sampleTypeDefinition->getId()])) {
                        $product = $this->productRepository->findOneBy(array(
                            "reference" => $sampleTypeDefinition->getReference()
                        ));
                        $sampleTypes[$sampleTypeDefinition->getId()] = array(
                            "sampleType" => $sampleType,
                            "product" => $product
                        );
                    }
                }
            }
            echo count($sampleTypes) . "<br>";
            $pets[$petIndex]["sampleTypes"] = $sampleTypes;
        }


        if ($request->isMethod("post")) {
            return $this->redirectToRoute('shop_cart_address');
        }


        return $this->render('ZoolyxShopBundle:Cart:material.html.twig', array(
            'cart' => $cart,
            'pets' => $pets
        ));
    }
    public function addressAction(Request $request)
    {
        $cart = $this->cartLoader->getCart($this->getUser());

        if ($request->isMethod("post")) {
            return $this->redirectToRoute('shop_cart_payment');
        }

        return $this->render('ZoolyxShopBundle:Cart:address.html.twig', array(
            'cart' => $cart,
            'addresses' => array()
        ));
    }
    public function paymentAction(Request $request) {
        $cart = $this->cartLoader->getCart($this->getUser());

        if ($request->isMethod("post")) {

            $lines = $cart->getCartLines();
            /** @var CartLine $line */
            foreach ($lines as $line) {
                echo "wil remove ".$line->getId()."<br>";
                /** @todo dit werkt nog niet */
                $this->entityManager->remove($line);
            }
            $this->entityManager->persist($cart);
            $this->entityManager->flush();
            return $this->redirectToRoute('shop_cart_confirmation');
        }

        return $this->render('ZoolyxShopBundle:Cart:payment.html.twig', array(
            'cart' => $cart
        ));
    }
    public function confirmationAction() {
        $cart = $this->cartLoader->getCart($this->getUser());
        return $this->render('ZoolyxShopBundle:Cart:confirmation.html.twig', array(
            'cart' => $cart
        ));
    }

    public function addProductAction(Request $request, $id) {
        /** @var Product $product */
        $product = $this->productRepository->find($id);

        $cart = $this->cartLoader->getCart($this->getUser());

        $cartLineInCart = null;
        /** @var CartLine $cartLine */
        foreach ($cart->getCartLines() as $cartLine) {
            if (!$cartLine->isAnalyse() && ($cartLine->getProduct()->getId() == $id)) {
                $cartLineInCart = $cartLine;
            }
        }

        if (is_null($cartLineInCart)) {
            $cartLine = new CartLine();
            $cartLine
                ->setCart($cart)
                ->setProduct($product)
                ->setItems(1)
                ->setPrice($product->getPrice());
            $this->entityManager->persist($cartLine);
        } else {
            $cartLineInCart->setItems($cartLineInCart->getItems()+1);
            $this->entityManager->persist($cartLineInCart);
        }
        $this->entityManager->flush();

        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
        //return $this->redirectToRoute('shop_cart');
    }
    public function addProfileAction()
    {
        $cart = $this->cartLoader->getCart($this->getUser());

        $requestData = $this->requestManager->getRequestData();
        $profiles = $requestData->getProfilesToPay();
        $requestSample = $requestData->getSample();
        /** @var Sample $sample */
        $sample = $this->sampleRepository->find($requestSample->getId());


        /** @var LimsParameterProfile $profile */
        foreach ($profiles as $profile) {
            /** @var ParameterProfile $parameterProfile */
            $parameterProfile = $this->parameterProfileRepository->find($profile->getEntity()->getId());
            $price = 100*floatval(str_replace(",",".",$profile->getPrice()->getValue()));

            $cartLine = new CartLine();
            $cartLine
                ->setCart($cart)
                ->setIsAnalyse(true)
                ->setParameterProfile($parameterProfile)
                ->setSample($sample)
                ->setItems(1)
                ->setPrice($price);
            $this->entityManager->persist($cartLine);
        }

        //add the requested sampleTypes

        //$sampleTypesRequested = $requestData->getSampleTypes();
        /**
         * @var string $reference
         * @var RequestSampleType $sampleType
         */
        /*
        foreach ($sampleTypesRequested as $reference => $sampleType)
        {
            $product = $this->productRepository->findOneBy(array('reference'=>$reference));
            $cartLine = new CartLine();
            $cartLine
                ->setCart($cart)
                ->setIsAnalyse(false)
                ->setProduct($product)
                ->setItems(1)
                ->setPrice($product->getPrice());
            $this->entityManager->persist($cartLine);
        }

        if (count($sampleTypesRequested)) {
            $product = $this->productRepository->findOneBy(array('reference'=>'DELIVERY'));
            $cartLine = new CartLine();
            $cartLine
                ->setCart($cart)
                ->setIsAnalyse(false)
                ->setProduct($product)
                ->setItems(1)
                ->setPrice($product->getPrice());
            $this->entityManager->persist($cartLine);
        }
        */

        $this->entityManager->flush();

        return $this->redirectToRoute('shop_cart');
    }

    public function removeCartLineAction($id) {
        $cartLine = $this->cartLineRepository->find($id);
        $this->entityManager->remove($cartLine);
        $this->entityManager->flush();

        return $this->redirectToRoute('shop_cart');
    }


    public function processAction() {
        /** @var User $user */
        $user = $this->getUser();
        /** @var Cart $cart */
        $cart = $user->getCart();

        $order = new Order();
        /** @var CartLine $cartLine */
        foreach ($cart->getCartLines() as $cartLine) {
            $orderLine = new OrderLine();
            $orderLine
                ->setItems($cartLine->getItems())
                ->setPrice($cartLine->getPrice())
                ->setProduct($cartLine->getProduct())
                ->setParameterProfile($cartLine->getParameterProfile())
                ->setOrder($order)
            ;
            $this->entityManager->persist($orderLine);
            $this->entityManager->remove($cartLine);
        }
        $order
            ->setUser($user)
            ->setPrice($cart->getPrice())
            ->setItems($cart->getItems())
            ;
        $this->entityManager->persist($order);

        $this->entityManager->flush();
        return $this->render('ZoolyxShopBundle:Cart:success.html.twig', array(
        ));
    }
}
