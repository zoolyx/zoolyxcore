<?php

namespace Zoolyx\CoreBundle\Forms\Request;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Practice;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;


/**
 * @DI\Service("zoolyx_core.builder.lims_id_drop_down")
 */
class LimsIdDropDownBuilder
{

    /**
     * @param $veterinaryPractices
     * @return array
     */
    public function build($veterinaryPractices) {
        //$practices = array("selecteer een practijk" => '');
        $practices = array();
        /** @var VeterinaryPractice $veterinaryPractice */
        foreach ($veterinaryPractices as $index => $veterinaryPractice) {
            if (!is_null($veterinaryPractice->getLimsId())) {
                /** @var Veterinary $veterinary */
                $veterinary = $veterinaryPractice->getVeterinary();
                /** @var Practice $practice */
                $practice = $veterinaryPractice->getPractice();

                $veterinaryName = $veterinary ? $veterinary->getFirstName()." ".$veterinary->getLastName(): "";
                $practiceName = $practice ? $practice->getName() : "";
                $name = $veterinaryName . " - " . $practiceName;
                if (isset($practices[$name])) {
                    $name = $veterinaryPractice->getLimsId();
                }
                $practices[$name] = $veterinaryPractice->getLimsId();
            }
        }
        return $practices;
    }

}