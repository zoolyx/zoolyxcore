<?php

namespace Zoolyx\CoreBundle\Forms\Request;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Zoolyx\CoreBundle\Model\Address\Validation\FormAddressValidator;

/**
 * @DI\Service("zoolyx_core.form.form_data")
 */
class FormData
{
    /** @var FormAddressValidator $formAddressValidator */
    private $formAddressValidator;

    /**
     * Constructor.
     *
     * @param FormAddressValidator $formAddressValidator
     *
     * @DI\InjectParams({
     *  "formAddressValidator" = @DI\Inject("zoolyx_core.address.form_validator")
     * })
     */
    public function __construct(FormAddressValidator $formAddressValidator ) {
        $this->formAddressValidator = $formAddressValidator;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        //leeftijd van dier mag niet in toekomst liggen
        if ($this->getBirthDate() > new \DateTime()) {
            $context->buildViolation('request.pe_birth_date.future')
                ->atPath('birthDate')
                ->addViolation();
        }

        //minstens telefoon OF emailadres van eigenaar        
        if ( $this->getEmail() == "" && $this->getTelephone() == "" ) {
            $context->buildViolation('request.ad.tel_or_email')
                ->atPath('email')
                ->addViolation();
        }

        if($this->getTelephone() != "") {
            $telephone = $this->getTelephone();
            $telephone = str_replace("+","",$telephone);
            $telephone = str_replace(".","",$telephone);
            $telephone = str_replace("/","",$telephone);
            $telephone = str_replace(" ","",$telephone);
            //echo "$telephone"."<br>";
            preg_match_all("/^\d{9,13}$/",$telephone, $matches);
            if (count($matches[0])==0) {
                $context->buildViolation('request.ad.tel_invalid')
                    ->atPath('telephone')
                    ->addViolation();
            }
        }

        //diersoort moet aangegeven worden        
        if ( $this->getSpeciesCode() === 0 || $this->getSpeciesCode() == "" ) {
            $context->buildViolation('request.pe_species.missing')
                ->atPath('speciesCode')
                ->addViolation();
        }

        //chipnr moet uit 15 cijfers bestaan        
        if ( $this->getChipNbr() != "" && !preg_match_all('/^\d{15}$/m', $this->getChipNbr(), $matches))
        {
            $context->buildViolation('request.pe_chipid.invalid')
                ->atPath('chipNbr')
                ->addViolation();
        }

        //controleer of het adres geldig is
        if ($this->getCountry() == "BE") {
            $this->formAddressValidator->validate(
                $this->getStreet(),
                $this->getZipCode(),
                $this->getCity(),
                $context,
                array(
                    FormAddressValidator::FORM_NAME_STREET => "street",
                    FormAddressValidator::FORM_NAME_ZIPCODE => "zipCode",
                    FormAddressValidator::FORM_NAME_CITY => "city",
                )
            );
            if ($this->formAddressValidator->getStreet()) {
                $this->setStreet($this->formAddressValidator->getStreet());
            }
            if ($this->formAddressValidator->getZipCode()) {
                $this->setZipCode($this->formAddressValidator->getZipCode());
            }
            if ($this->formAddressValidator->getCity()) {
                $this->setCity($this->formAddressValidator->getCity());
            }
        }
    }

    /** @var string */
    private $limsId;
    /** @var string */
    private $practiceReference;
    /** @var string */
    private $firstName;
    /** @var string */
    private $lastName;
    /** @var string */
    private $street;
    /** @var string */
    private $zipCode;
    /** @var string */
    private $city;
    /** @var string */
    private $country;
    /** @var string */
    private $telephone;
    /**
     * @Assert\Email(message = "userRegistration.email.invalid")
     * @var string
     */
    private $email;
    /** @var string */
    private $language;

    /** @var string */
    private $name;
    /** @var string */
    private $chipNbr;
    /** @var string */
    private $speciesCode;
    /** @var string */
    private $breedCode = '';
    /** @var string */
    private $gender;
    /** @var \DateTime */
    private $birthDate;

    /** @var int */
    public $invoiceFor = 1;
    /** @var string */
    public $faId = '';
    /** @var string */
    public $vatNbr = '';

    /**
     * @return string
     */
    public function getLimsId()
    {
        return $this->limsId;
    }

    /**
     * @param string $limsId
     * @return $this
     */
    public function setLimsId($limsId)
    {
        $this->limsId = $limsId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPracticeReference()
    {
        return $this->practiceReference;
    }

    /**
     * @param string $practiceReference
     * @return $this
     */
    public function setPracticeReference($practiceReference)
    {
        $this->practiceReference = $practiceReference;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return $this
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     * @return $this
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     * @return $this
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getChipNbr()
    {
        return $this->chipNbr;
    }

    /**
     * @param string $chipNbr
     * @return $this
     */
    public function setChipNbr($chipNbr)
    {
        $this->chipNbr = $chipNbr;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpeciesCode()
    {
        return $this->speciesCode;
    }

    /**
     * @param string $speciesCode
     * @return $this
     */
    public function setSpeciesCode($speciesCode)
    {
        $this->speciesCode = $speciesCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getBreedCode()
    {
        return $this->breedCode;
    }

    /**
     * @param string $breedCode
     * @return $this
     */
    public function setBreedCode($breedCode)
    {
        $this->breedCode = $breedCode;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTime $birthDate
     * @return $this
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getFaId()
    {
        return $this->faId;
    }

    /**
     * @param string $faId
     * @return $this
     */
    public function setFaId($faId)
    {
        $this->faId = $faId;
        return $this;
    }

    /**
     * @return string
     */
    public function getVatNbr()
    {
        return $this->vatNbr;
    }

    /**
     * @param string $vatNbr
     * @return $this
     */
    public function setVatNbr($vatNbr)
    {
        $this->vatNbr = $vatNbr;
        return $this;
    }

    /**
     * @return int
     */
    public function getInvoiceFor()
    {
        return $this->invoiceFor;
    }

    /**
     * @param int $invoiceFor
     * @return $this
     */
    public function setInvoiceFor($invoiceFor)
    {
        $this->invoiceFor = $invoiceFor;
        return $this;
    }

}
