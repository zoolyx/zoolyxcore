<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 9/7/14
 * Time: 4:34 PM
 */

namespace Zoolyx\CoreBundle\Forms\Request;


use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Zoolyx\CoreBundle\Entity\Breed;
use Zoolyx\CoreBundle\Entity\Species;
use Zoolyx\CoreBundle\Model\Request\RequestData;


class FormDataType extends AbstractType
{
    /** @var RequestData */
    private $requestData;
    /** @var Translator */
    private $translator;
    /** @var array */
    private $species = array();
    /** @var array */
    private $breeds = array();
    /** @var array */
    private $candidateVeterinaryPractices = array();

    public function __construct(RequestData $requestData, $translator, $species, $breeds)
    {
        $this->requestData = $requestData;

        $this->translator = $translator;
        /** @var Species $specimen */
        
        foreach ($species as $specimen) {
            $this->species[$specimen->getDescription($this->translator->getLocale())->getDescription()] = $specimen->getSpeciesId();
        }
        ksort($this->species);
        $this->species = array($this->translator->trans('request.pe_species.placeholder') => 0) + $this->species;

        $tempBreeds = array();
        /** @var Breed $breed */
        foreach ($breeds as $breed) {
            $breedIdParts = explode("_",$breed->getBreedId());
            $breedId = count($breedIdParts)==2 ? $breedIdParts[1] : null;
            if (!is_null($breedId)) {
                $tempBreeds[$breed->getDescription($this->translator->getLocale())] = $breedId;
            }
        }
        ksort($tempBreeds);

        $originalBreedName = $requestData->getSample()->getPet()->getBreedCode()!="" ? "" : $requestData->getSample()->getPet()->getOriginalBreedName();
        if ($originalBreedName) {
            $this->breeds[$originalBreedName] = "";
        } else {
            $this->breeds[$this->translator->trans('request.pe_breed.placeholder')] = "";
        }
        foreach ($tempBreeds as $index=>$value) {
            $this->breeds[$index] = $value;
        }

        $this->candidateVeterinaryPractices = $requestData->getCandidateVeterinaryPractices();
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $currentYear = date("Y");
        $years = array();
        for ($i = $currentYear; $i>=$currentYear-60; $i--) {
            $years[] = $i;
        }

        $builder
            ->add('firstName', 'text', array(
                'label' => $this->translator->trans('request.ad_first_name'),
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('request.ad_first_name'),
                )
            ))
            ->add('lastName', 'text', array(
                'label' => $this->translator->trans('request.ad_last_name'),
                'attr' => array(
                    'class' => 'form-control',
                    'maxlength' => '30',
                    'placeholder' => $this->translator->trans('request.ad_last_name'),
                )
            ))
            ->add('street', 'text', array(
                'label' => $this->translator->trans('request.ad_street'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('request.ad_street'),
                )
            ))
            ->add('zipCode', 'text', array(
                'label' => $this->translator->trans('request.ad_zip_code'),
                'attr' => array(
                    'class' => 'form-control',
                    'pattern' => '\d{4}',
                    'placeholder' => $this->translator->trans('request.ad_zip_code'),
                )
            ))
            ->add('city', 'text', array(
                'label' => $this->translator->trans('request.ad_city'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('request.ad_city'),
                )
            ))
            ->add('country', 'choice', array(
                'label' => $this->translator->trans('request.ad_country'),
                'choices' => array(
                    $this->translator->trans('country.be') => 'BE',
                    $this->translator->trans('country.lu') => 'LU',
                    $this->translator->trans('country.nl') => 'NL',
                    $this->translator->trans('country.fr') => 'FR'
                ),
                'choices_as_values' => true,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('request.ad_country'),
                )
            ))
            ->add('telephone', 'text', array(
                'label' => $this->translator->trans('request.ad_tel'),
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('request.ad_tel'),
                )
            ))
            ->add('email', 'email', array(
                'label' => $this->translator->trans('request.ad_email'),
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('request.ad_email'),
                )
            ))
            ->add('language', 'choice', array(
                'label' => $this->translator->trans('request.ad_lang'),
                'choices' => array('Nederlands'=> 'nl', 'Français' => 'fr'),
                'choices_as_values' => true,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('request.ad_lang'),
                )
            ))

            ->add('name', 'text', array(
                'label' => $this->translator->trans('request.pe_name'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('request.pe_name'),
                )
            ))
            ->add('chipNbr', 'text', array(
                'label' => $this->translator->trans('request.pe_chip_id'),
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('request.pe_chip_id'),
                )
            ))
            ->add('speciesCode', 'choice', array(
                'label' => $this->translator->trans('request.pe_species'),
                'choices' => $this->species,
                'choices_as_values' => true,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('request.pe_species'),
                )
            ))

            ->add('breedCode', 'choice', array(
                'required' => false,
                'label' => $this->translator->trans('request.pe_breed'),
                'choices' => count($this->breeds) ? $this->breeds : array($this->translator->trans('request.pe_breed.placeholder') => 0),
                'choices_as_values' => true,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('request.pe_breed'),
                )
            ))

            ->add('gender', 'choice', array(
                'label' => $this->translator->trans('request.pe_gender'),
                'choices' => array('gender.male'=> 'M', 'gender.male_neutered'=> 'MN', 'gender.female_neutered' => 'FN', 'gender.female' => 'F', 'gender.unknown' => ''),
                'choices_as_values' => true,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('request.pe_gender'),
                )
            ))
            ->add('birthDate', 'date', array(
                'label' => $this->translator->trans('request.pe_birth_date'),
                'required' => false,
                'widget' => 'choice',
                'format' => 'yyyy-MM-dd',
                'years' => $years,
                'placeholder' => [
                    'year' => 'date.year', 'month' => 'date.month', 'day' => 'date.day'
                ],
            ))
            ->add('invoiceFor', 'choice', array(
                'label' => $this->translator->trans('request.invoice'),
                'choices' => array( 
                	 $this->translator->trans('request.da') =>1, 
                	 $this->translator->trans('request.ad') =>2
                ),
                'choices_as_values' => true,
                'expanded' => true,
                'multiple' => false,
                'attr' => array(
                    //'class' => 'form-control'
                )
            ))
            ->add('faId', 'hidden', array(
            ))
            ->add('vatNbr', 'text', array(
                'label' => $this->translator->trans('request.vat_nbr'),
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('request.vat_nbr'),
                )
            ))

        ;
        if ($this->requestData->editPracticeReference()) {
            $builder->add('practiceReference', 'text', array(
                'label' => $this->translator->trans('request.practice_ref'),
                'required' => false,
                'attr' => array(
                    'maxlength' => '30',
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('request.practice_ref'),
                )
            ));
        }
        if (count($this->candidateVeterinaryPractices)>0 || $this->requestData->isAdministrator()) {
            $builder
                ->add('limsId', 'choice', array(
                    'label' => $this->translator->trans('request.select_practice'),
                    'required' => true,
                    'choices' => $this->candidateVeterinaryPractices,
                    'choices_as_values' => true,
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => $this->translator->trans('request.select_practice'),
                    )
                ));
        }

    }

    /**
     * this gets called in the final stage before rendering the form
     *
     * @return void
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $fieldName = "street";
        /** @var FormErrorIterator $errorIterator */
        $errorIterator = $view->children[$fieldName]->vars["errors"];
        if ($errorIterator->count() > 0) {
            $view->children[$fieldName]->vars["value"] = $form->getData()->getStreet();
        }

        $fieldName = "zipCode";
        /** @var FormErrorIterator $errorIterator */
        $errorIterator = $view->children[$fieldName]->vars["errors"];
        if ($errorIterator->count() > 0) {
            $view->children[$fieldName]->vars["value"] = $form->getData()->getZipCode();
        }

        $fieldName = "city";
        /** @var FormErrorIterator $errorIterator */
        $errorIterator = $view->children[$fieldName]->vars["errors"];
        if ($errorIterator->count() > 0) {
            $view->children[$fieldName]->vars["value"] = $form->getData()->getCity();
        }

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zoolyx\CoreBundle\Forms\Request\FormData',
        ));
    }

    public function getName()
    {
        return 'FormData';
    }
}
