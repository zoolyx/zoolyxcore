<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 9/7/14
 * Time: 4:34 PM
 */

namespace Zoolyx\CoreBundle\Forms\Request;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Zoolyx\CoreBundle\Entity\Repository\BreedRepository;
use Zoolyx\CoreBundle\Entity\Repository\SpeciesRepository;
use Zoolyx\CoreBundle\Model\Request\RequestData;


/**
 * @DI\Service("zoolyx_core.provider.form_data_type")
 */
class FormDataTypeProvider
{
    /** @var SpeciesRepository */
    private $speciesRepository;
    /** @var BreedRepository */
    private $breedRepository;
    /** @var Translator */
    private $translator;

    /**
     * Constructor.
     *
     * @param SpeciesRepository $speciesRepository
     * @param BreedRepository $breedRepository
     * @param Translator $translator
     *
     * @DI\InjectParams({
     *  "speciesRepository" = @DI\Inject("zoolyx_core.repository.species"),
     *  "breedRepository" = @DI\Inject("zoolyx_core.repository.breed"),
     *  "translator" = @DI\Inject("translator.default")
     * })
     */
    public function __construct(SpeciesRepository $speciesRepository, BreedRepository $breedRepository, Translator $translator) {
        $this->speciesRepository = $speciesRepository;
        $this->breedRepository = $breedRepository;
        $this->translator = $translator;
    }

    /**
     * @param RequestData $requestData
     * @param string $speciesCode
     * @return FormDataType
     */
    public function provide($requestData, $speciesCode = null) {
        $species = $this->speciesRepository->findAll();
        $breeds = $this->breedRepository->findForSpeciesCode($speciesCode ? $speciesCode : $requestData->getSample()->getPet()->getSpeciesCode());
        return new FormDataType($requestData, $this->translator, $species, $breeds);

    }

}
