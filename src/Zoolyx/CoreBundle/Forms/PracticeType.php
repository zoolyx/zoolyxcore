<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 9/7/14
 * Time: 4:34 PM
 */

namespace Zoolyx\CoreBundle\Forms;


use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class PracticeType extends AbstractType
{
    /** @var Translator */
    private $translator;

    public function __construct($translator)
    {
        $this->translator = $translator;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => $this->translator->trans('form.practice.name'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.practice.name.placeholder'),
                )
            ))
            ->add('description', 'text', array(
                'label' => $this->translator->trans('form.practice.description'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.practice.description.placeholder'),
                )
            ))
            ->add('street', 'text', array(
                'label' => $this->translator->trans('form.practice.street'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.practice.street.placeholder'),
                )
            ))
            ->add('zipCode', 'text', array(
                'label' => $this->translator->trans('form.practice.zipcode'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.practice.zipcode.placeholder'),
                )
            ))
            ->add('city', 'text', array(
                'label' => $this->translator->trans('form.practice.city'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.practice.city.placeholder'),
                )
            ))

        ;


    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zoolyx\AccountBundle\Forms\Registration',
        ));
    }

    public function getName()
    {
        return 'Registration';
    }
}
