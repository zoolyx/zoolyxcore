<?php

namespace Zoolyx\CoreBundle;

final class Events
{
    /**
     * This event is dispatched each time a report is imported.
     *
     * @see Zoolyx\CoreBundle\Event\ReportEvent
     *
     * @var string
     */
    const REPORT_IMPORTED = 'report.imported';
    /**
     * This event is dispatched each time a report for a specific user is imported.
     *
     * @see Zoolyx\CoreBundle\Event\ReportUserEvent
     *
     * @var string
     */
    const REPORT_IMPORTED_FOR_USER = 'report.imported_for_veterinary';

    /**
     * This event is dispatched each time a new ticket is created.
     *
     * @see Zoolyx\TicketBundle\Event\NewTicketEvent
     *
     * @var string
     */
    const NEW_TICKET = 'ticket.new';

    /**
     * This event is dispatched each time a new ticket is created because a mail was received.
     *
     * @see Zoolyx\TicketBundle\Event\NewMailTicketEvent
     *
     * @var string
     */
    const NEW_MAIL_TICKET = 'mail_ticket.new';

    /**
     * This event is dispatched each time a ticket has been updated.
     *
     * @see Zoolyx\TicketBundle\Event\UpdatedTicketEvent
     *
     * @var string
     */
    const UPDATED_TICKET = 'ticket.updated';

    /**
     * This event is dispatched each time a ticket is assigned to an administrator or an administrator group.
     *
     * @see Zoolyx\TicketBundle\Event\AssignedTicketEvent
     *
     * @var string
     */
    const ASSIGNED_TICKET = 'ticket.assigned';

    /**
     * This event is dispatched each time a new account is created while a report is being imported.
     *
     * @see Zoolyx\TicketBundle\Event\NewAccountEvent
     *
     * @var string
     */
    const NEW_ACCOUNT = 'account.new';

}
