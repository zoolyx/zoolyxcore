<?php
namespace Zoolyx\CoreBundle\EventListener;


use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Zoolyx\CoreBundle\Entity\User;

/**
 * Stores the locale of the user in the session after the
 * login. This can be used by the LocaleListener afterwards.
 */
class UserLocaleListener
{
    /**
     * @var Session
     */
    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onInteractiveLogin(InteractiveLoginEvent $event)
    {
        /** @var User $user */
        $user = $event->getAuthenticationToken()->getUser();
        //die('interactive');
        $this->session->set('wordpress',array(
            'username' => $user->getUsername(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName()
        ));
        if (null !== $user->getLanguage() && '' !== $user->getLanguage()) {
            $this->session->set('_locale', $user->getLanguage());
        } else {
            $this->session->set('_locale', 'nl');
        }
    }
}