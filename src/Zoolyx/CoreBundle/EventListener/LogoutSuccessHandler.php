<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 04.07.17
 * Time: 05:27
 */

namespace Zoolyx\CoreBundle\EventListener;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * @DI\Service("zoolyx.logout_success_handler")
 */
class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{

    protected $router;
    protected $security;

    /**
     * Constructor.
     *
     * @param Router $router
     * @param SecurityContext $security
     * @param Session $session
     *
     * @DI\InjectParams({
     *  "router" = @DI\Inject("router"),
     *  "security" = @DI\Inject("security.context"),
     *  "session" = @DI\Inject("session")
     * })
     */
    public function __construct(Router $router, SecurityContext $security, Session $session)
    {
        $this->router = $router;
        $this->security = $security;
        $this->session = $session;
    }

    public function onLogoutSuccess(Request $request)
    {
        $language = $this->session->get('_locale');

        if ($language == "") {
            $response = new RedirectResponse($this->router->generate('zoolyx_logged_out', array()));
        } else {
            $response = new RedirectResponse($this->router->generate('zoolyx_logged_out_language', array(
                'language' => $this->session->get('_locale')
            )));
        }

        return $response;
    }

} 