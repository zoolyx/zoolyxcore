<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 04.07.17
 * Time: 05:27
 */

namespace Zoolyx\CoreBundle\EventListener;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Zoolyx\CoreBundle\Entity\User;


/**
 * @DI\Service("zoolyx.login_success_handler")
 */
class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{

    protected $router;
    protected $security;

    /**
     * Constructor.
     *
     * @param Router $router
     * @param SecurityContext $security
     *
     * @DI\InjectParams({
     *  "router" = @DI\Inject("router"),
     *  "security" = @DI\Inject("security.context")
     * })
     */
    public function __construct(Router $router, SecurityContext $security)
    {
        $this->router = $router;
        $this->security = $security;
    }


    public function onAuthenticationSuccess(Request $request, TokenInterface $token) {
        /** @var User $user */
        $user = $this->security->getToken()->getUser();

        if ($request->getSession()->get('_security.main.target_path') != '') {
            return new RedirectResponse($request->getSession()->get('_security.main.target_path'));
        }

        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            return new RedirectResponse($this->router->generate('admin_users'));
        }
        if ($user->hasRole('ROLE_VETERINARY')) {
            return new RedirectResponse($this->router->generate('veterinary_report_index'));
        }
        if ($user->hasRole('ROLE_OWNER')) {
            return new RedirectResponse($this->router->generate('owner_report_index'));
        }
        if ($user->hasRole('ROLE_ORGANISATION')) {
            return new RedirectResponse($this->router->generate('veterinary_report_index'));
        }
        if ($user->hasRole('ROLE_ADMINISTRATOR')) {
            return new RedirectResponse($this->router->generate('zoolyx_ticket_homepage'));
        }
        return new RedirectResponse($this->router->generate('zoolyx_account_settings'));
    }


} 