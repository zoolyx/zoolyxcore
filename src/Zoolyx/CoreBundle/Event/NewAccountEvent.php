<?php

namespace Zoolyx\CoreBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Zoolyx\CoreBundle\Entity\User;

class NewAccountEvent extends Event
{
    const TRIGGER_DEFAULT = 0;
    const TRIGGER_USER_REGISTRATION = 1;
    const TRIGGER_IMPORT_REPORT = 2;
    const TRIGGER_REPORT_SHARED = 3;
    const TRIGGER_MAIL_RECEIVED = 4;

    /**
     * @var User
     */
    private $user;

    /**
     * @var int
     */
    private $trigger;

    /**
     * Constructor.
     *
     * @param User $user
     * @param int $trigger
     */
    public function __construct(User $user, $trigger = self::TRIGGER_DEFAULT)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getTrigger()
    {
        return $this->trigger;
    }
}
