<?php

namespace Zoolyx\CoreBundle\Event;

use Zoolyx\CoreBundle\Entity\Report;
use Symfony\Component\EventDispatcher\Event;
use Zoolyx\CoreBundle\Entity\User;

class ReportUserEvent extends Event
{
    /** @var Report */
    private $report;

    /** @var User */
    private $user;

    /**
     * Constructor.
     *
     * @param Report $report
     * @param User $user
     */
    public function __construct(Report $report, User $user)
    {
        $this->report = $report;
        $this->user = $user;
    }

    /**
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @return User
     */
    public function getUser() {
        return $this->user;
    }
}
