<?php

namespace Zoolyx\CoreBundle\Event;

use Zoolyx\CoreBundle\Entity\Report;
use Symfony\Component\EventDispatcher\Event;

class ReportEvent extends Event
{
    /**
     * @var Report
     */
    private $report;

    /**
     * Constructor.
     *
     * @param Report $report
     */
    public function __construct(Report $report)
    {
        $this->report = $report;
    }

    /**
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }
}
