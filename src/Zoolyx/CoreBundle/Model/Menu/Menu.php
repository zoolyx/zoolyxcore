<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Menu;

class Menu {

    /** @var array  */
    private $items = array();

    /** @var MenuItem */
    private $defaultItem;

    /**
     * @return array
     */
    public function getItems() {
        return $this->items;
    }

    /**
     * @param MenuItem $item
     * @return $this
     */
    public function addItem(MenuItem $item) {
        $this->items[] = $item;
        return $this;
    }

    /**
     * @return MenuItem
     */
    public function getDefaultItem()
    {
        return $this->defaultItem;
    }

    /**
     * @param MenuItem $defaultItem
     * @return $this
     */
    public function setDefaultItem($defaultItem)
    {
        $this->defaultItem = $defaultItem;
        return $this;
    }


} 