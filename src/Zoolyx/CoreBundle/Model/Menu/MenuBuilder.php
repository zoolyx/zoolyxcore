<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Menu;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker as Security;
use Zoolyx\CoreBundle\Entity\User;


/**
 * @DI\Service("zoolyx_core.menu.builder")
 */
class MenuBuilder {

    const ACTION_LOGIN = 0;
    const ACTION_LOGOUT = 1;
    const ACTION_SETTINGS = 2;
    const ACTION_MESSAGES = 3;
    const ACTION_PICKUP = 4;
    const ACTION_REQUEST = 5;
    const ACTION_VETERINARY_REPORTS = 6;
    const ACTION_OWNER_REPORTS = 7;
    const ACTION_USER_TICKETS = 8;
    const ACTION_ADMIN_USERS = 10;
    const ACTION_ADMIN_REPORTS = 11;
    const ACTION_ADMIN_CONTRACTORS = 12;
    const ACTION_ADMIN_NOTIFICATIONS = 13;
    const ACTION_ADMIN_TICKETS = 14;
    const ACTION_ADMIN_GEOOP = 15;
    const ACTION_ADMIN_IMPERSONATION = 16;
    const ACTION_CONTRACTOR_REPORTS = 20;

    /** @var Translator */
    private $translator;

    /** @var Router */
    private $router;

    /** @var Security */
    private $security;

    /** @var array  */
    private $items = array();

    /**
     * Constructor.
     *
     * @param Translator $translator
     * @param Router $router
     * @param Security $security
     *
     * @DI\InjectParams({
     *  "translator" = @DI\Inject("translator"),
     *  "router" = @DI\Inject("router"),
     *  "security" = @DI\Inject("security.authorization_checker")
     * })
     */
    public function __construct($translator, $router, $security) {
        $this->translator = $translator;
        $this->router = $router;
        $this->security = $security;
    }

    public function getMenu(User $user = null, $language = 'nl')
    {
        $this->translator->setLocale($language);

        $this->items = array(
            self::ACTION_LOGIN => new MenuItem(
                $this->translator->trans('menu.inloggen'),
                $this->router->generate('fos_user_security_login')
            ),
            self::ACTION_LOGOUT => new MenuItem(
                $this->translator->trans('menu.uitloggen'),
                $this->router->generate('fos_user_security_logout')
            ),
            self::ACTION_SETTINGS => new MenuItem(
                $user ? ($name = $user->getFirstName().' '.$user->getLastName()) == ' ' ? $user->getUserName() : $name : '',
                $this->router->generate('zoolyx_account_settings')
            ),
            self::ACTION_MESSAGES => new MenuItem(
                $this->translator->trans('menu.berichten'),
                $this->router->generate('veterinary_messages')
            ),
            self::ACTION_PICKUP => new MenuItem(
                $this->translator->trans('menu.ophaling'),
                $this->router->generate('veterinary_pickup')
            ),
            self::ACTION_REQUEST => new MenuItem(
                $this->translator->trans('menu.aanvraag'),
                $this->router->generate('request_new')
            ),
            self::ACTION_VETERINARY_REPORTS => new MenuItem(
                $this->translator->trans('menu.resultaten'),
                $this->router->generate('veterinary_report_index')
            ),
            self::ACTION_OWNER_REPORTS => new MenuItem(
                $this->translator->trans('menu.myanimals'),
                $this->router->generate('owner_report_index')
            ),
            self::ACTION_USER_TICKETS => new MenuItem(
                $this->translator->trans('menu.tickets'),
                $this->router->generate('zoolyx_user_tickets')
            ),
            self::ACTION_ADMIN_USERS => new MenuItem(
                $this->translator->trans('menu.users'),
                $this->router->generate('admin_users')
            ),
            self::ACTION_ADMIN_REPORTS => new MenuItem(
                $this->translator->trans('menu.reports'),
                $this->router->generate('admin_reports')
            ),
            self::ACTION_ADMIN_CONTRACTORS => new MenuItem(
                $this->translator->trans('menu.contractors'),
                $this->router->generate('admin_contractors')
            ),
            self::ACTION_ADMIN_NOTIFICATIONS => new MenuItem(
                $this->translator->trans('menu.notifications'),
                $this->router->generate('admin_notifications')
            ),
            self::ACTION_ADMIN_TICKETS => new MenuItem(
                $this->translator->trans('menu.tickets'),
                $this->router->generate('zoolyx_ticket_homepage')
            ),
            self::ACTION_ADMIN_GEOOP => new MenuItem(
                $this->translator->trans('menu.geoop'),
                $this->router->generate('admin_geoop_log')
            ),
            self::ACTION_ADMIN_IMPERSONATION => new MenuItem(
                $this->translator->trans('menu.exit_impersonation'),
                $this->router->generate('admin_reports', array('_switch_user'=> '_exit'))
            ),
            self::ACTION_CONTRACTOR_REPORTS => new MenuItem(
                $this->translator->trans('menu.reports'),
                $this->router->generate('contractor_report_index')
            ),
        );

        $menu = new Menu();

        if (!$user) {
            $menu->addItem($this->items[self::ACTION_LOGIN]);
        }
        if ($user) {
            $menu->addItem($this->items[self::ACTION_SETTINGS]);
        }
        if ($user && $user->hasRole('ROLE_VETERINARY')) {
//            $menu->addItem($this->items[self::ACTION_MESSAGES]);
        }
        if ($user && $user->hasRole('ROLE_VETERINARY')) {
            $menu->addItem($this->items[self::ACTION_PICKUP]);
        }
        if ($user && ($user->hasRole('ROLE_VETERINARY') /*|| $user->hasRole('ROLE_OWNER')*/ || $user->hasRole('ROLE_ADMINISTRATOR'))) {
            $menu->addItem($this->items[self::ACTION_REQUEST]);
        }
        if ($user && ($user->hasRole('ROLE_VETERINARY') || $user->hasRole('ROLE_ORGANISATION'))) {
            $menu->addItem($this->items[self::ACTION_VETERINARY_REPORTS]);
        }
        if ($user && $user->hasRole('ROLE_OWNER')) {
            $menu->addItem($this->items[self::ACTION_OWNER_REPORTS]);
        }
        if ($user && $user->hasRole('ROLE_CONTRACTOR')) {
            $menu->addItem($this->items[self::ACTION_CONTRACTOR_REPORTS]);
        }
        if ($user && $user->hasRole('ROLE_USER') && !($user->hasRole('ROLE_SUPER_ADMIN') || $user->hasRole('ROLE_ADMINISTRATOR'))) {
            $menu->addItem($this->items[self::ACTION_USER_TICKETS]);
        }

        if ($user && ($user->hasRole('ROLE_SUPER_ADMIN') || $user->hasRole('ROLE_ADMINISTRATOR'))) {
            $menu->addItem($this->items[self::ACTION_ADMIN_USERS]);
        }
        if ($user && ($user->hasRole('ROLE_SUPER_ADMIN') || $user->hasRole('ROLE_ADMINISTRATOR'))) {
            $menu->addItem($this->items[self::ACTION_ADMIN_REPORTS]);
        }
        if ($user && ($user->hasRole('ROLE_SUPER_ADMIN') || $user->hasRole('ROLE_ADMINISTRATOR'))) {
            $menu->addItem($this->items[self::ACTION_ADMIN_TICKETS]);
        }
        if ($user && $user->hasRole('ROLE_SUPER_ADMIN')) {
            $menu->addItem($this->items[self::ACTION_ADMIN_CONTRACTORS]);
        }
        if ($user && $user->hasRole('ROLE_SUPER_ADMIN')) {
            $menu->addItem($this->items[self::ACTION_ADMIN_NOTIFICATIONS]);
        }
        if ($user && ($user->hasRole('ROLE_SUPER_ADMIN') || $user->hasRole('ROLE_ADMINISTRATOR'))) {
            $menu->addItem($this->items[self::ACTION_ADMIN_GEOOP]);
        }
        if ($user) {
            if ($this->security->isGranted('ROLE_PREVIOUS_ADMIN')) {
                $menu->addItem($this->items[self::ACTION_ADMIN_IMPERSONATION]);
            } else {
                $menu->addItem($this->items[self::ACTION_LOGOUT]);
            }
        }

        //DEFAULT
        if (!$user) {
            $menu->setDefaultItem($this->items[self::ACTION_LOGIN]);
        }
        if ($user) {
            $menu->setDefaultItem($this->items[self::ACTION_SETTINGS]);
        }
        if ($user && $user->hasRole('ROLE_VETERINARY')) {
            $menu->setDefaultItem($this->items[self::ACTION_VETERINARY_REPORTS]);
        }
        if ($user && $user->hasRole('ROLE_ORGANISATION')) {
            $menu->setDefaultItem($this->items[self::ACTION_VETERINARY_REPORTS]);
        }
        if ($user && $user->hasRole('ROLE_OWNER')) {
            $menu->setDefaultItem($this->items[self::ACTION_OWNER_REPORTS]);
        }
        if ($user && $user->hasRole('ROLE_SUPER_ADMIN')) {
            $menu->setDefaultItem($this->items[self::ACTION_ADMIN_REPORTS]);
        }
        if ($user && $user->hasRole('ROLE_ADMINISTRATOR')) {
            $menu->setDefaultItem($this->items[self::ACTION_ADMIN_TICKETS]);
        }
        //var_dump($menu); die();
        return $menu;
    }

} 