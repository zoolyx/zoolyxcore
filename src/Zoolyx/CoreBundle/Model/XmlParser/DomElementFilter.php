<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser;

use DOMElement;
use DOMNode;
use JMS\DiExtraBundle\Annotation as DI;



/**
 * @DI\Service("zoolyx_core.dom_element.filter")
 */
class DomElementFilter {

    /**
     * @param DomNode $domElement
     * @param string $needle
     * @return DomElement
     */
    public function findOne($domElement, $needle) {
        $childNodes = $domElement->childNodes;
        foreach ($childNodes as $childNode) {
            if ($childNode->nodeName == $needle) {
                return $childNode;
            }
        }
        return null;
    }

    /**
     * @param DomNode $domElement
     * @param string $needle
     * @return array
     */
    public function findAll($domElement, $needle) {
        $nodes = array();
        $childNodes = $domElement->childNodes;
        foreach ($childNodes as $childNode) {
            if ($childNode->nodeName == $needle) {
                $nodes[] = $childNode;
            }
        }
        return $nodes;
    }

}