<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 06.03.16
 * Time: 12:40
 */

namespace Zoolyx\CoreBundle\Model\XmlParser;

use JMS\DiExtraBundle\Annotation as DI;
use SimpleXMLElement;

abstract class XmlParser extends BaseParser{

    private $attributes = array();
    private $tags = array();


    public function reset() {
        $this->attributes = array();
        $this->tags = array();
    }

    public function getAttributes() {
        return $this->attributes;
    }
    public function addAttribute($attribute) {
        $this->attributes[] = $attribute;
    }

    public function getTags() {
       return $this->tags;
    }
    public function addTag($tag) {
        $this->tags[] = $tag;
    }

    public function parse(SimpleXMLElement $xml)
    {
        $object = $this->getObject();
        $this->messages = array();

        /** @var SimpleXmlElement $attributeValue */
        foreach ($xml->attributes() as $attributeName => $attributeValue) {
            /** @var Parser $parser */
            foreach ($this->getAttributes() as $parser) {
                if ($parser->canParse(strtoupper($attributeName)) && $parser->canBeUsedOn($object)) {
                    $parser->process($object, $attributeValue);
                    $this->addMessages($parser->getParser()->getMessages());
                    $this->addMessage(new ParserMessage('success', $this->getObjectType(), 'added attribute ' . $parser->getKey() . ' = '.$attributeValue->__toString()));
                }
            }
        }

        foreach ($this->getAttributes() as $parser) {
            if ($parser->requiredIsMissing()) {
                $this->addMessage(new ParserMessage('critical', $this->getObjectType(), 'attribute ' . $parser->getKey() . ' is not present'));
            }
            if ($parser->optionalIsMissing()) {
                $this->addMessage(new ParserMessage('warning', $this->getObjectType(), 'attribute ' . $parser->getKey() . ' is not present'));
            }
        }

        /** @var SimpleXmlElement $child */
        foreach ($xml->children() as $child) {
            $tagName = strtoupper($child->getName());
            /** @var Parser $parser */
            $isParsed = false;
            foreach ($this->getTags() as $parser) {
                if ($parser->canParse(strtoupper($tagName)) && $parser->canBeUsedOn($object)) {
                    $parser->process($object, $child);
                    $this->addMessages($parser->getParser()->getMessages());
                    $this->addMessage(new ParserMessage('success', $this->getObjectType(),'added tag ' . $parser->getKey() . ' = '.$child->__toString()));
                    $isParsed = true;
                }
            }
            if (!$isParsed) {
                $this->addMessage(new ParserMessage('not found', $this->getObjectType(), 'could not parse tag ' . $tagName));
            }
        }

        foreach ($this->getTags() as $parser) {
            if ($parser->requiredIsMissing()) {
                $this->addMessage(new ParserMessage('critical', $this->getObjectType(), 'tag ' . $parser->getKey() . ' is not present'));
            }
            if ($parser->optionalIsMissing()) {
            $this->addMessage(new ParserMessage('warning', $this->getObjectType(), 'tag ' . $parser->getKey() . ' is not present'));
            }
        }

        $object = $this->processObject($object);
        return $object;
    }
} 