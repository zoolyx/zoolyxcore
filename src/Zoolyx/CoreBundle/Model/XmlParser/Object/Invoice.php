<?php

namespace Zoolyx\CoreBundle\Model\XmlParser\Object;

/**
 * GroupPractice
 */
class Invoice
{
    /** @var string */
    private $id;

    /** @var string */
    private $vat;

    /** @var string */
    private $fee;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param string $vat
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
    }

    /**
     * @return string
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * @param string $fee
     */
    public function setFee($fee)
    {
        $this->fee = $fee;
    }


}

