<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 06.03.16
 * Time: 12:40
 */

namespace Zoolyx\CoreBundle\Model\XmlParser;

use JMS\DiExtraBundle\Annotation as DI;

abstract class BaseParser implements ParserInterface{

    protected $messages = array();

    /**
     * @param ParserMessage $parserMessage
     */
    public function addMessage(ParserMessage $parserMessage) {
        $this->messages[] = $parserMessage;
    }

    public function addMessages($messages) {
        $this->messages = array_merge($this->messages, $messages);
    }

    /**
     * @return array
     */
    public function getMessages() {
        return $this->messages;
    }


} 