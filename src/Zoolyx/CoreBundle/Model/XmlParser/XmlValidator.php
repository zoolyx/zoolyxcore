<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 12.12.16
 * Time: 11:28
 */

namespace Zoolyx\CoreBundle\Model\XmlParser;

use DOMDocument;
use DOMElement;
use DOMNode;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Config\Definition\Exception\Exception;
use Zoolyx\CoreBundle\Model\File\File;

/**
 * @DI\Service("zoolyx_core.xml_parser.xml_validator")
 */

class XmlValidator {

    private $xml;
    /** @var  DomDocument */
    private $doc;
    private $errors = array();

    public function setXml($xml) {
        $this->xml = $xml;
    }

    public function getDoc() {
        return $this->doc;
    }

    public function getErrors() {
        return $this->errors;
    }

    public function getFiles($removeForXml = true) {
        $files = array();
        /** @var DomElement $fileElement */
        foreach ($this->doc->getElementsByTagName('file') as $fileElement) {
            /** @var DomNode $node */
            foreach ($fileElement->childNodes as $node) {
                if ($node->nodeType == XML_CDATA_SECTION_NODE) {
                    $file = new File();
                    $file
                        ->setName($fileElement->getAttribute('name'))
                        ->setContent($node->textContent);
                    $files[] = $file;
                    /*
                    $files[] = array(
                        'name' => $fileElement->getAttribute('name'),
                        'content' => $node->textContent
                    );
                    */
                    if ($removeForXml) $fileElement->removeChild($node);
                }
            }
        }
        return $files;
    }

    public function validate($xsdFile) {
        libxml_use_internal_errors(true);
        $this->errors = array();
        $this->doc = new DOMDocument();
        try {
            $this->doc->loadXML($this->xml);
        } catch(Exception $e) {
            $this->errors[] = array(
                'Error' => 'Invalid xml message received'
            );
            return false;
        }

        if ($xsdFile)
            return $this->schemaValidate($xsdFile);

        return true;
    }

    public function schemaValidate($xsdFile) {
        libxml_use_internal_errors(true);
        $is_valid_xml = $this->doc->schemaValidate($xsdFile);
        if (!$is_valid_xml) {
            $errors = libxml_get_errors();
            $levels = array(
                LIBXML_ERR_WARNING => 'Warning',
                LIBXML_ERR_ERROR => 'Error',
                LIBXML_ERR_FATAL => 'Fatal error'
            );
            foreach ($errors as $error) {
                $this->errors[] = array(
                    $levels[$error->level]." ".$error->code => trim($error->message)
                );
            }
            libxml_clear_errors();
            return false;
        }
        return true;
    }
} 