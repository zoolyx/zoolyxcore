<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 06.03.16
 * Time: 13:36
 */

namespace Zoolyx\CoreBundle\Model\XmlParser;


class Parser {

    private $key;
    private $method;
    private $reverseMethod = null;
    /** @var  XmlParser */
    private $parser;
    private $required;
    private $isPresent = false;

    /**
     * @param string $key
     * @param string $method
     * @param string $reverseMethod
     * @param ParserInterface $parser
     * @param bool $required
     */
    public function __construct($key, $method, $reverseMethod, ParserInterface $parser, $required = false) {
        $this->key = $key;
        $this->method = $method;
        $this->reverseMethod = $reverseMethod;
        $this->parser = $parser;
        $this->required = $required;
    }

    /**
     * @return mixed
     */
    public function getKey() {
        return $this->key;
    }

    /**
     * @return XmlParser
     */
    public function getParser() {
        return $this->parser;
    }

    /**
     * @param $key
     * @return bool
     */
    public function canParse($key) {
        return $this->key == $key;
    }

    /**
     * @param $object
     * @return bool
     */
    public function canBeUsedOn($object) {
        $isCallable = is_callable(array($object, $this->method));
        if (!$isCallable) {
            die("something wrong in configuration of method ".$this->method);
        }
       return $isCallable;
    }

    /**
     * @return bool
     */
    public function requiredIsMissing() {
        return $this->required && !$this->isPresent;
    }

    /**
     * @return bool
     */
    public function optionalIsMissing() {
        return !$this->required && !$this->isPresent;
    }

    /**
     * @param $object
     * @param $value
     */
    public function process($object, $value) {

        $newObject = $this->parser->parse($value);
        $object->{$this->method}($newObject);
        if (!is_null($this->reverseMethod)) {
            $newObject->{$this->reverseMethod}($object);
        }
        $this->isPresent = true;
    }
} 