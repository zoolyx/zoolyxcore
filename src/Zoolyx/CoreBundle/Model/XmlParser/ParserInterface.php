<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 06.03.16
 * Time: 13:29
 */

namespace Zoolyx\CoreBundle\Model\XmlParser;

use SimpleXMLElement;

interface ParserInterface{

    public function getObject();

    public function parse(SimpleXMLElement $xml);

    public function processObject($object);

    public function getObjectType();

    public function addMessage(ParserMessage $parserMessage);
    public function addMessages($messages);
    public function getMessages();
} 