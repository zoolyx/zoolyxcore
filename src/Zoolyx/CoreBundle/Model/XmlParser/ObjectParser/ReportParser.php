<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Owner as OwnerEntity;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\ReportShare;
use Zoolyx\CoreBundle\Entity\VeterinaryPracticeReport;
use Zoolyx\CoreBundle\Model\Report\VeterinaryPracticeReport\VeterinaryPracticeReportRoles;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.report")
 */
class ReportParser extends XmlParser{

    const REQUEST_TAG = 'RQ';
    const REQUEST_ID = 'ID';
    const MULTI_SAMPLE_TAG = 'MULTISAMPLE';
    const PRACTICE_REF_TAG = "PRACTICEREF";
    const VETERINARY_TAG = 'DA';
    const FA_TAG = 'FA';
    const INVOICE_TAG = 'INVOICE';
    const OWNER_TAG = "AD";
    const FILES_TAG = 'FILES';
    const CARBON_COPY_TAG = 'CC';

    /** @var Report */
    private $report = null;

    /** @var ObjectManager */
    private $entityManager;

    /**
     * Constructor.
     *
     * @param VeterinaryParser $veterinaryParser
     * @param OwnerParser $ownerParser
     * @param BillingParser $billingParser
     * @param InvoiceParser $invoiceParser
     * @param FilesParser $filesParser
     * @param StringParser $stringParser
     * @param CarbonCopyParser $carbonCopyParser
     * @param ObjectManager $entityManager
     *
     * @DI\InjectParams({
     *  "veterinaryParser" = @DI\Inject("zoolyx_core.xml_parser.veterinary"),
     *  "ownerParser" = @DI\Inject("zoolyx_core.xml_parser.owner"),
     *  "billingParser" = @DI\Inject("zoolyx_core.xml_parser.billing"),
     *  "invoiceParser" = @DI\Inject("zoolyx_core.xml_parser.invoice"),
     *  "filesParser" = @DI\Inject("zoolyx_core.xml_parser.file_array"),
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string"),
     *  "carbonCopyParser" = @DI\Inject("zoolyx_core.xml_parser.carbon_copy"),
     *  "entityManager" = @DI\Inject("doctrine.orm.entity_manager")
     * })
     */
    public function __construct(VeterinaryParser $veterinaryParser, OwnerParser $ownerParser, BillingParser $billingParser, InvoiceParser $invoiceParser, FilesParser $filesParser, StringParser $stringParser, CarbonCopyParser $carbonCopyParser, ObjectManager $entityManager)
    {

        $this->addAttribute(new Parser(self::REQUEST_ID,'setRequestId', null, $stringParser, true));
        $this->addTag(new Parser(self::MULTI_SAMPLE_TAG,'setMultiSample', null, $stringParser));
        $this->addTag(new Parser(self::PRACTICE_REF_TAG,'setPracticeReference', null, $stringParser, true));
        $this->addTag(new Parser(self::VETERINARY_TAG,'addVeterinaryPracticeReportAsVeterinary', 'setReport', $veterinaryParser, true));
        $this->addTag(new Parser(self::INVOICE_TAG,'setInvoiceObject', null, $invoiceParser, true));
        $this->addTag(new Parser(self::FA_TAG,'setBilling', null, $billingParser, true));
        $this->addTag(new Parser(self::OWNER_TAG,'setOwner', null, $ownerParser, true));
        $this->addTag(new Parser(self::FILES_TAG,'setFiles', null, $filesParser));
        $this->addTag(new Parser(self::CARBON_COPY_TAG,'setInCopy', null, $carbonCopyParser));

        $this->entityManager = $entityManager;
    }

    /**
     * @param Report $report
     * @return ReportParser
     */
    public function setReport(Report $report) {
        $this->report = $report;
        return $this;
    }

    public function getObject() {
        if (is_null($this->report)) { $this->report = new Report(); }
        $this->report->setCreatedOn(new \DateTime());

        // remove the owner of a report
        /** @var VeterinaryPracticeReport $vpr */
        foreach ($this->report->getVeterinaryPracticeReports() as $vpr) {
            if ($vpr->getRole() == VeterinaryPracticeReportRoles::VPR_VETERINARY) {
                $this->entityManager->remove($vpr);
            }
        }
        $this->entityManager->flush();

        return $this->report;
    }
    public function processObject($object) {
        /** @var Report $object */
        /** @var VeterinaryPracticeReport $veterinaryPracticeReport */
        foreach ($object->getVeterinariesInCopy() as $veterinaryPracticeReport) {
            $veterinaryPracticeReport->setReport($object)->setRole(VeterinaryPracticeReportRoles::VPR_SHARED);
            $object->addVeterinaryPracticeReport($veterinaryPracticeReport);
        }
        /** @var OwnerEntity $owner */
        foreach ($object->getOwnersInCopy() as $owner) {
            $reportShare = new ReportShare();
            $reportShare
                ->setOwner($owner)
                ->setReport($object);
            $this->entityManager->persist($reportShare);
            $this->entityManager->persist($object);
        }

        $invoice = $object->getInvoiceObject();
        $object->setIsInvoiced($invoice->getId());
        $object->setVat($invoice->getVat());
        $object->setFee($invoice->getFee());
        return $object;
    }
    public function getObjectType() {
        return 'Report';
    }
}