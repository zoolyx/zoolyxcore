<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use SimpleXMLElement;
use Zoolyx\CoreBundle\Model\XmlParser\ValueParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.string")
 */
class StringParser extends ValueParser {


    public function getObject() {
        return '';
    }

    /**
     * @param SimpleXMLElement $parameterXml
     * @return string
     */
    public function parse(SimpleXMLElement $parameterXml)
    {
        return $parameterXml->__toString();
    }

    public function processObject($object) {
        return $object;
    }


    public function getObjectType() {
        return 'string';
    }
}