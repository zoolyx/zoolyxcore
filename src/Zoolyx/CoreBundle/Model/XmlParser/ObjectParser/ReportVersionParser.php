<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\ReportVersion;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.report_version")
 */
class ReportVersionParser extends XmlParser{

    const SAMPLE_TAG = 'SC';
    const LANGUAGE_TAG = 'LANGUAGE';

    /**
     * Constructor.
     *
     * @param SampleParser $sampleParser
     * @param StringParser $stringParser
     *
     * @DI\InjectParams({
     *  "sampleParser" = @DI\Inject("zoolyx_core.xml_parser.sample"),
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string"),
     * })
     */
    public function __construct(SampleParser $sampleParser, StringParser $stringParser)
    {
        $this->addTag(new Parser(self::LANGUAGE_TAG,'setLanguage', null, $stringParser));
        $this->addTag(new Parser(self::SAMPLE_TAG,'addSample', 'setReportVersion', $sampleParser, true));
    }

    public function getObject() {
        $reportVersion = new ReportVersion();
        return $reportVersion;
    }
    public function processObject($object) {
        return $object;
    }
    public function getObjectType() {
        return 'ReportVersion';
    }
}