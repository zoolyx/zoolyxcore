<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Sample;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.sample")
 */
class SampleParser extends XmlParser{

    const CODE_TAG = "ID";
    const CUMULATIVE_TAG = "CUMULATIVE";
    const PET_TAG = "PE";
    const COMPLETED_TAG = "COMPLETED";
    const DATE_RECEIVED = "DATE_RECEIVED";
    const DATE_COMPLETED = "DATE_COMPLETED";
    const URGENT_TAG = "URGENT";
    const PARAMETER_GROUP_TAG = "PG";
    const EXTERNAL_ID_TAG = "EXTID";
    const CONTRACTOR_REF_TAG = "COREF";
    const PHYSICAL_TAG = "PHYSICAL";

    /**
     * Constructor.
     *
     * @param PetParser $petParser
     * @param ParameterGroupParser $parameterGroupParser
     * @param StringParser $stringParser
     * @param DateTimeParser $dateTimeParser
     *
     * @DI\InjectParams({
     *  "petParser" = @DI\Inject("zoolyx_core.xml_parser.pet"),
     *  "parameterGroupParser" = @DI\Inject("zoolyx_core.xml_parser.parameter_group"),
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string"),
     *  "dateTimeParser" = @DI\Inject("zoolyx_core.xml_parser.datetime")
     * })
     */
    public function __construct(PetParser $petParser, ParameterGroupParser $parameterGroupParser, StringParser $stringParser, DateTimeParser $dateTimeParser)
    {
        $this->addAttribute(new Parser(self::CODE_TAG,'setCode', null, $stringParser, true));
        $this->addTag(new Parser(self::CUMULATIVE_TAG,'setCumulative', null, $stringParser, true));
        $this->addTag(new Parser(self::PET_TAG,'setPet', null, $petParser, true));
        $this->addTag(new Parser(self::COMPLETED_TAG,'setCOMPLETED', null, $stringParser, true));
        $this->addTag(new Parser(self::DATE_RECEIVED,'setDateReceived', null, $dateTimeParser, true));
        $this->addTag(new Parser(self::DATE_COMPLETED,'setDateCompleted', null, $dateTimeParser, true));
        $this->addTag(new Parser(self::URGENT_TAG,'setUrgent', null, $stringParser, true));
        $this->addTag(new Parser(self::PARAMETER_GROUP_TAG,'addParameterGroup', 'setSample', $parameterGroupParser, true));
        $this->addTag(new Parser(self::EXTERNAL_ID_TAG,'setExternalId', null, $stringParser));
        $this->addTag(new Parser(self::CONTRACTOR_REF_TAG,'setContractorReference', null, $stringParser, true));
        $this->addTag(new Parser(self::PHYSICAL_TAG,'setIsPhysical', null, $stringParser, true));
    }

    public function getObject() {
        return new Sample();
    }
    public function processObject($object) {
        return $object;
    }
    public function getObjectType() {
        return 'Sample';
    }
}