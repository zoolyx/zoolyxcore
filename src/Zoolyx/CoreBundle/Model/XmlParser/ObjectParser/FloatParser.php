<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use SimpleXMLElement;
use Zoolyx\CoreBundle\Model\XmlParser\ValueParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.float")
 */
class FloatParser extends ValueParser {


    public function getObject() {
        return 0;
    }
    /**
     * @param SimpleXMLElement $parameterXml
     * @return string
     */
    public function parse(SimpleXMLElement $parameterXml)
    {
        $result = $parameterXml->__toString();
        return is_numeric($result) ? $result : null;
    }

    public function processObject($object) {
        return $object;
    }

    public function getObjectType() {
        return 'float';
    }
}