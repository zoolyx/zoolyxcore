<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use SimpleXMLElement;
use Zoolyx\CoreBundle\Model\XmlParser\ValueParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.datetime")
 */
class DateTimeParser extends ValueParser {

    public function getObject() {
        return '';
    }

    /**
     * @param SimpleXMLElement $parameterXml
     * @return string
     */
    public function parse(SimpleXMLElement $parameterXml)
    {
        $content = $parameterXml->__toString();
        return $content != '' ? new \DateTime($parameterXml->__toString()) : null;
    }

    public function processObject($object) {
        return $object;
    }


    public function getObjectType() {
        return 'DateTime';
    }
}