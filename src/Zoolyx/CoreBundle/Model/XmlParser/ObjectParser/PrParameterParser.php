<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\PrParameter;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.pr_parameter")
 */
class PrParameterParser extends XmlParser{

    const PARAMETER_ID = "ID";
    const RESULT_TAG = "RESULT";


    /**
     * Constructor.
     *
     * @param StringParser $stringParser
     *
     * @DI\InjectParams({
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string")
     * })
     */
    public function __construct(StringParser $stringParser)
    {
        $this->addAttribute(new Parser(self::PARAMETER_ID,'setParameterId', null, $stringParser, true));
        $this->addTag(new Parser(self::RESULT_TAG,'setResult', null, $stringParser, true));
    }

    public function getObject() {
        return new PrParameter();
    }

    public function processObject($object) {
        return $object;
    }
    public function getObjectType() {
        return 'PrParameter';
    }
}