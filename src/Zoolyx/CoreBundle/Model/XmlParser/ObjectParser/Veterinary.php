<?php

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

class Veterinary
{

    //Veterinary related
    /** @var string */
    private $title;
    private $firstName;
    private $lastName;
    private $language;
    private $email;
    private $orderNumber = "";
    /** @var string */
    private $fa='';
    /** @var string */
    private $vat='';

    //VeterinaryPractice related
    private $limsId;

    //PracticeRelated
    private $practiceId;
    private $practiceName="";
    private $street="";
    private $zipCode="";
    private $city="";
    private $country="";
    private $telephone = "";
    private $mobile = "";

    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Veterinary
     */
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return $this
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFa()
    {
        return $this->fa;
    }

    /**
     * @param string $fa
     * @return $this
     */
    public function setFa($fa)
    {
        $this->fa = $fa;
        return $this;
    }

    /**
     * @return string
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param string $vat
     * @return $this
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     * @return $this
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     * @return $this
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLimsId()
    {
        return $this->limsId;
    }

    /**
     * @param mixed $limsId
     */
    public function setLimsId($limsId)
    {
        $this->limsId = $limsId;
    }

    /**
     * @return mixed
     */
    public function getPracticeId()
    {
        return $this->practiceId;
    }

    /**
     * @param mixed $practiceId
     */
    public function setPracticeId($practiceId)
    {
        $this->practiceId = $practiceId;
    }
    
    /**
     * @return mixed
     */
    public function getPracticeName()
    {
        return $this->practiceName;
    }

    /**
     * @param mixed $practiceName
     */
    public function setPracticeName($practiceName)
    {
        $this->practiceName = $practiceName;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param mixed $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }



}
