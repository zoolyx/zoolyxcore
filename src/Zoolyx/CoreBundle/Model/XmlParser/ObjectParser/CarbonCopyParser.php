<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.carbon_copy")
 */
class CarbonCopyParser extends XmlParser{

    const VETERINARY_TAG = 'DA';
    const OWNER_TAG = 'AD';

    /**
     * Constructor.
     *
     * @param VeterinaryParser $veterinaryParser
     * @param OwnerParser $ownerParser
     *
     * @DI\InjectParams({
     *  "veterinaryParser" = @DI\Inject("zoolyx_core.xml_parser.veterinary"),
     *  "ownerParser" = @DI\Inject("zoolyx_core.xml_parser.owner")
     * })
     */
    public function __construct(VeterinaryParser $veterinaryParser, OwnerParser $ownerParser)
    {

        $this->addTag(new Parser(self::VETERINARY_TAG,'add', null, $veterinaryParser, true));
        $this->addTag(new Parser(self::OWNER_TAG,'add', null, $ownerParser, true));
    }

    public function getObject() {
        return new Collection();
    }
    public function processObject($object) {
        /** @var Collection $object */
        return $object->getElements();
    }
    public function getObjectType() {
        return 'Collection';
    }
}