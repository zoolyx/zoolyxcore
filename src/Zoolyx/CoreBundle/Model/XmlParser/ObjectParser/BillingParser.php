<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use SimpleXMLElement;
use Zoolyx\CoreBundle\Model\XmlParser\ValueParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.billing")
 */
class BillingParser extends ValueParser{

    const FA_ID = "ID";

    /**
     * @return string
     */
    public function getObject() {
        return '';
    }

    /**
     * @param SimpleXMLElement $parameterXml
     * @return string
     */
    public function parse(SimpleXMLElement $parameterXml)
    {
        foreach ($parameterXml->attributes() as $attributeName => $attributeValue) {
            if (strtoupper($attributeName) == self::FA_ID) {
                return $attributeValue;
            }
        }
        return '';
    }

    public function processObject($object) {
        return $object;
    }

    public function getObjectType() {
        return 'Billing';
    }
}