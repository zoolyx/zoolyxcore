<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Parameter;
use Zoolyx\CoreBundle\Entity\ParameterBase;
use Zoolyx\CoreBundle\Entity\Repository\ParameterBaseRepository;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.parameter")
 */
class ParameterParser extends XmlParser{

    const PARAMETER_ID = "ID";
    const DESCRIPTION_TAG = "DESCRIPTION";
    const RESULT_TAG = "RESULT";
    const RESULT_FLAG_TAG = "RESULT_FLAG";
    const STATUS_TAG = "STATUS";
    const LOINC_TAG = "LOINC";
    const LINK_TAG = "LINK";
    const P_P_TAG = "PP";
    const PRICE_TAG = "PRICE";
    const UNIT_TAG = "UNIT";
    const REFERENCE_TAG = "REFRANGE";
    const COMMENT_TAG = "COMMENT";
    const IS_TITLE_TAG = "ISTITLE";
    const VALIDATED_BY_TAG = 'VALIDATED_BY';
    const CUMULATIVE_TAG = 'CUMULATIVE';
    const FILES_TAG = 'FILES';
    const CONTRACTOR_TAG = "CO";
    const EXECUTION_DATE_TAG = "EXECUTION_DATE";
    const DATE_EXPECTED_TAG = "DATE_EXPECTED";

    /** @var ParameterBaseRepository */
    private $parameterBaseRepository;

    /**
     * Constructor.
     *
     * @param ParameterReferenceParser $parameterReferenceParser
     * @param FilesParser $filesParser
     * @param ValidatorParser $validatorParser
     * @param ContractorRequiredParser $contractorRequiredParser
     * @param StringParser $stringParser
     * @param ParameterBaseRepository $parameterBaseRepository
     * @param DateTimeParser $dateTimeParser
     *
     * @DI\InjectParams({
     *  "parameterReferenceParser" = @DI\Inject("zoolyx_core.xml_parser.parameter_reference"),
     *  "filesParser" = @DI\Inject("zoolyx_core.xml_parser.file_array"),
     *  "validatorParser" = @DI\Inject("zoolyx_core.xml_parser.validator"),
     *  "contractorRequiredParser" = @DI\Inject("zoolyx_core.xml_parser.contractor_required"),
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string"),
     *  "parameterBaseRepository" = @DI\Inject("zoolyx_core.repository.parameter_base"),
     *  "dateTimeParser" = @DI\Inject("zoolyx_core.xml_parser.datetime")
     * })
     */
    public function __construct(ParameterReferenceParser $parameterReferenceParser, FilesParser $filesParser, ValidatorParser $validatorParser, ContractorRequiredParser $contractorRequiredParser, StringParser $stringParser, ParameterBaseRepository $parameterBaseRepository, DateTimeParser $dateTimeParser)
    {
        $this->parameterBaseRepository = $parameterBaseRepository;
        $this->addAttribute(new Parser(self::PARAMETER_ID,'setParameterId', null, $stringParser, true));
        $this->addTag(new Parser(self::RESULT_TAG,'setResult', null, $stringParser, true));
        $this->addTag(new Parser(self::RESULT_FLAG_TAG,'setResultFlag', null, $stringParser, false));
        $this->addTag(new Parser(self::STATUS_TAG,'setStatus', null, $stringParser, false));
        $this->addTag(new Parser(self::P_P_TAG,'setParameterP', null, $stringParser, false));
        $this->addTag(new Parser(self::PRICE_TAG,'setPrice', null, $stringParser, false));
        $this->addTag(new Parser(self::REFERENCE_TAG,'setReference', null, $parameterReferenceParser, true));
        $this->addTag(new Parser(self::COMMENT_TAG,'setComment', null, $stringParser, false));
        $this->addTag(new Parser(self::VALIDATED_BY_TAG,'setValidatedBy', null, $validatorParser, false));
        $this->addTag(new Parser(self::FILES_TAG,'setFiles', null, $filesParser, false));
        $this->addTag(new Parser(self::CONTRACTOR_TAG,'setContractorRequired', null, $contractorRequiredParser, true));
        $this->addTag(new Parser(self::EXECUTION_DATE_TAG,'setExecutionDate', null, $dateTimeParser, true));
        $this->addTag(new Parser(self::DATE_EXPECTED_TAG,'setDateExpected', null, $dateTimeParser, true));

    }

    public function getObject() {
        return new Parameter();
    }

    public function processObject($object) {
        /** @var Parameter $object */
        $object->setResultString($object->getResult());

        $result = $object->getResult();
        $result = ltrim($result);
        $result = ltrim($result, '<');
        $result = ltrim($result, '>');
        $result = trim($result);
        if (!is_numeric($result) || strlen($result) > 10) {
            $object->setResult(null);
        } else {
            $object->setResult($result);
        }
        /** @var ParameterBase $parameterBase */
        $parameterBase = $this->parameterBaseRepository->findOneBy(array('parameterId'=>$object->getParameterId()));
        $object->setParameterBase($parameterBase);

        return $object;
    }
    public function getObjectType() {
        return 'Parameter';
    }
}