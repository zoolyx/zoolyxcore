<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\ParameterReference;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.parameter_reference")
 */
class ParameterReferenceParser extends XmlParser{

    const REF_LOW_TAG = "REFLOW";
    const REF_HIGH_TAG = "REFHIGH";
    const REF_DISPLAY_TAG = "REFDISPLAY";
    const REF_IMAGE_TAG = "FILE";

    /**
     * Constructor.
     *
     * @param StringParser $stringParser
     * @param FloatParser $floatParser
     * @param ReferenceImageParser $referenceImageParser
     *
     * @DI\InjectParams({
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string"),
     *  "floatParser" = @DI\Inject("zoolyx_core.xml_parser.float"),
     *  "referenceImageParser" = @DI\Inject("zoolyx_core.xml_parser.reference_image")
     * })
     */
    public function __construct(StringParser $stringParser, FloatParser $floatParser,ReferenceImageParser $referenceImageParser)
    {
        $this->addTag(new Parser(self::REF_LOW_TAG,'setLow', null, $floatParser, true));
        $this->addTag(new Parser(self::REF_HIGH_TAG,'setHigh', null, $floatParser, true));
        $this->addTag(new Parser(self::REF_DISPLAY_TAG,'setDisplay', null, $stringParser, true));
        $this->addTag(new Parser(self::REF_IMAGE_TAG,'setImage', null, $referenceImageParser, true));
    }

    public function getObject() {
        return new ParameterReference();
    }

    public function processObject($object) {
        return $object;
    }
    public function getObjectType() {
        return 'ParameterReference';
    }
}