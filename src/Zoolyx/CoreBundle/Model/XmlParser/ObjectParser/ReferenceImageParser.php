<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\File;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.reference_image")
 */
class ReferenceImageParser extends XmlParser{

    const NAME_TAG = "NAME";


    /**
     * Constructor.
     *
     * @param StringParser $stringParser
     *
     * @DI\InjectParams({
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string")
     * })
     */
    public function __construct(StringParser $stringParser)
    {
        $this->addTag(new Parser(self::NAME_TAG,'setName', null, $stringParser, false));
    }


    /**
     * @return File
     */
    public function getObject() {
        $file = new File();
        $file->setName('');
        return $file;
    }

    /**
     * @param File $object
     * @return File
     */
    public function processObject($object) {
        return $object->getName();
    }

    /**
     * @return string
     */
    public function getObjectType() {
        return 'ReferenceImage';
    }


}