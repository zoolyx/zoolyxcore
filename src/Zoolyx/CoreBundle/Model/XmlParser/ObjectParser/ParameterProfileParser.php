<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\ParameterProfile;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.parameter_profile")
 */
class ParameterProfileParser extends XmlParser{

    const REFERENCE_TAG = 'ID';
    const CATEGORY_TAG = 'CATEGORY';
    const SPECIES_TAG = 'SPECIES';
    const DESCRIPTION_TAG = 'DESCRIPTION';
    const PRICE_TAG = 'PRICE';
    const URL_TAG = 'URL';
    const PARAMETER_TAG = 'PA';

    /**
     * Constructor.
     *
     * @param StringParser $stringParser
     * @param FloatParser $floatParser
     * @param PrParameterParser $parameterParser
     *
     *
     * @DI\InjectParams({
     *  "parameterParser" = @DI\Inject("zoolyx_core.xml_parser.pr_parameter"),
     *  "floatParser" = @DI\Inject("zoolyx_core.xml_parser.float"),
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string")
     * })
     */
    public function __construct(StringParser $stringParser, FloatParser $floatParser, PrParameterParser $parameterParser)
    {
        $this->addAttribute(new Parser(self::REFERENCE_TAG,'setReference', null, $stringParser, true));
        $this->addTag(new Parser(self::CATEGORY_TAG,'setCategory', null, $stringParser, true));
        $this->addTag(new Parser(self::SPECIES_TAG,'setSpecies', null, $stringParser, true));
        $this->addTag(new Parser(self::DESCRIPTION_TAG,'setDescription', null, $stringParser, true));
        $this->addTag(new Parser(self::PRICE_TAG,'setPrice', null, $floatParser, true));
        $this->addTag(new Parser(self::URL_TAG,'setUrl', null, $stringParser, true));
        $this->addTag(new Parser(self::PARAMETER_TAG,'addParameter', 'setParameterProfile', $parameterParser, true));
    }

    public function getObject() {
        return new ParameterProfile();
    }
    public function processObject($object) {
        return $object;
    }
    public function getObjectType() {
        return 'ParameterProfile';
    }
}