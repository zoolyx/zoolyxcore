<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use phpDocumentor\Reflection\DocBlock\Tag;
use Zoolyx\CoreBundle\Entity\File;
use Zoolyx\CoreBundle\Model\XmlParser\Object\Invoice;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.invoice")
 */
class InvoiceParser extends XmlParser{

    const INVOICE_ID = "ID";
    const FEE_TAG = "FEE";
    const VAT_TAG = "VAT";

    /**
     * Constructor.
     *
     * @param StringParser $stringParser
     *
     * @DI\InjectParams({
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string")
     * })
     */
    public function __construct(StringParser $stringParser)
    {
        $this->addAttribute(new Parser(self::INVOICE_ID,'setId', null, $stringParser, true));
        $this->addTag(new Parser(self::FEE_TAG,'setFee', null, $stringParser, false));
        $this->addTag(new Parser(self::VAT_TAG,'setVat', null, $stringParser, false));
    }


    /**
     * @return File
     */
    public function getObject() {
        return new Invoice();
    }

    /**
     * @param File $object
     * @return File
     */
    public function processObject($object) {
        return $object;
    }

    /**
     * @return string
     */
    public function getObjectType() {
        return 'Invoice';
    }
}