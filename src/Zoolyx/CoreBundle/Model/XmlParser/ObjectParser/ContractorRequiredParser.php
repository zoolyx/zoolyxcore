<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Contractor;
use Zoolyx\CoreBundle\Entity\Repository\ContractorRepository;
use Zoolyx\CoreBundle\Model\Report\Parameter\ContractorRequired;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.contractor_required")
 */
class ContractorRequiredParser extends XmlParser{

    //<co id="DB" pp="205" pa="H205"></co>
    const CONTRACTOR_ID = "ID";
    const PARAMETER_PROFILE_ATTRIBUTE = "PP";
    const PARAMETER_ATTRIBUTE = "PA";

    /**
     * @var ContractorRepository
     */
    private $contractorRepository;

    /**
     * Constructor.
     *
     * @param StringParser $stringParser
     * @param ContractorRepository $contractorRepository
     *
     * @DI\InjectParams({
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string"),
     *  "contractorRepository" = @DI\Inject("zoolyx_core.repository.contractor")
     * })
     */
    public function __construct(StringParser $stringParser, ContractorRepository $contractorRepository) {
        $this->contractorRepository = $contractorRepository;
        $this->addAttribute(new Parser(self::CONTRACTOR_ID,'setContractorId', null, $stringParser, true));
        $this->addAttribute(new Parser(self::PARAMETER_PROFILE_ATTRIBUTE,'setParameterProfileId', null, $stringParser, true));
        $this->addAttribute(new Parser(self::PARAMETER_ATTRIBUTE,'setParameterId', null, $stringParser, true));
    }

    /**
     * @return Contractor
     */
    public function getObject() {
        return new ContractorRequired();
    }

    /**
     * @param Contractor $object
     * @return Contractor
     */
    public function processObject($object) {
        return $object;
    }

    /**
     * @return string
     */
    public function getObjectType() {
        return "ContractorRequired";
    }
}