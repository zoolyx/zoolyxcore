<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use phpDocumentor\Reflection\DocBlock\Tag;
use Zoolyx\CoreBundle\Entity\ParameterGroup;
use Zoolyx\CoreBundle\Entity\ParameterGroupBase;
use Zoolyx\CoreBundle\Entity\Repository\ParameterGroupBaseRepository;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.parameter_group")
 */
class ParameterGroupParser extends XmlParser{

    const PARAMETER_GROUP_ID = "ID";
    //const DESCRIPTION_TAG = "DESCRIPTION";
    const PARAMETER_TAG = "PA";

    /** @var ParameterGroupBaseRepository */
    private $parameterGroupBaseRepository;

    /**
     * Constructor.
     *
     * @param ParameterGroupBaseRepository $parameterGroupBaseRepository
     * @param ParameterParser $parameterParser
     * @param StringParser $stringParser
     *
     * @DI\InjectParams({
     *  "parameterGroupBaseRepository" = @DI\Inject("zoolyx_core.repository.parameter_group_base"),
     *  "parameterParser" = @DI\Inject("zoolyx_core.xml_parser.parameter"),
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string")
     * })
     */
    public function __construct(ParameterGroupBaseRepository $parameterGroupBaseRepository, ParameterParser $parameterParser, StringParser $stringParser)
    {
        $this->parameterGroupBaseRepository = $parameterGroupBaseRepository;
        $this->addAttribute(new Parser(self::PARAMETER_GROUP_ID,'setParameterGroupId', null, $stringParser, true));
        //$this->addTag(new Parser(self::DESCRIPTION_TAG,'setDescription', null, $stringParser, true));
        $this->addTag(new Parser(self::PARAMETER_TAG,'addParameter', 'setParameterGroup', $parameterParser, true));
    }

    public function getObject() {
        return new ParameterGroup();
    }

    public function processObject($parameterGroup) {
        /** @var ParameterGroup $parameterGroup */
        /** @var ParameterGroupBase $parameterGroupBase */
        $parameterGroupBase = $this->parameterGroupBaseRepository->findOneBy(array('parameterGroupId'=>$parameterGroup->getParameterGroupId()));
        $parameterGroup->setParameterGroupBase($parameterGroupBase);
        return $parameterGroup;
    }
    public function getObjectType() {
        return 'ParameterGroup';
    }

}