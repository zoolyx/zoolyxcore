<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use SimpleXMLElement;
use Zoolyx\CoreBundle\Entity\Repository\ValidatorRepository;
use Zoolyx\CoreBundle\Model\XmlParser\ValueParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.validator")
 */
class ValidatorParser extends ValueParser{

    protected $validatorRepository;

    /**
     * Constructor.
     *
     * @param ValidatorRepository $validatorRepository

     *
     * @DI\InjectParams({
     *  "validatorRepository" = @DI\Inject("zoolyx_core.repository.validator")
     * })
     */
    public function __construct(ValidatorRepository $validatorRepository) {
        $this->validatorRepository = $validatorRepository;
    }

    public function getObject() {
        return '';
    }

    /**
     * @param SimpleXMLElement $parameterXml
     * @return string
     */
    public function parse(SimpleXMLElement $parameterXml)
    {
        $validatorReference = $parameterXml->__toString();
        $validator = $this->validatorRepository->findOneByReference($validatorReference);
        return $validator;
    }

    public function processObject($object) {
        return $object;
    }


    public function getObjectType() {
        return 'string';
    }
}