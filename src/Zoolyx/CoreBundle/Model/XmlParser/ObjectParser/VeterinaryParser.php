<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Zoolyx\AccountBundle\Model\Token\TokenGenerator;
use Zoolyx\CoreBundle\Entity\Repository\VatRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryPracticeReportRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Vat;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Entity\VeterinaryPracticeReport;
use Zoolyx\CoreBundle\Event\NewAccountEvent;
use Zoolyx\CoreBundle\Events;
use Zoolyx\CoreBundle\Model\Account\AccountManager;
use Zoolyx\CoreBundle\Model\XmlParser\ObjectParser\Veterinary as VeterinaryLocal;
use Zoolyx\CoreBundle\Entity\Practice;
use Zoolyx\CoreBundle\Entity\Repository\PracticeRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryPracticeRepository;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.veterinary")
 */
class VeterinaryParser extends XmlParser{

    const LIMS_ID = "ID";
    const TITLE_TAG = "TITLE";
    const FIRST_NAME_TAG = "FIRSTNAME";
    const LAST_NAME_TAG = "LASTNAME";
    const PRACTICE_ID_TAG = "PRACTICEID";
    const PRACTICE_NAME_TAG = "PRACTICENAME";
    const STREET_TAG = "STREET";
    const ZIP_CODE_TAG = "ZIPCODE";
    const CITY_TAG = "CITY";
    const COUNTRY_TAG = "COUNTRY";
    const TELEPHONE_TAG = "TEL";
    const MOBILE_TAG = "GSM";
    const LANGUAGE_TAG = "LANGUAGE";
    const EMAIL_TAG = "EMAIL";
    const ORDER_NUMBER_TAG = "REGID";
    const FA_TAG = "FA";
    const VAT_TAG = "VATID";

    /** @var AccountManager */
    private $accountManager;

    /** @var  VeterinaryPracticeRepository */
    private $veterinaryPracticeRepository;

    /** @var  VeterinaryPracticeReportRepository */
    private $veterinaryPracticeReportRepository;

    /** @var  PracticeRepository */
    private $practiceRepository;
    /** @var  VatRepository */
    private $vatRepository;

    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /** @var TokenGenerator */
    private $tokenGenerator;



    /**
     * Constructor.
     *
     * @param AccountManager $accountManager
     * @param VeterinaryPracticeRepository $veterinaryPracticeRepository
     * @param VeterinaryPracticeReportRepository $veterinaryPracticeReportRepository
     * @param PracticeRepository $practiceRepository
     * @param VatRepository $vatRepository
     * @param StringParser $stringParser
     * @param EventDispatcherInterface $eventDispatcher
     * @param TokenGenerator $tokenGenerator
     *
     * @DI\InjectParams({
     *  "accountManager" = @DI\Inject("zoolyx_core.account.manager"),
     *  "veterinaryPracticeRepository" = @DI\Inject("zoolyx_core.repository.veterinary_practice"),
     *  "veterinaryPracticeReportRepository" = @DI\Inject("zoolyx_core.repository.veterinary_practice_report"),
     *  "practiceRepository" = @DI\Inject("zoolyx_core.repository.practice"),
     *  "vatRepository" = @DI\Inject("zoolyx_core.repository.vat"),
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string"),
     *  "eventDispatcher" = @DI\Inject("event_dispatcher"),
     *  "tokenGenerator" = @DI\Inject("zoolyx_account.generator.token")
     * })
     */
    public function __construct(
            AccountManager $accountManager,
            VeterinaryPracticeRepository $veterinaryPracticeRepository,
            VeterinaryPracticeReportRepository $veterinaryPracticeReportRepository,
            PracticeRepository $practiceRepository,
            VatRepository $vatRepository,
            StringParser $stringParser,
            EventDispatcherInterface $eventDispatcher,
            TokenGenerator $tokenGenerator
    ) {
        $this->accountManager = $accountManager;
        $this->veterinaryPracticeRepository = $veterinaryPracticeRepository;
        $this->veterinaryPracticeReportRepository = $veterinaryPracticeReportRepository;
        $this->vatRepository = $vatRepository;
        $this->practiceRepository = $practiceRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->tokenGenerator = $tokenGenerator;
        $this->addAttribute(new Parser(self::LIMS_ID,'setLimsId', null, $stringParser, true));
        $this->addTag(new Parser(self::TITLE_TAG,'setTitle', null, $stringParser, true));
        $this->addTag(new Parser(self::FIRST_NAME_TAG,'setFirstName', null, $stringParser, true));
        $this->addTag(new Parser(self::LAST_NAME_TAG,'setLastName', null, $stringParser, true));
        $this->addTag(new Parser(self::PRACTICE_ID_TAG,'setPracticeId', null, $stringParser, true));
        $this->addTag(new Parser(self::PRACTICE_NAME_TAG,'setPracticeName', null, $stringParser, true));
        $this->addTag(new Parser(self::STREET_TAG,'setStreet', null, $stringParser, true));
        $this->addTag(new Parser(self::ZIP_CODE_TAG,'setZipCode', null, $stringParser, true));
        $this->addTag(new Parser(self::CITY_TAG,'setCity', null, $stringParser, true));
        $this->addTag(new Parser(self::COUNTRY_TAG,'setCountry', null, $stringParser, true));
        $this->addTag(new Parser(self::TELEPHONE_TAG,'setTelephone', null, $stringParser, true));
        $this->addTag(new Parser(self::MOBILE_TAG,'setMobile', null, $stringParser, true));
        $this->addTag(new Parser(self::LANGUAGE_TAG,'setLanguage', null, $stringParser, true));
        $this->addTag(new Parser(self::EMAIL_TAG,'setEmail', null, $stringParser, true));
        $this->addTag(new Parser(self::ORDER_NUMBER_TAG,'setOrderNumber', null, $stringParser, true));
        $this->addTag(new Parser(self::FA_TAG,'setFa', null, $stringParser, true));
        $this->addTag(new Parser(self::VAT_TAG,'setVAT', null, $stringParser, true));
    }

    /**
     * @return VeterinaryLocal
     */
    public function getObject() {
        return new VeterinaryLocal();
    }

    /**
     * @param VeterinaryLocal $object
     * @return Practice
     */
    public function processObject($object) {
        $veterinaryAccount = null;
        /* @todo write method to validate an email address */
        if ($object->getEmail()) {
            $veterinaryAccount = $this->accountManager->loadAccount($object->getEmail());
            if (!$veterinaryAccount) {
                $veterinaryAccount = new User();
                $veterinaryAccount
                    ->setEmail($object->getEmail())
                    ->setUsername($object->getEmail())
                    ->setPlainPassword("tmp")
                    ->setLanguage($object->getLanguage())
                    ->setEnabled(true);
                $this->accountManager->setAccount($veterinaryAccount);
            }
            if (!$veterinaryAccount->hasRole("ROLE_VETERINARY")) {
                $veterinaryAccount->addRole("ROLE_VETERINARY");
            }
            $veterinaryAccount->setEnabled(true);
        }

        $vat = null;
        if (!is_null($object->getFa()) && $object->getFa()!='') {
            $vat = $this->vatRepository->findOneBy(array('faId'=>$object->getFa()));
            if (is_null($vat)) {
                $vat = new Vat();
                $vat->setFaId($object->getFa());
            }
            $vat->setVat($object->getVat());
        }

        $veterinaryPractice = $this->veterinaryPracticeRepository->findOneBy(array("limsId"=>$object->getLimsId()));
        if (is_null($veterinaryPractice)) {
            $veterinaryPractice = new VeterinaryPractice();
            $veterinaryPractice
                ->setLimsId($object->getLimsId())
                ->setRole(VeterinaryPractice::ROLE_PRACTICE_ADMIN)
                ->setVat($vat)
            ;

            $veterinary = new Veterinary();
            $veterinary
                ->setFirstName($object->getFirstName())
                ->setLastName($object->getLastName())
                ->setLanguage($object->getLanguage())
                ->setOrderNumber($object->getOrderNumber())
                ->setTelephone($object->getMobile())
                ->setAccount($veterinaryAccount);
            $veterinaryPractice->setVeterinary($veterinary);
            $veterinary->addVeterinaryPractice($veterinaryPractice);

            if ($object->getPracticeId() == '') $object->setPracticeId(null);
            $practice = is_null($object->getPracticeId()) ? null : $this->practiceRepository->findOneBy(array("practiceId" => $object->getPracticeId()));
            if (is_null($practice)) {
                $practice = new Practice();
                $practice
                    ->setName($object->getPracticeName())
                    ->setDescription('')
                    ->setPracticeId($object->getPracticeId())
                    ->setStreet($object->getStreet())
                    ->setCity($object->getCity())
                    ->setZipCode($object->getZipCode())
                    ->setCountry($object->getCountry())
                    ->setTelephone($object->getTelephone())
                ;
            }
            /*
            $practice
                ->setName(ucfirst(mb_strtolower($object->getFirstName())) . ' ' . ucfirst(mb_strtolower($object->getLastName())));
            */
            $veterinaryPractice->setPractice($practice);
            //$practice->addVeterinaryPractice($veterinaryPractice);
        } else {
            $veterinaryPractice->setVat($vat);
            /** @var Veterinary $veterinary */
            $veterinary = $veterinaryPractice->getVeterinary();
            $veterinary
                ->setFirstName($object->getFirstName())
                ->setLastName($object->getLastName())
                ->setLanguage($object->getLanguage())
                ->setOrderNumber($object->getOrderNumber())
                ->setTelephone($object->getMobile())
                ->setAccount($veterinaryAccount);
            /** @var Practice $practice */
            $practice = $veterinaryPractice->getPractice();
            $practice
                ->setName($object->getPracticeName())
                ->setDescription('')
                ->setPracticeId($object->getPracticeId())
                ->setStreet($object->getStreet())
                ->setCity($object->getCity())
                ->setZipCode($object->getZipCode())
                ->setCountry($object->getCountry())
                ->setTelephone($object->getTelephone())
            ;
        }

        $veterinaryPracticeReport = new VeterinaryPracticeReport();
        $veterinaryPracticeReport->setVeterinaryPractice($veterinaryPractice);

        if ($veterinaryAccount && is_null($veterinaryAccount->getId())) {
            $veterinaryAccount->setConfirmationToken($this->tokenGenerator->get(32));
            $veterinaryAccount->setPasswordRequestedAt(new \DateTime());
            $this->eventDispatcher->dispatch(Events::NEW_ACCOUNT, new NewAccountEvent($veterinaryAccount, NewAccountEvent::TRIGGER_IMPORT_REPORT));
        }
        return $veterinaryPracticeReport;
    }

    /**
     * @return string
     */
    public function getObjectType() {
        return "Veterinary";
    }

}