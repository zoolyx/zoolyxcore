<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use phpDocumentor\Reflection\DocBlock\Tag;
use Zoolyx\CoreBundle\Entity\FileArray;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.file_array")
 */
class FilesParser extends XmlParser{

    const FILE_TAG = "FILE";

    /**
     * Constructor.
     *
     * @param FileParser $fileParser
     *
     * @DI\InjectParams({
     *  "fileParser" = @DI\Inject("zoolyx_core.xml_parser.file")
     * })
     */
    public function __construct(FileParser $fileParser)
    {
        $this->addTag(new Parser(self::FILE_TAG,'addFile', null, $fileParser, true));
    }

    public function getObject() {
        return new FileArray();
    }

    public function processObject($object) {
        /** @var FileArray $object */
        return $object;
    }
    public function getObjectType() {
        return 'File';
    }
}