<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\OriginalRequest;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.original_request")
 */
class OriginalRequestParser extends XmlParser{

    const REQUEST_TAG = 'RQ';
    const REQUEST_ID = 'ID';
    const PRACTICE_REF_TAG = "PRACTICEREF";
    const FILES_TAG = 'FILES';

    /**
     * Constructor.
     *
     * @param FilesParser $filesParser
     * @param StringParser $stringParser
     *
     * @DI\InjectParams({
     *  "filesParser" = @DI\Inject("zoolyx_core.xml_parser.file_array"),
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string")
     * })
     */
    public function __construct(FilesParser $filesParser, StringParser $stringParser)
    {

        $this->addAttribute(new Parser(self::REQUEST_ID,'setRequestId', null, $stringParser, true));
        $this->addTag(new Parser(self::PRACTICE_REF_TAG,'setPracticeReference', null, $stringParser, true));
        $this->addTag(new Parser(self::FILES_TAG,'setFiles', null, $filesParser));
    }

    public function getObject() {
        $originalRequest = new OriginalRequest();
        $originalRequest->setCreatedOn(new \DateTime());
        return $originalRequest;
    }
    public function processObject($object) {
        return $object;
    }
    public function getObjectType() {
        return 'OriginalRequest';
    }
}