<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Pet;
use Zoolyx\CoreBundle\Entity\Repository\PetRepository;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;

/**
 * @DI\Service("zoolyx_core.xml_parser.pet")
 */
class PetParser extends XmlParser{

    const PET_ID = "ID";
    const NAME_TAG = "NAME";
    const SPECIES_CODE_TAG = "SPECIES_CODE";
    const SPECIES_TAG = "SPECIES";
    const BIRTH_DATE_TAG = "BIRTH_DATE";
    const BIRTH_DATE_2_TAG = "BIRTHDATE";
    const GENDER_TAG = "GENDER";
    const BREED_TAG = "BREED";
    const BREED_CODE_TAG = "BREED_CODE";
    const CHIP_NBR_TAG = "CHIPID";
    const TATTOO_NBR_TAG = "TATTOOID";
    const PEDIGREE_NBR_TAG = "PEDIGREEID";

    /** @var  PetRepository */
    private $petRepository;

    /**
     * Constructor.
     *
     * @param PetRepository $petRepository
     * @param StringParser $stringParser
     * @param DateTimeParser $dateTimeParser
     *
     * @DI\InjectParams({
     *  "petRepository" = @DI\Inject("zoolyx_core.repository.pet"),
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string"),
     *  "dateTimeParser" = @DI\Inject("zoolyx_core.xml_parser.datetime")
     * })
     */
    public function __construct(PetRepository $petRepository, StringParser $stringParser, DateTimeParser $dateTimeParser)
    {
        $this->petRepository = $petRepository;
        $this->addAttribute(new Parser(self::PET_ID,'setPetId', null, $stringParser, true));
        $this->addTag(new Parser(self::NAME_TAG,'setName', null, $stringParser, true));
        $this->addTag(new Parser(self::SPECIES_CODE_TAG,'setSpeciesCode', null, $stringParser, true));
        $this->addTag(new Parser(self::SPECIES_TAG,'setSpeciesCode', null, $stringParser, true));
        $this->addTag(new Parser(self::BIRTH_DATE_TAG,'setBirthDate', null, $dateTimeParser, true));
        $this->addTag(new Parser(self::BIRTH_DATE_2_TAG,'setBirthDate', null, $dateTimeParser, true));
        $this->addTag(new Parser(self::GENDER_TAG,'setGender', null, $stringParser, true));
        $this->addTag(new Parser(self::BREED_TAG,'setBreed', null, $stringParser, true));
        $this->addTag(new Parser(self::BREED_CODE_TAG,'setBreedCode', null, $stringParser, true));
        $this->addTag(new Parser(self::CHIP_NBR_TAG,'setChipNbr', null, $stringParser, true));
        $this->addTag(new Parser(self::TATTOO_NBR_TAG,'setTattooNbr', null, $stringParser, true));
        $this->addTag(new Parser(self::PEDIGREE_NBR_TAG,'setPedigreeNbr', null, $stringParser, true));
    }

    /**
     * @return Pet
     */
    public function getObject() {
        return new Pet();
    }

    /**
     * @param Pet $object
     * @return Pet
     */
    public function processObject($object) {
        /** @var Pet $pet */
        $pet = $this->petRepository->findOneBy(array('petId' => $object->getPetId()));

        if ($pet) {
            $pet->setName($object->getName());
            $pet->setSpeciesCode($object->getSpeciesCode());
            $pet->setBirthDate($object->getBirthDate());
            $pet->setGender($object->getGender());
            $pet->setBreedCode($object->getBreedCode());
            $pet->setBreed($object->getBreed());
            $pet->setChipNbr($object->getChipNbr());
            $pet->setTattooNbr($object->getTattooNbr());
            $pet->setPedigreeNbr($object->getPedigreeNbr());
        }
        //var_dump($object); die();
        return is_null($pet) ? $object : $pet;
    }

    /**
     * @return string
     */
    public function getObjectType() {
        return 'Pet';
    }
}