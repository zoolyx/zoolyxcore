<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\AccountBundle\Forms\Account\Account;
use Zoolyx\CoreBundle\Entity\Owner;
use Zoolyx\CoreBundle\Entity\Repository\OwnerRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Model\Account\AccountManager;
use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlParser;
use Zoolyx\CoreBundle\Model\XmlParser\ObjectParser\Owner as OwnerLocal;

/**
 * @DI\Service("zoolyx_core.xml_parser.owner")
 */
class OwnerParser extends XmlParser{

    const OWNER_ID = "ID";
    const FIRST_NAME_TAG = "FIRSTNAME";
    const LAST_NAME_TAG = "LASTNAME";
    const STREET_TAG = "STREET";
    const ZIP_CODE_TAG = "ZIPCODE";
    const CITY_TAG = "CITY";
    const EMAIL_TAG = "EMAIL";
    const COUNTRY_TAG = "COUNTRY";
    const TELEPHONE_TAG = "TEL";
    const LANGUAGE_TAG = "LANGUAGE";

    /** @var OwnerRepository */
    private $ownerRepository;

    /** @var AccountManager */
    private $accountManager;


    /**
     * Constructor.
     *
     * @param AccountManager $accountManager
     * @param OwnerRepository $ownerRepository
     * @param StringParser $stringParser
     *
     * @DI\InjectParams({
     *  "accountManager" = @DI\Inject("zoolyx_core.account.manager"),
     *  "ownerRepository" = @DI\Inject("zoolyx_core.repository.owner"),
     *  "stringParser" = @DI\Inject("zoolyx_core.xml_parser.string")
     * })
     */
    public function __construct(AccountManager $accountManager, OwnerRepository $ownerRepository, StringParser $stringParser) {
        $this->accountManager = $accountManager;
        $this->ownerRepository = $ownerRepository;
        $this->addAttribute(new Parser(self::OWNER_ID,'setLimsId', null, $stringParser, true));
        $this->addTag(new Parser(self::FIRST_NAME_TAG,'setFirstName', null, $stringParser, true));
        $this->addTag(new Parser(self::LAST_NAME_TAG,'setLastName', null, $stringParser, true));
        $this->addTag(new Parser(self::STREET_TAG,'setStreet', null, $stringParser, true));
        $this->addTag(new Parser(self::ZIP_CODE_TAG,'setZipCode', null, $stringParser, true));
        $this->addTag(new Parser(self::CITY_TAG,'setCity', null, $stringParser, true));
        $this->addTag(new Parser(self::COUNTRY_TAG,'setCountry', null, $stringParser, true));
        $this->addTag(new Parser(self::TELEPHONE_TAG,'setTelephone', null, $stringParser, true));
        $this->addTag(new Parser(self::LANGUAGE_TAG,'setLanguage', null, $stringParser, true));
        $this->addTag(new Parser(self::EMAIL_TAG,'setEmail', null, $stringParser, true));
    }

    /**
     * @return OwnerLocal
     */
    public function getObject() {
        return new OwnerLocal();
    }

    /**
     * @param OwnerLocal $object
     * @return Owner
     */
    public function processObject($object) {
        if (is_null($object->getLimsId())) {
            return null;
        }

        $ownerAccount = null;
        if ($object->getEmail()) {
            $ownerAccount = $this->accountManager->loadAccount($object->getEmail());
            if (!$ownerAccount) {
                $ownerAccount = new User();
                $ownerAccount->setEmail($object->getEmail())
                    ->setUsername($object->getEmail())
                    ->setPlainPassword("tobechanged")
                    ->setReportFrequency(Account::FREQUENCY_ONLY_COMPLETED)
                    ->setLanguage($object->getLanguage())
                    ->setEnabled(false);
                $this->accountManager->setAccount($ownerAccount);
            }
            if (!$ownerAccount->hasRole("ROLE_OWNER")) {
                $ownerAccount->addRole("ROLE_OWNER");
            }
        }
        /** @var Owner $owner */
        $owner = $this->ownerRepository->findOneBy(array("limsId"=>$object->getLimsId()));
        if (is_null($owner)) {
            $owner = new Owner();
            $owner->setLimsId($object->getLimsId());
        }
        $owner->setFirstName($object->getFirstName());
        $owner->setLastName($object->getLastName());
        $owner->setStreet($object->getStreet());
        $owner->setZipCode($object->getZipCode());
        $owner->setCity($object->getCity());
        $owner->setCountry($object->getCountry());
        $owner->setTelephone($object->getTelephone());
        $owner->setLanguage($object->getLanguage());
        $owner->setAccount($ownerAccount);

        return $owner;
    }

    /**
     * @return string
     */
    public function getObjectType() {
        return "Owner";
    }
}