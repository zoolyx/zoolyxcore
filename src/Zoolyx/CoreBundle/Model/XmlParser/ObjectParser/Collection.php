<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 07.07.16
 * Time: 12:22
 */

namespace Zoolyx\CoreBundle\Model\XmlParser\ObjectParser;


class Collection {

    /** @var array */
    private $elements = [];

    public function getElements() {
        return $this->elements;
    }
    public function add($object) {
        $this->elements[] = $object;
    }
} 