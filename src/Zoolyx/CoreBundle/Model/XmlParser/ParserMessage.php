<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 06.03.16
 * Time: 12:35
 */

namespace Zoolyx\CoreBundle\Model\XmlParser;


class ParserMessage {

    /** @var  string */
    private $type;
    /** @var  string */
    private $objectType;

    /** @var  string */
    private $message;

    public function __construct($type, $objectType, $message) {
        $this->type = $type;
        $this->objectType = $objectType;
        $this->message = $message;
    }
    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getObjectType() {
        return $this->objectType;
    }

    /**
     * @param $type
     * @return ParserMessage
     */
    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * @param $message
     * @return ParserMessage
     */
    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }
} 