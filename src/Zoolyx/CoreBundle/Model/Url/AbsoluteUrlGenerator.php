<?php
namespace Zoolyx\CoreBundle\Model\Url;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

/**
 * @DI\Service("zoolyx_core.generator.absolute_url")
 */
class AbsoluteUrlGenerator {

    /** @var string */
    private $serverUrl;
    /** @var string */
    private $serverBaseUrl;

    /** @var Router */
    private $router;

    /**
     * Constructor.
     *
     * @param Router $router
     * @param string $serverUrl
     * @param string $serverBaseUrl
     *
     * @DI\InjectParams({
     *  "router" = @DI\Inject("router"),
     *  "serverUrl" = @DI\Inject("%server_url%"),
     *  "serverBaseUrl" = @DI\Inject("%server_base_url%")
     * })
     */
    public function __construct(Router $router, $serverUrl, $serverBaseUrl)
    {
        $this->router = $router;
        $this->serverUrl = $serverUrl;
        $this->serverBaseUrl = $serverBaseUrl;
    }

    public function generate($path, $parameters = array()) {
        $baseUrl = $this->router->getContext()->getBaseUrl() == "" ? $this->serverUrl : $this->serverBaseUrl;
        $url = $baseUrl . $this->router->generate($path, $parameters);
        return $url;
    }
} 