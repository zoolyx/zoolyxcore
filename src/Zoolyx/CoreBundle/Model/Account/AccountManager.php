<?php

namespace Zoolyx\CoreBundle\Model\Account;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Repository\UserRepository;
use Zoolyx\CoreBundle\Entity\User;

/**
 * @DI\Service("zoolyx_core.account.manager")
 */

class AccountManager {

    /** @var User */
    private $account = null;

    /**
     * Constructor.
     *
     * @param UserRepository $userRepository
     *
     * @DI\InjectParams({
     *  "userRepository" = @DI\Inject("zoolyx_core.repository.user")
     * })
     */
    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    public function loadAccount($email) {
        if (!is_null($this->account) && $this->account->getEmail() == $email) {
            return $this->account;
        }
        return $this->userRepository->findOneBy(array("email"=>$email));

    }

    public function setAccount(User $account) {
        $this->account = $account;
    }

} 