<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 08.10.16
 * Time: 12:43
 */
namespace Zoolyx\CoreBundle\Model\XmlBuilder\ServiceBuilder;

use Zoolyx\CoreBundle\Model\Request\Object\RequestFacturation;
use Zoolyx\CoreBundle\Model\Request\Object\RequestForm;
use Zoolyx\CoreBundle\Model\Request\Object\RequestPet;
use Zoolyx\CoreBundle\Model\Request\Object\RequestProfile;
use Zoolyx\CoreBundle\Model\Request\Object\RequestSample;
use Zoolyx\CoreBundle\Model\Request\Object\RequestSampleType;
use Zoolyx\CoreBundle\Model\Request\Object\RequestUser;

abstract class RequestServiceBuilder extends ServiceBuilder {

    /** @var string */
    private $requestId;

    /** @var string */
    private $requestBy = null;

    /** @var string */
    private $practiceReference;

    /** @var RequestSample */
    private $sample;

    /** @var RequestUser */
    private $veterinary;

    /** @var RequestUser */
    private $owner;

    /** @var RequestFacturation */
    private $facturation;


    public function __construct() {
        parent::__construct();
    }

    /**
     * @param string $requestId
     * @return $this
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;
        return $this;
    }

    /**
     * @param $email
     * @return $this
     */
    public function setRequestBy($email) {
        $this->requestBy = $email;
        return $this;
    }

    /**
     * @param $practiceReference
     * @return $this
     */
    public function setPracticeReference($practiceReference) {
        $this->practiceReference = $practiceReference;
        return $this;
    }

    /**
     * @param RequestUser $owner
     * @return mixed
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @param RequestUser $veterinary
     * @return $this
     */
    public function setVeterinary($veterinary)
    {
        $this->veterinary = $veterinary;
        return $this;
    }

    /**
     * @param RequestFacturation $facturation
     * @return $this
     */
    public function setFacturation($facturation) {
        $this->facturation = $facturation;
        return $this;
    }

    /**
     * @param RequestSample $sample
     * @return $this
     */
    public function setSample($sample)
    {
        $this->sample = $sample;
        return $this;
    }

    public function getContent() {
        $veterinaryDoc = $this->service->createElement('da');
        if (!is_null($this->veterinary))  {
            $veterinaryDoc->setAttribute('id',$this->veterinary ? $this->veterinary->getLimsId() : "");
            $veterinaryDoc->appendChild($this->makeDoc('firstName',$this->veterinary->getFirstName()));
            $veterinaryDoc->appendChild($this->makeDoc('lastName',$this->veterinary->getLastName()));
            $veterinaryDoc->appendChild($this->makeDoc('email',$this->veterinary->getEmail()));
            $veterinaryDoc->appendChild($this->makeDoc('street',$this->veterinary->getStreet()));
            $veterinaryDoc->appendChild($this->makeDoc('zipCode',$this->veterinary->getZipCode()));
            $veterinaryDoc->appendChild($this->makeDoc('city',$this->veterinary->getCity()));
            $veterinaryDoc->appendChild($this->makeDoc('country',$this->veterinary->getCountry()));
            $veterinaryDoc->appendChild($this->makeDoc('tel',$this->veterinary->getTelephone()));
            $veterinaryDoc->appendChild($this->makeDoc('language',$this->veterinary->getLanguage()));
        }

        $ownerDoc = $this->service->createElement('ad');
        $ownerDoc->setAttribute('id',$this->owner->getLimsId());
        $ownerDoc->appendChild($this->makeDoc('extid',$this->owner->getExternalId()));
        $ownerDoc->appendChild($this->makeDoc('firstName',$this->owner->getFirstName()));
        $ownerDoc->appendChild($this->makeDoc('lastName',$this->owner->getLastName()));
        $ownerDoc->appendChild($this->makeDoc('email',$this->owner->getEmail()));
        $ownerDoc->appendChild($this->makeDoc('street',$this->owner->getStreet()));
        $ownerDoc->appendChild($this->makeDoc('zipCode',$this->owner->getZipCode()));
        $ownerDoc->appendChild($this->makeDoc('city',$this->owner->getCity()));
        $ownerDoc->appendChild($this->makeDoc('country',$this->owner->getCountry()));
        $ownerDoc->appendChild($this->makeDoc('tel',$this->owner->getTelephone()));
        $ownerDoc->appendChild($this->makeDoc('language',$this->owner->getLanguage()));

        $faDoc = $this->service->createElement('fa');
        $faDoc->setAttribute('id',$this->owner->getLimsId());
        if (!is_null($this->facturation))  {
            $faDoc->setAttribute('id', $this->facturation->getFaId());
            $faDoc->setAttribute('tp', $this->facturation->invoiceForOwner() ? 'ad' : 'da');
            $faDoc->appendChild($this->makeDoc('vat',$this->facturation->getVatNumber()));
        }

        /** @var RequestPet $pet */
        $pet = $this->sample->getPet();
        $petDoc = $this->service->createElement('pe');
        $petDoc->setAttribute('id',$pet->getLimsId());
        $petDoc->appendChild($this->makeDoc('extid',$pet->getExternalId()));
        $petDoc->appendChild($this->makeDoc('chipId',$pet->getChipNbr()));
        $petDoc->appendChild($this->makeDoc('species',$pet->getSpeciesCode()));
        $petDoc->appendChild($this->makeDoc('breedId',$pet->getBreedCode()));
        $petDoc->appendChild($this->makeDoc('breed',$pet->getOriginalBreedName()));
        $petDoc->appendChild($this->makeDoc('gender',$pet->getGender()));
        $petDoc->appendChild($this->makeDoc('name',$pet->getName()));
        $petDoc->appendChild($this->makeDoc('birthDate',$pet->getBirthDate() ? $pet->getBirthDate()->format('Y/m/d') : ''));

        $sampleDoc = $this->service->createElement('sc');
        $sampleDoc->setAttribute('id',$this->sample->getCode());
        $sampleDoc->appendChild($this->makeDoc('sample',1));
        $sampleDoc->appendChild($petDoc);

        /** @var RequestProfile $requestProfile */
        foreach ($this->sample->getProfiles() as $requestProfile) {
            $profileReference = $requestProfile->getCode();

            $profileDoc = $this->service->createElement('pp');
            $profileDoc->setAttribute('id',$profileReference);
            $sampleDoc->appendChild($profileDoc);

            $stListDoc = $this->service->createElement('stlist');
            $profileDoc->appendChild($stListDoc);

            $sampleTypes = $requestProfile->getSampleTypes();

            /** @var RequestSampleType $sampleType */
            foreach ($sampleTypes as $sampleType) {
                $stDoc = $this->service->createElement('st');
                $stDoc->setAttribute('id',$sampleType->getReference());
                $stListDoc->appendChild($stDoc);
            }
        }

        $formsDoc = $this->service->createElement('forms');
        /** @var RequestForm $requestForm */
        foreach ($this->sample->getForms() as $requestForm)
        {
            $formDoc = $this->service->createElement('form');
            $formDoc->setAttribute('id',$requestForm->getName());
            $formsDoc->appendChild($formDoc);

            foreach ($requestForm->getValues() as $name => $value) {
                if ($value)
                    $formDoc->appendChild($this->makeDoc($name,$value));
            }
        }

        $requestDoc = $this->service->createElement('rq');
        $requestDoc->setAttribute('id',$this->requestId);
        $requestDoc->appendChild($this->makeDoc('practiceRef',$this->practiceReference));
        if ($this->requestBy)
            $requestDoc->appendChild($this->makeDoc('requestBy',$this->requestBy));
        $requestDoc->appendChild($veterinaryDoc);
        $requestDoc->appendChild($ownerDoc);
        $requestDoc->appendChild($faDoc);
        $requestDoc->appendChild($sampleDoc);
        $requestDoc->appendChild($formsDoc);

        return $requestDoc;
    }


} 