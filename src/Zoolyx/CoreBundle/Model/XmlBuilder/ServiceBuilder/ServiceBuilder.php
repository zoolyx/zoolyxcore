<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 08.10.16
 * Time: 12:43
 */
namespace Zoolyx\CoreBundle\Model\XmlBuilder\ServiceBuilder;

use DOMDocument;

abstract class ServiceBuilder implements ServiceBuilderInterface {

    /** @var string  */
    private $serviceName = "";

    protected $service;

    public function __construct() {
        $this->service = new DOMDocument("1.0","UTF-8");
    }

    /**
     * @param string $serviceName
     * @return $this
     */
    protected function setServiceName($serviceName)
    {
        $this->serviceName = $serviceName;
        return $this;
    }


    public function getDoc() {
        $serviceContainer = $this->service->createElement('service');
        $serviceContainer->appendChild($this->getContent());
        $serviceContainer->setAttribute('type',$this->serviceName);
        $this->service->appendChild($serviceContainer);

        return $this->service;
    }

    protected function makeDoc($name, $value) {
        $doc = $this->service->createElement($name);
        $doc->nodeValue = $value;
        return $doc;
    }
} 