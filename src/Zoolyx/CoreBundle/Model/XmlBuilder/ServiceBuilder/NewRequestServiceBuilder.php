<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 08.10.16
 * Time: 12:43
 */
namespace Zoolyx\CoreBundle\Model\XmlBuilder\ServiceBuilder;

class NewRequestServiceBuilder extends RequestServiceBuilder {

    public function __construct() {
        parent::__construct();
        $this->setServiceName('create_request');
    }
} 