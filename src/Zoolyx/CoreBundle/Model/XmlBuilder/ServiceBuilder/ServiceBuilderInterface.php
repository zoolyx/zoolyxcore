<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 08.10.16
 * Time: 12:43
 */
namespace Zoolyx\CoreBundle\Model\XmlBuilder\ServiceBuilder;

use DOMDocument;


interface ServiceBuilderInterface {

    /**
     * @return DomDocument
     */
    public function getContent();

} 