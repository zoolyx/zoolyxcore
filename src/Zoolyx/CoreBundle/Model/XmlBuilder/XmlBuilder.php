<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 08.10.16
 * Time: 12:43
 */
namespace Zoolyx\CoreBundle\Model\XmlBuilder;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("zoolyx_core.xml_build.builder")
 */

class XmlBuilder {

    public function build($requestId, $language) {
        $xml = '<?xml version="1.0" encoding="utf-8"?>';
        $xml.= '<service type="translate">';
        $xml.= '<rq id="'.$requestId.'">';
        $xml.= '<lang>'.$language.'</lang>';
        $xml.= '</rq>';
        $xml.= '</service>';
        return $xml;
    }

} 