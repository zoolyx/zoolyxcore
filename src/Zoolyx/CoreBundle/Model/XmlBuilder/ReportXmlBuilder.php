<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 08.10.16
 * Time: 12:43
 */
namespace Zoolyx\CoreBundle\Model\XmlBuilder;
use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Owner;
use Zoolyx\CoreBundle\Entity\ParameterProfile;
use Zoolyx\CoreBundle\Entity\Pet;
use Zoolyx\CoreBundle\Entity\Practice;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\Sample;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;

/**
 * @DI\Service("zoolyx_core.xml_build.report_xml_builder")
 */

class ReportXmlBuilder {

    /** @var Encapsulator  */
    private $encapsulator;

    /**
     * Constructor.
     *
     * @param Encapsulator $encapsulator
     *
     * @DI\InjectParams({
     *  "encapsulator" = @DI\Inject("zoolyx_core.xml_build.encapsulator")
     * })
     */
    public function __construct(Encapsulator $encapsulator) {
        $this->encapsulator = $encapsulator;
    }

    /**
     * @param Report $report
     * @return string
     */
    public function getXml($report) {
        /** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = $report->getVeterinaryPractice();
        /** @var Veterinary $veterinary */
        $veterinary = $veterinaryPractice->getVeterinary();
        /** @var Practice $practice */
        $practice = $veterinaryPractice->getPractice();
        $veterinaryXml = $this->encapsulator->get('lastname',$veterinary->getLastName());
        $veterinaryXml.= $this->encapsulator->get('firstname',$veterinary->getFirstName());
        $veterinaryXml.= $this->encapsulator->get('street',$practice->getStreet());
        $veterinaryXml.= $this->encapsulator->get('zipcode',$practice->getZipCode());
        $veterinaryXml.= $this->encapsulator->get('city',$practice->getCity());
        $veterinaryXml.= $this->encapsulator->get('country',$practice->getCountry());
        $veterinaryXml.= $this->encapsulator->get('language',$veterinary->getLanguage());
        $veterinaryXml.= $this->encapsulator->get('email',$veterinary->getEmail());

        /** @var Owner $owner */
        $owner = $report->getOwner();
        $ownerXml = $this->encapsulator->get('lastname',$owner->getLastName());
        $ownerXml.= $this->encapsulator->get('firstname',$owner->getFirstName());
        $ownerXml.= $this->encapsulator->get('street',$owner->getStreet());
        $ownerXml.= $this->encapsulator->get('zipcode',$owner->getZipCode());
        $ownerXml.= $this->encapsulator->get('city',$owner->getCity());
        $ownerXml.= $this->encapsulator->get('country',$owner->getCountry());
        $ownerXml.= $this->encapsulator->get('language',$owner->getLanguage());
        $ownerXml.= $this->encapsulator->get('email',$owner->getEmail());

        $samplesXml = '';
        /** @var Sample $sample */
        foreach ($report->getDefaultVersion()->getSamples() as $sample) {
            /** @var Pet $pet */
            $pet = $sample->getPet();
            $petXml = $this->encapsulator->get('name',$pet->getName());
            $petXml.= $this->encapsulator->get('species_code',$pet->getSpeciesCode());
            $petXml.= $this->encapsulator->get('gender',$pet->getGender());
            $petXml.= $this->encapsulator->get('birth_date',$pet->getBirthDate()->format('Y-m-d'), array('fmt'=>'yyy-mm-dd'));
            $petXml.= $this->encapsulator->get('breed',$pet->getBreed());
            $petXml.= $this->encapsulator->get('chipid',$pet->getChipNbr());

            $parameterProfilesXml = '';
            /** @var ParameterProfile $parameterProfile */
            foreach ($sample->getParameterProfiles() as $parameterProfile) {
                $parameterProfileXml = $this->encapsulator->get('pp','',array('id'=>$parameterProfile->getReference()));
                $parameterProfilesXml.= $parameterProfileXml;
            }

            $sampleXml = $this->encapsulator->get('pe',$petXml, array('id'=>$pet->getPetId())) . "\n";
            $sampleXml.= $parameterProfilesXml;

            $samplesXml.= $this->encapsulator->get('sc',$sampleXml, array('id'=>$sample->getCode())) . "\n";
        }

        $request = $this->encapsulator->get('practiceref',$report->getPracticeReference()) . "\n";
        $request.= $this->encapsulator->get('da',$veterinaryXml,array('id'=>$veterinaryPractice->getLimsId())) . "\n";
        $request.= $this->encapsulator->get('ad',$ownerXml,array('id'=>$owner->getId())) . "\n";
        $request.= $samplesXml;
        $xml = $this->encapsulator->get('rq',$request,array('id'=>'kmsh+' . $report->getPracticeReference()));
        return $xml;
    }

} 