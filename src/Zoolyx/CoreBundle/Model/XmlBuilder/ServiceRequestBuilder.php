<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 08.10.16
 * Time: 12:43
 */
namespace Zoolyx\CoreBundle\Model\XmlBuilder;
use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Report;

/**
 * @DI\Service("zoolyx_core.xml_build.service_request")
 */

class ServiceRequestBuilder {

    /** @var ReportXmlBuilder $reportXmlBuilder */
    private $reportXmlBuilder;

    /**
     * Constructor.
     *
     * @param ReportXmlBuilder $reportXmlBuilder
     *
     * @DI\InjectParams({
     *  "reportXmlBuilder" = @DI\Inject("zoolyx_core.xml_build.report_xml_builder")
     * })
     */
    public function __construct(ReportXmlBuilder $reportXmlBuilder) {
        $this->reportXmlBuilder = $reportXmlBuilder;
    }

    public function build($serviceName, $content) {
        $xml = '<?xml version="1.0" encoding="utf-8"?>';
        $xml.= '<service type="'.$serviceName.'">';
        $xml.= $content;
        $xml.= '</service>';
        return $xml;
    }

    /**
     * @param string $serviceName
     * @param Report $report
     * @return string
     */
    public function buildReport($serviceName, Report $report) {
        return $this->build($serviceName,$this->reportXmlBuilder->getXml($report));
    }

} 