<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 08.10.16
 * Time: 12:43
 */
namespace Zoolyx\CoreBundle\Model\XmlBuilder;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("zoolyx_core.xml_build.encapsulator")
 */

class Encapsulator {

    /**
     * @param string $tag
     * @param string $content
     * @param array $attributes
     * @return string
     */
    public function get($tag, $content, $attributes = array()) {
        return $this->getOpeningTag($tag, $attributes) . $content . $this->getClosingTag($tag);
    }

    private function getOpeningTag($tag, $attributes) {
        $xmlAttributes = array();
        foreach($attributes as $key=>$value) {
            if ($value != '') {
                $xmlAttributes[] = $key . '=' . '"'.$value.'"';
            }
        }
        $xmlAttributesString = (count($xmlAttributes)>0 ? ' ' : '') . implode(' ',$xmlAttributes);
        return '<'.$tag.$xmlAttributesString.'>';
    }
    private function getClosingTag($tag) {
        return '</'.$tag.'>';
    }

} 