<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 11.03.16
 * Time: 21:17
 */
namespace Zoolyx\CoreBundle\Model\Wiki;

use Doctrine\Common\Collections\Collection;

class ParameterPage extends WikiPage {

    /** @var array */
    private $species;

    /** @var string */
    private $description;

    /** @var string */
    private $descriptionShort;

    /** @var string */
    private $category;

    /** @var string */
    private $loinc;

    /** @var string */
    private $method;

    /** @var int */
    private $delay;

    /** @var string */
    private $unit;

    /** @var string */
    private $alternativeUnit;

    /** @var string */
    private $conversionFactor;

    /** @var int */
    private $decimals;

    /** @var Collection */
    private $sampleTypes = array();


    /**
     * @return string
     */
    public function getSpecies()
    {
        return $this->species;
    }

    /**
     * @param string $species
     * @return $this
     */
    public function setSpecies($species)
    {
        $this->species = $species;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescriptionShort()
    {
        return $this->descriptionShort;
    }

    /**
     * @param string $descriptionShort
     * @return $this
     */
    public function setDescriptionShort($descriptionShort)
    {
        $this->descriptionShort = $descriptionShort;
        return $this;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string
     */
    public function getLoinc()
    {
        return $this->loinc;
    }

    /**
     * @param string $loinc
     * @return $this
     */
    public function setLoinc($loinc)
    {
        $this->loinc = $loinc;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return $this
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return int
     */
    public function getDelay()
    {
        return $this->delay;
    }

    /**
     * @param int $delay
     * @return $this
     */
    public function setDelay($delay)
    {
        $this->delay = $delay;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     * @return $this
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
        return $this;
    }


    /**
     * @return string
     */
    public function getAlternativeUnit()
    {
        return $this->alternativeUnit;
    }

    /**
     * @param string $alternativeUnit
     * @return $this
     */
    public function setAlternativeUnit($alternativeUnit)
    {
        $this->alternativeUnit = $alternativeUnit;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasAlternativeUnit() {
        return $this->alternativeUnit != '';
    }

    /**
     * @return string
     */
    public function getConversionFactor()
    {
        return $this->conversionFactor;
    }

    /**
     * @param string $conversionFactor
     * @return $this
     */
    public function setConversionFactor($conversionFactor)
    {
        $this->conversionFactor = $conversionFactor;
        return $this;
    }

    /**
     * @return int
     */
    public function getDecimals()
    {
        return $this->decimals;
    }

    /**
     * @param int $decimals
     * @return $this
     */
    public function setDecimals($decimals)
    {
        $this->decimals = $decimals;
        return $this;
    }


    /**
     * @return Collection
     */
    public function getSampleTypes()
    {
        return $this->sampleTypes;
    }

    /**
     * @param Collection $sampleTypes
     * @return $this
     */
    public function setSampleTypes($sampleTypes)
    {
        $this->sampleTypes = $sampleTypes;
        return $this;
    }


} 