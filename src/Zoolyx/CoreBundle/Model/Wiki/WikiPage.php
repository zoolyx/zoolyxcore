<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 11.03.16
 * Time: 21:17
 */
namespace Zoolyx\CoreBundle\Model\Wiki;

class WikiPage {

    /** @var string */
    private $id;

    /** @var string */
    private $language;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }



} 