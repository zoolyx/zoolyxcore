<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 11.03.16
 * Time: 21:17
 */
namespace Zoolyx\CoreBundle\Model\Wiki;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\ParameterBase;
use Zoolyx\CoreBundle\Entity\ParameterBaseMethod;
use Zoolyx\CoreBundle\Entity\ParameterGroupBase;
use Zoolyx\CoreBundle\Entity\ReferenceRange;
use Zoolyx\CoreBundle\Entity\Repository\ParameterGroupBaseRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileCommentRepository;
use Zoolyx\CoreBundle\Entity\Repository\SpeciesRepository;
use Zoolyx\CoreBundle\Model\Pet\PetManager;
use Zoolyx\CoreBundle\Model\Report\Parameter\Reference\ReferenceCommentFetcher;

/**
 * @DI\Service("zoolyx_core.wiki.parameter_page_provider")
 */
class ParameterPageProvider {

    private $languages = array('nl','fr');

    /** @var ParameterGroupBaseRepository */
    private $parameterGroupBaseRepository;

    /** @var SpeciesRepository */
    private $speciesRepository;

    /** @var PetManager */
    private $petManager;

    /** ParameterProfileCommentRepository */
    private $parameterProfileCommentRepository;

    /** @var ReferenceCommentFetcher */
    private $referenceCommentFetcher;

    /**
     * Constructor.
     *
     * @param ParameterGroupBaseRepository $parameterGroupBaseRepository
     * @param ParameterProfileCommentRepository $parameterProfileCommentRepository
     * @param SpeciesRepository $speciesRepository
     * @param PetManager $petManager
     * @param ReferenceCommentFetcher $referenceCommentFetcher
     *
     * @DI\InjectParams({
     *  "parameterGroupBaseRepository" = @DI\Inject("zoolyx_core.repository.parameter_group_base"),
     *  "parameterProfileCommentRepository" = @DI\Inject("zoolyx_core.repository.parameter_profile_comment"),
     *  "speciesRepository" = @DI\Inject("zoolyx_core.repository.species"),
     *  "petManager" = @DI\Inject("zoolyx_core.pet.manager"),
     *  "referenceCommentFetcher" = @DI\Inject("zoolyx_core.report.parameter_reference_comment_fetcher")
     * })
     */
    public function __construct(ParameterGroupBaseRepository $parameterGroupBaseRepository, ParameterProfileCommentRepository $parameterProfileCommentRepository, SpeciesRepository $speciesRepository, PetManager $petManager, ReferenceCommentFetcher $referenceCommentFetcher) {
        $this->parameterGroupBaseRepository = $parameterGroupBaseRepository;
        $this->parameterProfileCommentRepository = $parameterProfileCommentRepository;
        $this->speciesRepository = $speciesRepository;
        $this->petManager = $petManager;
        $this->referenceCommentFetcher = $referenceCommentFetcher;
    }

    /**
     * @param array $parameters
     * @return array
     */
    public function getPages($parameters) {
        $parameterPages = array();
        /** @var ParameterBase $parameter */
        foreach ($parameters as $parameter) {
            if (!$parameter) continue;

            foreach ($this->languages as $language) {
                $parameterPages[] = $this->getPage($parameter, $language);
            }
        }
        return $parameterPages;
    }

    /**
     * @param ParameterBase $parameterBase
     * @param string $language
     * @return ParameterPage
     */
    public function getPage($parameterBase, $language) {
        $parameterGroupDescription = '';
        /** @var ParameterGroupBase $parameterGroup */
        $parameterGroup = $this->parameterGroupBaseRepository->findOneBy(array('parameterGroupId' => $parameterBase->getParameterGroup()));
        if ($parameterGroup)
            $parameterGroupDescription = ucfirst(strtolower($parameterGroup->getDescription($language)->getDescription()));

        $methodDescription = '';
        /** @var ParameterBaseMethod $method */
        foreach ($parameterBase->getMethods() as $method) {
            if ($method->getLanguage() == $language) {
                $methodDescription = $method->getDescription();
            }
        }

        // build reference ranges
        $referenceRanges = array();
        /** @var ReferenceRange $referenceRange */
        foreach ($parameterBase->getReferenceRanges() as $referenceRange) {
            $referenceRanges[$referenceRange->getSpeciesCode()] = $referenceRange;
        }

        $speciesList = array();
        $speciesCodesString = $parameterBase->getSpeciesCodes();
        $speciesCodes = $speciesCodesString == '' ? array() : explode(',',$speciesCodesString);

        foreach ($speciesCodes as $speciesCode) {
            $speciesName = '';
            $species = $this->speciesRepository->findOneBy(array('speciesId' => $speciesCode));
            if ($species) {
                $speciesName = $species->getDescription($language)->getDescription();
            }
            $reference = '';
            if (isset($referenceRanges[$speciesCode])) {
                /** @var ReferenceRange $referenceRange */
                $referenceRange = $referenceRanges[$speciesCode];
                $reference = $referenceRange->getReferenceDisplay();
            }
            $speciesObject = new Species();
            $speciesObject
                ->setCode($speciesCode)
                ->setName($speciesName)
                ->setImage($this->petManager->getSpeciesNameForCode($speciesCode))
                ->setReference($reference)
            ;
            $speciesList[] = $speciesObject;
        }

        //sort speciesList
        usort($speciesList, array($this, "cmp"));

        $conversionFactor = str_replace(",",".",$parameterBase->getConversionFactor());

        $parameterPage = new ParameterPage();
        $parameterPage
            ->setId('PR_'.$parameterBase->getParameterId())
            ->setSpecies($speciesList)
            ->setLanguage($language)
            ->setDescription($parameterBase->getDescription($language)->getDescription())
            ->setDescriptionShort($parameterBase->getDescription2($language)->getDescription())
            ->setCategory($parameterGroupDescription)
            ->setLoinc($parameterBase->getLoinc())
            ->setMethod($methodDescription)
            ->setDelay($parameterBase->getDelay())
            ->setUnit($parameterBase->getUnit())
            ->setAlternativeUnit($parameterBase->getAlternativeUnit())
            ->setConversionFactor($conversionFactor)
            ->setDecimals($parameterBase->getDecimals())
            ->setSampleTypes($parameterBase->getSampleTypes());

        return $parameterPage;
    }

    /**
     * @param Species $a
     * @param Species $b
     * @return int
     */
    private function cmp(Species $a, Species $b)
    {
        return strcmp($a->getName(), $b->getName());
    }

}