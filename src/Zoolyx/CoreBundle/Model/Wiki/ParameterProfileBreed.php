<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 11.03.16
 * Time: 21:17
 */
namespace Zoolyx\CoreBundle\Model\Wiki;

class ParameterProfileBreed {

    /** @var string */
    private $code;

    /** @var string */
    private $description;

    /** @var string */
    private $url;

    /** @var bool */
    private $hasUrl = false;

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }


    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }


    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }
    /**
     * @return boolean
     */
    public function hasUrl()
    {
        return $this->hasUrl;
    }

    /**
     * @param boolean $hasUrl
     * @return $this
     */
    public function setHasUrl($hasUrl)
    {
        $this->hasUrl = $hasUrl;
        return $this;
    }



} 