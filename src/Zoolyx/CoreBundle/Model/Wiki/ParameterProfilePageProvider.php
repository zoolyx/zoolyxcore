<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 11.03.16
 * Time: 21:17
 */
namespace Zoolyx\CoreBundle\Model\Wiki;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Breed;
use Zoolyx\CoreBundle\Entity\ParameterBase;
use Zoolyx\CoreBundle\Entity\ParameterProfile;
use Zoolyx\CoreBundle\Entity\ParameterProfilePrice;
use Zoolyx\CoreBundle\Entity\Repository\ParameterGroupBaseRepository;
use Zoolyx\CoreBundle\Entity\Repository\SpeciesRepository;
use Zoolyx\CoreBundle\Entity\Species;
use Zoolyx\CoreBundle\Model\Pet\PetManager;

/**
 * @DI\Service("zoolyx_core.wiki.parameter_profile_page_provider")
 */
class ParameterProfilePageProvider {

    private $languages = array('nl','fr');

    /** @var ParameterGroupBaseRepository */
    private $parameterGroupBaseRepository;

    /** @var SpeciesRepository */
    private $speciesRepository;

    /** @var PetManager */
    private $petManager;


    /**
     * Constructor.
     *
     * @param ParameterGroupBaseRepository $parameterGroupBaseRepository
     * @param SpeciesRepository $speciesRepository
     * @param PetManager $petManager
     *
     * @DI\InjectParams({
     *  "parameterGroupBaseRepository" = @DI\Inject("zoolyx_core.repository.parameter_group_base"),
     *  "speciesRepository" = @DI\Inject("zoolyx_core.repository.species"),
     *  "petManager" = @DI\Inject("zoolyx_core.pet.manager")
     * })
     */
    public function __construct(ParameterGroupBaseRepository $parameterGroupBaseRepository, SpeciesRepository $speciesRepository, PetManager $petManager) {
        $this->parameterGroupBaseRepository = $parameterGroupBaseRepository;
        $this->speciesRepository = $speciesRepository;
        $this->petManager = $petManager;
    }

    /**
     * @param array $parameterProfiles
     * @return array
     */
    public function getPages($parameterProfiles) {
        $parameterProfilePages = array();
        /** @var ParameterProfile $parameterProfile */
        foreach ($parameterProfiles as $parameterProfile) {
            if (is_null($parameterProfile->getReservedFor())) {
                foreach ($this->languages as $language) {
                    $parameterProfilePages[] = $this->getPage($parameterProfile,$language);
                }
            }
        }
        return $parameterProfilePages;
    }

    /**
     * @param ParameterProfile $parameterProfile
     * @param string $language
     * @return ParameterPage
     */
    public function getPage($parameterProfile, $language)
    {
        $price = 0;
        /** @var ParameterProfilePrice $priceList */
        foreach ($parameterProfile->getPriceList() as $priceList) {
            if (is_null($priceList->getBilling()) || $priceList->getBilling() == '' ) {
                $price = floatval(str_replace(',', '.',$priceList->getValue()));
            }
        }

        $parameters = array();
        /** @var ParameterBase $parameter */
        foreach ($parameterProfile->getParameters() as $parameter) {
            $parameterProfileParameter = new ParameterProfileParameter();
            $parameterProfileParameter->setName('PR_'.$parameter->getParameterId());
            $parameterProfileParameter->setDescription($parameter->getDescription($language)->getDescription());
            $parameters[] = $parameterProfileParameter;
        }
        usort($parameters, array($this, "compare_parameter"));

        $speciesName = '';
        /** @var Species $species */
        $species = $this->speciesRepository->findOneBy(array('speciesId' => $parameterProfile->getSpecies()));
        if ($species) {
            $speciesName = $species->getDescription($language)->getDescription();
        }

        $breeds = array();
        /** @var Breed $breed */
        foreach ($parameterProfile->getBreeds() as $breed) {
            $codeParts = explode('_',$breed->getBreedId());
            $breedCode = $codeParts[1];
            $url = $parameterProfile->getSpecies() == 'H' && $breedCode != '*' ? $breed->getDescription($language) . '-' . $breedCode : '';

            $parameterProfileBreed = new ParameterProfileBreed();
            $parameterProfileBreed
                ->setCode($breedCode)
                ->setDescription($breed->getDescription($language))
                ->setUrl($url)
                ->setHasUrl($url != '')
            ;
            $breeds[] = $parameterProfileBreed;
        }
        usort($breeds, array($this, "compare_breed"));

        $parameterProfilePage = new ParameterProfilePage();
        $parameterProfilePage
            ->setId('PP_'.$parameterProfile->getReference())
            ->setSpecies($parameterProfile->getSpecies())
            ->setSpeciesName($speciesName)
            ->setSpeciesImageName($species ? $this->petManager->getSpeciesNameForCode($species->getSpeciesId()) : 'blank')
            ->setBreeds($breeds)
            ->setLanguage($language)
            ->setDescription($parameterProfile->getDescription($language)->getDescription())
            ->setCategory($parameterProfile->getCategory()->getDescription($language)->getDescription())
            ->setReference($parameterProfile->getReference())
            ->setPrice($price)
            ->setPriceInclusive(round(121*$price)/100)
            ->setParameters($parameters)
            ->setSampleTypes($parameterProfile->getSampleTypes())
        ;

        return $parameterProfilePage;
    }

    private function compare_breed(ParameterProfileBreed $a, ParameterProfileBreed $b) {
        return strcasecmp($a->getDescription(), $b->getDescription());
    }

    private function compare_parameter(ParameterProfileParameter $a, ParameterProfileParameter $b) {
        return strcasecmp($a->getDescription(), $b->getDescription());
    }

}