<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 11.03.16
 * Time: 21:17
 */
namespace Zoolyx\CoreBundle\Model\Wiki;

class ParameterProfilePage extends WikiPage {

    /** @var string */
    private $reference;

    /** @var string */
    private $species;

    /** @var string */
    private $speciesName;

    /** @var string */
    private $speciesImageName;

    /** @var array */
    private $breeds = array();

    /** @var string */
    private $description;

    /** @var string */
    private $category;

    /** @var float */
    private $price;

    /** @var float */
    private $priceInclusive;

    /** @var array */
    private $parameters = array();

    /** @var array */
    private $sampleTypes = array();

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpecies()
    {
        return $this->species;
    }

    /**
     * @param string $species
     * @return $this
     */
    public function setSpecies($species)
    {
        $this->species = $species;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpeciesName()
    {
        return $this->speciesName;
    }

    /**
     * @param string $speciesName
     * @return $this
     */
    public function setSpeciesName($speciesName)
    {
        $this->speciesName = $speciesName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpeciesImageName()
    {
        return $this->speciesImageName;
    }

    /**
     * @param string $speciesImageName
     * @return $this
     */
    public function setSpeciesImageName($speciesImageName)
    {
        $this->speciesImageName = $speciesImageName;
        return $this;
    }

    /**
     * @return array
     */
    public function getBreeds()
    {
        return $this->breeds;
    }

    /**
     * @param array $breeds
     * @return $this
     */
    public function setBreeds($breeds)
    {
        $this->breeds = $breeds;
        return $this;
    }


    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceInclusive()
    {
        return $this->priceInclusive;
    }

    /**
     * @param float $priceInclusive
     * @return $this
     */
    public function setPriceInclusive($priceInclusive)
    {
        $this->priceInclusive = $priceInclusive;
        return $this;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     * @return $this
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * @return array
     */
    public function getSampleTypes()
    {
        return $this->sampleTypes;
    }

    /**
     * @param array $sampleTypes
     * @return $this
     */
    public function setSampleTypes($sampleTypes)
    {
        $this->sampleTypes = $sampleTypes;
        return $this;
    }

} 