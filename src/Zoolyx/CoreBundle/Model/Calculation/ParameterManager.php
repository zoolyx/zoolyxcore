<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 09.03.16
 * Time: 10:45
 */
namespace Zoolyx\CoreBundle\Model\Calculation;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Parameter;
use Zoolyx\CoreBundle\Entity\ParameterBaseDescription;

/**
 * @DI\Service("zoolyx_core.calculation.parameter_manager")
 */
class ParameterManager {

    const LOW = 'L';
    const VERY_LOW = 'ZL';
    const HIGH = 'H';
    const VERY_HIGH = 'ZH';

    private $rootUrl = "";

    /** @var string */
    private $language;

    /** @var Parameter $parameter */
    private $parameter;

    /**
     * Constructor.
     *
     * @param string $rootUrl
     *
     * @DI\InjectParams({
     *  "rootUrl" = @DI\Inject("%root_url%")
     * })
     */

    public function __construct($rootUrl) {
        $this->rootUrl = $rootUrl;
    }

    /**
     * @param string $language
     * @return $this
     */
    public function setLanguage($language) {
        $this->language = $language;
        return $this;
    }

    /**
     * @param Parameter $parameter
     * @return $this
     */
    public function setParameter(Parameter $parameter) {
        $this->parameter = $parameter;
        return $this;
    }


    public function isFinal() {
        return $this->parameter->getStatus() == 'B';
    }
    public function isValidated() {
        return $this->parameter->getStatus() != '';
    }
    public function exceeds() {
        $resultFlag = strtoupper($this->parameter->getResultFlag());
        return $resultFlag==self::HIGH || $resultFlag==self::LOW || $resultFlag==self::VERY_HIGH || $resultFlag==self::VERY_LOW;
    }
    public function exceedsALot() {
        $resultFlag = strtoupper($this->parameter->getResultFlag());
        return $resultFlag==self::VERY_HIGH || $resultFlag==self::VERY_LOW;
    }
    public function isLow() {
        $resultFlag = strtoupper($this->parameter->getResultFlag());
        return $resultFlag==self::LOW || $resultFlag==self::VERY_LOW;
    }
    public function isHigh() {
        $resultFlag = strtoupper($this->parameter->getResultFlag());
        return $resultFlag==self::HIGH || $resultFlag==self::VERY_HIGH;
    }
    public function isVeryLow() {
        $resultFlag = strtoupper($this->parameter->getResultFlag());
        return $resultFlag==self::VERY_LOW;
    }
    public function isVeryHigh() {
        $resultFlag = strtoupper($this->parameter->getResultFlag());
        return $resultFlag==self::VERY_HIGH;
    }

    public function getDescription() {
        //return "description";
        $language = is_null($this->language) ? "nl" : $this->language;
        //return $language;
        /** @var ParameterBaseDescription $description */
        foreach ($this->parameter->getParameterBase()->getDescriptions() as $description) {
            if ($description->getLanguage() == $language) {
                return $description->getDescription();
            }
        }
        return "failed";
    }

    public function hasValidLink() {
        $link = $this->parameter->getParameterBase()->getLink();
        $hasWiki = $this->parameter->getParameterBase()->isWikilab();
        return (!is_null($link) && $link!='') || $hasWiki;
    }
    public function getLink() {
        $link = $this->parameter->getParameterBase()->getLink();
        if ($link != '')
            return $link;

        $language = is_null($this->language) ? "nl" : $this->language;
        return $this->rootUrl . '/' . $language . '/wiki/PR_'.$this->parameter->getParameterBase()->getParameterId();
    }

    public function getUnit() {
        return $this->parameter->getParameterBase()->getUnit();
    }

    public function isTitle() {
        return $this->parameter->getParameterBase()->getIsTitle() == '' ? 0 : $this->parameter->getParameterBase()->getIsTitle();
    }
    public function isCumulative() {
        return $this->parameter->getParameterBase()->getIsCumulative() == '' ? 0 : $this->parameter->getParameterBase()->getIsCumulative();
    }

    public function hasDateExpected() {
        return !is_null($this->parameter->getDateExpected());
    }

}