<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 09.03.16
 * Time: 10:45
 */
namespace Zoolyx\CoreBundle\Model\Calculation;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Parameter;

/**
 * @DI\Service("zoolyx_core.calculation.unitary_value_manager")
 */
class UnitaryValueManager {

    /** @var  Parameter $parameter */
    private $parameter;

    /**
     * @param Parameter $parameter
     */
    public function setParameter(Parameter $parameter) {
        $this->parameter = $parameter;
    }

    public function hasValidLowHigh() {
        $isValid = true;
        $isValid = is_numeric($this->parameter->getResult()) && $isValid;
        $isValid = is_numeric($this->parameter->getReference()->getLow()) && $isValid;
        $isValid = is_numeric($this->parameter->getReference()->getHigh()) && $isValid;
        $isValid = $this->parameter->getReference()->getHigh() - $this->parameter->getReference()->getLow() > 0 && $isValid;
        return $isValid;
    }
    public function hasValidHigh() {
        $isValid = true;
        $isValid = (is_numeric($this->parameter->getResult()) || $this->isNumericWithComparatorSign($this->parameter->getResult())) && $isValid;
        $isValid = is_numeric($this->parameter->getReference()->getHigh()) && $isValid;
        $isValid = $this->parameter->getReference()->getHigh() > 0 && $isValid;
        return $isValid;
    }

    public function isNumericWithComparatorSign($value) {
        $value = (string) $value;
        $isValid = true;
        $isValid = substr($value,0,1) != ">" && $isValid;
        $isValid = is_numeric(substr($value,1,strlen($value)-1)) && $isValid;
        return $isValid;
    }
    public function removeComparatorSign($value) {
        if (is_numeric($value)) {
            return $value;
        } elseif ($this->isNumericWithComparatorSign($value)) {
            return substr($value,1,strlen($value)-1);
        }
        return false;
    }
    public function getUnitaryValue() {
        if ( $this->hasValidLowHigh()) {
            $halfRange = ($this->parameter->getReference()->getHigh() - $this->parameter->getReference()->getLow() ) / 2;
            $middleValue = ($this->parameter->getReference()->getHigh() + $this->parameter->getReference()->getLow() ) / 2;
            return ($this->parameter->getResult() - $middleValue) / $halfRange;
        }
        return false;
    }

    public function getUnitaryValueBar() {
        if ($this->hasValidLowHigh()) {
            $value = $this->getUnitaryValue();
            $value = min($value, 3);
            $value = max($value, -3);
            return min(100*(3+$value)/6,98);
        }
        if ($this->hasValidHigh()) {
            $reference = $this->parameter->getReference()->getHigh();
            $value = $this->removeComparatorSign($this->parameter->getResult());
            $ratio = min($value/$reference,100);

            return $ratio<1 ? (100/3)*$ratio : (100/3)*(log10($ratio)+1);
            // return (100*2/3)*$value/$reference /* the linear version */
        }
        return 0;
    }

} 