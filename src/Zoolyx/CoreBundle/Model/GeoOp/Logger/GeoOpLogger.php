<?php
namespace Zoolyx\CoreBundle\Model\GeoOp\Logger;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\GeoOpLog;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpClient;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpJob;

/**
 * @DI\Service("zoolyx_core.geoop.logger")
 */
class GeoOpLogger
{

    public function getLoggingEntity(GeoOpClient $geoOpClient, GeoOpJob $geoOpJob, $response, VeterinaryPractice $veterinaryPractice, \DateTime $dateAvailable, $ophaling)
    {
        $geoOpLog = new GeoOpLog();
        $geoOpLog
            ->setClientId($geoOpClient->getId())
            ->setVeterinary($veterinaryPractice->getVeterinary())
            ->setVeterinaryPractice($veterinaryPractice)
            ->setTitle($geoOpJob->getTitle())
            ->setVoorzien($dateAvailable)
            ->setOphaling($ophaling)
            ->setDescription($geoOpJob->getDescription())
            ->setPriority($geoOpJob->getPriority())
            ->setLine1($geoOpClient->getAddress()->getLine1())
            ->setLine2($geoOpClient->getAddress()->getLine2() ? $geoOpClient->getAddress()->getLine2() : "")
            ->setCity($geoOpClient->getAddress()->getCity())
            ->setState($geoOpClient->getAddress()->getState() ? $geoOpClient->getAddress()->getState() : "")
            ->setPostCode($geoOpClient->getAddress()->getPostCode())
            ->setLatitude($geoOpClient->getAddress()->getLatitude())
            ->setLongitude($geoOpClient->getAddress()->getLongitude())
            ->setResponse($response["result"])
            ->setJobId($response["jobs"][0]["id"])
            ->setJobReference($response["jobs"][0]["reference"])
            ->setCreatedOn(new \DateTime())
        ;
        return $geoOpLog;
    }
} 