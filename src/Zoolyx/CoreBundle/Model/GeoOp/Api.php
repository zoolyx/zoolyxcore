<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\GeoOp;

use DateInterval;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\GeoOpEndpoint;
use Zoolyx\CoreBundle\Entity\Repository\GeoOpEndpointRepository;
use Zoolyx\CoreBundle\Model\Exception\GeoOpException;
use Zoolyx\CoreBundle\Model\GeoOp\Encoder\GeoOpClientEncoder;
use Zoolyx\CoreBundle\Model\GeoOp\Encoder\GeoOpJobEncoder;
use Zoolyx\CoreBundle\Model\GeoOp\Encoder\GeoOpNoteEncoder;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpClient;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpJob;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpNote;
use Zoolyx\CoreBundle\Model\GeoOp\Parser\GeoOpClientParser;

/**
 * @DI\Service("zoolyx_core.geoop.api")
 */
class Api {
    const RESPONSE_SUCCESS = 'success';
    const RESPONSE_CLIENTS = 'clients';

    /** @var GeoOpEndpoint */
    private $geoOpEndpoint;
    private $loginUrl = "https://login.geoop.com/";
    private $baseUrl = "https://api.geoop.com/";


    /** @var ObjectManager */
    private $entityManager;

    /** @var GeoOpClientParser */
    private $geoOpClientParser;

    /** @var GeoOpJobEncoder */
    private $geoOpJobEncoder;
    /** @var GeoOpNoteEncoder */
    private $geoOpNoteEncoder;
    /** @var  GeoOpClientEncoder */
    private $geoOpClientEncoder;

    /** @var string */
    private $geoOpAccountId;
    /** @var string */
    private $geoOpUserId;


    /**
     * Constructor.
     *
     * @param ObjectManager $entityManager
     * @param GeoOpEndpointRepository $geoOpEndpointRepository
     * @param GeoOpClientParser $geoOpClientParser
     * @param GeoOpJobEncoder $geoOpJobEncoder
     * @param GeoOpNoteEncoder $geoOpNoteEncoder
     * @param GeoOpClientEncoder $geoOpClientEncoder
     * @param string $geoOpAccountId
     * @param string $geoOpUserId
     *
     * @DI\InjectParams({
     *  "entityManager" = @DI\Inject("doctrine.orm.entity_manager"),
     *  "geoOpEndpointRepository" = @DI\Inject("zoolyx_core.repository.geo_op_endpoint"),
     *  "geoOpClientParser" = @DI\Inject("zoolyx_core.geoop_parser.client"),
     *  "geoOpJobEncoder" = @DI\Inject("zoolyx_core.geoop_encoder.job"),
     *  "geoOpNoteEncoder" = @DI\Inject("zoolyx_core.geoop_encoder.note"),
     *  "geoOpClientEncoder" = @DI\Inject("zoolyx_core.geoop_encoder.client"),
     *  "geoOpAccountId" = @DI\Inject("%geo_op_account_id%"),
     *  "geoOpUserId" = @DI\Inject("%geo_op_user_id%")
     * })
     */
    public function __construct(ObjectManager $entityManager, GeoOpEndpointRepository $geoOpEndpointRepository, GeoOpClientParser $geoOpClientParser, GeoOpJobEncoder $geoOpJobEncoder, GeoOpNoteEncoder $geoOpNoteEncoder, GeoOpClientEncoder $geoOpClientEncoder, $geoOpAccountId, $geoOpUserId)
    {
        $this->entityManager = $entityManager;
        $this->geoOpClientParser = $geoOpClientParser;
        $this->geoOpJobEncoder = $geoOpJobEncoder;
        $this->geoOpNoteEncoder = $geoOpNoteEncoder;
        $this->geoOpClientEncoder = $geoOpClientEncoder;
        $this->geoOpAccountId = $geoOpAccountId;
        $this->geoOpUserId = $geoOpUserId;
        $this->geoOpEndpoint = $geoOpEndpointRepository->find(1);
        if ($this->geoOpEndpoint->getExpiresIn() < new \DateTime()) {
            $this->refreshToken();
        }
    }

    public function refreshToken() {
        $url = $this->loginUrl . "oauth2/token";
        $postData = array(
            'grant_type' => 'refresh_token',
            'refresh_token' => $this->geoOpEndpoint->getRefreshToken(),
            'client_id' => $this->geoOpEndpoint->getClientId(),
            'client_secret' => $this->geoOpEndpoint->getClientSecret(),
        );
        $postDataStringParts = array();
        foreach ($postData as $key => $value) {
            $postDataStringParts[] = $key . "=" . urlencode($value);
        }
        $data = implode('&',$postDataStringParts);
        
	$contentType = 'application/x-www-form-urlencoded';
        $response = $this->call('POST',$url, $data, $contentType, true);
        if (isset($response["error"])) {
            $message = $response["error_code"] . ": " . $response["error_description"];
            echo $message;
            throw new GeoOpException($message);
        } else {
            //Array ( [access_token] => 6a== [token_type] => Bearer [expires_in] => 2592000 )
            if (isset($response["access_token"])) {
                $accessToken = $response["access_token"];
                $expiresIn = $response["expires_in"];
                $this->geoOpEndpoint->setAccessToken($accessToken);
                $now = new \DateTime();
                $now->add(new DateInterval('PT'.$expiresIn.'S'));
                $this->geoOpEndpoint->setExpiresIn($now);
                $this->entityManager->persist($this->geoOpEndpoint);
                $this->entityManager->flush();
            } 
            return true;
        }
    }

    public function getClient($limsId) {
        $url = $this->baseUrl . "clients?where=clientCode+is+%22" . $limsId . "%22";
        $response = $this->call('GET',$url);
        if ($response["result"] == self::RESPONSE_SUCCESS) {
            return $this->geoOpClientParser->parseClients($response[self::RESPONSE_CLIENTS]);
        } else {
            return null;
        }
    }
    public function postClient(GeoOpClient $geoOpClient, $limsId) {
        $url = $this->baseUrl . "clients";
        $data = $this->geoOpClientEncoder->getJson($geoOpClient, $limsId, $this->geoOpAccountId);

        $response = $this->call('POST',$url, $data);
        return $response;
    }
    public function postJob(GeoOpJob $geoOpJob) {
        $url = $this->baseUrl . "jobs";
        $data = $this->geoOpJobEncoder->getJson($geoOpJob, $this->geoOpAccountId);

        $response = $this->call('POST',$url, $data);
        return $response;
    }
    public function patchJob(GeoOpJob $geoOpJob) {
        $url = $this->baseUrl . "jobs/".$geoOpJob->getId();
        $data = $this->geoOpJobEncoder->getPatchJson($geoOpJob, "616419");

        $response = $this->call('PATCH',$url, $data);
        return $response;
    }
    public function postNote(GeoOpNote $geoOpNote) {
        $url = $this->baseUrl . "notes";
        $data = $this->geoOpNoteEncoder->getJson($geoOpNote, $this->geoOpAccountId, $this->geoOpUserId);

        $response = $this->call('POST',$url, $data);
        return $response;
    }

    private function call($method, $url, $data = null, $contentType = null, $noAuth = false) {
        $ch = curl_init();
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => ( $noAuth ? $this->getRefreshHeaders($contentType) : $this->getHeaders($contentType) ),
            CURLINFO_HEADER_OUT => true
        );
        if ((strtolower($method) == "post" || strtolower($method) == "patch") && !is_null($data)) {
            $options[CURLOPT_POSTFIELDS] = $data;
        }
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        return json_decode($content, true);
    }

    private function getHeaders($contentType) {
        $headers = array();
        $headers[] = $contentType ? 'Content-type: '.$contentType : 'Content-type: application/json';
        $headers[] = 'Authorization: Bearer '.$this->geoOpEndpoint->getAccessToken();
        $headers[] = 'Accept-Language: en-US,en;q=0.9,nl;q=0.8';
        $headers[] = 'Cache-Control: no-cache';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'X-Version: 1.2';
        return $headers;
    }

    private function getRefreshHeaders($contentType) {
        $headers = array();
        $headers[] = $contentType ? 'Content-type: '.$contentType : 'Content-type: application/json';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,nl;q=0.8';
        $headers[] = 'Cache-Control: no-cache';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'X-Version: 1.2';
        return $headers;
    }
} 