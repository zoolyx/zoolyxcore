<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\GeoOp\Encoder;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpJob;

/**
 * @DI\Service("zoolyx_core.geoop_encoder.job")
 */
class GeoOpJobEncoder {

    /** @var GeoOpAddressEncoder */
    private $geoOpAddressEncoder;


    /**
     * Constructor.
     *
     * @param GeoOpAddressEncoder $geoOpAddressEncoder
     *
     * @DI\InjectParams({
     *  "geoOpAddressEncoder" = @DI\Inject("zoolyx_core.geoop_encoder.address")
     * })
     */
    public function __construct(GeoOpAddressEncoder $geoOpAddressEncoder)
    {
        $this->geoOpAddressEncoder = $geoOpAddressEncoder;
    }


    /*
 * "jobs" => array(
        array(
            //"externalId"    => "externalId",  //must be unique
            //"reference"     => "reference",
            //"priority"      => "",   //1, 2 or 3
            "title"         => "short description of the job",
            "description"   => "customer provided description",
            //"jobNumber"     => "??",
            "client"        => array(
                "id" => "$clientId"
            ),
            "address"       => array(
                "line1" =>"Vredeweg 2",
                "city"  =>"Brakel",
                "postcode"=>"9660"
            ),
            "account"       => array(
                "id"    => "33692"
            )
        )
    )
 */
    public function getArray(GeoOpJob $geoOpJob, $geoOpAccountId)
    {
        return $data = array(
            'title'         => $geoOpJob->getTitle(),
            'priority'      => intval($geoOpJob->getPriority()),
            //'description'   => $geoOpJob->getDescription(),
            'client'        => array(
                                'id' =>$geoOpJob->getClientId()
            ),
            'address'       => $this->geoOpAddressEncoder->getArray($geoOpJob->getAddress()),
            'account'       => array(
                                'id' => $geoOpAccountId
            )
        );
    }

    public function getJson(GeoOpJob $geoOpJob, $geoOpAccountId)
    {
        $data = array(
            'jobs' => array($this->getArray($geoOpJob, $geoOpAccountId))
        );
        return json_encode($data);
    }


    /*
json {
    "jobs": [
        {
            "id": jobid,
             "status": {
               "id": "616419"
             }
        }
    ]
}
     */
    public function getPatchArray(GeoOpJob $geoOpJob, $statusCode)
    {
        return $data = array(
            'id'         => intval($geoOpJob->getId()),
            'status'        => array(
                'id' => $statusCode
            )
        );
    }
    public function getPatchJson(GeoOpJob $geoOpJob, $statusCode)
    {
        $data = array(
            'jobs' => array($this->getPatchArray($geoOpJob, $statusCode))
        );
        return json_encode($data);
    }

} 