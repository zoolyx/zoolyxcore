<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\GeoOp\Encoder;


use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpClient;

/**
 * @DI\Service("zoolyx_core.geoop_encoder.client")
 */
class GeoOpClientEncoder {

    /** @var GeoOpAddressEncoder */
    private $geoOpAddressEncoder;

    /**
     * Constructor.
     *
     * @param GeoOpAddressEncoder $geoOpAddressEncoder
     *
     * @DI\InjectParams({
     *  "geoOpAddressEncoder" = @DI\Inject("zoolyx_core.geoop_encoder.address")
     * })
     */

    public function __construct(GeoOpAddressEncoder $geoOpAddressEncoder) {
        $this->geoOpAddressEncoder = $geoOpAddressEncoder;
    }
    /*
     {
  "clients": [
    {
      "firstName": "string",
      "lastName": "string",
      "companyName": "string",
      "businessType": "string",
      "leadSource": "string",
      "emailAddress": "string",
      "phoneNumber": "string",
      "mobileNumber": "string",
      "mobileCountryCode": "integer",
      "faxNumber": "string",
      "notes": "text",
      "clientCode": "string",
      "deleted": "boolean",
      "url": "string",
      "billingClient": {"id": "int"},
      "account": {"id": "int"},
      "createdBy": {"id": "int"},
      "modifiedBy": {"id": "int"}
    }
  ]
}
     */
    /**
     * @param GeoOpClient $geoOpClient
     * @param $limsId
     * @param $accountId
     * @return array
     */
    public function getArray(GeoOpClient $geoOpClient, $limsId, $accountId)
    {
        return $data = array(
            'firstName'     => $geoOpClient->getFirstName(),
            'lastName'     => $geoOpClient->getLastName(),
            'companyName'      => $geoOpClient->getCompanyName(),
            'address'       => $this->geoOpAddressEncoder->getArray($geoOpClient->getAddress()),
            //'businessType'  => $geoOpClient->getBusinessType(),
            //'leadSource'  => $geoOpClient->getLeadSource(),
            'emailAddress'  => $geoOpClient->getEmailAddress(),
            'phoneNumber'  => $geoOpClient->getPhoneNumber(),
            'mobileNumber'  => $geoOpClient->getMobileNumber(),
            //'mobileCountryCode'  => $geoOpClient->getMobileCountryCode(),
            //'faxNumber'  => $geoOpClient->getFaxNumber(),
            //'notes'  => $geoOpClient->getNotes(),
            'clientCode'  => $limsId,
            'account' => array('id'=>$accountId)
        );
    }

    public function getJson(GeoOpClient $geoOpClient, $limsId, $accountId)
    {
        $data = array(
            'clients' => array($this->getArray($geoOpClient, $limsId, $accountId))
        );
        return json_encode($data);
    }

} 