<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\GeoOp\Encoder;


use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpAddress;

/**
 * @DI\Service("zoolyx_core.geoop_encoder.address")
 */
class GeoOpAddressEncoder {

    /*
 * "address"       => array(
                "line1" =>"Vredeweg 2",
                "city"  =>"Brakel",
                "postcode"=>"9660"
            )
 */
    public function getArray(GeoOpAddress $geoOpAddress)
    {
        return $data = array(
            'line1'     => $geoOpAddress->getLine1(),
            'line2'     => $geoOpAddress->getLine2(),
            'city'      => $geoOpAddress->getCity(),
            'postcode'  => $geoOpAddress->getPostCode(),
            //'latitude'  => $geoOpAddress->getLatitude(),
            //'longitude' => $geoOpAddress->getLongitude()
        );
    }

    public function getJson(GeoOpAddress $geoOpAddress)
    {
        return json_encode($this->getArray($geoOpAddress));
    }

} 