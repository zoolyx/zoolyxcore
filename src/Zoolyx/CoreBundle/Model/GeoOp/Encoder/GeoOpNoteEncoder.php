<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\GeoOp\Encoder;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpNote;

/**
 * @DI\Service("zoolyx_core.geoop_encoder.note")
 */
class GeoOpNoteEncoder {

/*
{
  "notes": [
    {
      "description": "Staal beschikbaar vanaf",
      "time": "2020-09-23T11:00:00 Europe/Brussels",
      "job": {
        "id": "39955125"
      },
      "account": {
        "id": "33692"
      },
      "user": {
        "id": "956672"   -> is fixed (moet uit parameters komen)
      }
    }
  ]
}
 */
    public function getArray(GeoOpNote $geoOpNote, $geoOpAccountId, $geoOpUserId)
    {
        return $data = array(
            'description'   =>  $geoOpNote->getDescription(),
            'time'          =>  $geoOpNote->getTime(),
            'job'           =>  array(
                                    'id' =>$geoOpNote->getJobId()
                                ),
            'account'       =>  array(
                                    'id' => $geoOpAccountId
                                ),
            'user'          =>  array(
                                    'id' => $geoOpUserId
                                )
        );
    }

    public function getJson(GeoOpNote $geoOpNote, $geoOpAccountId, $geoOpUserId)
    {
        $data = array(
            'notes' => array($this->getArray($geoOpNote, $geoOpAccountId, $geoOpUserId))
        );
        return json_encode($data);
    }

} 