<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\GeoOp\Parser;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpClient;

/**
 * @DI\Service("zoolyx_core.geoop_parser.client")
 */
class GeoOpClientParser {

    const CLIENT_ID = 'id';
    const CLIENT_FIRST_NAME = 'firstName';
    const CLIENT_LAST_NAME = 'lastName';
    const CLIENT_COMPANY_NAME = 'companyName';
    const CLIENT_BUSINESS_TYPE = 'businessType';
    const CLIENT_LEAD_SOURCE = 'leadSource';
    const CLIENT_EMAIL_ADDRESS = 'emailAddress';
    const CLIENT_PHONE_NUMBER = 'phoneNumber';
    const CLIENT_MOBILE_NUMBER = 'mobileNumber';
    const CLIENT_MOBILE_COUNTRY_CODE = 'mobileCountryCode';
    const CLIENT_FAX_NUMBER = 'faxNumber';
    const CLIENT_NOTES = 'notes';
    const CLIENT_DELETED = 'deleted';
    const CLIENT_CREATED = 'created';
    const CLIENT_MODIFIED = 'modified';
    const CLIENT_BILLING_CLIENT = 'billingClient';
    const CLIENT_ADDRESS = 'address';

    /** @var GeoOpAddressParser */
    private $geoOpAddressParser;


    /**
     * Constructor.
     *
     * @param GeoOpAddressParser $geoOpAddressParser
     *
     * @DI\InjectParams({
     *  "geoOpAddressParser" = @DI\Inject("zoolyx_core.geoop_parser.address")
     * })
     */
    public function __construct(GeoOpAddressParser $geoOpAddressParser)
    {
        $this->geoOpAddressParser = $geoOpAddressParser;
    }

    public function parseClients($data) {
        $clients = array();
        foreach ($data as $entry) {
            $clients[] = $this->parseClient($entry);
        }
        return $clients[0];
    }

    public function parseClient($data) {
        $geoOpClient = new GeoOpClient();
        /**
         * [{"id":7100632,"firstName":"ISABELLE","lastName":"VAN DESSEL",
         * "companyName":"","businessType":"Dierenarts",
         * "leadSource":"","emailAddress":"DIERENARTS@ISABELLEVANDESSEL.BE",
         * "phoneNumber":"03\/237.10.98","mobileNumber":"0477\/33.12.14",
         * "mobileCountryCode":32,"faxNumber":"+32.32371098",
         * "notes":"","deleted":false,"created":"2016-01-06T12:27:53+00:00",
         * "modified":"2016-01-06T12:28:05+00:00","billingClient":null,
         * "account":{"id":33692},
         * "address":{"line1":"PALEISSTRAAT 26","line2":null,"city":"ANTWERPEN",
         * "state":null,"postcode":"2018","latitude":51.2076202,"longitude":4.4020468},
         * "mailingAddress":{}}]
         */
        $geoOpClient->setId($data[self::CLIENT_ID]);
        $geoOpClient->setFirstName($data[self::CLIENT_FIRST_NAME]);
        $geoOpClient->setLastName($data[self::CLIENT_LAST_NAME]);
        $geoOpClient->setCompanyName($data[self::CLIENT_COMPANY_NAME]);
        $geoOpClient->setBusinessType($data[self::CLIENT_BUSINESS_TYPE]);
        $geoOpClient->setLeadSource($data[self::CLIENT_LEAD_SOURCE]);
        $geoOpClient->setEmailAddress($data[self::CLIENT_EMAIL_ADDRESS]);
        $geoOpClient->setPhoneNumber($data[self::CLIENT_PHONE_NUMBER]);
        $geoOpClient->setMobileNumber($data[self::CLIENT_MOBILE_NUMBER]);
        $geoOpClient->setMobileCountryCode($data[self::CLIENT_MOBILE_COUNTRY_CODE]);
        $geoOpClient->setFaxNumber($data[self::CLIENT_FAX_NUMBER]);
        $geoOpClient->setNotes($data[self::CLIENT_NOTES]);
        $geoOpClient->setDeleted($data[self::CLIENT_DELETED] == 'true');
        $geoOpClient->setCreated(new \DateTime($data[self::CLIENT_CREATED]));
        $geoOpClient->setModified(new \DateTime($data[self::CLIENT_MODIFIED]));
        $geoOpClient->setBillingClient(null);
        $geoOpClient->setAddress($this->geoOpAddressParser->parseAddress($data[self::CLIENT_ADDRESS]));

        return $geoOpClient;
    }

} 