<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\GeoOp\Parser;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpAddress;

/**
 * @DI\Service("zoolyx_core.geoop_parser.address")
 */
class GeoOpAddressParser {

    const ADDRESS_LINE1 = 'line1';
    const ADDRESS_LINE2 = 'line2';
    const ADDRESS_CITY = 'city';
    const ADDRESS_STATE = 'state';
    const ADDRESS_POST_CODE = 'postcode';
    const ADDRESS_LATITUDE = 'latitude';
    const ADDRESS_LONGITUDE = 'longitude';


    public function parseAddress($data) {
        $geoOpAddress = new GeoOpAddress();
        /**

         * "address":{
         * "line1":"PALEISSTRAAT 26","line2":null,"city":"ANTWERPEN",
         * "state":null,"postcode":"2018","latitude":51.2076202,"longitude":4.4020468
         * }
         */
        $geoOpAddress->setLine1($data[self::ADDRESS_LINE1]);
        $geoOpAddress->setLine2($data[self::ADDRESS_LINE2]);
        $geoOpAddress->setCity($data[self::ADDRESS_CITY]);
        $geoOpAddress->setState($data[self::ADDRESS_STATE]);
        $geoOpAddress->setPostCode($data[self::ADDRESS_POST_CODE]);
        $geoOpAddress->setLatitude($data[self::ADDRESS_LATITUDE]);
        $geoOpAddress->setLongitude($data[self::ADDRESS_LONGITUDE]);

        return $geoOpAddress;
    }

} 