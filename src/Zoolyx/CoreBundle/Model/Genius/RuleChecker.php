<?php
namespace Zoolyx\CoreBundle\Model\Genius;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\GeniusCode;
use Zoolyx\CoreBundle\Entity\Parameter;
use Zoolyx\CoreBundle\Entity\ParameterGroup;
use Zoolyx\CoreBundle\Entity\Repository\GeniusCodeRepository;
use Zoolyx\CoreBundle\Entity\Sample;

/**
 * @DI\Service("zoolyx_core.genius.rule_checker")
 */
class RuleChecker {

    /** @var GeniusCodeRepository */
    private $geniusCodeRepository;

    /**
     * Constructor.
     *
     * @param GeniusCodeRepository $geniusCodeRepository
     *
     * @DI\InjectParams({
     *  "geniusCodeRepository" = @DI\Inject("zoolyx_core.repository.genius_code")
     * })
     */
    public function __construct(GeniusCodeRepository $geniusCodeRepository)
    {
        $this->geniusCodeRepository = $geniusCodeRepository;
    }

    public function getSuggestionsForSample(Sample $sample)
    {
        $dateReceived = $sample->getDateReceived();
        $now = new \DateTime(date("Y-m-d H:i:s"));
        $daysOld = $now->diff($dateReceived)->days;

        $reportParameters = array();
        $speciesCode = $sample->getPet()->getSpeciesCode();
        /** @var ParameterGroup $parameterGroup */
        foreach ($sample->getParameterGroups() as $parameterGroup) {
            /** @var Parameter $parameter */
            foreach ($parameterGroup->getParameters() as $parameter) {
                $reportParameters[$parameter->getParameterBase()->getParameterId()] = $parameter;
            }
        }

        $geniusCodes = $this->geniusCodeRepository->findAll();

        $suggestions = array();
        /** @var GeniusCode $geniusCode */
        foreach ($geniusCodes as $geniusCode) {
            if ($geniusCode->getSpecies()->getSpeciesId() != $speciesCode) { //wrong species
                continue;
            }
            if ($daysOld > $geniusCode->getExpires()) {  //too late
                continue;
            }
            $rule = $geniusCode->getRule();
            $ruleParts = explode(';',$rule);
            $rules = array();
            foreach ($ruleParts as $rulePart) {
                $rpp = explode('=',$rulePart);
                $rules[$rpp[0]] = $rpp[1];
            }

            $rulesMatch = true;
            foreach ($rules as $id=>$value) {
                /** @var Parameter $reportParameter */
                $reportParameter = isset($reportParameters[$id]) ? $reportParameters[$id] : null;
                $rulesMatch = !$reportParameter || $reportParameter->getResultFlag() != $value ? false : $rulesMatch;
            }


            if (!$rulesMatch) {
                continue;
            }

            // check if there are parameters in the profile that are not yet analysed
            $hasNonAnalysedParameter = false;
            foreach ($geniusCode->getParameterProfile()->getParameters() as $parameter) {
                $hasNonAnalysedParameter = !isset($reportParameters[$parameter->getParameterId()]) ? true : $hasNonAnalysedParameter;
            }

            if (!$hasNonAnalysedParameter) {
                continue;
            }

            $suggestions[] = $geniusCode;
        }

        return $suggestions;
    }
} 