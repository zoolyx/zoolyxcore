<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 07.10.19
 * Time: 13:30
 */

namespace Zoolyx\CoreBundle\Model\Address\Validation;


class Address {
    private $streetName;
    private $streetNumber;
    private $boxNumber;
    private $postalCode;
    private $municipalityName;
    private $countryName;
    private $addressLanguage;

    public function __construct($street = "", $postalCode = "", $municipalityName = "", $country = "BELGIE") {
        $this->streetName = $street;
        $this->postalCode = $postalCode;
        $this->municipalityName = $municipalityName;
        $this->countryName = $country;
    }

    /**
     * @return mixed
     */
    public function getBoxNumber()
    {
        return $this->boxNumber;
    }

    /**
     * @param mixed $boxNumber
     */
    public function setBoxNumber($boxNumber)
    {
        $this->boxNumber = $boxNumber;
    }

    /**
     * @return mixed
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * @param mixed $countryName
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;
    }

    /**
     * @return mixed
     */
    public function getMunicipalityName()
    {
        return $this->municipalityName;
    }

    /**
     * @param mixed $municipalityName
     */
    public function setMunicipalityName($municipalityName)
    {
        $this->municipalityName = $municipalityName;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return mixed
     */
    public function getStreetName()
    {
        return $this->streetName;
    }

    /**
     * @param mixed $streetName
     */
    public function setStreetName($streetName)
    {
        $this->streetName = $streetName;
    }

    /**
     * @return mixed
     */
    public function getStreetNumber()
    {
        return $this->streetNumber;
    }

    /**
     * @param mixed $streetNumber
     */
    public function setStreetNumber($streetNumber)
    {
        $this->streetNumber = $streetNumber;
    }

    /**
     * @return mixed
     */
    public function getAddressLanguage()
    {
        return $this->addressLanguage;
    }

    /**
     * @param mixed $addressLanguage
     * @return $this
     */
    public function setAddressLanguage($addressLanguage)
    {
        $this->addressLanguage = $addressLanguage;
        return $this;
    }

    public function getFullStreet() {
        $parts = array($this->getStreetName(),$this->getStreetNumber());
        if ($this->getBoxNumber()) {
            $parts[] = $this->getAddressLanguage() == "fr" ? "boîte" : "bus";
            $parts[] = $this->getBoxNumber();
        }
        return implode(" ",$parts);
    }


} 