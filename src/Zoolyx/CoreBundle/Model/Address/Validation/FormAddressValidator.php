<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 05.05.20
 * Time: 16:56
 */

namespace Zoolyx\CoreBundle\Model\Address\Validation;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @DI\Service("zoolyx_core.address.form_validator")
 */
class FormAddressValidator {

    const FORM_NAME_STREET = "street";
    const FORM_NAME_ZIPCODE = "zipcode";
    const FORM_NAME_CITY = "city";

    /** @var BpostApi $bpostApi */
    private $bpostApi;

    private $street = null;
    private $zipCode = null;
    private $city = null;

    /**
     * Constructor.
     *
     * @param BpostApi $bpostApi
     *
     * @DI\InjectParams({
     *  "bpostApi" = @DI\Inject("zoolyx_core.address.bpost_api")
     * })
     */
    public function __construct(BpostApi $bpostApi) {
        $this->bpostApi = $bpostApi;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }



    /**
     * @param string $street
     * @param string $zipCode
     * @param string $city
     * @param ExecutionContextInterface $context
     * @param array $fields
     */
    public function validate($street, $zipCode, $city, $context, $fields) {

        if (!isset($fields[self::FORM_NAME_STREET])) {
            $fields[self::FORM_NAME_STREET] = "street";
        }
        if (!isset($fields[self::FORM_NAME_ZIPCODE])) {
            $fields[self::FORM_NAME_ZIPCODE] = "zipCode";
        }
        if (!isset($fields[self::FORM_NAME_CITY])) {
            $fields[self::FORM_NAME_CITY] = "city";
        }


        $address = new Address($street, $zipCode, $city);
        $this->bpostApi->validate($address);
        $warnings = $this->bpostApi->getWarnings();
        $errors = $this->bpostApi->getErrors();

        if (count($errors)) {
            if (count($warnings)) {
                /** @var BpostMessage $warning */
                foreach ($warnings as $warning) {
                    switch ($warning->getcomponent()) {
                        case BpostMessage::COMPONENT_STR_NAME:
                            $context->buildViolation('request.pe_street.invalid')
                                ->atPath($fields[self::FORM_NAME_STREET])
                                ->addViolation();
                            break;
                        case BpostMessage::COMPONENT_NUMBER:
                            $context->buildViolation('request.pe_number.invalid')
                                ->atPath($fields[self::FORM_NAME_STREET])
                                ->addViolation();
                            break;
                        case BpostMessage::COMPONENT_BOX:
                            $context->buildViolation('request.pe_number.invalid')
                                ->atPath($fields[self::FORM_NAME_STREET])
                                ->addViolation();
                            break;
                        case BpostMessage::COMPONENT_POSTAL_CODE:
                            $context->buildViolation('request.pe_zipcode.invalid')
                                ->atPath($fields[self::FORM_NAME_ZIPCODE])
                                ->addViolation();
                            break;
                        case BpostMessage::COMPONENT_MUN_NAME:
                            $context->buildViolation('request.pe_city.invalid')
                                ->atPath($fields[self::FORM_NAME_CITY])
                                ->addViolation();
                            break;
                    }

                }
            } else {
                // toon algemene fout
                $context->buildViolation('request.address.invalid')
                    ->atPath($fields[self::FORM_NAME_STREET])
                    ->addViolation();
            }
        } else {
            /** @var Address $address */
            $address = $this->bpostApi->getAddress();
            //var_dump($address);

            $this->street = $address->getFullStreet();
            if (strtoupper($this->street) != strtoupper($street)) {
                $context->buildViolation('request.pe_street.changed')
                    ->atPath($fields[self::FORM_NAME_STREET])
                    ->addViolation();
            }

            $this->zipCode = $address->getPostalCode();
            if (strtoupper($this->zipCode) != strtoupper($zipCode)) {
                $context->buildViolation('request.pe_zipcode.changed')
                    ->atPath($fields[self::FORM_NAME_ZIPCODE])
                    ->addViolation();
            }


            $this->city = $address->getMunicipalityName();
            if (strtoupper($this->city) != strtoupper($city)) {
                $context->buildViolation('request.pe_city.changed')
                    ->atPath($fields[self::FORM_NAME_CITY])
                    ->addViolation();
            }

        }
    }
} 