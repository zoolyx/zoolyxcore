<?php
namespace Zoolyx\CoreBundle\Model\Address\Validation;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("zoolyx_core.address.encoder")
 */
class AddressEncoder {

    public function encode(Address $address) {
        $json = $this->buildJson($address);
        return $json;
    }
    private function buildJson(Address $address) {
        $request = array(
            "ValidateAddressesRequest" => array(
                "AddressToValidateList" => array(
                    "AddressToValidate" => array(
                        array(
                            "@id" => 1,
                            "AddressBlockLines" => array(
                                "UnstructuredAddressLine" => array(
                                    is_null($address->getStreetName()) ? "" : $address->getStreetName(),
                                    is_null($address->getPostalCode()) ? "" : $address->getPostalCode(),
                                    is_null($address->getMunicipalityName()) ? "" : $address->getMunicipalityName()
                                )
                            ),
                            /*
                            "PostalAddress" => array(
                                "DeliveryPointLocation" => array(
                                    "StructuredDeliveryPointLocation" => array(
                                        "StreetName"    => is_null($address->getStreetName()) ? "" : $address->getStreetName(),
                                        "StreetNumber"  => is_null($address->getStreetNumber()) ? "" : $address->getStreetNumber(),
                                        "BoxNumber"     => is_null($address->getBoxNumber()) ? "" : $address->getBoxNumber()
                                    )
                                ),
                                "PostalCodeMunicipality" => array(
                                    "StructuredPostalCodeMunicipality" => array(
                                        "PostalCode"        => is_null($address->getPostalCode()) ? "" : $address->getPostalCode(),
                                        "MunicipalityName"  => is_null($address->getMunicipalityName()) ? "" : $address->getMunicipalityName()
                                    )
                                ),
                            ),
                            "CountryName" => is_null($address->getCountryName()) ? "" : $address->getCountryName()
                            */
                        )
                    )
                ),
                "ValidateAddressOptions" => array(
                    "IncludeFormatting"         =>true,
                    "IncludeSuggestions"        => true,
                    "IncludeSubmittedAddress"   => false,
                    "IncludeDefaultGeoLocation" => false,
                    "IncludeListOfBoxes"        => true,
                    "IncludeNumberOfBoxes"      => true
                )
            )
        );

        return json_encode($request);
    }
} 