<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Address\Validation;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("zoolyx_core.address.bpost_api")
 */
class BpostApi {
    const RESPONSE_SUCCESS = 'success';
    const RESPONSE_CLIENTS = 'clients';

    private $formatUrl = "https://webservices-pub.bpost.be/ws/ExternalMailingAddressProofingCSREST_v1/address/formatAddresses";
    private $validateUrl = "https://webservices-pub.bpost.be/ws/ExternalMailingAddressProofingCSREST_v1/address/validateAddresses";


    /** @var AddressEncoder */
    private $addressEncoder;
    /** @var AddressInterpreter */
    private $addressInterpreter;
    /** @var Address */
    private $address;
    /** @var array */
    private $errors;
    /** @var array */
    private $warnings;

    /**
     * Constructor.
     *
     * @param AddressEncoder $addressEncoder
     * @param AddressInterpreter $addressInterpreter
     *
     * @DI\InjectParams({
     *  "addressEncoder" = @DI\Inject("zoolyx_core.address.encoder"),
     *  "addressInterpreter" = @DI\Inject("zoolyx_core.address.interpreter")
     * })
     */
    public function __construct(AddressEncoder $addressEncoder, AddressInterpreter $addressInterpreter)
    {
        $this->addressEncoder = $addressEncoder;
        $this->addressInterpreter = $addressInterpreter;
    }


    public function format(Address $address) {
        $data = $this->addressEncoder->encode($address);
        $response = $this->call('POST',$this->formatUrl, $data);
        return $response;
    }

    public function validate(Address $address) {
        $data = $this->addressEncoder->encode($address);
        $response = $this->call('POST',$this->validateUrl, $data);
        $result = $this->addressInterpreter->analyse($response);
        $this->errors = $this->addressInterpreter->getErrors();
        $this->warnings = $this->addressInterpreter->getWarnings();
        if ($result) {
            $this->address = $this->addressInterpreter->getNewAddress();
        }
        return $result;
    }

    private function call($method, $url, $data = null, $contentType = null) {
        $ch = curl_init();
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => ( $this->getHeaders($contentType)),
            CURLINFO_HEADER_OUT => true
        );
        if (strtolower($method) == "post" && !is_null($data)) {
            $options[CURLOPT_POSTFIELDS] = $data;
        }
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        return json_decode($content, true);
    }

    private function getHeaders($contentType) {
        $headers = array();
        $headers[] = $contentType ? 'Content-type: '.$contentType : 'Content-type: application/json';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,nl;q=0.8';
        $headers[] = 'Cache-Control: no-cache';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'X-Version: 1.2';
        return $headers;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    public function getWarnings()
    {
        return $this->warnings;
    }



} 