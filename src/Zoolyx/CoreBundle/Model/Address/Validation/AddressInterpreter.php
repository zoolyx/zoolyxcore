<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 07.10.19
 * Time: 14:17
 */

namespace Zoolyx\CoreBundle\Model\Address\Validation;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("zoolyx_core.address.interpreter")
 */
class AddressInterpreter {
    const RESPONSE = "ValidateAddressesResponse";
    const RESULT_LIST = "ValidatedAddressResultList";
    const RESULT = "ValidatedAddressResult";
    const ADDRESS_LIST = "ValidatedAddressList";
    const ADDRESS = "ValidatedAddress";
    const POSTAL = "PostalAddress";
    const LANGUAGE = "AddressLanguage";
    const LABEL = "Label";
    const BOX_LIST = "ServicePointBoxList";
    const LOCATION = "StructuredDeliveryPointLocation";
    const STR_NAME = "StreetName";
    const NUMBER = "StreetNumber";
    const BOX = "BoxNumber";
    const MUNICIPALITY = "StructuredPostalCodeMunicipality";
    const CODE = "PostalCode";
    const MUN_NAME = "MunicipalityName";
    const COUNTRY_NAME = "CountryName";

    const ADDRESS_LANGUAGE = "AddressLanguage";

    const G_ERROR = "GeneralError";
    const ERROR = "Error";
    const ERROR_C = "ErrorCode";
    const ERROR_S = "ErrorSeverity";
    const ERROR_REF = "ComponentRef";

    const ERROR_S_WARNING = "warning";
    const ERROR_S_ERROR = "error";

    private $newAddress = null;
    private $warnings = array();
    private $errors = array();

    public function analyse($response) {
        //print_r($response);

        if (!isset($response[self::RESPONSE])) {
            $this->errors[] = new BpostMessage(BpostMessage::COMPONENT_API,"general_error");
            return false;
        }

        $response = $response[self::RESPONSE];
        if (isset($response[self::G_ERROR])) {
            $generalError = $response[self::G_ERROR];
            $errorCode = isset($generalError[self::ERROR_C]) ? $generalError[self::ERROR_C] : "";
            $errorSeverity = isset($generalError[self::ERROR_S]) ? $generalError[self::ERROR_S] : "";
            switch (strtolower($errorSeverity)) {
                case self::ERROR_S_WARNING :
                    $this->warnings[] = new BpostMessage(BpostMessage::COMPONENT_API,$errorCode);
                    break;
                case self::ERROR_S_ERROR :
                    $this->errors[] = new BpostMessage(BpostMessage::COMPONENT_API,$errorCode);
            }
        }

        $results = $response[self::RESULT_LIST][self::RESULT];
        $numResults = count($results);
        if ($numResults == 0) {
            $this->errors[] = new BpostMessage(BpostMessage::COMPONENT_API,"no_results");
            return false;
        }
        if ($numResults > 1) {
            //die(" don't support multiple results yet");
        }

        $result = $results[0];

        if (isset($result[self::ERROR])) {
            $responseErrors = $result[self::ERROR];
            foreach ($responseErrors as $responseError) {
                $errorCode = $responseError[self::ERROR_C];
                $errorSeverity = $responseError[self::ERROR_S];
                $errorComponent = $responseError[self::ERROR_REF];
                switch (strtolower($errorSeverity)) {
                    case self::ERROR_S_WARNING :
                        $this->warnings[] = new BpostMessage($errorComponent,$errorCode);
                        break;
                    case self::ERROR_S_ERROR :
                        $this->errors[] = new BpostMessage($errorComponent,$errorCode);
                }
            }
        }


        if (!isset($result[self::ADDRESS_LIST]) ||
            !isset($result[self::ADDRESS_LIST][self::ADDRESS])
        ) {
            $this->errors[] = array("no_address", null);
            return false;
        }

        $addresses = $result[self::ADDRESS_LIST][self::ADDRESS];
        $numAddresses = count($addresses);
        if ($numAddresses == 0) {
            //$this->errors[] = array("no_address", null);
            return false;
        }

        $address = $addresses[0];

        if (!isset($address[self::POSTAL]) ||
            !isset($address[self::POSTAL][self::LOCATION]) ||
            !isset($address[self::POSTAL][self::LOCATION][self::STR_NAME]) ||
            !isset($address[self::POSTAL][self::LOCATION][self::NUMBER]) ||
            !isset($address[self::POSTAL][self::LOCATION][self::BOX]) ||
            !isset($address[self::POSTAL][self::MUNICIPALITY]) ||
            !isset($address[self::POSTAL][self::MUNICIPALITY][self::CODE]) ||
            !isset($address[self::POSTAL][self::MUNICIPALITY][self::MUN_NAME]) ||
            !isset($address[self::POSTAL][self::COUNTRY_NAME])
        ) {
            //$this->errors[] = array("missing field", null);
        }

        if (count($this->errors)) return false;



        $newAddress = new Address();
        $newAddress->setStreetName($address[self::POSTAL][self::LOCATION][self::STR_NAME]);
        $newAddress->setStreetNumber($address[self::POSTAL][self::LOCATION][self::NUMBER]);
        $newAddress->setBoxNumber(isset($address[self::POSTAL][self::LOCATION][self::BOX]) ? $address[self::POSTAL][self::LOCATION][self::BOX] : null);
        $newAddress->setPostalCode($address[self::POSTAL][self::MUNICIPALITY][self::CODE]);
        $newAddress->setMunicipalityName($address[self::POSTAL][self::MUNICIPALITY][self::MUN_NAME]);
        $newAddress->setCountryName($address[self::POSTAL][self::COUNTRY_NAME]);
        $newAddress->setAddressLanguage($address[self::ADDRESS_LANGUAGE]);
        $this->newAddress = $newAddress;
        return true;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return Address
     */
    public function getNewAddress()
    {
        return $this->newAddress;
    }

    /**
     * @return array
     */
    public function getWarnings()
    {
        return $this->warnings;
    }
} 