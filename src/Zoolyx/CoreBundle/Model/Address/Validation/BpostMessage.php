<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 28.10.19
 * Time: 12:06
 */

namespace Zoolyx\CoreBundle\Model\Address\Validation;


class BpostMessage {
    const COMPONENT_API = "API";
    const COMPONENT_STR_NAME = AddressInterpreter::STR_NAME;
    const COMPONENT_NUMBER = AddressInterpreter::NUMBER;
    const COMPONENT_BOX = AddressInterpreter::BOX;
    const COMPONENT_POSTAL_CODE = AddressInterpreter::CODE;
    const COMPONENT_MUN_NAME = AddressInterpreter::MUN_NAME;

    private $component = "";
    private $code = "";

    public function __construct($component, $code) {
        $this->component = $component;
        $this->code = $code;
    }

    public function getcomponent() {
        return $this->component;
    }
    public function getCode() {
        return $this->code;
    }
} 