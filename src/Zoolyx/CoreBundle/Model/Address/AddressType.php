<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 9/7/14
 * Time: 4:34 PM
 */

namespace Zoolyx\CoreBundle\Model\Address;


use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class AddressType extends AbstractType
{
    /** @var Translator */
    private $translator;

    public function __construct($translator)
    {
        $this->translator = $translator;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('streetName', 'text', array(
                'label' => $this->translator->trans('form.practice.street'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.practice.street_only.placeholder'),
                )
            ))
            ->add('streetNumber', 'text', array(
                'label' => $this->translator->trans('form.practice.number'),
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.practice.number.placeholder'),
                )
            ))
            ->add('postalCode', 'text', array(
                'label' => $this->translator->trans('form.practice.zipcode'),
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.practice.zipcode.placeholder'),
                )
            ))
            ->add('municipalityName', 'text', array(
                'label' => $this->translator->trans('form.practice.city'),
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.practice.city.placeholder'),
                )
            ))
            ->add('countryName', 'text', array(
                'label' => $this->translator->trans('form.practice.country'),
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.practice.country.placeholder'),
                )
            ))
            ->add('submit', 'submit', array(
                'label' => $this->translator->trans('form.practice.submit'),
            ))

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zoolyx\CoreBundle\Model\Address\Validation\Address',
        ));
    }

    public function getName()
    {
        return 'Address';
    }
}
