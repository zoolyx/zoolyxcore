<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 14.06.17
 * Time: 17:53
 */

namespace Zoolyx\CoreBundle\Model\File;

class File {

    private $name = "";
    private $originalName = "";
    private $content = "";
    private $base64Encoded = true;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    /**
     * @param string $originalName
     * @return $this
     */
    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName;
        return $this;
    }



    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isBase64Encoded()
    {
        return $this->base64Encoded;
    }

    /**
     * @param boolean $base64Encoded
     */
    public function setBase64Encoded($base64Encoded)
    {
        $this->base64Encoded = $base64Encoded;
    }

} 