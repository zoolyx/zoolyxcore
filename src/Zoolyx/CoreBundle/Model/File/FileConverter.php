<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 14.06.17
 * Time: 17:44
 */

namespace Zoolyx\CoreBundle\Model\File;


use JMS\DiExtraBundle\Annotation as DI;


class FileConverter {

    /** @var FileManager $fileManager */
    private $fileManager;

    /** @var string */
    private $tempFolder;

    /**
     * Constructor.
     *
     * @param FileManager $fileManager
     * @param string $tempFolder
     */
    public function __construct(FileManager $fileManager, $tempFolder)
    {
        $this->fileManager = $fileManager;
        $this->tempFolder = $tempFolder;
    }

    /**
     * @param File $file
     * @return resource
     */
    public function convert($file) {
        $content = $file->getContent();
        if ($file->isBase64Encoded()) {
            $content = base64_decode($content);
        }
        $contentLength = strlen($content);
        if ($contentLength > 500000) {
            $tempFileIn = $this->tempFolder.'/tmp_'.$file->getName();
            $tempFileOut = $this->tempFolder.'/tmp_out_'.$file->getName();
            file_put_contents($tempFileIn,$content);
            $extension = $this->fileManager->getExtension($file);

            if ($extension == 'pdf') {
                exec("gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -sOutputFile=".$tempFileOut." ".$tempFileIn);
            } else {
                exec("convert -verbose -density 150 -trim ".$tempFileIn." -quality 10 -flatten -sharpen 0x1.0 ".$tempFileOut);
            }

            if (file_exists($tempFileOut)) {
                $newContent = file_get_contents($tempFileOut);
                $newContentLength = strlen($newContent);
                if ($newContentLength < $contentLength) {
                    $content = $newContent;
                    $contentLength = $newContentLength;
                }
            }
            unlink($tempFileIn);
            unlink($tempFileOut);
        }


        $tempFile = fopen('php://memory', 'r+');

        fputs($tempFile, $content, $contentLength);
        rewind($tempFile);
        return $tempFile;
    }
} 