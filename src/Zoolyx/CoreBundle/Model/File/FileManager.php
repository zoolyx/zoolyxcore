<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 14.06.17
 * Time: 17:44
 */

namespace Zoolyx\CoreBundle\Model\File;


use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("zoolyx_core.file.manager")
 */

class FileManager {

    /**
     * @param File $file
     * @return string
     */
    public function getExtension(File $file) {
        $parts = explode(".", $file->getName());
        return count($parts)>1 ? $parts[count($parts)-1] : null;
    }

    /**
     * @param File $file
     * @param string $newName
     */
    public function renameTo(File $file, $newName) {
                $newNameParts = array($newName);
        $extension = $this->getExtension($file);
        if ($extension) {
            $newNameParts[] = $extension;
        }

        $file->setOriginalName($file->getName());
        $file->setName(implode(".",$newNameParts));
    }

} 