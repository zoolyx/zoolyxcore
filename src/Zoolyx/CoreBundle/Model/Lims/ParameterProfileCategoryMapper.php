<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Lims;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\ParameterProfileCategory as ParameterProfileCategoryEntity;
use Zoolyx\CoreBundle\Entity\ParameterProfileCategoryDescription;

/**
 * @DI\Service("zoolyx_core.mapper.parameter_profile_category")
 */
class ParameterProfileCategoryMapper {
    /**
     * @param ParameterProfileCategoryEntity $categoryEntity
     * @param string $language
     * @return ParameterProfileCategory
     */
    public function map($categoryEntity, $language = 'nl') {
        $category = new ParameterProfileCategory();
        $category->setReference($categoryEntity->getReference());
        $descriptions = $categoryEntity->getDescriptions();
        /** @var ParameterProfileCategoryDescription $description */
        foreach ($descriptions as $description) {
            if ($description->getLanguage() == $language) {
                $category->setDescription($description);
            }
        }
        return $category;
    }

} 