<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Lims;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Repository\BreedRepository;
use Zoolyx\CoreBundle\Entity\Repository\SpeciesRepository;
use Zoolyx\CoreBundle\Entity\Breed as BreedEntity;

/**
 * @DI\Service("zoolyx_core.manager.breed")
 */
class BreedManager {

    /** @var string */
    private $language = 'nl';
    /** @var string */
    private $speciesCode = 'H';

    /** @var SpeciesRepository */
    private $speciesRepository;

    /** @var BreedRepository */
    private $breedRepository;

    /** @var BreedMapper */
    private $breedMapper;

    /**
     * Constructor.
     *
     * @param SpeciesRepository $speciesRepository
     * @param BreedRepository $breedRepository
     * @param BreedMapper $breedMapper
     *
     * @DI\InjectParams({
     *  "speciesRepository" = @DI\Inject("zoolyx_core.repository.species"),
     *  "breedRepository" = @DI\Inject("zoolyx_core.repository.breed"),
     *  "breedMapper" = @DI\Inject("zoolyx_core.mapper.breed")
     * })
     */
    public function __construct(BreedRepository $breedRepository, BreedMapper $breedMapper, SpeciesRepository $speciesRepository) {
        $this->speciesRepository = $speciesRepository;
        $this->breedRepository = $breedRepository;
        $this->breedMapper = $breedMapper;
    }

    /**
     * @param string $language
     * @return $this
     */
    public function setLanguage($language) {
        $this->language = $language;
        return $this;
    }

    /**
     * @param string $speciesCode
     * @return $this
     */
    public function setSpeciesCode($speciesCode) {
        $this->speciesCode = $speciesCode;
        return $this;
    }

    public function getBreeds() {
        $species = $this->speciesRepository->findOneBy(array('speciesId'=>$this->speciesCode));
        if (is_null($species)) {
            return array();
        }
        $breedEntities = $this->breedRepository->findBy(array('species'=>$species->getId()));

        $breeds = array();
        /** @var BreedEntity $breedEntity */
        foreach ($breedEntities as $breedEntity) {
            if ($breed = $this->breedMapper->map($breedEntity, $this->language)) {
                $breeds[] = $breed;
            }
        }
        usort($breeds, array($this, "cmp"));
        return $breeds;
    }

    /**
     * @param Breed $a
     * @param Breed $b
     * @return int
     */
    private function cmp($a, $b)
    {
        return strcmp($a->getDescription(), $b->getDescription());
    }


} 