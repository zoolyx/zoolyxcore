<?php

namespace Zoolyx\CoreBundle\Model\Lims;
use Zoolyx\CoreBundle\Entity\ParameterProfileSampleTypeDefinition;

/**
 * ParameterProfileSampleType
 */
class ParameterProfileSampleType
{
    /** @var integer */
    private $id;

    /** @var string */
    private $minimumVolume;

    /** @var boolean */
    private $preferred;

    /** @var ParameterProfileSampleTypeDefinition */
    private $definition;

    /** @var String */
    private $comment;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMinimumVolume() {
        return $this->minimumVolume;
    }

    /**
     * @param string $minimumVolume
     * @return ParameterProfileSampleType
     */
    public function setMinimumVolume($minimumVolume) {
        $this->minimumVolume = $minimumVolume;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPreferred() {
        return $this->preferred;
    }

    /**
     * @param bool $preferred
     * @return ParameterProfileSampleType
     */
    public function setPreferred($preferred) {
        $this->preferred = $preferred;
        return $this;
    }

    /**
     * @return ParameterProfileSampleTypeDefinition
     */
    public function getDefinition() {
        return $this->definition;
    }

    /**
     * @param ParameterProfileSampleTypeDefinition $definition
     * @return ParameterProfileSampleType
     */
    public function setDefinition($definition) {
        $this->definition = $definition;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }


}

