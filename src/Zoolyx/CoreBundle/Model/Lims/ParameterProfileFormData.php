<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Lims;


class ParameterProfileFormData {


    private $formData = array();

    /*
     * $formdata looks like this
     * array (size=7)
          0 =>
            object(stdClass)[800]
              public 'name' => string 'pe.name' (length=7)
              public 'value' => string 'Blackie' (length=7)
          1 =>
            object(stdClass)[801]
              public 'name' => string 'pe.chipId' (length=9)
              public 'value' => string '392141000491376' (length=15)
          2 =>
            object(stdClass)[802]
              public 'name' => string 'pe.breedId' (length=10)
              public 'value' => string '' (length=0)
     */
    public function addFormData($formId, $formData) {
        if (!isset($this->formData[$formId])) $this->formData[$formId] = array();

        foreach ($formData as $formElement) {
            $name = $formElement->name;
            $value = $formElement->value;
            $this->formData[$formId][$name] = $value;
        }
    }

    public function getFormData() {
        return $this->formData;
    }

} 