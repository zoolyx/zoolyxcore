<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Lims;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\ParameterBase;
use Zoolyx\CoreBundle\Entity\ParameterProfile as ParameterProfileEntity;
use Zoolyx\CoreBundle\Entity\ParameterProfilePrice;
use Zoolyx\CoreBundle\Entity\ParameterProfileSampleType as ParameterProfileSampleTypeEntity;
use Zoolyx\CoreBundle\Entity\User;

/**
 * @DI\Service("zoolyx_core.mapper.parameter_profile")
 */
class ParameterProfileMapper {

    private $parameterProfileSampleTypeMapper;

    /**
     * Constructor.
     *
     * @param ParameterProfileSampleTypeMapper $parameterProfileSampleTypeMapper
     *
     * @DI\InjectParams({
     *  "parameterProfileSampleTypeMapper" = @DI\Inject("zoolyx_core.mapper.parameter_profile_sample_type")
     * })
     */

    public function __construct(ParameterProfileSampleTypeMapper $parameterProfileSampleTypeMapper) {
        $this->parameterProfileSampleTypeMapper = $parameterProfileSampleTypeMapper;
    }

    /**
     * @param ParameterProfileEntity $parameterProfileEntity
     * @param User $user
     * @param string $language
     * @param string $billing
     * @return ParameterProfile
     */
    public function map($parameterProfileEntity, $user, $language = 'nl' , $billing = null) {
        $parameterProfile = new ParameterProfile();
        $parameterProfile->setEntity($parameterProfileEntity);
        $parameterProfile->setReference($parameterProfileEntity->getReference());
        $parameterProfile->setSpecies($parameterProfileEntity->getSpecies());
        $parameterProfile->setDescription($parameterProfileEntity->getDescription($language));

        $parameterDescriptions2 = array();
        $parameters = array();
        foreach ($parameterProfileEntity->getParameters() as $parameter) {
            $parameters[] = $parameter;
        }
        usort($parameters, array($this,'compareParameters'));
        /** @var ParameterBase $parameterBase */
        foreach ($parameters as $parameterBase) {
            $description = $parameterBase->getDescription2($language);
            if ($description && $description->getDescription()!= '') {
                $parameterDescriptions2[] = $description->getDescription();

            }
        }
        $parameterProfile->setDescription2(implode(', ',$parameterDescriptions2));

        $priceList = $parameterProfileEntity->getPriceList();
        /** @var ParameterProfilePrice $price */
        foreach ($priceList as $price) {
            if ($price->getBilling() == $billing) {
                $parameterProfile->setPrice($price);
            }
        }
        if (is_null($parameterProfile->getPrice())) {
            foreach ($priceList as $price) {
                if ($price->getBilling() == null) {
                    $parameterProfile->setPrice($price);
                }
            }
        }
        $parameterProfile->setUrl($parameterProfileEntity->getUrl());
        $parameterProfile->setParameters($parameterProfileEntity->getParameters());

        $parameterProfile->setSampleTypesDisabled(true);
        $sampleTypes = array();
        /** @var ParameterProfileSampleTypeEntity $sampleTypeEntity */
        foreach ($parameterProfileEntity->getSampleTypes() as $sampleTypeEntity) {
            if ($user->hasRole('ROLE_OWNER') && !$sampleTypeEntity->getDefinition()->ownerCanOrder()) {
                continue;
            }
            $sampleTypes[] = $this->parameterProfileSampleTypeMapper->map($sampleTypeEntity);
            if (!$sampleTypeEntity->isPreferred())
                $parameterProfile->setSampleTypesDisabled(false);
        }
        $parameterProfile->setSampleTypes($sampleTypes);

        $parameterProfile->setBreeds($parameterProfileEntity->getBreeds());
        $parameterProfile->setForm($parameterProfileEntity->getForm());
        return $parameterProfile;
    }

    /**
     * @param ParameterBase $parameter1
     * @param ParameterBase $parameter2
     * @return int
     */
    private static function compareParameters($parameter1, $parameter2)
    {
        return $parameter1->getSequence() > $parameter2->getSequence();
    }
} 