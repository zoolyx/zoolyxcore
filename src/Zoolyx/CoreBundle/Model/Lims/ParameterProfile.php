<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Lims;

use Zoolyx\CoreBundle\Entity\ParameterProfileDescription;
use Zoolyx\CoreBundle\Entity\ParameterProfileForm;
use Zoolyx\CoreBundle\Entity\ParameterProfilePrice;
use Zoolyx\CoreBundle\Entity\ParameterProfile as ParameterProfileEntity;

class ParameterProfile {

    /** @var ParameterProfileEntity */
    private $entity;

    /** @var string */
    private $reference;

    /** @var string */
    private $species;

    /** @var ParameterProfileDescription */
    private $description;

    /** @var string */
    private $description2;

    /** @var ParameterProfilePrice */
    private $price;

    /** @var string */
    private $url = null;

    /** @var array */
    private $parameters = array();

    /** @var array */
    private $sampleTypes = array();

    /** @var bool  */
    private $sampleTypesDisabled = false;

    /** @var array */
    private $breeds = array();

    /** @var ParameterProfileForm */
    private $form = null;

    /**
     * @return ParameterProfileEntity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param ParameterProfileEntity $entity
     * @return $this
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
        return $this;
    }





    /**
     * @return string
     */
    public function getReference() {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return ParameterProfile
     */
    public function setReference($reference) {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpecies() {
        return $this->species;
    }

    /**
     * @param string $species
     * @return ParameterProfile
     */
    public function setSpecies($species) {
        $this->species = $species;
        return $this;
    }

    /**
     * @return ParameterProfileDescription
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param ParameterProfileDescription $description
     * @return ParameterProfile
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription2() {
        return $this->description2;
    }

    /**
     * @param string $description
     * @return ParameterProfile
     */
    public function setDescription2($description) {
        $this->description2 = $description;
        return $this;
    }

    /**
     * @return ParameterProfilePrice
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * @param ParameterProfilePrice $price
     * @return $this
     */
    public function setPrice($price) {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * @param string $url
     * @return ParameterProfile
     */
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }

    /**
     * @return array
     */
    public function getParameters() {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     * @return ParameterProfile
     */
    public function setParameters($parameters) {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * @return array
     */
    public function getSampleTypes() {
        return $this->sampleTypes;
    }

    /**
     * @param array $sampleTypes
     * @return ParameterProfile
     */
    public function setSampleTypes($sampleTypes) {
        $this->sampleTypes = $sampleTypes;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isSampleTypesDisabled()
    {
        return $this->sampleTypesDisabled;
    }

    /**
     * @param boolean $sampleTypesDisabled
     * @return $this
     */
    public function setSampleTypesDisabled($sampleTypesDisabled)
    {
        $this->sampleTypesDisabled = $sampleTypesDisabled;
        return $this;
    }

    /**
     * @return array
     */
    public function getBreeds() {
        return $this->breeds;
    }

    /**
     * @param array $breeds
     * @return $this
     */
    public function setBreeds($breeds) {
        $this->breeds = $breeds;
        return $this;
    }

    /**
     * @return ParameterProfileForm
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param ParameterProfileForm $form
     * @return $this
     */
    public function setForm($form)
    {
        $this->form = $form;
        return $this;
    }


} 