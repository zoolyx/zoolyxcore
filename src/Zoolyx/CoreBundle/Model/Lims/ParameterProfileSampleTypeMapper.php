<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Lims;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\ParameterProfileSampleType as ParameterProfileSampleTypeEntity;
use Zoolyx\CoreBundle\Entity\ParameterProfilePrice;

/**
 * @DI\Service("zoolyx_core.mapper.parameter_profile_sample_type")
 */
class ParameterProfileSampleTypeMapper {

    /**
     * @param ParameterProfileSampleTypeEntity $parameterProfileSampleTypeEntity
     * @param string $language
     * @return ParameterProfileSampleType
     */
    public function map($parameterProfileSampleTypeEntity, $language = 'nl') {
        $parameterProfileSampleType = new ParameterProfileSampleType();
        $parameterProfileSampleType->setMinimumVolume($parameterProfileSampleTypeEntity->getMinimumVolume());
        $parameterProfileSampleType->setPreferred($parameterProfileSampleTypeEntity->isPreferred());
        $parameterProfileSampleType->setDefinition($parameterProfileSampleTypeEntity->getDefinition($language));

        if ($parameterProfileSampleTypeEntity->getComment()) {
            $parameterProfileSampleType->setComment($parameterProfileSampleTypeEntity->getComment()->getDescription($language)->getDescription());
        } else {
            $parameterProfileSampleType->setComment('');
        }
        return $parameterProfileSampleType;
    }
} 