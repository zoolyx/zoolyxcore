<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Lims;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Zoolyx\CoreBundle\Entity\ParameterProfile as ParameterProfileEntity;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;

/**
 * @DI\Service("zoolyx_core.manager.parameter_profile")
 */
class ParameterProfileManager {

    /** @var string */
    private $language = 'nl';
    /** @var string */
    private $speciesCode = 'H';
    /** @var string */
    private $billing;
    /** @var User */
    private $requestor;
    

    /** @var ParameterProfileRepository */
    private $parameterProfileRepository;
    /** @var ParameterProfileCategoryMapper */
    private $parameterProfileCategoryMapper;
    /** @var ParameterProfileMapper */
    private $parameterProfileMapper;

    /** @var VeterinaryRepository */
    private $veterinaryRepository;
    
    /**
     * Constructor.
     *
     * @param ParameterProfileRepository $parameterProfileRepository
     * @param ParameterProfileMapper $parameterProfileMapper
     * @param ParameterProfileCategoryMapper $parameterProfileCategoryMapper
     * @param TokenStorage $tokenStorage
     * @param veterinaryRepository $veterinaryRepository
     *
     *
     * @DI\InjectParams({
     *  "parameterProfileRepository" = @DI\Inject("zoolyx_core.repository.parameter_profile"),
     *  "parameterProfileMapper" = @DI\Inject("zoolyx_core.mapper.parameter_profile"),
     *  "parameterProfileCategoryMapper" = @DI\Inject("zoolyx_core.mapper.parameter_profile_category"),
     *  "tokenStorage" = @DI\Inject("security.token_storage"),
     *  "veterinaryRepository" = @DI\Inject("zoolyx_core.repository.veterinary"),
     * })
     */
    public function __construct(
    	ParameterProfileRepository $parameterProfileRepository, 
    	ParameterProfileMapper $parameterProfileMapper, 
    	ParameterProfileCategoryMapper $parameterProfileCategoryMapper, 
    	TokenStorage $tokenStorage,
    	veterinaryRepository $veterinaryRepository
    ) {
        $this->parameterProfileRepository = $parameterProfileRepository;
        $this->parameterProfileCategoryMapper = $parameterProfileCategoryMapper;
        $this->parameterProfileMapper = $parameterProfileMapper;
        $this->requestor = $tokenStorage->getToken()->getUser();
        $this->veterinaryRepository = $veterinaryRepository;
    }

    /**
     * @param string $language
     * @return $this
     */
    public function setLanguage($language) {
        $language = strtolower($language);
        if (in_array($language,array("nl","fr","en")))
        {
            $this->language = $language;
        }
        return $this;
    }

    /**
     * @param string $speciesCode
     * @return $this
     */
    public function setSpeciesCode($speciesCode) {
        $this->speciesCode = $speciesCode;
        return $this;
    }

    /**
     * @param string $billing
     * @return $this
     */
    public function setBilling($billing) {
        $this->billing = $billing;
        return $this;
    }

    /**
     * @param User $requestor
     * @return $this
     */
    public function setRequestor($requestor) {
        $this->requestor = $requestor;
        return $this;
    }

    public function getParameterProfileCategories() {
        $profiles = $this->parameterProfileRepository->findBy(array('species'=> $this->speciesCode));
        $categories = array();
        /** @var ParameterProfileEntity $profileEntity */
        foreach ($profiles as $profileEntity) {
            if ($this->requestor->hasRole('ROLE_VETERINARY')) {
                if (!$profileEntity->veterinaryCanOrder()) {
                    continue;
                }  
            } elseif ($this->requestor->hasRole('ROLE_OWNER')) {
				if (!$profileEntity->ownerCanOrder()){
					continue;
				}
			} 

            $categoryEntity = $profileEntity->getCategory();
            if (!is_null($profileEntity->getReservedFor()) && $this->requestor->hasRole('ROLE_VETERINARY') ) {
                $veterinary = $this->veterinaryRepository->findOneBy(array(
          		  'account' => $this->requestor
       			 ));
       			 $match = false;
                
                /** @var VeterinaryPractice $veterinaryPractice */
            	foreach ($veterinary->getVeterinaryPractices() as $veterinaryPractice) {
                	if ($profileEntity->getReservedFor() == $veterinaryPractice->getLimsId() ) {
                	    $match = true;
                	}
                }
                
                if (!$match) { 
                	continue;
                }
            }
            $categoryId = $categoryEntity->getId();

            if (!isset($categories[$categoryId])) {
                $category = $this->parameterProfileCategoryMapper->map($categoryEntity,$this->language);
                $categories[$categoryId] = $category;
            } else {
                $category = $categories[$categoryId];
            }
            $category->addParameterProfile($this->parameterProfileMapper->map($profileEntity, $this->requestor, $this->language, $this->billing));
        }
        return $categories;
    }

    public function getParameterProfiles($profileReferences) {
        $parameterProfiles = array();
        foreach($profileReferences as $profileReference) {
            /** @var ParameterProfileEntity $profileEntity */
            $profileEntity = $this->parameterProfileRepository->findOneBy(array('reference'=>$profileReference));
            $parameterProfiles[] = $this->parameterProfileMapper->map($profileEntity, $this->requestor, $this->language, $this->billing);
        }
        return $parameterProfiles;
    }


} 