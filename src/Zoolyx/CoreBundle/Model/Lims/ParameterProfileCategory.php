<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Lims;

use Zoolyx\CoreBundle\Entity\ParameterProfileCategoryDescription;

class ParameterProfileCategory {

    /** @var string */
    private $reference;
    /** @var ParameterProfileCategoryDescription */
    private $description;
    /** @var array */
    private $parameterProfiles = array();

    /**
     * @return string
     */
    public function getReference() {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference) {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return ParameterProfileCategoryDescription
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param ParameterProfileCategoryDescription $description
     * @return $this
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    /**
     * @return array
     */
    public function getParameterProfiles() {
        return $this->parameterProfiles;
    }

    /**
     * @param ParameterProfile $parameterProfile
     * @return $this
     */
    public function addParameterProfile($parameterProfile) {
        $this->parameterProfiles[] = $parameterProfile;
        return $this;
    }
} 