<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Lims;

class Breed {

    /** @var integer */
    private $id;

    /** @var string */
    private $breedId;

    /** @var string */
    private $description;

    /**
     * @return int
     */
    public function getId()  {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getBreedId() {
        return $this->breedId;
    }

    /**
     * @param string $breedId
     * @return $this
     */
    public function setBreedId($breedId) {
        $this->breedId = $breedId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }
} 