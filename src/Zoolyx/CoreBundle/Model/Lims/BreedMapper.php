<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Lims;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Breed as BreedEntity;

/**
 * @DI\Service("zoolyx_core.mapper.breed")
 */
class BreedMapper {

    /**
     * @param BreedEntity $breedEntity
     * @param string $language
     * @return Breed
     */
    public function map($breedEntity, $language = 'nl') {
        $breed = new Breed();
        $breed->setId($breedEntity->getId());
        $breed->setDescription($breedEntity->getDescription($language));
        $fullBreedId = explode("_",$breedEntity->getBreedId());
        if ($fullBreedId[1]<=0) return null;
        $breed->setBreedId($fullBreedId[1]);
        return $breed;
    }
} 