<?php

namespace Zoolyx\CoreBundle\Model\Exception;

use Exception;

class FtpConnectionFailedException extends Exception
{
    public function __construct($message = 'Could not connect to the ftp server.', Exception $previous = null, $code = 0)
    {
        parent::__construct($message, $code, $previous);
    }
}
