<?php

namespace Zoolyx\CoreBundle\Model\Exception;

use Exception;

class LanguageNotSupportedException extends Exception
{
    public function __construct($message = 'This language is not supported.', Exception $previous = null, $code = 0)
    {
        parent::__construct($message, $code, $previous);
    }
}
