<?php

namespace Zoolyx\CoreBundle\Model\Exception;

use Exception;

class ObjectNotFoundException extends Exception
{
    public function __construct($message = 'object not found.', Exception $previous = null, $code = 0)
    {
        parent::__construct($message, $code, $previous);
    }
}
