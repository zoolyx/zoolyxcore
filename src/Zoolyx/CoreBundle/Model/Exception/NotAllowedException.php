<?php

namespace Zoolyx\CoreBundle\Model\Exception;

use Exception;

class NotAllowedException extends Exception
{
    public function __construct($message = 'not allowed.', Exception $previous = null, $code = 0)
    {
        parent::__construct($message, $code, $previous);
    }
}
