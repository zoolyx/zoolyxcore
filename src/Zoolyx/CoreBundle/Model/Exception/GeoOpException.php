<?php

namespace Zoolyx\CoreBundle\Model\Exception;

use Exception;

class GeoOpException extends Exception
{
    public function __construct($message = 'GeoOp exception.', Exception $previous = null, $code = 0)
    {
        parent::__construct($message, $code, $previous);
    }
}
