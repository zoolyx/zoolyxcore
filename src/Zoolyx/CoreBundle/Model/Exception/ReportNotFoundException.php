<?php

namespace Zoolyx\CoreBundle\Model\Exception;

use Exception;

class ReportNotFoundException extends Exception
{
    public function __construct($message = 'Report not found.', Exception $previous = null, $code = 0)
    {
        parent::__construct($message, $code, $previous);
    }
}
