<?php

namespace Zoolyx\CoreBundle\Model\Exception;

use Exception;

class VersionExistsException extends Exception
{
    public function __construct($message = 'A version with this language already exists.', Exception $previous = null, $code = 0)
    {
        parent::__construct($message, $code, $previous);
    }
}
