<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 08.10.16
 * Time: 17:23
 */
namespace Zoolyx\CoreBundle\Model\Ftp;

use Zoolyx\CoreBundle\Model\Exception\FtpConnectionFailedException;

class FtpService {

    private $ftpHost;
    private $ftpUser;
    private $ftpPassword;
    private $ftpDir;

    private $logDir;

    public function __construct($ftpHost, $ftpUser, $ftpPassword, $ftpDir, $logDir) {
        $this->ftpHost = $ftpHost;
        $this->ftpUser = $ftpUser;
        $this->ftpPassword = $ftpPassword;
        $this->ftpDir = $ftpDir;
        $this->logDir = $logDir;
    }

    public function put($filename, $content) {
        file_put_contents($this->logDir . '/error/' . $filename,$content);

    }

    public function ftp($filename, $content) {
        try {
            $connectionId = ftp_connect($this->ftpHost);
            if (!$connectionId) {
                throw new FtpConnectionFailedException();
            }
            if (! @ftp_login($connectionId, $this->ftpUser, $this->ftpPassword)) {
                throw new FtpConnectionFailedException();
            }

            ftp_pasv($connectionId, true);

            if (!ftp_fput($connectionId, $this->ftpDir . $filename, $content, FTP_BINARY)) {
                throw new FtpConnectionFailedException();
            }

            ftp_close($connectionId);
            return true;
        } catch(FtpConnectionFailedException $e) {
            return false;
        }



    }

}