<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 30.09.16
 * Time: 13:33
 */

namespace Zoolyx\CoreBundle\Model\Report;


use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Model\Exception\LanguageNotSupportedException;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\ReportVersion;
use Zoolyx\CoreBundle\Model\Exception\VersionExistsException;
use Zoolyx\CoreBundle\Model\Ftp\FtpService;
use Zoolyx\CoreBundle\Model\XmlBuilder\XmlBuilder;

/**
 * @DI\Service("zoolyx_core.report.translator")
 */
class Translator {

    private $languageValidator;
    private $xmlBuilder;
    private $ftpService;


    /**
     * Constructor.
     *
     * @param LanguageValidator $languageValidator
     * @param XmlBuilder $xmlBuilder
     * @param FtpService $ftpService
     *
     * @DI\InjectParams({
     *  "languageValidator" = @DI\Inject("zoolyx_core.report.language_validator"),
     *  "xmlBuilder" = @DI\Inject("zoolyx_core.xml_build.builder"),
     * "ftpService" = @DI\Inject("zoolyx_core.ftp.service")
     * })
     */
    public function __construct(LanguageValidator $languageValidator, XmlBuilder $xmlBuilder, FtpService $ftpService) {
        $this->languageValidator = $languageValidator;
        $this->xmlBuilder = $xmlBuilder;
        $this->ftpService = $ftpService;
    }

    /**
     * @param Report $report
     * @param $language
     * @throws LanguageNotSupportedException
     * @throws VersionExistsException
     */
    public function translateTo(Report $report, $language) {

        if (!$this->languageValidator->isSupported($language)) {
            throw new LanguageNotSupportedException();
        }

        //check if language is already available
        $languageAvailable = false;
        /** @var ReportVersion $reportVersion */
        foreach ($report->getVersions() as $reportVersion) {
            if ($reportVersion->getLanguage() == $language) {
                $languageAvailable = true;
            }
        }
        if ($languageAvailable) {
            throw new VersionExistsException();
        }

        $xml = $this->xmlBuilder->build($report->getRequestId(), $language);

        $tempFile = fopen('php://memory', 'r+');
        fputs($tempFile, $xml);
        rewind($tempFile);
        $this->ftpService->put($report->getRequestId() . '_' . time() . '_' . $language . '.xml', $tempFile);

    }

} 