<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 27.09.16
 * Time: 10:34
 */

namespace Zoolyx\CoreBundle\Model\Report;


use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\ReportVersion;
use Zoolyx\CoreBundle\Entity\Repository\ReportRepository;
use Zoolyx\CoreBundle\Entity\Repository\ReportVersionRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Model\Exception\ReportNotFoundException;

/**
 * @DI\Service("zoolyx_core.report.loader")
 */

class ReportLoader {

    /** @var ReportRepository */
    private $reportRepository;

    /** @var ReportVersionRepository */
    private $reportVersionRepository;

    /** @var  LanguageValidator */
    private $languageValidator;


    /**
     * Constructor.
     *
     * @param ReportRepository $reportRepository
     * @param ReportVersionRepository $reportVersionRepository
     * @param LanguageValidator $languageValidator
     *
     * @DI\InjectParams({
     *  "reportRepository" = @DI\Inject("zoolyx_core.repository.report"),
     *  "reportVersionRepository" = @DI\Inject("zoolyx_core.repository.report_version"),
     *  "languageValidator" = @DI\Inject("zoolyx_core.report.language_validator")
     * })
     */
    public function __construct(ReportRepository $reportRepository, ReportVersionRepository $reportVersionRepository, LanguageValidator $languageValidator)
    {
        $this->reportRepository = $reportRepository;
        $this->reportVersionRepository = $reportVersionRepository;
        $this->languageValidator = $languageValidator;
    }

    /**
     * @param ReportHash $hash
     * @return ReportVersion
     * @throws ReportNotFoundException
     */
    public function loadByHash(ReportHash $hash) {
        /** @var ReportVersion $reportVersion */
        $reportVersion = null;

        if ($this->languageValidator->isSupported($hash->getLanguage())) {
            $reportVersion = $this->reportVersionRepository->findOneByHash($hash);
        }

        if (is_null($reportVersion)) {
            /** @var Report $report */
            $report = $this->reportRepository->findOneByHash($hash);
            if ($report) {
                $reportVersion = $report->getFirstVersion();
            } else {
                throw new ReportNotFoundException();
            }

        }
        return $reportVersion;
    }

    /**
     * @param ReportHash $hash
     * @return ReportVersion
     * @throws ReportNotFoundException
     */
    public function loadByAlternativeHash(ReportHash $hash) {
        /** @var ReportVersion $reportVersion */
        $reportVersion = null;

        if ($this->languageValidator->isSupported($hash->getLanguage())) {
            $reportVersion = $this->reportVersionRepository->findOneByAlternativeHash($hash);
        }

        if (is_null($reportVersion)) {
            /** @var Report $report */
            $report = $this->reportRepository->findOneByAlternativeHash($hash);
            if ($report) {
                $reportVersion = $report->getFirstVersion();
            } else {
                throw new ReportNotFoundException();
            }

        }
        return $reportVersion;
    }

    /**
     * @param User $user
     * @param string $reference
     * @return ReportVersion
     * @throws ReportNotFoundException
     */
    public function loadByReference($user, $reference) {
        /** @var ReportVersion $reportVersion */
        $reportVersion = null;

        /** @var Report $report */
        $report = $this->reportRepository->findOneByReference($user, $reference);
        if ($report) {
            $reportVersion = $report->getFirstVersion();
        } else {
            throw new ReportNotFoundException();
        }
        if (is_null($reportVersion)) {
            $reportVersion = $this->reportVersionRepository->findOneByReference($user, $reference);
        }
        return $reportVersion;
    }
} 