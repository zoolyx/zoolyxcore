<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 27.09.16
 * Time: 09:59
 */

namespace Zoolyx\CoreBundle\Model\Report;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Parameter;
use Zoolyx\CoreBundle\Entity\ParameterGroup;
use Zoolyx\CoreBundle\Entity\ReportVersion;
use Zoolyx\CoreBundle\Entity\Sample;

/**
 * @DI\Service("zoolyx_core.report_version.file_manager")
 */
class ReportVersionFileManager {

    public function getFiles(ReportVersion $reportVersion) {
        $files = array();

        /** @var Sample $sample */
        foreach ($reportVersion->getSamples() as $sample) {
            /** @var ParameterGroup $parameterGroup */
            foreach ($sample->getParameterGroups() as $parameterGroup) {
                /** @var Parameter $parameter */
                foreach ($parameterGroup->getParameters() as $parameter) {
                    foreach ($parameter->getFiles() as $file) {
                        $files[] = $file;
                    }
                }
            }
        }

        return $files;
    }
}