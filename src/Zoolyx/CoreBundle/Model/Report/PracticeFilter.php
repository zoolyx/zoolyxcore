<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 24.02.17
 * Time: 17:06
 */

namespace Zoolyx\CoreBundle\Model\Report;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Entity\VeterinaryPracticeReport;

/**
 *
 * This class filters new practices. When a report has multiple new Veterinaries (in copy) then each of them will
 * get their own practice.
 * But if the practiceId is set then we will create a duplicate practice (2 practices with the same practiceId
 *
 * Class PracticeFilter
 * @package Zoolyx\CoreBundle\Model\Report
 */
/**
 * @DI\Service("zoolyx_core.report.practice_filter")
 */
class PracticeFilter {

    /**
     * @param Report $report
     */
    public function filter($report) {
        $veterinariesPracticesInvolved = array();
        $veterinariesPracticesInvolved[] = $report->getVeterinaryPractice();
        /** @var VeterinaryPracticeReport $vpr */
        foreach($report->getVeterinariesInCopy() as $vpr) {
            $veterinariesPracticesInvolved[] = $vpr->getVeterinaryPractice();
        }

        /** @var VeterinaryPractice $veterinaryPractice */
        $newPractices = array();
        foreach ($veterinariesPracticesInvolved as $veterinaryPractice) {
            $practice = $veterinaryPractice->getPractice();

            if (!is_null($practice->getId()))
                continue;

            $practiceId = $practice->getPracticeId();
            if (is_null($practiceId))
                continue;

            if (isset($newPractices[$practiceId])) {
                $veterinaryPractice->setPractice($newPractices[$practiceId]);
            } else {
                $newPractices[$practiceId] = $practice;
            }
        }
    }
} 