<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 27.09.16
 * Time: 11:16
 */

namespace Zoolyx\CoreBundle\Model\Report\Authorisation;


use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Practice;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\ReportShare;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Entity\VeterinaryPracticeReport;
use Zoolyx\CoreBundle\Model\Report\VeterinaryPracticeReport\VeterinaryPracticeReportRoles;

/**
 * @DI\Service("zoolyx_core.report.access_validator")
 */
class AccessValidator {


    /**
     * @param User $user
     * @param Report $report
     *
     * @return bool
     */
    public function canAccess($user, $report) {
        if ($user->hasRole('ROLE_SUPER_ADMIN') || $user->hasRole('ROLE_ADMINISTRATOR') ) {
            return true;
        }

        //check if the report is shared
        /** @var ReportShare $reportShare */
        foreach ($report->getShares() as $reportShare) {
            if ($reportShare->getUser() && $reportShare->getUser()->getId() == $user->getId()) {
                return true;
            }

            if ($reportShare->getOwner() && $reportShare->getOwner()->getAccount() && $reportShare->getOwner()->getAccount()->getId() == $user->getId()) {
                return true;
            }
        }

        if ($user->hasRole('ROLE_OWNER')) {
            if ($report->getOwner() && $report->getOwner()->getAccount() &&
                $report->getOwner()->getAccount()->getId() == $user->getId()
            ) {
                return true;
            }

            if (!$user->hasRole('ROLE_VETERINARY') && !$user->hasRole('ROLE_ORGANISATION')) {
                return false;
            }
        }

        /** @var VeterinaryPracticeReport $vpr */
        foreach ($report->getVeterinaryPracticeReports() as $vpr) {
            if (!$veterinaryPractice = $vpr->getVeterinaryPractice()) continue;
            if ($this->hasAccount($veterinaryPractice,$user)) {
                return true;
            }
        }

        /** @var VeterinaryPracticeReport $vpr */
        foreach ($report->getVeterinaryPracticeReports() as $vpr) {
            $practice = $vpr->getVeterinaryPractice()->getPractice();
            if (is_null($practice))
                continue;
            foreach ($practice->getVeterinaryPractices() as $veterinaryPractice) {
                if ($this->hasAccount($veterinaryPractice,$user))
                    return true;
            }
        }

        return false;
    }

    /**
     * @param Report $report
     * @return array
     */
    public function getAccountsWithAccess($report) {
        $accounts = array();

        /** @var VeterinaryPracticeReport $vpr */
        foreach ($report->getVeterinaryPracticeReports() as $vpr) {
            if ($vpr->getRole() == VeterinaryPracticeReportRoles::VPR_VETERINARY || $vpr->getRole() == VeterinaryPracticeReportRoles::VPR_ORGANISATION) {
                /** @var Practice $practice */
                $practice = $vpr->getVeterinaryPractice()->getPractice();
                /** @var VeterinaryPractice $veterinaryPractice */
                foreach ($practice->getVeterinaryPractices() as $veterinaryPractice) {
                    $veterinary = $veterinaryPractice->getVeterinary();
                    $veterinaryAccount = $veterinary->getAccount();
                    if ($veterinaryAccount) {
                        $accounts[$veterinaryAccount->getId()] = $veterinaryAccount;
                    }
                }
            } else {
                $veterinary = $vpr->getVeterinaryPractice()->getVeterinary();
                $veterinaryAccount = $veterinary->getAccount();
                if ($veterinaryAccount) {
                    $accounts[$veterinaryAccount->getId()] = $veterinaryAccount;
                }
            }

        }

        if ($report->getOwner() && $report->getOwner()->getAccount() ){
            $owner = $report->getOwner();
            $ownerAccount = $owner ? $owner->getAccount() : null;
            if ($ownerAccount) {
                $accounts[$ownerAccount->getId()] = $ownerAccount;
            }
        }

        /** @var ReportShare $rs */
        foreach ($report->getShares() as $rs) {
            $owner = $rs->getOwner();
            $ownerAccount = $owner ? $owner->getAccount() : null;
            if ($ownerAccount) {
                $accounts[$ownerAccount->getId()] = $ownerAccount;
            }

            $user = $rs->getUser();
            if ($user) {
                $accounts[$user->getId()] = $user;
            }
        }

        return $accounts;
    }

    /**
     * @param VeterinaryPractice $veterinaryPractice
     * @param User $user
     * @return bool
     */
    private function hasAccount($veterinaryPractice, $user) {
        if (!$veterinary = $veterinaryPractice->getVeterinary())
            return false;
        if (!$account = $veterinary->getAccount())
            return false;
        if ($account->getId() != $user->getId())
            return false;

        return true;
    }

} 