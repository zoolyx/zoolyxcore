<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 11.03.16
 * Time: 21:17
 */
namespace Zoolyx\CoreBundle\Model\Report;

use stdClass;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Zoolyx\CoreBundle\Entity\GeniusCode;
use Zoolyx\CoreBundle\Entity\Owner;
use Zoolyx\CoreBundle\Entity\Parameter;
use Zoolyx\CoreBundle\Entity\ParameterGroup;
use Zoolyx\CoreBundle\Entity\Pet;
use Zoolyx\CoreBundle\Entity\Practice;
use \Zoolyx\CoreBundle\Entity\Report;
use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\ReportVersion;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\Sample;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Validator;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Model\Calculation\ParameterManager;
use Zoolyx\CoreBundle\Model\Calculation\UnitaryValueManager;
use Zoolyx\CoreBundle\Model\Genius\RuleChecker;
use Zoolyx\CoreBundle\Model\Pet\PetManager;
use Zoolyx\CoreBundle\Model\Report\Authorisation\AccessValidator;
use Zoolyx\CoreBundle\Model\Report\Parameter\Reference\ReferenceCommentFetcher;
use Zoolyx\TicketBundle\Entity\Repository\TicketRepository;
use Zoolyx\TicketBundle\Entity\Ticket;
use Zoolyx\TicketBundle\Entity\WatchTicket;

/**
 * @DI\Service("zoolyx_core.report.builder")
 */
class ReportBuilder {

    /** @var Report */
    public $report;
    /** @var ReportVersion */
    public $version;
    /** @var bool  */
    public $isMultiSample = false;
    /** @var bool  */
    public $isMultiReport = false;
    /** @var Owner */
    public $owner;
    /** @var array  */
    public $samples = array();
    /** @var array  */
    public $allSamples = array();
    /** @var Pet */
    public $pet;
    /** @var string */
    public $hash;
    /** @var string */
    public $alternativeHash;
    /** @var string */
    public $language;
    /** @var array */
    public $supportedLanguages;
    /** @var string */
    public $fee;
    /** @var string */
    public $vat;
    /** @var Object */
    public $pricing;

    /** @var Veterinary */
    public $veterinary;
    /** @var Practice */
    public $practice;


    private $history = array();
    public $codeHistory = array();
    public $dateHistory = array();

    /** @var bool */
    private $isCumulative = false;
    /** @var bool  */
    private $hasMultipleSamples = false;

    /** @var bool  */
    public $hasOriginalRequest = false;
    /** @var bool  */
    public $canHaveAdditionalRequest = true;

    /** @var array */
    public $suggestions = array();


    /** @var UnitaryValueManager  */
    private $unitaryValueManager;
    /** @var ParameterManager  */
    private $parameterManager;
    /** @var  LanguageValidator */
    private $languageValidator;
    /** @var PetManager */
    public $petManager;
    /** @var RuleChecker */
    private $geniusRuleChecker;
    /** @var TokenStorage */
    private $tokenStorage;
    /** @var VeterinaryRepository */
    private $veterinaryRepository;
    /** @var TicketRepository */
    private $ticketRepository;
    /** @var ReferenceCommentFetcher */
    private $referenceCommentFetcher;
    /** @var AccessValidator */
    private $accessValidator;
    /**
     * Constructor.
     *
     * @param UnitaryValueManager $unitaryValueManager
     * @param ParameterManager $parameterManager
     * @param LanguageValidator $languageValidator
     * @param PetManager $petManager
     * @param RuleChecker $ruleChecker
     * @param TokenStorage $tokenStorage
     * @param VeterinaryRepository $veterinaryRepository
     * @param TicketRepository $ticketRepository
     * @param ReferenceCommentFetcher $referenceCommentFetcher
     * @param AccessValidator $accessValidator
     *
     * @DI\InjectParams({
     *  "unitaryValueManager" = @DI\Inject("zoolyx_core.calculation.unitary_value_manager"),
     *  "parameterManager" = @DI\Inject("zoolyx_core.calculation.parameter_manager"),
     *  "languageValidator" = @DI\Inject("zoolyx_core.report.language_validator"),
     *  "petManager" = @DI\Inject("zoolyx_core.pet.manager"),
     *  "ruleChecker" = @DI\Inject("zoolyx_core.genius.rule_checker"),
     *  "tokenStorage" = @DI\Inject("security.token_storage"),
     *  "veterinaryRepository" = @DI\Inject("zoolyx_core.repository.veterinary"),
     *  "ticketRepository" = @DI\Inject("zoolyx_ticket.repository.ticket"),
     *  "referenceCommentFetcher" = @DI\Inject("zoolyx_core.report.parameter_reference_comment_fetcher"),
     *  "accessValidator" = @DI\Inject("zoolyx_core.report.access_validator")
     * })
     */
    public function __construct(UnitaryValueManager $unitaryValueManager,ParameterManager $parameterManager, LanguageValidator $languageValidator, PetManager $petManager, RuleChecker $ruleChecker, TokenStorage $tokenStorage, VeterinaryRepository $veterinaryRepository, TicketRepository $ticketRepository, ReferenceCommentFetcher $referenceCommentFetcher, AccessValidator $accessValidator) {
        $this->unitaryValueManager = $unitaryValueManager;
        $this->parameterManager = $parameterManager;
        $this->languageValidator = $languageValidator;
        $this->petManager = $petManager;
        $this->geniusRuleChecker = $ruleChecker;
        $this->tokenStorage = $tokenStorage;
        $this->veterinaryRepository = $veterinaryRepository;
        $this->ticketRepository = $ticketRepository;
        $this->referenceCommentFetcher = $referenceCommentFetcher;
        $this->accessValidator = $accessValidator;
    }

    /**
     * @param ReportVersion $reportVersion
     * @param int $sampleId
     * @param bool $loadSamples
     * @return ReportBuilder
     */
    public function setReportVersion(ReportVersion $reportVersion, $sampleId = null, $loadSamples = true) {
        $this->version = $reportVersion;
        $this->report = $reportVersion->getReport();
        $this->isMultiSample = $this->report->getMultiSample() == 1;
        $this->isMultiReport = $this->report->getMultiSample() == 2;
        $this->owner = $this->report->getOwner();
        $this->pet = $reportVersion->getSamples()[0]->getPet();
        $this->hash = $this->report->getHash();
        $this->alternativeHash = $this->report->getAlternativeHash();
        $this->language = $reportVersion->getLanguage();
        $this->fee = $this->report->getFee();
        $this->vat = $this->report->getVat();

        $supportedLanguages = $this->languageValidator->getSupportedLanguages();
        //remove the current language
        $index = array_search($this->language,$supportedLanguages);
        if($index !== FALSE){
            unset($supportedLanguages[$index]);
        }
        $this->supportedLanguages = $supportedLanguages;

        if ($this->report->getVeterinaryPractice()){
	        $this->veterinary = $this->report->getVeterinaryPractice()->getVeterinary();
    	    $this->practice = $this->report->getVeterinaryPractice()->getPractice();
		}
		
        $this->allSamples = $this->version->getSamples();
        if ($loadSamples) {
            if ($this->isMultiSample || $this->isMultiReport) {
                $this->samples = array();
                if ($sampleId) {
                    /** @var Sample $sample */
                    foreach ($this->allSamples as $sample) {
                        if ($sample->getId() == $sampleId) {
                            $this->samples[] = $sample;
                        }
                    }
                } else {
                    $this->samples = $this->allSamples;
                }
            } else {
                $this->samples = $this->allSamples;
            }
        }

        if (count($this->samples)) {
            $this->pet = $this->samples[0]->getPet();
        }
        if (count($this->samples) == 1) {
            /** @var Sample $currentSample */
            $currentSample = $this->samples[0];
            if (!is_null($currentSample->getDateCompleted())) {
                $this->isCumulative = true;
                $petSamples = $this->pet->getSamples();

                /** @var Sample $mostRecentSample */
                $mostRecentSample = null;
                /** @var Sample $secondMostRecentSample */
                $secondMostRecentSample = null;
                /** @var Sample $petSample */
                foreach ($petSamples as $petSample) {
                    if (is_null($petSample->getDateCompleted())) {
                        continue;
                    }
                    if ($this->samples[0]->getDateCompleted() > $petSample->getDateCompleted()) {
                        if ($mostRecentSample) {
                            if ($petSample->getDateCompleted() > $mostRecentSample->getDateCompleted()) {
                                $secondMostRecentSample = $mostRecentSample;
                                $mostRecentSample = $petSample;
                            } else {
                                if ($secondMostRecentSample) {
                                    if ($petSample->getDateCompleted() > $secondMostRecentSample->getDateCompleted()) {
                                        $secondMostRecentSample = $petSample;
                                    }
                                } else {
                                    $secondMostRecentSample = $petSample;
                                }
                            }
                        } else {
                            $mostRecentSample = $petSample;
                        }
                    }
                }

                if ($mostRecentSample) $this->history[] = $mostRecentSample->getReportVersion()->getReport();
                if ($secondMostRecentSample) $this->history[] = $secondMostRecentSample->getReportVersion()->getReport();

                $this->codeHistory = $this->getCodeHistory();
                $this->dateHistory = $this->getDateHistory();
            }
        } else {
            $this->hasMultipleSamples = true;
        }

        $this->hasOriginalRequest = $this->report->getOriginalRequest() ? true : false;
        return $this;
    }

    public function setLanguage($language) {
        $this->language = $language;
        $this->parameterManager->setLanguage($language);
        return $this;
    }

    public function canSeePrice() {
        if ($this->report->getBilling() == '') return true;

        /** @var User $viewer */
        $viewer = $this->tokenStorage->getToken()->getUser();
        if (is_null($viewer)) return false;
        if ($viewer->hasRole('ROLE_ADMINISTRATOR')) return true;
        if ($viewer->hasRole('ROLE_VETERINARY') or $viewer->hasRole('ROLE_ORGANISATION')) {
            $veterinaries = $this->veterinaryRepository->findBy(array('account'=>$viewer));
            /** @var Veterinary $veterinary */
            foreach ($veterinaries as $veterinary) {
                /** @var VeterinaryPractice $veterinaryPractice */
                foreach ($veterinary->getVeterinaryPractices() as $veterinaryPractice) {
                    if ($veterinaryPractice->getVat() && $veterinaryPractice->getVat()->getFaId() == $this->report->getBilling()) return true;
                }

            }
        }
        return false;
    }

    public function displayingSample(Sample $sample) {
        $displaying = false;
        /** @var Sample $s */
        foreach ($this->samples as $s) {
            $displaying = $s->getId() == $sample->getId() ? true : $displaying;
        }
        return $displaying;
    }

    public function calculatePrices() {
        $this->pricing = new stdClass();

        $totalPrice = 0;
        /** @var Sample $sample */
        $this->pricing->samples = array();
        foreach ($this->version->getSamples() as $sample) {
            $price = 0;
            /** @var ParameterGroup $parameterGroup */
            foreach ($sample->getParameterGroups() as $parameterGroup) {
                /** @var Parameter $parameter */
                foreach ($parameterGroup->getParameters() as $parameter) {
                    $price+= floatval(str_replace(',', '.',$parameter->getPrice()));
                }
            }
            $this->pricing->samples[$sample->getId()] = $price;
            $totalPrice+= $price;
        }
        $this->pricing->fee = floatval(str_replace(',', '.',$this->report->getFee()));
        $totalPrice+= $this->pricing->fee;
        $this->pricing->total = $totalPrice;
        $this->pricing->vat = 0.01 * $this->report->getVat() * $totalPrice;
        $this->pricing->totalInclusive = $this->pricing->total + $this->pricing->vat;
    }
    public function getPriceTotal() {
        return $this->pricing->total;
    }
    public function getPriceVat() {
        return $this->pricing->vat;
    }
    public function getPriceTotalInclusive() {
        return $this->pricing->totalInclusive;
    }
    public function getPriceForSample(Sample $sample) {
        return isset($this->pricing->samples[$sample->getId()]) ? $this->pricing->samples[$sample->getId()] : '';
    }
    public function getFee() {
        return $this->pricing->fee;
    }

    /**
     * @return bool
     */
    public function isCumulative() {
        return $this->isCumulative;
    }

    public function getReportType() {
        if ($this->hasMultipleSamples) {
            return "Multiple Samples";
        } else {
            return "";
        }
    }

    public function hasHistory($index) {
        return $this->isCumulative && isset($this->history[$index]);
    }
    public function getCodeHistory() {
        $codeHistory = array();
        foreach ($this->history as $historyReport) {
            /** @var Report $historyReport */
            $samples = $historyReport->getFirstVersion()->getSamples();
            /* @var Sample $firstSample */
            $firstSample = $samples[0];
            $codeHistory[] = $firstSample->getCode();
        }
        return $codeHistory;
    }

    public function getDateHistory() {
        $dateHistory = array();
        foreach ($this->history as $historyReport) {
            /** @var Report $historyReport */
            $samples = $historyReport->getFirstVersion()->getSamples();
            /* @var Sample $firstSample */
            $firstSample = $samples[0];
            $dateHistory[] = $firstSample->getDateReceived();
        }
        return $dateHistory;
    }

    public function getResultHistory(Parameter $parameter) {
        $resultHistory = array();
        foreach ($this->history as $historyReport) {
            $resultHistory[] = $this->findResult($parameter, $historyReport);
        }
        return $resultHistory;
    }

    private function findResult(Parameter $searchParameter, Report $report) {
        $resultFound = '';
        $resultFlag = '';
        /** @var Sample $sample */
        foreach ($report->getDefaultVersion()->getSamples() as $sample) {
            /** @var ParameterGroup $parameterGroup */
            foreach ($sample->getParameterGroups() as $parameterGroup) {
                /** @var Parameter $parameter */
                foreach ($parameterGroup->getParameters() as $parameter) {
                    if ($parameter->getParameterId() == $searchParameter->getParameterId()) {
                        $resultFound = $parameter->getResultString();
                        $resultFlag = strtoupper($parameter->getResultFlag());
                    }
                }
            }
        }
        $exceeds =  $resultFlag==ParameterManager::HIGH || $resultFlag==ParameterManager::LOW || $resultFlag==ParameterManager::VERY_HIGH || $resultFlag==ParameterManager::VERY_LOW;
        return array($resultFound,$exceeds);
    }
    /**
     * @param Parameter $parameter
     * @return UnitaryValueManager
     */
    public function getUnitaryValueManager(Parameter $parameter) {
        $this->unitaryValueManager->setParameter($parameter);
        return $this->unitaryValueManager;
    }

    /**
     * @param Parameter $parameter
     * @return ParameterManager
     */
    public function getParameterManager(Parameter $parameter) {
        $this->parameterManager
            ->setLanguage($this->language)
            ->setParameter($parameter);
        return $this->parameterManager;
    }

    /**
     * @param Pet $pet
     * @return string
     */
    public function petSpeciesName(Pet $pet = null) {
        return $this->petManager->getSpeciesName($pet ? $pet : $this->pet);
    }

    /**
     * @return array
     */
    public function getValidators() {
        $validators = array();
        /** @var Sample $sample */
        foreach ($this->version->getSamples() as $sample) {
            /** @var ParameterGroup $parameterGroup */
            foreach ($sample->getParameterGroups() as $parameterGroup) {
                /** @var Parameter $parameter */
                foreach ($parameterGroup->getParameters() as $parameter) {
                    $validatedBy = $parameter->getValidatedBy();
                    if ($validatedBy) {
                        $foundValidator = false;
                        /** @var Validator $validator */
                        foreach($validators as $validator) {
                            if ($validator->getId() == $validatedBy->getId()) {
                                $foundValidator = true;
                                break;
                            }
                        }
                        if (!$foundValidator) {
                            $validators[] = $validatedBy;
                        }
                    }
                }
            }
        }
        return $validators;
    }

    public function getFirstSampleId() {
        /** @var Sample $sample */
        $sample = count($this->samples) ? $this->samples[0] : null;
        return $sample ? $sample->getId() : 0;
    }

    public function getSample($id) {
        $sample = null;
        /** @var Sample $s */
        foreach ($this->allSamples as $s) {
            $sample = $s->getId() == $id ? $s : $sample;
        }
        return $sample;
    }

    public function getDateReceived(Report $report) {
        $samples = $report->getDefaultVersion()->getSamples();

        $dateReceived = null;
        /** @var Sample $sample */
        foreach ($samples as $sample) {
            if ( is_null($dateReceived) || $dateReceived < $sample->getDateReceived()) {
                $dateReceived = $sample->getDateReceived();
            }
        }
        return $dateReceived;
    }

    public function loadSuggestions() {
        /** @var Sample $sample */
        foreach ($this->samples as $sample) {
            $this->suggestions[$sample->getId()] = $this->geniusRuleChecker->getSuggestionsForSample($sample);
        }

        //sort suggestions
        foreach ($this->suggestions as $sampleId => $suggestions) {
            usort($suggestions, array($this, "compareGenius"));
            $this->suggestions[$sampleId] = $suggestions;
        }
    }
    public function getSuggestions($sampleId) {
        return isset($this->suggestions[$sampleId]) ? $this->suggestions[$sampleId] : null;
    }

    public function getReferenceComment(Sample $sample,Parameter $parameter) {
        return $this->referenceCommentFetcher->get($sample, $parameter, $this->language);
    }

    public function hasQuestion(Report $report) {
        /** @var User $viewer */
        $viewer = $this->tokenStorage->getToken()->getUser();
        $ticket = $this->ticketRepository->findOneBy(array(
            'report' => $report->getId(),
            'owner' => $viewer->getId()
        ));
        return $ticket;
    }
    public function QuestionHasNewEvent(Report $report) {
        /** @var User $viewer */
        $viewer = $this->tokenStorage->getToken()->getUser();

        /** @var Ticket $ticket */
        $ticket = $this->hasQuestion($report);
        /** @var WatchTicket $watcher */
        foreach ($ticket->getWatchers() as $watcher) {
            if ($watcher->getUser()->getId() == $viewer->getId() && $watcher->hasNew()) {
                return true;
            }
        }
        return false;
    }

    private function compareGenius(GeniusCode $a, GeniusCode $b)
    {
        $string1 = $a->getParameterProfile()->getDescription($this->language)->getDescription();
        $string2 = $b->getParameterProfile()->getDescription($this->language)->getDescription();
        return strcmp($string1, $string2);
    }

    public function getRemainingAccountsWithAccess() {
        $accountsWithAccess = $this->accessValidator->getAccountsWithAccess($this->report);
        $accountsWithTicket = array();
        /** @var Ticket $ticket */
        foreach ($this->report->getTickets() as $ticket) {
            $accountsWithTicket[$ticket->getOwner()->getId()] = $ticket->getOwner();
        }
        $remainingAccountsWithAccess = array();
        /** @var User $accountWithAccess */
        foreach ($accountsWithAccess as $accountWithAccess) {
            if (!isset($accountsWithTicket[$accountWithAccess->getId()])) {
                $remainingAccountsWithAccess[] = $accountWithAccess;
            }
        }
        return $remainingAccountsWithAccess;
    }
} 