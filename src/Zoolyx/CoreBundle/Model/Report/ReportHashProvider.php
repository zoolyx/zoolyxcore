<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 27.09.16
 * Time: 12:10
 */

namespace Zoolyx\CoreBundle\Model\Report;

use JMS\DiExtraBundle\Annotation as DI;

class ReportHashProvider {

    const CHARACTER_SET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    private $hashLength;

    public function __construct($hashLength) {
        $this->hashLength = $hashLength;
    }

    public function get() {
        srand();
        $characterSetLength = strlen(self::CHARACTER_SET);
        $randomString = '';
        for ($i=0; $i<$this->hashLength; $i++) {
            $randomString.= substr(self::CHARACTER_SET,rand(0,$characterSetLength-1),1);
        }
        return $randomString;
    }

    public function getHash($length) {
        $this->hashLength = $length;
        return $this->get();
    }
} 