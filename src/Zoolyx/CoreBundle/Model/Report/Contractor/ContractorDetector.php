<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 27.09.16
 * Time: 10:34
 */

namespace Zoolyx\CoreBundle\Model\Report\Contractor;


use Zoolyx\CoreBundle\Entity\Parameter;
use Zoolyx\CoreBundle\Entity\ParameterGroup;
use Zoolyx\CoreBundle\Entity\Report;
use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Repository\ContractorRepository;
use Zoolyx\CoreBundle\Entity\Sample;

/**
 * @DI\Service("zoolyx_core.report.contractor_detector")
 */

class ContractorDetector {

    /** @var ContractorRepository  */
    private $contractorRepository;

    /**
     * Constructor.
     *
     * @param ContractorRepository $contractorRepository
     *
     * @DI\InjectParams({
     *  "contractorRepository" = @DI\Inject("zoolyx_core.repository.contractor")
     * })
     */
    public function __construct( ContractorRepository $contractorRepository ) {
        $this->contractorRepository = $contractorRepository;
    }



    /**
     * @param Report $report
     * @return array
     */
    public function getInvolvedContractor(Report $report) {
        $contractorIds = array();
        //print_r($report->getVersion("nl")); die();
        /** @var Sample $sample */
        foreach ($report->getDefaultVersion()->getSamples() as $sample) {
            /** @var ParameterGroup $parameterGroup */
            foreach ($sample->getParameterGroups() as $parameterGroup) {
                /** @var Parameter $parameter */
                foreach ($parameterGroup->getParameters() as $parameter) {
                    if ($parameter->getResultString() == '') {
                        $contractorRequired = $parameter->getContractorRequired();
                        if (!is_null($contractorRequired)) {
                            $contractorId = $contractorRequired->getContractorId();
                            if (!in_array($contractorId, $contractorIds)) {
                                $contractorIds[] = $contractorId;
                            }
                        }
                    }
                }
            }
        }
        $contractors = array();
        foreach ($contractorIds as $contractorId) {
            $contractor = $this->contractorRepository->findOneBy(array('contractorId' =>$contractorId));
            if (!is_null($contractor)) {
                $contractors[] = $contractor;
            }
        }
        return $contractors;
    }
} 