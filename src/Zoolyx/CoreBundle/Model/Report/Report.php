<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 25.04.17
 * Time: 10:51
 */

namespace Zoolyx\CoreBundle\Model\Report;

use Zoolyx\CoreBundle\Entity\Report as ReportEntity;
use Zoolyx\CoreBundle\Entity\Sample;

class Report {

    /** @var ReportEntity */
    private $report = null;

    public function __construct(ReportEntity $report) {
        $this->report = $report;
    }

    /**
     * @return ReportEntity
     */
    public function getReport() {
        return $this->report;
    }

    public function getHash($language='') {
        return $this->report->getHash().$language;
    }

    /**
     * @param null $language
     * @return bool
     */
    public function hasVersion($language = null) {
        if ($language) {
            foreach($this->report->getVersions() as $version) {
                if ($version->getLanguage() == $language) {
                    return true;
                }
            }
            return false;
        } else {
            return count($this->report->getVersions()) > 0;
        }
    }

    public function getDefaultVersion() {
        if ($this->hasVersion()) return $this->report->getVersions()[0];
        return null;
    }



    public function hasSample() {
        if ($this->hasVersion()) {
            return count($this->getDefaultVersion()->getSamples()) > 0;
        }
        return false;
    }

    public function hasMultipleSamples() {
        if ($this->hasVersion()) {
            return count($this->getDefaultVersion()->getSamples()) > 1;
        }
        return false;
    }

    public function getDefaultSample() {
        if ($this->hasSample()) {
            return $this->getDefaultVersion()->getSamples()[0];
        }
        return null;
    }

    public function getDateReceived() {
        /** @var Sample $defaultSample */
        $defaultSample = $this->getDefaultSample();
        if ($defaultSample) {
            return $defaultSample->getDateReceived();
        }
        return '';
    }
    public function getDateCompleted() {
        /** @var Sample $defaultSample */
        $defaultSample = $this->getDefaultSample();
        if ($defaultSample) {
            return $defaultSample->getDateCompleted();
        }
        return '';
    }

    public function getCreatedOn() {
        return $this->report->getCreatedOn();
    }

} 