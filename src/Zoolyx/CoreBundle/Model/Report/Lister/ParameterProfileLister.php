<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 27.09.16
 * Time: 11:16
 */

namespace Zoolyx\CoreBundle\Model\Report\Lister;


use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Parameter;
use Zoolyx\CoreBundle\Entity\ParameterProfile as ParameterProfileEntity;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileRepository;

/**
 * @DI\Service("zoolyx_core.report.parameter_profile_lister")
 */
class ParameterProfileLister {

    /** @var ParameterProfileRepository */
    private $parameterProfileRepository;

    /**
     * Constructor.
     *
     * @param ParameterProfileRepository $parameterProfileRepository
     *
     * @DI\InjectParams({
     *  "parameterProfileRepository" = @DI\Inject("zoolyx_core.repository.parameter_profile")
     * })
     */
    public function __construct(ParameterProfileRepository $parameterProfileRepository) {
        $this->parameterProfileRepository = $parameterProfileRepository;
    }
    /**
     * @param $parameters
     * @return mixed
     */
    public function getParameterProfiles($parameters)
    {
        $parameterProfiles = array();
        /** @var Parameter $parameter */
        foreach ($parameters as $parameter) {
            $reference = $parameter->getParameterP();
            if ($reference) {
                $parameterProfile = null;
                if (isset($parameterProfiles[$reference])) {
                    $parameterProfile = $parameterProfiles[$reference];
                } else {
                    /** @var ParameterProfileEntity $parameterProfileEntity */
                    $parameterProfileEntity = $this->parameterProfileRepository->findOneBy(array('reference'=>$reference));
                    if ($parameterProfileEntity) {
                        $parameterProfile = new ParameterProfile();
                        $parameterProfile->setReference($reference);
                        $parameterProfile->setParameterProfile($parameterProfileEntity);
                        $parameterProfiles[$reference] = $parameterProfile;
                    }
                }
                if ($parameterProfile)
                    $parameterProfile->addPrice($parameter->getPrice());
            }
        }

        return $parameterProfiles;
    }


} 