<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 27.09.16
 * Time: 11:16
 */

namespace Zoolyx\CoreBundle\Model\Report\Lister;


use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\ParameterGroup;
use Zoolyx\CoreBundle\Entity\Sample;

/**
 * @DI\Service("zoolyx_core.report.parameter_lister")
 */
class ParameterLister {

    /**
     * @param Sample|null $sample
     * @return array
     */
    public function getParameters($sample)
    {
        if (is_null($sample)) {
            return array();
        }

        $parameters = array();

        /** @var ParameterGroup $parameterGroup */
        foreach ($sample->getParameterGroups() as $parameterGroup) {
            foreach ($parameterGroup->getParameters() as $parameter) {
                $parameters[] = $parameter;
            }
        }
        return $parameters;
    }


} 