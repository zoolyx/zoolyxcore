<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 27.09.16
 * Time: 11:16
 */

namespace Zoolyx\CoreBundle\Model\Report\Lister;

use Zoolyx\CoreBundle\Entity\ParameterProfile as ParameterProfileEntity;
use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\ParameterProfilePrice;


class ParameterProfile {

    /** @var string */
    private $reference = null;

    /** @var ParameterProfilePrice */
    private $price = null;

    /** @var ParameterProfileEntity */
    private $parameterProfile = null;


    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return ParameterProfilePrice
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param ParameterProfilePrice $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @param string $value
     */
    public function addPrice($value) {
        if (is_null($this->price)) {
            $this->price = new ParameterProfilePrice();
        }
        $sum = floatval($this->price->getValue()) + floatval($value);
        $this->price->setValue(strval($sum));
    }

    /**
     * @return ParameterProfileEntity
     */
    public function getParameterProfile()
    {
        return $this->parameterProfile;
    }

    /**
     * @param ParameterProfileEntity $parameterProfile
     * @return $this
     */
    public function setParameterProfile($parameterProfile)
    {
        $this->parameterProfile = $parameterProfile;
        return $this;
    }


} 