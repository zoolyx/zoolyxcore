<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 27.09.16
 * Time: 11:16
 */

namespace Zoolyx\CoreBundle\Model\Report;


use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("zoolyx_core.report.language_validator")
 */
class LanguageValidator {

    private static $SUPPORTED_LANGUAGES = array('nl', 'fr', 'en');


    public function getSupportedLanguages() {
        return self::$SUPPORTED_LANGUAGES;
    }
    /**
     * @param string $language
     * @return bool
     */
    public function isSupported($language) {
        return (in_array($language, self::$SUPPORTED_LANGUAGES));
    }
} 