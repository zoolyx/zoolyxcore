<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 27.09.16
 * Time: 09:59
 */

namespace Zoolyx\CoreBundle\Model\Report;


class ReportHash {

    /** @var  string */
    private $hash;

    /** @var  string */
    private $baseHash;

    /** @var  string */
    private $language;

    /**
     * @param string $hash
     */
    public function __construct($hash) {
        $this->hash = $hash;
        $this->baseHash = substr($hash,0,8);
        $this->language = substr($hash,8,2);
    }

    /**
     * @return string
     */
    public function getBaseHash() {
        return $this->baseHash;
    }


    public function getLanguage() {
        return $this->language;
    }
} 