<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 10.12.16
 * Time: 13:14
 */

namespace Zoolyx\CoreBundle\Model\Report\VeterinaryPracticeReport;

class VeterinaryPracticeReportRoles {
    const VPR_VETERINARY = 1;
    const VPR_ORGANISATION = 2;
    const VPR_SHARED = 3;
} 