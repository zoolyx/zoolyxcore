<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 25.04.17
 * Time: 10:51
 */

namespace Zoolyx\CoreBundle\Model\Report\Menu;


class AdditionalRequestProgress {

    const ITEM_SAMPLES = 0;
    const ITEM_PROFILES = 1;
    const ITEM_SAMPLE_TYPES = 2;
    const ITEM_CONFIRM = 3;
    const ITEM_READY = 4;

    private $activeItem = null;

    public function __construct($item) {
        $this->activeItem = $item;
    }

    public function isActive($item) {
        return $this->activeItem == $item;
    }

    public function isCompleted($item) {
        return $item < $this->activeItem;
    }

    public function getColor($item) {
        if ($this->isCompleted($item)) {
            return "green";
        } elseif ($this->isActive($item)) {
            return "blue";
        } else {
            return "#777";
        }

    }
} 