<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 25.04.17
 * Time: 10:51
 */

namespace Zoolyx\CoreBundle\Model\Report\Menu;


class ReportMenu {

    const ITEM_DISPLAY_REPORT = 0;
    const ITEM_ASK_QUESTION = 1;
    const ITEM_DOWNLOAD_PDF = 2;
    const ITEM_ADDITIONAL_REQUEST = 3;
    const ITEM_ORIGINAL_REQUEST = 4;
    const ITEM_SHARE = 5;

    private $activeItem = null;

    public function __construct($item) {
        $this->activeItem = $item;
    }

    public function isActive($item) {
        return $this->activeItem == $item;
    }
} 