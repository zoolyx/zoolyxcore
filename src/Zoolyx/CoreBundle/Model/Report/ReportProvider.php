<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 25.04.17
 * Time: 10:51
 */

namespace Zoolyx\CoreBundle\Model\Report;

use Zoolyx\CoreBundle\Entity\Report as ReportEntity;

use JMS\DiExtraBundle\Annotation as DI;
/**
 * @DI\Service("zoolyx_core.provider.model_report")
 */
class ReportProvider {

    /**
     * @param ReportEntity $reportEntity
     * @return Report
     */
    public function provide($reportEntity) {
        return new Report($reportEntity);
    }
} 