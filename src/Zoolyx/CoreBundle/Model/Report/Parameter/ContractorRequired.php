<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Report\Parameter;

class ContractorRequired {
    /** @var int */
    private $contractorId;

    /** @var int */
    private $parameterProfileId;

    /** @var int */
    private $parameterId;

    /**
     * @return int
     */
    public function getContractorId() {
        return $this->contractorId;
    }

    /**
     * @param int $contractorId
     * @return ContractorRequired
     */
    public function setContractorId($contractorId) {
        $this->contractorId = $contractorId;
        return $this;
    }

    /**
     * @return int
     */
    public function getParameterProfileId() {
        return $this->parameterProfileId;
    }

    /**
     * @param int $parameterProfileId
     * @return ContractorRequired
     */
    public function setParameterProfileId($parameterProfileId) {
        $this->parameterProfileId = $parameterProfileId;
        return $this;
    }

    /**
     * @return int
     */
    public function getParameterId() {
        return $this->parameterId;
    }

    /**
     * @param int $parameterId
     * @return ContractorRequired
     */
    public function setParameterId($parameterId) {
        $this->parameterId = $parameterId;
        return $this;
    }

} 