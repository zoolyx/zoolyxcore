<?php
namespace Zoolyx\CoreBundle\Model\Report\Parameter\Reference;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Parameter;
use Zoolyx\CoreBundle\Entity\ParameterBase;
use Zoolyx\CoreBundle\Entity\ParameterProfileComment;
use Zoolyx\CoreBundle\Entity\ReferenceRange;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileCommentRepository;
use Zoolyx\CoreBundle\Entity\Sample;

/**
 * @DI\Service("zoolyx_core.report.parameter_reference_comment_fetcher")
 */

class ReferenceCommentFetcher {

    /** @var ParameterProfileCommentRepository */
    private $parameterProfileCommentRepository;

    /**
     * Constructor.
     *
     * @param ParameterProfileCommentRepository $parameterProfileCommentRepository
     *
     * @DI\InjectParams({
     *  "parameterProfileCommentRepository" = @DI\Inject("zoolyx_core.repository.parameter_profile_comment")
     * })
     */
    public function __construct(ParameterProfileCommentRepository $parameterProfileCommentRepository) {
        $this->parameterProfileCommentRepository = $parameterProfileCommentRepository;
    }

    /**
     * @param Sample $sample
     * @param Parameter $parameter
     * @param string $language
     * @return string
     */
    public function get($sample, $parameter, $language) {
        return $this->getExplicit($sample->getPet()->getSpeciesCode(), $parameter->getParameterBase(), $language);
    }

    /**
     * @param string $speciesCode
     * @param ParameterBase $parameterBase
     * @param string $language
     * @return string
     */
    public function getExplicit($speciesCode, $parameterBase, $language) {
        $commentCode = null;
        /** @var ReferenceRange $referenceRange */
        foreach ($parameterBase->getReferenceRanges() as $referenceRange) {
            if ($referenceRange->getSpeciesCode() == $speciesCode) {
                $commentCode = $referenceRange->getComment();
            }
        }
        if (is_null($commentCode))
            return '';

        /** @var ParameterProfileComment $parameterProfileComment */
        $parameterProfileComment = $this->parameterProfileCommentRepository->findOneBy(array('reference' => $commentCode));

        if (is_null($parameterProfileComment))
            return '';

        return $parameterProfileComment->getDescription($language)->getDescription();
    }

} 