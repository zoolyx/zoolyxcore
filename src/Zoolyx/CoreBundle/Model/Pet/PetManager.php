<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 03.11.16
 * Time: 11:09
 */
namespace Zoolyx\CoreBundle\Model\Pet;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Breed;
use Zoolyx\CoreBundle\Entity\Pet;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\Repository\BreedRepository;

/**
 * @DI\Service("zoolyx_core.pet.manager")
 */
class PetManager {

    /** @var BreedRepository */
    private $breedRepository;

    /**
     * Constructor.
     *
     * @param BreedRepository $breedRepository
     *
     * @DI\InjectParams({
     *  "breedRepository" = @DI\Inject("zoolyx_core.repository.breed")
     * })
     */
    public function __construct(BreedRepository $breedRepository) {
        $this->breedRepository = $breedRepository;
    }

    public function getSpeciesNameForReport(Report $report) {
        return $this->getSpeciesName($report->getDefaultVersion()->getSamples()[0]->getPet());
    }

    /**
     * @param Pet $pet
     * @return string
     */
    public function getSpeciesName(Pet $pet) {
        return $this->getSpeciesNameForCode($pet->getSpeciesCode());
    }

    public function getSpeciesNameForCode($speciesCode) {
        switch ($speciesCode) {
            case 'A' : $speciesName = "blank"; break; //andere
            case 'B' : $speciesName = "guinea_pig"; break;
            case 'C' :
            case 'Geit' : $speciesName = "goat"; break;
            case 'D' :
            case 'Vogel' : $speciesName = "bird"; break;
            case 'E' :
            case 'Ezel' : $speciesName = "donkey"; break;
            case 'F' :
            case 'Fret' : $speciesName = "ferret"; break;
            case 'G' : $speciesName = "rodent"; break; //knaagdier
            case 'H' :
            case 'Hond' : $speciesName = "dog"; break;
            case 'I' :
            case 'Vis' : $speciesName = "fish"; break;
            case 'J' :
            case 'Konijn' : $speciesName = "rabbit"; break;
            case 'K' :
            case 'Kat' : $speciesName = "cat"; break;
            case 'L' :
            case 'Schaap' : $speciesName = "sheep"; break;
            case 'M' : $speciesName = "alpaca"; break;
            case 'N' : $speciesName = "elephant"; break;
            case 'P' :
            case 'Paard' : $speciesName = "horse"; break;
            case 'R' :
            case 'Rund' : $speciesName = "cattle"; break;
            case 'S' :
            case 'Reptiel' : $speciesName = "snake"; break;
            case 'T' :
            case 'Rat' : $speciesName = "rat"; break;
            case 'U' : $speciesName = "mouse"; break;
            case 'X' : $speciesName = "dolphin"; break;
            case 'Y' : $speciesName = "seal"; break;
            case 'Z' :
            case 'Varken' : $speciesName = "pig"; break;

            default: $speciesName = "blank"; break;
        }
        return $speciesName;
    }

    public function getBreedNameForCode($speciesCode,$breedCode, $language = 'nl') {
        $breedCode = $speciesCode . "_" . $breedCode;
        /** @var Breed $breed */
        $breed = $this->breedRepository->findOneBy(array('breedId'=> $breedCode));
        return $breed ? $breed->getDescription($language) : "";
    }

} 