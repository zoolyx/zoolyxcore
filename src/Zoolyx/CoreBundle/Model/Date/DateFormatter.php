<?php
namespace Zoolyx\CoreBundle\Model\Date;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("zoolyx_core.format.date")
 */

class DateFormatter {



    public function format($date, $format = null) {

        if (is_null($date) || !($date instanceof \DateTime))
            return '';

        $format = is_null($format) ? 'Y-m-d H:i:s' : $format;
        $formattedDate = $date->format($format);
        return $formattedDate;
    }

} 