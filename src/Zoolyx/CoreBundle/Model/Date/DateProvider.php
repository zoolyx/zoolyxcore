<?php
namespace Zoolyx\CoreBundle\Model\Date;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("zoolyx_core.provider.date")
 */

class DateProvider {



    public function getNextWorkingDay() {

        $day = new \DateTime("tomorrow");
        //$day = new \DateTime("2020/04/10");
        while (!$this->isAWorkingDay($day)) {
            $day->modify("+1 day");
        }
        return $day;
    }

    public function getAfterNextWorkingDay() {
        $day = $this->getNextWorkingDay();
        $day->modify("+1 day");
        while (!$this->isAWorkingDay($day)) {
            $day->modify("+1 day");
        }
        return $day;
    }

    /**
     * @param \DateTime $day
     * @return bool
     */
    public function isAWorkingDay($day)
    {
        // check weekend
        if ($day->format("N") > 6) return false;

        $year = $day->format("Y");
        $easterMonday = new \DateTime();
        $easterMonday->setTimestamp(easter_date($year))->modify("+1 day");
        $easterMondayFormatted = $easterMonday->format("m/d");

        $ascension = $easterMonday->modify("+ 40 days");
        $ascensionFormatted = $ascension->format("m/d");
        
        $pentecost = $easterMonday->modify("+ 50 days");
        $pentecostFormatted = $pentecost->format("m/d");

        $dayFormatted = $day->format("m/d");
        if ($dayFormatted == "01/01") return false;   // newyear
        if ($dayFormatted == "05/01") return false;   // 1 mei
        if ($dayFormatted == "07/21") return false;   // Nationale feestdag
        if ($dayFormatted == "08/15") return false;   // OLV hemelvaart
        if ($dayFormatted == "11/01") return false;   // 1 november
        if ($dayFormatted == "11/11") return false;   // 11 november
        if ($dayFormatted == "12/25") return false;   // christmas
        if ($dayFormatted == $easterMondayFormatted) return false;   // Paasmaandag
        if ($dayFormatted == $pentecostFormatted) return false;   // Pinkstermaandag
        if ($dayFormatted == $ascensionFormatted) return false;   // OLH hemelvaart

        return true;
    }

} 