<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\ApiParser;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Request;
use Zoolyx\CoreBundle\Model\XmlParser\XmlValidator;

/**
 * @DI\Service("zoolyx_core.api_parser.content")
 */
class ApiContentParser {

    /** @var Request */
    private $request = null;

    /** @var array  */
    private $errors = array();

    /** @var ApiContentTypeParser */
    private $apiContentTypeParser = null;

    /** @var ApiParser */
    private $apiParser = null;

    /** @var array */
    private $candidateParsers = array();

    /** @var  XmlValidator */
    private $xmlValidator;


    /**
     * Constructor.
     *
     * @param ApiContentTypeParser $apiContentTypeParser
     * @param XmlParser $xmlParser
     * @param MixedParser $mixedParser
     * @param XmlValidator $xmlValidator
     *
     * @DI\InjectParams({
     *  "apiContentTypeParser" = @DI\Inject("zoolyx_core.api_parser.content_type"),
     *  "xmlParser" = @DI\Inject("zoolyx_core.api_parser.xml"),
     *  "mixedParser" = @DI\Inject("zoolyx_core.api_parser.mixed"),
     *  "xmlValidator" = @DI\Inject("zoolyx_core.xml_parser.xml_validator")
     * })
     */



    public function construct(ApiContentTypeParser $apiContentTypeParser, XmlParser $xmlParser, MixedParser $mixedParser, XmlValidator $xmlValidator )
    {
        $this->apiContentTypeParser = $apiContentTypeParser;
        $this->candidateParsers = array($xmlParser, $mixedParser);
        $this->xmlValidator = $xmlValidator;
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
        $this->apiContentTypeParser->parse($request);
        $this->loadParser();
    }

    public function isValid($xsdFile = null) {
        /** @var ApiContentTypeParser $contentType */
        $contentType = $this->getContentType();
        if (!$contentType->hasValidContentType()) {
            $this->errors[] = array(
                'Error' => $contentType->getContentTypeStatus()
            );
            return false;
        }

        if (!$this->canParse()) {
            $this->errors[] = array(
                'Error' => 'could not parse'
            );
            return false;
        }

        $this->xmlValidator->setXml($this->getXml());
        if (!$this->xmlValidator->validate($xsdFile)) {
            $this->errors = $this->xmlValidator->getErrors();
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getErrors() {
        return $this->errors;
    }

    public function canParse() {
        return !is_null($this->apiParser);
    }

    public function getXml() {
        return $this->apiParser->getXml($this->apiContentTypeParser, $this->request);
    }

    public function getFile($contentId) {
        return $this->apiParser->getFile($contentId);
    }
    public function getDomDocument() {
        return $this->xmlValidator->getDoc();
    }
    public function getFiles() {
        return $this->xmlValidator->getFiles();
    }

    public function getContentType()
    {
        return $this->apiContentTypeParser;
    }


    private function loadParser()
    {
        if (!$this->apiContentTypeParser->hasValidContentType()) {
            return;
        }

        /** @var ApiParser $candidateParser */
        foreach($this->candidateParsers as $candidateParser) {
            if ($candidateParser->canParse($this->getContentType()->getContentType())) {
                $this->apiParser = $candidateParser;
            }
        }
    }
}