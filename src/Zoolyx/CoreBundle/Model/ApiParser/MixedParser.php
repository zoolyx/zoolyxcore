<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\ApiParser;

use JMS\DiExtraBundle\Annotation as DI;
use phpDocumentor\Reflection\DocBlock\Tag;
use Symfony\Component\HttpFoundation\Request;

/**
 * @DI\Service("zoolyx_core.api_parser.mixed")
 */
class MixedParser extends ApiParser
{
    const SUPPORTED_CONTENT_TYPE = "multipart/related";

    private $parts = array();

    public function canParse($contentType)
    {
        return $contentType == $this::SUPPORTED_CONTENT_TYPE;
    }


    public function getXml(ApiContentTypeParser $apiContentTypeParser, Request $request) {
        $content =  $request->getContent();
        $this->setParts($content,$apiContentTypeParser->getBoundary());
        /** @var MixedPartParser $mainPart */
        $mainPart = $this->getMainPart();
        $xmlString = $mainPart->getContent();

        return $xmlString;
    }

    public function getFile($contentId) {
        $apiFile = new ApiFile();
        /** @var MixedPartParser $part */
        foreach($this->parts as $part) {
            $header = $part->getHeaderValue('Content-Id');
            if ($header && $header == $contentId) {
                //define the content
                $contentTransferEncoding = $part->getHeaderValue('Content-Transfer-Encoding');
                $content = $part->getContent();
                switch (strtoupper($contentTransferEncoding)) {
                    case 'BASE64' :
                        $content = base64_decode($content);
                        break;
                }
                $apiFile->setContent($content);

                //define the file extension
                $contentType = $part->getHeaderValue('Content-Type');
                $contentTypeParts = explode('/',$contentType);
                $apiFile->setExtension($contentTypeParts[1]);
                return $apiFile;
            }
        }
        return null;
    }

    private function setParts($content, $boundary) {
        $parts = explode($boundary,$content);
        for ($i=1; $i<count($parts)-1; $i++) {
            $this->parts[] = new MixedPartParser($parts[$i]);
        }
    }
    private function getMainPart() {
        return $this->parts[0];
    }

}