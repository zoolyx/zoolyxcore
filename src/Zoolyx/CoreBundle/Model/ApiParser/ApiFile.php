<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\ApiParser;

use phpDocumentor\Reflection\DocBlock\Tag;

class ApiFile
{
    private $content = null;
    private $extension = null;

    public function setContent($content) {
        $this->content = $content;
    }

    public function getContent() {
        return $this->content;
    }

    public function setExtension($extension) {
        $this->extension = $extension;
    }

    public function getExtension() {
        return $this->extension;
    }
}