<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\ApiParser;

use JMS\DiExtraBundle\Annotation as DI;
use phpDocumentor\Reflection\DocBlock\Tag;
use Symfony\Component\HttpFoundation\Request;

/**
 * @DI\Service("zoolyx_core.api_parser.xml")
 */
class XmlParser extends ApiParser
{
    const SUPPORTED_CONTENT_TYPE = "application/xml";
    const SUPPORTED_CONTENT_TYPE2 = "text/xml";

    public function canParse($contentType)
    {
        return $contentType == $this::SUPPORTED_CONTENT_TYPE || $contentType == $this::SUPPORTED_CONTENT_TYPE2;
    }

    public function getXml(ApiContentTypeParser $apiContentTypeParser, Request $request) {
        return $request->getContent();
    }

    public function getFile($contentId) {
        return null;
    }
}