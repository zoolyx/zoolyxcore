<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 06.03.16
 * Time: 13:29
 */

namespace Zoolyx\CoreBundle\Model\ApiParser;

use Symfony\Component\HttpFoundation\Request;

interface ApiParserInterface{

    public function canParse($contentType);
    public function getXml(ApiContentTypeParser $apiContentTypeParser, Request $request);
    public function getFile($contentId);
} 