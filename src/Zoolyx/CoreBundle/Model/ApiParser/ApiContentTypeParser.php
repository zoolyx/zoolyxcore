<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\ApiParser;

use JMS\DiExtraBundle\Annotation as DI;
use phpDocumentor\Reflection\DocBlock\Tag;
use Symfony\Component\HttpFoundation\Request;

/**
 * @DI\Service("zoolyx_core.api_parser.content_type")
 */
class ApiContentTypeParser {

    /** @var string $contentType */
    private $contentType = '';

    /** @var bool $contentTypeIsValid */
    private $contentTypeIsValid = false;

    /** @var string $contentTypeStatus */
    private $contentTypeStatus = "";

    /** @var string $boundary */
    private $boundary = "";



    public function parse(Request $request) {
        $this->init();
        $contentType = $request->headers->get('content-type');
        if (is_null($contentType)) {
            $this->contentTypeStatus = 'no Content-Type header present';
        }
        $contentTypeParts = explode(';',$contentType);
        $this->contentType = $contentTypeParts[0];
        $this->boundary = null;
        switch ($this->contentType) {
            case "multipart/related":
                $boundaryParts = explode('=',$contentTypeParts[1]);
                $this->contentTypeIsValid = true;
                $this->boundary = $boundaryParts[1];
                break;
            case "application/xml":
                $this->contentTypeIsValid = true;
                break;
            case "text/xml":
                $this->contentTypeIsValid = true;
                break;
            default:
                $this->contentTypeStatus = 'invalid Content-Type header ' . $this->contentType . ' found, Allowed values:[application/xml,multipart/related]';
        }
    }


    private function init() {
        $this->boundary = '';
        $this->contentType = '';
        $this->contentTypeIsValid = false;
        $this->contentTypeStatus = '';
    }
    public function getContentType()
    {
        return $this->contentType;
    }

    public function hasValidContentType() {
        return $this->contentTypeIsValid;
    }

    public function getContentTypeStatus()
    {
        return $this->contentTypeStatus;
    }

    public function getBoundary()
    {
        return $this->boundary;
    }

}