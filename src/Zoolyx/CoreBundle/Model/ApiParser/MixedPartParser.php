<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.02.16
 * Time: 11:34
 */

namespace Zoolyx\CoreBundle\Model\ApiParser;


class MixedPartParser
{
    private $headers = array();
    private $content = null;

    public function __construct($part)
    {
        $parts = explode("\n",$part);
        $state = 'wait_header';
        $content = array();
        foreach($parts as $index=>$txt) {
            $txt = rtrim($txt);
            switch($state) {
                case 'wait_header':
                    if ($txt != '') {
                        $this->headers[] = $txt;
                        $state = 'header';
                    }
                    break;
                case 'header':
                    if ($txt == '') {
                        $state = 'wait_content';
                    } else {
                        $this->headers[] = $txt;
                    }
                    break;
                case 'wait_content':
                    if ($txt != '') {
                        $content[] = $txt;
                        $state = 'content';
                    }
                    break;
                case 'content':
                    $content[] = $txt;
                    break;
            }
            $this->content = implode("\n",$content);
        }
    }

    public function getContent() {
        return $this->content;
    }
    public function getHeaders() {
        return $this->headers;
    }
    public function getHeaderValue($name) {
        foreach ($this->headers as $header) {
            $headerParts = explode(':', $header);
            $headerName = trim($headerParts[0]);
            if (strtoupper($headerName) == strtoupper($name)) {
                return trim($headerParts[1]);
            }
        }
        return null;
    }

}