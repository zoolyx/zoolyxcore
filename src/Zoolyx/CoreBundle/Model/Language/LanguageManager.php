<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Language;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker as Security;
use Zoolyx\CoreBundle\Entity\User;


/**
 * @DI\Service("zoolyx_core.language.manager")
 */
class LanguageManager {

    /** @var string */
    private $defaultLocale;

    /** @var array */
    private $supportedLanguages;

    /**
     * Constructor.
     *
     * @param string $defaultLocale
     * @param string $supportedLanguages
     *
     * @DI\InjectParams({
     *  "defaultLocale" = @DI\Inject("%locale%"),
     *  "supportedLanguages" = @DI\Inject("%supported_languages%")
     * })
     */
    public function __construct($defaultLocale, $supportedLanguages) {
        $this->defaultLocale = $defaultLocale;
        $this->supportedLanguages = explode(",",$supportedLanguages);
    }

    /**
     * @param string $language
     * @param Request $request
     * @return string
     */
    public function validate($language=null, Request $request = null)
    {
        if ($this->isSupported($language)) {
            return $language;
        }

        if ($request && $this->isSupported($request->getLocale())) {
            return $request->getLocale();
        }

        return $this->defaultLocale;
    }

    private function isSupported($language) {
        return in_array($language,$this->supportedLanguages);
    }

} 