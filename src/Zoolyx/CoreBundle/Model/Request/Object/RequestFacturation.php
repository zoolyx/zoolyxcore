<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Request\Object;


class RequestFacturation extends RequestUser {

    /** @var string */
    private $faId;
    /** @var string */
    private $vatNumber;
    /** @var boolean */
    private $invoiceForOwner = true;

    /**
     * @return mixed
     */
    public function getFaId()
    {
        return $this->faId;
    }

    /**
     * @param string $faId
     * @return $this
     */
    public function setFaId($faId)
    {
        $this->faId = $faId;
        return $this;
    }


    /**
     * @return string
     */
    public function getVatNumber()
    {
        return $this->vatNumber;
    }

    /**
     * @param string $vatNumber
     * @return $this
     */
    public function setVatNumber($vatNumber)
    {
        $this->vatNumber = $vatNumber;
        return $this;
    }

    /**
     * @return boolean
     */
    public function invoiceForOwner()
    {
        return $this->invoiceForOwner;
    }

    /**
     * @param boolean $invoiceForOwner
     * @return $this
     */
    public function setInvoiceForOwner($invoiceForOwner)
    {
        $this->invoiceForOwner = $invoiceForOwner;
        return $this;
    }



} 