<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Request\Object;

class RequestProfile {

    /** @var string */
    private $code = '';

    /** @var string */
    private $description = '';

    /** @var float */
    private $price = null;

    /** @var string */
    private $sampleTypes = '';

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return array
     */
    public function getSampleTypes()
    {
        return $this->sampleTypes;
    }

    /**
     * @param array $sampleTypes
     * @return $this
     */
    public function setSampleTypes($sampleTypes)
    {
        $this->sampleTypes = $sampleTypes;
        return $this;
    }

} 