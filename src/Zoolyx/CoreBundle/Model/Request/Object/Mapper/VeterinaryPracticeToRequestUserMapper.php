<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Request\Object\Mapper;


use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Model\Request\Object\RequestUser;

/**
 * @DI\Service("zoolyx_core.mapper.vp_to_request_user")
 */

class VeterinaryPracticeToRequestUserMapper {

    /**
     * @param VeterinaryPractice $veterinaryPractice
     * @return RequestUser
     */
    public function map(VeterinaryPractice $veterinaryPractice) {
        $veterinary = $veterinaryPractice->getVeterinary();
        $practice = $veterinaryPractice->getPractice();

        $requestVeterinary = new RequestUser();
        $requestVeterinary
            ->setLimsId($veterinaryPractice->getLimsId())
            ->setEmail($veterinary->getAccount() ? $veterinary->getAccount()->getEmail() : '')
            ->setFirstName($veterinary->getFirstName())
            ->setLastName($veterinary->getLastName())
            ->setStreet($practice->getStreet())
            ->setZipCode($practice->getZipCode())
            ->setCity($practice->getCity())
            ->setCountry($practice->getCountry())
            ->setTelephone($practice->getTelephone())
            ->setLanguage($veterinary->getLanguage())
        ;
        return $requestVeterinary;
    }
} 