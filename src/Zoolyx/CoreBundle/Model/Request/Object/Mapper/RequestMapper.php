<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Request\Object\Mapper;


use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Forms\Request\FormData;
use Zoolyx\CoreBundle\Model\Request\RequestData;

/**
 * @DI\Service("zoolyx_core.mapper.request")
 */
class RequestMapper {

    private $formData;

    /**
     * Constructor.
     *
     * @param FormData $formData
     *
     * @DI\InjectParams({
     *  "formData" = @DI\Inject("zoolyx_core.form.form_data")
     * })
     */
    public function __construct(FormData $formData) {
        $this->formData = $formData;
    }

    /**
     * @param RequestData $requestData
     * @return FormData
     */
    public function mapRequestToForm($requestData) {
        $this->formData
            ->setFirstName($requestData->getOwner()->getFirstName())
            ->setLastName($requestData->getOwner()->getLastName())
            ->setStreet($requestData->getOwner()->getStreet())
            ->setZipCode($requestData->getOwner()->getZipCode())
            ->setCity($requestData->getOwner()->getCity())
            ->setCountry($requestData->getOwner()->getCountry())
            ->setTelephone($requestData->getOwner()->getTelephone())
            ->setEmail($requestData->getOwner()->getEmail())
            ->setLanguage($requestData->getOwner()->getLanguage())
            ->setName($requestData->getSample()->getPet()->getName())
            ->setChipNbr($requestData->getSample()->getPet()->getChipNbr())
            ->setSpeciesCode($requestData->getSample()->getPet()->getSpeciesCode())
            ->setBreedCode($requestData->getSample()->getPet()->getBreedCode()!="" ? $requestData->getSample()->getPet()->getBreedCode() : "")
            ->setGender($requestData->getSample()->getPet()->getGender())
            ->setBirthDate($requestData->getSample()->getPet()->getBirthDate())
            ->setInvoiceFor($requestData->getFacturation()->invoiceForOwner() ? 2 : 1)
            ->setFaId($requestData->getFacturation()->getFaId())
            ->setVatNbr($requestData->getFacturation()->getVatNumber())
        ;
        return $this->formData;
    }
} 