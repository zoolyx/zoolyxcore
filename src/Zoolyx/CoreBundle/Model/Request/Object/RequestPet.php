<?php
namespace Zoolyx\CoreBundle\Model\Request\Object;


use JMS\DiExtraBundle\Annotation as DI;

class RequestPet {

    /** @var string */
    private $limsId;
    /** @var string */
    private $externalId;
    /** @var string */
    private $chipNbr;
    /** @var string */
    private $speciesCode;
    /** @var string */
    private $speciesName;
    /** @var string */
    private $breedCode = '';
    /** @var string */
    private $breed = '';
    /** @var string */
    private $originalBreedName = '';
    /** @var string */
    private $gender;
    /** @var string */
    private $name;
    /** @var \DateTime */
    private $birthDate;

    /**
     * @return string
     */
    public function getLimsId()
    {
        return $this->limsId;
    }

    /**
     * @param string $limsId
     * @return $this
     */
    public function setLimsId($limsId)
    {
        $this->limsId = $limsId;
        return $this;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     * @return $this
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getChipNbr()
    {
        return $this->chipNbr;
    }

    /**
     * @param string $chipNbr
     * @return $this
     */
    public function setChipNbr($chipNbr)
    {
        $this->chipNbr = $chipNbr;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpeciesCode()
    {
        return $this->speciesCode;
    }

    /**
     * @param string $speciesCode
     * @return $this
     */
    public function setSpeciesCode($speciesCode)
    {
        $this->speciesCode = $speciesCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpeciesName()
    {
        return $this->speciesName;
    }

    /**
     * @param string $speciesName
     * @return $this
     */
    public function setSpeciesName($speciesName)
    {
        $this->speciesName = $speciesName;
        return $this;
    }

    /**
     * @return string
     */
    public function getBreedCode()
    {
        return $this->breedCode;
    }

    /**
     * @param string $breedCode
     * @return $this
     */
    public function setBreedCode($breedCode)
    {
        $this->breedCode = $breedCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getBreed()
    {
        return $this->breed;
    }

    /**
     * @param string $breed
     * @return $this
     */
    public function setBreed($breed)
    {
        $this->breed = $breed;
        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalBreedName()
    {
        return $this->originalBreedName;
    }

    /**
     * @param string $originalBreedName
     * @return $this
     */
    public function setOriginalBreedName($originalBreedName)
    {
        $this->originalBreedName = $originalBreedName;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTime $birthDate
     * @return $this
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
        return $this;
    }

} 