<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Request\Object;


class RequestInvoice {

    /** @var int */
    private $vat;

    /** @var float */
    private $amount;

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return int
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param int $vat
     * @return $this
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }



} 