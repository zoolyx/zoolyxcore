<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Request\Object;

class RequestSampleType {

    /** @var string */
    private $reference;

    /** @var string */
    private $description;

    /** @var string */
    private $imageName;

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param string $imageName
     * @return $this
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }


} 