<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Request\Object;

class RequestSample {

    const SAMPLE_VIRTUAL = 0;
    const SAMPLE_REAL = 1;
    const SAMPLE_REAL_PRESENT = 2;
    const SAMPLE_REAL_NO_KIT = 3;


    /** @var int */
    private $id;

    /** @var string */
    private $code;

    /** @var int */
    private $sampleType;

    /** @var RequestPet */
    private $pet;

    /** @var array */
    private $profiles = array();

    /** @var array */
    private $forms = array();


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }


    /**
     * @return RequestPet
     */
    public function getPet()
    {
        return $this->pet;
    }

    /**
     * @param RequestPet $pet
     * @return $this
     */
    public function setPet($pet)
    {
        $this->pet = $pet;
        return $this;
    }

    /**
     * @return array
     */
    public function getProfiles()
    {
        return $this->profiles;
    }

    /**
     * @param RequestProfile $profile
     * @return $this
     */
    public function addProfile($profile)
    {
        $this->profiles[] = $profile;
        return $this;
    }

    /**
     * @param $profiles
     * @return $this
     */
    public function setProfiles($profiles) {
        $this->profiles = $profiles;
        return $this;
    }

    /**
     * @return int
     */
    public function getSampleType()
    {
        return $this->sampleType;
    }

    /**
     * @param int $sampleType
     * @return $this
     */
    public function setSampleType($sampleType)
    {
        $this->sampleType = $sampleType;
        return $this;
    }

    /**
     * @return array
     */
    public function getForms()
    {
        return $this->forms;
    }

    /**
     * @param RequestForm $form
     */
    public function addForm($form)
    {
        $this->forms[] = $form;
    }
} 