<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Request\Object;

class RequestForm {



    /** @var string */
    private $name = '';

    /** @var array */
    private $values = array();

    /** @var array */
    private $files = array();

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function setValue($name,$value)
    {
        $this->values[$name] = $value;
        return $this;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function setValues($values) {
        $this->values = $values;
        return $this;
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param string $filename
     * @return $this
     */
    public function addFile($filename)
    {
        $this->files[] = $filename;
        return $this;
    }



} 