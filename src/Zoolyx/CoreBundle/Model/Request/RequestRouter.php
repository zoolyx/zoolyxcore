<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Request;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;

/**
 * @DI\Service("zoolyx_core.router.request")
 */
class RequestRouter {

    /** @var Translator */
    private $translator;

    const STEP_0_SAMPLE_FORM = 0; // via form
    const STEP_1_SELECT_SAMPLE = 1; // select sample from report
    const STEP_2_SELECT_PROFILES = 2;
    const STEP_3_SELECT_SAMPLE_TYPE = 3;
    const STEP_4_CONFIRM = 4;
    const STEP_5_COMPLETE = 5;

    /** @var RequestData */
    public $requestData = null;

    private $step = null;

    private $previous = null;
    private $previousCaption = null;
    private $next = null;
    private $nextCaption = null;

    private $routes = array(
        'request_new',
        'request_submit_sample',
        'request_submit_pet_form',
        'request_submit_profiles',
        'request_submit_sample_types',
        'request_confirm'
    );


    /**
     * Constructor.
     *
     * @param Session $session
     * @param Translator $translator
     *
     * @DI\InjectParams({
     *  "session" = @DI\Inject("session"),
     *  "translator" = @DI\Inject("translator.default")
     * })
     */
    public function __construct(Session $session, Translator $translator) {
        $this->translator = $translator;
        $this->translator->setLocale($session->get('_locale', 'nl'));
    }

    /**
     * @param RequestData $requestData
     * @return $this
     */
    public function setRequestData($requestData) {
        $this->requestData = $requestData;
        $this->setStep($this->findCurrentStep());
        return $this;
    }

    public function setStep($step) {
        $this->step = $step;
        $this->next = isset($this->routes[$this->step+1]) ? $this->routes[$this->step+1] : null;
        $this->nextCaption =
            isset($this->routes[$this->step+1])
                ? $this->translator->trans('request.'.$this->routes[$this->step].'.next')
                : null;
        if (!$this->requestData->isAdditionalRequest() && $this->step==self::STEP_2_SELECT_PROFILES) {
            $this->previous = self::STEP_0_SAMPLE_FORM;
            $this->previousCaption = $this->translator->trans('request.'.$this->routes[self::STEP_2_SELECT_PROFILES].'.prev');
        } else {
            $this->previous = ($this->step > 1) && ($this->step < 5) ? $this->step-1 : null;
            $this->previousCaption =
                isset($this->routes[$this->step-1])
                    ? $this->translator->trans('request.'.$this->routes[$this->step].'.prev')
                    : null;
        }
        return $this;
    }

    /**
     * @return int
     */
    private function findCurrentStep() {
        if (!$this->requestData->sampleIsValid()) {
            if ($this->requestData->isAdditionalRequest()) {
                return self::STEP_1_SELECT_SAMPLE;
            } else {
                return self::STEP_0_SAMPLE_FORM;
            }

        }
        if (count($this->requestData->getSample()->getProfiles()) == 0) {
            return self::STEP_2_SELECT_PROFILES;
        }
        if ($this->requestData->sampleTypesAreSet() == false) {
            return self::STEP_3_SELECT_SAMPLE_TYPE;
        }

        if (!$this->requestData->isSent()) {
            return self::STEP_4_CONFIRM;
        }
        return self::STEP_5_COMPLETE;
    }

    /**
     * @return string
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * @return string
     */
    public function getNextCaption()
    {
        return $this->nextCaption;
    }

    /**
     * @return string
     */
    public function getPrevious()
    {
        return $this->previous;
    }

    /**
     * @return string
     */
    public function getPreviousCaption()
    {
        return $this->previousCaption;
    }

    /**
     * @return int
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @return int
     */
    public function getTwigStep()
    {
        return $this->step ? $this->step : 1;
    }
} 