<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Request;

use Zoolyx\CoreBundle\Model\Request\Object\RequestFacturation;
use Zoolyx\CoreBundle\Model\Request\Object\RequestForm;
use Zoolyx\CoreBundle\Model\Request\Object\RequestInvoice;
use Zoolyx\CoreBundle\Model\Request\Object\RequestProfile;
use Zoolyx\CoreBundle\Model\Request\Object\RequestSample;
use Zoolyx\CoreBundle\Model\Request\Object\RequestSampleType;
use Zoolyx\CoreBundle\Model\Request\Object\RequestUser;

class RequestData {

    /** @var bool */
    private $isAdditionalRequest = true;
    /** @var bool */
    private $isAdministrator = false;
    /** @var string */
    private $administratorEmail = '';

    /** @var string */
    private $reportRequestId = '';
    /** @var string */
    private $reportRequestNumber = '';

    /** @var string */
    private $hash = '';

    /** @var string */
    private $practiceReference = '';
    /** @var bool */
    private $editPracticeReference = false;

    /** @var \DateTime */
    private $requestDate = null;

    /** @var bool */
    private $prependPracticeIdToExtId = false;

    /** @var RequestUser */
    private $veterinary = null;
    /** @var string */
    private $limsId = null;
    /** @var array */
    private $candidateVeterinaryPractices = array();

    /** @var RequestUser */
    private $owner = null;

    /** @var RequestFacturation */
    private $facturation = null;

    /** @var float */
    private $vat = 21;

    /** @var string */
    private $vatNbr = '';

    /** @var RequestSample */
    private $sample = null;

    /** @var bool */
    private $sampleIsValid = false;

    /** @var RequestInvoice */
    private $invoice = null;

    /** @var int */
    private $invoiceDefault = null;

    /** @var array */
    private $profilesToPay = array();

    /** @var bool  */
    private $sampleTypesAreSet = false;

    /** @var bool */
    private $isSent = false;

    /**
     * @return boolean
     */
    public function isAdditionalRequest()
    {
        return $this->isAdditionalRequest;
    }

    /**
     * @param boolean $isAdditionalRequest
     * @return $this
     */
    public function setIsAdditionalRequest($isAdditionalRequest)
    {
        $this->isAdditionalRequest = $isAdditionalRequest;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isAdministrator()
    {
        return $this->isAdministrator;
    }

    /**
     * @param boolean $isAdministrator
     * @return $this
     */
    public function setIsAdministrator($isAdministrator)
    {
        $this->isAdministrator = $isAdministrator;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdministratorEmail()
    {
        return $this->administratorEmail;
    }

    /**
     * @param string $administratorEmail
     * @return $this
     */
    public function setAdministratorEmail($administratorEmail)
    {
        $this->administratorEmail = $administratorEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getReportRequestId()
    {
        return $this->reportRequestId;
    }

    /**
     * @param string $reportRequestId
     * @return $this
     */
    public function setReportRequestId($reportRequestId)
    {
        $this->reportRequestId = $reportRequestId;
        return $this;
    }


    /**
     * @return string
     */
    public function getReportRequestNumber()
    {
        return $this->reportRequestNumber;
    }

    /**
     * @param string $reportRequestNumber
     * @return $this
     */
    public function setReportRequestNumber($reportRequestNumber)
    {
        $this->reportRequestNumber = $reportRequestNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     * @return $this
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @return string
     */
    public function getPracticeReference()
    {
        return $this->practiceReference;
    }

    /**
     * @param string $practiceReference
     * @return $this
     */
    public function setPracticeReference($practiceReference)
    {
        $this->practiceReference = $practiceReference;
        return $this;
    }

    /**
     * @return boolean
     */
    public function editPracticeReference()
    {
        return $this->editPracticeReference;
    }

    /**
     * @param boolean $editPracticeReference
     * @return $this
     */
    public function setEditPracticeReference($editPracticeReference)
    {
        $this->editPracticeReference = $editPracticeReference;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRequestDate()
    {
        return $this->requestDate;
    }

    /**
     * @param \DateTime $requestDate
     * @return $this
     */
    public function setRequestDate($requestDate)
    {
        $this->requestDate = $requestDate;
        return $this;
    }

    /**
     * @return boolean
     */
    public function prependPracticeIdToExtId()
    {
        return $this->prependPracticeIdToExtId;
    }

    /**
     * @param boolean $prependPracticeIdToExtId
     */
    public function setPrependPracticeIdToExtId($prependPracticeIdToExtId)
    {
        $this->prependPracticeIdToExtId = $prependPracticeIdToExtId;
    }


    /**
     * @return RequestUser
     */
    public function getVeterinary()
    {
        return $this->veterinary;
    }

    /**
     * @param RequestUser $veterinary
     * @return $this
     */
    public function setVeterinary($veterinary)
    {
        $this->veterinary = $veterinary;
        return $this;
    }

    /**
     * @return string
     */
    public function getLimsId()
    {
        return $this->limsId;
    }

    /**
     * @param string $limsId
     * @return $this
     */
    public function setLimsId($limsId)
    {
        $this->limsId = $limsId;
        return $this;
    }


    /**
     * @return array
     */
    public function getCandidateVeterinaryPractices()
    {
        return $this->candidateVeterinaryPractices;
    }

    /**
     * @param array $candidateVeterinaryPractices
     * @return $this
     */
    public function setCandidateVeterinaryPractices($candidateVeterinaryPractices)
    {
        $this->candidateVeterinaryPractices = $candidateVeterinaryPractices;
        return $this;
    }



    /**
     * @return RequestUser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param RequestUser $owner
     * @return $this
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return RequestFacturation
     */
    public function getFacturation()
    {
        return $this->facturation;
    }

    /**
     * @param RequestFacturation $facturation
     * @return $this
     */
    public function setFacturation($facturation)
    {
        $this->facturation = $facturation;
        return $this;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param float $vat
     * @return $this
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }

    /**
     * @return string
     */
    public function getVatNbr()
    {
        return $this->vatNbr;
    }

    /**
     * @param string $vatNbr
     * @return $this
     */
    public function setVatNbr($vatNbr)
    {
        $this->vatNbr = $vatNbr;
        return $this;
    }


    /**
     * @return RequestSample
     */
    public function getSample()
    {
        return $this->sample;
    }

    /**
     * @param RequestSample $sample
     * @return $this
     */
    public function setSample($sample)
    {
        $this->sample = $sample;
        return $this;
    }

    /**
     * @return boolean
     */
    public function sampleIsValid()
    {
        return $this->sampleIsValid;
    }

    /**
     * @param boolean $sampleIsValid
     * @return $this
     */
    public function setSampleIsValid($sampleIsValid)
    {
        $this->sampleIsValid = $sampleIsValid;
        return $this;
    }


    /**
     * @return RequestInvoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param RequestInvoice $invoice
     * @return $this
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
        return $this;
    }

    /**
     * @return int
     */
    public function getInvoiceDefault()
    {
        return $this->invoiceDefault;
    }

    /**
     * @param int $invoiceDefault
     * @return $this
     */
    public function setInvoiceDefault($invoiceDefault)
    {
        $this->invoiceDefault = $invoiceDefault;
        return $this;
    }

    /**
     * @return string
     */
    public function getProfiles()
    {
        $profiles = $this->sample->getProfiles();
        $profileReferences = array();
        /** @var RequestProfile $profile */
        foreach ($profiles as $profile) {
            $profileReferences[] = $profile->getCode();
        }
        return $profileReferences;
    }

    /**
     * @param string $profiles
     * @return $this
     */
    public function setProfiles($profiles)
    {
        $this->getSample()->setProfiles($profiles);
        return $this;
    }

    public function setSampleTypes($profileCode, $sampleTypes) {
        $profiles = $this->getSample()->getProfiles();
        /** @var RequestProfile $currentProfile */
        $currentProfile = null;
        /** @var RequestProfile $profile */
        foreach ($profiles as $profile) {
            if ($profile->getCode() == $profileCode) {
                $currentProfile = $profile;
            }
        }
        if (is_null($currentProfile)) {
            $currentProfile = new RequestProfile();
            $currentProfile->setCode($profileCode);
            $currentProfile->setDescription('some Description');
            $currentProfile->setPrice(15);
            $this->getSample()->addProfile($currentProfile);
        }

        $currentProfile->setSampleTypes($sampleTypes);
        $this->sampleTypesAreSet = true;
    }

    /**
     * @return bool
     */
    public function sampleTypesAreSet() {
        return $this->sampleTypesAreSet;
    }

    public function getSampleTypes() {
        $aggregatedSampleTypes = array();
        /** @var RequestProfile $profile */
        foreach ($this->sample->getProfiles() as $profile) {
            $sampleTypes = $profile->getSampleTypes();
            /** @var RequestSampleType $sampleType */
            foreach ($sampleTypes as $sampleType) {
                $aggregatedSampleTypes[$sampleType->getReference()] = $sampleType;
            }
        }
        return $aggregatedSampleTypes;
    }
    public function getTotalPrice() {
        $totalPrice = 0;
        /** @var RequestProfile $profile */
        foreach($this->sample->getProfiles() as $profile) {
            $price = $profile->getPrice();
            $price = str_replace(",",".",$price);
            $totalPrice+= $price;
        }
        $totalPrice+= 10;
        $totalPrice = $totalPrice * 1.21;
        return $totalPrice;
    }

    public function getRequestForm($formName) {
        /** @var RequestForm $form */
        foreach ($this->sample->getForms() as $form) {
            if ($form->getName() == $formName) {
                return $form;
            }
        }
        $form = new RequestForm();
        $form->setName($formName);
        $this->sample->addForm($form);
        return $form;
    }

    /**
     * @return array
     */
    public function getProfilesToPay()
    {
        return $this->profilesToPay;
    }

    /**
     * @param array $profilesToPay
     * @return $this
     */
    public function setProfilesToPay($profilesToPay)
    {
        $this->profilesToPay = $profilesToPay;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isSent()
    {
        return $this->isSent;
    }

    /**
     * @param boolean $isSent
     * @return $this
     */
    public function setIsSent($isSent)
    {
        $this->isSent = $isSent;
        return $this;
    }



} 