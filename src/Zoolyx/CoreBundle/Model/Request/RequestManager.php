<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Request;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\Session\Session;
use Zoolyx\CoreBundle\Entity\Breed;
use Zoolyx\CoreBundle\Entity\BreedDescription;
use Zoolyx\CoreBundle\Entity\Owner;
use Zoolyx\CoreBundle\Entity\Pet;
use Zoolyx\CoreBundle\Entity\Practice;
use Zoolyx\CoreBundle\Entity\ReportVersion;
use Zoolyx\CoreBundle\Entity\Repository\BreedDescriptionRepository;
use Zoolyx\CoreBundle\Entity\Repository\BreedRepository;
use Zoolyx\CoreBundle\Entity\Repository\OwnerRepository;
use Zoolyx\CoreBundle\Entity\Repository\SpeciesRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryPracticeRepository;
use Zoolyx\CoreBundle\Entity\Sample;
use Zoolyx\CoreBundle\Entity\Species;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Vat;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Entity\VeterinaryPracticeReport;
use Zoolyx\CoreBundle\Forms\Request\FormData;
use Zoolyx\CoreBundle\Forms\Request\LimsIdDropDownBuilder;
use Zoolyx\CoreBundle\Model\Pet\PetManager;
use Zoolyx\CoreBundle\Model\Request\Object\Mapper\VeterinaryPracticeToRequestUserMapper;
use Zoolyx\CoreBundle\Model\Request\Object\RequestFacturation;
use Zoolyx\CoreBundle\Model\Request\Object\RequestPet;
use Zoolyx\CoreBundle\Model\Request\Object\RequestSample;
use Zoolyx\CoreBundle\Model\Request\Object\RequestUser;

/**
 * @DI\Service("zoolyx_core.manager.request")
 */
class RequestManager {
    const OPTIONS_POST = "post";
    const OPTIONS_REPORT_VERSION = "report";
    const OPTIONS_SAMPLE_ID = "sample_id";
    const OPTIONS_LIMS_ID = "lims_id";
    const OPTIONS_IS_ADDITIONAL = "additional";
    const OPTIONS_LANGUAGE = "language";
    const OPTIONS_EDIT_PRACTICE_REFERENCE = "edit_practice_reference";
    const OPTIONS_PREPEND_PRACTICE_ID_TO_EXT_ID = "prepend_practice_id";

    /** @var Session*/
    private $session;
    /** @var Translator */
    private $translator;
    /** @var VeterinaryPracticeRepository */
    private $veterinaryPracticeRepository;
    /** @var OwnerRepository */
    private $ownerRepository;
    /** @var BreedRepository  */
    private $breedRepository;
    /** @var BreedDescriptionRepository  */
    private $breedDescriptionRepository;
    /** @var SpeciesRepository */
    private $speciesRepository;
    /** @var PetManager */
    public $petManager;
    /** @var LimsIdDropDownBuilder */
    private $limsIdDropDownBuilder;
    /** @var RequestIdProvider */
    private $requestIdProvider;
    /** @var VeterinaryPracticeToRequestUserMapper */
    private $veterinaryPracticeToRequestUserMapper;


    /** @var RequestData */
    private $requestData = null;

    /**
     * Constructor.
     *
     * @param Session $session
     * @param Translator $translator
     * @param VeterinaryPracticeRepository $veterinaryPracticeRepository
     * @param OwnerRepository $ownerRepository
     * @param BreedRepository $breedRepository
     * @param BreedDescriptionRepository $breedDescriptionRepository
     * @param SpeciesRepository $speciesRepository
     * @param PetManager $petManager
     * @param LimsIdDropDownBuilder $limsIdDropDownBuilder
     * @param RequestIdProvider $requestIdProvider
     * @param VeterinaryPracticeToRequestUserMapper $veterinaryPracticeToRequestUserMapper
     *
     * @DI\InjectParams({
     *  "session" = @DI\Inject("session"),
     *  "translator" = @DI\Inject("translator.default"),
     *  "veterinaryPracticeRepository" = @DI\Inject("zoolyx_core.repository.veterinary_practice"),
     *  "ownerRepository" = @DI\Inject("zoolyx_core.repository.owner"),
     *  "breedRepository" = @DI\Inject("zoolyx_core.repository.breed"),
     *  "breedDescriptionRepository" = @DI\Inject("zoolyx_core.repository.breed_description"),
     *  "speciesRepository" = @DI\Inject("zoolyx_core.repository.species"),
     *  "petManager" = @DI\Inject("zoolyx_core.pet.manager"),
     *  "limsIdDropDownBuilder" = @DI\Inject("zoolyx_core.builder.lims_id_drop_down"),
     *  "requestIdProvider" = @DI\Inject("zoolyx_core.provider.request_id"),
     *  "veterinaryPracticeToRequestUserMapper" = @DI\Inject("zoolyx_core.mapper.vp_to_request_user")
     * })
     */

    public function __construct(
        $session,
        Translator $translator,
        VeterinaryPracticeRepository $veterinaryPracticeRepository,
        OwnerRepository $ownerRepository,
        BreedRepository $breedRepository,
        BreedDescriptionRepository $breedDescriptionRepository,
        SpeciesRepository $speciesRepository,
        PetManager $petManager,
        LimsIdDropDownBuilder $limsIdDropDownBuilder,
        RequestIdProvider $requestIdProvider,
        VeterinaryPracticeToRequestUserMapper $veterinaryPracticeToRequestUserMapper
    ) {
        $this->session = $session;
        $this->translator = $translator;
        $this->veterinaryPracticeRepository = $veterinaryPracticeRepository;
        $this->ownerRepository = $ownerRepository;
        $this->breedRepository = $breedRepository;
        $this->breedDescriptionRepository = $breedDescriptionRepository;
        $this->speciesRepository = $speciesRepository;
        $this->petManager = $petManager;
        $this->limsIdDropDownBuilder = $limsIdDropDownBuilder;
        $this->requestIdProvider = $requestIdProvider;
        $this->veterinaryPracticeToRequestUserMapper = $veterinaryPracticeToRequestUserMapper;

        if ($this->session->get('requestData')) {
            $this->loadFromSession();
        }
    }

    public function init(User $user, array $options) {
        $this->session->set('requestData',null);
        $this->requestData = new RequestData();

        // load options
        /** @var object $post */
        $post = isset($options[self::OPTIONS_POST]) ? $options[self::OPTIONS_POST] : null;
        /** @var ReportVersion $reportVersion */
        $reportVersion = isset($options[self::OPTIONS_REPORT_VERSION]) ? $options[self::OPTIONS_REPORT_VERSION] : null;
        /** @var string $limsId */
        $limsId = isset($options[self::OPTIONS_LIMS_ID]) ? $options[self::OPTIONS_LIMS_ID] : null;
        /** @var string $sampleId */
        $sampleId = isset($options[self::OPTIONS_SAMPLE_ID]) ? $options[self::OPTIONS_SAMPLE_ID] : null;
        $isAdditional = isset($options[self::OPTIONS_IS_ADDITIONAL]) ? $options[self::OPTIONS_IS_ADDITIONAL] : false;
        $language = isset($options[self::OPTIONS_LANGUAGE]) ? $options[self::OPTIONS_LANGUAGE] : 'nl';
        $editPracticeReference = isset($options[self::OPTIONS_EDIT_PRACTICE_REFERENCE]) ? $options[self::OPTIONS_EDIT_PRACTICE_REFERENCE] : false;
        $prependPracticeIdToExtId = isset($options[self::OPTIONS_PREPEND_PRACTICE_ID_TO_EXT_ID]) ? $options[self::OPTIONS_PREPEND_PRACTICE_ID_TO_EXT_ID] : false;

        $this->loadVeterinary($user, $limsId, $reportVersion);
        $this->loadOwner($reportVersion, $post);
        $this->loadSample($reportVersion, $post, $sampleId, $language);
        $this->loadFacturation();
        $this->loadReportRequestId($user, $reportVersion);
        $this->loadPracticeReference($reportVersion, $post, $editPracticeReference);

        $this->getRequestData()
            ->setIsAdditionalRequest($isAdditional)
            ->setRequestDate(new \DateTime())
            ->setPrependPracticeIdToExtId($prependPracticeIdToExtId);
        ;

        $this->saveToSession();
    }

    /**
     * @param FormData $formData
     */
    public function updateFromForm($formData) {

        /** @var Species $species */
        $species = $this->speciesRepository->findOneBy(array('speciesId'=>$formData->getSpeciesCode()));
        /** @var Breed $breed */
        $breed = $formData->getBreedCode() == "" ? null : $this->breedRepository->findOneBy(array(
            'breedId'=>$formData->getSpeciesCode()."_".$formData->getBreedCode()
        ));

        if ($formData->getPracticeReference())
            $this->getRequestData()->setPracticeReference($formData->getPracticeReference());

        $this->getRequestData()->getSample()->getPet()
            ->setName($formData->getName())
            ->setChipNbr($formData->getChipNbr())
            ->setSpeciesCode($formData->getSpeciesCode())
            ->setSpeciesName($species->getDescription($this->translator->getLocale())->getDescription())
            ->setBreedCode($formData->getBreedCode())
            ->setBreed($breed ? $breed->getDescription($this->translator->getLocale()) : "")
            ->setGender($formData->getGender())
            ->setBirthDate($formData->getBirthDate())
        ;
        $this->requestData->getFacturation()
            ->setFaId($formData->getFaId())
            ->setVatNumber($formData->getVatNbr())
            ->setInvoiceForOwner($formData->getInvoiceFor()==VeterinaryPractice::INVOICE_DEFAULT_OWNER);

        if (count($this->getRequestData()->getCandidateVeterinaryPractices())>0 || $this->requestData->isAdministrator()) {
            /** @var VeterinaryPractice $veterinaryPractice */
            $veterinaryPractice = $this->veterinaryPracticeRepository->findOneBy(array('limsId'=>$formData->getLimsId()));
            if ($veterinaryPractice) {
                $this->getRequestData()->setVeterinary($this->veterinaryPracticeToRequestUserMapper->map($veterinaryPractice));
            }
        }
        $this->requestData->getOwner()
            ->setEmail($formData->getEmail())
            ->setFirstName($formData->getFirstName())
            ->setLastName($formData->getLastName())
            ->setStreet($formData->getStreet())
            ->setZipCode($formData->getZipCode())
            ->setCity($formData->getCity())
            ->setCountry($formData->getCountry())
            ->setTelephone($formData->getTelephone())
            ->setLanguage($formData->getLanguage())
        ;
        if ($this->requestData->prependPracticeIdToExtId()) {
            $limsId = $this->requestData->getVeterinary()->getLimsId();
            /** @var VeterinaryPractice $veterinaryPractice */
            $veterinaryPractice = $this->veterinaryPracticeRepository->findOneBy(array('limsId'=>$limsId));
            if ($veterinaryPractice) {
                $prefix = $veterinaryPractice->getPractice()->getId();
                $this->requestData->getOwner()->setExternalId($prefix . "_" . $this->requestData->getOwner()->getExternalId());
                $this->requestData->getSample()->getPet()->setExternalId($prefix . "_" . $this->requestData->getSample()->getPet()->getExternalId());
            }
        }
        $this->getRequestData()->setSampleIsValid(true);
        $this->saveToSession();
    }
    public function saveToSession() {
        $this->session->set('requestData',$this->requestData);
    }
    public function loadFromSession() {
        $this->requestData = $this->session->get('requestData');
    }
    public function getRequestData() {
        return $this->requestData;
    }

    private function loadVeterinary(User $user, $limsId = null, ReportVersion $reportVersion = null) {
        if ($user->hasRole('ROLE_ADMINISTRATOR') || $user->hasRole('ROLE_SUPER_ADMIN')) {
            $this->getRequestData()
                ->setIsAdministrator(true)
                ->setAdministratorEmail($user->getEmail())
                ->setCandidateVeterinaryPractices(array());
        }
        elseif ($user->hasRole('ROLE_VETERINARY'))
        {
            $veterinaryPractices = array();
            $veterinaryPractice = null;
            if ($limsId) {
                $veterinaryPractice = $this->veterinaryPracticeRepository->findOneBy(array('limsId'=>$limsId));
            } else {
                $veterinaryPractices = $this->veterinaryPracticeRepository->getVeterinaryPracticesViaPracticeOf($user);
                if (count($veterinaryPractices) == 1) {
                    /** @var VeterinaryPractice $veterinaryPractice */
                    $veterinaryPractice = $veterinaryPractices[0];
                } else if ($reportVersion) {
                    $report = $reportVersion->getReport();
                    /** @var VeterinaryPracticeReport $vpr */
                    foreach ($report->getVeterinaryPracticeReports() as $vpr) {
                        /** @var VeterinaryPractice $vp */
                        foreach ($veterinaryPractices as $vp) {
                            if ($vpr->getVeterinaryPractice()->getId() == $vp->getId()) {
                                $veterinaryPractice = $vp;
                                break;
                            }
                        }

                    }
                }
            }
            if ($veterinaryPractice) {
                $this->getRequestData()
                    ->setVeterinary($this->veterinaryPracticeToRequestUserMapper->map($veterinaryPractice))
                    ->setLimsId($veterinaryPractice->getLimsId());
                $this->loadFacturation($veterinaryPractice->getVat(), $veterinaryPractice->getInvoiceDefault()==VeterinaryPractice::INVOICE_DEFAULT_OWNER);
            } else {
                $candidates = $this->limsIdDropDownBuilder->build($veterinaryPractices);
                /** @var VeterinaryPractice $firstCandidate */
                $firstCandidate = null;
                foreach ($candidates as $candidate=>$limsId) {
                    /** @var VeterinaryPractice $vp */
                    foreach ($veterinaryPractices as $vp) {
                        if ($vp->getLimsId() == $limsId) {
                            $firstCandidate = $vp;
                        }
                    }
                    break;
                }
                $this->getRequestData()
                    ->setCandidateVeterinaryPractices($candidates);
                $this->loadFacturation($firstCandidate ? $firstCandidate->getVat() : null, $firstCandidate ? $firstCandidate->getInvoiceDefault()==VeterinaryPractice::INVOICE_DEFAULT_OWNER : true);
            }
        }
        elseif ($user->hasRole('ROLE_OWNER'))
        {
            /** @var Owner $owner */
            $owner = $this->ownerRepository->findOneBy(array('account'=>$user));

            if ($owner) {
                $requestOwner = new RequestUser();
                $requestOwner
                    ->setLimsId($owner->getLimsId())
                    ->setEmail($user->getEmail())
                    ->setFirstName($owner->getFirstName())
                    ->setLastName($owner->getLastName())
                    ->setStreet($owner->getStreet())
                    ->setZipCode($owner->getZipCode())
                    ->setCity($owner->getCity())
                    ->setCountry($owner->getCountry())
                    ->setTelephone($owner->getTelephone())
                    ->setLanguage($owner->getLanguage())
                ;
                $this->getRequestData()->setOwner($requestOwner);
            }

            $zoolyxVeterinaryPractice = $this->veterinaryPracticeRepository->findOneBy(array('limsId'=>$owner->getLanguage() == 'fr' ? 2 : 1));
            if ($zoolyxVeterinaryPractice) {
                /** @var Veterinary $veterinary */
                $veterinary = $zoolyxVeterinaryPractice->getVeterinary();
                /** @var Practice $practice */
                $practice = $zoolyxVeterinaryPractice->getPractice();

                $requestVeterinary = new RequestUser();
                $requestVeterinary
                    ->setLimsId($zoolyxVeterinaryPractice->getLimsId())
                    ->setEmail($veterinary->getAccount() ? $veterinary->getAccount()->getEmail() : '')
                    ->setFirstName($veterinary->getFirstName())
                    ->setLastName($veterinary->getLastName())
                    ->setStreet($practice->getStreet())
                    ->setZipCode($practice->getZipCode())
                    ->setCity($practice->getCity())
                    ->setCountry($practice->getCountry())
                    ->setTelephone('')
                    ->setLanguage($veterinary->getLanguage())
                ;
                $this->getRequestData()->setVeterinary($requestVeterinary);
            }
        }
    }

    /**
     * @param ReportVersion|null $reportVersion
     * @param object|null $post
     */
    private function loadOwner($reportVersion, $post) {
        $owner = new RequestUser();
        $owner
            ->setExternalId('')
            ->setEmail('')
            ->setFirstName('')
            ->setLastName('')
            ->setStreet('')
            ->setZipCode('')
            ->setCity('')
            ->setCountry('')
            ->setTelephone('')
            ->setLanguage('')
        ;

        if ($post) {
            $owner
                ->setExternalId((string)$post->ad->extid)
                ->setEmail((string)$post->ad->email)
                ->setFirstName((string)$post->ad->firstName)
                ->setLastName((string)$post->ad->lastName)
                ->setStreet((string)$post->ad->street)
                ->setZipCode((string)$post->ad->zipCode)
                ->setCity((string)$post->ad->city)
                ->setCountry((string)$post->ad->country)
                ->setTelephone((string)$post->ad->tel)
                ->setLanguage((string)$post->ad->language)
            ;
        }
        if ($reportVersion) {
            $reportOwner = $reportVersion->getReport()->getOwner();
            $owner = new RequestUser();
            if ($reportOwner) {
                $owner
                    ->setLimsId($reportOwner->getLimsId())
                    ->setEmail($reportOwner->getAccount() ? $reportOwner->getAccount()->getEmail() : '')
                    ->setFirstName($reportOwner->getFirstName())
                    ->setLastName($reportOwner->getLastName())
                    ->setStreet($reportOwner->getStreet())
                    ->setZipCode($reportOwner->getZipCode())
                    ->setCity($reportOwner->getCity())
                    ->setCountry($reportOwner->getCountry())
                    ->setTelephone('')
                    ->setLanguage($reportOwner->getLanguage())
                ;
            }
        }
        $this->getRequestData()->setOwner($owner);
    }
    /**
     * @param ReportVersion|null $reportVersion
     * @param object|null $post
     * @param string|null $sampleId
     * @param string|null $language
     */
    private function loadSample($reportVersion, $post, $sampleId, $language = 'nl') {
        $pet = new RequestPet();
        $requestSample = new RequestSample();
        $sampleIsValid = false;

        if ($post) {
            $breedDescription = (string)$post->pe->breed;
            if ($breedDescription) {
                $breed = null;
                /** @var BreedDescription $breedDescriptionObject */
                $breedDescriptionObject = $this->breedDescriptionRepository->findOneBy(array('description'=>$breedDescription));
                if ($breedDescriptionObject) {
                    $breedId = $breedDescriptionObject->getBreed()->getId();
                    $breed = $breedId ? $this->breedRepository->findOneBy(array('id'=>$breedId)) : null;
                }

                if ($breed) {
                    $breedCode = $breed->getBreedId();
                    $parts = explode("_",$breedCode);
                    $breedCode = $parts[1];
                } else {
                    $breedCode = "";
                }
            } else {
                $breedCode = "";
            }
            $pet->setExternalId((string)$post->pe->extid)
                ->setName((string)$post->pe->name)
                ->setSpeciesCode((string)$post->pe->species)
                ->setBreedCode($breedCode)
                ->setOriginalBreedName($breedDescription)
                ->setGender((string)$post->pe->gender)
                ->setChipNbr((string)$post->pe->chipId)
                ->setBirthDate((string)$post->pe->birthDate == "" ? null : new \DateTime((string)$post->pe->birthDate));
        }
        if ($reportVersion) {
            $samples = $reportVersion->getSamples();

            /** @var Sample $sample */
            $sample = null;

            if ($sampleId) {
                /** @var Sample $s */
                foreach ($samples as $s) {
                    if ($s->getId() == $sampleId) {
                        $sample = $s;
                        break;
                    }
                }
            }

            if (is_null($sample)) {
                $physicalSamples = array();
                /** @var Sample $sample */
                foreach ($samples as $s) {
                    if (is_null($s->isPhysical()) || $s->isPhysical()) {
                        $physicalSamples[] = $s;
                    }
                }
                if (count($physicalSamples) == 1) {
                    $sample = $physicalSamples[0];
                }
            }
            if ($sample) {
                /** @var Pet $reportPet */
                $reportPet = $sample->getPet();
                $pet->setLimsId($reportPet->getPetId())
                    ->setName($reportPet->getName())
                    ->setSpeciesCode($reportPet->getSpeciesCode())
                    ->setSpeciesName($this->petManager->getSpeciesNameForCode($reportPet->getSpeciesCode()))
                    ->setBreed($this->petManager->getBreedNameForCode($reportPet->getSpeciesCode(), $reportPet->getBreedCode(), $language))
                    ->setBreedCode($reportPet->getBreedCode())
                    ->setGender($reportPet->getGender())
                    ->setChipNbr($reportPet->getChipNbr())
                    ->setBirthDate($reportPet->getBirthDate());
                $requestSample
                    ->setId($sample->getId())
                    ->setCode($sample->getCode())
                    ->setSampleType(RequestSample::SAMPLE_REAL)
                    ->setPet($pet)
                ;
                $sampleIsValid = true;
            }
        }

        $requestSample
            ->setSampleType(RequestSample::SAMPLE_REAL)
            ->setPet($pet)
        ;
        $this->getRequestData()
            ->setSample($requestSample)
            ->setSampleIsValid($sampleIsValid);
        ;
    }
    private function loadFacturation(Vat $vat = null, $invoiceForOwner = true) {
        if (!is_null($this->getRequestData()->getFacturation()) && is_null($vat)) return;

        /** @todo */
        //set FA -> als we dit kennen vanuit het rapport
        //set VAT -> vanuit form of rapport
        //set BTW% (6 of 21)
        $facturation = new RequestFacturation();
        $facturation->setFaId($vat ? $vat->getFaId() : '');
        $facturation->setVatNumber($vat ? $vat->getVat() : '');
        $facturation->setInvoiceForOwner($invoiceForOwner);
        $this->getRequestData()->setFacturation($facturation);
    }
    /**
     * @param User $user
     * @param ReportVersion|null $reportVersion
     */
    private function loadReportRequestId($user, $reportVersion) {
        if ($reportVersion) {
            $reportRequestNumber = $reportVersion->getReport()->getRequestId();
            $hash = $reportVersion->getReport()->getHash();
        } else {
            $reportRequestNumber = $this->requestIdProvider->getId($user);
            $hash = null;
        }
        $reportRequestId = $this->requestIdProvider->addPrefix($reportRequestNumber, "www",20);

        $this->getRequestData()
            ->setReportRequestId($reportRequestId)
            ->setReportRequestNumber($reportRequestNumber)
            ->setHash($hash)
        ;
    }
    /**
     * @param ReportVersion|null $reportVersion
     * @param object|null $post
     * @param bool $editPracticeReference
     */
    private function loadPracticeReference($reportVersion, $post, $editPracticeReference) {
        $practiceReference = null;
        if ($reportVersion) {
            $practiceReference = $reportVersion->getReport()->getPracticeReference();
        }
        if ($post) {
            $practiceReference = (string)$post->practiceRef;
        }
        $editPracticeReference = is_null($practiceReference) ? true : $editPracticeReference;

        $this->getRequestData()
            ->setPracticeReference(is_null($practiceReference) ? '' : $practiceReference)
            ->setEditPracticeReference($editPracticeReference)
        ;
    }
} 