<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 22.02.17
 * Time: 11:17
 */
namespace Zoolyx\CoreBundle\Model\Request;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Session\Session;
use Zoolyx\CoreBundle\Entity\User;

/**
 * @DI\Service("zoolyx_core.provider.request_id")
 */
class RequestIdProvider {

    public function getId(User $user) {
        return $user->getId().time();
    }

    public function addPrefix($reportRequestId, $prefix, $maxLength) {
        $requestIdParts = explode("+",$reportRequestId);
        if (count($requestIdParts)==1) {
            //there was no "+"
            if (strlen($reportRequestId)<=$maxLength-4) {
                array_unshift($requestIdParts,$prefix);
            }
        } else {
            if (count($requestIdParts[0]) > 4) {
                if (strlen($reportRequestId)<=$maxLength-4) {
                    array_unshift($requestIdParts,$prefix);
                }
            } else {
                $requestIdParts[0] = $prefix;
            }
        }

        return implode("+",$requestIdParts);
    }
} 