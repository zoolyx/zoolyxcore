<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 04.03.16
 * Time: 20:00
 */

namespace Zoolyx\CoreBundle\DataFixtures\ORM;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zoolyx\CoreBundle\Entity\Validator;


class LoadValidatorData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /** @var ObjectManager $entityManager */
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $validator = new Validator(); $validator->setReference('FRANK')->setName('Da. Frank Van Campen'); $entityManager->persist($validator);
        $validator = new Validator(); $validator->setReference('TOM')->setName('Da. Tom Nagels'); $entityManager->persist($validator);
        $validator = new Validator(); $validator->setReference('GRIET')->setName('Da. Griet Vercauteren'); $entityManager->persist($validator);
        $validator = new Validator(); $validator->setReference('CAROLINE')->setName('Da. Caroline Bauwens'); $entityManager->persist($validator);
        $validator = new Validator(); $validator->setReference('LARA')->setName('Da. Lara Bonner'); $entityManager->persist($validator);
        $validator = new Validator(); $validator->setReference('VINCENT')->setName('Vét. Vincent Pironnet'); $entityManager->persist($validator);
        $validator = new Validator(); $validator->setReference('DBAMDL')->setName('AutoVal'); $entityManager->persist($validator);

        $entityManager->flush();
    }

}
