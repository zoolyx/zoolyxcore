<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 04.03.16
 * Time: 20:00
 */

namespace Zoolyx\CoreBundle\DataFixtures\ORM;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zoolyx\CoreBundle\Entity\NewsItem;


class LoadNewsItemsData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /** @var ObjectManager $entityManager */
        $entityManager = $this->container->get('doctrine.orm.entity_manager');

        $newsItem = new NewsItem();
        $newsItem
            ->setTitle('Lorem ipsum dolor sit amet consetetur sadipscing')
            ->setImage('http://www.sickchirpse.com/wp-content/uploads/2012/11/Cats.jpg')
            ->setUrl('http://www.google.be')
            ->setContent('Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.')
            ->setContentShort('');
        $entityManager->persist($newsItem);

        $newsItem = new NewsItem();
        $newsItem
            ->setTitle('consetetur sadipscing elitr, sed diam nonumy eirmod')
            ->setImage('http://ep.yimg.com/ty/cdn/entirelypets/top-10-places-to-take-your-pet-on-the-east-coast6.jpg')
            ->setUrl('http://www.google.be')
            ->setContent('Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.')
            ->setContentShort('');
        $entityManager->persist($newsItem);

        $newsItem = new NewsItem();
        $newsItem
            ->setTitle('tempor invidunt ut labore et dolore')
            ->setImage('https://i1.wp.com/sophieshorsetales.com/wp-content/uploads/2017/05/DSC2885.jpg?resize=760%2C400')
            ->setUrl('http://www.google.be')
            ->setContent('Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.')
            ->setContentShort('');
        $entityManager->persist($newsItem);

        $newsItem = new NewsItem();
        $newsItem
            ->setTitle('magna aliquyam erat, sed diam voluptua')
            ->setImage('http://click.ir/wp-content/uploads/2017/04/red-blood-cells-6-760x400.jpg')
            ->setUrl('http://www.google.be')
            ->setContent('Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.')
            ->setContentShort('');
        $entityManager->persist($newsItem);

        $newsItem = new NewsItem();
        $newsItem
            ->setTitle('tempor invidunt ut labore et dolore magna aliquyam erat')
            ->setImage('http://salamati.ir/wp-content/uploads/2017/06/medic-233-652-754-760x400.jpg')
            ->setUrl('http://www.google.be')
            ->setContent('Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.')
            ->setContentShort('');
        $entityManager->persist($newsItem);

        $entityManager->flush();
    }

}
