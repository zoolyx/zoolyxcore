<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 04.03.16
 * Time: 20:00
 */

namespace Zoolyx\CoreBundle\DataFixtures\ORM;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\TicketBundle\Entity\AdministrationGroup;


class LoadAdministrationData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /** @var ObjectManager $entityManager */
        $entityManager = $this->container->get('doctrine.orm.entity_manager');

        $admin1 = new User(); $admin1->setUserName('administration 1')->setEmail('admin1@zoolyx.be')->setEnabled(true)->addRole('ROLE_ADMINISTRATOR')->setPlainPassword('admin'); $entityManager->persist($admin1);
        $andrea = new User(); $andrea->setUserName('Andrea De Smedt')->setEmail('andrea.de.smedt@zoolyx.be')->setEnabled(true)->addRole('ROLE_ADMINISTRATOR')->setPlainPassword('admin'); $entityManager->persist($andrea);
        $bernadette = new User(); $bernadette->setUserName('Bernadette Nagels')->setEmail('bernadette.nagels@zoolyx.be')->setEnabled(true)->addRole('ROLE_ADMINISTRATOR')->setPlainPassword('admin'); $entityManager->persist($bernadette);
        $bram = new User(); $bram->setUserName('Bram Steemans')->setEmail('bram.steemans@zoolyx.be')->setEnabled(true)->addRole('ROLE_ADMINISTRATOR')->setPlainPassword('admin'); $entityManager->persist($bram);
        $peggy = new User(); $peggy->setUserName("peggyaerts")->setEmail("peggy.aerts@zoolyx.be")->setEnabled(true)->setPassword("d783f24a5c13ff3c0929f3bafe4da2b3ae846223")->addRole('ROLE_VETERINARY')->addRole('ROLE_ADMINISTRATOR')->setFirstName(ucwords("peggy"))->setLastName(ucwords("aerts"))->setStreet(ucwords("vondelen 43"))->setZipCode("9450")->setCity(ucwords("denderhoutem"))->setCountry("B")->setLanguage("nl"); $entityManager->persist($peggy);
        $sylvie = new User(); $sylvie->setUserName("sylvieke")->setEmail("sylvie.goessens@zoolyx.be")->setEnabled(true)->setPassword("59bbba3cb4b4b38f003d4ed4c66845995b89ba84")->addRole('ROLE_VETERINARY')->addRole('ROLE_ADMINISTRATOR')->setFirstName(ucwords("sylvie"))->setLastName(ucwords("goessens"))->setStreet(ucwords("zonnestraat 3"))->setZipCode("9300")->setCity(ucwords("aalst"))->setCountry("B")->setLanguage("nl"); $entityManager->persist($sylvie);

        $ag1 = new AdministrationGroup(); $ag1->setName('Administratie');
        $ag1->addAdministrator($admin1)
            ->addAdministrator($andrea)
            ->addAdministrator($bernadette)
            ->addAdministrator($bram)
            ->addAdministrator($peggy)
            ->addAdministrator($sylvie)        ;
        $entityManager->persist($ag1);

        $vet1 = new User(); $vet1->setUserName('veterinary 1')->setEmail('vet1@zoolyx.be')->setEnabled(true)->addRole('ROLE_ADMINISTRATOR')->setPlainPassword('admin'); $entityManager->persist($vet1);
        $frank = new User(); $frank->setUserName("zoolyx01")->setEmail("frank.van.campen@zoolyx.be")->setEnabled(true)->setPassword("b67f74072390ced92927194a3fd646dd1cdd7402")->addRole('ROLE_VETERINARY')->addRole('ROLE_ADMINISTRATOR')->setFirstName(ucwords("frank"))->setLastName(ucwords("van campen"))->setStreet(ucwords("zonnestraat 3"))->setZipCode("9300")->setCity(ucwords("aalst"))->setCountry("B")->setLanguage("nl"); $entityManager->persist($frank);
        $tom = new User(); $tom->setUserName('Tom Nagels')->setEmail('tom.nagels@zoolyx.be')->setEnabled(true)->addRole('ROLE_ADMINISTRATOR')->setPlainPassword('admin'); $entityManager->persist($tom);
        $griet = new User(); $griet->setUserName('Griet Vercauteren')->setEmail('griet.vercauteren@zoolyx.be')->setEnabled(true)->addRole('ROLE_ADMINISTRATOR')->setPlainPassword('admin'); $entityManager->persist($griet);
        $caroline = new User(); $caroline->setUserName('Caroline Bauwens')->setEmail('caroline.bauwens@zoolyx.be')->setEnabled(true)->addRole('ROLE_ADMINISTRATOR')->setPlainPassword('admin'); $entityManager->persist($caroline);
        $vincent = new User(); $vincent->setUserName('Vincent Pironnet')->setEmail('vincent.pironnet@zoolyx.be')->setEnabled(true)->addRole('ROLE_ADMINISTRATOR')->setPlainPassword('admin'); $entityManager->persist($vincent);

        $ag2 = new AdministrationGroup(); $ag2->setName('Dierenartsen');
        $ag2->addAdministrator($vet1)
            ->addAdministrator($frank)
            ->addAdministrator($tom)
            ->addAdministrator($griet)
            ->addAdministrator($caroline)
            ->addAdministrator($vincent);
        $entityManager->persist($ag2);

        $entityManager->flush();
    }

}
