<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 04.03.16
 * Time: 20:00
 */

namespace Zoolyx\CoreBundle\DataFixtures\ORM;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zoolyx\CoreBundle\Entity\Contractor;
use Zoolyx\CoreBundle\Entity\Veterinary;


class LoadContractorData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /** @var ObjectManager $entityManager */
        $entityManager = $this->container->get('doctrine.orm.entity_manager');

        $organisation = new Veterinary();
        $organisation->setEnabled(true)
            ->setUsername('kmsh')
            ->setEmail('kmsh@test.be')
            ->addRole('ROLE_ORGANISATION')
            ->setPlainPassword('kmsh');
        $entityManager->persist($organisation);

        $contractor = new Contractor();
        $contractor->setEnabled(true)
            ->setContractorId('DB')
            ->setUsername('vhl')
            ->setEmail('vhl@test.be')
            ->addRole('ROLE_CONTRACTOR')
            ->setPlainPassword('vhl');
        $entityManager->persist($contractor);
        $entityManager->flush();
    }

}
