<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 04.03.16
 * Time: 20:00
 */

namespace Zoolyx\CoreBundle\DataFixtures\ORM;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zoolyx\TicketBundle\Entity\TicketState;


class LoadTicketStatesData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /** @var ObjectManager $entityManager */
        $entityManager = $this->container->get('doctrine.orm.entity_manager');

        $state = new TicketState(); $state->setName('New');$entityManager->persist($state);
        $state = new TicketState(); $state->setName('Assigned');$entityManager->persist($state);
        $state = new TicketState(); $state->setName('Open');$entityManager->persist($state);
        $state = new TicketState(); $state->setName('Closed');$entityManager->persist($state);

        $entityManager->flush();
    }

}
