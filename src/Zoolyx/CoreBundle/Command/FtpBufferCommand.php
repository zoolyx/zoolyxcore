<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 13.03.17
 * Time: 16:28
 */

namespace Zoolyx\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zoolyx\CoreBundle\Model\Ftp\FtpService;

class FtpBufferCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this
            ->setName('app:ftp-buffer')

            // the short description shown while running "php bin/console list"
            ->setDescription('Tries to send files to the ftp server.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Tries to send files to the ftp server.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var FtpService $ftpService */
        $ftpService = $this->getContainer()->get('zoolyx_core.ftp.service');
        $ftpLogDir = $this->getContainer()->getParameter('log_dir_ftp');

        $errorDir = $ftpLogDir . "/error";
        $successDir = $ftpLogDir . "/success";
        $files = array_diff(scandir($errorDir), array('..', '.'));

        foreach ($files as $filename) {
            $content = file_get_contents($errorDir . "/" . $filename);

            $tempFile = fopen('php://memory', 'r+');
            fputs($tempFile, $content);
            rewind($tempFile);

            if ($ftpService->ftp($filename, $tempFile)) {
                exec("mv " . $errorDir . "/" . $filename . " " . $successDir . "/" . $filename);
            }
        }
    }

}