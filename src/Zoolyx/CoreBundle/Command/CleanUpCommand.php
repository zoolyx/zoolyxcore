<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 13.03.17
 * Time: 16:28
 */

namespace Zoolyx\CoreBundle\Command;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Zoolyx\CoreBundle\Entity\ContractorReport;
use Zoolyx\CoreBundle\Entity\ContractorRequest;

class CleanUpCommand extends ContainerAwareCommand {

    /** @var string */
    private $logDirLims = '';
    /** @var ObjectManager $entityManager */
    private $entityManager;
    /** @var array */
    private $contractorRequests = array();

    protected function configure()
    {
        $this
            ->setName('app:clean-up')

            // the short description shown while running "php bin/console list"
            ->setDescription('Removes old records and files.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Removes old records and files.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->logDirLims = $this->getContainer()->getParameter('log_dir_lims');
        $contractorRequestRepository = $this->getContainer()->get('zoolyx_core.repository.contractor_request');
        $this->contractorRequests = $contractorRequestRepository->findOldRequests();

        echo $this->removeRequests(false);

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Continue with this action? (y/n) ', false);

        if (!$helper->ask($input, $output, $question)) {
            echo "Cleanup canceled".PHP_EOL;
            return;
        }

        $this->removeRequests(true);
        echo "Files and records have been removed";
    }

    private function removeRequests($delete = false) {
        $summary = array();

        $numberOfContractorRequests = count($this->contractorRequests);
        $numberOfContractorReports = 0;
        $numberOfFiles = 0;
        /** @var ContractorRequest $contractorRequest */
        foreach ($this->contractorRequests as $contractorRequest) {
            $contractorReports = $contractorRequest->getContractorReports();
            $numberOfContractorReports += count($contractorReports);
            /** @var ContractorReport $contractorReport */
            foreach ($contractorReports as $contractorReport) {
                $report = $contractorReport->getReport();
                $filename = $this->logDirLims .'/'.$report->getId().'.xml';
                if (file_exists($filename)) {
                    $numberOfFiles++;
                    if ($delete) {
                        unlink($filename);
                    }
                }
            }
            if ($delete) {
                $this->entityManager->remove($contractorRequest);
            }
        }
        if ($delete) {
            $this->entityManager->flush();
        }
        $summary[] = "number of contractor requests to be deleted:".$numberOfContractorRequests;
        $summary[] = "number of contractor reports to be deleted:".$numberOfContractorReports;
        $summary[] = "number of files to be deleted:".$numberOfFiles;

        return implode(PHP_EOL,$summary).PHP_EOL;
    }

}