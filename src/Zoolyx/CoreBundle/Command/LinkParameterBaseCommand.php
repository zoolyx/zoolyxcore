<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 13.03.17
 * Time: 16:28
 */

namespace Zoolyx\CoreBundle\Command;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zoolyx\CoreBundle\Entity\Parameter;
use Zoolyx\CoreBundle\Entity\ParameterBase;
use Zoolyx\CoreBundle\Entity\Repository\ParameterBaseRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterRepository;

class LinkParameterBaseCommand extends ContainerAwareCommand {


    protected function configure()
    {

        $this
            ->addArgument('max', InputArgument::OPTIONAL, 'Maximum number of parameters?')
            // the name of the command (the part after "bin/console")
            ->setName('app:link-parameter-base')

            // the short description shown while running "php bin/console list"
            ->setDescription('Link report parameters to base parameters.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command is only executed for parameters with parameterBase equal to null')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $max = $input->getArgument('max') ? $input->getArgument('max') : 100;
        $output->writeln('Will try to handle '.$max.' parameters');

        /** @var ObjectManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        /** @var ParameterRepository $parameterRepository */
        $parameterRepository = $this->getContainer()->get('zoolyx_core.repository.parameter');
        /** @var ParameterBaseRepository $parameterBaseRepository */
        $parameterBaseRepository = $this->getContainer()->get('zoolyx_core.repository.parameter_base');

        $numPersisted = 0;
        /** @var Parameter $parameter */
        foreach ($parameterRepository->findBy(array('parameterBase' => null), null, $max) as $parameter) {
            if ($parameter->getParameterId()) {
                /** @var ParameterBase $parameterBase */
                $parameterBase = $parameterBaseRepository->findOneBy(array('parameterId'=>$parameter->getParameterId()));
                $parameter->setParameterBase($parameterBase);
                $entityManager->persist($parameter);
                $numPersisted++;
            }
        }
        $entityManager->flush();
        $output->writeln($numPersisted.' parameters where persisted');
    }

}