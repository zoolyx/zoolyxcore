<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 13.03.17
 * Time: 16:28
 */

namespace Zoolyx\CoreBundle\Command;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zoolyx\CoreBundle\Entity\Parameter;
use Zoolyx\CoreBundle\Entity\ParameterBase;
use Zoolyx\CoreBundle\Entity\ParameterGroup;
use Zoolyx\CoreBundle\Entity\Repository\ParameterGroupBaseRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterGroupRepository;

class LinkParameterGroupBaseCommand extends ContainerAwareCommand {


    protected function configure()
    {

        $this
            ->addArgument('max', InputArgument::OPTIONAL, 'Maximum number of parameters?')
            // the name of the command (the part after "bin/console")
            ->setName('app:link-parameter-group-base')

            // the short description shown while running "php bin/console list"
            ->setDescription('Link report parameter groups to base parameter groups.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command is only executed for parameter groups with parameterGroupBase equal to null')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $max = $input->getArgument('max') ? $input->getArgument('max') : 100;
        $output->writeln('Will try to handle '.$max.' parameter groups');

        /** @var ObjectManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        /** @var ParameterGroupRepository $parameterGroupRepository */
        $parameterGroupRepository = $this->getContainer()->get('zoolyx_core.repository.parameter_group');
        /** @var ParameterGroupBaseRepository $parameterGroupBaseRepository */
        $parameterGroupBaseRepository = $this->getContainer()->get('zoolyx_core.repository.parameter_group_base');

        $numPersisted = 0;
        /** @var ParameterGroup $parameterGroup */
        foreach ($parameterGroupRepository->findBy(array('parameterGroupBase' => null), null, $max) as $parameterGroup) {
            if ($parameterGroup->getParameterGroupId()) {
                /** @var ParameterBase $parameterBase */
                $parameterGroupBase = $parameterGroupBaseRepository->findOneBy(array('parameterGroupId'=>$parameterGroup->getParameterGroupId()));
                $parameterGroup->setParameterGroupBase($parameterGroupBase);
                $entityManager->persist($parameterGroup);
                $numPersisted++;
            }
        }
        $entityManager->flush();
        $output->writeln($numPersisted.' parameter groups where persisted');
    }

}