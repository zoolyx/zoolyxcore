<?php

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zoolyx\CoreBundle\Model\Language\LanguageManager;

class LanguageManagerTest extends KernelTestCase
{
    /**
     * @var LanguageManager
     */
    private $sut;


    protected function setUp()
    {
        self::bootKernel();
        $this->sut = static::$kernel->getContainer()->get('zoolyx_core.language.manager');
    }

    /** @test */
    public function it_return_the_default_language()
    {
        $this->assertEquals('nl',$this->sut->validate());
    }

    /** @test */
    public function it_does_accept_supported_languages()
    {
        $this->assertEquals('nl',$this->sut->validate('nl'));
        $this->assertEquals('fr',$this->sut->validate('fr'));
    }

    /** @test */
    public function it_changes_not_supported_languages_to_the_default_language()
    {
        $this->assertEquals('nl',$this->sut->validate(''));
        $this->assertEquals('nl',$this->sut->validate('en'));
        $this->assertEquals('nl',$this->sut->validate('anything'));
    }

    /** @test */
    public function it_takes_the_language_from_the_request()
    {
        $request = new \Symfony\Component\HttpFoundation\Request();
        $request->setLocale('nl');
        $this->assertEquals('nl',$this->sut->validate(null,$request));
        $request->setLocale('fr');
        $this->assertEquals('fr',$this->sut->validate(null,$request));
    }

    /** @test */
    public function it_changes_not_supported_languages_in_request_to_the_default_language()
    {
        $request = new \Symfony\Component\HttpFoundation\Request();
        $request->setLocale('');
        $this->assertEquals('nl',$this->sut->validate(null,$request));
        $request->setLocale('anything');
        $this->assertEquals('nl',$this->sut->validate(null,$request));
    }


}
