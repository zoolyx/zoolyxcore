<?php



use Zoolyx\CoreBundle\Model\XmlParser\Parser;
use Prophecy\PhpUnit\ProphecyTestCase;

class ParserTest extends ProphecyTestCase
{
    /**
     * @var Parser
     */
    private $sut;

    protected function setUp()
    {
        parent::setUp();
        $key = 'A CONSTANT';
        $method = 'some method';
        $reverseMethod = 'some reverse method';
        $parser = $this->prophesize('\Zoolyx\CoreBundle\Model\XmlParser\ParserInterface')->reveal();
        $required = false;
        $this->sut = new Parser($key, $method, $reverseMethod, $parser, $required);
    }

    /** @test */
    public function it_is_a_parser()
    {
        $this->assertInstanceOf('\Zoolyx\CoreBundle\Model\XmlParser\Parser', $this->sut);
    }

    /** @test */
    public function testEqual()
    {
        $this->assertEquals(0, 0);
    }
}
