<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 06.12.16
 * Time: 19:30
 */

use Zoolyx\CoreBundle\Tests\Controller\Api\ApiTestCase;

class ReportVersionFileManagerTest extends ApiTestCase
{
    public function testEmptyVersion()
    {
        $reportVersionFileManager = $this->getService('zoolyx_core.report_version.file_manager');

        $reportVersion = new \Zoolyx\CoreBundle\Entity\ReportVersion();
        $files = $reportVersionFileManager->getFiles($reportVersion);
        $this->assertEquals(array(), $files);
    }

    public function testVersionWithFilesInParameter()
    {
        $reportVersionFileManager = $this->getService('zoolyx_core.report_version.file_manager');

        $parameter = new \Zoolyx\CoreBundle\Entity\Parameter();
        $parameterGroup = new \Zoolyx\CoreBundle\Entity\ParameterGroup();
        $parameterGroup->addParameter($parameter);
        $sample = new \Zoolyx\CoreBundle\Entity\Sample();
        $sample->addParameterGroup($parameterGroup);
        $reportVersion = new \Zoolyx\CoreBundle\Entity\ReportVersion();
        $reportVersion->addSample($sample);

        $files = $reportVersionFileManager->getFiles($reportVersion);
        $this->assertEquals(0, count($files));

        $parameter->addFile(new \Zoolyx\CoreBundle\Entity\File());
        $files = $reportVersionFileManager->getFiles($reportVersion);
        $this->assertEquals(1, count($files));
    }

    public function testReportWithIncompleteStructure()
    {
        $reportVersionFileManager = $this->getService('zoolyx_core.report_version.file_manager');

        $reportVersion = new \Zoolyx\CoreBundle\Entity\ReportVersion();

        $sample = new \Zoolyx\CoreBundle\Entity\Sample();
        $reportVersion->addSample($sample);
        $files = $reportVersionFileManager->getFiles($reportVersion);
        $this->assertEquals(0, count($files));

        $parameterGroup = new \Zoolyx\CoreBundle\Entity\ParameterGroup();
        $sample->addParameterGroup($parameterGroup);
        $files = $reportVersionFileManager->getFiles($reportVersion);
        $this->assertEquals(0, count($files));

        $parameter = new \Zoolyx\CoreBundle\Entity\Parameter();
        $parameterGroup->addParameter($parameter);
        $files = $reportVersionFileManager->getFiles($reportVersion);
        $this->assertEquals(0, count($files));

        $parameter->addFile(new \Zoolyx\CoreBundle\Entity\File());
        $files = $reportVersionFileManager->getFiles($reportVersion);
        $this->assertEquals(1, count($files));
    }

}