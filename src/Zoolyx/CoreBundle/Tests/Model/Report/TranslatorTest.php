<?php

use Zoolyx\CoreBundle\Entity\ReportVersion;
use Zoolyx\CoreBundle\Model\Exception\LanguageNotSupportedException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Model\Exception\VersionExistsException;
use Zoolyx\CoreBundle\Model\Report\Translator;

class TranslatorTest extends KernelTestCase
{
    /**
     * @var Translator
     */
    private $sut;
    /** @var Report */
    private $report;


    protected function setUp()
    {
        self::bootKernel();
        $this->report = new Report();
        $this->report->setRequestId('555');
        $this->sut = static::$kernel->getContainer()->get('zoolyx_core.report.translator');
    }

    /** @test */
    public function it_does_not_translate_to_chinese()
    {
        $this->setExpectedException(LanguageNotSupportedException::class);
        $this->sut->translateTo($this->report,'cn');

    }

    /** @test */
    public function it_does_translate_to_dutch()
    {
        $this->sut->translateTo($this->report,'nl');

    }

    /** @test */
    public function it_does_not_translate_when_version_exists()
    {
        $reportVersion = new ReportVersion();
        $reportVersion->setLanguage('nl');
        $this->report->addVersion($reportVersion);
        $this->setExpectedException(VersionExistsException::class);
        $this->sut->translateTo($this->report,'nl');
    }

    /*
    public function it_raises_a_warning_when_ftp_connection_fails()
    {
        $this->setExpectedException(PHPUnit_Framework_Error_Warning::class);
        $this->sut->translateTo($this->report,'fr');

    }

    public function it_raises_a_warning_when_file_could_not_be_written()
    {
        $this->setExpectedException(PHPUnit_Framework_Error::class);
        $this->sut->translateTo($this->report,'fr');

    }

    */
}
