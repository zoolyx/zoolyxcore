<?php

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Zoolyx\CoreBundle\Entity\Practice;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Model\Request\RequestManager;

class RequestManagerTest extends WebTestCase
{
    /**
     * @var RequestManager
     */
    private $sut;

    private $client;


    protected function setUp()
    {
        self::bootKernel();
        $this->client = self::createClient();
        $this->sut = $this->client->getContainer()->get('zoolyx_core.manager.request');
    }

    /** @test */
    public function it_initialises_requestData()
    {
        $client = self::createClient();
        /** @var RequestManager $requestManager */
        $requestManager = $client->getContainer()->get('zoolyx_core.manager.request');
        $requestManager->init(new User(),array());
        $requestData = $requestManager->getRequestData();

        $this->assertInstanceOf('Zoolyx\CoreBundle\Model\Request\RequestData',$requestData);
    }

    /** @test */
    public function it_handles_admin_request()
    {   $email = 'admin@test.be';
        $user = new User();
        $user->setEmail($email)->addRole('ROLE_ADMINISTRATOR');

        $client = self::createClient();
        /** @var RequestManager $requestManager */
        $requestManager = $client->getContainer()->get('zoolyx_core.manager.request');
        $requestManager->init($user,array());
        $requestData = $requestManager->getRequestData();

        $this->assertEquals($email,$requestData->getVeterinary()->getEmail());
    }

    /** @test */
    public function it_handles_veterinary_request()
    {
        $email = 'veterinary@test.be';
        $limsId = 'lims_123';

        $user = new User();
        $user->setEmail($email)->addRole('ROLE_VETERINARY');

        $veterinary = new Veterinary();
        $veterinary->setAccount($user);

        $practice = new Practice();

        $veterinaryPractice = new VeterinaryPractice();
        $veterinaryPractice->setLimsId($limsId)
            ->setVeterinary($veterinary)
            ->setPractice($practice);


        $veterinaryPracticeRepository = $this->getMockBuilder('Zoolyx\CoreBundle\Entity\Repository\VeterinaryPracticeRepository')
            ->disableOriginalConstructor()
            ->getMock();
        $veterinaryPracticeRepository->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($veterinaryPractice));

        $client = self::createClient();
        $client->getContainer()->set('zoolyx_core.repository.veterinary_practice', $veterinaryPracticeRepository);

        /** @var RequestManager $requestManager */
        $requestManager = $client->getContainer()->get('zoolyx_core.manager.request');
        $requestManager->init($user, array("lims_id"=>$limsId));
        $requestData = $requestManager->getRequestData();
        $this->assertEquals($email,$requestData->getVeterinary()->getEmail());
    }

    /** @test */
    public function it_handles_veterinary_request_without_limsid()
    {
        $email = 'veterinary@test.be';

        $user = new User();
        $user->setEmail($email)->addRole('ROLE_VETERINARY');

        $veterinary = new Veterinary();
        $veterinary->setAccount($user);

        $practice = new Practice();

        $veterinaryPractice = new VeterinaryPractice();
        $veterinaryPractice
            ->setVeterinary($veterinary)
            ->setPractice($practice);


        $veterinaryPracticeRepository = $this->getMockBuilder('Zoolyx\CoreBundle\Entity\Repository\VeterinaryPracticeRepository')
            ->disableOriginalConstructor()
            ->getMock();
        $veterinaryPracticeRepository->expects($this->any())
            ->method('getVeterinaryPracticesViaPracticeOf')
            ->will($this->returnValue(array($veterinaryPractice)));

        $client = self::createClient();
        $client->getContainer()->set('zoolyx_core.repository.veterinary_practice', $veterinaryPracticeRepository);

        /** @var RequestManager $requestManager */
        $requestManager = $client->getContainer()->get('zoolyx_core.manager.request');
        $requestManager->init($user, array());
        $requestData = $requestManager->getRequestData();
        $this->assertEquals($email,$requestData->getVeterinary()->getEmail());
    }
}
