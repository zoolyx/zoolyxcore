<?php

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Zoolyx\CoreBundle\Model\Request\RequestIdProvider;

class RequestIdProviderTest extends WebTestCase
{

    protected function setUp()
    {
        self::bootKernel();
    }

    /** @test */
    public function it_adds_a_prefix()
    {
        $client = self::createClient();
        /** @var RequestIdProvider $requestIdProvider */
        $requestIdProvider = $client->getContainer()->get('zoolyx_core.provider.request_id');

        $requestId = "test123";
        $prefix = "www";
        $newRequestId = $requestIdProvider->addPrefix($requestId,$prefix,20);

        $this->assertEquals($prefix."+".$requestId, $newRequestId);
    }
    /** @test */
    public function it_replaces_a_prefix()
    {
        $client = self::createClient();
        /** @var RequestIdProvider $requestIdProvider */
        $requestIdProvider = $client->getContainer()->get('zoolyx_core.provider.request_id');

        $requestId = "test123";
        $originalPrefix = "kmsh";
        $prefix = "www";
        $newRequestId = $requestIdProvider->addPrefix($originalPrefix."+".$requestId,$prefix,20);

        $this->assertEquals($prefix."+".$requestId, $newRequestId);
    }

    /** @test */
    public function it_does_not_add_a_prefix_if_too_long()
    {
        $client = self::createClient();
        /** @var RequestIdProvider $requestIdProvider */
        $requestIdProvider = $client->getContainer()->get('zoolyx_core.provider.request_id');

        $requestId = "test123";
        $maxLength = strlen($requestId)+2;
        $prefix = "www";
        $newRequestId = $requestIdProvider->addPrefix($requestId,$prefix,$maxLength);

        $this->assertEquals($requestId, $newRequestId);
    }

}
