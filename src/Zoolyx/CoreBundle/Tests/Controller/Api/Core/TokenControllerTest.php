<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 06.12.16
 * Time: 21:30
 */


use Zoolyx\CoreBundle\Tests\Controller\Api\ApiTestCase;

class TokenControllerTest extends ApiTestCase {

    const TOKENS_URL = '/app_dev.php/api/tokens';

    protected function setUp()
    {
        parent::setUp();
        $this->createUser('dierenarts');
    }


    public function testPOSTCreateToken() {
        $response = $this->client->post(self::TOKENS_URL, [
            'auth' => ['dierenarts', 'tmp']
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyExists(
            $response,
            'token'
        );
    }

    public function testPOSTTokenInvalidCredentials()
    {
        $response = $this->client->post(self::TOKENS_URL, [
            'auth' => ['dierenarts', 'fout']
        ]);
        $this->assertEquals(401, $response->getStatusCode());
    }
} 