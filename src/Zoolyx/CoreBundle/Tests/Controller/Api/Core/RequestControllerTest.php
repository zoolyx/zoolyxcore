<?php
use Zoolyx\CoreBundle\Tests\Controller\Api\ApiTestCase;

/**
 * Created by PhpStorm.
 * User: bart
 * Date: 06.12.16
 * Time: 20:12
 */

class RequestControllerTest extends ApiTestCase
{

    public function testRequiresAuthentication()
    {
        $this->createOrganisation('kmsh');
        $data = file_get_contents('/opt/repositories/zoolyx/src/Zoolyx/CoreBundle/Tests/files/requests/kmsh_request.xml');
        $headers = array('Content-Type' => 'application/xml');

        $response = $this->client->post('/app_dev.php/api/request', [
            'headers' => $headers,
            'body' => $data
        ]);
        $this->assertEquals(401, $response->getStatusCode());

    }

    public function testPostWorksWithCorrectRole()
    {
        $this->createOrganisation('kmsh');
        $data = file_get_contents('/opt/repositories/zoolyx/src/Zoolyx/CoreBundle/Tests/files/requests/kmsh_request.xml');
        $headers = array('Content-Type' => 'application/xml');

        $response = $this->client->post('/app_dev.php/api/request', [
            'headers' => $this->getAuthorizedHeaders('kmsh',$headers),
            'body' => $data
        ]);

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPostFailsWhenXmlDoesNotPassXsd()
    {
        $this->createOrganisation('kmsh');
        $data = file_get_contents('/opt/repositories/zoolyx/src/Zoolyx/CoreBundle/Tests/files/requests/wrong_request.xml');
        $headers = array('Content-Type' => 'application/xml');

        $response = $this->client->post('/app_dev.php/api/request', [
            'headers' => $this->getAuthorizedHeaders('kmsh',$headers),
            'body' => $data
        ]);

        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testPostFailsWithoutRole()
    {
        $this->createUser('dierenarts');

        $data = file_get_contents('/opt/repositories/zoolyx/src/Zoolyx/CoreBundle/Tests/files/requests/request.xml');
        $headers = array('Content-Type' => 'application/xml');
        $response = $this->client->post('/app_dev.php/api/request', [
            'headers' => $this->getAuthorizedHeaders('dierenarts',$headers),
            'body' => $data
        ]);

        $this->assertEquals(403, $response->getStatusCode());
    }
} 