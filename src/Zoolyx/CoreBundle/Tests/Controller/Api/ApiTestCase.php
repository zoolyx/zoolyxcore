<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 06.12.16
 * Time: 19:33
 */

namespace Zoolyx\CoreBundle\Tests\Controller\Api;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\Persistence\ObjectManager;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;

class ApiTestCase extends KernelTestCase
{
    private static $staticClient;

    /** @var Client */
    protected $client;
    /** @var ResponseAsserter */
    private $responseAsserter;

    public static function setUpBeforeClass()
    {
        self::$staticClient = new Client([
            'base_url' => 'http://www.zoolyx.localhost',
            'defaults' => [
                'exceptions' => false
            ]
        ]);
        self::bootKernel();
    }
    protected function setUp()
    {
        $this->client = self::$staticClient;
        $this->purgeDatabase();
    }

    /**
     * Clean up Kernel usage in this test.
     */
    protected function tearDown()
    {
        // purposefully not calling parent class, which shuts down the kernel
    }
    private function purgeDatabase()
    {
        $purger = new ORMPurger($this->getService('doctrine.orm.default_entity_manager'));
        $purger->purge();
    }
    protected function getService($id)
    {
        return self::$kernel->getContainer()
            ->get($id);
    }

    /**
     * @param $username
     * @param string $plainPassword
     * @return User
     */
    protected function createAccount($username, $plainPassword = 'tmp')
    {
        $user = new User();
        $user->setUsername($username)->setEmail($username.'@test.be')->setEnabled(true);
        $password = $this->getService('security.password_encoder')
            ->encodePassword($user, $plainPassword);
        $user->setPassword($password);
        return $user;
    }


    protected function createUser($username, $plainPassword = 'tmp')
    {
        $user = new User();
        $user->setUsername($username)->setEmail($username.'@test.be')->setEnabled(true);
        $password = $this->getService('security.password_encoder')
            ->encodePassword($user, $plainPassword);
        $user->setPassword($password);
        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();
        return $user;
    }
    protected function createOrganisation($username, $plainPassword = 'tmp')
    {
        $account = $this->createAccount($username, $plainPassword);
        $account->addRole('ROLE_ORGANISATION');
        $veterinary = new Veterinary();
        $veterinary->setAccount($account);

        $em = $this->getEntityManager();
        $em->persist($account);
        $em->persist($veterinary);
        $em->flush();
        return $account;
    }

    /**
     * @return ObjectManager
     */
    protected function getEntityManager()
    {
        return $this->getService('doctrine.orm.entity_manager');
    }

    protected function asserter()
    {
        if ($this->responseAsserter === null) {
            $this->responseAsserter = new ResponseAsserter();
        }
        return $this->responseAsserter;
    }

    protected function getAuthorizedHeaders($username, $headers = array())
    {
        $token = $this->getService('lexik_jwt_authentication.encoder')
            ->encode(['username' => $username]);
        $headers['Authorization'] = 'Bearer '.$token;
        return $headers;
    }
} 