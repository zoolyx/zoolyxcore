<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 06.12.16
 * Time: 19:30
 */

use Zoolyx\CoreBundle\Tests\Controller\Api\ApiTestCase;

class ImportControllerTest extends ApiTestCase
{

    public function testPOST()
    {
        $data = '<?xml version="1.0" encoding="UTF-8"?><rq id="1701-55555-api"><cumulative>true</cumulative><practiceRef></practiceRef><da id="993159"><lastName>a</lastName><firstName>b</firstName><street>E. RUELENSVEST 107</street><zipCode>3001</zipCode><city>HEVERLEE</city><country>B</country><language>nl</language><email>tom.nagels@nowhere.be</email></da><ad id="1161832"><lastName>TEST</lastName><firstName>OLGA</firstName><street>DURMENSTR 20</street><zipCode>9300</zipCode><city>AALST</city><country>B</country><language>nl</language><email>tom.nagels@tmp.be</email></ad><sc id="1601-55555"><date_received fmt="YYYY-MM-DD">2016-01-13</date_received><date_completed fmt="YYYY-MM-DD"></date_completed><completed>0</completed><pe id="1161832"><name>OLGA</name><species_code>K</species_code><gender>M</gender><birth_date fmt="YYYY-MM-DD">2015-01-01</birth_date><breed>Fv</breed><chipID>9819919199911111</chipID></pe><pg id="10"><description lang="nl">KLINISCHE GEGEVENS</description><pa id="RAS"><description lang="nl">Breed / Ras</description><status>C</status><loinc></loinc><link>http://wikilab.zoolyx.be/mediawiki/index.php/Ras</link><pp>RAS</pp><result>Fv</result><unit></unit><result_flag>N</result_flag><refRange/><comment lang="nl"></comment><execution_date fmt="YYYY-MM-DD">2016-05-18</execution_date><date_expected fmt="YYYY-MM-DD">2016-05-18</date_expected></pa></pg></sc></rq>';

        $response = $this->client->post('/app_dev.php/api/lims/import', [
            'headers' => [
                'Content-Type' => 'application/xml'
            ],
            'body' => $data
        ]);

        //$this->assertEquals(200, $response->getStatusCode());

        $responseCode = 200;
        $this->assertEquals(200, $responseCode);

    }

} 