<?php

namespace Zoolyx\CoreBundle\Controller\Owner;

use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Exception\NotValidCurrentPageException;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zoolyx\CoreBundle\Entity\Repository\ReportRepository;
use Zoolyx\CoreBundle\Model\Report\ReportBuilder;

class ReportController extends Controller
{

    /**
     * @var ReportRepository $reportRepository
     *
     * @DI\Inject("zoolyx_core.repository.report")
     */
    private $reportRepository;

    /**
     * @var ReportBuilder $reportBuilder
     *
     * @DI\Inject("zoolyx_core.report.builder")
     */
    private $reportBuilder;

    public function indexAction(Request $request, $keyword='') {
        $user = $this->getUser();
        if ($keyword) {
            $query = $this->reportRepository->searchForOwnerQuery($user, $keyword);
        } else {
            $query = $this->reportRepository->findRecentForOwnerQuery($user);
        }

        $adapter = new DoctrineORMAdapter($query);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(15);
        $page = $request->query->get('page', 1);
        try  {
            $pager->setCurrentPage($page);
        }
        catch(NotValidCurrentPageException $e) {
            throw new NotFoundHttpException('Illegal page');
        }

        return $this->render('ZoolyxCoreBundle:Owner:Report/index.html.twig', array(
            'keyword' => $keyword,
            'reportBuilder' => $this->reportBuilder,
            'pager' => $pager
        ));
    }
}
