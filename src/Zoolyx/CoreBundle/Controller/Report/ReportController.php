<?php

namespace Zoolyx\CoreBundle\Controller\Report;

use Doctrine\Common\Persistence\ObjectManager;
use DOMDocument;
use JMS\DiExtraBundle\Annotation as DI;
use Knp\Bundle\SnappyBundle\Snappy\LoggableGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Zoolyx\CoreBundle\Entity\OriginalRequest;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\ReportVersion;
use Zoolyx\CoreBundle\Entity\Repository\UserRepository;
use Zoolyx\CoreBundle\Entity\Sample;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Event\ReportEvent;
use Zoolyx\CoreBundle\Events;
use Zoolyx\CoreBundle\Model\Exception\ReportNotFoundException;
use Zoolyx\CoreBundle\Model\Report\Authorisation\AccessValidator;
use Zoolyx\CoreBundle\Model\Report\Menu\ReportMenu;
use Zoolyx\CoreBundle\Model\Report\ReportBuilder;
use Zoolyx\CoreBundle\Model\Report\ReportHash;
use Zoolyx\CoreBundle\Model\Report\ReportLoader;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Zoolyx\TicketBundle\Entity\QuestionEvent;
use Zoolyx\TicketBundle\Entity\Repository\TicketRepository;
use Zoolyx\TicketBundle\Entity\Ticket;
use Zoolyx\TicketBundle\Event\NewTicketEvent;
use Zoolyx\TicketBundle\Event\UpdatedTicketEvent;
use Zoolyx\TicketBundle\Forms\QuestionEventType;
use Zoolyx\TicketBundle\Model\Ticket\TicketContentManager;
use Zoolyx\TicketBundle\Model\Ticket\TicketContextManager;
use Zoolyx\TicketBundle\Model\Ticket\TicketFactory;
use Zoolyx\TicketBundle\Model\Ticket\TicketWatchManager;

class ReportController extends Controller
{
    /**
     * @var TicketRepository $ticketRepository
     *
     * @DI\Inject("zoolyx_ticket.repository.ticket")
     */
    private $ticketRepository;

    /**
     * @var UserRepository $userRepository
     *
     * @DI\Inject("zoolyx_core.repository.user")
     */
    private $userRepository;

    /**
     * @var TicketFactory
     *
     * @DI\Inject("zoolyx_ticket.factory.ticket")
     */
    private $ticketFactory;

    /**
     * @var TicketContentManager
     *
     * @DI\Inject("zoolyx_ticket.manager.ticket_content")
     */
    private $ticketContentManager;

    /**
     * @var TicketContextManager
     *
     * @DI\Inject("zoolyx_ticket.manager.ticket_context")
     */
    private $ticketContextManager;

    /**
     * @var TicketWatchManager
     *
     * @DI\Inject("zoolyx_ticket.manager.ticket_watch")
     */
    private $ticketWatchManager;

    /**
     * @var ReportLoader $reportLoader
     *
     * @DI\Inject("zoolyx_core.report.loader")
     */
    private $reportLoader;

    /**
     * @var ReportBuilder $reportBuilder
     *
     * @DI\Inject("zoolyx_core.report.builder")
     */
    private $reportBuilder;

    /**
     * @var LoggableGenerator $snappy
     *
     * @DI\Inject("knp_snappy.pdf")
     */
    private $snappy;

    /**
     * @var AccessValidator $accessValidator
     *
     * @DI\Inject("zoolyx_core.report.access_validator")
     */
    private $accessValidator;

    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager")
     */
    private $entityManager;

    /**
     * @var Translator $translator
     *
     *  @DI\Inject("translator")
     */
    private $translator;

    /**
     * @var EventDispatcherInterface
     *
     * @DI\Inject("event_dispatcher")
     */
    private $eventDispatcher;

    public function viewAction(Request $request, $hash, $sampleId = null) {
        if ($request->getLocale() == '') {
            return $this->redirectToRoute('zoolyx_report_no_redirect', array('hash'=> $hash));
        }
        /** @var ReportVersion $reportVersion */
        $reportVersion = $this->reportLoader->loadByHash(new ReportHash($hash));

        /** @var User $user */
        $user = $this->getUser();

        if (!$this->accessValidator->canAccess($user, $reportVersion->getReport())) {
            return $this->redirectToRoute('zoolyx_account_settings');
        }

        $this->reportBuilder->setReportVersion($reportVersion,$sampleId);
        $this->reportBuilder->setLanguage($request->getSession()->get('_locale', $user->getLanguage()));
        $this->reportBuilder->loadSuggestions();

        $params = array(
            'report' => $this->reportBuilder,
            'reportMenu' => new ReportMenu(ReportMenu::ITEM_DISPLAY_REPORT)
        );
        return $this->render('ZoolyxCoreBundle:ReportView:report.html.twig', $params);
    }

    public function viewNoRedirectAction(Request $request, $hash, $sampleId = null) {
        /** @var ReportVersion $reportVersion */
        $reportVersion = $this->reportLoader->loadByHash(new ReportHash($hash));

        /** @var User $user */
        $user = $this->getUser();

        if (!$this->accessValidator->canAccess($user, $reportVersion->getReport())) {
            return $this->redirectToRoute('zoolyx_account_settings');
        }

        $this->reportBuilder->setReportVersion($reportVersion,$sampleId);
        $this->reportBuilder->setLanguage($request->getSession()->get('_locale', $user->getLanguage()));
        $this->reportBuilder->loadSuggestions();

        $params = array(
            'report' => $this->reportBuilder,
            'reportMenu' => new ReportMenu(ReportMenu::ITEM_DISPLAY_REPORT)
        );
        return $this->render('ZoolyxCoreBundle:ReportView:report.html.twig', $params);
    }

    public function viewByReferenceAction(Request $request, $reference) {
        /** @var User $user */
        $user = $this->getUser();

        /** @var ReportVersion $reportVersion */
        $reportVersion = $this->reportLoader->loadByReference($user, $reference);

        /** @var User $user */
        $user = $this->getUser();

        if (!$this->accessValidator->canAccess($user, $reportVersion->getReport())) {
            return $this->redirectToRoute('zoolyx_account_settings');
        }

        $this->reportBuilder->setReportVersion($reportVersion);
        $this->reportBuilder->setLanguage($request->getSession()->get('_locale', $user->getLanguage()));
        $this->reportBuilder->loadSuggestions();

        $params = array(
            'report' => $this->reportBuilder,
            'reportMenu' => new ReportMenu(ReportMenu::ITEM_DISPLAY_REPORT)
        );
        return $this->render('ZoolyxCoreBundle:ReportView:report.html.twig', $params);
    }

    public function downloadAction($hash, $sampleId = null, Request $request) {
        $avoidPageBreak = (null !== $request->request->get('avoid_page_break')) ? true : false;

        try {
            /** @var ReportVersion $reportVersion */
            $reportVersion = $this->reportLoader->loadByAlternativeHash(new ReportHash($hash));

        } catch (ReportNotFoundException $e) {
            /** @var ReportVersion $reportVersion */
            $reportVersion = $this->reportLoader->loadByHash(new ReportHash($hash));
            if (!$this->accessValidator->canAccess($this->getUser(), $reportVersion->getReport())) {
                return $this->redirectToRoute('zoolyx_account_settings');
            }
            $html = $this->renderView('ZoolyxCoreBundle:Report:report_with_link.html.twig', array(
                'hash' => $hash
            ));
            return new Response($this->snappy->getOutputFromHtml($html,array(
                    'orientation'=>'Portrait'
                )), 200, array(
                    'Content-Type' => 'application/pdf',
                    'Content-Disposition' => 'attachment; filename="report_'.$hash.'.pdf"'
                )
            );
        }

        if (!$this->accessValidator->canAccess($this->getUser(), $reportVersion->getReport())) {
            return $this->redirectToRoute('zoolyx_account_settings');
        }

        $this->reportBuilder->setReportVersion($reportVersion, $sampleId);
        $this->reportBuilder->setLanguage($request->getSession()->get('_locale', $this->getUser()->getLanguage()));

        $html = $this->renderView('ZoolyxCoreBundle:Report:report.html.twig', array(
            'report' => $this->reportBuilder,
            'avoidPageBreakInGroup' => $avoidPageBreak,
            'isPdf' => true
        ));
        $htmlHeader =  $this->renderView('ZoolyxCoreBundle:Report:header.html.twig', array(
            'report' => $this->reportBuilder
        ));
        $htmlFooter =  $this->renderView('ZoolyxCoreBundle:Report:footer.html.twig');

        $hash = $reportVersion->getReport()->getHash();
        return new Response($this->snappy->getOutputFromHtml($html,array(
                'orientation'=>'Portrait',
                'header-spacing' => 5,
                'header-html' => $htmlHeader,
                'footer-spacing' => 10,
                'footer-html' => $htmlFooter
            )), 200, array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="report_'.$hash.'.pdf"'
            )
        );
    }

    public function downloadAllAction($hash, Request $request) {
        $avoidPageBreak = (null !== $request->request->get('avoid_page_break')) ? true : false;

        try {
            /** @var ReportVersion $reportVersion */
            $reportVersion = $this->reportLoader->loadByAlternativeHash(new ReportHash($hash));

        } catch (ReportNotFoundException $e) {
            /** @var ReportVersion $reportVersion */
            $reportVersion = $this->reportLoader->loadByHash(new ReportHash($hash));
            if (!$this->accessValidator->canAccess($this->getUser(), $reportVersion->getReport())) {
                return $this->redirectToRoute('zoolyx_account_settings');
            }
            $html = $this->renderView('ZoolyxCoreBundle:Report:report_with_link.html.twig', array(
                'hash' => $hash
            ));
            return new Response($this->snappy->getOutputFromHtml($html,array(
                    'orientation'=>'Portrait'
                )), 200, array(
                    'Content-Type' => 'application/pdf',
                    'Content-Disposition' => 'attachment; filename="report_'.$hash.'.pdf"'
                )
            );
        }

        if (!$this->accessValidator->canAccess($this->getUser(), $reportVersion->getReport())) {
            return $this->redirectToRoute('zoolyx_account_settings');
        }

        $fileNames = array();
        $snappyPath = '/tmp/';

        /** @var Sample $sample */
        foreach ($reportVersion->getSamples() as $sample) {
            $this->reportBuilder->setReportVersion($reportVersion, $sample->getId());
            $this->reportBuilder->setLanguage($request->getSession()->get('_locale', $this->getUser()->getLanguage()));

            $html = $this->renderView('ZoolyxCoreBundle:Report:report.html.twig', array(
                'report' => $this->reportBuilder,
                'avoidPageBreakInGroup' => $avoidPageBreak,
                'isPdf' => true
            ));
            $htmlHeader =  $this->renderView('ZoolyxCoreBundle:Report:header.html.twig', array(
                'report' => $this->reportBuilder
            ));
            $htmlFooter =  $this->renderView('ZoolyxCoreBundle:Report:footer.html.twig');

            $filename = $snappyPath.$sample->getId().'.pdf';
            $fileNames[] = $filename;

            $this->snappy->generateFromHtml($html,$filename,array(
                'orientation'=>'Portrait',
                'header-spacing' => 5,
                'header-html' => $htmlHeader,
                'footer-spacing' => 10,
                'footer-html' => $htmlFooter
            ), true);
        }

        exec("gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=".$snappyPath."out.pdf ".implode(" ",$fileNames));
        exec("rm ".implode(" ",$fileNames));

        //return new Response('ok: '.$cmd, 200);
        $hash = $reportVersion->getReport()->getHash();
        return (new BinaryFileResponse($snappyPath."out.pdf", 200, array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="report_'.$hash.'.pdf"'
            )
        ))->deleteFileAfterSend(true);
    }


    public function questionAction($hash, $ticketId = null, $accountId = null, Request $request) {
        /** @var ReportVersion $reportVersion */
        $reportVersion = $this->reportLoader->loadByHash(new ReportHash($hash));
        if (!$this->accessValidator->canAccess($this->getUser(), $reportVersion->getReport())) {
            return $this->redirectToRoute('zoolyx_account_settings');
        }

        $this->reportBuilder->setReportVersion($reportVersion, null, false);
        /** @var Report $report */
        $report = $reportVersion->getReport();

        /** @var Ticket $ticket */
        $ticket = null;
        $tickets = array();
        $ticketOwner = null;
        if ($ticketId) {
            $ticket = $this->ticketRepository->find($ticketId);
            $ticketOwner = $ticket ? $ticket->getOwner() : null;
        }

        if ($accountId) {
            $ticketOwner = $this->userRepository->find($accountId);
        }
        if (is_null($ticketOwner)) {
            $ticketOwner =
                $this->getUser()->hasRole('ROLE_ADMINISTRATOR') || $this->getUser()->hasRole('ROLE_SUPER_ADMIN') ?
                    $report->getVeterinaryPractice()->getVeterinary()->getAccount() :
                    $this->getUser();
        }
        if ($ticketOwner) {
            $tickets = $this->ticketRepository->findForVeterinary($report, $ticketOwner);
        }

        if (is_null($ticket) && count($tickets)>0) {
            $ticket = $tickets[0];
        }

        $questionEvent = new QuestionEvent();
        $form = $this->createForm(new QuestionEventType($this->translator), $questionEvent);

        if (!is_null($request)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $ticketIsNew = false;
                if (is_null($ticket)) {
                    $ticket = $this->ticketFactory->create($ticketOwner);
                    $ticketIsNew = true;

                    $this->ticketContextManager->setReport($ticket, $report);
                    $this->ticketContextManager->setVeterinary($ticket, $report->getVeterinaryPractice()->getVeterinary());
                    $this->ticketContextManager->setOwner($ticket, $report->getOwner());
                }
                $questionEvent->setInternal(false);

                $this->ticketContentManager->addQuestion($ticket, $questionEvent, $this->getUser());
                $this->ticketContentManager->handleAttachments($questionEvent);
				
				$this->entityManager->persist($ticket);
                $this->entityManager->flush();

                if ($ticketIsNew) {
                    $this->eventDispatcher->dispatch(Events::NEW_TICKET, new NewTicketEvent($ticket, $this->getUser(), $questionEvent->getContent()));
                } else {
                    $this->eventDispatcher->dispatch(Events::UPDATED_TICKET, new UpdatedTicketEvent($questionEvent));
                }

            }
        };

        if ($ticket) {
            $this->ticketWatchManager->watchTicket($ticket, $this->getUser());
            $this->entityManager->persist($ticket);
            $this->entityManager->flush();  
        }

        $questionEvent = new QuestionEvent();
        $form = $this->createForm(new QuestionEventType($this->translator), $questionEvent);

        return $this->render('ZoolyxCoreBundle:ReportView:question.html.twig', array(
            'report' => $this->reportBuilder,
            'ticket' => $ticket,
            'ticketOwner' => $ticketOwner,
            'tickets' => $tickets,
            'form' => $form->createView(),
            'reportMenu' => new ReportMenu(ReportMenu::ITEM_ASK_QUESTION)
        ));
    }

    public function originalRequestAction($hash) {
        /** @var ReportVersion $reportVersion */
        $reportVersion = $this->reportLoader->loadByHash(new ReportHash($hash));
        if (!$this->accessValidator->canAccess($this->getUser(), $reportVersion->getReport())) {
            return $this->redirectToRoute('zoolyx_account_settings');
        }
        $this->reportBuilder->setReportVersion($reportVersion, null, false);

        /** @var OriginalRequest $originalRequest */
        $originalRequest = $reportVersion->getReport()->getOriginalRequest();

        if (!$originalRequest) {
            //not found
        }
        return $this->render('ZoolyxCoreBundle:ReportView:original_request.html.twig', array(
            'report' => $this->reportBuilder,
            'originalRequest' => $originalRequest,
            'reportMenu' => new ReportMenu(ReportMenu::ITEM_ORIGINAL_REQUEST)
        ));
    }

    public function sendNotificationAction($hash) {
        /** @var ReportVersion $reportVersion */
        $reportVersion = $this->reportLoader->loadByHash(new ReportHash($hash));
        $report = $reportVersion->getReport();
        $this->eventDispatcher->dispatch(Events::REPORT_IMPORTED, new ReportEvent($report));

        return $this->redirect($this->generateUrl('zoolyx_report', array('hash'=>$hash)));
    }
}
