<?php

namespace Zoolyx\CoreBundle\Controller\Report;

use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Zoolyx\AccountBundle\Model\Token\TokenGenerator;
use Zoolyx\CoreBundle\Entity\Owner;
use Zoolyx\CoreBundle\Entity\Practice;
use Zoolyx\CoreBundle\Entity\ReportShare;
use Zoolyx\CoreBundle\Entity\ReportVersion;
use Zoolyx\CoreBundle\Entity\Repository\OwnerRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Entity\VeterinaryPracticeReport;
use Zoolyx\CoreBundle\Event\NewAccountEvent;
use Zoolyx\CoreBundle\Event\ReportUserEvent;
use Zoolyx\CoreBundle\Events;
use Zoolyx\CoreBundle\Model\Report\Authorisation\AccessValidator;
use Zoolyx\CoreBundle\Model\Report\Menu\ReportMenu;
use Zoolyx\CoreBundle\Model\Report\ReportBuilder;
use Zoolyx\CoreBundle\Model\Report\ReportHash;
use Zoolyx\CoreBundle\Model\Report\ReportLoader;
use Zoolyx\CoreBundle\Model\Report\VeterinaryPracticeReport\VeterinaryPracticeReportRoles;

class ShareController extends Controller
{
    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var VeterinaryRepository $veterinaryRepository
     *
     * @DI\Inject("zoolyx_core.repository.veterinary")
     */
    private $veterinaryRepository;

    /**
     * @var OwnerRepository $ownerRepository
     *
     * @DI\Inject("zoolyx_core.repository.owner")
     */
    private $ownerRepository;

    /**
     * @var ReportLoader $reportLoader
     *
     * @DI\Inject("zoolyx_core.report.loader")
     */
    private $reportLoader;

    /**
     * @var ReportBuilder $reportBuilder
     *
     * @DI\Inject("zoolyx_core.report.builder")
     */
    private $reportBuilder;

    /**
     * @var AccessValidator $accessValidator
     *
     * @DI\Inject("zoolyx_core.report.access_validator")
     */
    private $accessValidator;

    /**
     * @var UserManager
     *
     * @DI\Inject("fos_user.user_manager")
     */
    private $userManager;

    /**
     * @var EventDispatcherInterface
     *
     * @DI\Inject("event_dispatcher")
     */
    private $eventDispatcher;

    /**
     * @var TokenGenerator
     *
     * @DI\Inject("zoolyx_account.generator.token")
     */
    private $tokenGenerator;


    public function shareAction(Request $request, $hash) {
        /** @var ReportVersion $reportVersion */
        $reportVersion = $this->reportLoader->loadByHash(new ReportHash($hash));
        if (!$this->accessValidator->canAccess($this->getUser(), $reportVersion->getReport())) {
            return $this->redirectToRoute('zoolyx_account_settings');
        }
        $this->reportBuilder->setReportVersion($reportVersion, null, false);

        $report = $reportVersion->getReport();

        $veterinaries = array();

        /** @var VeterinaryPracticeReport $vpr */
        foreach ($report->getVeterinaryPracticeReports() as $vpr) {
            if ($vpr->getRole() == VeterinaryPracticeReportRoles::VPR_VETERINARY || $vpr->getRole() == VeterinaryPracticeReportRoles::VPR_ORGANISATION) {
                /** @var Practice $practice */
                $practice = $vpr->getVeterinaryPractice()->getPractice();
                /** @var VeterinaryPractice $veterinaryPractice */
                foreach ($practice->getVeterinaryPractices() as $veterinaryPractice) {
                    $veterinary = $veterinaryPractice->getVeterinary();
                    $veterinaries[$veterinary->getId()] = $veterinary;
                }
            } else {
                $veterinary = $vpr->getVeterinaryPractice()->getVeterinary();
                $veterinaries[$veterinary->getId()] = $veterinary;
            }

        }
        
        $owners = array();
        /** @var ReportShare $rs */
        foreach ($report->getShares() as $rs) {
            $owner = $rs->getOwner();
            if ($owner) {
                $owners[$owner->getId()] = $owner;
            }
        }
        
        if ($report->getOwner() && $report->getOwner()->getAccount() ){
            $owner = $report->getOwner();
            $owners[$owner->getId()] = $owner;
        }

        $email = null;
        $newVeterinaries = array();
        if ($request->getMethod() == 'POST') {
            $email = $request->request->get('email',null);
            if ($email) {
                $newVeterinaries = $this->veterinaryRepository->findAllForEmail($email);
            }

            if (count($newVeterinaries) == 0) {
                $user = $this->userManager->findUserByEmail($email);
                if (is_null($user)) {
                    $user = new User();
                    $confirmationToken = $this->tokenGenerator->get(32);
                    $user
                        ->setEmail($email)
                        ->setUsername($email)
                        ->setPlainPassword('none')
                        ->setLanguage('')
                        ->setConfirmationToken($confirmationToken)
    	                ->setPasswordRequestedAt(new \DateTime())
                    ;
                    $this->userManager->updatePassword($user);
                    $this->userManager->updateCanonicalFields($user);
                    $this->entityManager->persist($user);

                    $this->eventDispatcher->dispatch(Events::NEW_ACCOUNT, new NewAccountEvent($user, NewAccountEvent::TRIGGER_REPORT_SHARED));
                }

                $owner = $this->ownerRepository->findOneBy(array('account'=>$user));
                if (is_null($owner)) {
                    $owner = new Owner();
                    $owner->setAccount($user);
                    $user->addRole('ROLE_OWNER');
                    $this->entityManager->persist($owner);
                }

                $reportShare = new ReportShare();
                $reportShare
                    ->setOwner($owner)
                    ->setReport($report);
                $this->entityManager->persist($reportShare);

                //send notification mail to the user
                $this->eventDispatcher->dispatch(Events::REPORT_IMPORTED_FOR_USER, new ReportUserEvent($report,$user));

            } else {
                /** @var Veterinary $newVeterinary */
                foreach ($newVeterinaries as $newVeterinary) {
                    if (!isset($veterinaries[$newVeterinary->getId()])) {
                        $vps = $newVeterinary->getVeterinaryPractices();
                        /** @var VeterinaryPractice $veterinaryPractice */
                        if (count($vps) > 0) {
                            $veterinaryPractice = $vps[0];
                            $vpr = new VeterinaryPracticeReport();
                            $vpr
                                ->setVeterinaryPractice($veterinaryPractice)
                                ->setReport($report)
                                ->setRole(VeterinaryPracticeReportRoles::VPR_SHARED);
                            $this->entityManager->persist($vpr);

                            $this->eventDispatcher->dispatch(Events::REPORT_IMPORTED_FOR_USER, new ReportUserEvent($report,$newVeterinary->getAccount()));
                        }
                    }
                }
            }

            $this->entityManager->flush();

            return $this->redirectToRoute('zoolyx_report_share', array('hash'=>$hash));
        }
		

        return $this->render('ZoolyxCoreBundle:ReportView:share.html.twig', array(
            'report' => $this->reportBuilder,
            'veterinaries' => $veterinaries,
            'owners' => $owners,
            'reportMenu' => new ReportMenu(ReportMenu::ITEM_SHARE)
        ));
    }

}
