<?php

namespace Zoolyx\CoreBundle\Controller\Report;


use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Zoolyx\CoreBundle\Entity\Pet;
use Zoolyx\CoreBundle\Entity\ReportVersion;
use Zoolyx\CoreBundle\Entity\Sample;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Model\Ftp\FtpService;
use Zoolyx\CoreBundle\Model\Lims\BreedManager;
use Zoolyx\CoreBundle\Model\Lims\ParameterProfileFormData;
use Zoolyx\CoreBundle\Model\Lims\ParameterProfileManager;
use Zoolyx\CoreBundle\Model\Lims\ParameterProfile as LimsParameterProfile;
use Zoolyx\CoreBundle\Model\Report\Lister\ParameterLister;
use Zoolyx\CoreBundle\Model\Report\Lister\ParameterProfileLister;
use Zoolyx\CoreBundle\Model\Report\ReportBuilder;
use Zoolyx\CoreBundle\Model\Report\ReportHash;
use Zoolyx\CoreBundle\Model\Report\ReportLoader;
use Zoolyx\CoreBundle\Model\Request\RequestManager;
use Zoolyx\CoreBundle\Model\XmlBuilder\ServiceBuilder\AdditionalRequestServiceBuilder;

class NewRequestController extends Controller
{
    /**
     * @var ReportLoader $reportLoader
     *
     * @DI\Inject("zoolyx_core.report.loader")
     */
    private $reportLoader;

    /**
     * @var ReportBuilder $reportBuilder
     *
     * @DI\Inject("zoolyx_core.report.builder")
     */
    private $reportBuilder;

    /**
     * @var ParameterProfileManager $parameterProfileManager
     *
     * @DI\Inject("zoolyx_core.manager.parameter_profile")
     */
    private $parameterProfileManager;

    /**
     * @var ParameterLister $parameterLister
     *
     * @DI\Inject("zoolyx_core.report.parameter_lister")
     */
    private $parameterLister;

    /**
     * @var ParameterProfileLister $parameterProfileLister
     *
     * @DI\Inject("zoolyx_core.report.parameter_profile_lister")
     */
    private $parameterProfileLister;

    /**
     * @var BreedManager $breedManager
     *
     * @DI\Inject("zoolyx_core.manager.breed")
     */
    private $breedManager;

    /**
     * @var FtpService $ftpService
     *
     *  @DI\Inject("zoolyx_core.ftp.service")
     */
    private $ftpService;

    /**
     * @var RequestManager $requestManager
     *
     *  @DI\Inject("zoolyx_core.manager.request")
     */
    private $requestManager;


    public function newAction() {
        $this->requestManager->init();
        $this->requestManager->setReportHash('new');
        $this->requestManager->saveToSession();

        return $this->render('ZoolyxCoreBundle:Request:step/a2_sample_form.html.twig', array());
    }

    public function selectSampleAction(Request $request)
    {
        $sample = new Sample();
        $pet = new Pet();
        $pet->setChipNbr( (string) $request->request->get('chipId', ''))
            ->setSpeciesCode( (string) $request->request->get('species', ''))
            ->setGender((string) $request->request->get('gender', ''))
            ->setName((string) $request->request->get('name', ''))
        ;
        //$pet->setChipNbr( 'test');
        $sample->setPet($pet);
//var_dump($sample);die();

        $this->requestManager->setSample2($sample);
        $this->requestManager->saveToSession();

        $speciesCode = $sample->getPet()->getSpeciesCode();

        $billing = null;
        $language = $this->getUser()->getLanguage();

        $this->parameterProfileManager->setLanguage($language)->setSpeciesCode($speciesCode)->setBilling($billing);
        $categories = $this->parameterProfileManager->getParameterProfileCategories();

        $this->breedManager->setLanguage($language)->setSpeciesCode($speciesCode);
        $breeds = $this->breedManager->getBreeds();

        $parametersInSample = $this->parameterLister->getParameters($sample);
        $parameterProfilesInSample = $this->parameterProfileLister->getParameterProfiles($parametersInSample);

        return $this->render('ZoolyxCoreBundle:Request:step/b2_profile_list.html.twig', array(
            'report' => $this->reportBuilder,
            'language' => $language,
            'sample' => $sample,
            'categories' => $categories,
            'breeds' => $breeds,
            'parametersInSample' => $parametersInSample,
            'parameterProfilesInSample' => $parameterProfilesInSample,
        ));
    }

    public function selectProfilesAction(Request $request)
    {
        if (is_null($request) && $request->getMethod() == 'POST') {
            //something is wrong
        }

        $profilesString = $request->request->get('profiles','');

        $this->requestManager->setProfiles($profilesString);
        $this->requestManager->saveToSession();

       $profileReferences = explode(",",$profilesString);

        if (count($profileReferences) == 0) {
            //no profile selected
        }


        /** @var Sample $sample */
        $sample = $this->requestManager->getRequestData()->getSample();
        $speciesCode = $sample->getPet()->getSpeciesCode();

        $billing = null;
        $language = $this->getUser()->getLanguage();

        $this->parameterProfileManager->setLanguage($language)->setSpeciesCode($speciesCode)->setBilling($billing);

        $parameterProfiles = $this->parameterProfileManager->getParameterProfiles($profileReferences);
        $totalValue = 0;
        $parameterProfileReferences = array();
        /** @var LimsParameterProfile $profile */
        foreach ($parameterProfiles as $profile) {
            $totalValue+= floatval($profile->getPrice()->getValue());
            $parameterProfileReferences[] = $profile->getReference();
        }

        return $this->render('ZoolyxCoreBundle:Request:step/c2_sample_type_list.html.twig', array(
            'report' => $this->reportBuilder,
            'sample' => $sample,
            'parameterProfiles' => $parameterProfiles,
            'parameterProfileReferencesString' => implode(",",$parameterProfileReferences),
            'total' => $totalValue,
        ));
    }

    public function selectSampleTypesAction(Request $request)
    {
        if (is_null($request) && $request->getMethod() == 'POST') {
            //something is wrong
        }
        $profiles = $request->request->get('profiles');
        $sampleTypes = array();
        $sampleTypesRequested = array();
        foreach(explode(",",$profiles) as $profile) {
            $sampleTypesArray = json_decode($request->request->get('sampleTypes_'.$profile));
            foreach ($sampleTypesArray as $sampleType) {
                if ($sampleType->name == 'sampleTypes[' . $profile . ']') {
                    $sampleTypeValue = $sampleType->value;
                    if (!isset($sampleTypes[$sampleTypeValue])) {
                        $sampleTypes[$sampleTypeValue] = 1;
                    }
                }
                if ($sampleType->name == 'sampleTypesRequested[' . $profile . ']') {
                    $sampleTypeValue = $sampleType->value;
                    if (isset($sampleTypesRequested[$sampleTypeValue])) {
                        $sampleTypesRequested[$sampleTypeValue]++;
                    } else {
                        $sampleTypesRequested[$sampleTypeValue] = 1;
                    }
                }
            }
        }

        $this->requestManager->setsampleTypes(array('type'=>$sampleTypes, 'requested'=>$sampleTypesRequested));
        $this->requestManager->saveToSession();

        // @todo : handle the sampletypes

        $profileReferences = explode(",",$this->requestManager->getRequestData()->getProfiles());

        if (count($profileReferences) == 0) {
            //no profile selected
        }

        /** @var Sample $sample */
        $sample = $this->requestManager->getRequestData()->getSample();
        $speciesCode = $sample->getPet()->getSpeciesCode();

        $billing = null;
        $language = $this->getUser()->getLanguage();

        $this->parameterProfileManager->setLanguage($language)->setSpeciesCode($speciesCode)->setBilling($billing);

        $parameterProfiles = $this->parameterProfileManager->getParameterProfiles($profileReferences);
        $totalValue = 0;
        $parameterProfileReferences = array();
        $postData = array();
        /** @var LimsParameterProfile $profile */
        foreach ($parameterProfiles as $profile) {
            $totalValue+= floatval($profile->getPrice()->getValue());
            $parameterProfileReferences[] = $profile->getReference();
            $postData[] = array(
                "id" => $profile->getReference(),
                //"price" => (int) intval(100*str_replace(",",".",$profile->getPrice()->getValue())),
                //"extra" => $profile->getDescription()->getDescription() . " pour ".$sample->getPet()->getName()." (".$profile->getReference().")"
            );
        }

        //check if the user has to pay
        $profilesToPay = array();
        /** @var User $user */
        $user = $this->getUser();
        if ($user->hasRole('ROLE_VETERINARY')) {
            /** @var LimsParameterProfile $profile */
            foreach ($parameterProfiles as $profile) {
                if ($profile->getEntity()->veterinaryMustPay()) {
                    $profilesToPay[] = $profile;
                }
            }
        }
        elseif ($user->hasRole('ROLE_OWNER')) {
            /** @var LimsParameterProfile $profile */
            foreach ($parameterProfiles as $profile) {
                if ($profile->getEntity()->ownerMustPay()) {
                    $profilesToPay[] = $profile;
                }
            }
        }
        $needToPay = count($profilesToPay) ? true : false;

        //var_dump($profilesToPay); die();
        if ($needToPay) {
            $this->requestManager->setProfilesToPay($profilesToPay);
            $this->requestManager->saveToSession();
        }

        return $this->render('ZoolyxCoreBundle:Request:step/d_confirm.html.twig', array(
            'needToPay' => $needToPay,
            'postData' => $postData,
            'report' => $this->reportBuilder,
            'sample' => $sample,
            'parameterProfiles' => $parameterProfiles,
            'parameterProfileReferencesString' => implode(",",$parameterProfileReferences),
            'total' => $totalValue,
        ));
    }

    public function sendAction()
    {
        /** @var Veterinary $veterinary */
        $veterinary = $this->getUser();

        $hash = $this->requestManager->getRequestData()->getReportHash();
        $sampleId = $this->requestManager->getRequestData()->getSampleId();
        $profileReferences = explode(",",$this->requestManager->getRequestData()->getProfiles());
        $sampleTypes = $this->requestManager->getRequestData()->getSampleTypes();

        //handle the sampletypes
        $sampleTypesJson = json_decode($sampleTypes);
        $sampleTypesList = array();
        foreach ($sampleTypesJson as $sampleTypeJson) {
            $name = $sampleTypeJson->name;
            $value = $sampleTypeJson->value;
            if (!isset($sampleTypesList[$name])) $sampleTypesList[$name] = array();
            $sampleTypesList[$name][] = $value;
        }

        //handle the parameterProfileForms
        $parameterProfileFormData = new ParameterProfileFormData();
        /*
        foreach ($request->request->all() as $formId => $formContent) {
            if ($formId != 'profiles' && $formId != 'sampleTypes') {
                //echo ("trying $formId<br>");
                $formContentJson = json_decode($formContent);
                $parameterProfileFormData->addFormData($formId,$formContentJson);
            }
        }
        */

        /** @var ReportVersion $reportVersion */
        $reportVersion = $this->reportLoader->loadByHash(new ReportHash($hash));
        $this->reportBuilder->setReportVersion($reportVersion, $sampleId);

        $additionalRequestServiceBuilder = new AdditionalRequestServiceBuilder();
        $additionalRequestServiceBuilder
            ->setVeterinary($veterinary)
            ->setOwner($reportVersion->getReport()->getOwner())
            ->setSample($this->reportBuilder->getSample($sampleId))
            ->setProfileReferences($profileReferences)
            ->setSampleTypesList($sampleTypesList)
            ->setParameterProfileForm($parameterProfileFormData);

        $service = $additionalRequestServiceBuilder->getDoc();

        $tempFile = fopen('php://memory', 'r+');
        fputs($tempFile, $service->saveXML());
        rewind($tempFile);
        $this->ftpService->put("request_".time().'.xml', $tempFile);

        //handle the uploaded files
        foreach ($_FILES as $key=>$file) {
            $tempFile = fopen('php://memory', 'r+');
            $content = file_get_contents($file['tmp_name']);
            $contentLength = strlen($content);
            fputs($tempFile, $content, $contentLength);
            rewind($tempFile);

            $this->ftpService->put($key."_".$file['name'], $tempFile);
        }


        return $this->redirectToRoute('zoolyx_report_additional_request_select_complete',
            array(
                "hash" => $hash,
                "sampleId" => $sampleId
            ));
    }

    public function completeAction()
    {
        $hash = $this->requestManager->getRequestData()->getReportHash();
        $sampleId = $this->requestManager->getRequestData()->getSampleId();

        /** @var ReportVersion $reportVersion */
        $reportVersion = $this->reportLoader->loadByHash(new ReportHash($hash));
        $this->reportBuilder->setReportVersion($reportVersion, $sampleId);

        return $this->render('ZoolyxCoreBundle:Request:step/e_complete.html.twig', array(
            'report' => $this->reportBuilder,
        ));
    }

}
