<?php

namespace Zoolyx\CoreBundle\Controller\Api;

use Doctrine\Common\Persistence\ObjectManager;
use DOMElement;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Zoolyx\CoreBundle\Entity\ParameterBase;
use Zoolyx\CoreBundle\Entity\ParameterBaseDescription;
use Zoolyx\CoreBundle\Entity\ParameterBaseDescription2;
use Zoolyx\CoreBundle\Entity\ParameterBaseMethod;
use Zoolyx\CoreBundle\Entity\ParameterBaseSampleType;
use Zoolyx\CoreBundle\Entity\ParameterGroupBase;
use Zoolyx\CoreBundle\Entity\ParameterGroupBaseDescription;
use Zoolyx\CoreBundle\Entity\ParameterProfileComment;
use Zoolyx\CoreBundle\Entity\ParameterProfileCommentDescription;
use Zoolyx\CoreBundle\Entity\ParameterProfileSampleTypeDefinition;
use Zoolyx\CoreBundle\Entity\ParameterProfileSampleTypeDescription;
use Zoolyx\CoreBundle\Entity\ReferenceRange;
use Zoolyx\CoreBundle\Entity\Repository\ParameterBaseDescription2Repository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterBaseDescriptionRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterBaseRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterBaseSampleTypeRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterGroupBaseDescriptionRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterGroupBaseRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileCommentRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileSampleTypeDefinitionRepository;
use Zoolyx\CoreBundle\Entity\Repository\ReferenceRangeRepository;
use Zoolyx\CoreBundle\Model\APIParser\ApiContentParser;
use Zoolyx\CoreBundle\Model\ApiParser\ApiContentTypeParser;
use Zoolyx\CoreBundle\Model\XmlParser\DomElementFilter;

class ParameterController extends Controller
{

    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager")
     */
    private $entityManager;

    /**
     * @var ParameterBaseRepository $parameterBaseRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_base")
     */
    private $parameterBaseRepository;

    /**
     * @var ParameterBaseDescriptionRepository $parameterBaseDescriptionRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_base_description")
     */
    private $parameterBaseDescriptionRepository;

    /**
     * @var ParameterBaseDescription2Repository $parameterBaseDescription2Repository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_base_description_2")
     */
    private $parameterBaseDescription2Repository;

    /**
     * @var ParameterBaseSampleTypeRepository $parameterBaseSampleTypeRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_base_sample_type")
     */
    private $parameterBaseSampleTypeRepository;

    /**
     * @var ReferenceRangeRepository $referenceRangeRepository
     *
     *  @DI\Inject("zoolyx_core.repository.reference_range")
     */
    private $referenceRangeRepository;

    /**
     * @var ParameterGroupBaseRepository $parameterGroupBaseRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_group_base")
     */
    private $parameterGroupBaseRepository;

    /**
     * @var ParameterGroupBaseDescriptionRepository $parameterGroupBaseDescriptionRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_group_base_description")
     */
    private $parameterGroupBaseDescriptionRepository;

    /**
     * @var ParameterProfileSampleTypeDefinitionRepository $parameterProfileSampleTypeDefinitionRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_profile_sample_type_definition")
     */
    private $parameterProfileSampleTypeDefinitionRepository;

    /**
     * @var ParameterProfileCommentRepository $parameterProfileCommentRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_profile_comment")
     */
    private $parameterProfileCommentRepository;



    /**
     * @var ApiContentParser $apiContentParser
     *
     *  @DI\Inject("zoolyx_core.api_parser.content")
     */
    private $apiContentParser;

    /**
     * @var DomElementFilter $domElementFilter
     *
     *  @DI\Inject("zoolyx_core.dom_element.filter")
     */
    private $domElementFilter;


    /**
     * @param Request $request
     *
     * @return View
     *
     * @throws NotFoundHttpException when parameters could not be parsed
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Lims",
     *  description="upload parameters",
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when parameters could not be parsed",
     *  })
     */
    public function indexAction(Request $request)
    {
        file_put_contents($this->container->getParameter('log_dir_lims') .'/'.date("YmdHis",time()).'-parameters.xml',$request->getContent());
        $this->apiContentParser->setRequest($request);
        /** @var ApiContentTypeParser $contentType */
        $contentType = $this->apiContentParser->getContentType();
        if (!$contentType->hasValidContentType()) {
            return View::create('{"code":400,"message":"' . $contentType->getContentTypeStatus() . '"}', 400);
        }

        if (!$this->apiContentParser->canParse()) {
            return View::create('{"code":400,"message":"could not parse"}', 400);
        }

        //validate the request
        $xsdFile = $this->container->getParameter('application_path') .'/web/bundles/zoolyxcore/xsd/import_pa.xsd';
        if (!$this->apiContentParser->isValid($xsdFile)) {
            return View::create(['errors'=>$this->apiContentParser->getErrors()], 400);
        }


        $doc = $this->apiContentParser->getDomDocument();

        $parameterProfileComments = array();
        /** @var DomElement $docComments */
        $docComments = $this->domElementFilter->findOne($doc->childNodes->item(0), 'comments');

        /** @var DomElement $docComment */
        foreach ($docComments->getElementsByTagName('comment') as $docComment) {
            $reference = $docComment->getAttribute('id');
            $parameterProfileComment = $this->parameterProfileCommentRepository->findOneBy(array('reference'=> $reference));

            if (is_null($parameterProfileComment)) {
                $parameterProfileComment = new ParameterProfileComment();
                $parameterProfileComment->setReference($reference);
            } else {
                foreach ($parameterProfileComment->getDescriptions() as $description) {
                    $this->entityManager->remove($description);
                }
                $this->entityManager->flush();
            }

            /** @var DomElement $childNode */
            foreach ($docComment->getElementsByTagName('descr') as $childNode) {
                $description = new ParameterProfileCommentDescription();
                $description->setLanguage($childNode->getAttribute('lang'))->setDescription($childNode->nodeValue);
                $description->setComment($parameterProfileComment);
                $parameterProfileComment->addDescription($description);

            }

            $parameterProfileComments[$reference] = $parameterProfileComment;
            $this->entityManager->persist($parameterProfileComment);
        }
        echo "parameterProfileComments: ".count($parameterProfileComments) . "\n";



        $sampleTypes = array();
        $docSampleTypes = $this->domElementFilter->findOne($doc->childNodes->item(0), 'sampleTypes');

        /** @var DomElement $docSampleType */
        foreach ($this->domElementFilter->findAll($docSampleTypes, 'st') as $docSampleType) {
            /** @var ParameterProfileSampleTypeDefinition $parameterProfileSampleTypeDefinition */
            $parameterProfileSampleTypeDefinition = $this->parameterProfileSampleTypeDefinitionRepository->findOneBy(array('reference'=>$docSampleType->getAttribute('id')));
            if (is_null($parameterProfileSampleTypeDefinition)) {
                $parameterProfileSampleTypeDefinition = new ParameterProfileSampleTypeDefinition();
                $parameterProfileSampleTypeDefinition->setReference($docSampleType->getAttribute('id'));
            } else {
                foreach ($parameterProfileSampleTypeDefinition->getDescriptions() as $description) {
                    $this->entityManager->remove($description);
                }
                $this->entityManager->flush();
            }
            $docImage = $this->domElementFilter->findOne($docSampleType,'img');
            $parameterProfileSampleTypeDefinition->setImageName($docImage->nodeValue);
            $docOwnerCanOrder = $this->domElementFilter->findOne($docSampleType,'ad_order');
            $parameterProfileSampleTypeDefinition->setOwnerCanOrder($docOwnerCanOrder->nodeValue);
            /** @var DomElement $childNode */
            foreach ($this->domElementFilter->findAll($docSampleType, 'descr') as $childNode) {
                $description = new ParameterProfileSampleTypeDescription();
                $description->setLanguage($childNode->getAttribute('lang'))->setDescription($childNode->nodeValue);
                $description->setSampleTypeDefinition($parameterProfileSampleTypeDefinition);
                $parameterProfileSampleTypeDefinition->addDescription($description);

            }
            $sampleTypes[$parameterProfileSampleTypeDefinition->getReference()] = $parameterProfileSampleTypeDefinition;
            $this->entityManager->persist($parameterProfileSampleTypeDefinition);

        }
        echo "sampletypes: ".count($sampleTypes) . "\n";



        /** @var DomElement $docParameters */
        $docParameters = $this->domElementFilter->findOne($doc->childNodes->item(0), 'parameters');

        /** @var DomElement $docParameter */
        foreach ($docParameters->getElementsByTagName('pa') as $docParameter) {
            $parameterId = $docParameter->getAttribute('id');
            $parameterBase = $this->parameterBaseRepository->findOneBy(array('parameterId' => $parameterId));
            if (!$parameterBase) {
                $parameterBase = new ParameterBase();
                $parameterBase->setParameterId($parameterId);
            }
            /** @var DomElement $childNode */
            foreach ($docParameter->childNodes as $childNode) {
                switch ($childNode->nodeName) {
                    case 'descr':
                        $language = $childNode->getAttribute('lang');
                        $description = null;
                        if ($parameterBase->getId()) {
                            $description = $this->parameterBaseDescriptionRepository->findOneBy(array('parameterBase'=>$parameterBase,'language'=>$language));
                        }
                        if (!$description) {
                            $description = new ParameterBaseDescription();
                            $description->setLanguage($language);
                            $description->setParameterBase($parameterBase);
                            $parameterBase->addDescription($description);
                        }
                        $description->setDescription($childNode->nodeValue);
                        break;
                    case 'descr2':
                        $language = $childNode->getAttribute('lang');
                        $description = null;
                        if ($parameterBase->getId()) {
                            $description = $this->parameterBaseDescription2Repository->findOneBy(array('parameterBase'=>$parameterBase,'language'=>$language));
                        }
                        if (!$description) {
                            $description = new ParameterBaseDescription2();
                            $description->setLanguage($language);
                            $description->setParameterBase($parameterBase);
                            $parameterBase->addDescription2($description);
                        }
                        $description->setDescription($childNode->nodeValue);
                        break;
                    case 'unit':
                        $parameterBase->setUnit($childNode->nodeValue);
                        break;
                    case 'altUnit':
                        $parameterBase->setAlternativeUnit($childNode->nodeValue);
                        $parameterBase->setConversionFactor($childNode->getAttribute('convFactor'));
                        $parameterBase->setDecimals($childNode->getAttribute('dec'));
                        break;
                    case 'delay':
                        $parameterBase->setDelay($childNode->nodeValue);
                        break;
                    case 'refRanges':
                        /** @var DomElement $docSampleType */
                        foreach($childNode->getElementsByTagName('refRange') as $docReferenceRange) {
                            $speciesCode = null;
                            $toAge = null;
                            $low = null;
                            $high = null;
                            $veryLow = null;
                            $veryHigh = null;
                            $refDisplay = null;
                            $comment = null;
                            $imageName = null;
                            foreach ($docReferenceRange->childNodes as $childNode) {
                                switch ($childNode->nodeName) {
                                    case 'speciesCode':
                                        $speciesCode = $childNode->nodeValue;
                                        break;
                                    case 'toAge':
                                        $toAge = $childNode->nodeValue;
                                        break;
                                    case 'refLow':
                                        $low = $childNode->nodeValue;
                                        break;
                                    case 'refHigh':
                                        $high = $childNode->nodeValue;
                                        break;
                                    case 'refVeryLow':
                                        $veryLow = $childNode->nodeValue;
                                        break;
                                    case 'refVeryHigh':
                                        $veryHigh = $childNode->nodeValue;
                                        break;
                                    case 'refDisplay':
                                        $refDisplay = $childNode->nodeValue;
                                        break;
                                    case 'comment':
                                        $comment = $childNode->nodeValue;
                                        break;
                                    case 'fileName':
                                        $imageName = $childNode->nodeValue;
                                        break;
                                }
                            }
                            $referenceRange = null;
                            if ($parameterBase->getId()) {
                                $referenceRange = $this->referenceRangeRepository->findOneBy(array('parameterBase'=>$parameterBase,'speciesCode'=>$speciesCode,'toAge'=>$toAge));
                            }
                            if (!$referenceRange) {
                                $referenceRange = new ReferenceRange();
                                $referenceRange->setParameterBase($parameterBase);
                                $referenceRange->setSpeciesCode($speciesCode);
                                $referenceRange->setToAge($toAge);
                                $parameterBase->addReferenceRange($referenceRange);
                            }

                            $referenceRange->setLow($low);
                            $referenceRange->setHigh($high);
                            $referenceRange->setVeryLow($veryLow);
                            $referenceRange->setVeryHigh($veryHigh);
                            $referenceRange->setReferenceDisplay($refDisplay);
                            $referenceRange->setComment($comment);
                            $referenceRange->setImageName($imageName);
                            $this->entityManager->persist($referenceRange);
                        }
                        break;
                    case 'species':
                        $speciesCodes = array();
                        foreach($childNode->getElementsByTagName('speciesCode') as $speciesCode) {
                            $speciesCodes[] = $speciesCode->nodeValue;
                        }
                        $parameterBase->setSpeciesCodes(implode(',',$speciesCodes));
                        break;
                    case 'sampleTypes':
                        /**
                         *  <sampleTypes>
                                <st id="EDTA">
                                    <minVolume></minVolume>
                                    <pref>1</pref>
                                    <comment></comment>
                                </st>
                                <st id="SWABG">
                                    <minVolume></minVolume>
                                    <pref>1</pref>
                                    <comment></comment>
                                </st>
                            </sampleTypes>
                         */
                        /** @var DomElement $sampleType */
                        foreach($childNode->getElementsByTagName('st') as $sampleType) {
                            $reference = $sampleType->getAttribute('id');
                            $minVolume = '';
                            $pref = '';
                            $comment = '';
                            foreach ($sampleType->childNodes as $sampleTypeChildNode) {
                                switch ($sampleTypeChildNode->nodeName) {
                                    case 'minVolume':
                                        $minVolume = $sampleTypeChildNode->nodeValue;
                                        break;
                                    case 'pref':
                                        $pref = $sampleTypeChildNode->nodeValue;
                                        break;
                                    case 'comment':
                                        $comment = $sampleTypeChildNode->nodeValue;
                                        break;
                                }
                            }
                            $parameterBaseSampleType = $this->parameterBaseSampleTypeRepository->findOneBy(array(
                                'parameter'=> $parameterBase,
                                'reference' => $reference
                            ));
                            if (is_null($parameterBaseSampleType)) {
                                $parameterBaseSampleType = new ParameterBaseSampleType();
                                $parameterBaseSampleType->setParameter($parameterBase);
                                $parameterBaseSampleType->setReference($reference);

                            }
                            $parameterBaseSampleType->setMinVolume($minVolume);
                            $parameterBaseSampleType->setPref($pref);
                            if (isset($parameterProfileComments[$comment])) {
                                $parameterBaseSampleType->setComment($parameterProfileComments[$comment]);
                            }
                            if (isset($sampleTypes[$reference])) {
                                $parameterBaseSampleType->setSampleType($sampleTypes[$reference]);
                            }

                            $this->entityManager->persist($parameterBaseSampleType);
                        }

                        break;
                    case 'isTitle':
                        $parameterBase->setIsTitle($childNode->nodeValue);
                        break;
                    case 'cumulative':
                        $parameterBase->setIsCumulative($childNode->nodeValue);
                        break;
                    case 'loinc':
                        $parameterBase->setLoinc($childNode->nodeValue);
                        break;
                    case 'link':
                        $parameterBase->setLink($childNode->nodeValue);
                        break;
                    case 'mt':
                        /**
                         *  <mt>
                                <descr lang="nl"></descr>
                                <descr lang="fr"></descr>
                            </mt>
                         */
                        foreach ($parameterBase->getMethods() as $method) {
                            $parameterBase->removeMethod($method);
                            $this->entityManager->remove($method);
                        }

                        foreach ($childNode->getElementsByTagName('descr') as $docMethod) {
                            $language = $docMethod->getAttribute('lang');
                            $description = $docMethod->nodeValue;
                            if ($description != '') {
                                $parameterBaseMethod = new ParameterBaseMethod();
                                $parameterBaseMethod->setParameterBase($parameterBase);
                                $parameterBaseMethod->setLanguage($language);
                                $parameterBaseMethod->setDescription($description);
                                $this->entityManager->persist($parameterBaseMethod);
                            }
                        }
                        break;
                    case 'pg':
                        $parameterGroup = $childNode->nodeValue;
                        $parameterBase->setParameterGroup($parameterGroup);
                        break;
                    case 'seq':
                        $sequence = intval($childNode->nodeValue);
                        $parameterBase->setSequence($sequence);
                        break;
                    case 'wikilab':
                        $wikilab = intval($childNode->nodeValue) == 1;
                        $parameterBase->setWikilab($wikilab);
                        break;
                }
            }
            $this->entityManager->persist($parameterBase);
        }
        $this->entityManager->flush();



        /** @var DomElement $docParameterGroups */
        $docParameterGroups = $doc->getElementsByTagName('parametergroups')->item(0);
        /** @var DomElement $docParameterGroup */
        foreach ($docParameterGroups->getElementsByTagName('pg') as $docParameterGroup) {
            $parameterGroupId = $docParameterGroup->getAttribute('id');
            $parameterGroupBase = $this->parameterGroupBaseRepository->findOneBy(array('parameterGroupId' => $parameterGroupId));
            if (!$parameterGroupBase) {
                $parameterGroupBase = new ParameterGroupBase();
                $parameterGroupBase->setParameterGroupId($parameterGroupId);
            }
            /** @var DomElement $childNode */
            foreach ($docParameterGroup->childNodes as $childNode) {
                if ($childNode->nodeName == "descr") {
                    $language = $childNode->getAttribute('lang');
                    $description = null;
                    if ($parameterGroupBase->getId()) {
                        $description = $this->parameterGroupBaseDescriptionRepository->findOneBy(array('parameterGroupBase'=>$parameterGroupBase,'language'=>$language));
                    }
                    if (!$description) {
                        $description = new ParameterGroupBaseDescription();
                        $description->setLanguage($language);
                        $description->setParameterGroupBase($parameterGroupBase);
                        $parameterGroupBase->addDescription($description);
                    }
                    $description->setDescription($childNode->nodeValue);
                }
            }
            $this->entityManager->persist($parameterGroupBase);
        }
        $this->entityManager->flush();

        return new Response();
    }

}
