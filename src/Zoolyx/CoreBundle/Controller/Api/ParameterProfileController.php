<?php

namespace Zoolyx\CoreBundle\Controller\Api;

use Doctrine\Common\Persistence\ObjectManager;
use DOMElement;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Zoolyx\CoreBundle\Entity\Breed;
use Zoolyx\CoreBundle\Entity\ParameterBase;
use Zoolyx\CoreBundle\Entity\ParameterProfile;
use Zoolyx\CoreBundle\Entity\ParameterProfileCategory;
use Zoolyx\CoreBundle\Entity\ParameterProfileCategoryDescription;
use Zoolyx\CoreBundle\Entity\ParameterProfileComment;
use Zoolyx\CoreBundle\Entity\ParameterProfileCommentDescription;
use Zoolyx\CoreBundle\Entity\ParameterProfileDescription;
use Zoolyx\CoreBundle\Entity\ParameterProfileForm;
use Zoolyx\CoreBundle\Entity\ParameterProfilePrice;
use Zoolyx\CoreBundle\Entity\ParameterProfileSampleType;
use Zoolyx\CoreBundle\Entity\ParameterProfileSampleTypeDefinition;
use Zoolyx\CoreBundle\Entity\Repository\BreedRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterBaseRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileCategoryRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileFormRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileSampleTypeDefinitionRepository;
use Zoolyx\CoreBundle\Model\APIParser\ApiContentParser;
use Zoolyx\CoreBundle\Model\ApiParser\ApiContentTypeParser;

class ParameterProfileController extends Controller
{

    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager")
     */
    private $entityManager;

    /**
     * @var ParameterProfileRepository $parameterProfileRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_profile")
     */
    private $parameterProfileRepository;

    /**
     * @var ParameterProfileFormRepository $parameterProfileFormRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_profile_form")
     */
    private $parameterProfileFormRepository;

    /**
     * @var ParameterProfileCategoryRepository $parameterProfileCategoryRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_profile_category")
     */
    private $parameterProfileCategoryRepository;

    /**
     * @var ParameterProfileSampleTypeDefinitionRepository $parameterProfileSampleTypeDefinitionRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_profile_sample_type_definition")
     */
    private $parameterProfileSampleTypeDefinitionRepository;

    /**
     * @var ParameterBaseRepository $parameterBaseRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_base")
     */
    private $parameterBaseRepository;

    /**
     * @var BreedRepository $breedRepository
     *
     *  @DI\Inject("zoolyx_core.repository.breed")
     */
    private $breedRepository;

    /**
     * @var ApiContentParser $apiContentParser
     *
     *  @DI\Inject("zoolyx_core.api_parser.content")
     */
    private $apiContentParser;

    /**
     * @param Request $request
     *
     * @return View
     *
     * @throws NotFoundHttpException if techne agora cannot be found
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Lims",
     *  description="upload parameter profiles",
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when parameter profiles could not be parsed",
     *  })
     */
    public function indexAction(Request $request)
    {
        file_put_contents($this->container->getParameter('log_dir_lims') .'/'.date("YmdHis",time()).'-param-profile.xml',$request->getContent());
        $this->apiContentParser->setRequest($request);
        /** @var ApiContentTypeParser $contentType */
        $contentType = $this->apiContentParser->getContentType();
        if (!$contentType->hasValidContentType()) {
            return View::create('{"code":400,"message":"' . $contentType->getContentTypeStatus() . '"}', 400);
        }

        if (!$this->apiContentParser->canParse()) {
            return View::create('{"code":400,"message":"could not parse"}', 400);
        }

        // validate the request
        $xsdFile = $this->container->getParameter('application_path') .'/web/bundles/zoolyxcore/xsd/import_pp.xsd';
        if (!$this->apiContentParser->isValid($xsdFile)) {
            return View::create(['errors'=>$this->apiContentParser->getErrors()], 400);
        }

        $doc = $this->apiContentParser->getDomDocument();


        foreach ($this->parameterProfileRepository->findAll() as $pp) {
            $this->entityManager->remove($pp);
        }
        foreach ($this->parameterProfileCategoryRepository->findAll() as $ppc) {
            $this->entityManager->remove($ppc);
        }

        $this->entityManager->flush();




        /** @var DomElement $docParameterProfileCategories */
        $docParameterProfileCategories = $doc->getElementsByTagName('ppCategories')->item(0);

        $parameterProfileCategories = array();
        /** @var DomElement $docParameterProfileCategory */
        foreach ($docParameterProfileCategories->getElementsByTagName('ppCat') as $docParameterProfileCategory) {
            $parameterProfileCategory = new ParameterProfileCategory();
            $parameterProfileCategory->setReference($docParameterProfileCategory->getAttribute('id'));
            /** @var DomElement $childNode */
            foreach ($docParameterProfileCategory->getElementsByTagName('descr') as $childNode) {
                $description = new ParameterProfileCategoryDescription();
                $description->setLanguage($childNode->getAttribute('lang'))->setDescription($childNode->nodeValue);
                $description->setCategory($parameterProfileCategory);
                $parameterProfileCategory->addDescription($description);

            }
            $parameterProfileCategories[] = $parameterProfileCategory;
            $this->entityManager->persist($parameterProfileCategory);
        }
        $this->entityManager->flush();



        $parameterProfileSampleTypeDefinitions = $this->parameterProfileSampleTypeDefinitionRepository->findAll();


        $parameterProfileComments = array();
        /** @var DomElement $docComments */
        $docComments = $doc->getElementsByTagName('comments')->item(0);
        /** @var DomElement $docComment */
        foreach ($docComments->getElementsByTagName('comment') as $docComment) {
            $parameterProfileComment = new ParameterProfileComment();
            $parameterProfileComment->setReference($docComment->getAttribute('id'));
            /** @var DomElement $childNode */
            foreach ($docComment->getElementsByTagName('descr') as $childNode) {
                $description = new ParameterProfileCommentDescription();
                $description->setLanguage($childNode->getAttribute('lang'))->setDescription($childNode->nodeValue);
                $description->setComment($parameterProfileComment);
                $parameterProfileComment->addDescription($description);

            }
            $parameterProfileComments[$parameterProfileComment->getReference()] = $parameterProfileComment;
            $this->entityManager->persist($parameterProfileComment);

        }

        $parameterProfileForms = array();

        /** @var DomElement $docForms */
        $docForms = $doc->getElementsByTagName('forms')->item(0);
        /** @var DomElement $docForm */
        foreach ($docForms->getElementsByTagName('form') as $docForm) {
            $reference = $docForm->getAttribute('id');
            $parameterProfileForm = $this->parameterProfileFormRepository->findOneBy(array('reference'=>$reference));
            if (is_null($parameterProfileForm)) {
                $parameterProfileForm = new ParameterProfileForm();
                $parameterProfileForm->setReference($reference);
            }

            $children = $docForm->getElementsByTagName('html');

            /** @var DomElement $docHtml */
            $docHtml = $children->item(0);
            $parameterProfileForm->setContent($docHtml->textContent);

            $parameterProfileForms[$parameterProfileForm->getReference()] = $parameterProfileForm;
            $this->entityManager->persist($parameterProfileForm);

        }

        /** @var DomElement $docParameterProfiles */
        $docParameterProfiles = $doc->getElementsByTagName('parameterProfiles')->item(0);

        /** @var DomElement $docParameterProfile */
        foreach ($docParameterProfiles->getElementsByTagName('pp') as $docParameterProfile) {
            $parameterProfile = new ParameterProfile();
            $parameterProfile->setReference($docParameterProfile->getAttribute('id'));
            /** @var DomElement $childNode */
            foreach ($docParameterProfile->childNodes as $childNode) {
                switch ($childNode->nodeName) {
                    case 'ppCat' :
                        $ppCatId = $childNode->nodeValue;
                        /** @var ParameterProfileCategory $parameterProfileCategory */
                        foreach ($parameterProfileCategories as $parameterProfileCategory) {
                            if ($parameterProfileCategory->getReference() == $ppCatId)
                                $parameterProfile->setCategory($parameterProfileCategory);
                        }
                        break;
                    case 'species' :
                        $speciesId = $childNode->getAttribute('id');
                        $parameterProfile->setSpecies($speciesId);
                        /** @var DomElement $docBreed */
                        foreach($childNode->getElementsByTagName('breed') as $docBreed) {
                            $breedId = $docBreed->getAttribute('id');
                            /** @var Breed $breed */
                            $breed = $this->breedRepository->findOneBy(array("breedId"=>$speciesId."_".$breedId));

                            if ($breed) {
                                $parameterProfile->addBreed($breed);
                            } else {
                                //we should throw error because we cannot find the breed
                                echo "could not find breed $breedId for species $speciesId\n";
                            }
                        }
                        break;
                    case 'descr':
                        $description = new ParameterProfileDescription();
                        $description->setLanguage($childNode->getAttribute('lang'))->setDescription($childNode->nodeValue);
                        $description->setParameterProfile($parameterProfile);
                        $parameterProfile->addDescription($description);
                        break;
                    case 'url':
                        $parameterProfile->setUrl($childNode->nodeValue);
                        break;
                    case 'form':
                        $formReference = $childNode->nodeValue;
                        if (!is_null($formReference) && $formReference !='') {
                            /** @var ParameterProfileForm $form */
                            $form = $parameterProfileForms[$formReference];
                            $parameterProfile->setForm($form);
                        }
                        break;
                    case 'sampleTypes':
                        /** @var DomElement $docSampleType */
                        foreach($childNode->getElementsByTagName('st') as $docSampleType) {
                            $sampleType = new ParameterProfileSampleType();
                            $reference = $docSampleType->getAttribute('id');
                            /** @var ParameterProfileSampleTypeDefinition $parameterProfileSampleTypeDefinition */
                            foreach ($parameterProfileSampleTypeDefinitions as $parameterProfileSampleTypeDefinition) {
                                if ($parameterProfileSampleTypeDefinition->getReference() == $reference)
                                    $sampleType->setDefinition($parameterProfileSampleTypeDefinition);
                            }

                            foreach ($docSampleType->childNodes as $docSampleTypeChild) {
                                switch($docSampleTypeChild->nodeName) {
                                    case 'minVolume' :
                                        $sampleType->setMinimumVolume($docSampleTypeChild->nodeValue);
                                        break;
                                    case 'pref' :
                                        $sampleType->setPreferred(intval($docSampleTypeChild->nodeValue));
                                        break;
                                    case 'comment' :
                                        if (isset($parameterProfileComments[$docSampleTypeChild->nodeValue])) {
                                            $sampleType->setComment($parameterProfileComments[$docSampleTypeChild->nodeValue]);
                                        }
                                        break;
                                }
                            }
                            $sampleType->setParameterProfile($parameterProfile);
                            $parameterProfile->addSampleType($sampleType);
                        }
                        break;
                    case 'priceList':
                        /** @var DomElement $docPrice */
                        foreach($childNode->getElementsByTagName('price') as $docPrice) {
                            $price = new ParameterProfilePrice();
                            if ($docPrice->hasAttribute('fa')) {
                                $faId = $docPrice->getAttribute('fa');
                                $price->setBilling($faId);
                            }
                            $price->setValue($docPrice->nodeValue);

                            $parameterProfile->addPrice($price);
                            $price->setParameterProfile($parameterProfile);
                        }
                        break;
                    case 'paList':
                        /** @var DomElement $docParameter */
                        foreach($childNode->getElementsByTagName('pa') as $docParameter) {
                            $parameterId = $docParameter->getAttribute('id');
                            /** @var ParameterBase $parameterBase */
                            $parameterBase = $this->parameterBaseRepository->findOneBy(array('parameterId'=>$parameterId));
                            $parameterProfile->addParameter($parameterBase);
                        }
                        break;
                    case 'daList':
                        /** @var DomElement $docVeterinaryPractice */
                        $limsIds = array();
                        foreach($childNode->getElementsByTagName('da') as $docVeterinaryPractice) {
                            $limsId = $docVeterinaryPractice->getAttribute('id');
                            $limsIds[] = $limsId;
                        }
                        if (count($limsIds))
                            $parameterProfile->setReservedFor(implode(',',$limsIds));
                        break;
                    case 'da_order':
                        $parameterProfile->setVeterinaryCanOrder($childNode->nodeValue);
                        break;
                    case 'ad_order':
                        $parameterProfile->setOwnerCanOrder($childNode->nodeValue);
                        break;
                    case 'da_pay':
                        $parameterProfile->setVeterinaryMustPay($childNode->nodeValue);
                        break;
                    case 'ad_pay':
                        $parameterProfile->setOwnerMustPay($childNode->nodeValue);
                        break;
                }
            }
            $this->entityManager->persist($parameterProfile);
        }
        $this->entityManager->flush();


        return new Response();
    }

}
