<?php

namespace Zoolyx\CoreBundle\Controller\Api\Core;

use Doctrine\Common\Persistence\ObjectManager;
use DOMDocument;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Zoolyx\CoreBundle\Entity\Owner;
use Zoolyx\CoreBundle\Entity\Parameter;
use Zoolyx\CoreBundle\Entity\ParameterGroup;
use Zoolyx\CoreBundle\Entity\ParameterReference;
use Zoolyx\CoreBundle\Entity\Pet;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\ReportVersion;
use Zoolyx\CoreBundle\Entity\Repository\ReportRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\Sample;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPracticeReport;
use Zoolyx\CoreBundle\Model\ApiParser\ApiContentParser;
use Zoolyx\CoreBundle\Model\Date\DateFormatter;
use Zoolyx\CoreBundle\Model\File\File as MemoryFile;
use Zoolyx\CoreBundle\Model\File\FileConverter;
use Zoolyx\CoreBundle\Model\File\FileManager;
use Zoolyx\CoreBundle\Model\Ftp\FtpService;
use Zoolyx\CoreBundle\Model\Report\Parameter\Reference\ReferenceCommentFetcher;
use Zoolyx\CoreBundle\Model\Report\ReportHashProvider;

/**
 * @Security("is_granted('ROLE_ORGANISATION')")
 */
class RequestController extends Controller
{
    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var SecurityContextInterface
     *
     * @DI\Inject("security.context")
     */
    private $securityContext;

    /**
     * @var ApiContentParser $apiContentParser
     *
     *  @DI\Inject("zoolyx_core.api_parser.content"),
     */
    private $apiContentParser;

    /**
     * @var FtpService $ftpService
     *
     *  @DI\Inject("zoolyx_core.ftp.service"),
     */
    private $ftpService;

    /**
     * @var ReportHashProvider $reportVHashProvider
     *
     * @DI\Inject("zoolyx_core.report.hash_provider")
     */
    private $reportHashProvider;


    /**
     * @var VeterinaryRepository $veterinaryRepository
     *
     * @DI\Inject("zoolyx_core.repository.veterinary")
     */
    private $veterinaryRepository;

    /**
     * @var ReportRepository $reportRepository
     *
     * @DI\Inject("zoolyx_core.repository.report")
     */
    private $reportRepository;

    /**
     * @var FileManager $fileManager
     *
     * @DI\Inject("zoolyx_core.file.manager")
     */
    private $fileManager;

    /**
     * @var FileConverter $fileConverter
     *
     * @DI\Inject("zoolyx_core.file.converter")
     */
    private $fileConverter;

    /**
     * @var ReferenceCommentFetcher $referenceCommentFetcher
     *
     * @DI\Inject("zoolyx_core.report.parameter_reference_comment_fetcher")
     */
    private $referenceCommentFetcher;

    /**
     * @var DateFormatter $dateFormatter
     *
     * @DI\Inject("zoolyx_core.format.date")
     */
    private $dateFormatter;

    /**
     * @param Request $request
     *
     * @return View
     *
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx",
     *  description="send a request",
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when the request could not be parsed",
     *      401="Returned when token is missing or invalid",
     *  })
     */
    public function indexAction(Request $request)
    {
        file_put_contents($this->container->getParameter('log_dir_organisation') .'/'.date("YmdHis",time()).'-'.$this->reportHashProvider->get().'-request.xml',$request->getContent());

        /** @var User $veterinaryAccount */
        $veterinaryAccount = $this->getUser();
        /** @var Veterinary $veterinary */
        $veterinary = $this->veterinaryRepository->findOneBy(array('account'=>$veterinaryAccount));

        $this->apiContentParser->setRequest($request);

        $xsdFile = $this->container->getParameter('application_path') .'/web/bundles/zoolyxcore/xsd/request.xsd';
        if (!$this->apiContentParser->isValid($xsdFile)) {
            return View::create(['errors'=>$this->apiContentParser->getErrors()], 400);
        }

        $doc = $this->apiContentParser->getDomDocument();
        $files = $this->apiContentParser->getFiles();

        $x = $doc->documentElement;

        //define the request id
        $practiceReference = "";
        foreach ($x->childNodes AS $item) {
            if ($item->nodeName == 'practiceRef') {
                $practiceReference = $item->nodeValue;
            }
        }
        $requestId = $veterinaryAccount->getUsername() . "+" . $practiceReference;
        $x->setAttribute('id',$requestId);

        /**
         * @var int $index
         * @var MemoryFile $file
         */
        foreach ($files as $index=>$file) {
            //rename the files to prevent problems with special characters
            $this->fileManager->renameTo($file, $requestId . "_" . ($index+1));
        }
        //rename the files in the xml
        foreach ($x->childNodes as $item) {
            if ($item->nodeName == 'sc') {
                foreach ($item->childNodes as $sampleItem) {
                    if ($sampleItem->nodeName == 'pp') {
                        foreach ($sampleItem->childNodes as $ppItem) {
                            if ($ppItem->nodeName == 'pa') {
                                foreach ($ppItem->childNodes as $paItem) {
                                    if ($paItem->nodeName == 'file') {
                                        $filename = $paItem->getAttribute('name');
                                        $paItem->setAttribute('orig_name', $filename);
                                        /** @var MemoryFile $file */
                                        foreach ($files as $file) {
                                            if ($file->getOriginalName() == $filename) {
                                                $paItem->setAttribute('name', $file->getName());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        $service = new DOMDocument("1.0","ISO-8859-1");
        $serviceContainer = $service->createElement('service');
        $serviceContainer->appendChild($service->importNode($x, true));
        $serviceContainer->setAttribute('type','create_request');

        $service->appendChild($serviceContainer);

        $tempFile = fopen('php://memory', 'r+');
        fputs($tempFile, $this->encodeToIso($service->saveXML()));
        rewind($tempFile);
        $this->ftpService->put($requestId . '.xml', $tempFile);

        /** @var MemoryFile $file */
        foreach ($files as $file) {
            $tempFile = $this->fileConverter->convert($file);
            $this->ftpService->put($file->getName(), $tempFile);
        }

        $report = $this->reportRepository->findOneBy(array('requestId'=>$requestId));
        if (is_null($report)) {
            $report = new Report();
            $report->setRequestId($requestId);
            $report->setHash($this->reportHashProvider->get());
            $report->setAlternativeHash($this->reportHashProvider->get());
            $report->setPracticeReference($practiceReference);
            $report->setCreatedOn(new \DateTime());

            $veterinaryPractice = $veterinary->getDefaultVeterinaryPractice();
            $veterinaryPracticeReport = new VeterinaryPracticeReport();
            $veterinaryPractice->addVeterinaryPracticeReport($veterinaryPracticeReport);
            $veterinaryPracticeReport->setVeterinaryPractice($veterinaryPractice);
            $veterinaryPracticeReport->setReport($report);
            $report->addVeterinaryPracticeReportAsOrganisation($veterinaryPracticeReport);

            $this->entityManager->persist($report);
            $this->entityManager->persist($veterinary);
            $this->entityManager->persist($veterinaryPractice);
            $this->entityManager->persist($veterinaryPracticeReport);
            $this->entityManager->flush();
        }


        $doc = new DOMDocument("1.0","UTF-8");
        $urlDoc = $doc->createElement('url');
        $urlDoc->nodeValue = 'OK';
        $doc->appendChild($urlDoc);
        return new Response($doc->saveXML());
        //return View::create([], 200);
    }

    /**
     * @param string $hash
     *
     * @return View
     *
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx",
     *  description="retrieve a report",
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when the report cannot be found",
     *      401="Returned when token is missing or invalid",
     *  })
     */
    public function reportAction($hash) {
        /** @var Report $report */
        $report = $this->reportRepository->findOneBy(array('hash' => $hash));

        if (is_null($report)) {
            $doc = new DOMDocument("1.0","UTF-8");
            $requestDoc = $doc->createElement('rq');

            $requestDoc->nodeValue = "report with hash ". $hash . ' not found';
            $doc->appendChild($requestDoc);
            return new Response($doc->saveXML());
        }

        return $this->buildXml($report);
    }

    /**
     * @param string $reference
     *
     * @return View
     *
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx",
     *  description="retrieve a report",
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when the report cannot be found",
     *      401="Returned when token is missing or invalid",
     *  })
     */
    public function reportViaReferenceAction($reference) {
        /** @var Report $report */
        $report = $this->reportRepository->findOneBy(array('practiceReference' => $reference));

        if (is_null($report)) {
            $doc = new DOMDocument("1.0","UTF-8");
            $requestDoc = $doc->createElement('rq');

            $requestDoc->nodeValue = "report with reference ". $reference . ' not found';
            $doc->appendChild($requestDoc);
            return new Response($doc->saveXML());
        }

        return $this->buildXml($report);

    }

    /**
     * @param Report $report
     * @return Response
     */
    private function buildXml($report) {
        /** @var User $user */
        $user = $this->getUser();
        $language = $user->getLanguage();
        $language = $language = '' ? 'nl' : $language;

        $doc = new DOMDocument("1.0","UTF-8");
        $requestDoc = $doc->createElement('rq');

        $requestDoc->setAttribute('id',$report->getRequestId());

        $practiceRefDoc = $doc->createElement('practiceRef');
        $practiceRefDoc->nodeValue = $report->getPracticeReference();
        $requestDoc->appendChild($practiceRefDoc);

        $nameDoc = $doc->createElement('language');
        $nameDoc->nodeValue = $language;
        $requestDoc->appendChild($nameDoc);

        $veterinaryDoc = $doc->createElement('da');
        $veterinaryDoc->setAttribute('id',$report->getVeterinaryPractice()->getLimsId());

        /** @var Veterinary $veterinary */
        $veterinary = $report->getVeterinaryPractice()->getVeterinary();

        $firstNameDoc = $doc->createElement('firstName');
        $firstNameDoc->nodeValue = $veterinary->getFirstName();
        $veterinaryDoc->appendChild($firstNameDoc);
        $lastNameDoc = $doc->createElement('lastName');
        $lastNameDoc->nodeValue = $veterinary->getLastName();
        $veterinaryDoc->appendChild($lastNameDoc);
        $nameDoc = $doc->createElement('language');
        $nameDoc->nodeValue = $veterinary->getLanguage();
        $veterinaryDoc->appendChild($nameDoc);
        $nameDoc = $doc->createElement('email');
        $nameDoc->nodeValue = $veterinary->getAccount() ? $veterinary->getAccount()->getEmail() : "";
        $veterinaryDoc->appendChild($nameDoc);

        $practice = $report->getVeterinaryPractice()->getPractice();

        $nameDoc = $doc->createElement('street');
        $nameDoc->nodeValue = $practice->getStreet();
        $veterinaryDoc->appendChild($nameDoc);
        $nameDoc = $doc->createElement('zipCode');
        $nameDoc->nodeValue = $practice->getZipCode();
        $veterinaryDoc->appendChild($nameDoc);
        $nameDoc = $doc->createElement('city');
        $nameDoc->nodeValue = $practice->getCity();
        $veterinaryDoc->appendChild($nameDoc);
        $nameDoc = $doc->createElement('country');
        $nameDoc->nodeValue = $practice->getCountry();
        $veterinaryDoc->appendChild($nameDoc);

        $requestDoc->appendChild($veterinaryDoc);


        $ownerDoc = $doc->createElement('ad');

        /** @var Owner $owner */
        $owner = $report->getOwner();
        $ownerDoc->setAttribute('id',$owner->getLimsId());

        $firstNameDoc = $doc->createElement('firstName');
        $firstNameDoc->nodeValue = $owner->getFirstName();
        $ownerDoc->appendChild($firstNameDoc);
        $lastNameDoc = $doc->createElement('lastName');
        $lastNameDoc->nodeValue = $owner->getLastName();
        $ownerDoc->appendChild($lastNameDoc);
        $nameDoc = $doc->createElement('language');
        $nameDoc->nodeValue = $owner->getLanguage();
        $ownerDoc->appendChild($nameDoc);
        $nameDoc = $doc->createElement('email');
        $nameDoc->nodeValue = $owner->getAccount() ? $owner->getAccount()->getEmail() : "";
        $ownerDoc->appendChild($nameDoc);
        $nameDoc = $doc->createElement('street');
        $nameDoc->nodeValue = $owner->getStreet();
        $ownerDoc->appendChild($nameDoc);
        $nameDoc = $doc->createElement('zipCode');
        $nameDoc->nodeValue = $owner->getZipCode();
        $ownerDoc->appendChild($nameDoc);
        $nameDoc = $doc->createElement('city');
        $nameDoc->nodeValue = $owner->getCity();
        $ownerDoc->appendChild($nameDoc);
        $nameDoc = $doc->createElement('country');
        $nameDoc->nodeValue = $owner->getCountry();
        $ownerDoc->appendChild($nameDoc);

        $requestDoc->appendChild($ownerDoc);

        $faDoc = $doc->createElement('fa');
        $faDoc->setAttribute('id',$report->getBilling());
        $requestDoc->appendChild($faDoc);



        /** @var ReportVersion $reportVersion */
        $reportVersion = $report->getDefaultVersion();
        /** @var Sample $sample */
        foreach ($reportVersion->getSamples() as $sample) {
            $sampleDoc = $doc->createElement('sc');
            $sampleDoc->setAttribute('id',$sample->getCode());

            $dateReceivedDoc = $doc->createElement('date_received');
            $dateReceivedDoc->nodeValue = $this->dateFormatter->format($sample->getDateReceived(),'Y-m-d H:i:s');
            $sampleDoc->appendChild($dateReceivedDoc);

            $dateCompletedDoc = $doc->createElement('date_completed');
            $dateCompletedDoc->nodeValue =$this->dateFormatter->format($sample->getDateCompleted(),'Y-m-d H:i:s');
            $sampleDoc->appendChild($dateCompletedDoc);

            $completedDoc = $doc->createElement('completed');
            $completedDoc->nodeValue =$sample->getCompleted();
            $sampleDoc->appendChild($completedDoc);

            $externalIdDoc = $doc->createElement('extId');
            $externalIdDoc->nodeValue =$sample->getExternalId();
            $sampleDoc->appendChild($externalIdDoc);

            $contractorReferenceDoc = $doc->createElement('vhlID');
            $contractorReferenceDoc->nodeValue =$sample->getContractorReference();
            $sampleDoc->appendChild($contractorReferenceDoc);

            $petDoc = $doc->createElement('pe');
            /** @var Pet $pet */
            $pet = $sample->getPet();
            $petDoc->setAttribute('id',$pet->getPetId());

            $nameDoc = $doc->createElement('name');
            $nameDoc->nodeValue = $pet->getName();
            $petDoc->appendChild($nameDoc);

            $nameDoc = $doc->createElement('species_code');
            $nameDoc->nodeValue =$pet->getSpeciesCode();
            $petDoc->appendChild($nameDoc);

            $nameDoc = $doc->createElement('gender');
            $nameDoc->nodeValue =$pet->getGender();
            $petDoc->appendChild($nameDoc);

            $nameDoc = $doc->createElement('birth_date');
            $nameDoc->nodeValue =$this->dateFormatter->format($pet->getBirthDate(),'Y-m-d H:i:s');
            $petDoc->appendChild($nameDoc);

            $nameDoc = $doc->createElement('breed');
            $nameDoc->nodeValue =$pet->getBreed();
            $petDoc->appendChild($nameDoc);
            $nameDoc = $doc->createElement('breed_code');
            $nameDoc->nodeValue =$pet->getBreed();
            $petDoc->appendChild($nameDoc);
            $nameDoc = $doc->createElement('tattooID');
            $nameDoc->nodeValue =$pet->getTattooNbr();
            $petDoc->appendChild($nameDoc);
            $nameDoc = $doc->createElement('pedigreeID');
            $nameDoc->nodeValue =$pet->getPedigreeNbr();
            $petDoc->appendChild($nameDoc);
            $nameDoc = $doc->createElement('chipID');
            $nameDoc->nodeValue =$pet->getChipNbr();
            $petDoc->appendChild($nameDoc);
            $nameDoc = $doc->createElement('oldChipID');
            $nameDoc->nodeValue ='';
            $petDoc->appendChild($nameDoc);

            $sampleDoc->appendChild($petDoc);

            /** @var ParameterGroup $parameterGroup */
            foreach ($sample->getParameterGroups() as $parameterGroup) {
                $parameterGroupDoc = $doc->createElement('pg');
                $parameterGroupDoc->setAttribute('id',$parameterGroup->getParameterGroupId());

                $detailDoc = $doc->createElement('description');
                $detailDoc->nodeValue = $parameterGroup->getParameterGroupBase()->getDescription($language)->getDescription();
                // @TODO real language
                $detailDoc->setAttribute('lang',$language);
                $parameterGroupDoc->appendChild($detailDoc);

                /** @var Parameter $parameter */
                foreach ($parameterGroup->getParameters() as $parameter)  {
                    $parameterDoc = $doc->createElement('pa');
                    $parameterDoc->setAttribute('id',$parameter->getParameterId());

                    $descriptionDoc = $doc->createElement('description');
                    $descriptionDoc->setAttribute('lang',$language);
                    $descriptionDoc->nodeValue = $parameter->getParameterBase()->getDescription($language)->getDescription();
                    $parameterDoc->appendChild($descriptionDoc);

                    /*
                    $nameDoc = $doc->createElement('isTitle');
                    $nameDoc->nodeValue =$parameter->getIsTitle();
                    $parameterDoc->appendChild($nameDoc);
                    */

                    $statusDoc = $doc->createElement('status');
                    $statusDoc->nodeValue = $parameter->getStatus();
                    $parameterDoc->appendChild($statusDoc);

                    $statusDoc = $doc->createElement('loinc');
                    $statusDoc->nodeValue = $parameter->getParameterBase()->getLoinc();
                    $parameterDoc->appendChild($statusDoc);

                    $linkDoc = $doc->createElement('link');
                    $linkDoc->nodeValue = $parameter->getParameterBase()->getLink();
                    $parameterDoc->appendChild($linkDoc);

                    $statusDoc = $doc->createElement('pp');
                    $statusDoc->nodeValue = $parameter->getParameterP();
                    $parameterDoc->appendChild($statusDoc);

                    $statusDoc = $doc->createElement('validated_by');
                    $statusDoc->nodeValue = $parameter->getValidatedBy() ? $parameter->getValidatedBy()->getReference() : '';
                    $parameterDoc->appendChild($statusDoc);

                    $resultDoc = $doc->createElement('result');
                    $resultDoc->nodeValue = $parameter->getResultString();
                    $parameterDoc->appendChild($resultDoc);

                    $statusDoc = $doc->createElement('unit');
                    $statusDoc->nodeValue = $parameter->getParameterBase()->getUnit();
                    $parameterDoc->appendChild($statusDoc);

                    $statusDoc = $doc->createElement('result_flag');
                    $statusDoc->nodeValue = $parameter->getResultFlag();
                    $parameterDoc->appendChild($statusDoc);

                    $commentDoc = $doc->createElement('comment');
                    $commentDoc->setAttribute('lang',$language);
                    $commentDoc->nodeValue = $parameter->getComment();
                    $parameterDoc->appendChild($commentDoc);

                    $referenceRangeDoc = $doc->createElement('refRange');
                    /** @var ParameterReference $referenceRange */
                    $parameterReference = $parameter->getReference();
                    $statusDoc = $doc->createElement('refLow');
                    $statusDoc->nodeValue = $parameterReference->getLow();
                    $referenceRangeDoc->appendChild($statusDoc);

                    $statusDoc = $doc->createElement('refHigh');
                    $statusDoc->nodeValue = $parameterReference->getHigh();
                    $referenceRangeDoc->appendChild($statusDoc);

                    $statusDoc = $doc->createElement('refComment');
                    $statusDoc->nodeValue = $this->referenceCommentFetcher->get($sample, $parameter, $reportVersion->getLanguage());
                    $referenceRangeDoc->appendChild($statusDoc);

                    $parameterDoc->appendChild($referenceRangeDoc);

                    /*
                        $statusDoc = $doc->createElement('execution_date');
                        $statusDoc->nodeValue = $parameter->getExec();
                        $parameterDoc->appendChild($statusDoc);

                        $statusDoc = $doc->createElement('date_expected');
                        $statusDoc->nodeValue = $parameter->getDate();
                        $parameterDoc->appendChild($statusDoc);
                    */
                    /*
                    $files = $parameter->getFiles();
                    if (count($files)) {
                        $filesDoc = $doc->createElement('files');

                        /** @var File $file */
                    /*   foreach ($files as $file) {
                            $fileDoc = $doc->createElement('file');
                            $content = file_get_contents($this->container->getParameter('application_path') . '/web/report/images/' . $file->getName());
                            $cdata = $doc->createCDATASection(base64_encode($content));
                            $fileDoc->appendChild($cdata);
                            $filesDoc->appendChild($fileDoc);
                        }
                        $parameterDoc->appendChild($filesDoc);
                    }
                    */

                    $parameterGroupDoc->appendChild($parameterDoc);
                }
                $sampleDoc->appendChild($parameterGroupDoc);
            }

            $requestDoc->appendChild($sampleDoc);
        }


        $doc->appendChild($requestDoc);
        return new Response($doc->saveXML());
    }

    public function encodeToIso($string) {
        return mb_convert_encoding($string, "ISO-8859-1", mb_detect_encoding($string, "UTF-8, ISO-8859-1, ISO-8859-15", true));
    }


}
