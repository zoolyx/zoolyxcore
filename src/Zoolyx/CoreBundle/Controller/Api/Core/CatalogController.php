<?php

namespace Zoolyx\CoreBundle\Controller\Api\Core;

use DOMDocument;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Zoolyx\CoreBundle\Entity\Breed;
use Zoolyx\CoreBundle\Entity\ParameterBase;
use Zoolyx\CoreBundle\Entity\ParameterBaseDescription;
use Zoolyx\CoreBundle\Entity\ParameterProfile;
use Zoolyx\CoreBundle\Entity\ParameterProfileCategory;
use Zoolyx\CoreBundle\Entity\ParameterProfileCategoryDescription;
use Zoolyx\CoreBundle\Entity\ParameterProfileDescription;
use Zoolyx\CoreBundle\Entity\ParameterProfilePrice;
use Zoolyx\CoreBundle\Entity\ReferenceRange;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;

/**
 * @Security("is_granted('ROLE_ORGANISATION')")
 */
class CatalogController extends Controller
{
    /**
     * @var SecurityContextInterface
     *
     * @DI\Inject("security.context")
     */
    private $securityContext;


    /**
     * @var VeterinaryRepository $veterinaryRepository
     *
     * @DI\Inject("zoolyx_core.repository.veterinary")
     */
    private $veterinaryRepository;

    /**
     * @var ParameterProfileRepository $parameterProfileRepository
     *
     * @DI\Inject("zoolyx_core.repository.parameter_profile")
     */
    private $parameterProfileRepository;

    /**
     * @var string $rootUrl
     *
     * @DI\Inject("%root_url%")
     */
    private $rootUrl;

    /**
     * @param Request $request
     *
     * @return View
     *
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx",
     *  description="get the parameter profiles catalog",
     *  statusCodes={
     *      200="Returned when successful",
     *      401="Returned when token is missing or invalid",
     *  })
     */
    public function indexAction(Request $request)
    {

        $speciesFilterString = $request->query->get('species',null);
        $speciesFilter = is_null($speciesFilterString) ? array() : explode('-',$speciesFilterString);

        $categoryFilterString = $request->query->get('cat',null);
        $categoryFilter = is_null($categoryFilterString) ? array() : explode('-',$categoryFilterString);

        /** @var User $veterinaryAccount */
        $veterinaryAccount = $this->getUser();

        $faId = 0;//standard catalog

        /** @var Veterinary $veterinary */
        $veterinary = $this->veterinaryRepository->findOneBy(array('account'=>$veterinaryAccount));
        if ($veterinary) {
            foreach ($veterinary->getVeterinaryPractices() as $veterinaryPractice) {
                if ($veterinaryPractice->getVat()) {
                    $faId = $veterinaryPractice->getVat()->getFaId();
                    if ($faId == "") {
                        $faId = 0;
                    } else {
                        continue;
                    }
                }
            }
        }

        $parameterProfiles = $this->parameterProfileRepository->findAll();

        $doc = new DOMDocument("1.0","UTF-8");
        $container = $doc->createElement("importPp");
        $doc->appendChild($container);

        $ppContainer = $doc->createElement("parameterProfiles");
        $container->appendChild($ppContainer);

        $categories = array();
        /** @var ParameterProfile $parameterProfile */
        foreach ($parameterProfiles as $parameterProfile)
        {
            if ($speciesFilter) {
                if (!in_array($parameterProfile->getSpecies(), $speciesFilter)) {
                    continue;
                }
            }
            if ($categoryFilter) {
                if (!in_array($parameterProfile->getCategory()->getReference(), $categoryFilter)) {
                    continue;
                }
            }

            $selectedPrice = null;
            $standardPrice = null;
            /** @var ParameterProfilePrice $price */
            foreach ($prices = $parameterProfile->getPriceList() as $price) {
//                echo $price->getValue() . "(".$price->getBilling().") - ";
                //$billingId = $price->getBilling() ? $price->getBilling()->getId() : null;
                $billingId = $price->getBilling();
                if ($billingId == 0) {
                    $standardPrice = $price;
                }
                if ($faId && $billingId == $faId) {
                    $selectedPrice = $price;
                }


            }

//echo "\n";
            if (!is_null($selectedPrice)) {
                //echo "selected Price: " . $selectedPrice->getId();
            }
            if (is_null($standardPrice)) {
                continue;  //we don't add this parameterprofile to the catalog
            }

            $ppDoc = $doc->createElement("pp");
            $ppDoc->setAttribute('id',$parameterProfile->getReference());
            $ppContainer->appendChild($ppDoc);

            $ppCatDoc = $doc->createElement("ppCat");
            $ppCatDoc->nodeValue = $parameterProfile->getCategory()->getReference();
            $ppDoc->appendChild($ppCatDoc);
            $categories[$parameterProfile->getCategory()->getReference()] = $parameterProfile->getCategory();

            /** @var ParameterProfileDescription $description */
            foreach ($parameterProfile->getDescriptions() as $description) {
                $descDoc = $doc->createElement("desc");
                $descDoc->setAttribute('lang',$description->getLanguage());
                $descDoc->nodeValue = $description->getDescription();
                $ppDoc->appendChild($descDoc);
            }

            /** @var ParameterProfileDescription $description */
/*
            foreach ($parameterProfile->getDescriptions2() as $description) {
                $descDoc = $doc->createElement("desc2");
                $descDoc->setAttribute('lang',$description->getLanguage());
                $descDoc->nodeValue = $description->getDescription();
                $ppDoc->appendChild($descDoc);
            }
*/
            $speciesDoc = $doc->createElement("species");
            $speciesDoc->setAttribute('id',$parameterProfile->getSpecies());
            $ppDoc->appendChild($speciesDoc);

            /** @var Breed $breed */
            foreach ($parameterProfile->getBreeds() as $breed) {
                $fullBreedId = $breed->getBreedId();
                $breedIdParts = explode("_",$fullBreedId);
                $breedId = $breedIdParts[count($breedIdParts)-1];

                $breedDoc = $doc->createElement("breed");
                $breedDoc->setAttribute('id',$breedId);
                $speciesDoc->appendChild($breedDoc);
            }

            $parameterListDoc = $doc->createElement("paList");
            $ppDoc->appendChild($parameterListDoc);

            /** @var ParameterBase $parameter */
            foreach ($parameterProfile->getParameters() as $parameter) {
                $parameterDoc = $doc->createElement("pa");
                $parameterDoc->setAttribute('id',$parameter->getParameterId());
                $parameterListDoc->appendChild($parameterDoc);

                /** @var ParameterBaseDescription $description */
                foreach ($parameter->getDescriptions() as $description) {
                    $elementDoc = $doc->createElement("descr");
                    $elementDoc->setAttribute('lang',$description->getLanguage());
                    $elementDoc->nodeValue = $description->getDescription();
                    $parameterDoc->appendChild($elementDoc);
                }

                $elementDoc = $doc->createElement("unit");
                $elementDoc->nodeValue = $parameter->getUnit();
                $parameterDoc->appendChild($elementDoc);

                $elementDoc = $doc->createElement("altUnit");
                $elementDoc->setAttribute('convFactor',$parameter->getConversionFactor());
                $elementDoc->setAttribute('dec',$parameter->getDecimals());
                $elementDoc->nodeValue = $parameter->getAlternativeUnit();
                $parameterDoc->appendChild($elementDoc);

                $elementDoc = $doc->createElement("delay");
                $elementDoc->nodeValue = $parameter->getDelay();
                $parameterDoc->appendChild($elementDoc);
                
                $methodDoc = $doc->createElement("method");
                $parameterDoc->appendChild($methodDoc);

                foreach ($parameter->getMethods() as $method) {
                    $elementDoc = $doc->createElement("descr");
                    $elementDoc->setAttribute('lang',$method->getLanguage());
                    $elementDoc->nodeValue = $method->getDescription();
                    $methodDoc->appendChild($elementDoc);	
                }

                $refRangeDoc = $doc->createElement("refRange");
                $parameterDoc->appendChild($refRangeDoc);

                /** @var ReferenceRange $referenceRange */
                foreach ($parameter->getReferenceRanges() as $referenceRange) {
                    if ($referenceRange->getSpeciesCode() == $parameterProfile->getSpecies()) {
                        $elementDoc = $doc->createElement("refLow");
                        $elementDoc->nodeValue = $referenceRange->getLow();
                        $refRangeDoc->appendChild($elementDoc);

                        $elementDoc = $doc->createElement("refHigh");
                        $elementDoc->nodeValue = $referenceRange->getHigh();
                        $refRangeDoc->appendChild($elementDoc);

                        $elementDoc = $doc->createElement("refVeryLow");
                        $elementDoc->nodeValue = $referenceRange->getVeryLow();
                        $refRangeDoc->appendChild($elementDoc);

                        $elementDoc = $doc->createElement("refVeryHigh");
                        $elementDoc->nodeValue = $referenceRange->getVeryHigh();
                        $refRangeDoc->appendChild($elementDoc);

                        $elementDoc = $doc->createElement("refDisplay");
                        $elementDoc->nodeValue = $referenceRange->getReferenceDisplay();
                        $refRangeDoc->appendChild($elementDoc);

                        $elementDoc = $doc->createElement("toAge");
                        $elementDoc->nodeValue = $referenceRange->getToAge();
                        $refRangeDoc->appendChild($elementDoc);
                    }
                }

                //$elementDoc = $doc->createElement("link");
                //$elementDoc->nodeValue = $parameter->getLink();
                //$parameterDoc->appendChild($elementDoc);
            }

            foreach (array('nl','fr') as $language) {
                $urlDoc = $doc->createElement("url");
                $urlDoc->setAttribute('lang',$language);
                //$urlDoc->nodeValue = $parameterProfile->getUrl();
                $urlDoc->nodeValue = $this->rootUrl . '/' . $language . '/wiki/PP_'.$parameterProfile->getReference();
                $ppDoc->appendChild($urlDoc);
            }

            $priceListDoc = $doc->createElement("priceList");
            $ppDoc->appendChild($priceListDoc);

            $priceDoc = $doc->createElement("price");
            $priceDoc->setAttribute('type','standard');
            $priceDoc->nodeValue = $standardPrice->getValue();
            $priceListDoc->appendChild($priceDoc);

            if ($selectedPrice) {
                $priceDoc = $doc->createElement("price");
                $priceDoc->setAttribute('type','customer');
                $priceDoc->nodeValue = $selectedPrice->getValue();
                $priceListDoc->appendChild($priceDoc);
            }



        }

        $ppCatContainer = $doc->createElement("ppCategories");
        $container->appendChild($ppCatContainer);

        /** @var ParameterProfileCategory $category */
        foreach ($categories as $category) {
            $ppCatDoc = $doc->createElement("ppCat");
            $ppCatDoc->setAttribute('id',$category->getReference());
            $ppCatContainer->appendChild($ppCatDoc);

            /** @var ParameterProfileCategoryDescription $description */
            foreach ($category->getDescriptions() as $description) {
                //<descr lang="nl">Vitaminen</descr>
                $ppCatDescDoc = $doc->createElement("descr");
                $ppCatDescDoc->setAttribute('lang',$description->getLanguage());
                $ppCatDescDoc->nodeValue = $description->getDescription();
                $ppCatDoc->appendChild($ppCatDescDoc);
            }
        }

        return new Response($doc->saveXML());
    }

}
