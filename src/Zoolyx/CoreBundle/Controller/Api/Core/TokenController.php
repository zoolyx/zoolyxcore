<?php

namespace Zoolyx\CoreBundle\Controller\Api\Core;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Zoolyx\CoreBundle\Entity\Repository\UserRepository;

class TokenController extends Controller
{
    /**
     * @var UserRepository $userRepository
     *
     * @DI\Inject("zoolyx_core.repository.user")
     */
    private $userRepository;
    /**
     *
     * @return View
     *
     * @throws NotFoundHttpException if techne agora cannot be found
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx",
     *  description="ask for a token",
     *  statusCodes={
     *      200="Returned when successful",
     *      401="Returned when credentials are wrong",
     *  })
     */
    public function newTokenAction(Request $request)
    {
        $user = $this->userRepository->findOneBy(array(
            'username' => $request->getUser()
        ));
        if (!$user) {
            return View::create(['auth' => 'bad credentials'], 401);
            //throw new BadCredentialsException();
        }

        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $request->getPassword());

        if (!$isValid) {
            return View::create(['auth' => 'bad credentials'], 401);
            //throw new BadCredentialsException();
        }

        $token = $this->get('lexik_jwt_authentication.encoder')
            ->encode([
                'username' => $user->getUsername(),
                'exp' => time() + 3600 // 1 hour expiration
            ]);
        //return new JsonResponse(['token' => $token]);
        return View::create(['token' => $token], 200);
    }



}
