<?php

namespace Zoolyx\CoreBundle\Controller\Api;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Zoolyx\CoreBundle\Entity\File;
use Zoolyx\CoreBundle\Entity\OriginalRequest;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\Repository\OriginalRequestRepository;
use Zoolyx\CoreBundle\Entity\Repository\ReportRepository;
use Zoolyx\CoreBundle\Model\APIParser\ApiContentParser;
use Zoolyx\CoreBundle\Model\ApiParser\ApiContentTypeParser;
use Zoolyx\CoreBundle\Model\ApiParser\ApiFile;
use Zoolyx\CoreBundle\Model\XmlParser\ObjectParser\ReportParser;

class OriginalRequestController extends Controller
{

    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var ReportParser $reportParser
     *
     *  @DI\Inject("zoolyx_core.xml_parser.original_request"),
     */
    private $originalRequestParser;

    /**
     * @var ApiContentParser $apiContentParser
     *
     *  @DI\Inject("zoolyx_core.api_parser.content"),
     */
    private $apiContentParser;

    /**
     * @var ReportRepository $reportRepository
     *
     * @DI\Inject("zoolyx_core.repository.report")
     */
    private $reportRepository;

    /**
     * @var OriginalRequestRepository $originalRequestRepository
     *
     * @DI\Inject("zoolyx_core.repository.original_request")
     */
    private $originalRequestRepository;

    /**
     * @param Request $request
     *
     * @return View
     *
     * @throws NotFoundHttpException if techne agora cannot be found
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Lims",
     *  description="upload a the original request for a report",
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when the request could not be parsed",
     *      404="Returned when the related report was not found",
     *  })
     */
    public function indexAction(Request $request)
    {

        file_put_contents($this->container->getParameter('log_dir_lims') .'/'.date("YmdHis",time()).'-orig-request.xml',$request->getContent());

        $this->apiContentParser->setRequest($request);
        /** @var ApiContentTypeParser $contentType */
        $contentType = $this->apiContentParser->getContentType();
        if (!$contentType->hasValidContentType()) {
            return View::create('{"code":400,"message":"' . $contentType->getContentTypeStatus() . '"}', 400);
        }

        if (!$this->apiContentParser->canParse()) {
            return View::create('{"code":400,"message":"could not parse"}', 400);
        }

        $xml = $this->apiContentParser->getXml();
        $reportData=simplexml_load_string(utf8_encode($xml));

        /** @var OriginalRequest $originalRequest */
        $originalRequest = $this->originalRequestParser->parse($reportData);

        //validate the attachments
        /** @var File $file */
        foreach($originalRequest->getFiles() as $file) {
            if ($this->attachmentNotFound($file)) {
                return View::create('{"code":400,"message":"cannot find attachment"}', 400);
            }
        }

        /** @var OriginalRequest $existingRequest */
        if ($existingRequest = $this->originalRequestRepository->findOneBy(array('requestId'=>$originalRequest->getRequestId()))) {
            $existingRequest->setPracticeReference($originalRequest->getPracticeReference());
            $existingRequest->setCreatedOn($originalRequest->getCreatedOn());
            foreach ($existingRequest->getFiles() as $file) {
                $existingRequest->removeFile($file);
                $this->entityManager->remove($file);
            }
            foreach($originalRequest->getFiles() as $file) {
                $existingRequest->addFile($file);
            }
            $originalRequest = $existingRequest;
        }

        $this->entityManager->persist($originalRequest);
        $this->entityManager->flush();

        $fileNr = 1;
        /** @var File $file */
        foreach($originalRequest->getFiles() as $file) {
            $contentId = $file->getContentId();
            /** @var ApiFile $apiFile */
            $apiFile = $this->apiContentParser->getFile($contentId);
            $fileName = 'rq_' . $originalRequest->getId() . '_' . $fileNr . '.' . $apiFile->getExtension();
            file_put_contents($this->container->getParameter('application_path') .'/web/'. $this->container->getParameter('attachments_dir').'/'.$fileName,$apiFile->getContent());
            $file->setName($fileName);
            $file->setOriginalRequest($originalRequest);
            $this->entityManager->persist($file);
            $fileNr++;
        }
        $this->entityManager->flush();

        /** @var Report $report */
        if ($report = $this->reportRepository->findOneBy(array('requestId'=>$originalRequest->getRequestId()))) {
            $report->setOriginalRequest($originalRequest);
        }
        $this->entityManager->flush();

        return new Response();
    }



    private function attachmentNotFound(File $file) {
        $contentId = $file->getContentId();
        /** @var ApiFile $apiFile */
        $apiFile = $this->apiContentParser->getFile($contentId);
        return is_null($apiFile);
    }

}
