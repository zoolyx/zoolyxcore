<?php

namespace Zoolyx\CoreBundle\Controller\Api;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Zoolyx\CoreBundle\Entity\Repository\ParameterBaseRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileRepository;
use Zoolyx\CoreBundle\Model\Wiki\ParameterPageProvider;
use Zoolyx\CoreBundle\Model\Wiki\ParameterProfilePageProvider;


class WikiController extends Controller
{

    /**
     * @var ParameterBaseRepository $parameterBaseRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_base")
     */
    private $parameterBaseRepository;

    /**
     * @var ParameterProfileRepository $parameterProfileRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_profile")
     */
    private $parameterProfileRepository;

    /**
     * @var ParameterPageProvider $parameterPageProvider
     *
     *  @DI\Inject("zoolyx_core.wiki.parameter_page_provider")
     */
    private $parameterPageProvider;


    /**
     * @var ParameterProfilePageProvider $parameterProfilePageProvider
     *
     *  @DI\Inject("zoolyx_core.wiki.parameter_profile_page_provider")
     */
    private $parameterProfilePageProvider;


    /**
     *
     * @return View
     *
     * @throws NotFoundHttpException when breed data could not be parsed
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Wiki",
     *  description="retrieve wiki data",
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when data could not be found",
     *  })
     */
    public function indexAction()
    {
        $parameters = $this->parameterBaseRepository->findForWikilabs();
        $parameterPages = $this->parameterPageProvider->getPages($parameters);

        //$parameterProfiles = $this->parameterProfileRepository->findForCategories(array(100, 110, 120, 130, 690, 725));
        $parameterProfiles = $this->parameterProfileRepository->findAll();
        $parameterProfilePages = $this->parameterProfilePageProvider->getPages($parameterProfiles);

        return $this->render('ZoolyxCoreBundle:Wiki:pages.xml.twig', array(
            'parameterPages' => $parameterPages,
            'parameterProfilePages' => $parameterProfilePages
        ));
    }


}
