<?php

namespace Zoolyx\CoreBundle\Controller\Api;

use Doctrine\Common\Persistence\ObjectManager;
use DOMDocument;
use DOMElement;
use DOMNode;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use XSLTProcessor;
use Zoolyx\CoreBundle\Entity\Contractor;
use Zoolyx\CoreBundle\Entity\ContractorReport;
use Zoolyx\CoreBundle\Entity\File;
use Zoolyx\CoreBundle\Entity\OriginalRequest;
use Zoolyx\CoreBundle\Entity\Parameter;
use Zoolyx\CoreBundle\Entity\ParameterGroup;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\ReportShare;
use Zoolyx\CoreBundle\Entity\ReportVersion;
use Zoolyx\CoreBundle\Entity\Repository\ContractorReportRepository;
use Zoolyx\CoreBundle\Entity\Repository\OriginalRequestRepository;
use Zoolyx\CoreBundle\Entity\Repository\ReportRepository;
use Zoolyx\CoreBundle\Entity\Repository\ReportShareRepository;
use Zoolyx\CoreBundle\Entity\Sample;
use Zoolyx\CoreBundle\Event\ReportEvent;
use Zoolyx\CoreBundle\Events;
use Zoolyx\CoreBundle\Model\APIParser\ApiContentParser;
use Zoolyx\CoreBundle\Model\ApiParser\ApiContentTypeParser;
use Zoolyx\CoreBundle\Model\ApiParser\ApiFile;
use Zoolyx\CoreBundle\Model\Exception\ReportNotFoundException;
use Zoolyx\CoreBundle\Model\Report\Contractor\ContractorDetector;
use Zoolyx\CoreBundle\Model\Report\PracticeFilter;
use Zoolyx\CoreBundle\Model\Report\ReportBuilder;
use Zoolyx\CoreBundle\Model\Report\ReportHash;
use Zoolyx\CoreBundle\Model\Report\ReportHashProvider;
use Zoolyx\CoreBundle\Model\Report\ReportLoader;
use Zoolyx\CoreBundle\Model\Report\ReportVersionFileManager;
use Zoolyx\CoreBundle\Model\Report\Translator;
use Zoolyx\CoreBundle\Model\XmlParser\ObjectParser\ReportParser;
use Zoolyx\CoreBundle\Model\XmlParser\ObjectParser\ReportVersionParser;
use Zoolyx\CoreBundle\Model\XmlParser\XmlValidator;

class ImportController extends Controller
{

    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var ReportParser $reportParser
     *
     *  @DI\Inject("zoolyx_core.xml_parser.report"),
     */
    private $reportParser;

    /**
     * @var ReportVersionParser $reportVersionParser
     *
     *  @DI\Inject("zoolyx_core.xml_parser.report_version"),
     */
    private $reportVersionParser;

    /**
     * @var ApiContentParser $apiContentParser
     *
     *  @DI\Inject("zoolyx_core.api_parser.content"),
     */
    private $apiContentParser;


    /**
     * @var ReportRepository $reportRepository
     *
     * @DI\Inject("zoolyx_core.repository.report")
     */
    private $reportRepository;

    /**
     * @var ReportShareRepository $reportShareRepository
     *
     * @DI\Inject("zoolyx_core.repository.report_share")
     */
    private $reportShareRepository;

    /**
     * @var OriginalRequestRepository $originalRequestRepository
     *
     * @DI\Inject("zoolyx_core.repository.original_request")
     */
    private $originalRequestRepository;

    /**
     * @var ReportHashProvider $reportVHashProvider
     *
     * @DI\Inject("zoolyx_core.report.hash_provider")
     */
    private $reportHashProvider;

    /**
     * @var ReportLoader $reportLoader
     *
     * @DI\Inject("zoolyx_core.report.loader")
     */
    private $reportLoader;

    /**
     * @var ReportBuilder $reportBuilder
     *
     * @DI\Inject("zoolyx_core.report.builder")
     */
    private $reportBuilder;

    /**
     * @var ContractorReportRepository $contractorReportRepository
     *
     * @DI\Inject("zoolyx_core.repository.contractor_report")
     */
    private $contractorReportRepository;

    /**
     * @var ContractorDetector $contractorDetector
     *
     * @DI\Inject("zoolyx_core.report.contractor_detector")
     */
    private $contractorDetector;

    /**
     * @var PracticeFilter $practiceFilter
     *
     * @DI\Inject("zoolyx_core.report.practice_filter")
     */
    private $practiceFilter;
    /**
     * @var ReportVersionFileManager $reportVersionFileManager
     *
     * @DI\Inject("zoolyx_core.report_version.file_manager")
     */
    private $reportVersionFileManager;

    /**
     * @var Translator $translator
     * @DI\Inject("zoolyx_core.report.translator")
     */
    private $translator;

    /**
     * @var EventDispatcherInterface
     *
     * @DI\Inject("event_dispatcher")
     */
    private $eventDispatcher;

    /** @var  XmlValidator
     *
     * @DI\Inject("zoolyx_core.xml_parser.xml_validator")
     */
    private $xmlValidator;

    /**
     * @param Request $request
     *
     * @return View
     *
     * @throws NotFoundHttpException if techne agora cannot be found
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Lims",
     *  description="upload a new report",
     *  statusCodes={
     *      200="Returned when successful",
     *      404="Returned when report could not be parsed",
     *  })
     */
    public function indexAction(Request $request)
    {
        file_put_contents($this->container->getParameter('log_dir_lims') .'/'.date("YmdHis",time()).'-'.$this->reportHashProvider->get().'-request.xml',$request->getContent());

        $this->apiContentParser->setRequest($request);
        /** @var ApiContentTypeParser $contentType */
        $contentType = $this->apiContentParser->getContentType();
        if (!$contentType->hasValidContentType()) {
            return View::create('{"code":400,"message":"' . $contentType->getContentTypeStatus() . '"}', 400);
        }

        if (!$this->apiContentParser->canParse()) {
            return View::create('{"code":400,"message":"could not parse"}', 400);
        }

        $xml = utf8_encode($this->apiContentParser->getXml());
        $reportData=simplexml_load_string($xml);

        /** @var Report $report */
        $report = $this->reportParser->parse($reportData);
        $report->setHash($this->reportHashProvider->get());
        $report->setAlternativeHash($this->reportHashProvider->get());

        /** @var Report $existingReport */
        if ($existingReport = $this->reportRepository->findOneBy(array('requestId'=>$report->getRequestId()))) {
            foreach($existingReport->getFiles() as $file) {
                $existingReport->removeFile($file);
                $this->entityManager->remove($file);
            }
            $report = $this->reportParser->setReport($existingReport)->parse($reportData);
            $report->setIsNew(false);
        }

        /** @var ReportVersion $reportVersion */
        $reportVersion = $this->reportVersionParser->parse($reportData);

        //validate the attachments
        /** @var File $file */
        foreach ($report->getFiles() as $file){
            if (is_null($this->apiContentParser->getFile($file->getContentId()))) {
                return View::create('{"code":400,"message":"cannot find attachment"}', 400);
            }
        }
        foreach ($this->reportVersionFileManager->getFiles($reportVersion) as $file){
            if (is_null($this->apiContentParser->getFile($file->getContentId()))) {
                return View::create('{"code":400,"message":"cannot find attachment"}', 400);
            }
        }

        if ($report->hasVersion($reportVersion->getLanguage())) {
            foreach ($report->getVersions() as $version) {
                $report->removeVersion($version);
                $this->entityManager->remove($version);
            }
        }
        $report->addVersion($reportVersion);
        $reportVersion->setReport($report);

        $this->practiceFilter->filter($report);

        if (is_null($report->getOwner()) || $report->getOwner()->getLimsId() == '') {
            return $this->makeResponse(null, array('missing owner'));
        }

        $this->entityManager->persist($report);
        $this->persistAttachments($reportVersion);
        $this->entityManager->flush();

        //this needs to happen after flush
        $contractorReportDoc = new DOMDocument("1.0","UTF-8");
        $contractorReportDoc->loadXML($xml);
        $files = $contractorReportDoc->getElementsByTagName('file');
        /** @var DomElement $file */
        foreach ($files as $file) {
            $cid = $file->getAttribute('cid');
            if ($cid) {
                // remove all children
                while ($file->firstChild) {
                    $file->removeChild($file->firstChild);
                }
                $content = $this->apiContentParser->getFile($cid)->getContent();
                $cdata = $contractorReportDoc->createCDATASection(base64_encode($content));
                $file->appendChild($cdata);
            }
        }

        $contractorsInvolved = $this->contractorDetector->getInvolvedContractor($report);
        if ($contractorsInvolved) {
            file_put_contents($this->container->getParameter('log_dir_lims') .'/'.$report->getId().'.xml',$contractorReportDoc->saveXml());
        }


        $translationErrors = array();
        /** @var Contractor $contractor */
        foreach ($contractorsInvolved as $contractor) {
            //first check if we can translate the report for this contractor
            $requestXmlDoc = new DOMDocument("1.0","UTF-8");
            $requestContainer = $requestXmlDoc->createElement('request');
            $requestContainer->setAttribute('createdOn',gmdate("Y-m-d\TH:i:s\Z")); //display time in UTC
            $requestContainer->setAttribute('requestId','1');
            $requestXmlDoc->appendChild($requestContainer);

            $xml = $contractorReportDoc->saveXml();
            $xmlDoc = new DOMDocument();
            $xmlDoc->loadXML($xml);

            $x = $xmlDoc->documentElement;
            $requestContainer->appendChild($requestXmlDoc->importNode($x, true));
            if (!is_null($contractor->getXsdFile()) && !is_null($contractor->getXslFile())) {
                $xslFile = $this->container->getParameter('application_path') .'/web/bundles/zoolyxcore/xsl/'.$contractor->getXslFile();
                $xslDoc = new DOMDocument();
                $xslDoc->load($xslFile);

                $xsltProcessor = new XSLTProcessor();
                $xsltProcessor->importStylesheet($xslDoc);
                $xmlOutput = $xsltProcessor->transformToXML($requestXmlDoc);

                // validate our output
                $this->xmlValidator->setXml($xmlOutput);
                $xsdFile = $this->container->getParameter('application_path') .'/web/bundles/zoolyxcore/xsd/co/'.$contractor->getXsdFile();
                if (!$this->xmlValidator->validate($xsdFile)) {
                    if ($this->xmlValidator->getErrors()) {
                        $translationErrors[] = 'Xsd validation failed';
                        continue;
                    }
                }

                //check if each sample has an rvo code
                //file_put_contents($this->container->getParameter('log_dir_lims') .'/debug_output_'.$report->getId().'.xml',$xmlOutput);
                $checkXmlDoc = new DOMDocument();
                $checkXmlDoc->loadXML($xmlOutput);
                $sampleDocs = $checkXmlDoc->getElementsByTagName('sample');
                /** @var DomNode $sample */
                foreach ($sampleDocs as $sample) {
                    $rvoCodeFound = false;
                    /** @var DomNode $childNode */
                    foreach ($sample->childNodes as $childNode) {
                        if ($childNode->nodeName == 'rvo') $rvoCodeFound = true;
                    }
                    if (!$rvoCodeFound) {
                        $translationErrors[] = 'no rvo code for sample '.$sample->getAttribute('extRef');
                        continue;
                    }
                }

                if ($translationErrors) continue;
            }


            //we can translate, so we can create a contractorReport
            $contractorReport = $this->contractorReportRepository->findOneBy(array('report'=>$report, 'contractor'=>$contractor));
            if (is_null($contractorReport)) {
                $contractorReport = new ContractorReport();
                $contractorReport->setStatus(ContractorReport::CONTRACTOR_NEW_REPORT);
                $contractorReport->setCreated(new \DateTime());
                $contractorReport->setContractor($contractor);
                $contractorReport->setReport($report);
                $this->entityManager->persist($contractorReport);
            }

            //share this report with the contractor
            $shareAccount = is_null($contractor->getShareAccount()) ? $contractor : $contractor->getShareAccount();
            $reportShare = $this->reportShareRepository->findOneBy(array('report'=>$report, 'user' => $shareAccount));
            if (is_null($reportShare)) {
                $reportShare = new ReportShare();
                $reportShare->setReport($report)->setUser($shareAccount);
                $this->entityManager->persist($reportShare);
            }
        }

        /** @var OriginalRequest $originalRequest */
        if ($originalRequest = $this->originalRequestRepository->findOneBy(array('requestId'=>$report->getRequestId()))) {
            $report->setOriginalRequest($originalRequest);
        }

        $this->eventDispatcher->dispatch(Events::REPORT_IMPORTED, new ReportEvent($report));

        $this->entityManager->flush();

        $url = $this->getParameter('server_url') . $this->generateUrl('zoolyx_report', array('hash'=> $report->getHash().$reportVersion->getLanguage()));
        return $this->makeResponse($url, $translationErrors);
    }

    public function previewAction(Request $request)
    {
        file_put_contents($this->container->getParameter('log_dir_lims') .'/'.date("YmdHis",time()).'-'.$this->reportHashProvider->get().'-preview.xml',$request->getContent());
        $this->apiContentParser->setRequest($request);
        /** @var ApiContentTypeParser $contentType */
        $contentType = $this->apiContentParser->getContentType();
        if (!$contentType->hasValidContentType()) {
            return View::create('{"code":400,"message":"' . $contentType->getContentTypeStatus() . '"}', 400);
        }

        if (!$this->apiContentParser->canParse()) {
            return View::create('{"code":400,"message":"could not parse"}', 400);
        }

        $xml = $this->apiContentParser->getXml();
        $reportData=simplexml_load_string(utf8_encode($xml));

        /** @var Report $report */
        $report = $this->reportParser->parse($reportData);
        /** @var ReportVersion $reportVersion */
        $reportVersion = $this->reportVersionParser->parse($reportData);

        $report->addVersion($reportVersion);
        $reportVersion->setReport($report);

        //validate the attachments
        /** @var File $file */
        foreach ($report->getFiles() as $file){
            if (is_null($this->apiContentParser->getFile($file->getContentId()))) {
                return View::create('{"code":400,"message":"cannot find attachment"}', 400);
            }
        }
        foreach ($this->reportVersionFileManager->getFiles($reportVersion) as $file){
            if (is_null($this->apiContentParser->getFile($file->getContentId()))) {
                return View::create('{"code":400,"message":"cannot find attachment"}', 400);
            }
        }

        $this->persistAttachments($reportVersion);
        $this->reportBuilder->setReportVersion($reportVersion);

        $html = $this->renderView('ZoolyxCoreBundle:Report:report.html.twig', array(
            'report' => $this->reportBuilder,
            'avoidPageBreakInGroup' => false,
            'isPdf' => true
        ));
        $htmlHeader =  $this->renderView('ZoolyxCoreBundle:Report:header.html.twig', array(
            'report' => $this->reportBuilder
        ));
        $htmlFooter =  $this->renderView('ZoolyxCoreBundle:Report:footer.html.twig');

        $snappy = $this->get('knp_snappy.pdf');
        $pdf = $snappy->getOutputFromHtml($html,array(
            'orientation'=>'Portrait',
            'header-spacing' => 5,
            'header-html' => $htmlHeader,
            'footer-spacing' => 10,
            'footer-html' => $htmlFooter,
        ));

        $randomString = substr(str_shuffle(MD5(microtime())), 0, 5);
        $pdfFileName = $randomString.'.pdf';
        file_put_contents($this->container->getParameter('application_path') .'/web/'. $this->container->getParameter('preview_dir').'/'.$pdfFileName,$pdf);

        //remove the original files
        array_map("unlink",glob($this->container->getParameter('application_path') .'/web/'. $this->container->getParameter('attachments_dir').'/_*.*'));

        $url =  $request->getHost() . $this->container->get('templating.helper.assets')->getUrl( $this->container->getParameter('preview_dir') .'/'. $pdfFileName);

        return $this->makeResponse($url);
    }

    private function makeResponse($url, $errors = array()) {
        $doc = new DOMDocument("1.0","UTF-8");

        $resultDoc = $doc->createElement('result');
        $doc->appendChild($resultDoc);

        if (!is_null($url)) {
            $urlDoc = $doc->createElement('url');
            $urlDoc->nodeValue = $url;
            $resultDoc->appendChild($urlDoc);
        }

        if ($errors) {
            $errorsDoc = $doc->createElement('errors');
            $resultDoc->appendChild($errorsDoc);
            foreach ($errors as $error) {
                $errorDoc = $doc->createElement('error');
                $errorDoc->nodeValue = $error;
                $errorsDoc->appendChild($errorDoc);
            }
        }

        $status = is_null($url) ? 400 : 200;
        return new Response($doc->saveXML(), $status);
    }

    public function translateAction($id, $language) {
        $reportVersion = $this->reportLoader->loadByHash(new ReportHash($id));
        if (!$reportVersion) {
            throw new ReportNotFoundException();
        }

        $this->translator->translateTo($reportVersion->getReport(), $language);

        return $this->render('ZoolyxCoreBundle:View:translate.html.twig', array(
            'reportId' => $id
        ));
    }

    private function persistAttachments(ReportVersion $reportVersion) {
        $fileNr = 1;
        //the main attachments
        /** @var File $file */
        $report = $reportVersion->getReport();
        foreach($report->getFiles() as $file) {
            $this->handleAttachment($report, $file, $fileNr++);
        }

        //the attachments inside a parameter
        /** @var Sample $sample */
        foreach ($reportVersion->getSamples() as $sample) {
            /** @var ParameterGroup $parameterGroup */
            foreach ($sample->getParameterGroups() as $parameterGroup) {
                /** @var Parameter $parameter */
                foreach ($parameterGroup->getParameters() as $parameter) {
                    foreach ($parameter->getFiles() as $file) {
                        $this->handleAttachment($report, $file, $fileNr++);
                    }
                }
            }
        }
    }


    private function handleAttachment(Report $report, File $file, $fileNr) {
        $contentId = $file->getContentId();
        /** @var ApiFile $apiFile */
        $apiFile = $this->apiContentParser->getFile($contentId);
        $fileName = $report->getHash() . '_' . $fileNr . '.' . $apiFile->getExtension();
        $file->setName($fileName);
        file_put_contents($this->container->getParameter('application_path') .'/web/'. $this->container->getParameter('attachments_dir').'/'.$fileName,$apiFile->getContent());
    }
}
