<?php

namespace Zoolyx\CoreBundle\Controller\Api\Contractor;

use Doctrine\Common\Persistence\ObjectManager;
use DOMDocument;
use DOMElement;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use XSLTProcessor;
use Zoolyx\CoreBundle\Entity\Contractor;
use Zoolyx\CoreBundle\Entity\ContractorReport;
use Zoolyx\CoreBundle\Entity\ContractorRequest;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\Repository\ContractorReportRepository;
use Zoolyx\CoreBundle\Entity\Repository\ContractorRequestRepository;
use Zoolyx\CoreBundle\Entity\Repository\ReportRepository;
use Zoolyx\CoreBundle\Model\ApiParser\ApiContentParser;
use Zoolyx\CoreBundle\Model\Ftp\FtpService;
use Zoolyx\CoreBundle\Model\XmlParser\XmlValidator;

/**
 * @Security("is_granted('ROLE_CONTRACTOR')")
 */
class PollController extends Controller
{
    /**
     * @var SecurityContextInterface
     *
     * @DI\Inject("security.context")
     */
    private $securityContext;

    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var ApiContentParser $apiContentParser
     *
     *  @DI\Inject("zoolyx_core.api_parser.content")
     */
    private $apiContentParser;

    /**
     * @var ReportRepository $reportRepository
     *
     *  @DI\Inject("zoolyx_core.repository.report")
     */
    private $reportRepository;

    /**
     * @var ContractorReportRepository $contractorReportRepository
     *
     * @DI\Inject("zoolyx_core.repository.contractor_report")
     */
    private $contractorReportRepository;

    /**
     * @var ContractorRequestRepository $contractorRequestRepository
     *
     *  @DI\Inject("zoolyx_core.repository.contractor_request")
     */
    private $contractorRequestRepository;

    /**
     * @var FtpService $ftpService
     *
     *  @DI\Inject("zoolyx_core.ftp.service")
     */
    private $ftpService;

    /** @var  XmlValidator
     *
     * @DI\Inject("zoolyx_core.xml_parser.xml_validator")
     */
    private $xmlValidator;


    /**
     * @return View
     *
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Contractor",
     *  description="poll for new requests",
     *  statusCodes={
     *      200="Returned when successful"
     *  })
     */
    public function indexAction()
    {
        /** @var Contractor $contractor */
        $contractor = $this->securityContext->getToken()->getUser();
        $contractorReports = $contractor->getContractorReports();

        $doc = new DOMDocument("1.0","UTF-8");
        $requestsDoc = $doc->createElement('requests');
        $doc->appendChild($requestsDoc);

        // this should come from the database as a property of the contractor
        $reportsAreGroupedInRequest = true;

        if ($reportsAreGroupedInRequest) {
            $contractorRequests = $this->contractorRequestRepository->findBy(array('contractor'=>$contractor, 'status' => ContractorReport::CONTRACTOR_NEW_REPORT));
            /** @var ContractorRequest $contractorRequest */
            foreach ($contractorRequests as $contractorRequest) {
                $requestDoc = $doc->createElement('request');
                $requestsDoc->appendChild($requestDoc);
                $requestDoc->setAttribute('id',$contractorRequest->getId());
                $requestDoc->setAttribute('date',gmdate("Y-m-d\TH:i:s\Z", $contractorRequest->getCreated()->getTimestamp()));
            }

            $newContractorReports = array();
            /** @var ContractorReport $contractorReport */
            foreach ($contractorReports as $contractorReport) {
                if (is_null($contractorReport->getContractorRequest())) {
                    $newContractorReports[] = $contractorReport;
                }
            }
            if ($newContractorReports) {
                $contractorRequest = new ContractorRequest();
                $contractorRequest->setContractor($contractor)->setStatus(ContractorReport::CONTRACTOR_NEW_REPORT)->setCreated(new \DateTime());
                foreach ($newContractorReports as $contractorReport) {
                    $contractorRequest->addContractorReport($contractorReport);
                    $contractorReport->setContractorRequest($contractorRequest);
                    $this->entityManager->persist($contractorReport);
                }
                $this->entityManager->persist($contractorRequest);
                $this->entityManager->flush();

                $requestDoc = $doc->createElement('request');
                $requestsDoc->appendChild($requestDoc);
                $requestDoc->setAttribute('id',$contractorRequest->getId());
                $requestDoc->setAttribute('date',gmdate("Y-m-d\TH:i:s\Z", $contractorRequest->getCreated()->getTimestamp()));
            }
        } else {
            /** @var ContractorReport $contractorReport */
            foreach ($contractorReports as $contractorReport) {
                if ($contractorReport->getStatus() == ContractorReport::CONTRACTOR_NEW_REPORT) {
                    $report = $contractorReport->getReport();
                    $requestDoc = $doc->createElement('request');
                    $requestsDoc->appendChild($requestDoc);
                    $requestDoc->setAttribute('id',$report->getRequestId());
                    $requestDoc->setAttribute('date',gmdate("Y-m-d\TH:i:s\Z", $report->getCreatedOn()->getTimestamp()));
                }
            };
        }

        return new Response($doc->saveXML());
    }

    /**
     * @param String $requestId
     *
     * @return View
     **
     * @Doc\ApiDoc(
     *  section="Zoolyx Contractor",
     *  description="load request",
     *  statusCodes={
     *      200="Returned when successful"
     *  })
     */
    public function requestAction($requestId)
    {
        /** @var Contractor $contractor */
        $contractor = $this->securityContext->getToken()->getUser();

        /** @var ContractorRequest $contractorRequest */
        $contractorRequest = $this->contractorRequestRepository->find($requestId);
        if (!$contractorRequest) {
            return new Response('not found',404);
        }
        $requestContractor = $contractorRequest->getContractor();
        if (!$requestContractor || $requestContractor->getId() != $contractor->getId()) {
            return new Response('not found',404);
        }

        $requestXmlDoc = new DOMDocument("1.0","UTF-8");
        $requestContainer = $requestXmlDoc->createElement('request');
        $requestContainer->setAttribute('createdOn',gmdate("Y-m-d\TH:i:s\Z")); //display time in UTC
        $requestContainer->setAttribute('requestId',$contractorRequest->getId());
        $requestXmlDoc->appendChild($requestContainer);

        $contractorReports = $contractorRequest->getContractorReports();

        /** @var ContractorReport $contractorReport */
        foreach ($contractorReports as $contractorReport) {
            $report = $contractorReport->getReport();
            $xml = file_get_contents($this->container->getParameter('log_dir_lims') .'/'.$report->getId().'.xml');
            $xmlDoc = new DOMDocument();
            $xmlDoc->loadXML($xml);

            $x = $xmlDoc->documentElement;
            $requestContainer->appendChild($requestXmlDoc->importNode($x, true));
        }

        //die($requestXmlDoc->saveXML());

        $xslFile = $this->container->getParameter('application_path') .'/web/bundles/zoolyxcore/xsl/vhl_request.xsl';
        $xslDoc = new DOMDocument();
        $xslDoc->load($xslFile);

        $xsltProcessor = new XSLTProcessor();
        $xsltProcessor->importStylesheet($xslDoc);
        $xmlOutput = $xsltProcessor->transformToXML($requestXmlDoc);





        // validate our output
        $this->xmlValidator->setXml($xmlOutput);
        $xsdFile = $this->container->getParameter('application_path') .'/web/bundles/zoolyxcore/xsd/co/vhl_request.xsd';
        if (!$this->xmlValidator->validate($xsdFile)) {
            print_r($this->xmlValidator->getErrors());
            die();
        }

        return new Response($xmlOutput);
    }


    /**
     * @param String $requestId
     *
     * @return View
     **
     * @Doc\ApiDoc(
     *  section="Zoolyx Contractor",
     *  description="load a report",
     *  statusCodes={
     *      200="Returned when successful"
     *  })
     */
    public function requestPerReportAction($requestId)
    {
        /** @var Contractor $contractor */
        $contractor = $this->securityContext->getToken()->getUser();

        /** @var Report $report */
        $report = $this->reportRepository->findOneBy(array('requestId' => $requestId));
        if (!$report) {
            return new Response('not found',404);
        }

        /** @var ContractorReport $contractorReport */
        $contractorReport = $this->contractorReportRepository->findOneBy(array('report'=>$report, 'contractor'=>$contractor));
        if (is_null($contractorReport)) {
            return new Response('not found',404);
        }

        $xml = file_get_contents($this->container->getParameter('log_dir_lims') .'/'.$report->getId().'.xml');

        $xslFile = $this->container->getParameter('application_path') .'/web/bundles/zoolyxcore/xsl/vhl.xsl';
        $xslDoc = new DOMDocument();
        $xslDoc->load($xslFile);

        $xmlDoc = new DOMDocument();
        $xmlDoc->loadXML($xml);
        $rqElements = $xmlDoc->getElementsByTagName('rq');

        /** @var DOMElement $rqElement */
        foreach ($rqElements as $rqElement ) {
            $rqElement->setAttribute('createdOn',gmdate("Y-m-d\TH:i:s\Z")); //display time in UTC
        }


        $xsltProcessor = new XSLTProcessor();
        $xsltProcessor->importStylesheet($xslDoc);
        $xmlOutput = $xsltProcessor->transformToXML($xmlDoc);

        // validate our output
        /*
        $this->xmlValidator->setXml($xmlOutput);
        $xsdFile = $this->container->getParameter('application_path') .'/web/bundles/zoolyxcore/xsd/co/vhl_request.xsd';
        if (!$this->xmlValidator->validate($xsdFile)) {
            print_r($this->xmlValidator->getErrors());
            die();
        }
        */

        return new Response($xmlOutput);
    }

    /**
     * @param String $requestId
     *
     * @return View
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Contractor",
     *  description="confirm a request",
     *  statusCodes={
     *      200="Returned when successful"
     *  })
     */
    public function confirmAction($requestId)
    {
        /** @var Contractor $contractor */
        $contractor = $this->securityContext->getToken()->getUser();

        /** @var ContractorRequest $contractorRequest */
        $contractorRequest = $this->contractorRequestRepository->find($requestId);
        if (!$contractorRequest) {
            return new Response('not found',404);
        }
        $requestContractor = $contractorRequest->getContractor();
        if (!$requestContractor || $requestContractor->getId() != $contractor->getId()) {
            return new Response('not found',404);
        }

        if ($contractorRequest->getStatus() != ContractorReport::CONTRACTOR_NEW_REPORT) {
            return new Response('request already confirmed',404);
        }

        $contractorRequest->setStatus(ContractorReport::CONTRACTOR_REPORT_SEEN);
        $contractorRequest->setConfirmed(new \DateTime());
        $this->entityManager->persist($contractorRequest);
        $this->entityManager->flush();
        return new Response('confirmed',200);
    }


    /**
     * @param String $requestId
     *
     * @return View
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Contractor",
     *  description="confirm a request",
     *  statusCodes={
     *      200="Returned when successful"
     *  })
     */
    public function confirmPerReportAction($requestId)
    {
        /** @var Contractor $contractor */
        $contractor = $this->securityContext->getToken()->getUser();

        /** @var Report $report */
        $report = $this->reportRepository->findOneBy(array('requestId' => $requestId));
        if (!$report) {
            return new Response('not found',404);
        }

        /** @var ContractorReport $contractorReport */
        $contractorReport = $this->contractorReportRepository->findOneBy(array('report'=>$report, 'contractor'=>$contractor));
        if (is_null($contractorReport)) {
            return new Response('not found',404);
        }
        if ($contractorReport->getStatus() != ContractorReport::CONTRACTOR_NEW_REPORT) {
            return new Response('report already confirmed',404);
        }

        $contractorReport->setStatus(ContractorReport::CONTRACTOR_REPORT_SEEN);
        $contractorReport->setConfirmed(new \DateTime());
        $this->entityManager->persist($report);
        $this->entityManager->flush();
        return new Response('confirmed',200);
    }

    /**
     * @param Request $request
     *
     * @return View
     *
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Contractor",
     *  description="post a result",
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Result could not be parsed"
     *  })
     */
    public function resultAction(Request $request)
    {
        file_put_contents($this->container->getParameter('log_dir_contractor') .'/'.date("YmdHis",time()).'-result.xml',$request->getContent());

        /** @var Contractor $contractor */
        $contractor = $this->securityContext->getToken()->getUser();

        $this->apiContentParser->setRequest($request);

        $xsdFile = $this->container->getParameter('application_path') .'/web/bundles/zoolyxcore/xsd/co/vhl_result.xsd';
        if (!$this->apiContentParser->isValid($xsdFile)) {
            return View::create(['errors'=>$this->apiContentParser->getErrors()], 400);
        }


        $doc = $this->apiContentParser->getDomDocument();

        //$x = $doc->documentElement;

        $xslFile = $this->container->getParameter('application_path') .'/web/bundles/zoolyxcore/xsl/vhl_to_zoolyx.xsl';
        $xslDoc = new DOMDocument();
        $xslDoc->load($xslFile);

        $xsltProcessor = new XSLTProcessor();
        $xsltProcessor->importStylesheet($xslDoc);
        $xmlOutput = $xsltProcessor->transformToXML($doc);
        $xmlOutputDoc = new DOMDocument();
        $xmlOutputDoc->loadXML($xmlOutput);

        $requestId = "vhl_".time();
        $service = new DOMDocument("1.0","UTF-8");
        $serviceContainer = $service->createElement('service');
        $serviceContainer->appendChild($service->importNode($xmlOutputDoc->documentElement, true));
        $serviceContainer->setAttribute('type','save_results');

        $service->appendChild($serviceContainer);

        $tempFile = fopen('php://memory', 'r+');
        fputs($tempFile, $service->saveXML());
        rewind($tempFile);
        $this->ftpService->put($requestId . '.xml', $tempFile);

        return View::create([], 200);    }


}
