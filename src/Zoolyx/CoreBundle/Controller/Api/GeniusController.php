<?php

namespace Zoolyx\CoreBundle\Controller\Api;

use Doctrine\Common\Persistence\ObjectManager;
use DOMElement;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Zoolyx\CoreBundle\Entity\GeniusCode;
use Zoolyx\CoreBundle\Entity\GeniusCodeDescription;
use Zoolyx\CoreBundle\Entity\ParameterProfile;
use Zoolyx\CoreBundle\Entity\Repository\GeniusCodeDescriptionRepository;
use Zoolyx\CoreBundle\Entity\Repository\GeniusCodeRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileRepository;
use Zoolyx\CoreBundle\Entity\Repository\SpeciesRepository;
use Zoolyx\CoreBundle\Entity\Species;
use Zoolyx\CoreBundle\Model\APIParser\ApiContentParser;
use Zoolyx\CoreBundle\Model\ApiParser\ApiContentTypeParser;

class GeniusController extends Controller
{

    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager")
     */
    private $entityManager;

    /**
     * @var SpeciesRepository $speciesRepository
     *
     *  @DI\Inject("zoolyx_core.repository.species")
     */
    private $speciesRepository;

    /**
     * @var GeniusCodeDescriptionRepository $geniusCodeDescriptionRepository
     *
     *  @DI\Inject("zoolyx_core.repository.genius_code_description")
     */
    private $geniusCodeDescriptionRepository;

    /**
     * @var ParameterProfileRepository $parameterProfileRepository
     *
     *  @DI\Inject("zoolyx_core.repository.parameter_profile")
     */
    private $parameterProfileRepository;

    /**
     * @var ApiContentParser $apiContentParser
     *
     *  @DI\Inject("zoolyx_core.api_parser.content")
     */
    private $apiContentParser;

    /**
     * @var GeniusCodeRepository $geniusCodeRepository
     *
     *  @DI\Inject("zoolyx_core.repository.genius_code")
     */
    private $geniusCodeRepository;


    /**
     * @param Request $request
     *
     * @return View
     *
     * @throws NotFoundHttpException when breed data could not be parsed
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Lims",
     *  description="upload breed data",
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when breed data could not be parsed",
     *  })
     */
    public function indexAction(Request $request)
    {
        file_put_contents($this->container->getParameter('log_dir_lims') .'/'.date("YmdHis",time()).'-genius.xml',$request->getContent());
        $this->apiContentParser->setRequest($request);
        /** @var ApiContentTypeParser $contentType */
        $contentType = $this->apiContentParser->getContentType();
        if (!$contentType->hasValidContentType()) {
            return View::create('{"code":400,"message":"' . $contentType->getContentTypeStatus() . '"}', 400);
        }

        if (!$this->apiContentParser->canParse()) {
            return View::create('{"code":400,"message":"could not parse"}', 400);
        }
        //validate the request
        $xsdFile = $this->container->getParameter('application_path') .'/web/bundles/zoolyxcore/xsd/import_genius.xsd';
        if (!$this->apiContentParser->isValid($xsdFile)) {
            return View::create(['errors'=>$this->apiContentParser->getErrors()], 400);
        }

        $doc = $this->apiContentParser->getDomDocument();

        /** @var DomElement $docGenius */
        $docGenius = $doc->childNodes->item(0);

        $newGeniusCodes = array();
        /** @var DomElement $docGeniusCode */
        foreach ($docGenius->getElementsByTagName('gc') as $docGeniusCode) {
            $reference = $docGeniusCode->getAttribute('id');
            $speciesId = $docGeniusCode->getAttribute('species');

            $geniusCode = $this->geniusCodeRepository->findOneBy(array('reference' => $reference));
            if (is_null($geniusCode)) {
                $geniusCode = new GeniusCode();
                $geniusCode->setReference($reference);
            }

            /** @var Species $species */
            $species = $this->speciesRepository->findOneBy(array('speciesId' => $speciesId));
            if (!$species) {
                die('species '.$speciesId . ' not found');
            }
            $geniusCode->setSpecies($species);

            /** @var DomElement $childNode */
            foreach ($docGeniusCode->childNodes as $childNode) {
                switch ($childNode->nodeName) {
                    case 'rule':
                        $rule = $childNode->nodeValue;
                        $geniusCode->setRule($rule);
                        break;
                    case 'pp':
                        $parameterProfileReference = $childNode->getAttribute('id');
                        /** @var ParameterProfile $parameterProfile */
                        $parameterProfile = $this->parameterProfileRepository->findOneBy(array('reference'=>$parameterProfileReference));
                        $geniusCode->setParameterProfile($parameterProfile);
                        break;
                    case 'descr':
                        $language = $childNode->getAttribute('lang');
                        $description = null;

                        if ($geniusCode->getId()) {
                            $description = $this->geniusCodeDescriptionRepository->findOneBy(array('geniusCode'=>$geniusCode,'language'=>$language));
                        }
                        if (!$description) {
                            $description = new GeniusCodeDescription();
                            $description->setLanguage($language);
                            $description->setGeniusCode($geniusCode);
                            $geniusCode->addDescription($description);
                        }
                        $description->setDescription($childNode->nodeValue);
                        break;
                    case 'expires':
                        $expires = $childNode->nodeValue;
                        $geniusCode->setExpires($expires);
                        break;
                }
            }

            $this->entityManager->persist($geniusCode);
            $newGeniusCodes[$geniusCode->getReference()] = 1;
        }

        $this->entityManager->flush();

        //remove old codes
        $geniusCodes = $this->geniusCodeRepository->findAll();
        /** @var GeniusCode $geniusCode */
        foreach ($geniusCodes as $geniusCode) {
            if (!isset($newGeniusCodes[$geniusCode->getReference()])) {
                $this->entityManager->remove($geniusCode);
            }
        }
        $this->entityManager->flush();

        return new Response();
    }

}
