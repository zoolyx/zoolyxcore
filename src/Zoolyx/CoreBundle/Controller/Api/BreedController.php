<?php

namespace Zoolyx\CoreBundle\Controller\Api;

use Doctrine\Common\Persistence\ObjectManager;
use DOMElement;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Zoolyx\CoreBundle\Entity\Breed;
use Zoolyx\CoreBundle\Entity\BreedDescription;
use Zoolyx\CoreBundle\Entity\Repository\BreedDescriptionRepository;
use Zoolyx\CoreBundle\Entity\Repository\BreedRepository;
use Zoolyx\CoreBundle\Entity\Repository\SpeciesDescriptionRepository;
use Zoolyx\CoreBundle\Entity\Repository\SpeciesRepository;
use Zoolyx\CoreBundle\Entity\Species;
use Zoolyx\CoreBundle\Entity\SpeciesDescription;
use Zoolyx\CoreBundle\Model\APIParser\ApiContentParser;
use Zoolyx\CoreBundle\Model\ApiParser\ApiContentTypeParser;

class BreedController extends Controller
{

    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager")
     */
    private $entityManager;

    /**
     * @var SpeciesRepository $speciesRepository
     *
     *  @DI\Inject("zoolyx_core.repository.species")
     */
    private $speciesRepository;

    /**
     * @var SpeciesDescriptionRepository $speciesDescriptionRepository
     *
     *  @DI\Inject("zoolyx_core.repository.species_description")
     */
    private $speciesDescriptionRepository;

    /**
     * @var BreedRepository $breedRepository
     *
     *  @DI\Inject("zoolyx_core.repository.breed")
     */
    private $breedRepository;

    /**
     * @var BreedDescriptionRepository $breedDescriptionRepository
     *
     *  @DI\Inject("zoolyx_core.repository.breed_description")
     */
    private $breedDescriptionRepository;

    /**
     * @var ApiContentParser $apiContentParser
     *
     *  @DI\Inject("zoolyx_core.api_parser.content")
     */
    private $apiContentParser;

    /**
     * @param Request $request
     *
     * @return View
     *
     * @throws NotFoundHttpException when breed data could not be parsed
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Lims",
     *  description="upload breed data",
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when breed data could not be parsed",
     *  })
     */
    public function indexAction(Request $request)
    {
        file_put_contents($this->container->getParameter('log_dir_lims') .'/'.date("YmdHis",time()).'-breeds.xml',$request->getContent());
        $this->apiContentParser->setRequest($request);
        /** @var ApiContentTypeParser $contentType */
        $contentType = $this->apiContentParser->getContentType();
        if (!$contentType->hasValidContentType()) {
            return View::create('{"code":400,"message":"' . $contentType->getContentTypeStatus() . '"}', 400);
        }

        if (!$this->apiContentParser->canParse()) {
            return View::create('{"code":400,"message":"could not parse"}', 400);
        }
        //validate the request
        $xsdFile = $this->container->getParameter('application_path') .'/web/bundles/zoolyxcore/xsd/import_breed.xsd';
        if (!$this->apiContentParser->isValid($xsdFile)) {
            return View::create(['errors'=>$this->apiContentParser->getErrors()], 400);
        }

        $doc = $this->apiContentParser->getDomDocument();

        /** @var DomElement $importBreedsParameters */
        $importBreedsParameters = $doc->getElementsByTagName('importBreeds')->item(0);

        /** @var DomElement $speciesParameter */
        foreach ($importBreedsParameters->getElementsByTagName('species') as $speciesParameter) {
            $speciesId = $speciesParameter->getAttribute('id');

            $species = $this->speciesRepository->findOneBy(array('speciesId' => $speciesId));
            if (!$species) {
                $species = new Species();
                $species->setSpeciesId($speciesId);
            }
            /** @var DomElement $childNode */
            foreach ($speciesParameter->childNodes as $childNode) {
                switch ($childNode->nodeName) {
                    case 'descr':
                        $language = $childNode->getAttribute('lang');
                        $description = null;

                        if ($species->getId()) {
                            $description = $this->speciesDescriptionRepository->findOneBy(array('species'=>$species,'language'=>$language));
                        }
                        if (!$description) {
                            $description = new SpeciesDescription();
                            $description->setLanguage($language);
                            $description->setSpecies($species);
                            $species->addDescription($description);
                        }
                        $description->setDescription($childNode->nodeValue);

                        break;
                    case 'breeds':
                        $breedsDocument = $childNode;
                        /** @var DomElement $breedNode */
                        foreach ($breedsDocument->getElementsByTagName('breed') as $breedNode) {
                            $breedId = $breedNode->getAttribute('id');

                            //temporary fix
                            $breedId = $speciesId . "_" . $breedId;

                            $breed = $this->breedRepository->findOneBy(array('breedId' => $breedId));
                            if (!$breed) {
                                $breed = new Breed();
                                $breed->setBreedId($breedId);
                            }

                            $breed->setSpecies($species);
                            $this->entityManager->persist($breed);

                            /** @var DomElement $languageNode */
                            foreach ($breedNode->getElementsByTagName('descr') as $languageNode) {
                                $language = $languageNode->getAttribute('lang');
                                $description = null;

                                $description = $this->breedDescriptionRepository->findOneBy(array('breed'=>$breed,'language'=>$language));

                                if (!$description) {
                                    $description = new BreedDescription();
                                    $description->setLanguage($language);
                                    $description->setBreed($breed);
                                    $breed->addDescription($description);
                                }
                                $description->setDescription($languageNode->nodeValue);
                            }
                        }

                        break;
                }
            }
            $this->entityManager->persist($species);
        }
        $this->entityManager->flush();

        return new Response();
    }

}
