<?php

namespace Zoolyx\CoreBundle\Controller\Request;


use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Knp\Bundle\SnappyBundle\Snappy\LoggableGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Zoolyx\CoreBundle\Entity\ParameterProfile;
use Zoolyx\CoreBundle\Entity\ParameterProfilePrice;
use Zoolyx\CoreBundle\Entity\ParameterProfileSampleTypeDefinition;
use Zoolyx\CoreBundle\Entity\ReportVersion;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileRepository;
use Zoolyx\CoreBundle\Entity\Repository\ParameterProfileSampleTypeDefinitionRepository;
use Zoolyx\CoreBundle\Entity\Repository\SampleRepository;
use Zoolyx\CoreBundle\Entity\Sample;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Forms\Request\FormData;
use Zoolyx\CoreBundle\Forms\Request\FormDataTypeProvider;
use Zoolyx\CoreBundle\Model\File\File;
use Zoolyx\CoreBundle\Model\File\FileConverter;
use Zoolyx\CoreBundle\Model\Ftp\FtpService;
use Zoolyx\CoreBundle\Model\Lims\BreedManager;
use Zoolyx\CoreBundle\Model\Lims\ParameterProfileManager;
use Zoolyx\CoreBundle\Model\Lims\ParameterProfile as LimsParameterProfile;
use Zoolyx\CoreBundle\Model\Report\Lister\ParameterLister;
use Zoolyx\CoreBundle\Model\Report\Lister\ParameterProfileLister;
use Zoolyx\CoreBundle\Model\Report\ReportBuilder;
use Zoolyx\CoreBundle\Model\Report\ReportHash;
use Zoolyx\CoreBundle\Model\Report\ReportLoader;
use Zoolyx\CoreBundle\Model\Request\Object\Mapper\RequestMapper;
use Zoolyx\CoreBundle\Model\Request\Object\RequestForm;
use Zoolyx\CoreBundle\Model\Request\Object\RequestProfile;
use Zoolyx\CoreBundle\Model\Request\Object\RequestSampleType;
use Zoolyx\CoreBundle\Model\Request\RequestData;
use Zoolyx\CoreBundle\Model\Request\RequestManager;
use Zoolyx\CoreBundle\Model\Request\RequestRouter;
use Zoolyx\CoreBundle\Model\XmlBuilder\ServiceBuilder\AdditionalRequestServiceBuilder;
use Zoolyx\CoreBundle\Model\XmlBuilder\ServiceBuilder\NewRequestServiceBuilder;
use Zoolyx\CoreBundle\Model\XmlParser\XmlValidator;
use Zoolyx\ShopBundle\Entity\Cart;
use Zoolyx\ShopBundle\Entity\CartLine;
use Zoolyx\ShopBundle\Entity\Product;
use Zoolyx\ShopBundle\Entity\Repository\ProductRepository;

class RequestController extends Controller
{
    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var Translator
     *
     * @DI\Inject("translator")
     */
    private $translator;

    /**
     * @var ReportLoader $reportLoader
     *
     * @DI\Inject("zoolyx_core.report.loader")
     */
    private $reportLoader;

    /**
     * @var ReportBuilder $reportBuilder
     *
     * @DI\Inject("zoolyx_core.report.builder")
     */
    private $reportBuilder;

    /**
     * @var ParameterProfileManager $parameterProfileManager
     *
     * @DI\Inject("zoolyx_core.manager.parameter_profile")
     */
    private $parameterProfileManager;

    /**
     * @var ParameterLister $parameterLister
     *
     * @DI\Inject("zoolyx_core.report.parameter_lister")
     */
    private $parameterLister;

    /**
     * @var ParameterProfileLister $parameterProfileLister
     *
     * @DI\Inject("zoolyx_core.report.parameter_profile_lister")
     */
    private $parameterProfileLister;

    /**
     * @var BreedManager $breedManager
     *
     * @DI\Inject("zoolyx_core.manager.breed")
     */
    private $breedManager;

    /**
     * @var RequestMapper $requestMapper
     *
     * @DI\Inject("zoolyx_core.mapper.request")
     */
    private $requestMapper;

    /**
     * @var FormDataTypeProvider $formDataTypeProvider
     *
     * @DI\Inject("zoolyx_core.provider.form_data_type")
     */
    private $formDataTypeProvider;

    /**
     * @var ParameterProfileSampleTypeDefinitionRepository $sampleTypeDefinitionRepository
     *
     * @DI\Inject("zoolyx_core.repository.parameter_profile_sample_type_definition")
     */
    private $sampleTypeDefinitionRepository;

    /**
     * @var FtpService $ftpService
     *
     *  @DI\Inject("zoolyx_core.ftp.service")
     */
    private $ftpService;

    /**
     * @var RequestManager $requestManager
     *
     *  @DI\Inject("zoolyx_core.manager.request")
     */
    private $requestManager;

    /**
     * @var RequestRouter $requestRouter
     *
     *  @DI\Inject("zoolyx_core.router.request")
     */
    private $requestRouter;

    /**
     * @var  XmlValidator $xmlValidator
     *
     * @DI\Inject("zoolyx_core.xml_parser.xml_validator")
     */
    private $xmlValidator;

    /**
     * @var ProductRepository
     *
     * @DI\Inject("zoolyx_shop.repository.product")
     */
    private $productRepository;

    /**
     * @var ParameterProfileRepository
     *
     * @DI\Inject("zoolyx_core.repository.parameter_profile")
     */
    private $parameterProfileRepository;

    /**
     * @var SampleRepository
     *
     * @DI\Inject("zoolyx_core.repository.sample")
     */
    private $sampleRepository;

    /**
     * @var FormData
     *
     * @DI\Inject("zoolyx_core.form.form_data")
     */
    private $formData;

    /**
     * @var LoggableGenerator $snappy
     *
     * @DI\Inject("knp_snappy.pdf")
     */
    private $snappy;

    /**
     * @var FileConverter $fileConverter
     *
     * @DI\Inject("zoolyx_core.file.converter")
     */
    private $fileConverter;

    public function requestAction(Request $request, $step=null)
    {
        $requestData = $this->requestManager->getRequestData();
        $this->requestRouter->setRequestData($requestData);
        if (!is_null($step)) $this->requestRouter->setStep($step);

        switch ($this->requestRouter->getStep($step)) {
            case 0:
                $form = $this->createForm($this->formDataTypeProvider->provide($requestData), $this->requestMapper->mapRequestToForm($requestData));
                return $this->render('ZoolyxCoreBundle:Request:step0/sample_form.html.twig', array(
                    'router' => $this->requestRouter,
                    'requestData' => $requestData,
                    'form' => $form->createView(),
                ));
            case 1:
                /** @var ReportVersion $reportVersion */
                $reportVersion = $this->reportLoader->loadByHash(new ReportHash($requestData->getHash()));
                $this->reportBuilder->setReportVersion($reportVersion);
                $this->reportBuilder->setLanguage($request->getLocale());

                return $this->render('ZoolyxCoreBundle:Request:step1/select_sample.html.twig', array(
                    'router' => $this->requestRouter,
                    'requestData' => $requestData,
                    'report' => $this->reportBuilder,
                    'samples' => $reportVersion->getPhysicalSamples()
                ));
            case 2:
                $sample = $requestData->getSample();
                $speciesCode = $requestData->getSample()->getPet()->getSpeciesCode();

                $billing = null;
                $language = $request->getLocale();

                $this->parameterProfileManager->setLanguage($language)->setSpeciesCode($speciesCode)->setBilling($billing);
                $categories = $this->parameterProfileManager->getParameterProfileCategories();

                $this->breedManager->setLanguage($language)->setSpeciesCode($speciesCode);
                $breeds = $this->breedManager->getBreeds();

                /** @var Sample $reportSample */
                $reportSample = $sample->getId() ? $this->sampleRepository->find($sample->getId()) : null;
                $parametersInSample = $this->parameterLister->getParameters($reportSample);
                $parameterProfilesInSample = $this->parameterProfileLister->getParameterProfiles($parametersInSample);

                $selectedProfiles = $requestData->getProfiles();

                return $this->render('ZoolyxCoreBundle:Request:step2/profile_list.html.twig', array(
                    'router' => $this->requestRouter,
                    'language' => $language,
                    'categories' => $categories,
                    'breeds' => $breeds,
                    'parametersInSample' => $parametersInSample,
                    'parameterProfilesInSample' => $parameterProfilesInSample,
                    'selectedProfiles' => $selectedProfiles,
                    'report' => $this->reportBuilder
                ));
            case 3:
                /** @var Sample $sample */
                $sample = $this->requestManager->getRequestData()->getSample();

                $profileReferences = $this->requestManager->getRequestData()->getProfiles();

                $speciesCode = $sample->getPet()->getSpeciesCode();

                $billing = null;
                $language = $request->getLocale();

                $this->parameterProfileManager->setLanguage($language)->setSpeciesCode($speciesCode)->setBilling($billing);

                $parameterProfiles = $this->parameterProfileManager->getParameterProfiles($profileReferences);
                $totalValue = 0;
                $parameterProfileReferences = array();
                /** @var LimsParameterProfile $profile */
                foreach ($parameterProfiles as $profile) {
                    $totalValue+= floatval(str_replace(',', '.',$profile->getPrice()->getValue()));
                    $parameterProfileReferences[] = $profile->getReference();
                }

                $this->breedManager->setLanguage($language)->setSpeciesCode($speciesCode);
                $breeds = $this->breedManager->getBreeds();

                return $this->render('ZoolyxCoreBundle:Request:step3/sample_type_list.html.twig', array(
                    'language' => $language,
                    'router' => $this->requestRouter,
                    'report' => $this->reportBuilder,
                    'parameterProfiles' => $parameterProfiles,
                    'parameterProfileReferencesString' => implode(",",$parameterProfileReferences),
                    'total' => $totalValue,
                    'breeds' => $breeds,
                    'currentBreed' => $sample->getPet()->getBreedCode()
                ));
            case 4:
                $profileReferences = $this->requestManager->getRequestData()->getProfiles();

                if (count($profileReferences) == 0) {
                    //no profile selected
                }

                /** @var Sample $sample */
                $sample = $this->requestManager->getRequestData()->getSample();
                $speciesCode = $sample->getPet()->getSpeciesCode();

                $billing = null;
                $language = $this->getUser()->getLanguage();

                $this->parameterProfileManager->setLanguage($language)->setSpeciesCode($speciesCode)->setBilling($billing);

                $parameterProfiles = $this->parameterProfileManager->getParameterProfiles($profileReferences);
                $totalValue = 0;
                $parameterProfileReferences = array();
                $postData = array();
                /** @var LimsParameterProfile $profile */
                foreach ($parameterProfiles as $profile) {
                    $totalValue+= floatval(str_replace(',', '.',$profile->getPrice()->getValue()));
                    $parameterProfileReferences[] = $profile->getReference();
                    $postData[] = array(
                        "id" => $profile->getReference(),
                    );
                }

                //check if the user has to pay
                $profilesToPay = array();
                /** @var User $user */
                $user = $this->getUser();
                if ($user->hasRole('ROLE_VETERINARY')) {
                    /** @var LimsParameterProfile $profile */
                    foreach ($parameterProfiles as $profile) {
                        if ($profile->getEntity()->veterinaryMustPay()) {
                            $profilesToPay[] = $profile;
                        }
                    }
                }
                elseif ($user->hasRole('ROLE_OWNER')) {
                    /** @var LimsParameterProfile $profile */
                    foreach ($parameterProfiles as $profile) {
                        if ($profile->getEntity()->ownerMustPay()) {
                            $profilesToPay[] = $profile;
                        }
                    }
                }
                $needToPay = count($profilesToPay) ? true : false;

                if ($needToPay) {
                    $this->requestManager->getRequestData()->setProfilesToPay($profilesToPay);
                    $this->requestManager->saveToSession();
                }

                return $this->render('ZoolyxCoreBundle:Request:step4/confirm.html.twig', array(
                    'router' => $this->requestRouter,
                    'needToPay' => $needToPay,
                    'postData' => $postData,
                    'report' => $this->reportBuilder,
                    'sample' => $sample,
                    'parameterProfiles' => $parameterProfiles,
                    'parameterProfileReferencesString' => implode(",",$parameterProfileReferences),
                    'total' => $totalValue,
                ));
            case 5:
                $requestData = $this->requestManager->getRequestData();

                return $this->render('ZoolyxCoreBundle:Request:step5/complete.html.twig', array(
                    'router' => $this->requestRouter,
                    'additionalRequest' => $requestData->isAdditionalRequest(),
                    'hash' => $requestData->getHash()
                ));

        }

        return $this->render('ZoolyxCoreBundle:Request:step1/sample_form.html.twig', array());
    }

    public function newRequestAction()
    {
        $this->requestManager->init($this->getUser(), array());

        return $this->redirectToRoute('request');
    }
    public function postRequestAction(Request $request)
    {
        if (is_null($request) && $request->getMethod() != 'POST') {
            die("wrong request");
        }

        $xml = $this->getXml($request);

        $xsdFile = $this->container->getParameter('application_path') .'/web/bundles/zoolyxcore/xsd/request_pms.xsd';
        $this->xmlValidator->setXml($xml);
        if (!$this->xmlValidator->validate($xsdFile)) {
            foreach ($this->xmlValidator->getErrors() as $error) {
                foreach ($error as $code => $description) {
                    echo "$code: $description<br>";
                }
            }
            die();
        }

        $xml = simplexml_load_string($xml);

        $this->requestManager->init($this->getUser(), array(
            RequestManager::OPTIONS_POST=>$xml,
            RequestManager::OPTIONS_PREPEND_PRACTICE_ID_TO_EXT_ID=>true
        ));

        return $this->redirectToRoute('request');
    }
    public function additionalRequestAction(Request $request, $hash, $sampleId = null)
    {
        /** @var ReportVersion $reportVersion */
        $reportVersion = $this->reportLoader->loadByHash(new ReportHash($hash));

        $this->requestManager->init($this->getUser(), array(
            RequestManager::OPTIONS_IS_ADDITIONAL=>true,
            RequestManager::OPTIONS_REPORT_VERSION=>$reportVersion,
            RequestManager::OPTIONS_SAMPLE_ID=>$sampleId,
            RequestManager::OPTIONS_LANGUAGE=>$request->getLocale()
        ));

        if (!is_null($this->requestManager->getRequestData()->getSample()->getId())) {
            return $this->redirectToRoute('request_step', array('step' => 2));
        }

        return $this->redirectToRoute('request');
    }
    public function suggestionAction($hash, Request $request) {
        $sampleId = $request->request->get('sample');
        $profilesString = $request->request->get('profiles');
        $profileReferences = $profilesString == '' ? array() : explode(",",$profilesString);
        
		$profiles = array();
        foreach ($profileReferences as $profileReference) {
            $requestProfile = new RequestProfile();
            $requestProfile->setCode($profileReference);
            /** @var ParameterProfile $parameterProfile */
            $parameterProfile = $this->parameterProfileRepository->findOneBy(array('reference'=>$profileReference));
            if ($parameterProfile) {

                $requestProfile->setDescription($parameterProfile->getDescription($this->translator->getLocale())->getDescription());

                $priceList = $parameterProfile->getPriceList();
                $billing = null;
                /** @var ParameterProfilePrice $price */
                foreach ($priceList as $price) {
                    if ($price->getBilling() == $billing) {
                        $requestProfile->setPrice($price->getValue());
                    }
                }
                if (is_null($requestProfile->getPrice())) {
                    foreach ($priceList as $price) {
                        if ($price->getBilling() == null) {
                            $requestProfile->setPrice($price->getValue());
                        }
                    }
                }
            }
            $profiles[] = $requestProfile;
        }


        /** @var ReportVersion $reportVersion */
        $reportVersion = $this->reportLoader->loadByHash(new ReportHash($hash));

        $this->requestManager->init($this->getUser(), array(
            RequestManager::OPTIONS_IS_ADDITIONAL=>true,
            RequestManager::OPTIONS_REPORT_VERSION=>$reportVersion,
            RequestManager::OPTIONS_SAMPLE_ID=>$sampleId
        ));

        $this->requestManager
            ->getRequestData()
            ->setProfiles($profiles);

        $this->requestManager->saveToSession();

        if (!is_null($this->requestManager->getRequestData()->getSample()->getId())) {
            return $this->redirectToRoute('request_step', array('step' => 2));
        }
        return $this->redirectToRoute('request');
    }

    public function processPetFormAction(Request $request)
    {
        $speciesCode = $request->request->get('FormData')['speciesCode'];

        $requestData = $this->requestManager->getRequestData();
        if ($requestData->isAdministrator()) {
            $limsId = $request->request->get('FormData')['limsId'];
            $candidateVeterinaryPractices = $requestData->getCandidateVeterinaryPractices();
            $candidateVeterinaryPractices['new'] = $limsId;
            $requestData->setCandidateVeterinaryPractices($candidateVeterinaryPractices);
        }

        $form = $this->createForm($this->formDataTypeProvider->provide($this->requestManager->getRequestData(),$speciesCode), $this->formData);
        if (!is_null($request)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->requestManager->updateFromForm($this->formData);
                return $this->redirectToRoute('request');
            }
        };

        $requestData = $this->requestManager->getRequestData();
        $this->requestRouter->setRequestData($requestData);
        return $this->render('ZoolyxCoreBundle:Request:step0/sample_form.html.twig', array(
            'router' => $this->requestRouter,
            'requestData' => $requestData,
            'form' => $form->createView(),
        ));
    }
    public function processProfilesAction(Request $request)
    {
        if (is_null($request) && $request->getMethod() == 'POST') {
            //something is wrong
        }

        $profilesString = $request->request->get('profiles','');
        $profileReferences = $profilesString == '' ? array() : explode(",",$profilesString);

        $profiles = array();
        foreach ($profileReferences as $profileReference) {
            $requestProfile = new RequestProfile();
            $requestProfile->setCode($profileReference);
            /** @var ParameterProfile $parameterProfile */
            $parameterProfile = $this->parameterProfileRepository->findOneBy(array('reference'=>$profileReference));
            if ($parameterProfile) {

                $requestProfile->setDescription($parameterProfile->getDescription($this->translator->getLocale())->getDescription());

                $priceList = $parameterProfile->getPriceList();
                $billing = null;
                /** @var ParameterProfilePrice $price */
                foreach ($priceList as $price) {
                    if ($price->getBilling() == $billing) {
                        $requestProfile->setPrice($price->getValue());
                    }
                }
                if (is_null($requestProfile->getPrice())) {
                    foreach ($priceList as $price) {
                        if ($price->getBilling() == null) {
                            $requestProfile->setPrice($price->getValue());
                        }
                    }
                }
            }
            $profiles[] = $requestProfile;
        }
        $this->requestManager->getRequestData()->setProfiles($profiles);
        $this->requestManager->saveToSession();

        return $this->redirectToRoute('request');
    }
    public function processSampleTypesAction(Request $request)
    {
        $requestData = $this->requestManager->getRequestData();
        if (is_null($request) && $request->getMethod() == 'POST') {
            //something is wrong
        }
        $profiles = $request->request->get('profiles');
        foreach(explode(",",$profiles) as $profile) {
            $sampleTypeReferences = array();
            $sampleTypeReferencesRequested = array();
            $sampleTypesArray = json_decode($request->request->get('sampleTypes_'.$profile));
            if ($sampleTypesArray) {
                foreach ($sampleTypesArray as $sampleType) {
                    if ($sampleType->name == 'sampleTypes[' . $profile . ']' || $sampleType->name == 'sampleTypes[' . $profile . '][]') {
                        $sampleTypeValue = $sampleType->value;
                        if (!isset($sampleTypeReferences[$sampleTypeValue])) {
                            $sampleTypeReferences[$sampleTypeValue] = 1;
                        }
                    }
                    if ($sampleType->name == 'sampleTypesRequested[' . $profile . '][]') {
                        $sampleTypeValue = $sampleType->value;
                        if (isset($sampleTypeReferencesRequested[$sampleTypeValue])) {
                            $sampleTypeReferencesRequested[$sampleTypeValue]++;
                        } else {
                            $sampleTypeReferencesRequested[$sampleTypeValue] = 1;
                        }
                    }
                }
            }
            $sampleTypes = array();
            foreach ($sampleTypeReferences as $sampleTypeReference => $dummy) {
                /** @var ParameterProfileSampleTypeDefinition $sampleTypeDefinition */
                $sampleTypeDefinition = $this->sampleTypeDefinitionRepository->findOneBy(array('reference'=>$sampleTypeReference));
                if ($sampleTypeDefinition) {
                    $requestSampleType = new RequestSampleType();
                    $requestSampleType
                        ->setReference($sampleTypeReference)
                        ->setDescription($sampleTypeDefinition->getDescription($this->translator->getLocale())->getDescription())
                        ->setImageName($sampleTypeDefinition->getImageName())
                    ;
                    $sampleTypes[] = $requestSampleType;
                }
            }
            $requestData->setSampleTypes($profile,$sampleTypes);
        }
        $forms = $request->request->get('forms');
        $forms = $forms == '' ? array() : explode(',',$forms);
        foreach ($forms as $formName) {
            $requestForm = $requestData->getRequestForm($formName);

            $formData = $request->request->get($formName);
            $formArray = json_decode($formData, true);
            $formData = array();
            foreach ($formArray as $formElement) {
                $formData[$formElement['name']] = $formElement['value'];
            }
            //$picIndexes = array('pe.pic','sc.pic1','sc.pic2','sc.pic3', 'did1.dnafp', 'sid1.dnafp', 'sid2.dnafp', 'sid3.dnafp');
            $picIndexes = $formData["files"];
            //var_dump($picIndexes); die();
            foreach ($picIndexes as $picIndex) {
                if (isset($formData[$picIndex]) && is_uploaded_file($_FILES[$formData[$picIndex]]['tmp_name'])) {
                    $extension = pathinfo($_FILES[$formData[$picIndex]]["name"], PATHINFO_EXTENSION);
                    $targetFilename = 'form_'.$requestData->getHash().'_'.$formData[$picIndex] .".". $extension;
                    $target = $this->container->getParameter('log_dir_lims') .'/'. $targetFilename;
                    if (move_uploaded_file($_FILES[$formData[$picIndex]]["tmp_name"], $target)) {
                        $formData[$picIndex] = $targetFilename;
                        $requestForm->addFile($targetFilename);
                    }	
                } else {
                    $formData[$picIndex] = '';
                }
            }
            unset($formData["files"]);

            $requestForm->setValues($formData);
        }


        $this->requestManager->saveToSession();

        return $this->redirectToRoute('request');
    }
    public function confirmAction()
    {
        /** @var RequestData $requestData */
        $requestData = $this->requestManager->getRequestData();
        file_put_contents($this->container->getParameter('log_dir_lims') .'/service_request'.time().'.xml',serialize($requestData));

        if (count($requestData->getProfilesToPay()))
        {
            //we need to go to the shop first
            //return $this->redirectToRoute('shop_cart_add_profile');
        }

        //send a service request to lims
        if ($requestData->isAdditionalRequest()) {
            $requestServiceBuilder = new AdditionalRequestServiceBuilder();
        } else {
            $requestServiceBuilder = new NewRequestServiceBuilder();
        }

        $requestServiceBuilder
            ->setRequestId($requestData->getReportRequestId())
            ->setRequestBy($requestData->isAdministrator() ? $requestData->getAdministratorEmail() : "")
            ->setPracticeReference($requestData->getPracticeReference())
            ->setVeterinary($requestData->getVeterinary())
            ->setOwner($requestData->getOwner())
            ->setSample($requestData->getSample())
            ->setFacturation($requestData->getFacturation());

        $service = $requestServiceBuilder->getDoc();

        $tempFile = fopen('php://memory', 'r+');
        fputs($tempFile, $service->saveXML());
        rewind($tempFile);
        $this->ftpService->put("request_".time().'.xml', $tempFile);

        //handle the uploaded files
        /** @var RequestForm $requestForm */
        foreach ($requestData->getSample()->getForms() as $requestForm) {
            foreach ($requestForm->getFiles() as $filename) {
                $content = file_get_contents($this->container->getParameter('log_dir_lims') .'/'. $filename);
                $file = new File();
                $file
                    ->setContent($content)
                    ->setName($filename)
                    ->setBase64Encoded(false);

                $tempFile = $this->fileConverter->convert($file);
                $this->ftpService->put($file->getName(), $tempFile);
            }

        }

        $this->requestManager->getRequestData()->setIsSent(true);
        $this->requestManager->saveToSession();
        return $this->redirectToRoute('request');
    }

    public function documentAction() {
        //var_dump($this->requestManager->getRequestData()->getSample()); die();
        $html = $this->renderView('ZoolyxCoreBundle:Request/document:content.html.twig', array(
            'requestData' => $this->requestManager->getRequestData()
        ));
        $htmlHeader =  $this->renderView('ZoolyxCoreBundle:Request/document:header.html.twig', array(
        ));
        $htmlFooter =  $this->renderView('ZoolyxCoreBundle:Request/document:footer.html.twig');

        return new Response($this->snappy->getOutputFromHtml($html,array(
                'orientation'=>'Portrait',
                'header-spacing' => 5,
                'header-html' => $htmlHeader,
                'footer-spacing' => 10,
                'footer-html' => $htmlFooter
            )), 200, array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="request.pdf"'
            )
        );
    }

    public function confirmToShopTempAction(Request $request) {
        /** @var User $user */
        $user = $this->getUser();
        $cart = $user->getCart();
        if (is_null($cart)) {
            $cart = new Cart();
            $this->entityManager->persist($cart);
            $user->setCart($cart);
        }

        $requestData = $this->requestManager->getRequestData();
        $profiles = $requestData->getProfilesToPay();

        $sampleTypes = $requestData->getSampleTypes();
        $sampleTypesRequested = $sampleTypes["requested"];

        /** @var LimsParameterProfile $profile */
        foreach ($profiles as $profile) {
            /** @var ParameterProfile $parameterProfile */
            $parameterProfile = $this->parameterProfileRepository->find($profile->getEntity()->getId());
            $cartLine = new CartLine();
            $cartLine
                ->setCart($cart)
                ->setIsAnalyse(true)
                ->setParameterProfile($parameterProfile)
                ->setItems(1)
                ->setPrice($profile->getPrice()->getValue()*100);
            $this->entityManager->persist($cartLine);
        }

        //add the requested sampleTypes
        foreach ($sampleTypesRequested as $sampleType => $amount) {
            /** @var Product $product */
            $product = $this->productRepository->findOneBy(array('reference'=>$sampleType));
            $cartLine = new CartLine();
            $cartLine
                ->setCart($cart)
                ->setIsAnalyse(false)
                ->setProduct($product)
                ->setItems($amount)
                ->setPrice(0);
            $this->entityManager->persist($cartLine);
        }

        if (count($sampleTypesRequested)) {
            /** @var Product $product */
            $product = $this->productRepository->findOneBy(array('reference'=>'DELIVERY'));
            $cartLine = new CartLine();
            $cartLine
                ->setCart($cart)
                ->setIsAnalyse(false)
                ->setProduct($product)
                ->setItems(1)
                ->setPrice($product->getPrice());
            $this->entityManager->persist($cartLine);
        }

        $this->entityManager->flush();

        return $this->redirectToRoute('shop_cart');
    }

    public function sendAction()
    {
        $requestData = $this->requestManager->getRequestData();

        $additionalRequestServiceBuilder = new AdditionalRequestServiceBuilder();
        $additionalRequestServiceBuilder
            ->setVeterinary($requestData->getVeterinary())
            ->setOwner($requestData->getOwner())
            ->setSample($requestData->getSample());

        $service = $additionalRequestServiceBuilder->getDoc();

        $tempFile = fopen('php://memory', 'r+');
        fputs($tempFile, $service->saveXML());
        rewind($tempFile);
        $this->ftpService->put("request_".time().'.xml', $tempFile);

        //handle the uploaded files
        /** @var RequestForm $requestForm */
        foreach ($requestData->getSample()->getForms() as $requestForm) {
            foreach ($requestForm->getFiles() as $filename) {
                $tempFile = fopen('php://memory', 'r+');
                $content = file_get_contents($this->container->getParameter('log_dir_lims') .'/'. $filename);
                $contentLength = strlen($content);
                fputs($tempFile, $content, $contentLength);
                rewind($tempFile);

                $this->ftpService->put($filename, $tempFile);
            }
        }


        return $this->redirectToRoute('zoolyx_report_additional_request_select_complete',
            array(
                "hash" => $requestData->getHash(),
                "sampleId" => $requestData->getSample()->getId()
            ));
    }

    private function getXml(Request $request) {
        libxml_use_internal_errors(true);
        $xml = $request->getContent();
        $doc = simplexml_load_string($xml);
        if ($doc !== false) {
            return $xml;
        }
        libxml_clear_errors();

        $xml = $request->request->get('xml');
        $doc = simplexml_load_string($xml);
        if ($doc !== false) {
            return $xml;
        }libxml_clear_errors();


        $data = array();
        foreach($request->request->all() as $parameter => $value) {
            $p = explode("_",$parameter);
            if (count($p) == 3) {
                $data[$p[1]][$p[2]] = $value;
            }
            if (count($p) == 2) {
                $data[$p[1]] = $value;
            }
        }

        $xml_data = new \SimpleXMLElement('<?xml version="1.0"?><rq></rq>');
        $this->array_to_xml($data,$xml_data);

        $xml = $xml_data->asXML();

        return $xml;
    }
    private function array_to_xml( $data, &$xml_data ) {
        foreach( $data as $key => $value ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            if( is_array($value) ) {
                $subnode = $xml_data->addChild($key);
                $this->array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }

}
