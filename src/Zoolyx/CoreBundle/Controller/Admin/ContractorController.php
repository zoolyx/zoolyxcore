<?php

namespace Zoolyx\CoreBundle\Controller\Admin;


use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Exception\NotValidCurrentPageException;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zoolyx\CoreBundle\Entity\Contractor;
use Zoolyx\CoreBundle\Entity\Repository\ContractorRepository;

class ContractorController extends Controller
{


    /**
     * @var ContractorRepository
     *
     * @DI\Inject("zoolyx_core.repository.contractor")
     */
    private $contractorRepository;

    public function indexAction(Request $request)
    {
        $query = $this->contractorRepository->findAllQuery();

        $adapter = new DoctrineORMAdapter($query);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(15);
        $page = $request->query->get('page', 1);
        try  {
            $pager->setCurrentPage($page);
        }
        catch(NotValidCurrentPageException $e) {
            throw new NotFoundHttpException('Illegal page');
        }

        return $this->render('ZoolyxCoreBundle:Admin:Contractor/Contractors.html.twig',array(
            'pager' => $pager
        ));
    }

    public function contractorAction(Request $request, $contractorId) {
        /** @var Contractor $contractor */
        $contractor = $this->contractorRepository->find($contractorId);
        if (!$contractor) {
            throw $this->createNotFoundException(
                'No contractor found for id ' . $contractorId
            );
        }

        //die($request->request->get('username'));

        return $this->render('ZoolyxCoreBundle:Admin:Contractor/Contractor.html.twig',array(
            'contractor' => $contractor
        ));
    }

}
