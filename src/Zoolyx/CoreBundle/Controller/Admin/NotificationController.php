<?php

namespace Zoolyx\CoreBundle\Controller\Admin;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zoolyx\CoreBundle\Entity\NotificationEndpoint;
use Zoolyx\CoreBundle\Entity\Repository\NotificationEndpointRepository;

class NotificationController extends Controller
{
    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;
    /**
     * @var NotificationEndpointRepository
     *
     * @DI\Inject("zoolyx_core.repository.notification_endpoint")
     */
    private $notificationEndpointRepository;


    public function indexAction()
    {
        $notificationEndpoints = $this->notificationEndpointRepository->findAll();

        return $this->render('ZoolyxCoreBundle:Admin:NotificationEndpoints/index.html.twig',array(
            'notificationEndpoints' => $notificationEndpoints
        ));
    }

    public function notificationAction($id, Request $request) {
        /** @var NotificationEndpoint $notificationEndpoint */
        if ($id) {
            $notificationEndpoint = $this->notificationEndpointRepository->find($id);
            if (!$notificationEndpoint) {
                throw $this->createNotFoundException(
                    'No notification endpoint found for id ' . $id
                );
            }
        } else {
            $notificationEndpoint = new NotificationEndpoint();
        }


        if ($request->isMethod('POST')) {
            $name = $request->get('name');
            $url = $request->get('url');
            $subscriptionUrl = $request->get('subscriptionUrl');
            echo $url;
            $notificationEndpoint->setName($name);
            $notificationEndpoint->setUrl($url);
            $notificationEndpoint->setSubscriptionUrl($subscriptionUrl);
            $this->entityManager->persist($notificationEndpoint);
            $this->entityManager->flush();
        }


        return $this->render('ZoolyxCoreBundle:Admin:NotificationEndpoints/notification_endpoint.html.twig',array(
            'notificationEndpoint' => $notificationEndpoint
        ));
    }

}
