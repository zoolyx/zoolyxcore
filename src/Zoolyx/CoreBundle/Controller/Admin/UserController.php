<?php

namespace Zoolyx\CoreBundle\Controller\Admin;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Exception\NotValidCurrentPageException;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zoolyx\CoreBundle\Entity\Repository\UserRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;

class UserController extends Controller
{
    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var UserRepository
     *
     * @DI\Inject("zoolyx_core.repository.user")
     */
    private $userRepository;

    /**
     * @var VeterinaryRepository
     *
     * @DI\Inject("zoolyx_core.repository.veterinary")
     */
    private $veterinaryRepository;


    public function indexAction(Request $request)
    {
        $keyword = $request->query->get('key','');
        if ($keyword == '') {
            $query = $this->userRepository->findAllQuery();
        } else {
            $query = $this->userRepository->searchForQuery($keyword);
        }

        $adapter = new DoctrineORMAdapter($query);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(15);
        $page = $request->query->get('page', 1);
        try  {
            $pager->setCurrentPage($page);
        }
        catch(NotValidCurrentPageException $e) {
            throw new NotFoundHttpException('Illegal page');
        }

        return $this->render('ZoolyxCoreBundle:Admin:User/Users.html.twig',array(
            'keyword' => $keyword,
            'pager' => $pager
        ));
    }

    public function userAction(Request $request, $userId) {
        /** @var User $user */
        $user = $this->userRepository->find($userId);
        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id ' . $userId
            );
        }

        $veterinary = null;
        if ($user->hasRole("ROLE_VETERINARY") || $user->hasRole("ROLE_ORGANISATION")) {
            $veterinary = $this->veterinaryRepository->findOneBy(array('account'=>$user->getId()));
        }

        return $this->render('ZoolyxCoreBundle:Admin:User/User.html.twig',array(
            'user' => $user,
            'veterinary' => $veterinary,
            'roles' => $this->container->getParameter('security.role_hierarchy.roles'),
            'page' => $request->query->get('page', 1)
        ));
    }

    public function editVeterinaryAction($veterinaryId, Request $request) {
        /** @var Veterinary $veterinary */
        $veterinary = $this->veterinaryRepository->find($veterinaryId);

        $form = $this->createFormBuilder()
            ->add('fa', 'text',array(
                'label' => 'Number of Fa',
                'data' => $veterinary->getFa(),
                'attr'=>array('placeholder'=>'fa id')
            ))
            ->add('submit', 'submit')
            ->getForm();

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $data = $form->getData();
            $veterinary->setFa($data['fa']);
            $this->entityManager->persist($veterinary);
            $this->entityManager->flush();

            return $this->redirectToRoute('admin_user', array(
                'userId' => $veterinary->getAccount()->getId()
            ));
        }

        return $this->render('ZoolyxCoreBundle:Admin:User/EditVeterinary.html.twig', array(
            'veterinary' => $veterinary,
            'form' => $form->createView()
        ));
    }

    public function setRoleAction($userId, $role) {
        /** @var User $user */
        $user = $this->userRepository->find($userId);
        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id ' . $userId
            );
        }

        $user->addRole($role);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->redirect($this->generateUrl('admin_user', array('userId'=>$userId)));
    }

    public function takeRoleAction($userId, $role) {
        /** @var User $user */
        $user = $this->userRepository->find($userId);
        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id ' . $userId
            );
        }

        $user->removeRole($role);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->redirect($this->generateUrl('admin_user', array('userId'=>$userId)));
    }
}
