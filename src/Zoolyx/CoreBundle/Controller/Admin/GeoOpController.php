<?php

namespace Zoolyx\CoreBundle\Controller\Admin;

use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Exception\NotValidCurrentPageException;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zoolyx\CoreBundle\Entity\Repository\GeoOpLogRepository;

class GeoOpController extends Controller
{

    /**
     * @var GeoOpLogRepository
     *
     * @DI\Inject("zoolyx_core.repository.geo_op_log")
     */
    private $geoOpLogRepository;


    public function indexAction(Request $request)
    {
        $keyword = $request->query->get('key','');
        if ($keyword == '') {
        	$query = $this->geoOpLogRepository->findAllQuery();
        } else {
            $query = $this->geoOpLogRepository->searchForQuery($keyword);
        }

        $adapter = new DoctrineORMAdapter($query);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(15);
        $page = $request->query->get('page', 1);
        try  {
            $pager->setCurrentPage($page);
        }
        catch(NotValidCurrentPageException $e) {
            throw new NotFoundHttpException('Illegal page');
        }

        return $this->render('ZoolyxCoreBundle:Admin:GeoOp/index.html.twig',array(
            'pager' => $pager
        ));
    }

}