<?php

namespace Zoolyx\CoreBundle\Controller\Admin;

use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Exception\NotValidCurrentPageException;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zoolyx\CoreBundle\Entity\Repository\ReportRepository;
use Zoolyx\CoreBundle\Model\Report\ReportBuilder;
use Zoolyx\CoreBundle\Model\Report\ReportProvider;

class ReportController extends Controller
{
    /**
     * @var ReportRepository
     *
     * @DI\Inject("zoolyx_core.repository.report")
     */
    private $reportRepository;

    /**
     * @var ReportProvider
     *
     * @DI\Inject("zoolyx_core.provider.model_report")
     */
    private $reportProvider;

    /**
     * @var ReportBuilder $reportBuilder
     *
     * @DI\Inject("zoolyx_core.report.builder")
     */
    private $reportBuilder;

    public function indexAction(Request $request)
    {
        $keyword = $request->query->get('key','');
        $lang = $request->query->get('lang','');
        if ($lang != '') {
            $query = $this->reportRepository->findByLanguageQuery($lang);
        } elseif ($keyword == '') {
            $query = $this->reportRepository->findAllQuery();
        } else {
            $query = $this->reportRepository->searchForQuery($keyword);
        }

        $adapter = new DoctrineORMAdapter($query);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(15);
        $page = $request->query->get('page', 1);
        try  {
            $pager->setCurrentPage($page);
        }
        catch(NotValidCurrentPageException $e) {
            throw new NotFoundHttpException('Illegal page');
        }

        return $this->render('ZoolyxCoreBundle:Admin:Report/Reports.html.twig',array(
            'keyword'               => $keyword,
            'pager'                 => $pager,
            'reportProvider'        => $this->reportProvider,
            'reportBuilder'         => $this->reportBuilder
        ));
    }

    public function index2Action(Request $request)
    {
        $keyword = $request->query->get('key','');
        $lang = $request->query->get('lang','');
        if ($lang != '') {
            $query = $this->reportRepository->findByLanguageQuery($lang);
        } elseif ($keyword == '') {
            $query = $this->reportRepository->findAllQuery();
        } else {
            $query = $this->reportRepository->searchForQuery($keyword);
        }

        $adapter = new DoctrineORMAdapter($query);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(15);
        $page = $request->query->get('page', 1);
        try  {
            $pager->setCurrentPage($page);
        }
        catch(NotValidCurrentPageException $e) {
            throw new NotFoundHttpException('Illegal page');
        }


        $reportEntities = $this->reportRepository->findAll();
        return $this->render('ZoolyxCoreBundle:Admin:Report/Reports2.html.twig',array(
            'reports'               => $reportEntities,
            'keyword'               => $keyword,
            'pager'                 => $pager,
            'reportProvider'        => $this->reportProvider,
            'reportBuilder'         => $this->reportBuilder
        ));
    }


}
