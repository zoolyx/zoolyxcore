<?php

namespace Zoolyx\CoreBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    /**
     * @var Session $session
     *
     * @DI\Inject("session")
     */
    private $session;

    public function indexAction()
    {
        return $this->render('ZoolyxCoreBundle:Default:index.html.twig', array());
    }

    public function disclaimerAction()
    {
        return $this->render('ZoolyxCoreBundle:Default:disclaimer.html.twig');
    }

    public function loggedOutAction($language = '') {
        if ($language != '') {
            $this->session->set('_locale', $language);
            return $this->redirectToRoute('zoolyx_logged_out');
        }

        return $this->render('ZoolyxCoreBundle:Default:logged_out.html.twig', array());
    }
}
