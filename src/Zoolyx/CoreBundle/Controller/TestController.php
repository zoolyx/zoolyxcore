<?php

namespace Zoolyx\CoreBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\Request;
use Zoolyx\CoreBundle\Model\Address\AddressType;
use Zoolyx\CoreBundle\Model\Address\Validation\Address;
use Zoolyx\CoreBundle\Model\Address\Validation\BpostApi;

class TestController extends Controller
{

    /**
     * @var Translator
     *
     * @DI\Inject("translator")
     */
    private $translator;

    /**
     * @var BpostApi $bpostApi
     *
     * @DI\Inject("zoolyx_core.address.bpost_api")
     */
    private $bpostApi;


    public function addressAction(Request $request)
    {
        $address = new Address();
        $address->setStreetName('Vredeweg 5');
        $address->setStreetNumber('');
        $address->setBoxNumber('');
        $address->setPostalCode('');
        //$address->setMunicipalityName('Zottegem');
        $address->setMunicipalityName('Brakel');
        $address->setCountryName('Belgium');

        $form = $this->createForm(new AddressType($this->translator), $address);

        $errors = array();
        $warnings = array();
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $newAddress = $this->bpostApi->validate($address);
            if (!is_array($newAddress)) {
                return $this->render('ZoolyxCoreBundle:Test:result.html.twig', array(
                    'address' => $newAddress
                ));
            }
            foreach ($newAddress[1] as $error) {
                $errors[] = $this->translator->trans("form.practice.response.".$error[0]);
            }
            foreach ($newAddress[2] as $warning) {
                $warnings[] = array(
                    $this->translator->trans("form.practice.response.".$warning[0]),
                    $this->translator->trans("form.practice.response.".$warning[1]));
            }

        };

        return $this->render('ZoolyxCoreBundle:Test:test.html.twig', array(
            'form' => $form->createView(),
            'errors' => $errors,
            'warnings' => $warnings
        ));
    }
}
