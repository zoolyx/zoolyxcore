<?php

namespace Zoolyx\CoreBundle\Controller\Veterinary;

use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Exception\NotValidCurrentPageException;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zoolyx\CoreBundle\Entity\Repository\ReportRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryPracticeRepository;
use Zoolyx\CoreBundle\Model\Report\ReportBuilder;

class ReportController extends Controller
{
    /**
     * @var VeterinaryPracticeRepository $veterinaryPracticeRepository
     *
     * @DI\Inject("zoolyx_core.repository.veterinary_practice")
     */
    private $veterinaryPracticeRepository;

    /**
     * @var ReportRepository $reportRepository
     *
     * @DI\Inject("zoolyx_core.repository.report")
     */
    private $reportRepository;

    /**
     * @var ReportBuilder $reportBuilder
     *
     * @DI\Inject("zoolyx_core.report.builder")
     */
    private $reportBuilder;

    public function indexAction(Request $request, $keyword='', $veterinaryPracticeId = 0, $shared = false) {

        $user = $this->getUser();

        if ($keyword) {
            $query = $this->reportRepository->searchForVeterinaryQuery($user, $keyword);
        } else {
            if ($veterinaryPracticeId) {
                $query = $this->reportRepository->findRecentForVeterinaryPracticeQuery($veterinaryPracticeId);
            } else {
                $query = $this->reportRepository->findRecentForVeterinaryQuery($user, $veterinaryPracticeId, $shared);
            }

        }

        $adapter = new DoctrineORMAdapter($query);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(15);
        $page = $request->query->get('page', 1);
        try  {
            $pager->setCurrentPage($page);
        }
        catch(NotValidCurrentPageException $e) {
            throw new NotFoundHttpException('Illegal page');
        }

        return $this->render('ZoolyxCoreBundle:Veterinary:Report/index.html.twig', array(
            'keyword' => $keyword,
            'veterinaryPractices' => $this->veterinaryPracticeRepository->getRealPracticesFor($user),
            'veterinaryPracticeId' => $veterinaryPracticeId,
            'shared' => $shared,
            'reportBuilder' => $this->reportBuilder,
            'pager' => $pager
        ));
    }
}
