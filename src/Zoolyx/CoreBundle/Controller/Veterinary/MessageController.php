<?php

namespace Zoolyx\CoreBundle\Controller\Veterinary;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\TicketBundle\Entity\Repository\WatchRepository;

class MessageController extends Controller
{

    /**
     * @var VeterinaryRepository $veterinaryRepository
     *
     * @DI\Inject("zoolyx_core.repository.veterinary")
     */
    private $veterinaryRepository;

    /**
     * @var WatchRepository $watchRepository
     *
     * @DI\Inject("zoolyx_ticket.repository.watch")
     */
    private $watchRepository;

    public function indexAction() {
        $user = $this->getUser();
        /** @var Veterinary $veterinary */
        $veterinary = $this->veterinaryRepository->findOneBy(array('account'=>$user));

        $watches = $this->watchRepository->findAllForUserQuery($user);

        return $this->render('ZoolyxCoreBundle:Veterinary:Message/index.html.twig', array(
            'veterinary' => $veterinary,
            'watches' => $watches
        ));
    }

}
