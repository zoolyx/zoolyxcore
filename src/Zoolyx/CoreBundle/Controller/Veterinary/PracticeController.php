<?php

namespace Zoolyx\CoreBundle\Controller\Veterinary;

use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Exception\NotValidCurrentPageException;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Zoolyx\CoreBundle\Entity\Repository\PracticeRepository;
use Zoolyx\CoreBundle\Entity\Repository\ReportRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryPracticeRepository;
use Zoolyx\CoreBundle\Forms\PracticeType;
use Zoolyx\CoreBundle\Model\Report\ReportBuilder;

class PracticeController extends Controller
{


    /**
     * @var VeterinaryPracticeRepository $veterinaryPracticeRepository
     *
     * @DI\Inject("zoolyx_core.repository.veterinary_practice")
     */
    private $veterinaryPracticeRepository;

    /**
     * @var PracticeRepository $practiceRepository
     *
     * @DI\Inject("zoolyx_core.repository.practice")
     */
    private $practiceRepository;

    /**
     * @var Translator
     *
     * @DI\Inject("translator")
     */
    private $translator;

    public function indexAction(Request $request) {
        $user = $this->getUser();

        $query = $this->practiceRepository->searchForAccountQuery($user);
        $adapter = new DoctrineORMAdapter($query);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(15);
        $page = $request->query->get('page', 1);
        try  {
            $pager->setCurrentPage($page);
        }
        catch(NotValidCurrentPageException $e) {
            throw new NotFoundHttpException('Illegal page');
        }

        return $this->render('ZoolyxCoreBundle:Veterinary:Practice/index.html.twig', array(
            'veterinaryPractices' => $this->veterinaryPracticeRepository->getPracticesFor($user),
            'pager' => $pager
        ));
    }

    public function practiceAction(Request $request, $id) {
        $user = $this->getUser();

        $practice = $this->practiceRepository->find($id);

        $form = $this->createForm(new PracticeType($this->translator));

        if (!is_null($request)) {
            $form->handleRequest($request);
        };

        if ($form->isValid()) {

        }

        return $this->render('ZoolyxCoreBundle:Veterinary:Practice/practice.html.twig', array(
            'veterinaryPractices' => $this->veterinaryPracticeRepository->getPracticesFor($user),
            'form' => $form->createView(),
        ));
    }

}
