<?php

namespace Zoolyx\CoreBundle\Controller\Veterinary;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Exception\NotValidCurrentPageException;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zoolyx\CoreBundle\Entity\GeoOpLog;
use Zoolyx\CoreBundle\Entity\Repository\GeoOpLogRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryPracticeRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Model\Date\DateProvider;
use Zoolyx\CoreBundle\Model\GeoOp\Api;
use Zoolyx\CoreBundle\Model\GeoOp\Logger\GeoOpLogger;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpAddress;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpClient;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpJob;
use Zoolyx\CoreBundle\Model\GeoOp\Model\GeoOpNote;

class PickupController extends Controller
{

    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var VeterinaryRepository $veterinaryRepository
     *
     * @DI\Inject("zoolyx_core.repository.veterinary")
     */
    private $veterinaryRepository;

    /**
     * @var VeterinaryPracticeRepository $veterinaryPracticeRepository
     *
     * @DI\Inject("zoolyx_core.repository.veterinary_practice")
     */
    private $veterinaryPracticeRepository;

    /**
     * @var Api $geoOpApi
     *
     * @DI\Inject("zoolyx_core.geoop.api")
     */
    private $geoOpApi;
    /**
     * @var DateProvider $dateProvider
     *
     * @DI\Inject("zoolyx_core.provider.date")
     */
    private $dateProvider;

    /**
     * @var GeoOpLogger $geoOpLogger
     *
     * @DI\Inject("zoolyx_core.geoop.logger")
     */
    private $geoOpLogger;

    /**
     * @var GeoOpLogRepository
     *
     * @DI\Inject("zoolyx_core.repository.geo_op_log")
     */
    private $geoOpLogRepository;


    public function indexAction()
    {
        $user = $this->getUser();
        /** @var Veterinary $veterinary */
        $veterinary = $this->veterinaryRepository->findOneBy(array('account'=>$user));

        $practiceJobs = array();
        /** @var VeterinaryPractice $veterinaryPractice */
        foreach ($veterinary->getVeterinaryPractices() as $veterinaryPractice) {
            /** @var array $jobs */
            $jobs = $this->geoOpLogRepository->findFutureForVeterinayPractice($veterinaryPractice);
            if (count($jobs)) {
                $practice = $veterinaryPractice->getPractice();
                if (!isset($practiceJobs[$practice->getId()])) {
                    $practiceJobs[$practice->getId()] = array(
                        "practice" => $practice,
                        "jobs" => $jobs
                    );
                } else {
                    $practiceJobs[$practice->getId()]["jobs"] = array_merge($practiceJobs[$practice->getId()]["jobs"], $jobs);
                }
            }
        }

        if (count($practiceJobs) == 0) {
            return $this->redirectToRoute("veterinary_pickup_details");
        }

        return $this->render('ZoolyxCoreBundle:Veterinary:Pickup/existing_jobs.html.twig', array(
            'veterinary' => $veterinary,
            'practiceJobs' => $practiceJobs
        ));
    }

    public function detailsAction($jobId=null)
    {
        $user = $this->getUser();
        /** @var Veterinary $veterinary */
        $veterinary = $this->veterinaryRepository->findOneBy(array('account'=>$user));

        $practicesPickupTill = array();
        $practiceJobs = array();
        /** @var VeterinaryPractice $veterinaryPractice */
        foreach ($veterinary->getVeterinaryPractices() as $veterinaryPractice) {
            $practice = $veterinaryPractice->getPractice();
            $practicesPickupTill[] = array(
                "id" => $veterinaryPractice->getId(),
                "pickup_till" => $practice->getPickupTill()
            );

            /** @var array $jobs */
            $jobs = $this->geoOpLogRepository->findFutureForVeterinayPractice($veterinaryPractice);
            if (count($jobs)) {
                $practice = $veterinaryPractice->getPractice();
                if (!isset($practiceJobs[$practice->getId()])) {
                    $practiceJobs[$practice->getId()] = array(
                        "practice" => $practice,
                        "jobs" => $jobs
                    );
                } else {
                    $practiceJobs[$practice->getId()]["jobs"] = array_merge($practiceJobs[$practice->getId()]["jobs"], $jobs);
                }
            }
        }

        $jobToBeRescheduled = $jobId ? $this->geoOpLogRepository->find($jobId) : null;

        return $this->render('ZoolyxCoreBundle:Veterinary:Pickup/index.html.twig', array(
            'veterinary' => $veterinary,
            'today' => new \DateTime(),
            'nextWorkingDay' => $this->dateProvider->getNextWorkingDay(),
            'afterNextWorkingDay' => $this->dateProvider->getAfterNextWorkingDay(),
            'practicesPickupTillJson' => json_encode($practicesPickupTill),
            'jobToBeRescheduled' => $jobToBeRescheduled
        ));
    }

    public function postJobAction(Request $request, $jobIdToCancel = null)
    {
        $veterinaryPracticeId = $request->request->get('practice',0);
        $when = $request->request->get('when');  //today or nextday
        $availableFrom = $request->request->get('available-from');   //uur
        $what = $request->request->get('what');
        $ophaling = $request->request->get("ophaling");
        $isUrgent = $request->request->get("is-urgent");
        $isUrgent = $isUrgent == "on" ? 1 : 2;

        $dateAvailable = $when=="1" ? new \DateTime() : $this->dateProvider->getNextWorkingDay();
        $hourParts = explode(":",$availableFrom);
        $dateAvailable->setTime($hourParts[0],$hourParts[1]);

        $time = $dateAvailable->format("Y-m-d\TH:i:s e");

        /** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = $this->veterinaryPracticeRepository->find($veterinaryPracticeId);
        if (!$veterinaryPractice->getVeterinary() ||
            !$veterinaryPractice->getVeterinary()->getAccount() ||
            !$veterinaryPractice->getVeterinary()->getAccount()->getId() == $this->getUser()->getId()
        ) {
            return $this->redirectToRoute('veterinary_pickup');
        }

        if (!$veterinaryPractice->getPractice()) {
            return $this->redirectToRoute('veterinary_pickup');
        }

        $pickupPriority = $isUrgent;
        $pickupSubject = implode('+',$what);
        $pickupLine1 = $veterinaryPractice->getPractice()->getStreet();
        $pickupPostCode = $veterinaryPractice->getPractice()->getZipCode();
        $pickupCity = $veterinaryPractice->getPractice()->getCity();

        $geoOpAddress = new GeoOpAddress();
        $geoOpAddress->setLine1($pickupLine1);
        $geoOpAddress->setPostCode($pickupPostCode);
        $geoOpAddress->setCity($pickupCity);

        /** @var GeoOpClient $geoOpClient */
        $geoOpClient = $this->geoOpApi->getClient($veterinaryPractice->getLimsId());

        if (is_null($geoOpClient)) {
            /** @var Veterinary $veterinary */
            $veterinary = $veterinaryPractice->getVeterinary();
            $geoOpClient = new GeoOpClient();
            $geoOpClient
                ->setAddress($geoOpAddress)
                ->setFirstName($veterinary->getFirstName())
                ->setLastName($veterinary->getLastName())
                ->setPhoneNumber($veterinaryPractice->getPractice()->getTelephone())
                ->setMobileNumber($veterinary->getTelephone())
                ->setCompanyName($veterinaryPractice->getPractice()->getName())
                ->setEmailAddress($veterinary->getAccount()->getEmail())
            ;
            $result = $this->geoOpApi->postClient($geoOpClient,$veterinaryPractice->getLimsId());

            if (strtolower($result["result"]) == "success") {
                if (isset($result["clients"]) && isset($result["clients"][0]) && isset($result["clients"][0]["id"])) {
                    $geoOpClient->setId($result["clients"][0]["id"]);
                }
            }
        }

        if ( is_null($geoOpClient) || is_null($geoOpClient->getId())) {
            return $this->render('ZoolyxCoreBundle:Veterinary:Pickup/failure.html.twig', array());
        }

        $geoOpJob = new GeoOpJob();
        $geoOpJob
            ->setClientId($geoOpClient->getId())
            ->setTitle($pickupSubject)
            ->setAddress($geoOpAddress)
            ->setPriority($pickupPriority)
        ;

        $jobResult = $this->geoOpApi->postJob($geoOpJob);
        if (strtolower($jobResult["result"]) == "success") {
            $geoOpJob->setId($jobResult["jobs"][0]["id"]);
        }

        if ( is_null($geoOpJob->getId())) {
            $geoOpLog = $this->geoOpLogger->getLoggingEntity($geoOpClient, $geoOpJob, $jobResult, $veterinaryPractice, $dateAvailable, $ophaling);
            $this->entityManager->persist($geoOpLog);
            $this->entityManager->flush();
            return $this->render('ZoolyxCoreBundle:Veterinary:Pickup/failure.html.twig', array());
        }

        $geoOpNote = new GeoOpNote();
        $geoOpNote
            ->setDescription('Staal beschikbaar vanaf')
            ->setTime($time)
            ->setJobId($geoOpJob->getId());

        $resultNote1 = $this->geoOpApi->postNote($geoOpNote);
        
        $geoOpNote
            ->setDescription('Ophaling voorzien voor '.$ophaling)
            ->setTime($time)
            ->setJobId($geoOpJob->getId());

        $resultNote2 = $this->geoOpApi->postNote($geoOpNote);

        if (!is_null($jobIdToCancel)) {
            /** @var GeoOpLog $geoOpLog */
            $geoOpLog = $this->geoOpLogRepository->find($jobIdToCancel);
            $geoOpJobToCancel = new GeoOpJob();
            $geoOpJobToCancel->setId($geoOpLog->getJobId());
            $resultCancel = $this->geoOpApi->patchJob($geoOpJobToCancel);
            if (strtolower($resultCancel["result"]) != "success") {
                $geoOpLog = $this->geoOpLogger->getLoggingEntity($geoOpClient, $geoOpJob, $jobResult, $veterinaryPractice, $dateAvailable, $ophaling);
                $this->entityManager->persist($geoOpLog);
                $this->entityManager->flush();
                return $this->render('ZoolyxCoreBundle:Veterinary:Pickup/failure.html.twig', array());
            } else {
                $geoOpLog->setResponse("canceled");
                $this->entityManager->persist($geoOpLog);
            }
        }


        $geoOpLog = $this->geoOpLogger->getLoggingEntity($geoOpClient, $geoOpJob, $jobResult, $veterinaryPractice, $dateAvailable, $ophaling);
        $this->entityManager->persist($geoOpLog);
        $this->entityManager->flush();

        if (strtolower($resultNote1["result"]) == "success" && strtolower($resultNote2["result"]) == "success") {
            return $this->render('ZoolyxCoreBundle:Veterinary:Pickup/success.html.twig', array());
        }
        else {
            return $this->render('ZoolyxCoreBundle:Veterinary:Pickup/failure.html.twig', array());
        }
    }

    public function historyAction(Request $request)
    {
        $user = $this->getUser();
        /** @var Veterinary $veterinary */
        $veterinary = $this->veterinaryRepository->findOneBy(array('account'=>$user));

        $query = $this->geoOpLogRepository->findForVeterinaryQuery($veterinary);

        $adapter = new DoctrineORMAdapter($query);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(15);
        $page = $request->query->get('page', 1);
        try  {
            $pager->setCurrentPage($page);
        }
        catch(NotValidCurrentPageException $e) {
            throw new NotFoundHttpException('Illegal page');
        }

        return $this->render('ZoolyxCoreBundle:Veterinary:Pickup/history.html.twig',array(
            'pager' => $pager
        ));
    }
}
