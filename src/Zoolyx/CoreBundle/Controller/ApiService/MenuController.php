<?php

namespace Zoolyx\CoreBundle\Controller\ApiService;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Zoolyx\CoreBundle\Model\Menu\MenuBuilder;

class MenuController extends Controller
{
    /**
     * @var MenuBuilder
     *
     * @DI\Inject("zoolyx_core.menu.builder")
     */
    private $menuBuilder;

    /**
     *
     * @throws NotFoundHttpException if techne agora cannot be found
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Search",
     *  description="Returns all veterinaries with the given {key} in their name or username",
     *  statusCodes={
     *      200="Returned when successful"
     *  })
     */
    public function getMenuAction(Request $request, $language)
    {
        $request->getSession()->set('_locale', $language);

        $menu = $this->menuBuilder->getMenu($this->getUser(), $language);

        return $this->render('ZoolyxCoreBundle:Service:Menu/menu.html.twig',array(
            'menu' => $menu
        ));
    }

}
