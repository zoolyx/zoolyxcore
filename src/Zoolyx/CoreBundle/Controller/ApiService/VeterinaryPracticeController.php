<?php

namespace Zoolyx\CoreBundle\Controller\ApiService;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryPracticeRepository;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;

class VeterinaryPracticeController extends Controller
{
    /**
     * @var VeterinaryPracticeRepository $veterinaryPracticeRepository
     *
     * @DI\Inject("zoolyx_core.repository.veterinary_practice")
     */
    private $veterinaryPracticeRepository;

    /**
     * @param string $key
     *
     * @return View
     *
     * @throws NotFoundHttpException if techne agora cannot be found
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Search",
     *  description="Returns all veterinaries with the given {key} in their name or username",
     *  statusCodes={
     *      200="Returned when successful"
     *  })
     */
    public function searchAction($key)
    {
        $veterinaryPractices = $this->veterinaryPracticeRepository->searchFor($key);

        $results = array();
        /** @var VeterinaryPractice $veterinaryPractice */
        foreach ($veterinaryPractices as $veterinaryPractice) {
            $limsId = $veterinaryPractice->getLimsId();
            if (!is_null($limsId) && $limsId!='' && $limsId!="unknown") {
                $results[] = [
                    'limsId'=> $limsId,
                    'name'=>$veterinaryPractice->getVeterinary()->getFirstName() . " " .
                        $veterinaryPractice->getVeterinary()->getLastName() . " - " .
                        $veterinaryPractice->getPractice()->getName()
                ];
            }
        }

        return View::create(['results'=>$results], 200);
    }

    /**
     * @param string $limsId
     *
     * @return View
     *
     * @throws NotFoundHttpException if techne agora cannot be found
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Search",
     *  description="Returns all veterinaries with the given {key} in their name or username",
     *  statusCodes={
     *      200="Returned when successful"
     *  })
     */
    public function vatAction($limsId)
    {
        /** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = $this->veterinaryPracticeRepository->findOneBy(array('limsId'=>$limsId));
        $vat = $veterinaryPractice->getVat();
        $vatNumber = $vat ? $vat->getVat() : '';
        $faId = $vat ? $vat->getFaId() : '';
        $invoiceDefault = $veterinaryPractice->getInvoiceDefault();

        return View::create(['result'=>['vatNbr'=>$vatNumber,'faId' => $faId,'default' => $invoiceDefault]], 200);
    }



}
