<?php

namespace Zoolyx\CoreBundle\Controller\ApiService;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Zoolyx\CoreBundle\Entity\BreedDescription;
use Zoolyx\CoreBundle\Entity\Repository\BreedDescriptionRepository;

class SpeciesController extends Controller
{
    /**
     * @var BreedDescriptionRepository $breedDescriptionRepository
     *
     * @DI\Inject("zoolyx_core.repository.breed_description")
     */
    private $breedDescriptionRepository;


    /**
     * @param string $speciesCode
     * @param string $language
     *
     * @return View
     *
     * @throws NotFoundHttpException if techne agora cannot be found
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Search",
     *  description="Returns all veterinaries with the given {key} in their name or username",
     *  statusCodes={
     *      200="Returned when successful"
     *  })
     */
    public function breedAction($speciesCode, $language = "nl")
    {
        $breedDescriptions = $this->breedDescriptionRepository->findAllForSpecies($speciesCode, $language);
        $breeds = array();
        /** @var BreedDescription $breedDescription */
        foreach ($breedDescriptions as $breedDescription) {
            $breedCodeParts = explode("_",$breedDescription->getBreed()->getBreedId());
            $breeds[] = array(
                'id' => count($breedCodeParts)>1 ? $breedCodeParts[1] : $breedCodeParts[0],
                'description' => $breedDescription->getDescription()
            );
        }

        //var_dump($breeds); die();
        //return json_encode(['results'=>$veterinaries]);
        return View::create(['results'=>$breeds], 200);
    }

}
