<?php

namespace Zoolyx\CoreBundle\Controller\ApiService;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\Repository\OwnerRepository;
use Zoolyx\CoreBundle\Entity\Repository\ReportRepository;
use Zoolyx\CoreBundle\Entity\Repository\SampleRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\Sample;

class SearchController extends Controller
{
    /**
     * @var ReportRepository $reportRepository
     *
     * @DI\Inject("zoolyx_core.repository.report")
     */
    private $reportRepository;

    /**
     * @var SampleRepository $sampleRepository
     *
     * @DI\Inject("zoolyx_core.repository.sample")
     */
    private $sampleRepository;

    /**
     * @var VeterinaryRepository $veterinaryRepository
     *
     * @DI\Inject("zoolyx_core.repository.veterinary")
     */
    private $veterinaryRepository;


    /**
     * @var OwnerRepository $ownerRepository
     *
     * @DI\Inject("zoolyx_core.repository.owner")
     */
    private $ownerRepository;

    /**
     * @param string $key
     *
     * @return View
     *
     * @throws NotFoundHttpException if techne agora cannot be found
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Search",
     *  description="Returns all veterinaries with the given {key} in their name or username",
     *  statusCodes={
     *      200="Returned when successful"
     *  })
     */
    public function veterinaryAction($key)
    {
        $users = $this->veterinaryRepository->searchFor($key);
        //return json_encode(['results'=>$veterinaries]);
        return View::create(['results'=>$users], 200);
    }

    /**
     * @param string $key
     *
     * @return View
     *
     * @throws NotFoundHttpException if techne agora cannot be found
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Search",
     *  description="Returns all veterinaries with the given {key} in their name or username",
     *  statusCodes={
     *      200="Returned when successful"
     *  })
     */
    public function ownerAction($key)
    {
        $users = $this->ownerRepository->searchFor($key);
        //return json_encode(['results'=>$veterinaries]);
        return View::create(['results'=>$users], 200);
    }

    /**
     * @param string $key
     *
     * @return View
     *
     * @throws NotFoundHttpException if techne agora cannot be found
     *
     * @Doc\ApiDoc(
     *  section="Zoolyx Search",
     *  description="returns all reports for which the request ID contains the given {key}",
     *  statusCodes={
     *      200="Returned when successful"
     *  })
     *  })
     */
    public function reportAction($key)
    {
        $reports = $this->reportRepository->searchFor($key);
        $samples = $this->sampleRepository->searchFor($key);
        $results = array();
        /** @var Report $report */
        foreach ($reports as $report) {
            $results[$report->getId()] = array('id'=>$report->getId(), 'request_id' => $report->getRequestId());
        }
        /** @var Sample $sample */
        foreach ($samples as $sample) {
            $reportId = $sample->getReportVersion()->getReport()->getId();
            $results[$reportId] = array('id'=>$reportId, 'request_id' => $sample->getCode());
        }

        return View::create(['results'=>$results], 200);
    }

}
