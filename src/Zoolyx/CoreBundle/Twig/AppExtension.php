<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 28.02.16
 * Time: 17:40
 */
namespace Zoolyx\CoreBundle\Twig;

use DateTime;
use DOMDocument;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Zoolyx\CoreBundle\Entity\Breed;
use Zoolyx\CoreBundle\Entity\File;
use Zoolyx\CoreBundle\Entity\Owner;
use Zoolyx\CoreBundle\Entity\ParameterProfileForm;
use Zoolyx\CoreBundle\Entity\Pet;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\Repository\BreedRepository;
use Zoolyx\CoreBundle\Entity\Repository\OwnerRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\Sample;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Model\Date\DateProvider;
use Zoolyx\CoreBundle\Model\Request\Object\RequestPet;
use Zoolyx\CoreBundle\Model\Wiki\ParameterPage;
use Zoolyx\CoreBundle\Model\XmlParser\ParserMessage;
use Zoolyx\TicketBundle\Entity\MessageEvent;
use Zoolyx\TicketBundle\Entity\TicketEvent;
use Zoolyx\TicketBundle\Entity\WatchTicket;
use Zoolyx\TicketBundle\Model\UrlLinker\UrlLinker;
use Zoolyx\TicketBundle\Visitor\TicketEvent\GetTwigVisitor;
use Zoolyx\TicketBundle\Visitor\TicketEvent\ShowToUserVisitor;

class AppExtension extends \Twig_Extension
{
    /** @var Translator */
    private $translator;

    /** @var GetTwigVisitor */
    private $twigVisitor;
    /** @var ShowToUserVisitor */
    private $showToUserVisitor;

    /** @var BreedRepository */
    private $breedRepository;
    /** @var VeterinaryRepository */
    private $veterinaryRepository;
    /** @var OwnerRepository */
    private $ownerRepository;
    /** @var UrlLinker */
    private $urlLinker;
    /** @var DateProvider */
    private $dateProvider;

    private $formsDisplayed = array();

    /**
     * Constructor.
     *
     * @param Translator $translator
     * @param GetTwigVisitor $twigVisitor
     * @param ShowToUserVisitor $showToUserVisitor
     * @param BreedRepository $breedRepository
     * @param VeterinaryRepository $veterinaryRepository
     * @param OwnerRepository $ownerRepository
     * @param UrlLinker $urlLinker
     * @param DateProvider $dateProvider
     */

    public function __construct(Translator $translator, GetTwigVisitor $twigVisitor, ShowToUserVisitor $showToUserVisitor, BreedRepository $breedRepository, VeterinaryRepository $veterinaryRepository, OwnerRepository $ownerRepository, UrlLinker $urlLinker, DateProvider $dateProvider)
    {
        $this->translator = $translator;
        $this->twigVisitor = $twigVisitor;
        $this->showToUserVisitor = $showToUserVisitor;
        $this->breedRepository = $breedRepository;
        $this->veterinaryRepository = $veterinaryRepository;
        $this->ownerRepository = $ownerRepository;
        $this->urlLinker = $urlLinker;
        $this->dateProvider = $dateProvider;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('mainRoles', array($this, 'mainRolesFilter')),
            new \Twig_SimpleFilter('roleStyle', array($this, 'roleStyleFilter')),
            new \Twig_SimpleFilter('filterRoles', array($this, 'filterRolesFilter')),
            new \Twig_SimpleFilter('statusAsText', array($this, 'statusAsTextFilter')),
            new \Twig_SimpleFilter('ownerSummary', array($this, 'ownerSummaryFilter')),
            new \Twig_SimpleFilter('ownerName', array($this, 'ownerNameFilter')),
            new \Twig_SimpleFilter('veterinarySummary', array($this, 'veterinarySummaryFilter')),
            new \Twig_SimpleFilter('practiceSummary', array($this, 'practiceSummaryFilter')),
            new \Twig_SimpleFilter('firstUpper', array($this, 'firstUpperFilter')),
            new \Twig_SimpleFilter('niceDate', array($this, 'niceDateFilter')),
            new \Twig_SimpleFilter('parserMessage', array($this, 'parserMessageFilter')),
            new \Twig_SimpleFilter('icon', array($this, 'iconFilter')),
            new \Twig_SimpleFilter('isImage', array($this, 'isImageFilter')),
            new \Twig_SimpleFilter('isHtml', array($this, 'isHtmlFilter')),
            new \Twig_SimpleFilter('hasAge', array($this, 'hasAgeFilter')),
            new \Twig_SimpleFilter('age', array($this, 'ageFilter')),
            new \Twig_SimpleFilter('reportType', array($this, 'reportTypeFilter')),
            new \Twig_SimpleFilter('isCompleted', array($this, 'isCompletedFilter')),
            new \Twig_SimpleFilter('prepareDisplay', array($this, 'prepareDisplay')),
            new \Twig_SimpleFilter('breed', array($this, 'breedFilter')),
            new \Twig_SimpleFilter('speciesImage',array($this, 'speciesImageFilter')),
            new \Twig_SimpleFilter('formIsNew', array($this, 'formIsNewFilter')),
            new \Twig_SimpleFilter('autocomplete', array($this, 'autocompleteFilter')),
            new \Twig_SimpleFilter('formatDelay', array($this, 'formatDelayFilter')),
            new \Twig_SimpleFilter('getTwig', array($this, 'getTwigFilter')),
            new \Twig_SimpleFilter('showToUser', array($this, 'showToUserFilter')),
            new \Twig_SimpleFilter('kommaToDot', array($this,'kommaToDotFilter')),
            new \Twig_SimpleFilter('accountName', array($this, 'accountNameFilter')),
            new \Twig_SimpleFilter('formatPrice', array($this, 'formatPriceFilter')),
            new \Twig_SimpleFilter('price', array($this, 'priceFilter')),
            new \Twig_SimpleFilter('telephoneNumbers', array($this, 'telephoneNumbersFilter')),
            new \Twig_SimpleFilter('isOwner', array($this, 'isOwnerFilter')),
            new \Twig_SimpleFilter('eventColor', array($this, 'eventColorFilter')),
            new \Twig_SimpleFilter('firstChar', array($this, 'firstCharFilter')),
            new \Twig_SimpleFilter('watcherState', array($this, 'watcherStateFilter')),
            new \Twig_SimpleFilter('filterBadTags', array($this, 'filterBadTagsFilter')),
            new \Twig_SimpleFilter('linkUrls', array($this, 'linkUrlsFilter')),
            new \Twig_SimpleFilter('fillcol', array($this, 'fillcolFilter')),
            new \Twig_SimpleFilter('intlDate', array($this, 'intlDateFilter')),
            new \Twig_SimpleFilter('isNoWorkingDay', array($this, 'isNoWorkingDayFilter')),
        );
    }

    public function mainRolesFilter($roles)
    {
        $mainRoles = array();
        if (in_array('ROLE_SUPER_ADMIN',$roles)) {
            $mainRoles[] = 'Super_Admin';
        } elseif (in_array('ROLE_ADMIN',$roles)) {
            $mainRoles[] = 'Admin';
        }

        if (in_array('ROLE_ADMINISTRATOR',$roles)) {
            $mainRoles[] = 'Ticketing';
        }
        if (in_array('ROLE_ORGANISATION',$roles)) {
            $mainRoles[] = 'Organisation';
        }
        if (in_array('ROLE_CONTRACTOR',$roles)) {
            $mainRoles[] = 'Contractor';
        }
        if (in_array('ROLE_VETERINARY',$roles)) {
            $mainRoles[] = 'Veterinary';
        }
        if (in_array('ROLE_OWNER',$roles)) {
            $mainRoles[] = 'Owner';
        }

        if (  !count($mainRoles) && in_array('ROLE_USER',$roles)  ) {
            $mainRoles[] = 'User';
        }

        return $mainRoles;
    }

    public function roleStyleFilter($role) {
        switch($role) {
            case 'Super_Admin' : return 'label-danger'; break;
            case 'Ticketing' : return 'label-info'; break;
            case 'Organisation' : return 'label-primary'; break;
            case 'Contractor' : return 'label-warning'; break;
            case 'Veterinary' : return 'label-success'; break;
            case 'Owner' : return 'label-default'; break;
            case 'User' : return 'label-default'; break;
            default: return 'label-default';
        }
    }

    public function filterRolesFilter($roles, $rolesToFilter) {
        foreach ($rolesToFilter as $roleToFilter) {
            foreach ($roles[$roleToFilter] as $extraFilter) {
                $rolesToFilter[] = $extraFilter;
            }
        }
        $filteredRoles = array();
        foreach ($roles as $role => $subRoles) {
            if (!in_array($role,$rolesToFilter))
                $filteredRoles[] = $role;
        }
        return $filteredRoles;
    }

    public function statusAsTextFilter($status, $language="nl") {
        $statusText = "";
        switch ($status) {
            case "0": $statusText = "report.incomplete"; break;
            case "1": $statusText = "report.complete"; break;
        }
        //$this->translator->setLocale($language);
        return $this->translator->trans($statusText, array(), null, $language);
    }

    public function ownerSummaryFilter(Owner $owner) {
        $summary = array();
        $summary[] = $this->firstUpperFilter($owner->getFirstName());
        $summary[] = ' ';
        $summary[] = $this->firstUpperFilter($owner->getLastName());
        $summary[] = ', ';
        $summary[] = $this->firstUpperFilter($owner->getStreet());
        $summary[] = ', ';
        $summary[] = $this->firstUpperFilter($owner->getZipCode());
        $summary[] = ' ';
        $summary[] = $this->firstUpperFilter($owner->getCity());
        return implode('',$summary);
    }
    public function ownerNameFilter(Owner $owner) {
        $summary = array();
        $summary[] = $this->firstUpperFilter($owner->getFirstName());
        $summary[] = ' ';
        $summary[] = $this->firstUpperFilter($owner->getLastName());
        return implode('',$summary);
    }
    public function veterinarySummaryFilter(Report $report) {
        $veterinary = $report->getVeterinaryPractice()->getVeterinary();
        return $veterinary->getAccount()->getUsername();
    }
    public function practiceSummaryFilter(VeterinaryPractice $veterinaryPractice) {
        $practice = $veterinaryPractice->getPractice();
        $veterinary = $veterinaryPractice->getVeterinary();
        $practiceName = $practice->getName() ? $practice->getName() : "No name";
        return $veterinary->getFirstName() . " " . $veterinary->getLastName() . ' - ' . $practiceName;
        //return $practice->getPracticeId() ? "Group ".$practice->getPracticeId() : $practice->getName();
    }

    public function firstUpperFilter($input) {
        return ucfirst(strtolower($input));
    }

    public function niceDateFilter(DateTime $date) {
        $now = new DateTime();
        $today = new DateTime($now->format('Y-m-d'));
        if ($date > $today) {
            return $date->format('g:i a');
        } else {
            return $date->format('j M Y');
        }
    }
    
    public function intlDateFilter(DateTime $date, $format= 'YYYY-mm-dd', $language = 'en') {
         
         $dateFormatter = new \IntlDateFormatter(
  								    $language,
									\IntlDateFormatter::NONE,
									\IntlDateFormatter::NONE,
									date_default_timezone_get(),
									\IntlDateFormatter::GREGORIAN,
          							$format);
        
    	return $dateFormatter->format($date);
    
    }

    public function parserMessageFilter(ParserMessage $message) {
        $style = "";
        switch ($message->getType()) {
            case "success" : $style = "color: #0f0;"; break;
            case "warning" : $style = "color: #ffa500;"; break;
            case "critical" : $style = "color: #f00; font-weight: bold"; break;
        }
        return $style;
    }

    public function iconFilter(File $file) {
        $ext = pathinfo($file->getName(), PATHINFO_EXTENSION);
        $icon = 'fa-file-o';
        switch ($ext) {
            case 'pdf' :
                $icon = 'fa-file-pdf-o';
                break;
            case 'jpg' :
            case 'gif' :
            case 'png' :
                $icon = 'fa-file-image-o';
                break;
            case 'xls' :
            case 'xlsx' :
                $icon = 'fa-file-excel-o';
                break;
            case 'docx' :
                $icon = 'fa-file-text-o';
                break;
        }
        return $icon;
    }

    public function isImageFilter(File $file) {
        $ext = pathinfo($file->getName(), PATHINFO_EXTENSION);
        $showInline = false;
        switch (strtolower($ext)) {
            case 'jpg' :
            case 'jpeg' :
            case 'gif' :
            case 'png' :
            case 'bmp' :
                $showInline = true;
        }
        return $showInline;
    }
    public function isHtmlFilter(File $file) {
        $ext = pathinfo($file->getName(), PATHINFO_EXTENSION);
        $showInline = false;
        switch (strtolower($ext)) {
            case 'html' :
            case 'htm' :
                $showInline = true;
        }
        return $showInline;
    }

    public function hasAgeFilter(Pet $pet) {
        return $pet->getBirthDate() != null;
    }
    public function ageFilter(Pet $pet, $language) {
        $now = new DateTime();
        $interval = $now->diff($pet->getBirthDate());
        $years = $interval->format('%y');
        $months = $interval->format('%m');
        $age = array();
        $this->translator->setLocale($language);
        if ($years >0) $age[] = $years . ' '.$this->translator->trans('year');
        if ($months>0) $age[] = $months.' '.$this->translator->trans('month');
        return implode(', ',$age);
    }

    public function reportTypeFilter(Report $report) {
        if (count($samples = $report->getFirstVersion()->getSamples()) == 1) {
            /** @var Sample $sample */
            $sample = $samples[0];
            return $sample->getPet()->getName();
        } else {
            return "Multiple";
        }
    }

    public function isCompletedFilter(Report $report) {
        foreach ($report->getFirstVersion()->getSamples() as $sample) {
            if ($sample->getCompleted() == 0) {
                return 0;
            }
        }
        return 1;
    }

    public function prepareDisplay($display) {
        return $display;
    }

    public function showToUserFilter(TicketEvent $event) {
        return $event->accept($this->showToUserVisitor);
    }
    public function getTwigFilter(TicketEvent $event) {
        return $event->accept($this->twigVisitor);
    }

    public function breedFilter(Pet $pet) {
        $breedString = "";
        if ($pet->getBreedCode() !='' && !is_null($pet->getBreedCode())) {
            /** @var Breed $breed */
            $breed = $this->breedRepository->findOneBy(array('breedId'=>$pet->getSpeciesCode()."_".$pet->getBreedCode()));
            if ($breed) {
                $breedString = $breed->getDescription($this->translator->getLocale());
            }
        }

        $breedString = $breedString == "" ? $pet->getBreed() : $breedString;
        return $breedString;
    }

    public function speciesImageFilter($speciesCode) {
        switch ($speciesCode) {
            case 'A' : $speciesName = "blank"; break; //andere
            case 'B' : $speciesName = "guinea_pig"; break;
            case 'C' :
            case 'Geit' : $speciesName = "goat"; break;
            case 'D' :
            case 'Vogel' : $speciesName = "bird"; break;
            case 'E' :
            case 'Ezel' : $speciesName = "donkey"; break;
            case 'F' :
            case 'Fret' : $speciesName = "ferret"; break;
            case 'G' : $speciesName = "rodent"; break; //knaagdier
            case 'H' :
            case 'Hond' : $speciesName = "dog"; break;
            case 'I' :
            case 'Vis' : $speciesName = "fish"; break;
            case 'J' :
            case 'Konijn' : $speciesName = "rabbit"; break;
            case 'K' :
            case 'Kat' : $speciesName = "cat"; break;
            case 'L' :
            case 'Schaap' : $speciesName = "sheep"; break;
            case 'M' : $speciesName = "alpaca"; break;
            case 'N' : $speciesName = "elephant"; break;
            case 'P' :
            case 'Paard' : $speciesName = "horse"; break;
            case 'R' :
            case 'Rund' : $speciesName = "cattle"; break;
            case 'S' :
            case 'Reptiel' : $speciesName = "snake"; break;
            case 'T' :
            case 'Rat' : $speciesName = "rat"; break;
            case 'U' :
            case 'X' : $speciesName = "dolphin"; break;
            case 'Y' : $speciesName = "seal"; break;
            case 'Z' :
            case 'Varken' : $speciesName = "pig"; break;

            default: $speciesName = "blank"; break;
        }
        return $speciesName.".png";
    }


    public function formIsNewFilter(ParameterProfileForm $form) {
        return !in_array($form->getReference(), $this->formsDisplayed);
    }

    public function autocompleteFilter(ParameterProfileForm $form, RequestPet $pet) {
        if (in_array($form->getReference(), $this->formsDisplayed)) {
            return "already done";
        }
        $this->formsDisplayed[] = $form->getReference();

        $doc = new DOMDocument();
        $doc->loadHTML($form->getContent());

        /** @var \DomElement $element */
        foreach ($doc->getElementsByTagName('form') as $element) {
            $element->setAttribute('id',$form->getReference());
        }

        /** @var \DomElement $element */
        foreach ($doc->getElementsByTagName('input') as $element) {
            $name = $element->getAttribute("name");
            switch ($name) {
                case "pe.name" :
                    $element->setAttribute('value',$pet->getName());
                    break;
                case "pe.chipId" :
                    $element->setAttribute('value',$pet->getChipNbr());
                    break;
                case "pe.birthDate" :
                    $element->setAttribute('value',$pet->getBirthDate() ? $pet->getBirthDate()->format('Y-m-d') : '');
                    break;
                case "pe.gender" :
                    $gender = strtolower($element->getAttribute('value'));
                    if ($gender=='f' && strtolower($pet->getGender())=='v') {
                        $element->setAttribute('checked',true);
                    }
                    if ($gender=='m' && strtolower($pet->getGender())=='m') {
                        $element->setAttribute('checked',true);
                    }
                    break;
            }
            //echo "name: $name<br>";
        }

        return  utf8_decode($doc->saveHTML($doc->documentElement));
    }

    public function formatDelayFilter(ParameterPage $parameterPage) {
        $this->translator->setLocale($parameterPage->getLanguage());

        $delay = $parameterPage->getDelay();
        $formattedDelay = $delay ? $delay . " " . ($delay == 1 ? $this->translator->trans('wiki.day') : $this->translator->trans('wiki.days')) : $this->translator->trans('wiki.sameday');
        return $formattedDelay;
    }

    public function kommaToDotFilter($value) {
        return floatval(str_replace(',', '.',$value));
    }

    public function accountNameFilter(User $user) {
        return $user ? ($name = $user->getFirstName().' '.$user->getLastName()) == ' ' ? $user->getUserName() : $name : '';
    }

    public function getName()
    {
        return 'app_extension';
    }

    public function formatPriceFilter($value)
    {
      return number_format(str_replace(',', '.',$value), 2);
    }
    public function priceFilter($value)
    {
        return $this->formatPriceFilter($value/100)." €";
    }

    public function telephoneNumbersFilter(Veterinary $veterinary) {
        $telephoneNumbers = array();
        /** @var Veterinary $veterinary */
        $telephone = $veterinary->getTelephone();
        if ($telephone != '' and !in_array($telephone,$telephoneNumbers)) {
            $telephoneNumbers[] = (object) array("value" => $telephone, "description" => $veterinary->getFirstName()." ".$veterinary->getLastName());
        }
        /** @var VeterinaryPractice $veterinaryPractice */
        foreach ($veterinary->getVeterinaryPractices() as $veterinaryPractice) {
            $telephone = $veterinaryPractice->getPractice()->getTelephone();
            if ($telephone != '' and !in_array($telephone,$telephoneNumbers)) {
                $telephoneNumbers[] = (object) array("value" => $telephone, "description" => $veterinaryPractice->getPractice()->getName());
            }
        }
        return $telephoneNumbers;
    }


    public function isOwnerFilter(User $user) {

    }

    public function eventColorFilter(TicketEvent $event) {
       //{% if event.internal %}#f2dede;{% else %}#d8edff;{% endif %}
        if ($event instanceof MessageEvent && $event->isInternal()) {
            return "#dedede";
        }

        if ($event->getAuthor()->hasRole("ROLE_SUPER_ADMIN") || $event->getAuthor()->hasRole("ROLE_ADMINISTRATOR")) {
            return "#ffedd8";
        }
        return "#d8edff";
    }

    public function FirstCharFilter($string) {
        $firstCharacter = strlen($string)>0 ? $string[0] : ' ';
        return $firstCharacter;
    }

    public function watcherStateFilter(WatchTicket $watcher, TicketEvent $event)
    {
        return !is_null($watcher->getLastMessageEvent()) && $watcher->getLastMessageEvent()->getId() >= $event->getId();
    }

    public function filterBadTagsFilter($content)
    {
        $badTags = array('base', 'style');
        foreach ($badTags as $tag) {
            $content = preg_replace('~<'.$tag.'(.*?)</'.$tag.'>~si', "", $content);
        }
        return $content;
    }
    public function linkUrlsFilter($content)
    {
        $content = $this->urlLinker->linkUrlsInTrustedHtml($content);
        return $content;
    }
    public function fillcolFilter($content) {
        return $content != "" ? $content : "&nbsp;";
    }

    public function isNoWorkingDayFilter($day) {
        $date = new \DateTime($day);
        return !$this->dateProvider->isAWorkingDay($date);
    }
}