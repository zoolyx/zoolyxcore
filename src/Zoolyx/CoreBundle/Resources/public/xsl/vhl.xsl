<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" cdata-section-elements="pdfDnaProfile"/>
    <xsl:template match="/">
        <request>
            <orders>
                <xsl:attribute name="createdOn"><xsl:value-of select="rq/@createdOn"/></xsl:attribute>
                <xsl:attribute name="customer">Zoolyx-KMSH</xsl:attribute>
                <xsl:attribute name="vhlCustomerID">26799</xsl:attribute>
                <order>
                    <xsl:attribute name="orderID"><xsl:value-of select="translate(rq/@id,'kmsh-+','')"/></xsl:attribute>
                    <xsl:attribute name="chipperID">0</xsl:attribute>
                    <xsl:attribute name="sampleCount"><xsl:value-of select="count(rq/sc)"/></xsl:attribute>

                    <xsl:for-each select="rq/sc">
                        <sample>
                            <xsl:call-template name="makePet" />
                            <xsl:choose>
                                <!-- check of voor dit sample verwantschap moet worden onderzocht -->
                                <xsl:when test="pg/pa/pp = 'H5001'">
                                    <rvo>
                                        <xsl:attribute name="rvoCode">205</xsl:attribute>
                                    </rvo>

                                    <!-- find all candidate fathers -->
                                    <xsl:for-each select="pg/pa">
                                        <xsl:if test="substring(@id,1,3)='SID'">
                                            <rvo>
                                                <xsl:attribute name="rvoCode">200</xsl:attribute>

                                                <xsl:call-template name="findFather" />

                                                <!-- find the mother -->
                                                <xsl:for-each select="../../pg/pa">
                                                    <xsl:if test="@id='DID1'">
                                                        <xsl:call-template name="findMother" />
                                                    </xsl:if>
                                                </xsl:for-each>

                                            </rvo>
                                        </xsl:if>
                                    </xsl:for-each>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <!-- dna moet bepaald worden -->
                                        <xsl:when test="pg/pa/pp = 'H3201' or pg/pa/@id = 'AHT137'">
                                            <rvo>
                                                <xsl:attribute name="rvoCode">205</xsl:attribute>
                                            </rvo>
                                        </xsl:when>
                                        <!-- dna is beschikbaar in pdf -->
                                        <xsl:when test="pg/pa/@id = 'DNAFPF' and not(pg/pa[@id = 'DNAFPF']/result = 'x' or pg/pa[@id = 'DNAFPF']/result = 'OO')">
                                            <rvo>
                                                <xsl:attribute name="rvoCode">102</xsl:attribute>
                                            </rvo>
                                            <pdfDnaProfile><xsl:value-of select="pg/pa[@id = 'DNAFPF']/files/file"/></pdfDnaProfile>
                                        </xsl:when>
                                        <!-- dna is beschikbaar in data -->
                                        <xsl:when test="pg/pa/@id = 'AHT121'">
                                            <rvo>
                                                <xsl:attribute name="rvoCode">102</xsl:attribute>
                                            </rvo>
                                            <dnaProfile>
                                                <xsl:for-each select="pg/pa">
                                                    <xsl:if test="co/@pa != '' and @id != 'HDNA'">
                                                        <xsl:variable name="parameterId" select="co/@pa"/>
                                                        <xsl:attribute name="{$parameterId}"><xsl:value-of select="result"/></xsl:attribute>
                                                    </xsl:if>
                                                </xsl:for-each>
                                            </dnaProfile>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </sample>
                    </xsl:for-each>
                </order>
            </orders>
        </request>
    </xsl:template>

    <xsl:template name="findFather">
        <father>
            <xsl:call-template name="findPet" />
        </father>
    </xsl:template>
    <xsl:template name="findMother">
        <mother>
            <xsl:call-template name="findPet" />
        </mother>
    </xsl:template>

    <xsl:template name="findPet">
        <xsl:variable name="petId" select="translate(result,' ','')"/>
        <xsl:for-each select="../../../sc">
            <xsl:variable name="chipId" select="translate(pe/chipID,' ','')"/>
            <xsl:if test="$chipId=$petId">
                <xsl:call-template name="makePet" />
            </xsl:if>
        </xsl:for-each>

        <xsl:for-each select="../../../sc">
            <xsl:variable name="tattooID" select="translate(pe/tattooID,' ','')"/>
            <xsl:if test="$tattooID=$petId">
                <xsl:call-template name="makePet" />
            </xsl:if>
        </xsl:for-each>

        <xsl:for-each select="../../../sc">
            <xsl:variable name="pedigreeID" select="translate(pe/pedigreeID,' ','')"/>
            <xsl:if test="$pedigreeID=$petId">
                <xsl:call-template name="makePet" />
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="makePet">
        <xsl:attribute name="vhlID" />
        <xsl:attribute name="extRef"><xsl:value-of select="@id"/></xsl:attribute>
        <xsl:attribute name="chipNumber"><xsl:value-of select="translate(pe/chipID,' ','')"/></xsl:attribute>
        <xsl:attribute name="replChipNumber"><xsl:value-of select="translate(pe/oldChipId,' ','')"/></xsl:attribute>
        <xsl:attribute name="pedigreeNumber"><xsl:value-of select="translate(pe/pedigreeID,' ','')"/></xsl:attribute>
        <xsl:attribute name="tattooNumber"><xsl:value-of select="translate(pe/tattooID,' ','')"/></xsl:attribute>
        <xsl:attribute name="name"><xsl:value-of select="pe/name"/></xsl:attribute>
        <xsl:attribute name="sexe"><xsl:value-of select="translate(pe/gender,'FM','VM')"/></xsl:attribute>
        <xsl:attribute name="breedCode"><xsl:value-of select="pe/breed_code"/></xsl:attribute>
    </xsl:template>
</xsl:stylesheet>

