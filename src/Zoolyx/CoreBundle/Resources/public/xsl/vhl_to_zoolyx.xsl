<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <results>
            <xsl:for-each select="Results/sampleResults/sampleResult">
                <sc><!--->gewoon relatie samples en parameters met results-->
                    <xsl:attribute name="id"><xsl:value-of select="@extRef"/></xsl:attribute>
                    <xsl:attribute name="co_ref"><xsl:value-of select="@vhlID"/></xsl:attribute>
                    <xsl:if test="rvo/@rvoCode='102'"><!-- dna op basis van document -->
                        <pa>
                            <co id="DB">
                                <xsl:attribute name="pa">102</xsl:attribute>
                            </co>
                            <result>
                                <xsl:value-of select="rvo/@result"/>
                            </result>
                        </pa>
                        <xsl:for-each select="dnaProfile/@*">
                            <pa>
                                <co id="DB">
                                    <xsl:attribute name="pa"><xsl:value-of select="name()"/></xsl:attribute>
                                </co>
                                <result>
                                    <xsl:value-of select="."/>
                                </result>
                            </pa>
                        </xsl:for-each>
                    </xsl:if>
                    <xsl:if test="rvo/@rvoCode='205'"><!-- dna op basis van sample -->
                        <pa>
                            <co id="DB">
                                <xsl:attribute name="pa">205</xsl:attribute>
                            </co>
                            <result>
                                <xsl:value-of select="rvo/@result"/>
                            </result>
                        </pa>
                        <xsl:for-each select="dnaProfile/@*">
                            <pa>
                                <co id="DB">
                                    <xsl:attribute name="pa"><xsl:value-of select="name()"/></xsl:attribute>
                                </co>
                                <result>
                                    <xsl:value-of select="."/>
                                </result>
                            </pa>
                        </xsl:for-each>
                    </xsl:if>
                    <xsl:if test="rvo/@rvoCode='200'"><!-- afstamming -->
                        <pa>
                            <co id="DB">
                                <xsl:attribute name="pa">200</xsl:attribute>
                                <xsl:variable name="fChipId" select="rvo/father/@chipNumber" />
                                <xsl:variable name="fPedigreeId" select="rvo/father/@pedigreeNumber" />
                                <xsl:variable name="fTattooId" select="rvo/father/@tattooNumber" />
                                <xsl:attribute name="sir"><xsl:value-of select="concat($fChipId, '@', $fPedigreeId, '@', $fTattooId)"/></xsl:attribute>
                                <xsl:variable name="mChipId" select="rvo/mother/@chipNumber" />
                                <xsl:variable name="mPedigreeId" select="rvo/mother/@pedigreeNumber" />
                                <xsl:variable name="mTattooId" select="rvo/mother/@tattooNumber" />
                                <xsl:attribute name="dam"><xsl:value-of select="concat($mChipId, '@', $mPedigreeId, '@', $mTattooId)"/></xsl:attribute>
                            </co>
                            <result>
                                <xsl:value-of select="rvo/@result"/>
                            </result>
                        </pa>
                    </xsl:if>
                </sc>
            </xsl:for-each>
        </results>
    </xsl:template>
</xsl:stylesheet>

