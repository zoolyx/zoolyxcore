
$(function() {
    var $categoriesContainer = $('#categories-container');
    console.log(selectedProfiles);
    _.each(categories, function(category, categoryIndex) {
        //category.expanded = categoryIndex == 0;
        category.visible = true;
        category.html = buildCategoryHtml(category);
        $categoriesContainer.append(category.html);

        var selectedParameters = [];
        _.each(parameters_in_report, function(pa) {
            selectedParameters.push(pa.id);
        });

        _.each(category.profiles, function(profile) {
            profile.selected = _.contains(selectedProfiles,profile.reference);
            profile.html = buildProfileHtml(profile, true, selectedParameters);
            profile.visible = true;
            $categoriesContainer.append(profile.html);
        });
    });
    console.log(categories);
    update();


    $('#confirm').click(function() {
        var profiles = getSelectedProfiles();
        var profileReferences = [];
        _.each(profiles, function(profile){
            profileReferences.push(profile.reference)
        });
        var action = confirmAction;
        var $form = $('<form action="'+action+'" method="POST"></form>');
        $form.append('<input type="hidden" name="profiles" value="'+profileReferences.join(',')+'">');
        $form.appendTo("body").submit();
    });

    console.log('will set');
    $('#search-button').click(function() {
        console.log("will search");
        search();
    });
    $('#keyword').keypress(function(e) {
        console.log('is see char');
        if(e.which == 13) {
            search();
        }
    });
    var $removeFilter = $('#remove_filter');
    $removeFilter.attr("disabled", "disabled");
    $removeFilter.click(function() {
        clearBreedFilter();
        $removeFilter.attr("disabled", "disabled");
    });

    updateBreedList();

    var $breedContainer = $('#breed-container');
    var $breed = $('#breed');

    $breed.focus(function() { updateBreedList(); $breedContainer.show();});
    $breed.blur( function() { $breedContainer.delay(500).fadeOut('slow');});
    $breed.keyup(function(e){
        var keyword = $(this).val();
        console.log("changed to "+keyword);
        updateBreedList();
    });

});

function getSelectedProfiles() {
    var selectedProfiles = [];
    _.each(categories, function(category) {
        _.each(category.profiles,function(profile) {
            if (profile.selected) {
                selectedProfiles.push(profile);
            }
        })
    });
    return selectedProfiles;
}
function getSelectedParameters() {
    var selectedParameters = [];
    _.each(parameters_in_report, function(pa) {
        selectedParameters.push(pa.id);
    });
    _.each(getSelectedProfiles(), function(selectedProfile) {
        _.each(selectedProfile.paList, function(paId) {
            selectedParameters.push(paId);
        })
    });
    return selectedParameters;
}
function buildCategoryHtml(category) {
    var $categoryContainer = $('<div class="col-xs-12"></div>');
    var $categoryRow = $('<div class="row myzoolyx_row category"></div>');
    $categoryContainer.append($categoryRow);
    $categoryRow.append('<div class="col-xs-10">'+category.description+'</div>');

    return $categoryContainer;
}
function buildProfileHtml(profile, interactive, selectedParameters) {
    var $profileContainer= $('<div class="col-xs-12"></div>');
    var $profileRow = $('<div class="row myzoolyx_row"></div>');
    if (profile.selected) {
        $profileRow.addClass("parameter-profile-selected");
    } else {
        $profileRow.removeClass("parameter-profile-selected");
    }

    var language2 = typeof language !== "undefined" ? language :'nl';

    $profileContainer.append($profileRow);
    if (interactive) $profileRow.addClass("parameter-profile");
    $profileRow.append('<div class="hidden-xs col-sm-1 text-right"><a href="../../../'+language2+'/wiki/PP_'+profile.reference+'" target="_blank">'+profile.reference+'</a></div>');
    $profileRow.append('<div class="col-xs-9 col-sm-8">'+profile.description+'</div>');

    $profileRow.append('<div class="col-xs-3 col-sm-3 text-right">'+parseFloat(profile.price).toFixed(2)+'€</div>');

    $profileRow.append('<div class="col-xs-9 col-xs-offset-0 col-sm-8 col-sm-offset-1 parameter-description-2">'+profile.description2+'</div>');

    var $parametersUsed = $('<div class="col-xs-11 col-xs-offset-1 parameters-used">'+samplesUsedMessage+'</div>').hide();
    profile.parametersUsed = $parametersUsed;
    $profileRow.append($parametersUsed);
    var $allParametersUsed = $('<div class="col-xs-11 col-xs-offset-1 parameters-used">'+allParametersUsedMessage+'</div>').hide();
    profile.allParametersUsed = $allParametersUsed;
    $profileRow.append($allParametersUsed);
    var $overrides = $('<div class="col-xs-11 col-xs-offset-1 parameters-used">'+overrides+'</div>').hide();
    profile.overrides = $overrides;
    $profileRow.append($overrides);

    $profileRow.click(function() {
        profile.selected = !profile.selected;
        update();
    });
    profile.profileRow = $profileRow;
    return $profileContainer;
}
function buildSelectedProfileHtml(profile, interactive) {
    var $profileContainer= $('<div class="col-xs-12"></div>');
    var $profileRow = $('<div class="row myzoolyx_row"></div>');
    var language2 = typeof language !== "undefined" ? language:'nl';

    $profileContainer.append($profileRow);
    if (interactive) $profileRow.addClass("parameter-profile");
    $profileRow.append('<div class="hidden-xs col-sm-1 text-right"><a href="../../../'+language2+'/wiki/PP_'+profile.reference+'" target="_blank">'+profile.reference+'</a></div>');
    $profileRow.append('<div class="col-xs-9 col-sm-8">'+profile.description+'</div>');

    $profileRow.append('<div class="col-xs-3 col-sm-3 text-right">'+parseFloat(profile.price).toFixed(2)+'€</div>');

    $profileRow.append('<div class="col-xs-9 col-xs-offset-0 col-sm-8 col-sm-offset-1 parameter-description-2">'+profile.description2+'</div>');

    $profileRow.click(function() {
        profile.selected = !profile.selected;
        update();
    });
    return $profileContainer;
}

function update() {
    updateSelectedProfiles();
    refreshProfiles();
}
function updateSelectedProfiles() {
    var selectedProfiles = getSelectedProfiles();
    var $selectedProfilesContainer = $('#selected-profiles-container');

    var sum = 0;
    if (selectedProfiles.length) {
        $selectedProfilesContainer.html('');
        _.each(selectedProfiles, function(profile) {
            $selectedProfilesContainer.append(buildSelectedProfileHtml(profile, true));
            sum+= parseFloat(profile.price);
        });
        var $profileRow = $('<div class="col-xs-12">'+instruction_del+'</div>');
        $selectedProfilesContainer.append($('<div class="col-xs-12"></div>').append($profileRow));
    } else {
        $selectedProfilesContainer.html(buildEmptyProfiles());
    }

    var $totalPrice = $('#total');
    $totalPrice.html(sum.toFixed(2));

}

function buildEmptyProfiles() {
    var $profileRow = $('<div class="col-xs-12">'+instruction_add+'</div>');
    return $('<div class="col-xs-12"></div>').append($profileRow);
}

function search() {
    $('#breed').val('');
    var keyword = $('#keyword').val();
    keyword = keyword.toLowerCase();
    console.log('keyword: '+keyword);
    _.each(categories, function(category) {
        _.each(category.profiles,function(profile) {
            var found = false;
            found = profile.description.toLowerCase().indexOf(keyword) >= 0 || found;
            found = profile.description2.toLowerCase().indexOf(keyword) >= 0 || found;
            found = profile.reference.toLowerCase().indexOf(keyword) >= 0 || found;
            profile.visible = found;
        })
    });
    refreshProfiles();
}

function updateBreedList() {
    var $breedInput = $('#breed');
    var keyword = $breedInput.val().toLowerCase();
    var $breedList = $('#breed-list');
    $breedList.empty();
    if (currentBreed != '') {
        _.each(breeds, function(breed) {
            if (breed.breed == currentBreed) {
                var $breedListEntry = $('<a href="#" class="list-group-item"></a>');
                $breedListEntry.html(breed.description + " (current pet)");
                $breedListEntry.click(function() {
                    filterOnBreed(breed);
                });
                $breedList.append($breedListEntry);
            }
        })
    }
    _.each(breeds, function(breed) {
        if (breed.breed != currentBreed &&breed.description.toLowerCase().indexOf(keyword) >= 0) {
            var $breedListEntry = $('<a href="#" class="list-group-item"></a>');
            $breedListEntry.html(breed.description);
            $breedListEntry.click(function() {
                filterOnBreed(breed);
            });
            $breedList.append($breedListEntry);
        }
    });
}
function filterOnBreed(breed) {
    $('#keyword').val('');
    $('#remove_filter').attr("disabled", false);
    $('#breed').val(breed.description);
    console.log('filtering on '+breed.description);
    _.each(categories, function(category) {
        _.each(category.profiles,function(profile) {
            var found = false;
            _.each(profile.breedList, function(b) {
                var parts = b.split("_");
                var breedId = parts[1];
                if (breedId == "*" || breedId == breed.breed) {
                    found = true;
                }
            });
            profile.visible = found;
        })
    });
    refreshProfiles();
}
function clearBreedFilter() {
    $('#breed').val('');
    _.each(categories, function(category) {
        _.each(category.profiles,function(profile) {
            profile.visible = true;
        })
    });
    refreshProfiles();
}

function refreshProfiles() {
    var selectedParameters = getSelectedParameters();
    _.each(categories, function(category) {
        var hasVisibleProfile = false;
        _.each(category.profiles,function(profile) {
            //load html
            var $profileRow = $(profile.html.children()[0]);

            //set color
            if (profile.selected) {
                $profileRow.addClass("parameter-profile-selected");
            } else {
                $profileRow.removeClass("parameter-profile-selected");
            }

            //check if visible
            if (profile.visible || profile.selected) {
                profile.html.show();
                hasVisibleProfile = true;
            } else {
                profile.html.hide();
            }

            //check if parameters are already used
            var parametersUsed = [];
            var allParametersAreUsed = true;

            _.each(profile.paList, function(paId) {
                var parameterAlreadyUsed = false;
                _.each(selectedParameters, function(pa) {
                    if (pa == paId) {
                        parametersUsed.push(pa);
                        parameterAlreadyUsed = true;
                    }
                });
                if (!parameterAlreadyUsed) {
                    allParametersAreUsed = false;
                }
            });

            if (!profile.selected) {
                if (parametersUsed.length) { profile.parametersUsed.show() } else { profile.parametersUsed.hide() }
                if (allParametersAreUsed) { profile.allParametersUsed.show();profile.parametersUsed.hide() } else { profile.allParametersUsed.hide() }

                //check if profile is overriding a selected profile
                var overrides = false;
                _.each(getSelectedProfiles(),function(selectedProfile) {
                    var allPaUsed = true;
                    _.each(selectedProfile.paList,function(pa) {
                        allPaUsed = !_.contains(profile.paList,pa) ? false : allPaUsed;
                    });
                    overrides = allPaUsed ? true : overrides;
                });

                if (overrides) {
                    profile.overrides.show();
                } else {
                    profile.overrides.hide();
                }

                profile.profileRow.unbind("click");
                if (!overrides && !allParametersAreUsed) {
                    profile.profileRow.attr('style','cursor:pointer');
                    profile.profileRow.click(function() {
                        profile.selected = !profile.selected;
                        update();
                    });
                } else {
                    profile.profileRow.attr('style','cursor:default');
                }
            }




        });
        if (hasVisibleProfile) {
            category.html.show();
        } else {
            category.html.hide();
        }
    });
}
