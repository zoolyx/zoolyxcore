/**
 * Created by bart on 06.04.16.
 */

//var myDropZone = new Dropzone("#drop_zone", { url: "/file/post"});

/*myDropZone.on("complete", function(file) {
    console.log('drop completed');
    myDropZone.removeFile(file);
});
    */

/*
$(function() {
    // Now that the DOM is fully loaded, create the dropzone, and setup the
    // event listeners
    var myDropzone = new Dropzone("#dropzone");
    myDropzone.on("addedfile", function(file) {
        console.log("Added file.");
    });
});
*/


Dropzone.options.dropzone = {
    init: function() {
        this.on("addedfile", function(file) { console.log("Added file."); });
        this.on("success", function(result, response) {
            //still check for error
            window.location.href = response;
        });
        this.on("complete", function(file) {
            console.log('drop completed');
            this.removeFile(file);
        });
    }
};
