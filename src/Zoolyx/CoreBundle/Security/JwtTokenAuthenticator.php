<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 07.12.16
 * Time: 10:21
 */
namespace Zoolyx\CoreBundle\Security;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\Persistence\ObjectManager;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Zoolyx\CoreBundle\Model\Exception\NotAllowedException;

/**
 * @DI\Service("jwt_token_authenticator")
 */
class JwtTokenAuthenticator extends AbstractGuardAuthenticator
{
    /** @var JWTEncoderInterface  */
    private $jwtEncoder;
    /** @var  ObjectManager */
    private $entityManager;

    /**
     * Constructor.
     *
     * @param JWTEncoderInterface $jwtEncoder
     * @param ObjectManager $entityManager
     *
     * @DI\InjectParams({
     *  "jwtEncoder" = @DI\Inject("lexik_jwt_authentication.encoder"),
     *  "entityManager" = @DI\Inject("doctrine.orm.entity_manager")
     * })
     */

    public function __construct(JWTEncoderInterface $jwtEncoder, ObjectManager $entityManager)
    {
        $this->jwtEncoder = $jwtEncoder;
        $this->entityManager = $entityManager;
    }

    public function getCredentials(Request $request)
    {
        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );
        $token = $extractor->extract($request);
        if (!$token) {
            return null;
        }
        return $token;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        try {
            $data = $this->jwtEncoder->decode($credentials);
        } catch (JWTDecodeFailureException $e) {
            /*
             *  if you want to, use can use $e->getReason() to find out which of the 3 possible things went wrong
             *  and tweak the message accordingly
             *  https://github.com/lexik/LexikJWTAuthenticationBundle/blob/05e15967f4dab94c8a75b275692d928a2fbf6d18/Exception/JWTDecodeFailureException.php
             */
            throw new CustomUserMessageAuthenticationException('Invalid Token');
        }
        $username = $data['username'];
        return $this->entityManager
            ->getRepository('ZoolyxCoreBundle:User')
            ->findOneBy(['username' => $username]);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new JsonResponse([
            'error' => 'auth failed'
        ], 401);
        // TODO: Implement onAuthenticationFailure() method.
    }
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        //die('authentication success');
        // TODO: Implement onAuthenticationSuccess() method.
    }
    public function supportsRememberMe()
    {
        return false;
    }
    public function start(Request $request, AuthenticationException $authException = null)
    {
        // called when authentication info is missing from a
        // request that requires it
        return new JsonResponse([
            'error' => 'auth required'
        ], 401);
    }
} 