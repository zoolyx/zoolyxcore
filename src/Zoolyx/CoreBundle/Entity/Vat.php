<?php

namespace Zoolyx\CoreBundle\Entity;

class Vat
{
    /** @var integer */
    private $id;

    /** @var string */
    private $faId;

    /** @var string */
    private $vat;

    /** @var array */
    private $veterinaryPractices = array();

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFaId()
    {
        return $this->faId;
    }

    /**
     * @param string $faId
     * @return $this
     */
    public function setFaId($faId)
    {
        $this->faId = $faId;
        return $this;
    }

    /**
     * @return string
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param string $vat
     * @return $this
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }


}

