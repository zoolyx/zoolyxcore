<?php

namespace Zoolyx\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class ContractorRequest
{
    /** @var int */
    private $id;

    /** @var int */
    private $status;

    /** @var \DateTime */
    private $created;

    /** @var \DateTime */
    private $confirmed;

    /** @var Contractor */
    private $contractor;

    /** @var Collection */
    private $contractorReports;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param int $status
     * @return ContractorRequest
     */
    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return ContractorRequest
     */
    public function setCreated($created) {
        $this->created = $created;
        return $this;
    }
    /**
     * @return \DateTime
     */
    public function getConfirmed() {
        return $this->confirmed;
    }

    /**
     * @param \DateTime $confirmed
     * @return ContractorRequest
     */
    public function setConfirmed($confirmed) {
        $this->confirmed = $confirmed;
        return $this;
    }

    /**
     * @return Contractor
     */
    public function getContractor() {
        return $this->contractor;
    }

    /**
     * @param Contractor $contractor
     * @return ContractorRequest
     */
    public function setContractor($contractor) {
        $this->contractor = $contractor;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getContractorReports() {
        return $this->contractorReports;
    }

    /**
     * @param ContractorReport $contractorReport
     * @return ContractorRequest
     */
    public function addContractorReport($contractorReport) {
        $this->contractorReports[] = $contractorReport;
        return $this;
    }
}

