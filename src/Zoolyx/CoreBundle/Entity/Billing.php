<?php

namespace Zoolyx\CoreBundle\Entity;

use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class Billing extends AddressedUser
{
    /**
     * @var string
     */
    private $faId;

    /**
     * @var string
     */
    private $vatNr;

    /**
     * @return string
     */
    public function getFaId() {
        return $this->faId;
    }

    /**
     * @param string $faId
     * @return Billing
     */
    public function setFaId($faId) {
        $this->faId = $faId;
        return $this;
    }

    /**
     * @return string
     */
    public function getVatNr() {
        return $this->vatNr;
    }

    /**
     * @param string $vatNr
     * @return Billing
     */
    public function setVatNr($vatNr) {
        $this->vatNr = $vatNr;
        return $this;
    }
}

