<?php

namespace Zoolyx\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class Veterinary
{
    /**
     * @var int
     * @Serializer\Expose
     */
    private $id;

    /** @var User */
    private $account = null;

    /** @var string */
    private $title = "";

    /**
     * @var string
     * @Serializer\Expose
     */
    private $firstName = "";

    /**
     * @var string
     * @Serializer\Expose
     */
    private $lastName = "";

    /**
     * @var string
     */
    private $language = "nl";

    /** @var null | string */
    private $orderNumber = null;

    /** @var string */
    private $telephone;

    /** @var Collection  */
    private $veterinaryPractices = array();

    /** @var Collection */
    private $geoOpLogs = array();

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param $account
     * @return $this
     */
    public function setAccount($account)
    {
        $this->account = $account;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Veterinary
     */
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param $language
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param null|string $orderNumber
     * @return $this
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     * @return $this
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }


    /**
     * @param VeterinaryPractice $veterinaryPractice
     * @return Veterinary
     */
    public function addVeterinaryPractice(VeterinaryPractice $veterinaryPractice)
    {
        $this->veterinaryPractices[] = $veterinaryPractice;
        return $this;
    }

    /**
     * @param VeterinaryPractice $veterinaryPractice
     * @return Veterinary
     */
    public function removeVeterinaryPractice(VeterinaryPractice $veterinaryPractice)
    {
        $this->veterinaryPractices->removeElement($veterinaryPractice);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getVeterinaryPractices()
    {
        return $this->veterinaryPractices;
    }

    /**
     * @return VeterinaryPractice
     */
    public function getDefaultVeterinaryPractice() {
        /** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = null;
        if (count($this->veterinaryPractices)) {
            $veterinaryPractice =  $this->veterinaryPractices[0];
        } else {
            $veterinaryPractice = new VeterinaryPractice();
            $veterinaryPractice->setLimsId('unknown');
            $veterinaryPractice->setRole(0);
            $veterinaryPractice->setVeterinary($this);
            $practice = new Practice();
            $practice->setName('Default practice')
                ->setDescription('')
                ->setStreet('')
                ->setCity('')
                ->setZipCode('')
                ->setCity('BE')
                ->setCountry('')
            ;
            $veterinaryPractice->setPractice($practice);
            $this->addVeterinaryPractice($veterinaryPractice);
        }
        return $veterinaryPractice;
    }

    /**
     * @return Collection
     */
    public function getGeoOpLogs()
    {
        return $this->geoOpLogs;
    }

}
