<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * Parameter
 */
class ParameterProfile
{
    /** @var integer */
    private $id;

    /** @var string */
    private $reference;

    /** @var ParameterProfileCategory */
    private $category;

    /** @var string */
    private $species;

    /** @var array */
    private $descriptions = array();

    /** @var array */
    private $priceList = array();

    /** @var string */
    private $url = null;

    /** @var string */
    private $reservedFor = null;

    /** @var array */
    private $parameters = array();

    /** @var array */
    private $sampleTypes = array();

    /** @var array */
    private $breeds = array();

    /** @var ParameterProfileForm */
    private $form = null;

    /** @var bool */
    private $veterinaryCanOrder = true;

    /** @var bool */
    private $ownerCanOrder = false;

    /** @var bool */
    private $veterinaryMustPay = false;

    /** @var bool */
    private $ownerMustPay = true;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getReference() {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return ParameterProfile
     */
    public function setReference($reference) {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return ParameterProfileCategory
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * @param ParameterProfileCategory $category
     * @return ParameterProfile
     */
    public function setCategory($category) {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpecies() {
        return $this->species;
    }

    /**
     * @param string $species
     * @return ParameterProfile
     */
    public function setSpecies($species) {
        $this->species = $species;
        return $this;
    }

    /**
     * @return array
     */
    public function getDescriptions() {
        return $this->descriptions;
    }

    public function getDefaultDescription() {
        return count($this->descriptions) ? $this->descriptions[0] : null;
    }
    public function getDescription($language) {
        /** @var ParameterProfileDescription $description */
        foreach ($this->descriptions as $description) {
            if ($description->getLanguage() == $language) {
                return $description;
            }
        }
        return $this->getDefaultDescription();
    }

    /**
     * @param ParameterProfileDescription $description
     * @return ParameterProfile
     */
    public function addDescription($description) {
        $this->descriptions[] = $description;
        return $this;
    }

    /**
     * @return array
     */
    public function getPriceList() {
        return $this->priceList;
    }

    /**
     * @param ParameterProfilePrice $price
     * @return ParameterProfile
     */
    public function addPrice($price) {
        $this->priceList[] = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * @param string $url
     * @return ParameterProfile
     */
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getReservedFor()
    {
        return $this->reservedFor;
    }

    /**
     * @param string $reservedFor
     * @return $this
     */
    public function setReservedFor($reservedFor)
    {
        $this->reservedFor = $reservedFor;
        return $this;
    }

    /**
     * @return array
     */
    public function getParameters() {
        return $this->parameters;
    }

    /**
     * @param ParameterBase $parameter
     * @return ParameterProfile
     */
    public function addParameter(ParameterBase $parameter) {
        $this->parameters[] = $parameter;
        return $this;
    }

    /**
     * @return array
     */
    public function getSampleTypes() {
        return $this->sampleTypes;
    }

    /**
     * @param ParameterProfileSampleType $sampleType
     * @return ParameterProfile
     */
    public function addSampleType($sampleType) {
        $this->sampleTypes[] = $sampleType;
        return $this;
    }

    /**
     * @return array
     */
    public function getBreeds() {
        return $this->breeds;
    }

    /**
     * @param Breed $breed
     * @return ParameterProfile
     */
    public function addBreed($breed) {
        $this->breeds[] = $breed;
        return $this;
    }

    /**
     * @return ParameterProfileForm
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param ParameterProfileForm $form
     * @return $this
     */
    public function setForm($form)
    {
        $this->form = $form;
        return $this;
    }

    /**
     * @return boolean
     */
    public function ownerCanOrder()
    {
        return $this->ownerCanOrder;
    }

    /**
     * @param boolean $ownerCanOrder
     */
    public function setOwnerCanOrder($ownerCanOrder)
    {
        $this->ownerCanOrder = $ownerCanOrder;
    }

    /**
     * @return boolean
     */
    public function ownerMustPay()
    {
        return $this->ownerMustPay;
    }

    /**
     * @param boolean $ownerMustPay
     */
    public function setOwnerMustPay($ownerMustPay)
    {
        $this->ownerMustPay = $ownerMustPay;
    }

    /**
     * @return boolean
     */
    public function veterinaryCanOrder()
    {
        return $this->veterinaryCanOrder;
    }

    /**
     * @param boolean $veterinaryCanOrder
     */
    public function setVeterinaryCanOrder($veterinaryCanOrder)
    {
        $this->veterinaryCanOrder = $veterinaryCanOrder;
    }

    /**
     * @return boolean
     */
    public function veterinaryMustPay()
    {
        return $this->veterinaryMustPay;
    }

    /**
     * @param boolean $veterinaryMustPay
     */
    public function setVeterinaryMustPay($veterinaryMustPay)
    {
        $this->veterinaryMustPay = $veterinaryMustPay;
    }

}

