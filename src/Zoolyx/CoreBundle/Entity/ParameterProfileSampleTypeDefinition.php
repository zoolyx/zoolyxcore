<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * ParameterProfileSampleTypeDefinition
 */
class ParameterProfileSampleTypeDefinition
{
    /** @var integer */
    private $id;

    /** @var  string */
    private $reference;

    /** @var string */
    private $imageName;

    /** @var bool */
    private $ownerCanOrder = false;

    /** @var array */
    private $descriptions;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getReference() {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return ParameterProfileCategory
     */
    public function setReference($reference) {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param string $imageName
     * @return $this
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
        return $this;
    }




    /**
     * @return array
     */
    public function getDescriptions() {
        return $this->descriptions;
    }

    /**
     * @return ParameterProfileSampleTypeDescription
     */
    public function getDefaultDescription() {
        return count($this->descriptions) ? $this->descriptions[0] : null;
    }

    /**
     * @param string $language
     * @return ParameterProfileSampleTypeDescription
     */
    public function getDescription($language) {
        /** @var ParameterProfileSampleTypeDescription $description */
        foreach ($this->descriptions as $description) {
            if ($description->getLanguage() == $language) {
                return $description;
            }
        }
        return $this->getDefaultDescription();
    }

    /**
     * @param ParameterProfileSampleTypeDescription $description
     * @return ParameterProfile
     */
    public function addDescription($description) {
        $this->descriptions[] = $description;
        return $this;
    }

    /**
     * @return boolean
     */
    public function ownerCanOrder()
    {
        return $this->ownerCanOrder;
    }

    /**
     * @param boolean $ownerCanOrder
     */
    public function setOwnerCanOrder($ownerCanOrder)
    {
        $this->ownerCanOrder = $ownerCanOrder;
    }
}

