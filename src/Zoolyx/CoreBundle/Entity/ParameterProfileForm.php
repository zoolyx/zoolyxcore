<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * ParameterProfileForm
 */
class ParameterProfileForm
{
    /** @var integer */
    private $id;

    /** @var string */
    private $reference;

    /** @var string */
    private $content;

    /** @var array */
    private $parameterProfiles = array();


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return array
     */
    public function getParameterProfiles()
    {
        return $this->parameterProfiles;
    }

}

