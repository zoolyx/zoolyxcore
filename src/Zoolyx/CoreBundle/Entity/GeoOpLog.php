<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * GeoOpLog
 */
class GeoOpLog
{
    private $id;
    private $jobId = null;
    private $jobReference = null;
    private $clientId = '';
    /** @var Veterinary */
    private $veterinary = null;
    /** @var VeterinaryPractice */
    private $veterinaryPractice = null;
    private $title = '';
    /** @var \DateTime */
    private $voorzien = null;
    private $ophaling = "";
    private $description = '';
    private $priority = 1;
    private $line1 = null;
    private $line2 = null;
    private $city = '';
    private $state = null;
    private $postCode = '';
    private $latitude = '';
    private $longitude = '';
    private $response = "";
    /** @var \DateTime */
    private $createdOn = null;


    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return null
     */
    public function getJobId()
    {
        return $this->jobId;
    }

    /**
     * @param null $jobId
     * @return $this
     */
    public function setJobId($jobId)
    {
        $this->jobId = $jobId;
        return $this;
    }

    /**
     * @return null
     */
    public function getJobReference()
    {
        return $this->jobReference;
    }

    /**
     * @param null $jobReference
     * @return $this
     */
    public function setJobReference($jobReference)
    {
        $this->jobReference = $jobReference;
        return $this;
    }

    /**
     * @return Veterinary
     */
    public function getVeterinary()
    {
        return $this->veterinary;
    }

    /**
     * @param Veterinary $veterinary
     * @return $this
     */
    public function setVeterinary($veterinary)
    {
        $this->veterinary = $veterinary;
        return $this;
    }

    /**
     * @return VeterinaryPractice
     */
    public function getVeterinaryPractice()
    {
        return $this->veterinaryPractice;
    }

    /**
     * @param VeterinaryPractice $veterinaryPractice
     * @return $this
     */
    public function setVeterinaryPractice($veterinaryPractice)
    {
        $this->veterinaryPractice = $veterinaryPractice;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getVoorzien()
    {
        return $this->voorzien;
    }

    /**
     * @param \DateTime $voorzien
     * @return $this
     */
    public function setVoorzien($voorzien)
    {
        $this->voorzien = $voorzien;
        return $this;
    }

    /**
     * @return string
     */
    public function getOphaling()
    {
        return $this->ophaling;
    }

    /**
     * @param string $ophaling
     * @return $this
     */
    public function setOphaling($ophaling)
    {
        $this->ophaling = $ophaling;
        return $this;
    }


    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     * @return $this
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param string $response
     * @return $this
     */
    public function setResponse($response)
    {
        $this->response = $response;
        return $this;
    }


    /**
     * @return null
     */
    public function getLine1()
    {
        return $this->line1;
    }

    /**
     * @param null $line1
     * @return $this
     */
    public function setLine1($line1)
    {
        $this->line1 = $line1;
        return $this;
    }

    /**
     * @return null
     */
    public function getLine2()
    {
        return $this->line2;
    }

    /**
     * @param null $line2
     * @return $this
     */
    public function setLine2($line2)
    {
        $this->line2 = $line2;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return null
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param null $state
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * @param string $postCode
     * @return $this
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     * @return $this
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     * @return $this
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * @param \DateTime $createdOn
     * @return $this
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
        return $this;
    }



}

