<?php

namespace Zoolyx\CoreBundle\Entity;


/**
 * File
 */
class File
{
    /** @var integer */
    private $id;

    /** @var string */
    private $name;

    /** @var Report */
    private $report;

    /** @var Parameter */
    private $parameter;

    /** @var  OriginalRequest */
    private $originalRequest;


    private $contentId;

    public function setContentId($cid) {
        $this->contentId = $cid;
    }
    public function getContentId() {
        return $this->contentId;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Report $report
     * @return File
     */
    public function setReport(Report $report) {
        $this->report = $report;
        return $this;
    }

    /**
     * @return Report
     */
    public function getReport() {
        return $this->report;
    }

    /**
     * @param Parameter $parameter
     * @return File
     */
    public function setParameter(Parameter $parameter) {
        $this->parameter = $parameter;
        return $this;
    }

    /**
     * @return Parameter
     */
    public function getParameter() {
        return $this->parameter;
    }

    /**
     * @return OriginalRequest
     */
    public function getOriginalRequest() {
        return $this->originalRequest;
    }

    /**
     * @param OriginalRequest $originalRequest
     * @return File
     */
    public function setOriginalRequest($originalRequest) {
        $this->originalRequest = $originalRequest;
        return $this;
    }

}

