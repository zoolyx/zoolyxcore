<?php

namespace Zoolyx\CoreBundle\Entity;
use Doctrine\Common\Collections\Collection;

/**
 * VeterinaryPractice
 */
class VeterinaryPractice
{
    const ROLE_PRACTICE_MEMBER = 0;
    const ROLE_PRACTICE_ADMIN = 1;

    const INVOICE_DEFAULT_VETERINARY = 1;
    const INVOICE_DEFAULT_OWNER = 2;


    /** @var integer */
    private $id;

    /** @var string */
    private $limsId = null;

    /** @var integer */
    private $role;

    /** @var Veterinary */
    private $veterinary;

    /** @var Practice */
    private $practice;

    /** @var Vat */
    private $vat;

    /** @var int */
    private $invoiceDefault = self::INVOICE_DEFAULT_OWNER;

    /** @var Collection */
    private $veterinaryPracticeReports = array();

    /** @var string */
    private $transferCode = null;
    /** @var \DateTime */
    private $validTill = null;

    /** @var string */
    private $notificationId = null;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLimsId() {
        return $this->limsId;
    }

    /**
     * @param string $limsId
     * @return VeterinaryPractice
     */
    public function setLimsId($limsId)
    {
        $this->limsId = $limsId;
        return $this;
    }

    /**
     * Set role
     *
     * @param integer $role
     * @return VeterinaryPractice
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * Get role
     *
     * @return integer
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return Veterinary
     */
    public function getVeterinary()
    {
        return $this->veterinary;
    }

    /**
     * @param $veterinary
     * @return $this
     */
    public function setVeterinary($veterinary)
    {
        $this->veterinary = $veterinary;
        return $this;
    }

    /**
     * @param Practice $practice
     * @return VeterinaryPractice
     */
    public function setPractice(Practice $practice)
    {
        $this->practice = $practice;
        return $this;
    }

    /**
     * @return Practice
     */
    public function getPractice()
    {
        return $this->practice;
    }

    /**
     * @return Vat
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param Vat $vat
     * @return $this
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }

    /**
     * @return int
     */
    public function getInvoiceDefault()
    {
        return $this->invoiceDefault;
    }

    /**
     * @param int $invoiceDefault
     * @return $this
     */
    public function setInvoiceDefault($invoiceDefault)
    {
        $this->invoiceDefault = $invoiceDefault;
        return $this;
    }

    /**
     * @param VeterinaryPracticeReport $veterinaryPracticeReport
     * @return VeterinaryPractice
     */
    public function addVeterinaryPracticeReport(VeterinaryPracticeReport $veterinaryPracticeReport) {
        $this->veterinaryPracticeReports[] = $veterinaryPracticeReport;
        return $this;
    }

    /**
     * @param VeterinaryPracticeReport $veterinaryPracticeReport
     * @return VeterinaryPractice
     */
    public function removeVeterinaryPracticeReport(VeterinaryPracticeReport $veterinaryPracticeReport) {
        $this->veterinaryPracticeReports->removeElement($veterinaryPracticeReport);
        return $this;
    }


    /**
     * @return Collection
     */
    public function getVeterinaryPracticeReports() {
        return $this->veterinaryPracticeReports;
    }

    /**
     * @return string
     */
    public function getTransferCode()
    {
        return $this->transferCode;
    }

    /**
     * @param string $transferCode
     * @return $this
     */
    public function setTransferCode($transferCode)
    {
        $this->transferCode = $transferCode;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getValidTill()
    {
        return $this->validTill;
    }

    /**
     * @param \DateTime $validTill
     * @return $this
     */
    public function setValidTill($validTill)
    {
        $this->validTill = $validTill;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotificationId()
    {
        return $this->notificationId;
    }

    /**
     * @param string $notificationId
     * @return $this
     */
    public function setNotificationId($notificationId)
    {
        $this->notificationId = $notificationId;
        return $this;
    }

}

