<?php

namespace Zoolyx\CoreBundle\Entity;
use Doctrine\Common\Collections\Collection;
use Zoolyx\CoreBundle\Model\Report\Parameter\ContractorRequired;

/**
 * Parameter
 */
class Parameter
{
    /** @var integer */
    private $id;

    /** @var string */
    private $parameterId;

    /** @var ParameterBase */
    private $parameterBase;

    /** @var float */
    private $result;

    /** @var string */
    private $resultString;

    /** @var string */
    private $resultFlag = null;

    /** @var string */
    private $status;

    /** @var string */
    private $parameterP;

    /** @var string  */
    private $price = '';

    /** @var ParameterReference */
    private $reference;

    /** @var string */
    private $comment;

    /** @var Validator  */
    private $validatedBy;

    /** @var Collection */
    private $files = array();


    /** @var ParameterGroup */
    private $parameterGroup;

    /** @var ContractorRequired */
    private $contractorRequired;

    /** @var  \DateTime */
    private $dateExpected;

    /** @var  \DateTime */
    private $executionDate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $parameterId
     *
     * @return Parameter
     */

    public function setParameterId($parameterId)
    {
        $this->parameterId = $parameterId;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */

    public function getParameterId()
    {
        return $this->parameterId;
    }

    /**
     * @return ParameterBase
     */
    public function getParameterBase() {
        return $this->parameterBase;
    }

    /**
     * @param ParameterBase $parameterBase
     * @return $this
     */
    public function setParameterBase($parameterBase) {
        $this->parameterBase = $parameterBase;
        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Parameter
     */
    /*
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }
    */

    /**
     * Get description
     *
     * @return string
     */
    /*
    public function getDescription()
    {
        return $this->description;
    }
    */

    /**
     * Set value
     *
     * @param float $result
     *
     * @return Parameter
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param string $resultString
     * @return Parameter
     */
    public function setResultString($resultString) {
        $this->resultString = $resultString;
        return $this;
    }

    /**
     * @return string
     */
    public function getResultString() {
        return $this->resultString;
    }

    /**
     * @param bool $resultFlag
     * @return Parameter
     */
    public function setResultFlag($resultFlag) {
        $this->resultFlag = $resultFlag;
        return $this;
    }

    /**
     * @return bool
     */
    public function getResultFlag() {
        return $this->resultFlag;
    }

    /**
     * @param $status
     * @return Parameter
     */
    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param $loinc
     * @return Parameter
     */
    /*
    public function setLoinc($loinc) {
        $this->loinc = $loinc;
        return $this;
    }
    */

    /**
     * @return string
     */
    /*
    public function getLoinc() {
        return $this->loinc;
    }
    */

    /**
     * @param $link
     * @return Parameter
     */
    /*
    public function setLink($link) {
        $this->link = $link;
        return $this;
    }
    */

    /**
     * @return string
     */
    /*
    public function getLink() {
        return $this->link;
    }
    */

    /**
     * @param $parameterP
     * @return Parameter
     */
    public function setParameterP($parameterP) {
        $this->parameterP = $parameterP;
        return $this;
    }

    /**
     * @return string
     */
    public function getParameterP() {
        return $this->parameterP;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }


    /**
     * @param string $unit
     * @return Parameter
     */
    /*
    public function setUnit($unit) {
        $this->unit = $unit;
        return $this;
    }
    */

    /**
     * @return string
     */
    /*
    public function getUnit() {
        return $this->unit;
    }
    */

    /**
     * Set ParameterReference
     *
     * @param ParameterReference $reference
     *
     * @return Parameter
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * Get min
     *
     * @return ParameterReference
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param $comment
     * @return Parameter
     */
    public function setComment($comment) {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return string
     */

    public function getComment() {
        return $this->comment;
    }

    /**
     * @return int
     */
    /*
    public function getIsTitle()
    {
        return $this->isTitle;
    }
    */

    /**
     * @param int $isTitle
     * @return Parameter
     */
    /*
    public function setIsTitle($isTitle)
    {
        $this->isTitle = $isTitle;
        return $this;
    }
    */

    /**
     * @return Validator
     */
    public function getValidatedBy() {
        return $this->validatedBy;
    }

    /**
     * @param Validator $validatedBy
     * @return Parameter
     */
    public function setValidatedBy($validatedBy) {
        $this->validatedBy = $validatedBy;
        return $this;
    }

    /**
     * @return integer
     */
    /*
    public function getIsCumulative() {
        return $this->isCumulative;
    }
    */

    /**
     * @param integer $isCumulative
     * @return Parameter
     */
    /*
    public function setIsCumulative($isCumulative) {
        $this->isCumulative = $isCumulative;
        return $this;
    }
    */

    /**
     * @param File $file
     * @return Parameter
     */
    public function addFile(File $file) {
        $this->files[] = $file;
        return $this;
    }

    /**
     * @param File $file
     * @return Parameter
     */
    public function removeFile(File $file) {
        $this->files->removeElement($file);
        return $this;
    }

    /**
     * @param FileArray $fileArray
     */
    public function setFiles(FileArray $fileArray) {
        $this->files = $fileArray->getFiles();
        foreach ($this->files as $file) {
            /** @var File $file */
            $file->setParameter($this);
        }
    }

    /**
     * @return Collection
     */
    public function getFiles() {
        return $this->files;
    }

    /**
     * get Parameter Group
     *
     * @return ParameterGroup
     */
    public function getParameterGroup() {
        return $this->parameterGroup;
    }

    /**
     * set Parameter Group
     *
     * @param ParameterGroup $parameterGroup
     * @return Parameter
     */
    public function setParameterGroup($parameterGroup) {
        $this->parameterGroup = $parameterGroup;
        return $this;
    }

    /**
     * @return ContractorRequired
     */
    public function getContractorRequired() {
        return $this->contractorRequired;
    }

    /**
     * @param ContractorRequired $contractorRequired
     * @return Parameter
     */
    public function setContractorRequired($contractorRequired) {
        $this->contractorRequired = $contractorRequired;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateExpected()
    {
        return $this->dateExpected;
    }

    /**
     * @param \DateTime $dateExpected
     * @return $this
     */
    public function setDateExpected($dateExpected)
    {
        $this->dateExpected = $dateExpected;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExecutionDate()
    {
        return $this->executionDate;
    }

    /**
     * @param \DateTime $executionDate
     * @return $this
     */
    public function setExecutionDate($executionDate)
    {
        $this->executionDate = $executionDate;
        return $this;
    }


}

