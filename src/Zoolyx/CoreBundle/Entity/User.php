<?php

namespace Zoolyx\CoreBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;
use Zoolyx\ShopBundle\Entity\Cart;
use Zoolyx\ShopBundle\Entity\Order;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class User extends BaseUser
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     * @Serializer\Expose
     */
    private $firstName;

    /**
     * @var string
     * @Serializer\Expose
     */
    private $lastName;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $zipCode;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $country;

    /** @var string */
    private $telephone;

    /** @var int */
    public $reportFrequency = 1;

    /** @var bool */
    public $autoShareOwners = true;

    /**
     * @var string
     */
    private $language = "nl";

    /** @var bool */
    private $acceptMails = false;

    /** @var Cart */
    private $cart = null;

    /** @var array */
    private $orders = array();

    /** @var bool  */
    private $resetPassword = false;

    /** @var User */
    private $shareAccount = null;


    public function __construct()
    {
        parent::__construct();
        $this->salt = '';
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet() {
        return $this->street;
    }

    /**
     * @param string $street
     * @return User
     */
    public function setStreet($street) {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode() {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     * @return User
     */
    public function setZipCode($zipCode)  {
        $this->zipCode = $zipCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * @param string $city
     * @return User
     */
    public function setCity($city) {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * @param string $country
     * @return User
     */
    public function setCountry($country) {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     * @return $this
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * @return int
     */
    public function getReportFrequency()
    {
        return $this->reportFrequency;
    }

    /**
     * @param int $reportFrequency
     * @return $this;
     */
    public function setReportFrequency($reportFrequency)
    {
        $this->reportFrequency = $reportFrequency;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isAutoShareOwners()
    {
        return $this->autoShareOwners;
    }

    /**
     * @param boolean $autoShareOwners
     * @return $this;
     */
    public function setAutoShareOwners($autoShareOwners)
    {
        $this->autoShareOwners = $autoShareOwners;
        return $this;
    }



    /**
     * @return string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * @param string $language
     * @return User
     */
    public function setLanguage($language) {
        $this->language = $language;
        return $this;
    }

    /**
     * @return boolean
     */
    public function acceptMails()
    {
        return $this->acceptMails;
    }

    /**
     * @param boolean $acceptMails
     * @return $this
     */
    public function setAcceptMails($acceptMails)
    {
        $this->acceptMails = $acceptMails;
        return $this;
    }


    /**
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param Cart $cart
     * @return $this
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
        return $this;
    }

    /**
     * @return array
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function addOrders($order)
    {
        $this->orders[] = $order;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isResetPassword()
    {
        return $this->resetPassword;
    }

    /**
     * @param boolean $resetPassword
     * @return $this
     */
    public function setResetPassword($resetPassword)
    {
        $this->resetPassword = $resetPassword;
        return $this;
    }

    /**
     * @return User
     */
    public function getShareAccount()
    {
        return $this->shareAccount;
    }

    /**
     * @param User $shareAccount
     * @return $this
     */
    public function setShareAccount($shareAccount)
    {
        $this->shareAccount = $shareAccount;
        return $this;
    }



}
