<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * Parameter
 */
class ParameterBaseSampleType
{

    /** @var integer */
    private $id;

    /** @var string */
    private $reference;

    /** @var ParameterBase */
    private $parameter;

    /** @var ParameterProfileSampleTypeDefinition */
    private $sampleType;

    /** @var string */
    private $minVolume;

    /** @var string */
    private $pref;

    /** @var ParameterProfileComment */
    private $comment;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }



    /**
     * @return ParameterBase
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * @param ParameterBase $parameter
     * @return $this
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
        return $this;
    }

    /**
     * @return ParameterProfileSampleTypeDefinition
     */
    public function getSampleType()
    {
        return $this->sampleType;
    }

    /**
     * @param ParameterProfileSampleTypeDefinition $sampleType
     * @return $this
     */
    public function setSampleType($sampleType)
    {
        $this->sampleType = $sampleType;
        return $this;
    }

    /**
     * @return string
     */
    public function getMinVolume()
    {
        return $this->minVolume;
    }

    /**
     * @param string $minVolume
     * @return $this
     */
    public function setMinVolume($minVolume)
    {
        $this->minVolume = $minVolume;
        return $this;
    }

    /**
     * @return string
     */
    public function getPref()
    {
        return $this->pref;
    }

    /**
     * @param string $pref
     * @return $this
     */
    public function setPref($pref)
    {
        $this->pref = $pref;
        return $this;
    }

    /**
     * @return ParameterProfileComment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param ParameterProfileComment $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

}

