<?php

namespace Zoolyx\CoreBundle\Entity;



/**
 * File
 */
class ObjectArray
{
    /**
     * @var array
     */
    private $objects = array();

    /**
     * @param $object
     * @return FileArray
     */
    public function addObject($object) {
        $this->objects[] = $object;
        return $this;
    }

    /**
     * @return array
     */
    public function getObjects() {
        return $this->objects;
    }
}

