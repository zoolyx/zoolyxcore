<?php

namespace Zoolyx\CoreBundle\Entity;
use Doctrine\Common\Collections\Collection;

/**
 * Parameter
 */
class ParameterBase
{
    /** @var integer */
    private $id;

    /** @var string */
    private $parameterId;

    /** @var string */
    private $unit;

    /** @var string */
    private $alternativeUnit;

    /** @var string */
    private $conversionFactor;

    /** @var string */
    private $decimals;

    /** @var string */
    private $delay;

    /** @var string */
    private $speciesCodes = "";

    /** @var integer */
    private $isTitle;

    /** @var integer  */
    private $isCumulative = 1;

    /** @var string */
    private $loinc;

    /** @var string */
    private $link;

    /** @var string */
    private $parameterGroup;

    /** @var integer */
    private $sequence;

    /** @var boolean */
    private $wikilab;

    /** @var Collection */
    private $descriptions = array();

    /** @var array */
    private $descriptions2 = array();

    /** @var Collection */
    private $referenceRanges = array();

    /** @var Collection */
    private $sampleTypes = array();

    /** @var Collection */
    private $methods = array();


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $parameterId
     *
     * @return ParameterBase
     */
    public function setParameterId($parameterId)
    {
        $this->parameterId = $parameterId;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getParameterId()
    {
        return $this->parameterId;
    }

    /**
     * @param string $unit
     * @return Parameter
     */
    public function setUnit($unit) {
        $this->unit = $unit;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnit() {
        return $this->unit;
    }

    /**
     * @return string
     */
    public function getAlternativeUnit() {
        return $this->alternativeUnit;
    }

    /**
     * @param string $alternativeUnit
     * @return $this
     */
    public function setAlternativeUnit($alternativeUnit) {
        $this->alternativeUnit = $alternativeUnit;
        return $this;
    }

    /**
     * @return string
     */
    public function getConversionFactor() {
        return $this->conversionFactor;
    }

    /**
     * @param string $conversionFactor
     * @return $this
     */
    public function setConversionFactor($conversionFactor) {
        $conversionFactor = str_replace(",",".",$conversionFactor);
        if (isset($conversionFactor[0]) and $conversionFactor[0] == '.') {
            $conversionFactor = '0'.$conversionFactor;
        }
        $this->conversionFactor = $conversionFactor;
        return $this;
    }

    /**
     * @return string
     */
    public function getDecimals() {
        return $this->decimals;
    }

    /**
     * @param string $decimals
     * @return $this
     */
    public function setDecimals($decimals) {
        $this->decimals = $decimals;
        return $this;
    }

    /**
     * @return string
     */
    public function getDelay() {
        return $this->delay;
    }

    /**
     * @param string $delay
     * @return $this
     */
    public function setDelay($delay) {
        $this->delay = $delay;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpeciesCodes() {
        return $this->speciesCodes;
    }

    /**
     * @param string $speciesCodes
     * @return $this
     */
    public function setSpeciesCodes($speciesCodes) {
        $this->speciesCodes = $speciesCodes;
        return $this;
    }

    /**
     * @param $loinc
     * @return Parameter
     */
    public function setLoinc($loinc) {
        $this->loinc = $loinc;
        return $this;
    }

    /**
     * @return string
     */
    public function getLoinc() {
        return $this->loinc;
    }

    /**
     * @param $link
     * @return Parameter
     */
    public function setLink($link) {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink() {
        return $this->link;
    }

    /**
     * @return int
     */
    public function getIsTitle()
    {
        return $this->isTitle;
    }

    /**
     * @param int $isTitle
     * @return Parameter
     */
    public function setIsTitle($isTitle)
    {
        $this->isTitle = $isTitle;
        return $this;
    }

    /**
     * @return integer
     */
    public function getIsCumulative() {
        return $this->isCumulative;
    }

    /**
     * @param integer $isCumulative
     * @return Parameter
     */
    public function setIsCumulative($isCumulative) {
        $this->isCumulative = $isCumulative;
        return $this;
    }

    /**
     * @return string
     */
    public function getParameterGroup() {
        return $this->parameterGroup;
    }

    /**
     * @param string $parameterGroup
     * @return $this
     */
    public function setParameterGroup($parameterGroup) {
        $this->parameterGroup = $parameterGroup;
        return $this;
    }

    /**
     * @return int
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * @param int $sequence
     * @return $this
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isWikilab()
    {
        return $this->wikilab;
    }

    /**
     * @param boolean $wikilab
     * @return $this
     */
    public function setWikilab($wikilab)
    {
        $this->wikilab = $wikilab;
        return $this;
    }






    /**
     * @return Collection
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * @return ParameterBaseDescription
     */
    public function getDefaultDescription() {
        return count($this->descriptions) ? $this->descriptions[0] : null;
    }

    /**
     * @param $language
     * @return ParameterBaseDescription
     */
    public function getDescription($language) {
        /** @var ParameterBaseDescription $description */
        foreach ($this->descriptions as $description) {
            if ($description->getLanguage() == $language) {
                return $description;
            }
        }
        return $this->getDefaultDescription();
    }

    /**
     * @param ParameterBaseDescription $description
     * @return ParameterBase
     */
    public function addDescription($description)
    {
        $this->descriptions[] = $description;
        return $this;
    }

    /**
     * @return array
     */
    public function getDescriptions2() {
        return $this->descriptions2;
    }

    /**
     * @return ParameterBaseDescription2
     */
    public function getDefaultDescription2() {
        return count($this->descriptions2) ? $this->descriptions2[0] : null;
    }

    /**
     * @param $language
     * @return ParameterBaseDescription2
     */
    public function getDescription2($language) {
        /** @var ParameterBaseDescription2 $description */
        foreach ($this->descriptions2 as $description) {
            if ($description->getLanguage() == $language) {
                return $description;
            }
        }
        return $this->getDefaultDescription2();
    }

    /**
     * @param ParameterBaseDescription2 $description
     * @return ParameterBase
     */
    public function addDescription2($description) {
        $this->descriptions2[] = $description;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getReferenceRanges() {
        return $this->referenceRanges;
    }

    /**
     * @param ReferenceRange $referenceRange
     * @return $this
     */
    public function addReferenceRange($referenceRange) {
        $this->referenceRanges[] = $referenceRange;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getSampleTypes() {
        return $this->sampleTypes;
    }

    /**
     * @param ParameterBaseSampleType $sampleType
     * @return $this
     */
    public function addSampleType($sampleType) {
        $this->sampleTypes[] = $sampleType;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getMethods() {
        return $this->methods;
    }

    /**
     * @param ParameterBaseMethod $method
     * @return $this
     */
    public function addMethod($method) {
        $this->methods[] = $method;
        return $this;
    }

    /**
     * @param $method
     * @return $this
     */
    public function removeMethod($method) {
        $this->methods->removeElement($method);
        return $this;
    }
}

