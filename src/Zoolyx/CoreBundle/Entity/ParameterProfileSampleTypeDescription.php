<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * ParameterProfileSampleTypeDescription
 */
class ParameterProfileSampleTypeDescription extends Description
{
    /** @var ParameterProfileSampleTypeDefinition */
    private $sampleTypeDefinition;

    /**
     * @return ParameterProfileSampleTypeDefinition
     */
    public function getSampleTypeDefinition() {
        return $this->sampleTypeDefinition;
    }

    /**
     * @param ParameterProfileSampleTypeDefinition $sampleTypeDefinition
     * @return ParameterProfileSampleTypeDescription
     */
    public function setSampleTypeDefinition($sampleTypeDefinition) {
        $this->sampleTypeDefinition = $sampleTypeDefinition;
        return $this;
    }
}

