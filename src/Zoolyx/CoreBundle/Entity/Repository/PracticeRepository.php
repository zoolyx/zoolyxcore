<?php

namespace Zoolyx\CoreBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Zoolyx\CoreBundle\Entity\User;

class PracticeRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return array
     */
    public function searchForAccount($user) {
        return $this->searchForAccountQuery($user)->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return array
     */
    public function searchForAccountQuery($user) {
        $parameters = [
            'userId' => $user->getId()
        ];

        $query = $this->createQueryBuilder('p')
            ->innerJoin('p.veterinaryPractices', 'vp')
            ->innerJoin('vp.veterinary', 'v')
            ->where('v.account = :userId')
            ->setParameters($parameters);
        return $query;
    }

}
