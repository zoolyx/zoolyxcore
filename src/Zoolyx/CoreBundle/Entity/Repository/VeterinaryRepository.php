<?php

namespace Zoolyx\CoreBundle\Entity\Repository;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityRepository;

class VeterinaryRepository extends EntityRepository
{
    public function searchFor($key) {
        $parameters = [
            'key' => '%'.$key.'%'
        ];

        $query = $this->createQueryBuilder('v')
            ->innerJoin('v.account', 'a')
            ->where('a.username like :key')
            ->orWhere('a.email like :key')
            ->orWhere('v.firstName like :key')
            ->orWhere('v.lastName like :key')
            ->setMaxResults( 50 )
            ->setParameters($parameters);
        //die($query->getQuery()->getSQL());
        return $query->getQuery()->getResult();
    }

    public function searchForAccount($user) {
        return $this->findOneBy(array('account'=>$user));
    }

    public function findAllForAccount($user) {
        return $this->findBy(array('account'=>$user));
    }

    public function findAllForEmail($email) {
        $parameters = [
            'email' => $email
        ];

        $query = $this->createQueryBuilder('v')
            ->innerJoin('v.account', 'a')
            ->where('a.email = :email')
            ->setParameters($parameters);
        return $query->getQuery()->getResult();
    }
}
