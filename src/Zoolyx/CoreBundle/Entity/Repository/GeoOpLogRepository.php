<?php

namespace Zoolyx\CoreBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;

class GeoOpLogRepository extends EntityRepository
{
    public function findAllQuery()
    {
        return $this->createQueryBuilder('g')->orderBy('g.createdOn', 'Desc');
    }
    
    public function searchForQuery($keyword) {
        $parameters = [
            'key' => '%'.$keyword.'%'
        ];

        $query = $this->createQueryBuilder('g')
            ->innerJoin('g.veterinary', 'v')
            ->innerJoin('v.veterinaryPractices', 'vp')
            ->innerJoin('vp.practice', 'p')
            ->where('g.clientId like :key OR g.jobId like :key OR g.jobReference like :key OR g.title like :key OR g.voorzien like :key OR v.lastName like :key OR p.name like :key OR g.line1 like :key OR g.line2 like :key OR g.response like :key')
            ->orderBy('g.createdOn', 'Desc')
            ->setParameters($parameters);
        return $query;
    }


    public function findForVeterinaryQuery(Veterinary $veterinary) {
        $parameters['veterinaryId'] = $veterinary->getId();

        $query = $this->createQueryBuilder('g')
            ->innerJoin('g.veterinary', 'v')
            ->innerJoin('v.veterinaryPractices', 'vp')
            ->innerJoin('vp.practice', 'p')
            ->innerJoin('p.veterinaryPractices', 'vp2')
            ->orderBy('g.createdOn', 'Desc')
            ->where('vp2.veterinary = :veterinaryId')
            ->setParameters($parameters);

        return $query->getQuery();
    }

    public function findFutureForVeterinayPractice(VeterinaryPractice $veterinaryPractice) {
        $now = new \DateTime();
        $parameters = array(
            'veterinaryPracticeId' => $veterinaryPractice->getId(),
            'now' => $now->format("Y-m-d H:i:s"),
            'response' => "success"
        );

        $query = $this->createQueryBuilder('g')
            ->where('g.veterinaryPractice = :veterinaryPracticeId')
            ->andWhere('g.voorzien > :now')
            ->andWhere('g.response = :response')
            ->setParameters($parameters);

        return $query->getQuery()->getResult();
    }
}
