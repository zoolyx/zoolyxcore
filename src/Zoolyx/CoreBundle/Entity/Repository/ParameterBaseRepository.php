<?php

namespace Zoolyx\CoreBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class ParameterBaseRepository extends EntityRepository
{
    public function findForWikilabs() {
        $query = $this->createQueryBuilder('p')
            ->where("p.wikilab=1");
        return $query->getQuery()->getResult();
    }
}
