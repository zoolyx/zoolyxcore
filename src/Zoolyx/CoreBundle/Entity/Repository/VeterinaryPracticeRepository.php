<?php

namespace Zoolyx\CoreBundle\Entity\Repository;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityRepository;
use Zoolyx\AccountBundle\Forms\Account\PracticeMember;
use Zoolyx\CoreBundle\Entity\User;

class VeterinaryPracticeRepository extends EntityRepository
{

    public function getPracticesFor(User $user) {
        $query = $this->createQueryBuilder('vp')
            ->innerJoin('vp.practice','p')
            ->innerJoin('p.veterinaryPractices', 'vp2')
            ->innerJoin('vp2.veterinary', 'v')
            ->where('v.account = :userId')
            ->andWhere('vp2.role != :roleDeleted')
            ->setParameters(array(
                'userId' => $user->getId(),
                'roleDeleted' => PracticeMember::PRACTICE_ROLE_REMOVED
            ));
        //echo $user->getId() . "<br>";
        //echo $query->getQuery()->getSQL();
        //die();
        return $query->getQuery()->getResult();
    }

    public function getRealPracticesFor(User $user) {
        $query = $this->createQueryBuilder('vp')
            ->innerJoin('vp.practice','p')
            ->innerJoin('p.veterinaryPractices', 'vp2')
            ->innerJoin('vp2.veterinary', 'v')
            ->where('v.account = :userId')
            ->andWhere('vp2.role != :roleDeleted')
            ->andWhere('vp.limsId is not null')
            ->setParameters(array(
                'userId' => $user->getId(),
                'roleDeleted' => PracticeMember::PRACTICE_ROLE_REMOVED
            ));
        //echo $user->getId() . "<br>";
        //echo $query->getQuery()->getSQL();
        //die();
        return $query->getQuery()->getResult();
    }

    public function getVeterinaryPracticesOf(User $user) {
        $query = $this->createQueryBuilder('vp')
            ->innerJoin('vp.veterinary', 'v')
            ->where('v.account = :userId')
            ->andWhere('vp.role != :roleDeleted')
            ->setParameters(array(
                'userId' => $user->getId(),
                'roleDeleted' => PracticeMember::PRACTICE_ROLE_REMOVED
            ));
        return $query->getQuery()->getResult();
    }
    public function getVeterinaryPracticesViaPracticeOf(User $user) {
        $query = $this->createQueryBuilder('vp')
            ->innerJoin('vp.practice', 'p')
            ->innerJoin('p.veterinaryPractices', 'vp2')
            ->innerJoin('vp2.veterinary', 'v')
            ->where('v.account = :userId')
            ->andWhere('vp.role != :roleDeleted')
            ->setParameters(array(
                'userId' => $user->getId(),
                'roleDeleted' => PracticeMember::PRACTICE_ROLE_REMOVED
            ));
        return $query->getQuery()->getResult();
    }

    public function searchFor($keyword) {
        $query = $this->createQueryBuilder('vp')
            ->innerJoin('vp.practice', 'p')
            ->innerJoin('vp.veterinary', 'v')
            ->where('v.firstName LIKE :keyword OR v.lastName LIKE :keyword OR p.name LIKE :keyword')
            ->setParameters(array(
                'keyword' => '%'.$keyword.'%'
            ));
        return $query->getQuery()->getResult();
    }
}
