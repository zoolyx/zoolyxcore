<?php

namespace Zoolyx\CoreBundle\Entity\Repository;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function findAllQuery() {
        return $this->createQueryBuilder('u');
    }

    public function searchForQuery($keyword) {
        $parameters = [
            'key' => '%'.$keyword.'%'
        ];

        $query = $this->createQueryBuilder('u')
            ->where('u.username like :key')
            ->orWhere('u.email like :key')
            ->orWhere('u.roles like :key')
            ->orWhere('u.firstName like :key')
            ->orWhere('u.lastName like :key')
            ->setParameters($parameters);
        return $query;
    }

    public function searchFor($key) {
        $parameters = [
            'key' => '%'.$key.'%'
        ];

        $query = $this->createQueryBuilder('u')
            ->where('u.username like :key')
            ->orWhere('u.email like :key')
            ->orWhere('u.firstName like :key')
            ->orWhere('u.lastName like :key')
            ->setMaxResults( 50 )
            ->setParameters($parameters);
        return $query->getQuery()->getResult();
    }
}
