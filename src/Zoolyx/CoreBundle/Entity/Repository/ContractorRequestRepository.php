<?php

namespace Zoolyx\CoreBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class ContractorRequestRepository extends EntityRepository
{
    public function findOldRequests() {
        $parameters = [
            'age' => new \DateTime('-1 year')
        ];

        $query = $this->createQueryBuilder('r')
            ->where('r.created < :age')
            ->setParameters($parameters);

        return $query->getQuery()->getResult();
    }
}
