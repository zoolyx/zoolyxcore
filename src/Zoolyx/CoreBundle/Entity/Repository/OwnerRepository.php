<?php

namespace Zoolyx\CoreBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class OwnerRepository extends EntityRepository
{

	public function searchFor($key) {
        $parameters = [
            'key' => '%'.$key.'%'
        ];

        $query = $this->createQueryBuilder('o')
            ->innerJoin('o.account', 'a')
            ->where('a.username like :key')
            ->orWhere('a.email like :key')
            ->orWhere('o.firstName like :key')
            ->orWhere('o.lastName like :key')
            ->setMaxResults( 50 )
            ->setParameters($parameters);
        //die($query->getQuery()->getSQL());
        return $query->getQuery()->getResult();
    }

}
