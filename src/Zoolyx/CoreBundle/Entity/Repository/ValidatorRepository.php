<?php

namespace Zoolyx\CoreBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class ValidatorRepository extends EntityRepository
{
    const DEFAULT_REFERENCE = 'DBAMDL';

    public function findOneByReference($reference) {
        $validator = $this->findOneBy(['reference'=>$reference]);
        if (!$validator) {
            $validator = $this->findOneBy(['reference'=>self::DEFAULT_REFERENCE]);
        }
        return $validator;
    }
}
