<?php

namespace Zoolyx\CoreBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class BreedDescriptionRepository extends EntityRepository
{
    public function findAllForSpecies($speciesCode, $language = "nl") {
        $parameters = [
            'speciesCode' => $speciesCode,
            'language' => $language
        ];

        $query = $this->createQueryBuilder('bd')
            ->innerJoin('bd.breed', 'b')
            ->innerJoin('b.species', 's')
            ->where('s.speciesId = :speciesCode')
            ->andWhere('bd.language = :language')
            ->orderBy('bd.description')
            ->setParameters($parameters);
        return $query->getQuery()->getResult();
    }
}
