<?php

namespace Zoolyx\CoreBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class BreedRepository extends EntityRepository
{
    public function findForSpeciesCode($speciesCode) {
        $parameters = [
            'speciesCode' => $speciesCode
        ];

        $query = $this->createQueryBuilder('b')
            ->innerJoin('b.species', 's')
            ->where('s.speciesId = :speciesCode')
            ->setParameters($parameters);

        return $query->getQuery()->getResult();
    }
}
