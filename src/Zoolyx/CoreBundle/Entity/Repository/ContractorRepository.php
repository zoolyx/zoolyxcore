<?php

namespace Zoolyx\CoreBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class ContractorRepository extends EntityRepository
{

    public function findAllQuery() {
        return $this->createQueryBuilder('c');
    }

}
