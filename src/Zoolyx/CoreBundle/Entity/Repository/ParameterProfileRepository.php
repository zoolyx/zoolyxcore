<?php

namespace Zoolyx\CoreBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class ParameterProfileRepository extends EntityRepository
{

    public function findForCategories($categories) {
        $parameters = [
            'ppCat' => $categories
        ];

        $query = $this->createQueryBuilder('pp')
            ->innerJoin('pp.category', 'c')
            ->where('c.reference IN (:ppCat)')
            ->setParameters($parameters);

        return $query->getQuery()->getResult();
    }
}
