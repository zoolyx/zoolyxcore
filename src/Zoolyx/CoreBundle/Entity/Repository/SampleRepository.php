<?php

namespace Zoolyx\CoreBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class SampleRepository extends EntityRepository
{
    public function searchFor($key) {
        $parameters = [
            'key' => '%'.$key.'%'
        ];

        $query = $this->createQueryBuilder('s')
            ->where('s.code like :key')
            ->setMaxResults( 50 )
            ->setParameters($parameters);
        return $query->getQuery()->getResult();
    }

}
