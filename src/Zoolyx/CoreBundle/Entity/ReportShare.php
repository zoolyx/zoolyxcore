<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * ReportShare
 */
class ReportShare
{
    /** @var integer */
    private $id;

    /** @var Report */
    private $report;

    /** @var Owner */
    private $owner;

    /** @var User */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param $report
     * @return $this
     */
    public function setReport($report)
    {
        $this->report = $report;
        return $this;
    }

    /**
     * @return Owner
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param Owner $user
     * @return $this
     */
    public function setOwner($user)
    {
        $this->owner = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

}

