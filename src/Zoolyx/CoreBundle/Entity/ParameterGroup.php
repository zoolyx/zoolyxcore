<?php

namespace Zoolyx\CoreBundle\Entity;
use Doctrine\Common\Collections\Collection;

/**
 * ParameterGroup
 */
class ParameterGroup
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $parameterGroupId;

    /** @var ParameterGroupBase */
    private $parameterGroupBase;

    /**
     * @var string
     */
    private $description="";

    /**
     * @var Collection
     */
    private $parameters = array();

    /**
     * @var Sample
     */
    private $sample;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getParameterGroupId()  {
        return $this->parameterGroupId;
    }

    /**
     * @param string $parameterGroupId
     * @return ParameterGroup
     */
    public function setParameterGroupId($parameterGroupId) {
        $this->parameterGroupId = $parameterGroupId;
        return $this;
    }

    /**
     * @return ParameterGroupBase
     */
    public function getParameterGroupBase()
    {
        return $this->parameterGroupBase;
    }

    /**
     * @param ParameterGroupBase $parameterGroupBase
     * @return $this
     */
    public function setParameterGroupBase($parameterGroupBase)
    {
        $this->parameterGroupBase = $parameterGroupBase;
        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ParameterGroup
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add parameter
     *
     * @param Parameter $parameter
     *
     * @return ParameterGroup
     */
    public function addParameter($parameter)
    {
        $this->parameters[] = $parameter;

        return $this;
    }

    /**
     * Remove parameter
     *
     * @param Parameter $parameter
     *
     * @return ParameterGroup
     */
    public function removeParameter($parameter)
    {
        $this->parameters->removeElement($parameter);

        return $this;
    }

    /**
     * Get parameters
     *
     * @return Collection
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * get Sample
     *
     * @return Sample
     */
    public function getSample() {
        return $this->sample;
    }

    /**
     * set Sample
     *
     * @param Sample $sample
     * @return ParameterGroup
     */
    public function setSample($sample) {
        $this->sample = $sample;
        return $this;
    }
}

