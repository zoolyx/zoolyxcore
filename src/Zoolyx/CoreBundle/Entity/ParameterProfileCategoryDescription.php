<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * ParameterProfileCategoryDescription
 */
class ParameterProfileCategoryDescription extends Description
{
    /** @var ParameterProfileCategory */
    private $category;

    /**
     * @return ParameterProfileCategory
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * @param ParameterProfileCategory $category
     * @return ParameterProfileCategoryDescription
     */
    public function setCategory($category) {
        $this->category = $category;
        return $this;
    }
}

