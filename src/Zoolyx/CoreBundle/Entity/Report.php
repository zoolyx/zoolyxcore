<?php

namespace Zoolyx\CoreBundle\Entity;
use Doctrine\Common\Collections\Collection;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;
use Zoolyx\CoreBundle\Model\Report\VeterinaryPracticeReport\VeterinaryPracticeReportRoles;
use Zoolyx\CoreBundle\Model\XmlParser\Object\Invoice;
use Zoolyx\TicketBundle\Entity\Ticket;

/**
 * Report
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Report
{

    /** @var integer
     *  @Serializer\Expose
     */
    private $id;

    /** @var string */
    private $hash;

    /** @var string */
    private $alternativeHash;

    /** @var string
     *  @Serializer\Expose
     */
    private $requestId;

    /** @var int  */
    private $multiSample = 0;

    /** @var  string */
    private $practiceReference;

    /** @var string  */
    private $isInvoiced = 0;

    /** @var string  */
    private $vat = "";

    /** @var string */
    private $fee = "";

    /** @var Invoice */
    private $invoiceObject = null;

    /** @var Collection */
    private $veterinaryPracticeReports = array();

    /** @var Owner
     *  @Serializer\Expose
     */
    private $owner;

    /** @var string
     *  @Serializer\Expose
     */
    private $billing = 0;

    /** @var Collection */
    private $contractorReports;

    /** @var Collection
     *  @Serializer\Expose
     */
    private $versions = array();

    /** @var OriginalRequest */
    private $originalRequest = null;

    /** @var Collection */
    private $files = array();

    /** @var \DateTime
     *  @Serializer\Expose
     */
    private $createdOn;

    /** @var Collection  */
    private $veterinariesInCopy = array();
    /** @var Collection  */
    private $ownersInCopy = array();

    /** @var bool */
    private $isNew = true;

    /** @var array  */
    private $shares = array();

    /** @var array  */
    private $tickets = array();

    /** @var bool  */
    private $ownerHasAccess = false;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getHash() {
        return $this->hash;
    }

    /**
     * @param string $hash
     * @return ReportVersion
     */
    public function setHash($hash) {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlternativeHash()
    {
        return $this->alternativeHash;
    }

    /**
     * @param string $alternativeHash
     * @return $this
     */
    public function setAlternativeHash($alternativeHash)
    {
        $this->alternativeHash = $alternativeHash;
        return $this;
    }

    /**
     * @return string
     */
    public function getRequestId() {
        return $this->requestId;
    }

    /**
     * @param string $requestId
     * @return Report
     */
    public function setRequestId($requestId) {
        $this->requestId = $requestId;
        return $this;
    }

    /**
     * @return int
     */
    public function getMultiSample() {
        return $this->multiSample;
    }

    /**
     * @param int $multiSample
     * @return Report
     */
    public function setMultiSample($multiSample) {
        $this->multiSample = $multiSample;
        return $this;
    }

    /**
     * @return string
     */
    public function getPracticeReference()
    {
        return $this->practiceReference;
    }

    /**
     * @param $practiceReference
     * @return Report
     */
    public function setPracticeReference($practiceReference)
    {
        $this->practiceReference = $practiceReference;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsInvoiced() {
        return $this->isInvoiced;
    }

    /**
     * @param $isInvoiced
     * @return Report
     */
    public function setIsInvoiced($isInvoiced) {
        if ($isInvoiced != '') $this->isInvoiced = $isInvoiced;
        return $this;
    }

    /**
     * @return string
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param string $vat
     * @return $this
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }

    /**
     * @return string
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * @param string $fee
     * @return $this
     */
    public function setFee($fee)
    {
        $this->fee = $fee;
        return $this;
    }

    /**
     * @return Invoice
     */
    public function getInvoiceObject()
    {
        return $this->invoiceObject;
    }

    /**
     * @param Invoice $invoiceObject
     * @return $this
     */
    public function setInvoiceObject($invoiceObject)
    {
        $this->invoiceObject = $invoiceObject;
        return $this;
    }



    /**
     * @param VeterinaryPracticeReport $veterinaryPracticeReport
     * @return Report
     */
    public function addVeterinaryPracticeReportAsVeterinary(VeterinaryPracticeReport $veterinaryPracticeReport) {
        $veterinaryPracticeReport->setRole(VeterinaryPracticeReportRoles::VPR_VETERINARY);
        $this->addVeterinaryPracticeReport($veterinaryPracticeReport);
        return $this;
    }

    /**
     * @param VeterinaryPracticeReport $veterinaryPracticeReport
     * @return Report
     */
    public function addVeterinaryPracticeReportAsOrganisation(VeterinaryPracticeReport $veterinaryPracticeReport) {
        $veterinaryPracticeReport->setRole(VeterinaryPracticeReportRoles::VPR_ORGANISATION);
        $this->addVeterinaryPracticeReport($veterinaryPracticeReport);
        return $this;
    }

    /**
     * @param VeterinaryPracticeReport $veterinaryPracticeReport
     * @return Report
     */
    public function addVeterinaryPracticeReportAsShared(VeterinaryPracticeReport $veterinaryPracticeReport) {
        $veterinaryPracticeReport->setRole(VeterinaryPracticeReportRoles::VPR_SHARED);
        $this->addVeterinaryPracticeReport($veterinaryPracticeReport);
        return $this;
    }


    /**
     * @param VeterinaryPracticeReport $veterinaryPracticeReport
     * @return Report
     */
    public function addVeterinaryPracticeReport(VeterinaryPracticeReport $veterinaryPracticeReport) {
        if (!$this->veterinaryPracticeExists($veterinaryPracticeReport)) {
            $this->veterinaryPracticeReports[] = $veterinaryPracticeReport;
        }
        return $this;
    }

    private function veterinaryPracticeExists(VeterinaryPracticeReport $veterinaryPracticeReport) {
        if (is_null($veterinaryPracticeReport->getId())) {
            return false;
        }
        $exists = false;
        /** @var VeterinaryPracticeReport $vpr */
        foreach ($this->getVeterinaryPracticeReports() as $vpr) {
            if ($veterinaryPracticeReport->getVeterinaryPractice()->getId() == $vpr->getVeterinaryPractice()->getId())
                $exists = true;
        }
        return $exists;
    }

    /**
     * @param VeterinaryPracticeReport $veterinaryPracticeReport
     * @return Report
     */
    public function removeVeterinaryPracticeReport(VeterinaryPracticeReport $veterinaryPracticeReport) {
        $this->veterinaryPracticeReports->removeElement($veterinaryPracticeReport);
        return $this;
    }


    /**
     * @return Collection
     */
    public function getVeterinaryPracticeReports() {
        return $this->veterinaryPracticeReports;
    }

    /**
     * @return Owner
     */
    public function getOwner() {
        return $this->owner;
    }

    /**
     * @param null|Owner $owner
     * @return Report
     */
    public function setOwner($owner) {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return string
     */
    public function getBilling() {
        return $this->billing;
    }

    /**
     * @param string $billing
     * @return Report
     */
    public function setBilling($billing) {
        $this->billing = $billing;
        return $this;
    }

    /**
     * @param ContractorReport $contractorReport
     * @return Contractor
     */
    public function addContractorReport(ContractorReport $contractorReport) {
        $this->contractorReports[] = $contractorReport;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getContractorReports() {
        return $this->contractorReports;
    }

    /**
     * @param ReportVersion $reportVersion
     * @return Report
     */
    public function addVersion(ReportVersion $reportVersion) {
        $this->versions[] = $reportVersion;
        return $this;
    }

    /**
     * @param ReportVersion $reportVersion
     * @return Report
     */
    public function removeVersion(ReportVersion $reportVersion) {
        $this->versions->removeElement($reportVersion);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getVersions()  {
        return $this->versions;
    }

    /**
     * @return OriginalRequest
     */
    public function getOriginalRequest() {
        return $this->originalRequest;
    }

    /**
     * @param OriginalRequest $originalRequest
     * @return Report
     */
    public function setOriginalRequest($originalRequest) {
        $this->originalRequest = $originalRequest;
        return $this;
    }

    /**
     * @param File $file
     * @return Report
     */
    public function addFile(File $file) {
        $this->files[] = $file;
        return $this;
    }

    /**
     * @param File $file
     * @return Report
     */
    public function removeFile(File $file) {
        $this->files->removeElement($file);
        return $this;
    }

    /**
     * @param FileArray $fileArray
     */
    public function setFiles(FileArray $fileArray) {
        $this->files = $fileArray->getFiles();
        foreach ($this->files as $file) {
            /** @var File $file */
            $file->setReport($this);
        }
    }

    /**
     * @return Collection
     */
    public function getFiles() {
        return $this->files;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn() {
        return $this->createdOn;
    }

    /**
     * @param \DateTime $createdOn
     * @return Report
     */
    public function setCreatedOn(\DateTime $createdOn) {
        $this->createdOn = $createdOn;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getVeterinariesInCopy()
    {
        return $this->veterinariesInCopy;
    }

    /**
     * @param $veterinariesInCopy
     * @return Report
     */
    public function setVeterinariesInCopy($veterinariesInCopy)
    {
        $this->veterinariesInCopy = $veterinariesInCopy;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getOwnersInCopy()
    {
        return $this->ownersInCopy;
    }

    /**
     * @param $ownersInCopy
     * @return Report
     */
    public function setOwnersInCopy($ownersInCopy)
    {
        $this->ownersInCopy = $ownersInCopy;
        return $this;
    }

    /**
     * @param Collection $inCopy
     * @return Report
     */
    public function setInCopy($inCopy)
    {
        foreach ($inCopy as $person) {
            if ($person instanceof VeterinaryPracticeReport) {
                $this->veterinariesInCopy[] = $person;
            }
            if ($person instanceof Owner) {
                $this->ownersInCopy[] = $person;
            }
        }
        return $this;
    }



    public function getVeterinaryPractice() {
        foreach ($this->getVeterinaryPracticeReports() as $veterinaryPracticeReport) {
            /** @var VeterinaryPracticeReport $veterinaryPracticeReport */
            if ($veterinaryPracticeReport->getRole() == VeterinaryPracticeReportRoles::VPR_VETERINARY) {
                return $veterinaryPracticeReport->getVeterinaryPractice();
            }
        }
        return null;
    }

    /**
     * @return ReportVersion/null
     */
    public function getDefaultVersion() {
        return $this->getFirstVersion();
    }

    /**
     * @return ReportVersion/null
     */
    public function getFirstVersion() {
        foreach ($this->versions as $version) {
            return $version;
        }
        return null;
    }

    public function getLatestVersion() {
        $versions = $this->getVersions();
        if ($numVersions = count($versions)) {
            return $versions[$numVersions-1];
        }
        return null;
    }

    /**
     * @param string $language
     * @return null|ReportVersion
     */
    public function getVersion($language) {
        $versions = $this->getVersions();
        /** @var ReportVersion $version */
        foreach($versions as $version) {
            if ($version->getLanguage() == $language) {
                return $version;
            }
        }
        return null;
    }

    /**
     * @param string $language
     * @return bool
     */
    public function hasVersion($language) {
        /** @var ReportVersion $version */
        foreach($this->getVersions() as $version) {
            if ($version->getLanguage() == $language) {
                return true;
            }
        }
        return false;
    }

    public function hasAVersion() {
        return count($this->versions) > 0;
    }

    /**
     * @return boolean
     */
    public function isNew()
    {
        return $this->isNew;
    }

    /**
     * @param boolean $isNew
     * @return $this
     */
    public function setIsNew($isNew)
    {
        $this->isNew = $isNew;
        return $this;
    }


    /**
     * @return array
     */
    public function getShares()
    {
        return $this->shares;
    }

    /**
     * @param ReportShare $share
     * @return $this
     */
    public function addShares($share)
    {
        $this->shares[] = $share;
        return $this;
    }

    /**
     * @return array
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * @param Ticket $ticket
     * @return $this
     */
    public function addTicket($ticket)
    {
        $this->tickets[] = $ticket;
        return $this;
    }

    /**
     * @return boolean
     */
    public function ownerHasAccess()
    {
        return $this->ownerHasAccess;
    }

    /**
     * @param boolean $ownerHasAccess
     * @return $this
     */
    public function setOwnerHasAccess($ownerHasAccess)
    {
        $this->ownerHasAccess = $ownerHasAccess;
        return $this;
    }


}

