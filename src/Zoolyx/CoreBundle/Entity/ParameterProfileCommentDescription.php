<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * ParameterProfileCommentDescription
 */
class ParameterProfileCommentDescription extends Description
{
    /** @var ParameterProfileComment */
    private $comment;

    /**
     * @return ParameterProfileComment
     */
    public function getComment() {
        return $this->comment;
    }

    /**
     * @param ParameterProfileComment $comment
     * @return ParameterProfileCommentDescription
     */
    public function setComment($comment) {
        $this->comment = $comment;
        return $this;
    }
}

