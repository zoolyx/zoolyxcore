<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * ParameterBaseDescription2
 */
class ParameterBaseDescription2 extends Description
{
    /** @var ParameterBase */
    private $parameterBase;

    /**
     * @return ParameterBase
     */
    public function getParameterBase() {
        return $this->parameterBase;
    }

    /**
     * @param ParameterBase $parameterBase
     * @return ParameterBaseDescription2
     */
    public function setParameterBase($parameterBase) {
        $this->parameterBase = $parameterBase;
        return $this;
    }
}

