<?php

namespace Zoolyx\CoreBundle\Entity;
use Zoolyx\ShopBundle\Entity\Product;

/**
 * ProductLongDescription
 */
class ProductLongDescription extends Description
{
    /** @var Product */
    private $product;

    /**
     * @return Product
     */
    public function getProduct() {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct($product) {
        $this->product = $product;
        return $this;
    }
}

