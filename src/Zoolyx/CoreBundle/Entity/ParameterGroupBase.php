<?php

namespace Zoolyx\CoreBundle\Entity;
use Doctrine\Common\Collections\Collection;

/**
 * ParameterGroupBase
 */
class ParameterGroupBase
{
    /** @var integer */
    private $id;

    /** @var string */
    private $parameterGroupId;

    /** @var Collection */
    private $descriptions = array();

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getParameterGroupId()
    {
        return $this->parameterGroupId;
    }

    /**
     * @param string $parameterGroupId
     * @return $this
     */
    public function setParameterGroupId($parameterGroupId)
    {
        $this->parameterGroupId = $parameterGroupId;
        return $this;
    }



    /**
     * @return Collection
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * @return ParameterGroupBaseDescription
     */
    public function getDefaultDescription() {
        return count($this->descriptions) ? $this->descriptions[0] : null;
    }

    /**
     * @param $language
     * @return ParameterGroupBaseDescription
     */
    public function getDescription($language) {
        /** @var ParameterGroupBaseDescription $description */
        foreach ($this->descriptions as $description) {
            if ($description->getLanguage() == $language) {
                return $description;
            }
        }
        return $this->getDefaultDescription();
    }

    /**
     * @param ParameterGroupBaseDescription $description
     * @return ParameterGroupBase
     */
    public function addDescription($description)
    {
        $this->descriptions[] = $description;
        return $this;
    }

}

