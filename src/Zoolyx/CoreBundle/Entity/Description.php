<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * Description
 */
abstract class Description
{
    /** @var integer */
    private $id;

    /** @var string */
    private $language;

    /** @var string */
    private $description;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * @param string $language
     * @return Description
     */
    public function setLanguage($language) {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Description
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

}

