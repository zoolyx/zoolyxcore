<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * ParameterGroupBaseDescription
 */
class ParameterGroupBaseDescription extends Description
{
    /** @var ParameterGroupBase */
    private $parameterGroupBase;

    /**
     * @return ParameterGroupBase
     */
    public function getParameterGroupBase()
    {
        return $this->parameterGroupBase;
    }

    /**
     * @param ParameterGroupBase $parameterGroupBase
     * @return $this
     */
    public function setParameterGroupBase($parameterGroupBase)
    {
        $this->parameterGroupBase = $parameterGroupBase;
        return $this;
    }


}

