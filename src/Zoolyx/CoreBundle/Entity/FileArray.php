<?php

namespace Zoolyx\CoreBundle\Entity;


use Doctrine\Common\Collections\Collection;

/**
 * File
 */
class FileArray
{
    /**
     * @var Collection
     */
    private $files = array();

    /**
     * @param File $file
     * @return FileArray
     */
    public function addFile(File $file) {
        $this->files[] = $file;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getFiles() {
        return $this->files;
    }
}

