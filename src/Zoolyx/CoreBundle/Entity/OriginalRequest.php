<?php

namespace Zoolyx\CoreBundle\Entity;
use Doctrine\Common\Collections\Collection;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;

/**
 * Report
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OriginalRequest
{

    /** @var integer
     *  @Serializer\Expose
     */
    private $id;

    /** @var string
     *  @Serializer\Expose
     */
    private $requestId;

    /** @var  string */
    private $practiceReference;

    /** @var Collection */
    private $files = array();

    /** @var \DateTime
     *  @Serializer\Expose
     */
    private $createdOn;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getRequestId() {
        return $this->requestId;
    }

    /**
     * @param string $requestId
     * @return Report
     */
    public function setRequestId($requestId) {
        $this->requestId = $requestId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPracticeReference()
    {
        return $this->practiceReference;
    }

    /**
     * @param $practiceReference
     * @return Report
     */
    public function setPracticeReference($practiceReference)
    {
        $this->practiceReference = $practiceReference;
        return $this;
    }

    /**
     * @param File $file
     * @return Report
     */
    public function addFile(File $file) {
        $this->files[] = $file;
        return $this;
    }

    /**
     * @param File $file
     * @return Report
     */
    public function removeFile(File $file) {
        $this->files->removeElement($file);
        return $this;
    }

    /**
     * @param FileArray $fileArray
     */
    public function setFiles(FileArray $fileArray) {
        $this->files = $fileArray->getFiles();

        /** @var File $file */
/*
        foreach ($this->files as $file) {
            $file->setReport($this);
        }
*/
    }

    /**
     * @return Collection
     */
    public function getFiles() {
        return $this->files;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn() {
        return $this->createdOn;
    }

    /**
     * @param \DateTime $createdOn
     * @return Report
     */
    public function setCreatedOn(\DateTime $createdOn) {
        $this->createdOn = $createdOn;
        return $this;
    }

}

