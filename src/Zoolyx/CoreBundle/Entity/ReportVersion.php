<?php

namespace Zoolyx\CoreBundle\Entity;
use Doctrine\Common\Collections\Collection;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;

/**
 * ReportVersion
 *
 * @Serializer\ExclusionPolicy("all")
 */
class ReportVersion
{

    /** @var integer
     *  @Serializer\Expose
     */
    private $id;

    /** @var  string $language */
    private $language='nl';

    /** @var Report */
    private $report;

    /** @var Collection
     *  @Serializer\Expose
     */
    private $samples = array();


    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * @param string $language
     * @return ReportVersion
     */
    public function setLanguage($language) {
        $this->language = $language;
        return $this;
    }

    /**
     * @return Report
     */
    public function getReport() {
        return $this->report;
    }

    /**
     * @param Report $report
     * @return ReportVersion
     */
    public function setReport($report) {
        $this->report = $report;
        return $this;
    }
    /**
     * @param Sample $sample
     * @return ReportVersion
     */
    public function addSample(Sample $sample) {
        $this->samples[] = $sample;
        return $this;
    }

    /**
     * @param Sample $sample
     * @return ReportVersion
     */
    public function removeSample(Sample $sample) {
        $this->samples->removeElement($sample);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getSamples()  {
        return $this->samples;
    }

    public function getPhysicalSamples() {
        $physicalSamples = array();
        /** @var Sample $sample */
        foreach ($this->samples as $sample) {
            if (is_null($sample->isPhysical()) || $sample->isPhysical()) {
                $physicalSamples[] = $sample;
            }
        }
        return $physicalSamples;
    }

}

