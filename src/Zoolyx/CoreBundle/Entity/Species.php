<?php

namespace Zoolyx\CoreBundle\Entity;
use Doctrine\Common\Collections\Collection;

/**
 * Parameter
 */
class Species
{
    /** @var integer */
    private $id;

    /** @var string */
    private $speciesId;

    /** @var Collection */
    private $descriptions = array();

    /** @var Collection */
    private $breeds = array();


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSpeciesId() {
        return $this->speciesId;
    }

    /**
     * @param $speciesId
     * @return Species
     */
    public function setSpeciesId($speciesId) {
        $this->speciesId = $speciesId;
        return $this;
    }


    /**
     * @return Collection
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    public function getDescription($language) {
        /** @var SpeciesDescription $description */
        foreach ($this->descriptions as $description) {
            if ($description->getLanguage() == $language)
                return $description;
        }
        return null;
    }


    /**
     * @param SpeciesDescription $description
     * @return Species
     */
    public function addDescription($description)
    {
        $this->descriptions[] = $description;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getBreeds() {
        return $this->breeds;
    }

    /**
     * @param Breed $breed
     * @return Species
     */
    public function addBreed($breed) {
        $this->breeds[] = $breed;
        return $this;
    }
}

