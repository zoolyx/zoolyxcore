<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * ParameterBaseDescription
 */
class ParameterBaseDescription extends Description
{
    /** @var ParameterBase */
    private $parameterBase;

    /**
     * @return ParameterBase
     */
    public function getParameterBase() {
        return $this->parameterBase;
    }

    /**
     * @param ParameterBase $parameterBase
     * @return ParameterBaseDescription
     */
    public function setParameterBase($parameterBase) {
        $this->parameterBase = $parameterBase;
        return $this;
    }
}

