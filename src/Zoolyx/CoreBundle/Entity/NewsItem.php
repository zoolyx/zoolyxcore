<?php

namespace Zoolyx\CoreBundle\Entity;


/**
 * File
 */
class NewsItem
{
    /** @var integer */
    private $id;

    /** @var string */
    private $title;

    /** @var string */
    private $image;

    /** @var string */
    private $url;

    /** @var string */
    private $content;

    /** @var string */
    private $contentShort;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param $image
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getContentShort()
    {
        return $this->contentShort;
    }

    /**
     * @param $contentShort
     * @return $this
     */
    public function setContentShort($contentShort)
    {
        $this->contentShort = $contentShort;
        return $this;
    }

}

