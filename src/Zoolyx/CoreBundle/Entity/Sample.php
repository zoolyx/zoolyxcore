<?php

namespace Zoolyx\CoreBundle\Entity;
use Doctrine\Common\Collections\Collection;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;

/**
 * Sample
 * @Serializer\ExclusionPolicy("all")
 */
class Sample
{
    /** @var integer
     *  @Serializer\Expose
     */
    private $id;

    /** @var string
     *  @Serializer\Expose
     */
    private $code;

    /** @var integer */
    private $cumulative = 0;

    /** @var Pet
     *  @Serializer\Expose
     */
    private $pet;

    /** @var \DateTime
     *  @Serializer\Expose
     */
    private $dateReceived;

    /** @var \DateTime
     *  @Serializer\Expose
     */
    private $dateCompleted;

    /** @var integer
     *  @Serializer\Expose
     */
    private $completed;

    /** @var boolean */
    private $urgent;

    /** @var  string
     *  @Serializer\Expose
     */
    private $externalId;

    /**
     * @var boolean
     */
    private $isPhysical = null;

    /** @var  string */
    private $contractorReference;

    /** @var Collection
     *  @Serializer\Expose
     */
    private $parameterGroups = array();

    /** @var  ReportVersion */
    private $reportVersion;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $code
     * @return Sample
     */
    public function setCode($code) {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * @return bool
     */
    public function isCumulative() {
        return $this->cumulative;
    }

    /**
     * @param integer $cumulative
     * @return Sample
     */
    public function setCumulative($cumulative) {
        if ($cumulative != '' ) $this->cumulative = $cumulative;
        return $this;
    }

    /**
     * @return Pet
     */
    public function getPet() {
        return $this->pet;
    }

    /**
     * @param Pet $pet
     * @return Report
     */
    public function setPet(Pet $pet) {
        $this->pet = $pet;
        return $this;
    }

    /**
     * Set dateReceived
     *
     * @param \DateTime $date
     *
     * @return Sample
     */
    public function setDateReceived($date)
    {
        $this->dateReceived = $date;

        return $this;
    }

    /**
     * Get dateReceived
     *
     * @return \DateTime
     */
    public function getDateReceived()
    {
        return $this->dateReceived;
    }

    /**
     * Set dateCompleted
     *
     * @param \DateTime $date
     *
     * @return Sample
     */
    public function setDateCompleted($date)
    {
        $this->dateCompleted = $date;

        return $this;
    }

    /**
     * Get dateCompleted
     *
     * @return \DateTime
     */
    public function getDateCompleted()
    {
        return $this->dateCompleted;
    }

    /**
     * Set status
     * @param integer $completed
     * @return Sample
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;
        return $this;
    }

    /**
     * Get status
     * @return integer
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * @return boolean
     */
    public function isUrgent()
    {
        return $this->urgent;
    }

    /**
     * @param boolean $urgent
     * @return $this
     */
    public function setUrgent($urgent)
    {
        $this->urgent = $urgent;
        return $this;
    }


    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param $externalId
     * @return Sample
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isPhysical()
    {
        return $this->isPhysical;
    }

    /**
     * @param boolean $isPhysical
     * @return $this
     */
    public function setIsPhysical($isPhysical)
    {
        $this->isPhysical = $isPhysical;
        return $this;
    }


    /**
     * @return string
     */
    public function getContractorReference()
    {
        return $this->contractorReference;
    }

    /**
     * @param string $contractorReference
     * @return Report
     */
    public function setContractorReference($contractorReference)
    {
        $this->contractorReference = $contractorReference;
        return $this;
    }

    /**
     * @param ParameterGroup $parameterGroup
     * @return Sample
     */
    public function addParameterGroup(ParameterGroup $parameterGroup) {
        $this->parameterGroups[] = $parameterGroup;
        return $this;
    }

    /**
     * @param ParameterGroup $parameterGroup
     * @return Sample
     */
    public function removeParameterGroup(ParameterGroup $parameterGroup) {
        $this->parameterGroups->removeElement($parameterGroup);
        return $this;
    }

    /**
     * Get parameterGroups
     *
     * @return Collection
     */
    public function getParameterGroups()
    {
        return $this->parameterGroups;
    }

    /**
     * @return ReportVersion
     */
    public function getReportVersion() {
        return $this->reportVersion;
    }

    /**
     * @param ReportVersion $reportVersion
     * @return Sample
     */
    public function setReportVersion(ReportVersion $reportVersion) {
        $this->reportVersion = $reportVersion;
        return $this;
    }
}

