<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * Parameter
 */
class GeniusCode
{
    /** @var integer */
    private $id;

    /** @var string */
    private $reference;

    /** @var Species */
    private $species;

    /** @var string */
    private $rule;

    /** @var ParameterProfile */
    private $parameterProfile;

    /** @var array */
    private $descriptions;

    /** @var integer */
    private $expires;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return Species
     */
    public function getSpecies()
    {
        return $this->species;
    }

    /**
     * @param Species $species
     * @return $this
     */
    public function setSpecies($species)
    {
        $this->species = $species;
        return $this;
    }

    /**
     * @return string
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * @param string $rule
     * @return $this
     */
    public function setRule($rule)
    {
        $this->rule = $rule;
        return $this;
    }

    /**
     * @return ParameterProfile
     */
    public function getParameterProfile()
    {
        return $this->parameterProfile;
    }

    /**
     * @param ParameterProfile $parameterProfile
     * @return $this
     */
    public function setParameterProfile($parameterProfile)
    {
        $this->parameterProfile = $parameterProfile;
        return $this;
    }

    /**
     * @return int
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * @param int $expires
     * @return $this
     */
    public function setExpires($expires)
    {
        $this->expires = $expires;
        return $this;
    }

    /**
     * @return array
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    public function getDescription($language='nl') {
        /** @var GeniusCodeDescription $description */
        foreach ($this->getDescriptions() as $description) {
            if ($description->getLanguage() == $language) {
                return $description;
            }
        }
        return null;
    }
    /**
     * @param GeniusCodeDescription $description
     * @return $this
     */
    public function addDescription($description)
    {
        $this->descriptions[] = $description;
        return $this;
    }

}

