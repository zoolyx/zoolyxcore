<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * ParameterProfilePrice
 */
class ParameterProfilePrice
{
    /** @var integer */
    private $id;

    /** @var string */
    private $billing = '';

    /** @var string */
    private $value;

    /** @var ParameterProfile */
    private $parameterProfile;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBilling() {
        return $this->billing;
    }

    /**
     * @param string $billing
     * @return ParameterProfilePrice
     */
    public function setBilling($billing) {
        $this->billing = $billing;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * @param string $value
     * @return ParameterProfilePrice
     */
    public function setValue($value) {
        $this->value = $value;
        return $this;
    }

    /**
     * @return ParameterProfile
     */
    public function getParameterProfile() {
        return $this->parameterProfile;
    }

    /**
     * @param ParameterProfile $parameterProfile
     * @return ParameterProfileSampleType
     */
    public function setParameterProfile($parameterProfile) {
        $this->parameterProfile = $parameterProfile;
        return $this;
    }
}

