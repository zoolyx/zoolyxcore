<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * ParameterProfileDescription
 */
class ParameterProfileDescription extends Description
{
    /** @var ParameterProfile */
    private $parameterProfile;

    /**
     * @return ParameterProfile
     */
    public function getParameterProfile() {
        return $this->parameterProfile;
    }

    /**
     * @param ParameterProfile $parameterProfile
     * @return ParameterProfileDescription
     */
    public function setParameterProfile($parameterProfile) {
        $this->parameterProfile = $parameterProfile;
        return $this;
    }
}

