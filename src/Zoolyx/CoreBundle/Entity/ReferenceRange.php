<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * Parameter
 */
class ReferenceRange
{

    /** @var integer */
    private $id;

    /** @var string */
    private $speciesCode;

    /** @var string */
    private $toAge;

    /** @var string*/
    private $low;

    /** @var string*/
    private $high;

    /** @var string*/
    private $veryLow;

    /** @var string*/
    private $veryHigh;

    /** @var string*/
    private $referenceDisplay;

    /** @var string*/
    private $comment;

    /** @var string*/
    private $imageName;


    /** @var  ParameterBase */
    private $parameterBase;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSpeciesCode() {
        return $this->speciesCode;
    }

    /**
     * @param string $speciesCode
     * @return $this
     */
    public function setSpeciesCode($speciesCode) {
        $this->speciesCode = $speciesCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getToAge() {
        return $this->toAge;
    }

    /**
     * @param string $toAge
     * @return $this
     */
    public function setToAge($toAge) {
        $this->toAge = $toAge;
        return $this;
    }

    /**
     * Set min
     *
     * @param string $low
     *
     * @return ParameterReference
     */
    public function setLow($low)
    {
        $this->low = $low;

        return $this;
    }

    /**
     * Get min
     *
     * @return string
     */
    public function getLow()
    {
        return $this->low;
    }

    /**
     * Set max
     *
     * @param string $high
     *
     * @return ParameterReference
     */
    public function setHigh($high)
    {
        $this->high = $high;

        return $this;
    }

    /**
     * Get max
     *
     * @return string
     */
    public function getHigh()
    {
        return $this->high;
    }

    /**
     * @return string
     */
    public function getVeryLow()
    {
        return $this->veryLow;
    }

    /**
     * @param string $veryLow
     * @return ParameterReference
     */
    public function setVeryLow($veryLow)
    {
        $this->veryLow = $veryLow;
        return $this;
    }

    /**
     * @return string
     */
    public function getVeryHigh()
    {
        return $this->veryHigh;
    }

    /**
     * @param string $veryHigh
     * @return ParameterReference
     */
    public function setVeryHigh($veryHigh)
    {
        $this->veryHigh = $veryHigh;
        return $this;
    }

    /**
     * @return string
     */
    public function getReferenceDisplay()
    {
        return $this->referenceDisplay;
    }

    /**
     * @param string $referenceDisplay
     * @return ParameterReference
     */
    public function setReferenceDisplay($referenceDisplay)
    {
        $this->referenceDisplay = $referenceDisplay;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return ParameterReference
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param string $imageName
     * @return ParameterReference
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
        return $this;
    }




    /**
     * @return ParameterBase
     */
    public function getParameterBase() {
        return $this->parameterBase;
    }

    /**
     * @param ParameterBase $parameterBase
     * @return ParameterReference
     */
    public function setParameterBase($parameterBase) {
        $this->parameterBase = $parameterBase;
        return $this;
    }

}

