<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * NotificationEndpoint
 */
class NotificationEndpoint
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $subscriptionUrl;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $xslt = null;

    /** @var bool */
    private $veterinaryCanSelect = false;

    /** @var bool */
    private $usesNotificationId = false;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubscriptionUrl()
    {
        return $this->subscriptionUrl;
    }

    /**
     * @param string $subscriptionUrl
     * @return $this
     */
    public function setSubscriptionUrl($subscriptionUrl)
    {
        $this->subscriptionUrl = $subscriptionUrl;
        return $this;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getXslt()
    {
        return $this->xslt;
    }

    /**
     * @param string $xslt
     * @return $this
     */
    public function setXslt($xslt)
    {
        $this->xslt = $xslt;
        return $this;
    }

    /**
     * @return boolean
     */
    public function veterinaryCanSelect()
    {
        return $this->veterinaryCanSelect;
    }

    /**
     * @param boolean $veterinaryCanSelect
     * @return $this
     */
    public function setVeterinaryCanSelect($veterinaryCanSelect)
    {
        $this->veterinaryCanSelect = $veterinaryCanSelect;
        return $this;
    }

    /**
     * @return boolean
     */
    public function usesNotificationId()
    {
        return $this->usesNotificationId;
    }

    /**
     * @param boolean $usesNotificationId
     * @return $this
     */
    public function setUsesNotificationId($usesNotificationId)
    {
        $this->usesNotificationId = $usesNotificationId;
        return $this;
    }


}

