<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * ParameterProfileComment
 */
class ParameterProfileComment
{
    /** @var integer */
    private $id;

    /** @var  string */
    private $reference;
    /** @var array */
    private $descriptions;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getReference() {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return ParameterProfileComment
     */
    public function setReference($reference) {
        $this->reference = $reference;
        return $this;
    }
    /**
     * @return array
     */
    public function getDescriptions() {
        return $this->descriptions;
    }

    /**
     * @return ParameterProfileCommentDescription
     */
    public function getDefaultDescription() {
        return count($this->descriptions) ? $this->descriptions[0] : null;
    }

    /**
     * @param string $language
     * @return ParameterProfileCommentDescription
     */
    public function getDescription($language) {
        /** @var ParameterProfileCommentDescription $description */
        foreach ($this->descriptions as $description) {
            if ($description->getLanguage() == $language) {
                return $description;
            }
        }
        return $this->getDefaultDescription();
    }

    /**
     * @param ParameterProfileCommentDescription $description
     * @return ParameterProfile
     */
    public function addDescription($description) {
        $this->descriptions[] = $description;
        return $this;
    }

}

