<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * Parameter
 */
class ParameterReference
{

    /**
     * @var float
     */
    private $low;

    /**
     * @var float
     */
    private $high;

    /**
     * @var string
     */
    private $display;

    /**
     * @var string
     */
    private $image;


    /**
     * Set min
     *
     * @param float $low
     *
     * @return ParameterReference
     */
    public function setLow($low)
    {
        $this->low = $low;

        return $this;
    }

    /**
     * Get min
     *
     * @return float
     */
    public function getLow()
    {
        return $this->low;
    }

    /**
     * Set max
     *
     * @param float $high
     *
     * @return ParameterReference
     */
    public function setHigh($high)
    {
        $this->high = $high;
        return $this;
    }

    /**
     * Get max
     *
     * @return float
     */
    public function getHigh()
    {
        return $this->high;
    }

    /**
     * @param $display
     * @return ParameterReference
     */
    public function setDisplay($display) {
        $this->display = $display;
        return $this;
    }

    /**
     * @return string
     */
    public function getDisplay() {
        return $this->display;
    }

    /**
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * @param string $image
     * @return ParameterReference
     */
    public function setImage($image) {
        $this->image = $image;
        return $this;
    }

}

