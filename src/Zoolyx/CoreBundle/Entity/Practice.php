<?php

namespace Zoolyx\CoreBundle\Entity;
use Doctrine\Common\Collections\Collection;

/**
 * GroupPractice
 */
class Practice
{
    /** @var integer */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $practiceId;

    /** @var string */
    private $description;

    /** @var string */
    private $street;

    /** @var string */
    private $city;

    /** @var string */
    private $zipCode;

    /** @var string */
    private $country;

    /** @var string */
    private $telephone = '';

    /** @var string */
    private $pickupTill = null;

    /** @var Collection  */
    private $veterinaryPractices = array();

    /** @var bool */
    private $sendNotifications = false;

    /** @var NotificationEndpoint */
    private $notificationEndpoint = null;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return Practice
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPracticeId()
    {
        return $this->practiceId;
    }

    /**
     * @param string $practiceId
     * @return Practice
     */
    public function setPracticeId($practiceId)
    {
        $this->practiceId = $practiceId;
        return $this;
    }

    /**
     * Set description
     * @param string $description
     * @return Practice
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return Practice
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return Practice
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return Practice
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param $zipCode
     * @return Practice
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     * @return $this;
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * @return string
     */
    public function getPickupTill()
    {
        return $this->pickupTill;
    }

    /**
     * @param string $pickupTill
     * @return $this
     */
    public function setPickupTill($pickupTill)
    {
        $this->pickupTill = $pickupTill;
        return $this;
    }

    /**
     * @param VeterinaryPractice $veterinaryPractice
     * @return Veterinary
     */
    public function addVeterinaryPractice(VeterinaryPractice $veterinaryPractice)
    {
        $this->veterinaryPractices[] = $veterinaryPractice;
        return $this;
    }

    /**
     * @param VeterinaryPractice $veterinaryPractice
     * @return Veterinary
     */
    public function removeVeterinaryPractice(VeterinaryPractice $veterinaryPractice)
    {
        $this->veterinaryPractices->removeElement($veterinaryPractice);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getVeterinaryPractices()
    {
        return $this->veterinaryPractices;
    }

    /**
     * @return boolean
     */
    public function isSendNotifications()
    {
        return $this->sendNotifications;
    }

    /**
     * @param boolean $sendNotifications
     * @return $this
     */
    public function setSendNotifications($sendNotifications)
    {
        $this->sendNotifications = $sendNotifications;
        return $this;
    }

    /**
     * @return NotificationEndpoint
     */
    public function getNotificationEndpoint()
    {
        return $this->notificationEndpoint;
    }

    /**
     * @param NotificationEndpoint $notificationEndpoint
     * @return $this
     */
    public function setNotificationEndpoint($notificationEndpoint)
    {
        $this->notificationEndpoint = $notificationEndpoint;
        return $this;
    }

}

