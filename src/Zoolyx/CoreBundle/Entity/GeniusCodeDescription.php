<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * GeniusCodeDescription
 */
class GeniusCodeDescription extends Description
{
    /** @var GeniusCode */
    private $geniusCode;

    /**
     * @return GeniusCode
     */
    public function getGeniusCode()
    {
        return $this->geniusCode;
    }

    /**
     * @param GeniusCode $geniusCode
     * @return $this
     */
    public function setGeniusCode($geniusCode)
    {
        $this->geniusCode = $geniusCode;
        return $this;
    }

}

