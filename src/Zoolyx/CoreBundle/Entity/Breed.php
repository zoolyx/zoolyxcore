<?php

namespace Zoolyx\CoreBundle\Entity;
use Doctrine\Common\Collections\Collection;

/**
 * Parameter
 */
class Breed
{
    /** @var integer */
    private $id;

    /** @var string */
    private $breedId;

    /** @var Species */
    private $species = null;

    /** @var Collection */
    private $descriptions = array();


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBreedId() {
        return $this->breedId;
    }

    /**
     * @param $breedId
     * @return Breed
     */
    public function setBreedId($breedId) {
        $this->breedId = $breedId;
        return $this;
    }

    /**
     * @return Species
     */
    public function getSpecies() {
        return $this->species;
    }

    /**
     * @param Species $species
     * @return $this
     */
    public function setSpecies($species) {
        $this->species = $species;
        return $this;
    }
    /**
     * @return Collection
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * @param string $language
     * @return string
     */
    public function getDescription($language = "nl") {
        /** @var BreedDescription $description */
        foreach ($this->descriptions as $description) {
            if ($description->getLanguage() == $language) {
                return $description->getDescription();
            }
        }
        return "";
    }


    /**
     * @param BreedDescription $description
     * @return Species
     */
    public function addDescription($description)
    {
        $this->descriptions[] = $description;
        return $this;
    }

}

