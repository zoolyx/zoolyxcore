<?php

namespace Zoolyx\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class Contractor extends AddressedUser
{
    /** @var string */
    private $contractorId;
    /** @var string */
    private $xslFile = null;
    /** @var string */
    private $xsdFile = null;

    /** @var Collection */
    private $contractorReports;

    /**
     * @return string
     */
    public function getContractorId() {
        return $this->contractorId;
    }

    /**
     * @param string $contractorId
     * @return Contractor
     */
    public function setContractorId($contractorId) {
        $this->contractorId = $contractorId;
        return $this;
    }

    /**
     * @return string
     */
    public function getXsdFile()
    {
        return $this->xsdFile;
    }

    /**
     * @param string $xsdFile
     */
    public function setXsdFile($xsdFile)
    {
        $this->xsdFile = $xsdFile;
    }

    /**
     * @return string
     */
    public function getXslFile()
    {
        return $this->xslFile;
    }

    /**
     * @param string $xslFile
     */
    public function setXslFile($xslFile)
    {
        $this->xslFile = $xslFile;
    }

    /**
     * @param ContractorReport $contractorReport
     * @return Contractor
     */
    public function addContractorReport(ContractorReport $contractorReport) {
        $this->contractorReports[] = $contractorReport;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getContractorReports() {
        return $this->contractorReports;
    }
}

