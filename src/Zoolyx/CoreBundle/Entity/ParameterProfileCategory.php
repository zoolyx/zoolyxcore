<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * ParameterProfileCategory
 */
class ParameterProfileCategory
{
    /** @var integer */
    private $id;

    /** @var  string */
    private $reference;
    /** @var array */
    private $descriptions;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getReference() {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return ParameterProfileCategory
     */
    public function setReference($reference) {
        $this->reference = $reference;
        return $this;
    }
    /**
     * @return array
     */
    public function getDescriptions() {
        return $this->descriptions;
    }

    /**
     * @return ParameterProfileCategoryDescription
     */
    public function getDefaultDescription() {
        return count($this->descriptions) ? $this->descriptions[0] : null;
    }

    /**
     * @param string $language
     * @return ParameterProfileCategoryDescription
     */
    public function getDescription($language) {
        /** @var ParameterProfileCategoryDescription $description */
        foreach ($this->descriptions as $description) {
            if ($description->getLanguage() == $language) {
                return $description;
            }
        }
        return $this->getDefaultDescription();
    }

    /**
     * @param ParameterProfileCategoryDescription $description
     * @return ParameterProfile
     */
    public function addDescription($description) {
        $this->descriptions[] = $description;
        return $this;
    }

}

