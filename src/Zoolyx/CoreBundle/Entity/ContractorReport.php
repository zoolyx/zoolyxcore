<?php

namespace Zoolyx\CoreBundle\Entity;

use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class ContractorReport
{
    const CONTRACTOR_NEW_REPORT = 0;
    const CONTRACTOR_REPORT_SEEN = 1;
    const CONTRACTOR_REPORT_UPDATED = 2;

    /** @var int */
    private $id;

    /** @var int */
    private $status;

    /** @var \DateTime */
    private $created;

    /** @var \DateTime */
    private $confirmed;

    /** @var Contractor */
    private $contractor;

    /** @var Report */
    private $report;

    /** @var ContractorRequest $contractorRequest */
    private $contractorRequest = null;


    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param int $status
     * @return ContractorRequest
     */
    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return ContractorRequest
     */
    public function setCreated($created) {
        $this->created = $created;
        return $this;
    }
    /**
     * @return \DateTime
     */
    public function getConfirmed() {
        return $this->confirmed;
    }

    /**
     * @param \DateTime $confirmed
     * @return ContractorRequest
     */
    public function setConfirmed($confirmed) {
        $this->confirmed = $confirmed;
        return $this;
    }

    /**
     * @return Contractor
     */
    public function getContractor() {
        return $this->contractor;
    }

    /**
     * @param Contractor $contractor
     * @return ContractorRequest
     */
    public function setContractor($contractor) {
        $this->contractor = $contractor;
        return $this;
    }

    /**
     * @return Report
     */
    public function getReport() {
        return $this->report;
    }

    /**
     * @param Report $report
     * @return ContractorRequest
     */
    public function setReport($report) {
        $this->report = $report;
        return $this;
    }


    /**
     * @return ContractorRequest
     */
    public function getContractorRequest() {
        return $this->contractorRequest;
    }

    /**
     * @param ContractorRequest $contractorRequest
     * @return Report
     */
    public function setContractorRequest($contractorRequest) {
        $this->contractorRequest = $contractorRequest;
        return $this;
    }
}

