<?php

namespace Zoolyx\CoreBundle\Entity;
use Zoolyx\CoreBundle\Model\Report\VeterinaryPracticeReport\VeterinaryPracticeReportRoles;

/**
 * VeterinaryPracticeReport
 */
class VeterinaryPracticeReport
{
    /** @var integer */
    private $id;

    /** @var Report */
    private $report;

    /** @var VeterinaryPractice */
    private $veterinaryPractice;

    /** @var integer */
    private $role = VeterinaryPracticeReportRoles::VPR_VETERINARY;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param $report
     * @return VeterinaryPracticeReport
     */
    public function setReport($report)
    {
        $this->report = $report;
        return $this;
    }

    /**
     * @return VeterinaryPractice
     */
    public function getVeterinaryPractice()
    {
        return $this->veterinaryPractice;
    }

    /**
     * @param $veterinaryPractice
     * @return VeterinaryPracticeReport
     */
    public function setVeterinaryPractice($veterinaryPractice)
    {
        $this->veterinaryPractice = $veterinaryPractice;
        return $this;
    }


    /**
     * Set role
     *
     * @param integer $role
     * @return VeterinaryPractice
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * Get role
     *
     * @return integer
     */
    public function getRole()
    {
        return $this->role;
    }


}

