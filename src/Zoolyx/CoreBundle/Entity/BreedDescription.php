<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * BreedDescription
 */
class BreedDescription extends Description
{
    /** @var Breed */
    private $breed;

    /**
     * @return Breed
     */
    public function getBreed() {
        return $this->breed;
    }

    /**
     * @param Breed $breed
     * @return BreedDescription
     */
    public function setBreed($breed) {
        $this->breed = $breed;
        return $this;
    }
}

