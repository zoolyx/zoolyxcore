<?php

namespace Zoolyx\CoreBundle\Entity;

/**
 * SpeciesDescription
 */
class SpeciesDescription extends Description
{
    /** @var Species */
    private $species;

    /**
     * @return Species
     */
    public function getSpecies() {
        return $this->species;
    }

    /**
     * @param Species $species
     * @return SpeciesDescription
     */
    public function setSpecies($species) {
        $this->species = $species;
        return $this;
    }
}

