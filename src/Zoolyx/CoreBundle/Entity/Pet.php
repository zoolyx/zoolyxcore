<?php

namespace Zoolyx\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;

/**
 * Pet
 */
class Pet
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $petId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $speciesCode;

    /**
     * @var \DateTime
     */
    private $birthDate;

    /**
     * @var string
     */
    private $gender;

    /**
     * @var string
     */
    private $breed;

    /**
     * @var string
     */
    private $breedCode;

    /**
     * @var string
     */
    private $chipNbr = null;

    /**
     * @var string
     */
    private $tattooNbr = null;

    /**
     * @var string
     */
    private $pedigreeNbr = null;


    /**
     * @var Collection
     */
    private $samples = array();


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPetId()  {
        return $this->petId;
    }

    /**
     * @param string $petId
     * @return Pet
     */
    public function setPetId($petId)  {
        $this->petId = $petId;
        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Pet
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set speciesCode
     *
     * @param string $speciesCode
     *
     * @return Pet
     */
    public function setSpeciesCode($speciesCode)
    {
        $this->speciesCode = $speciesCode;

        return $this;
    }

    /**
     * Get speciesCode
     *
     * @return string
     */
    public function getSpeciesCode()
    {
        return $this->speciesCode;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return Pet
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return Pet
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set breed
     *
     * @param string $breed
     *
     * @return Pet
     */
    public function setBreed($breed)
    {
        $this->breed = $breed;

        return $this;
    }

    /**
     * Get breed
     *
     * @return string
     */
    public function getBreed()
    {
        return $this->breed;
    }

    /**
     * @return string
     */
    public function getBreedCode()
    {
        return $this->breedCode;
    }

    /**
     * @param string $breedCode
     * @return $this
     */
    public function setBreedCode($breedCode)
    {
        $this->breedCode = $breedCode;
        return $this;
    }



    /**
     * Set chipNbr
     *
     * @param string $chipNbr
     *
     * @return Pet
     */
    public function setChipNbr($chipNbr)
    {
        if ($chipNbr != '') $this->chipNbr = $chipNbr;

        return $this;
    }

    /**
     * Get chipNbr
     *
     * @return string
     */
    public function getChipNbr()
    {
        return $this->chipNbr;
    }

    /**
     * @param string $tattooNbr
     * @return $this
     */
    public function setTattooNbr($tattooNbr) {
        if ($tattooNbr != '') $this->tattooNbr = $tattooNbr;
        return $this;
    }

    /**
     * @return string
     */
    public function getTattooNbr() {
        return $this->tattooNbr;
    }

    /**
     * @param string $pedigreeNbr
     * @return $this
     */
    public function setPedigreeNbr($pedigreeNbr) {
        if ($pedigreeNbr != '') $this->pedigreeNbr = $pedigreeNbr;
        return $this;
    }

    /**
     * @return string
     */
    public function getPedigreeNbr() {
        return $this->pedigreeNbr;
    }

    /**
     * @param Sample $sample
     * @return Pet
     */
    public function addSample(Sample $sample) {
        $this->samples[] = $sample;
        return $this;
    }

    /**
     * @param Sample $sample
     * @return Pet
     */
    public function removeSample(Sample $sample) {
        $this->samples->removeElement($sample);
        return $this;
    }
    /**
     * @return Collection
     */
    public function getSamples() {
        return $this->samples;
    }
}

