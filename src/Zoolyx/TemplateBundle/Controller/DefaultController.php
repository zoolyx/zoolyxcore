<?php

namespace Zoolyx\TemplateBundle\Controller;


use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

    public function confirmRegistrationAction()
    {
        return $this->render('ZoolyxTemplateBundle:Mail/Registration:confirm_email_address_nl.html.twig', array(
            'confirmationUrl' => 'https://test.com'
        ));
    }


}
