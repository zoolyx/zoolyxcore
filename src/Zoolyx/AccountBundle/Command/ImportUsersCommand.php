<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 13.03.17
 * Time: 16:28
 */

namespace Zoolyx\AccountBundle\Command;

use Doctrine\Common\Persistence\ObjectManager;
use DOMDocument;
use Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Zoolyx\CoreBundle\Entity\NotificationEndpoint;
use Zoolyx\CoreBundle\Entity\Practice;
use Zoolyx\CoreBundle\Entity\Repository\UserRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryPracticeRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\NotificationBundle\Entity\Notification;
use Zoolyx\NotificationBundle\Entity\ReportNotification;
use Zoolyx\NotificationBundle\Entity\Repository\ReportNotificationRepository;
use Zoolyx\NotificationBundle\Model\NotificationGenerator;

class ImportUsersCommand extends ContainerAwareCommand {

    const FIELD_USERNAME = 'username';
    const FIELD_PASSWORD =  'password';
    const FIELD_LIMS_ID = 'limsId';
    const FIELD_EXTRA_LIMS = 'extra_lims';
    const FIELD_LAST_NAME = 'lastName';
    const FIELD_FIRST_NAME = 'firstName';
    const FIELD_STREET = 'street';
    const FIELD_ZIP_CODE = 'zipCode';
    const FIELD_CITY = 'city';
    const FIELD_TELEPHONE = 'telephone';
    const FIELD_FAX = 'fax';
    const FIELD_EMAIL = 'email';
    const FIELD_LANGUAGE = 'language';
    const FIELD_ORDER_NUMBER = 'orderNumber';
    const FIELD_PRACTICE_NAME = 'practiceName';
    const FIELD_GROUP = 'group';
    const FIELD_COUNTRY = 'country';
    const FIELD_PRACTICE_ID = 'practiceId';

    protected function configure()
    {
        $this
            ->addArgument('file', InputArgument::REQUIRED, 'csv-file (including path)')
            // the name of the command (the part after "bin/console")
            ->setName('app:import-users')

            // the short description shown while running "php bin/console list"
            ->setDescription('Imports users from a csv file')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Imports users from a csv file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logging = 1;

        $filename = $input->getArgument('file') ? $input->getArgument('file') : "";

        if (!file_exists($filename)) {
            $output->writeln('This file does not exist : '.$filename);
            return;
        }

        /** @var ObjectManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');

        $fieldsToImport = array(
            0  => self::FIELD_USERNAME,
            1  => self::FIELD_PASSWORD,
            5  => self::FIELD_LIMS_ID,
            17 => self::FIELD_LAST_NAME,
            18 => self::FIELD_FIRST_NAME,
            19 => self::FIELD_STREET,
            20 => self::FIELD_ZIP_CODE,
            21 => self::FIELD_CITY,
            22 => self::FIELD_TELEPHONE,
            23 => self::FIELD_FAX,
            24 => self::FIELD_EMAIL,
            25 => self::FIELD_LANGUAGE,
            27 => self::FIELD_ORDER_NUMBER,
            28 => self::FIELD_PRACTICE_NAME,
            29 => self::FIELD_GROUP,
            31 => self::FIELD_COUNTRY,
            32 => self::FIELD_PRACTICE_ID
        );

        $importUsers = array();
        if (($handle = fopen($filename, "r")) !== FALSE) {
            $row = 0;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $row++;
                $userData = array();
                foreach ($fieldsToImport as $index => $fieldName) {
                    $userData[$fieldName] = $data[$index];
                }
                $userData[self::FIELD_EXTRA_LIMS] = array();
                for ($i=6; $i<15; $i++) {
                    if ($data[$i] != '') {
                        $userData[self::FIELD_EXTRA_LIMS][] = $data[$i];
                    }
                }

                if ($row == 1) {
                    print_r($userData);
                    $helper = $this->getHelper('question');
                    $question = new ConfirmationQuestion('Are these columns correct', true);

                    if (!$helper->ask($input, $output, $question)) {
                        return;
                    }
                } else {
                    $importUsers[] = $userData;
                }
            }
            fclose($handle);
        }

        //validate fields
        foreach ($importUsers as $index=>$importUser) {
            $importUsers[$index][self::FIELD_LANGUAGE] = $importUser[self::FIELD_LANGUAGE]==0 ? 'nl' : 'fr';
            if ($importUser[self::FIELD_EMAIL] == '') {
                $output->writeln('no email for : '.$index);
            }
        }

        /** @var UserRepository $userRepository */
        $userRepository = $this->getContainer()->get('zoolyx_core.repository.user');
        /** @var VeterinaryRepository $veterinaryRepository */
        $veterinaryRepository = $this->getContainer()->get('zoolyx_core.repository.veterinary');
        /** @var VeterinaryPracticeRepository $veterinaryPracticeRepository */
        $veterinaryPracticeRepository = $this->getContainer()->get('zoolyx_core.repository.veterinary_practice');

        /*
        foreach ($importUsers as $index=>$importUser) {
            $user1 = $userRepository->findOneBy(array('email'=>$importUser[self::FIELD_EMAIL]));
            $user2 = $userRepository->findOneBy(array('username'=>$importUser[self::FIELD_USERNAME]));
            if ($user1 && $user2 && $user1->getId()!=$user2->getId()) {
                $output->writeln('Duplicate record for '.$importUser[self::FIELD_USERNAME]);
            }
        }
        die();
        */
        foreach ($importUsers as $index=>$importUser) {
            if ($logging) $output->writeln('Importing '.$importUser[self::FIELD_EMAIL]);
            /** @var User $user */
            $user = $userRepository->findOneBy(array('email'=>$importUser[self::FIELD_EMAIL]));
            if (!$user) {
                $user = $userRepository->findOneBy(array('username'=>$importUser[self::FIELD_USERNAME]));
            }
            if (!$user) {
                if ($logging) $output->writeln('Account not found '.$importUser[self::FIELD_EMAIL]);
                $user = new User();
                $user->setEmail($importUser[self::FIELD_EMAIL]);
                $user->setPassword($importUser[self::FIELD_PASSWORD]);
                $user
                    ->setEmailCanonical($importUser[self::FIELD_EMAIL])
                    ->setUsername($importUser[self::FIELD_USERNAME]!='' ? $importUser[self::FIELD_USERNAME] : $importUser[self::FIELD_EMAIL])
                    ->setUsernameCanonical($importUser[self::FIELD_USERNAME]!='' ? $importUser[self::FIELD_USERNAME] : $importUser[self::FIELD_EMAIL])
                    ->setFirstName($importUser[self::FIELD_FIRST_NAME])
                    ->setLastName($importUser[self::FIELD_LAST_NAME])
                    ->setStreet($importUser[self::FIELD_STREET])
                    ->setZipCode($importUser[self::FIELD_ZIP_CODE])
                    ->setCity($importUser[self::FIELD_CITY])
                    ->setCountry($importUser[self::FIELD_COUNTRY])
                    ->setLanguage($importUser[self::FIELD_LANGUAGE])
                ;

                $output->writeln('created a new account for : '.$importUser[self::FIELD_EMAIL]);
            }
            if ($logging) $output->writeln('Using Account '.$user->getId());
            if ($importUser[self::FIELD_USERNAME]!='') {
                $user
                    ->setEmailCanonical($importUser[self::FIELD_EMAIL])
                    ->setUsername($importUser[self::FIELD_USERNAME])
                    ->setUsernameCanonical($importUser[self::FIELD_USERNAME])
                    ->setFirstName($importUser[self::FIELD_FIRST_NAME])
                    ->setLastName($importUser[self::FIELD_LAST_NAME])
                    ->setStreet($importUser[self::FIELD_STREET])
                    ->setZipCode($importUser[self::FIELD_ZIP_CODE])
                    ->setCity($importUser[self::FIELD_CITY])
                    ->setCountry($importUser[self::FIELD_COUNTRY])
                    ->setLanguage($importUser[self::FIELD_LANGUAGE])
                ;
            }

            if (!$user->hasRole('ROLE_VETERINARY')) $user->addRole('ROLE_VETERINARY');
            $entityManager->persist($user);


            /** @var Veterinary $veterinary */
            $veterinary = $veterinaryRepository->searchForAccount($user);
            if (!$veterinary) {
                $veterinary = new Veterinary();
                $veterinary->setAccount($user)->setFa('');
                $output->writeln('create veterinary for : '.$importUser[self::FIELD_EMAIL]);
            }
            //$veterinary->setOrderNumber($importUser[self::FIELD_ORDER_NUMBER]);
            $entityManager->persist($veterinary);

            /** @var VeterinaryPractice $veterinaryPractice */
            if ($logging) $output->writeln('Handling Lims Id '.$importUser[self::FIELD_LIMS_ID]);

            $veterinaryPractice = $veterinaryPracticeRepository->findOneBy(array('limsId' => $importUser[self::FIELD_LIMS_ID]));
            if (!$veterinaryPractice) {
                $veterinaryPractice = new VeterinaryPractice();
                $veterinaryPractice->setLimsId($importUser[self::FIELD_LIMS_ID])->setRole(0);
            }
            $veterinaryPractice->setVeterinary($veterinary);

            $practice = $veterinaryPractice->getPractice();
            if (is_null($practice)) {
                $practice = new Practice();
                $veterinaryPractice->setPractice($practice);
            }
            if ($veterinaryPractice->getRole() == 0) {
                $practice->setName($importUser[self::FIELD_PRACTICE_NAME]!='' ? $importUser[self::FIELD_PRACTICE_NAME] : $importUser[self::FIELD_FIRST_NAME] . " " . $importUser[self::FIELD_LAST_NAME] );
                $practice
                    ->setDescription('')
                    ->setStreet($importUser[self::FIELD_STREET])
                    ->setZipCode($importUser[self::FIELD_ZIP_CODE])
                    ->setCity($importUser[self::FIELD_CITY])
                    ->setCountry($importUser[self::FIELD_COUNTRY])
                ;
            }
            $entityManager->persist($practice);
            $entityManager->persist($veterinaryPractice);

            $mainPractice = $practice;

            foreach ($importUser[self::FIELD_EXTRA_LIMS] as $extraLims) {
                $output->writeln('adding extra lims : '.$extraLims);
                $veterinaryPractice = $veterinaryPracticeRepository->findOneBy(array('limsId' => $extraLims));
                if (!$veterinaryPractice) {
                    $veterinaryPractice = new VeterinaryPractice();
                    $veterinaryPractice->setLimsId($extraLims)
                    ;
                }

                $practice = $veterinaryPractice->getPractice();
                if ($practice) $entityManager->remove($practice);

                $veterinaryPractice->setRole(1);
                $veterinaryPractice->setPractice($mainPractice);
                $entityManager->persist($veterinaryPractice);
            }
            //if ($user[self::FIELD_TELEPHONE] != $user->getTelephone()) $differenceFound = true;
            //if ($user[self::FIELD_FAX] != $user->getFax()) $differenceFound = true;
            /*
                29 => self::FIELD_GROUP,
                32 => self::FIELD_PRACTICE_ID
            */
            $entityManager->flush();
        }

    }

    private function describe($data) {
        $separator = ",";
        $output = '';
        $output.= $data[self::FIELD_EMAIL];
        $output.= $separator . $data[self::FIELD_USERNAME];
        $output.= $separator . $data[self::FIELD_FIRST_NAME];
        $output.= $separator . $data[self::FIELD_LAST_NAME];
        $output.= $separator . $data[self::FIELD_STREET];
        $output.= $separator . $data[self::FIELD_ZIP_CODE];
        $output.= $separator . $data[self::FIELD_CITY];
        $output.= $separator . $data[self::FIELD_COUNTRY];
        $output.= $separator . $data[self::FIELD_LANGUAGE];


        return $output;
    }
    private function toArray(User $user) {
        $data = array();
        $data[self::FIELD_EMAIL] = $user->getEmail();
        $data[self::FIELD_USERNAME] = $user->getUsername();
        $data[self::FIELD_FIRST_NAME] = $user->getFirstName();
        $data[self::FIELD_LAST_NAME] = $user->getLastName();
        $data[self::FIELD_STREET] = $user->getStreet();
        $data[self::FIELD_ZIP_CODE] = $user->getZipCode();
        $data[self::FIELD_CITY] = $user->getCity();
        $data[self::FIELD_COUNTRY] = $user->getCountry();
        $data[self::FIELD_LANGUAGE] = $user->getLanguage();

        return $data;
    }
}