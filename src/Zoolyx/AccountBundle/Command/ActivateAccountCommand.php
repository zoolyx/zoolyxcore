<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 13.03.17
 * Time: 16:28
 */

namespace Zoolyx\AccountBundle\Command;

use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zoolyx\CoreBundle\Entity\User;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Zoolyx\NotificationBundle\Model\Mail\MailMessage;


class ActivateAccountCommand extends ContainerAwareCommand {



    protected function configure()
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'email')
            // the name of the command (the part after "bin/console")
            ->setName('app:activate-account')

                // the short description shown while running "php app/console list"
            ->setDescription('Invite a user to activate his account')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Invite a user to activate his account')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument('email') ? $input->getArgument('email') : "";

        /** @var UserManager $userManager */
        $userManager = $this->getContainer()->get('fos_user.user_manager');
        /** @var User $user */
        $user = $userManager->findUserByEmail($email);

        if ($user) {
            $tokenGenerator = $this->getContainer()->get('zoolyx_account.generator.token');
            $confirmationToken = $tokenGenerator->get(32);
            $user
                ->setConfirmationToken($confirmationToken)
                ->setPasswordRequestedAt(new \DateTime())
                ->setEnabled(false)
                ->setResetPassword(false)
            ;



            $language = $user->getLanguage();

            /** @var Router $router */
            $router = $this->getContainer()->get('router');
            $context = $router->getContext();
            $context->setHost($this->getContainer()->getParameter('base_url'));
            $context->setScheme($this->getContainer()->getParameter('scheme'));
            $context->setBaseUrl('');
            $changePasswordUrl = $router->generate('zoolyx_account_password_reset_with_language',array('token'=>$confirmationToken, 'language'=>$user->getLanguage()), UrlGeneratorInterface::ABSOLUTE_URL);

            /** @var TwigEngine $templating */
            $templating = $this->getContainer()->get('templating');

            $htmlContent = $templating->render('ZoolyxTemplateBundle:Mail/AccountActivation:migration_'.$language.'.html.twig', array(
                'changePasswordUrl' => $changePasswordUrl
            ));
            $plainContent = $templating->render('ZoolyxTemplateBundle:Mail/AccountActivation:migration_'.$language.'.plain.twig', array(
                'changePasswordUrl' => $changePasswordUrl
            ));

            $mailMessage = new MailMessage($email);
            $mailMessage
                ->setSubject("Activate account")
                ->setFrom($this->getContainer()->getParameter('from'))
                ->setTo($email)
                ->setHtml($htmlContent,'text/html')
                ->setPlain($plainContent ,'text/plain')
            ;

            $mailer = $this->getContainer()->get('mailer');
            $mailer->send($mailMessage->getSwiftMessage());

            /** @var ObjectManager $entityManager */
            $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
            $entityManager->persist($user);
            $entityManager->flush();
        }
    }

}