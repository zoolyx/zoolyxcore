<?php

namespace Zoolyx\AccountBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Zoolyx\AccountBundle\Forms\PasswordNew;
use Zoolyx\AccountBundle\Forms\PasswordNewType;
use Zoolyx\AccountBundle\Forms\PasswordReset;
use Zoolyx\AccountBundle\Forms\PasswordResetType;
use Zoolyx\AccountBundle\Model\Token\TokenGenerator;
use Zoolyx\CoreBundle\Entity\Repository\UserRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Model\Language\LanguageManager;
use Zoolyx\NotificationBundle\Model\Mail\MailMessage;

class PasswordController extends Controller
{
    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var UserRepository
     *
     * @DI\Inject("zoolyx_core.repository.user")
     */
    private $userRepository;

    /**
     * @var Swift_Mailer
     *
     * @DI\Inject("mailer")
     */
    private $mailer;

    /**
     * @var Translator
     *
     * @DI\Inject("translator")
     */
    private $translator;

    /**
     * @var LanguageManager
     *
     * @DI\Inject("zoolyx_core.language.manager")
     */
    private $languageManager;

    /**
     * @var TokenGenerator
     *
     * @DI\Inject("zoolyx_account.generator.token")
     */
    private $tokenGenerator;

    public function indexAction(Request $request)
    {
        $passwordReset = new PasswordReset();

        $form = $this->createForm(new PasswordResetType($this->translator), $passwordReset);

        if (!is_null($request)) {
            $form->handleRequest($request);
        };

        if ($form->isValid()) {
            $email = $passwordReset->getEmail();
            /** @var User $user */
            $user = $this->userRepository->findOneBy(array(
                'email' => $email
            ));
            if ($user) {
                $confirmationToken = $this->tokenGenerator->get(32);
                $user
                    ->setEnabled(true)
                    ->setConfirmationToken($confirmationToken)
                    ->setPasswordRequestedAt(new \DateTime())
                ;
                $this->entityManager->persist($user);
                $this->entityManager->flush();

                $language = $this->languageManager->validate($user->getLanguage(), $request);
        
                $changePasswordUrl = $this->generateUrl('zoolyx_account_password_reset_with_language',array('token'=>$confirmationToken, 'language'=>$language), UrlGeneratorInterface::ABSOLUTE_URL);

                $htmlContent = $this->renderView('ZoolyxTemplateBundle:Mail/Password:reset_'.$language.'.html.twig', array(
                    'changePasswordUrl' => $changePasswordUrl
                ));
                $plainContent = $this->renderView('ZoolyxTemplateBundle:Mail/Password:reset_'.$language.'.plain.twig', array(
                    'changePasswordUrl' => $changePasswordUrl
                ));

                $mailMessage = new MailMessage($email);
                $mailMessage
                    ->setSubject("Password reset")
                    ->setFrom($this->getParameter('from'))
                    ->setTo($email)
                    ->setHtml($htmlContent,'text/html')
                    ->setPlain($plainContent ,'text/plain')
                ;
                $this->mailer->send($mailMessage->getSwiftMessage());

                return $this->render('ZoolyxAccountBundle:Default:mail_sent.html.twig', array(
                    'email' => $email
                ));
            }
        }

        return $this->render('ZoolyxAccountBundle:Password:password_reset.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function resetAction(Request $request, $token, $language='') {
        $language = $this->languageManager->validate($language, $request);
        
        /** @var User $user */
        $user = $this->userRepository->findOneBy(array(
           'confirmationToken' => $token
        ));
        if (is_null($user)) {
            return $this->render('ZoolyxAccountBundle:Default:confirmation_fail.html.twig', array());
        }

        $passwordNew = new PasswordNew();
        $this->translator->setLocale($language);
        $form = $this->createForm(new PasswordNewType($this->translator), $passwordNew);
        if (!is_null($request)) {
            $form->handleRequest($request);
        };

        if ($form->isValid()) {
            $user
                ->setPlainPassword($passwordNew->getPassword())
                ->setConfirmationToken(null)
                ->setPasswordRequestedAt(null)
                ->setEnabled(true)
            ;

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            return $this->render('ZoolyxAccountBundle:Password:password_change_success.html.twig', array(
                'passwordNew' => $passwordNew
            ));
        }

        $languagePrefix = $request->getLocale() == 'fr' ? '/fr' : '';

        return $this->render('ZoolyxAccountBundle:Password:password_new.html.twig', array(
            'language' => $language,
            'form' => $form->createView(),
            'passwordNew' => $passwordNew,
            'url_conditions' => $this->getParameter('server_base_url') . $languagePrefix . '/disclaimer/',
            'url_privacy' => $this->getParameter('server_base_url') . $languagePrefix . '/privacy-clausule/'
        ));
    }

}
