<?php

namespace Zoolyx\AccountBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use DOMDocument;
use FOS\UserBundle\Model\UserManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Zoolyx\AccountBundle\Forms\Account\AccountMapper;
use Zoolyx\AccountBundle\Forms\Account\AccountPracticeType;
use Zoolyx\AccountBundle\Forms\Account\AccountType;
use Zoolyx\AccountBundle\Forms\Account\EmailNew;
use Zoolyx\AccountBundle\Forms\Account\EmailNewType;
use Zoolyx\AccountBundle\Forms\Account\PasswordNew;
use Zoolyx\AccountBundle\Forms\Account\PasswordNewType;
use Zoolyx\AccountBundle\Forms\Account\PracticeMapper;
use Zoolyx\AccountBundle\Forms\Account\PracticeMember;
use Zoolyx\AccountBundle\Forms\Account\VeterinaryMapper;
use Zoolyx\AccountBundle\Forms\Account\VeterinaryType;
use Zoolyx\AccountBundle\Forms\Account\VeterinaryWithNotificationsType;
use Zoolyx\AccountBundle\Model\Ftp\VeterinaryXml;
use Zoolyx\CoreBundle\Entity\NotificationEndpoint;
use Zoolyx\CoreBundle\Entity\Practice;
use Zoolyx\CoreBundle\Entity\Repository\NotificationEndpointRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryPracticeRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Vat;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Entity\VeterinaryPracticeReport;
use Zoolyx\CoreBundle\Model\Ftp\FtpService;
use Zoolyx\CoreBundle\Model\Report\ReportHashProvider;
use Zoolyx\CoreBundle\Model\Report\VeterinaryPracticeReport\VeterinaryPracticeReportRoles;

class AccountController extends Controller
{
    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var AccountMapper $accountMapper
     *
     *  @DI\Inject("zoolyx_account.mapper.account"),
     */
    private $accountMapper;

    /**
     * @var VeterinaryMapper $veterinaryMapper
     *
     *  @DI\Inject("zoolyx_account.mapper.veterinary"),
     */
    private $veterinaryMapper;

    /**
     * @var PracticeMapper $practiceMapper
     *
     *  @DI\Inject("zoolyx_account.mapper.practice"),
     */
    private $practiceMapper;

    /**
     * @var VeterinaryPracticeRepository $veterinaryPracticeRepository
     *
     *  @DI\Inject("zoolyx_core.repository.veterinary_practice"),
     */
    private $veterinaryPracticeRepository;

    /**
     * @var NotificationEndpointRepository
     *
     * @DI\Inject("zoolyx_core.repository.notification_endpoint")
     */
    private $notificationEndpointRepository;

    /**
     * @var Translator
     *
     * @DI\Inject("translator")
     */
    private $translator;

    /**
     * @var UserManager
     *
     * @DI\Inject("fos_user.user_manager")
     */
    private $userManager;

    /**
     * @var ReportHashProvider
     *
     * @DI\Inject("zoolyx_core.report.hash_provider")
     */
    private $hashProvider;

    /**
     * @var FtpService $ftpService
     *
     *  @DI\Inject("zoolyx_core.ftp.service"),
     */
    private $ftpService;

    /**
     * @var EncoderFactory $encoderFactory
     *
     *  @DI\Inject("security.encoder_factory"),
     */
    private $encoderFactory;

    /**
     * @var EmailNew $emailNew
     *
     *  @DI\Inject("zoolyx_account.form.email_new"),
     */
    private $emailNew;


    public function settingsAction(Request $request) {
        /** @var User $user */
        $user = $this->getUser();

        $account = $this->accountMapper->map($user);
        //var_dump($account);die();

        $form = $this->createForm(new AccountType($account,$this->translator), $account);

        if (!is_null($request)) {
            $form->handleRequest($request);
        };

        if ($form->isValid()) {
            /** @var NotificationEndpoint $notificationEndpoint */
            $user
                ->setLanguage($account->getLanguage())
                ->setReportFrequency($account->getReportFrequency())
            ;

            if ($account->isVeterinary()) {
                /** @var Veterinary $veterinary */
                $veterinary = $this->accountMapper->getVeterinary();
                $user->setAutoShareOwners($account->isAutoShareOwners());

                $veterinaryPractices = $this->veterinaryPracticeRepository->findBy(array('veterinary'=>$veterinary));
                /** @var VeterinaryPractice $veterinaryPractice */
                foreach ($veterinaryPractices as $veterinaryPractice) {
                    if ($veterinaryPractice->getRole() == PracticeMember::PRACTICE_ROLE_REMOVED) continue;
                    $this->sendToLims($veterinaryPractice);
                }
            }
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }



        return $this->render('ZoolyxAccountBundle:Account:settings.html.twig', array(
            'form' => $form->createView(),
            'user' => $user,
            'account' => $account
        ));
    }

    public function changePasswordAction(Request $request) {
        /** @var User $user */
        $user = $this->getUser();

        $newPassword = new PasswordNew();
        $newPassword
            ->setEncoderFactory($this->encoderFactory)
            ->setUser($user)
        ;
        $form = $this->createForm(new PasswordNewType($this->translator), $newPassword);

        if (!is_null($request)) {
            $form->handleRequest($request);
        };

        if ($form->isValid()) {
            $user->setPlainPassword($newPassword->getNewPassword());
            $this->userManager->updatePassword($user);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            return $this->render('ZoolyxAccountBundle:Account:new_password_confirm.html.twig', array());
        }

        return $this->render('ZoolyxAccountBundle:Account:new_password.html.twig', array(
            'form' => $form->createView(),
            'user' => $user
        ));
    }

    public function changeEmailAction(Request $request, $id, $veterinaryPracticeId) {
        /** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = $this->veterinaryPracticeRepository->find($veterinaryPracticeId);
        /** @var User $user */
        $user = null;
        if ($veterinaryPractice->getVeterinary() && $veterinaryPractice->getVeterinary()->getAccount()) {
            $user = $veterinaryPractice->getVeterinary()->getAccount();
        }

        if (is_null($user)) {
            return $this->redirectToRoute('zoolyx_practice_settings', array('id'=>$id));
        }

        $this->emailNew->setEmail($user->getEmail());
        $form = $this->createForm(new EmailNewType($this->translator), $this->emailNew);

        if (!is_null($request)) {
            $form->handleRequest($request);
        };

        if ($form->isValid()) {
            $user
                ->setEmail($this->emailNew->getEmail())
                ->setUsername($this->emailNew->getEmail())
            ;
            $this->userManager->updatePassword($user);
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $this->sendToLims($veterinaryPractice);

            return $this->render('ZoolyxAccountBundle:Account:new_email_confirm.html.twig', array());
        }

        return $this->render('ZoolyxAccountBundle:Account:new_email.html.twig', array(
            'form' => $form->createView(),
            'user' => $user,
            'id' => $id,
            'veterinaryPracticeId' => $veterinaryPracticeId
        ));

    }

    public function practiceAction($id, Request $request) {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user->hasRole('ROLE_VETERINARY')) {
            return $this->redirectToRoute('zoolyx_account_settings');
        }

        $accountPractice = $this->practiceMapper->map($user,$id);

        $form = $this->createForm(new AccountPracticeType($this->translator, $this->notificationEndpointRepository->findAll()), $accountPractice);

        if (!is_null($request)) {
            $form->handleRequest($request);
        };

        /** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = $this->veterinaryPracticeRepository->find($id);

        //check validity of the transfercode
        if (!is_null($veterinaryPractice->getTransferCode())) {
            if (is_null($veterinaryPractice->getValidTill()) || (!is_null($veterinaryPractice->getValidTill()) && new \DateTime() > $veterinaryPractice->getValidTill() )) {
                $veterinaryPractice
                    ->setTransferCode(null)
                    ->setValidTill(null);
                $this->entityManager->persist($veterinaryPractice);
                $this->entityManager->flush();
            }
        }

        if ($form->isValid()) {
            if ($accountPractice->getVat() != '') {
                if (is_null($veterinaryPractice->getVat())) {
                    $vat = new Vat();
                    $vat->setFaId('')->setVat($accountPractice->getVat());
                } else {
                    $vat = $veterinaryPractice->getVat();
                    $vat->setVat($accountPractice->getVat());
                }

                $this->entityManager->persist($vat);
                //update vat for all vp's in this practice
                /** @var VeterinaryPractice $vp */
                foreach ($veterinaryPractice->getPractice()->getVeterinaryPractices() as $vp) {
                    $vp->setVat($vat);
                    $this->entityManager->persist($vp);
                }
            }
            $veterinaryPractice->setInvoiceDefault($accountPractice->getInvoiceDefault());
            $practice = $veterinaryPractice->getPractice();

            if ($accountPractice->isSendNotifications() && !is_null($accountPractice->getNotificationEndpointId())) {
                /** @var NotificationEndpoint $notificationEndpoint */
                $notificationEndpoint = $this->notificationEndpointRepository->find($accountPractice->getNotificationEndpointId());

                if ($notificationEndpoint) {
                    $practice
                        ->setSendNotifications($accountPractice->isSendNotifications())
                        ->setNotificationEndpoint($notificationEndpoint);
                } else {
                    $practice
                        ->setSendNotifications(false)
                        ->setNotificationEndpoint(null);
                }
            } else {
                $practice
                    ->setSendNotifications(false)
                    ->setNotificationEndpoint(null);
            }

            $practice
                ->setName($accountPractice->getName())
                ->setDescription($accountPractice->getDescription())
                ->setStreet($accountPractice->getStreet())
                ->setCity($accountPractice->getCity())
                ->setZipCode($accountPractice->getZipCode())
                ->setCountry($accountPractice->getCountry())
                ->setTelephone($accountPractice->getTelephone())
            ;

            $this->sendToLims($veterinaryPractice);

            $this->entityManager->persist($veterinaryPractice);
            $this->entityManager->persist($practice);
            $this->entityManager->flush();

            return $this->redirectToRoute('zoolyx_practice_settings', array('id'=>$id));
        }

        return $this->render('ZoolyxAccountBundle:Account:practice.html.twig', array(
            'form' => $form->createView(),
            'practice' => $accountPractice,
            'veterinaryPracticeId' => $id,
            'veterinaryPractice' => $veterinaryPractice
        ));

    }
    
    public function modifyVeterinaryAction(Request $request, $id, $veterinaryPracticeId)
    {
    	/** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = $this->veterinaryPracticeRepository->find($veterinaryPracticeId);
        $veterinary = $veterinaryPractice->getVeterinary();
        $practice = $veterinaryPractice->getPractice();
        $accountVeterinary = $this->veterinaryMapper->map($veterinaryPractice);

        if ($veterinaryPractice->getPractice()->isSendNotifications()) {
            $form = $this->createForm(new VeterinaryWithNotificationsType($this->translator), $accountVeterinary);
        } else {
            $form = $this->createForm(new VeterinaryType($this->translator), $accountVeterinary);
        }

        if (!is_null($request)) {
            $form->handleRequest($request);
        };
    
        if ($form->isValid()) {
            if ($practice->isSendNotifications() && $veterinaryPractice->getNotificationId() != $accountVeterinary->getNotificationId()) {
                $doc = new DOMDocument("1.0","UTF-8");
                $subscriptionDoc = $doc->createElement('subscription');
                $doc->appendChild($subscriptionDoc);
                $daDoc = $doc->createElement('da');
                $subscriptionDoc->appendChild($daDoc);
                $daDoc->setAttribute('id',$veterinaryPractice->getLimsId());
                $daDoc->setAttribute('extid',$accountVeterinary->getNotificationId());

                $tmpDoc = $doc->createElement('lastName');
                $tmpDoc->nodeValue = str_replace("&","&amp;",$veterinary->getLastName());
                $daDoc->appendChild($tmpDoc);

                $tmpDoc = $doc->createElement('firstName');
                $tmpDoc->nodeValue = str_replace("&","&amp;",$veterinary->getFirstName());
                $daDoc->appendChild($tmpDoc);

                $tmpDoc = $doc->createElement('street');
                $tmpDoc->nodeValue = str_replace("&","&amp;",$practice->getStreet());
                $daDoc->appendChild($tmpDoc);

                $tmpDoc = $doc->createElement('zipCode');
                $tmpDoc->nodeValue = str_replace("&","&amp;",$practice->getZipCode());
                $daDoc->appendChild($tmpDoc);

                $tmpDoc = $doc->createElement('city');
                $tmpDoc->nodeValue = str_replace("&","&amp;",$practice->getCity());
                $daDoc->appendChild($tmpDoc);

                $tmpDoc = $doc->createElement('country');
                $tmpDoc->nodeValue = str_replace("&","&amp;",$practice->getCountry());
                $daDoc->appendChild($tmpDoc);

                $options = array(
                    'http' => array(
                        'header'  => "Content-type: application/xml; charset=utf-8",
                        'method'  => 'POST',
                        'content' => $doc->saveXML()
                    )
                );
                $context  = stream_context_create($options);
                $subscriptionUrl = $practice->getNotificationEndpoint()->getSubscriptionUrl();
                $subscriptionUrl = str_replace('{{identifier}}',$accountVeterinary->getNotificationId(),$subscriptionUrl);
                $response = @file_get_contents($subscriptionUrl, false, $context);
                $responseCode = null;
                foreach ($http_response_header as $header) {
                    if ( preg_match( '~^'."HTTP/1.(?'version'\d) (?'code'\d+) .*".'$~i', $header, $responseCode ) )
                    {
                        break;
                    }
                }

                if (isset($responseCode["code"]) && $responseCode["code"]=="200") {
                    $veterinaryPractice
                        ->setNotificationId($accountVeterinary->getNotificationId())
                    ;
                }
            }
            $veterinary
                ->setFirstName($accountVeterinary->getFirstName())
                ->setLastName($accountVeterinary->getLastName())
                ->setOrderNumber($accountVeterinary->getOrderNumber())
                ->setTelephone($accountVeterinary->getGsm());
            $veterinary->getAccount()
                ->setFirstName($accountVeterinary->getFirstName())
                ->setLastName($accountVeterinary->getLastName())
            ;

            $this->entityManager->persist($veterinary);
            $this->entityManager->persist($veterinary->getAccount());

            $this->entityManager->flush();

            $this->sendToLims($veterinaryPractice);

            return $this->redirectToRoute('zoolyx_veterinary_settings', array('id'=>$id, 'veterinaryPracticeId'=> $veterinaryPracticeId));
        }
    
        return $this->render('ZoolyxAccountBundle:Account:veterinary.html.twig', array(
            'form' => $form->createView(),
            'veterinaryPracticeId' => $id,
            'veterinaryPractice' => $veterinaryPractice,
            'user' => $veterinary->getAccount()
        ));
    }

    public function makeAdminAction(Request $request, $id, $veterinaryPracticeId, $confirmed=false)
    {
        /** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = $this->veterinaryPracticeRepository->findOneBy(array(
            'id' => $veterinaryPracticeId
        ));

        if (!$veterinaryPractice) {
            return $this->redirectToRoute('zoolyx_practice_settings', array('id'=>$id));
        }

        if (!$confirmed) {
            return $this->render('ZoolyxAccountBundle:Account/Confirm:confirm_make_admin_'.$request->getLocale().'.html.twig', array(
                'id' => $id,
                'veterinaryPractice' => $veterinaryPractice
            ));
        }

        if ($veterinaryPractice) {
            $veterinaryPractice->setRole(PracticeMember::PRACTICE_ROLE_ADMIN);
            $this->entityManager->persist($veterinaryPractice);
            $this->entityManager->flush();
        }
        return $this->redirectToRoute('zoolyx_practice_settings', array('id'=>$id));
    }
    public function removeAdminAction(Request $request, $id, $veterinaryPracticeId, $confirmed=false)
    {
        /** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = $this->veterinaryPracticeRepository->findOneBy(array(
            'id' => $veterinaryPracticeId
        ));

        if (!$veterinaryPractice) {
            return $this->redirectToRoute('zoolyx_practice_settings', array('id'=>$id));
        }

        if (!$confirmed) {
            return $this->render('ZoolyxAccountBundle:Account/Confirm:confirm_remove_admin_'.$request->getLocale().'.html.twig', array(
                'id' => $id,
                'veterinaryPractice' => $veterinaryPractice
            ));
        }

        if ($veterinaryPractice) {
            $veterinaryPractice->setRole(PracticeMember::PRACTICE_ROLE_MEMBER);
            $this->entityManager->persist($veterinaryPractice);
            $this->entityManager->flush();
        }
        return $this->redirectToRoute('zoolyx_practice_settings', array('id'=>$id));
    }

    public function transferAction(Request $request, $id, $veterinaryPracticeId, $confirmed=false)
    {
        /** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = $this->veterinaryPracticeRepository->find($veterinaryPracticeId);

        if (!$veterinaryPractice) {
            return $this->redirectToRoute('zoolyx_practice_settings', array('id'=>$id));
        }

        if (!$confirmed) {
            return $this->render('ZoolyxAccountBundle:Account/Confirm:confirm_transfer_'.$request->getLocale().'.html.twig', array(
                'id' => $id,
                'veterinaryPractice' => $veterinaryPractice
            ));
        }

        if ($veterinaryPractice) {
            $practice = new Practice();
            $practice
                ->setName($veterinaryPractice->getVeterinary()->getFirstName() . " " . $veterinaryPractice->getVeterinary()->getLastName())
                ->setDescription('')
                ->setStreet('')
                ->setZipCode('')
                ->setCity('')
                ->setCountry('')
            ;
            $veterinaryPractice
                ->setPractice($practice)
                ->setRole(PracticeMember::PRACTICE_ROLE_ADMIN)
            ;
            $this->entityManager->persist($practice);
            $this->entityManager->persist($veterinaryPractice);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('zoolyx_practice_settings', array('id'=>$id));
    }
    public function removeAction(Request $request, $id, $veterinaryPracticeId, $confirmed=false) {
        /** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = $this->veterinaryPracticeRepository->find($veterinaryPracticeId);

        if (!$veterinaryPractice) {
            return $this->redirectToRoute('zoolyx_practice_settings', array('id'=>$id));
        }

        if (!$confirmed) {
            return $this->render('ZoolyxAccountBundle:Account/Confirm:confirm_remove_'.$request->getLocale().'.html.twig', array(
                'id' => $id,
                'veterinaryPractice' => $veterinaryPractice
            ));
        }

        if ($veterinaryPractice) {

            $dummyVeterinaryPractice = new VeterinaryPractice();
            $dummyVeterinaryPractice
                ->setPractice($veterinaryPractice->getPractice())
                ->setVeterinary($veterinaryPractice->getVeterinary())
                ->setRole(PracticeMember::PRACTICE_ROLE_REMOVED)
                ->setVat($veterinaryPractice->getVat())
                ->setLimsId('R'.$veterinaryPractice->getLimsId())
            ;

            $practice = new Practice();
            $practice
                ->setName($veterinaryPractice->getVeterinary()->getFirstName() . " " . $veterinaryPractice->getVeterinary()->getLastName())
                ->setDescription('')
                ->setStreet('')
                ->setZipCode('')
                ->setCity('')
                ->setCountry('')
            ;
            $veterinaryPractice
                ->setPractice($practice)
                ->setRole(PracticeMember::PRACTICE_ROLE_ADMIN)
            ;

            // copy the reports
            /** @var VeterinaryPracticeReport $vpr */
            foreach ($veterinaryPractice->getVeterinaryPracticeReports() as $vpr) {
                $dummyVpr = new VeterinaryPracticeReport();
                $dummyVpr
                    ->setVeterinaryPractice($dummyVeterinaryPractice)
                    ->setReport($vpr->getReport())
                    ->setRole(VeterinaryPracticeReportRoles::VPR_SHARED);
                $this->entityManager->persist($dummyVpr);
            }

            $this->entityManager->persist($veterinaryPractice);
            $this->entityManager->persist($dummyVeterinaryPractice);
            $this->entityManager->flush();
        }
        return $this->redirectToRoute('zoolyx_practice_settings', array('id'=>$id));
    }

    public function generateTransferCodeAction($id)
    {
        /** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = $this->veterinaryPracticeRepository->findOneBy(array(
            'id' => $id
        ));
        if ($veterinaryPractice) {
            $hash = $this->hashProvider->getHash(20);
            $validTill = new \DateTime();
            $validTill->modify('+1 week');
            $veterinaryPractice
                ->setTransferCode($hash)
                ->setValidTill($validTill)
            ;
            $this->entityManager->persist($veterinaryPractice);
            $this->entityManager->flush();
        }
        return $this->redirectToRoute('zoolyx_practice_settings', array('id'=>$id));
    }

    public function addVeterinaryAction($id, Request $request, $confirmed=false) {
        $transferCode = $request->request->get('transfer_code', null);
        //die($transferCode);
        /** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = $transferCode ? $this->veterinaryPracticeRepository->findOneBy(array('transferCode'=> $transferCode)) : null;

        if (is_null($veterinaryPractice) ) {
            return $this->redirectToRoute('zoolyx_practice_settings', array('id'=>$id));
        }

        if (
            is_null($veterinaryPractice->getValidTill()) ||
            ( !is_null($veterinaryPractice->getValidTill()) && new \DateTime() > $veterinaryPractice->getValidTill() )
        ) {
            $veterinaryPractice
                ->setTransferCode(null)
                ->setValidTill(null);
            $this->entityManager->persist($veterinaryPractice);
            $this->entityManager->flush();

            return $this->render('ZoolyxAccountBundle:Account/Confirm:confirm_add_expired_'.$request->getLocale().'.html.twig', array(
                'id' => $id,
                'veterinaryPractice' => $veterinaryPractice
            ));
        }

        if (!$veterinaryPractice) {
            return $this->redirectToRoute('zoolyx_practice_settings', array('id'=>$id));
        }

        if (!$confirmed) {
            return $this->render('ZoolyxAccountBundle:Account/Confirm:confirm_add_'.$request->getLocale().'.html.twig', array(
                'id' => $id,
                'veterinaryPractice' => $veterinaryPractice
            ));
        }

        if ($veterinaryPractice) {
            /** @var VeterinaryPractice $thisVeterinaryPractice */
            $thisVeterinaryPractice = $this->veterinaryPracticeRepository->find($id);
            $veterinaryPractice
                ->setRole(0)
                ->setPractice($thisVeterinaryPractice->getPractice())
                ->setTransferCode(null)
                ->setValidTill(null)
            ;
            $this->entityManager->persist($veterinaryPractice);
            $this->entityManager->flush();
        }
        return $this->redirectToRoute('zoolyx_practice_settings', array('id'=>$id));
    }

    private function encodeToIso($string) {
        return mb_convert_encoding($string, "ISO-8859-1", mb_detect_encoding($string, "UTF-8, ISO-8859-1, ISO-8859-15", true));
    }

    private function sendToLims(VeterinaryPractice $veterinaryPractice) {
        $veterinaryXml = new VeterinaryXml();
        $veterinaryXml
            ->setLimsId($veterinaryPractice->getLimsId())
            ->setPracticeId($veterinaryPractice->getPractice()->getPracticeId())
            ->setPracticeName($veterinaryPractice->getPractice()->getName())
            ->setLastName($veterinaryPractice->getVeterinary()->getLastName())
            ->setFirstName($veterinaryPractice->getVeterinary()->getFirstName())
            ->setStreet($veterinaryPractice->getPractice()->getStreet())
            ->setZipCode($veterinaryPractice->getPractice()->getZipCode())
            ->setCity($veterinaryPractice->getPractice()->getCity())
            ->setCountry($veterinaryPractice->getPractice()->getCountry())
            ->setLanguage($veterinaryPractice->getVeterinary()->getAccount()->getLanguage())
            ->setEmail($veterinaryPractice->getVeterinary()->getAccount()->getEmail())
            ->setTel($veterinaryPractice->getPractice()->getTelephone())
            ->setGsm($veterinaryPractice->getVeterinary()->getTelephone())
            ->setRegId($veterinaryPractice->getVeterinary()->getOrderNumber())
            ->setFa($veterinaryPractice->getVat() ? $veterinaryPractice->getVat()->getFaId() : '')
            ->setVatId($veterinaryPractice->getVat() ? $veterinaryPractice->getVat()->getVat() : '')
        ;

        $tempFile = fopen('php://memory', 'r+');
        fputs($tempFile, $this->encodeToIso($veterinaryXml->getXml()));
        rewind($tempFile);
        $this->ftpService->put($veterinaryPractice->getId() . '_'.time().'.xml', $tempFile);
    }

}
