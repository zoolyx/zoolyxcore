<?php

namespace Zoolyx\AccountBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Zoolyx\AccountBundle\Entity\Repository\UserRegistrationRequestRepository;
use Zoolyx\AccountBundle\Entity\UserRegistrationRequest;
use Zoolyx\AccountBundle\Forms\Account\PracticeMember;
use Zoolyx\CoreBundle\Entity\Practice;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\NotificationBundle\Model\Mail\MailMessage;
use Zoolyx\TicketBundle\Entity\MessageEvent;
use Zoolyx\TicketBundle\Entity\RegistrationEvent;
use Zoolyx\TicketBundle\Entity\Repository\RegistrationEventRepository;
use Zoolyx\TicketBundle\Model\Ticket\TicketContentManager;

class RequestController extends Controller
{
    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var UserRegistrationRequestRepository
     *
     * @DI\Inject("zoolyx_account.repository.user_registration_request")
     */
    private $userRegistrationRequestRepository;

    /**
     * @var RegistrationEventRepository
     *
     * @DI\Inject("zoolyx_ticket.repository.registration_event")
     */
    private $registrationEventRepository;

    /**
     * @var VeterinaryRepository
     *
     * @DI\Inject("zoolyx_core.repository.veterinary")
     */
    private $veterinaryRepository;

    /**
     * @var TicketContentManager
     *
     * @DI\Inject("zoolyx_ticket.manager.ticket_content")
     */
    private $ticketContentManager;

    /**
     * @var Swift_Mailer
     *
     * @DI\Inject("mailer")
     */
    private $mailer;



    public function indexAction($id) {
        $userRegistrationRequest = $this->userRegistrationRequestRepository->find($id);

        if (is_null($userRegistrationRequest)) {
            die('cannot find this request');
        }

        return $this->render('ZoolyxAccountBundle:Request:index.html.twig', array(
            'request' => $userRegistrationRequest
        ));
    }


    public function approveAction(Request $request, $id) {

        $limsId = $request->get('lims_id');

        /** @var UserRegistrationRequest $userRegistrationRequest */
        $userRegistrationRequest = $this->userRegistrationRequestRepository->find($id);

        if (is_null($userRegistrationRequest)) {
            die('cannot find this request');
        }

        $data = $userRegistrationRequest->getInfo();
        $orderNumber = $data["OrderNr"];

        $account = $userRegistrationRequest->getAccount();
        $account->addRole('ROLE_VETERINARY');
        $this->entityManager->persist($account);

        $veterinary = $this->veterinaryRepository->findOneBy(array(
            'account' => $account
        ));

        if (is_null($veterinary)) {
            $veterinary = new Veterinary();
            $veterinary
                ->setAccount($account)
                ->setFirstName($account->getFirstName())
                ->setLastName($account->getLastName())
                ->setOrderNumber($orderNumber)
            ;
            $this->entityManager->persist($veterinary);
        }

        if ($limsId && $limsId!='') {
            $found = false;
            /** @var VeterinaryPractice $veterinaryPractice */
            foreach ($veterinary->getVeterinaryPractices() as $veterinaryPractice) {
                if ($veterinaryPractice->getLimsId() == $limsId) {
                    $found = true;
                }
            }
            if (!$found) {
                $practice = new Practice();
                $practice
                    ->setName('MyPractice')
                    ->setDescription('')
                    ->setStreet($account->getStreet())
                    ->setZipCode($account->getZipCode())
                    ->setCity($account->getCity())
                    ->setCountry($account->getCountry())
                ;
                $this->entityManager->persist($practice);

                $veterinaryPractice = new VeterinaryPractice();
                $veterinaryPractice
                    ->setVeterinary($veterinary)
                    ->setLimsId($limsId)
                    ->setPractice($practice)
                    ->setRole(PracticeMember::PRACTICE_ROLE_ADMIN)
                ;
                $this->entityManager->persist($veterinaryPractice);
            }
        }

        $userRegistrationRequest
            ->setState(UserRegistrationRequest::STATE_APPROVED)
            ->setApprovedOn(new \DateTime());
        $this->entityManager->persist($userRegistrationRequest);

        /** @var RegistrationEvent $registrationEvent */
        $registrationEvent = $this->registrationEventRepository->findForUserRegistrationRequest($userRegistrationRequest);
        $ticket = $registrationEvent->getTicket();

        if (is_null($ticket)) {
            die('cannot find ticket');
        }

        $messageEvent = new MessageEvent();
        $messageEvent->setContent('This request has been approved')->setInternal(true);
        $this->ticketContentManager->addMessage($ticket,$messageEvent, $this->getUser());
        $this->entityManager->persist($ticket);
        $this->entityManager->flush();

        $homeUrl = $this->getParameter('server_base_url') . ($account->getLanguage()=='fr' ? '/fr' : '');
        $htmlContent = $this->renderView('ZoolyxTemplateBundle:Mail/Registration:confirm_vet_'.$account->getLanguage().'.html.twig', array(
            'home' => $homeUrl
        ));
        $plainContent = $this->renderView('ZoolyxTemplateBundle:Mail/Registration:confirm_vet_'.$account->getLanguage().'.plain.twig', array(
            'home' => $homeUrl
        ));

        $mailMessage = new MailMessage($this->getParameter('stub_to'));
        $mailMessage
            ->setSubject("Email verification")
            ->setFrom($this->getParameter('from'))
            ->setTo($account->getEmail())
            ->setHtml($htmlContent,'text/html')
            ->setPlain($plainContent ,'text/plain')
        ;
        $this->mailer->send($mailMessage->getSwiftMessage());

        return $this->redirectToRoute('zoolyx_ticket_ticket', array(
            'id' => $ticket->getId()
        ));
    }

    public function declineAction($id) {
        /** @var UserRegistrationRequest $userRegistrationRequest */
        $userRegistrationRequest = $this->userRegistrationRequestRepository->find($id);

        if (is_null($userRegistrationRequest)) {
            die('cannot find this request');
        }

        $userRegistrationRequest->setState(UserRegistrationRequest::STATE_DECLINED);
        $this->entityManager->persist($userRegistrationRequest);

        /** @var RegistrationEvent $registrationEvent */
        $registrationEvent = $this->registrationEventRepository->findForUserRegistrationRequest($userRegistrationRequest);

        $ticket = $registrationEvent->getTicket();

        if (is_null($ticket)) {
            die('cannot find ticket');
        }

        $messageEvent = new MessageEvent();
        $messageEvent->setContent('This request has been declined')->setInternal(true);
        $this->ticketContentManager->addMessage($ticket,$messageEvent, $this->getUser());
        $this->entityManager->persist($ticket);

        $this->entityManager->flush();

        return $this->redirectToRoute('zoolyx_ticket_ticket', array(
            'id' => $ticket->getId()
        ));
    }


}
