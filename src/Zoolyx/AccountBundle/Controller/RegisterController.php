<?php

namespace Zoolyx\AccountBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Zoolyx\AccountBundle\Entity\UserRegistrationRequest;
use Zoolyx\AccountBundle\Forms\Registration;
use Zoolyx\AccountBundle\Forms\RegistrationType;
use Zoolyx\AccountBundle\Forms\UserRegistrationMapper;
use Zoolyx\AccountBundle\Forms\UserRegistrationType;
use Zoolyx\AccountBundle\Model\Token\TokenGenerator;
use Zoolyx\AccountBundle\Model\UserRegistrationRequest\Info;
use Zoolyx\CoreBundle\Entity\Repository\UserRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Model\Language\LanguageManager;
use Zoolyx\NotificationBundle\Model\Mail\MailMessage;
use Zoolyx\TicketBundle\Entity\Ticket;
use Zoolyx\TicketBundle\Model\Ticket\TicketContentManager;
use Zoolyx\TicketBundle\Model\Ticket\TicketFactory;

class RegisterController extends Controller
{
    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var UserRepository
     *
     * @DI\Inject("zoolyx_core.repository.user")
     */
    private $userRepository;

    /**
     * @var Registration
     *
     * @DI\Inject("zoolyx_account.model.registration")
     */
    private $registration;

    /**
     * @var Swift_Mailer
     *
     * @DI\Inject("mailer")
     */
    private $mailer;

    /**
     * @var UserRegistrationMapper
     *
     * @DI\Inject("zoolyx_account.user_registration_mapper")
     */
    private $userRegistrationMapper;

    /**
     * @var Translator
     *
     * @DI\Inject("translator")
     */
    private $translator;

    /**
     * @var TicketFactory
     *
     * @DI\Inject("zoolyx_ticket.factory.ticket")
     */
    private $ticketFactory;

    /**
     * @var TicketContentManager
     *
     * @DI\Inject("zoolyx_ticket.manager.ticket_content")
     */
    private $ticketContentManager;

    /**
     * @var Info
     *
     * @DI\Inject("zoolyx_account.user_registration_request.info")
     */
    private $info;

    /**
     * @var LanguageManager
     *
     * @DI\Inject("zoolyx_core.language.manager")
     */
    private $languageManager;

    /**
     * @var TokenGenerator
     *
     * @DI\Inject("zoolyx_account.generator.token")
     */
    private $tokenGenerator;


    public function indexAction(Request $request)
    {
        $this->registration->setLanguage($request->getLocale());

        $form = $this->createForm(new RegistrationType($this->translator), $this->registration);
        if (!is_null($request)) {
            $form->handleRequest($request);
        };

        if ($form->isValid()) {
            $email = $this->registration->getEmail();
            $user = $this->userRepository->findOneBy(array(
                'email' => $email
            ));
            if ($user) {
                //this should not happen because of validation
                die("user already exists");
            } else {
                $confirmationToken = $this->tokenGenerator->get(32);
                $user = new User();
                $user->setUsername($email)
                    ->setUsernameCanonical($email)
                    ->setEmail($email)
                    ->setEmailCanonical($email)
                    ->setFirstName($this->registration->getFirstName())
                    ->setLastName($this->registration->getLastName())
                    ->setLanguage($this->registration->getLanguage())
                    ->setEnabled(false)
                    ->setPlainPassword('temp')
                    ->setConfirmationToken($confirmationToken)
                ;
                $this->entityManager->persist($user);
                $this->entityManager->flush();

                $language = $this->languageManager->validate($user->getLanguage(), $request);
                $confirmationUrl = $this->generateUrl('zoolyx_account_confirmation',array('token'=>$confirmationToken), UrlGeneratorInterface::ABSOLUTE_URL);

                $htmlContent = $this->renderView('ZoolyxTemplateBundle:Mail/Registration:confirm_email_address_'.$language.'.html.twig', array(
                    'confirmationUrl' => $confirmationUrl
                ));
                $plainContent = $this->renderView('ZoolyxTemplateBundle:Mail/Registration:confirm_email_address_'.$language.'.plain.twig', array(
                    'confirmationUrl' => $confirmationUrl
                ));

                $mailMessage = new MailMessage($this->getParameter('stub_to'));
                $mailMessage
                    ->setSubject("Email verification")
                    ->setFrom($this->getParameter('from'))
                    ->setTo($email)
                    ->setHtml($htmlContent,'text/html')
                    ->setPlain($plainContent ,'text/plain')
                ;
                $this->mailer->send($mailMessage->getSwiftMessage());

                return $this->render('ZoolyxAccountBundle:Default:mail_sent.html.twig', array(
                    'email' => $email
                ));
            }
        }

        $languagePrefix = $request->getLocale() == 'fr' ? '/fr' : '';

        return $this->render('ZoolyxAccountBundle:Default:register.html.twig', array(
            'form' => $form->createView(),
            'url_conditions' => $this->getParameter('server_base_url') . $languagePrefix . '/disclaimer/',
            'url_privacy' => $this->getParameter('server_base_url') . $languagePrefix . '/privacy-clausule/'
        ));
    }

    public function confirmAction(Request $request, $token) {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(array(
           'confirmationToken' => $token
        ));
        if (is_null($user)) {
            return $this->render('ZoolyxAccountBundle:Default:confirmation_fail.html.twig', array());
        }

        $userRegistration = $this->userRegistrationMapper->map($user);
        $this->translator->setLocale($user->getLanguage());
        $form = $this->createForm(new UserRegistrationType($this->translator), $userRegistration);
        if (!is_null($request)) {
            $form->handleRequest($request);
        };

        if ($form->isValid()) {
            /** @var array $formValue */
            $formValue = $userRegistration->isVeterinary();
            $userRegistration->setIsVeterinary(count($formValue) ? true : false);

            $user
                ->setFirstName($userRegistration->getFirstName())
                ->setLastName($userRegistration->getLastName())
                ->setLanguage($userRegistration->getLanguage())
                ->setStreet($userRegistration->getStreet())
                ->setZipCode($userRegistration->getZipCode())
                ->setCity($userRegistration->getCity())
                ->setCountry($userRegistration->getCountry())
                ->setTelephone($userRegistration->getTelephone())
                ->setPlainPassword($userRegistration->getPassword())
                ->setConfirmationToken(null)
                ->setEnabled(true)
                ->addRole('ROLE_OWNER')
            ;

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            if ($userRegistration->isVeterinary()) {
                //create a ticket to approve Veterinary
                $userRegistrationRequest = new UserRegistrationRequest();
                $userRegistrationRequest
                    ->setAccount($user)
                    ->setRequestedByUser(true)
                    ->setState(UserRegistrationRequest::STATE_OPEN)
                    ->setCreatedOn(new \DateTime())
                    ->setInfo($this->info->addInfo(Info::NAME_ORDER_NUMBER,$userRegistration->getOrderNumber())->getInfo());
                ;

                /** @var Ticket $ticket */
                $ticket = $this->ticketFactory->create($user);
                $this->ticketContentManager->addUserRegistration($ticket,$userRegistrationRequest, $user);

                $this->entityManager->persist($ticket);
                $this->entityManager->flush();
            }

            return $this->render('ZoolyxAccountBundle:Default:registration_success.html.twig', array(
                'user' => $userRegistration
            ));
        }
        return $this->render('ZoolyxAccountBundle:Default:register_user.html.twig', array(
            'form' => $form->createView(),
            'user' => $userRegistration
        ));
    }

}
