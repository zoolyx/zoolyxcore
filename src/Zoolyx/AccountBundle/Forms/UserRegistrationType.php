<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 9/7/14
 * Time: 4:34 PM
 */

namespace Zoolyx\AccountBundle\Forms;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class UserRegistrationType extends AbstractType
{
    /** @var Translator */
    private $translator;

    public function __construct($translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', 'text', array(
                'label' => $this->translator->trans('form.registration.first_name'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.registration.first_name.placeholder'),
                )
            ))
            ->add('lastName', 'text', array(
                'label' => $this->translator->trans('form.registration.last_name'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.registration.last_name.placeholder'),
                )
            ))
            ->add('language', 'choice', array(
                'label' => $this->translator->trans('form.registration.language'),
                'choices' => array('Nederlands'=> 'nl', 'Français' => 'fr'),
                'choices_as_values' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('street', 'text', array(
                'label' => $this->translator->trans('form.registration.street'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.registration.street.placeholder'),
                )
            ))
            ->add('zipCode', 'text', array(
                'label' => $this->translator->trans('form.registration.zipcode'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.registration.zipcode.placeholder'),
                )
            ))
            ->add('city', 'text', array(
                'label' => $this->translator->trans('form.registration.city'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.registration.city.placeholder'),
                )
            ))
            ->add('country', 'text', array(
                'label' => $this->translator->trans('form.registration.country'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.registration.country.placeholder'),
                )
            ))
            ->add('telephone', 'text', array(
                'label' => $this->translator->trans('form.registration.telephone'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.registration.telephone.placeholder'),
                )
            ))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'The password fields must match.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options'  => array('label' => $this->translator->trans('form.registration.password')),
                'second_options' => array('label' => $this->translator->trans('form.registration.password_repeat')),
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('isVeterinary', 'choice', array(
                'label' => 'Language',
                'choices' => array('form.registration.iam_a_veterinary'=> 'true'),
                'choices_as_values' => true,
                'expanded' => true,
                'multiple' => true,
                'choice_attr' => function($choiceValue, $key, $value) {
                    return ['style' => 'margin-top: -2px; margin-right: 10px'];
                }
            ))
            ->add('orderNumber', 'text', array(
                'label' => $this->translator->trans('form.registration.order_number'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.registration.order_number.placeholder'),
                ),
                'required' => false
            ))

        ;


    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zoolyx\AccountBundle\Forms\UserRegistration',
        ));
    }

    public function getName()
    {
        return 'UserRegistration';
    }
}
