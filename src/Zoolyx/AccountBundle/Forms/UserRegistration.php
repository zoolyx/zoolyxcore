<?php

namespace Zoolyx\AccountBundle\Forms;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;


class UserRegistration
{

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        if ($this->isVeterinary && $this->orderNumber == '') {
            $context->buildViolation('userRegistration.order_number.required')
                ->atPath('orderNumber')
                ->addViolation();
        }
    }

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $zipCode;

    /**
     * @Assert\NotBlank()
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $country = "B";

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[\+|0][0-9]+$/", message="userRegistration.telephone.number_only")
     * @Assert\Length(min = 8, max = 20, minMessage = "userRegistration.telephone.min_length", maxMessage = "max_length")
     * @var string
     */
    private $telephone = '';

    /**
     * @var string
     */
    private $language = "nl";

    /**
     * @var string
     */
    private $password;

    /**
     * @var bool
     */
    private $isVeterinary;

    /**
     * @var string
     */
    private $orderNumber;



    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }


    /**
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return UserRegistration
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return UserRegistration
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet() {
        return $this->street;
    }

    /**
     * @param string $street
     * @return UserRegistration
     */
    public function setStreet($street) {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode() {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     * @return UserRegistration
     */
    public function setZipCode($zipCode)  {
        $this->zipCode = $zipCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * @param string $city
     * @return UserRegistration
     */
    public function setCity($city) {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * @param string $country
     * @return UserRegistration
     */
    public function setCountry($country) {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     * @return $this
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * @param string $language
     * @return UserRegistration
     */
    public function setLanguage($language) {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isVeterinary()
    {
        return $this->isVeterinary;
    }

    /**
     * @param boolean $isVeterinary
     * @return $this
     */
    public function setIsVeterinary($isVeterinary)
    {
        $this->isVeterinary = $isVeterinary;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return $this
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }


}
