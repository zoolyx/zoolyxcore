<?php

namespace Zoolyx\AccountBundle\Forms;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Zoolyx\CoreBundle\Entity\Repository\UserRepository;

/**
 * @DI\Service("zoolyx_account.model.registration")
 */
class Registration
{
    /** @var UserRepository */
    private $userRepository;

    /**
     * Constructor.
     *
     * @param UserRepository $userRepository
     *
     * @DI\InjectParams({
     *  "userRepository" = @DI\Inject("zoolyx_core.repository.user")
     * })
     */
    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        $user = $this->userRepository->findOneBy(array(
            'email' => $this->getEmail()
        ));

        if ($user) {
            $context->buildViolation('userRegistration.email.already_used')
                ->atPath('email')
                ->addViolation();
        }
    }

    /**
     * @Assert\Length(min = 2, max = 128, minMessage = "userRegistration.min_length.2", maxMessage = "userRegistration.max_length.128")
     * @var string
     */
    private $firstName;

    /**
     * @Assert\Length(min = 1, max = 128, minMessage = "userRegistration.min_length.1", maxMessage = "userRegistration.max_length.128")
     * @var string
     */
    private $lastName;

    /**
     * @Assert\Email(message = "userRegistration.email.invalid")
     * @var string
     */
    private $email;

    /** @var string */
    private $language = "nl";

    /** @var boolean */
    private $agreeWithConditions;

    /** @var boolean */
    private $acceptMails;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }


    /**
     * @return string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * @param string $language
     * @return UserRegistration
     */
    public function setLanguage($language) {
        $this->language = $language;
        return $this;
    }

    /**
     * @return boolean
     */
    public function agreeWithConditions()
    {
        return $this->agreeWithConditions;
    }

    /**
     * @param boolean $agreeWithConditions
     * @return $this
     */
    public function setAgreeWithConditions($agreeWithConditions)
    {
        $this->agreeWithConditions = $agreeWithConditions;
        return $this;
    }

    /**
     * @return boolean
     */
    public function acceptMails()
    {
        return $this->acceptMails;
    }

    /**
     * @param boolean $acceptMails
     * @return $this
     */
    public function setAcceptMails($acceptMails)
    {
        $this->acceptMails = $acceptMails;
        return $this;
    }


}