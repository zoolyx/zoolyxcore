<?php

namespace Zoolyx\AccountBundle\Forms;

use Symfony\Component\Validator\Constraints as Assert;

class PasswordReset
{
    /**
     * @Assert\Email(message = "userRegistration.email.invalid", checkMX = true)
     * @var string
     */
    private $email;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }


}