<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 9/7/14
 * Time: 4:34 PM
 */

namespace Zoolyx\AccountBundle\Forms\Account;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Zoolyx\CoreBundle\Entity\NotificationEndpoint;


class VeterinaryWithNotificationsType extends VeterinaryType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('notificationId', 'text', array('required' => false))
        ;
    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zoolyx\AccountBundle\Forms\Account\Veterinary',
        ));
    }

    public function getName()
    {
        return 'Veterinary';
    }
}
