<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 9/7/14
 * Time: 4:34 PM
 */

namespace Zoolyx\AccountBundle\Forms\Account;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class PasswordNewType extends AbstractType
{
    /** @var Translator */
    private $translator;

    public function __construct($translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldPassword', 'password', array(
                'invalid_message' => 'This field is required.',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('newPassword', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'The password fields must match.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options'  => array('label' => $this->translator->trans('form.registration.password')),
                'second_options' => array('label' => $this->translator->trans('form.registration.password_repeat')),
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
        ;


    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zoolyx\AccountBundle\Forms\Account\PasswordNew',
        ));
    }

    public function getName()
    {
        return 'PasswordNew';
    }
}
