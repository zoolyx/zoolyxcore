<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 9/7/14
 * Time: 4:34 PM
 */

namespace Zoolyx\AccountBundle\Forms\Account;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;


class AccountType extends AbstractType
{
    /** @var Account */
    private $account = null;
    /** @var Translator */
    private $translator;

    public function __construct($account, $translator) {
        $this->account = $account;
        $this->translator = $translator;
    }



    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAttribute('class','form-horizontal zlx_form')
            ->add('language' , 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices'  => array(
                    'Nederlands' => Account::LANGUAGE_NL,
                    'Français' => Account::LANGUAGE_FR,
                ),
                'choices_as_values' => true,
            ))
            ->add('reportFrequency' , 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices'  => array(
                    $this->translator->trans('settings.every_version') => Account::FREQUENCY_EVERY_VERSION,
                    $this->translator->trans('settings.first_and_completed') => Account::FREQUENCY_FIRST_AND_COMPLETED,
                    $this->translator->trans('settings.only_completed') => Account::FREQUENCY_ONLY_COMPLETED,
                    $this->translator->trans('settings.never') => Account::FREQUENCY_NEVER
                ),
                'choices_as_values' => true,
            ))

        ;
        if ($this->account->isVeterinary() || $this->account->isOrganisation()) {
            $builder
                ->add('autoShareOwners', 'Symfony\Component\Form\Extension\Core\Type\CheckboxType', array('required' => false))
            ;
        }
    }



    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zoolyx\AccountBundle\Forms\Account\Account',
        ));
    }

    public function getName()
    {
        return 'Account';
    }
}
