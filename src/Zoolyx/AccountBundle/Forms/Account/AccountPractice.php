<?php

namespace Zoolyx\AccountBundle\Forms\Account;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Zoolyx\CoreBundle\Model\Address\Validation\FormAddressValidator;


class AccountPractice
{
    const COUNTRY_BE = 'BE';
    const COUNTRY_FR = 'FR';
    const COUNTRY_LU = 'LU';
    const COUNTRY_NL = 'NL';
    
    /** @var int */
    private $practiceId;
    /** @var string */
    private $name;
    /** @var string */
    private $description;
    /** @var string */
    private $street;
    /** @var string */
    private $city;
    /** @var string */
    private $zipCode;
    /** @var string */
    private $country;
    /** @var string */
    private $telephone = '';
    /** @var string */
    private $vat = '';
    /** @var int */
    private $invoiceDefault;

    /** @var bool */
    public $sendNotifications = false;
    /** @var int */
    public $notificationEndpointId = 0;

    /** @var array */
    private $members = array();


    /** @var FormAddressValidator $formAddressValidator */
    private $formAddressValidator;

    
    /**
     * @return int
     */
    public function getPracticeId()
    {
        return $this->practiceId;
    }

    /**
     * @param int $practiceId
     * @return $this
     */
    public function setPracticeId($practiceId)
    {
        $this->practiceId = $practiceId;
        return $this;
    }


    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return $this
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param $zipCode
     * @return $this
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     * @return $this;
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * @return string
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param string $vat
     * @return $this
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }

    /**
     * @return int
     */
    public function getInvoiceDefault()
    {
        return $this->invoiceDefault;
    }

    /**
     * @param int $invoiceDefault
     * @return $this
     */
    public function setInvoiceDefault($invoiceDefault)
    {
        $this->invoiceDefault = $invoiceDefault;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isSendNotifications()
    {
    return $this->sendNotifications;
    }

    /**
     * @param boolean $sendNotifications
     * @return $this
     */
    public function setSendNotifications($sendNotifications)
    {
    $this->sendNotifications = $sendNotifications;
    return $this;
    }

    /**
     * @return int
     */
    public function getNotificationEndpointId()
    {
    return $this->notificationEndpointId;
    }

    /**
     * @param int $notificationEndpointId
     * @return $this
     */
    public function setNotificationEndpointId($notificationEndpointId)
    {
    $this->notificationEndpointId = $notificationEndpointId;
    return $this;
    }

    /**
     * @return array
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @param PracticeMember $member
     * @return $this
     */
    public function addMember($member)
    {
        $this->members[] = $member;
        return $this;
    }

    /**
     * @return FormAddressValidator
     */
    public function getFormAddressValidator()
    {
        return $this->formAddressValidator;
    }

    /**
     * @param FormAddressValidator $formAddressValidator
     * @return $this
     */
    public function setFormAddressValidator($formAddressValidator)
    {
        $this->formAddressValidator = $formAddressValidator;
        return $this;
    }


    

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {    
		//controleer of het adres geldig is
		if ($this->getCountry() == "BE") {

            $this->formAddressValidator->validate(
                $this->getStreet(),
                $this->getZipCode(),
                $this->getCity(),
                $context,
                array(
                    FormAddressValidator::FORM_NAME_STREET => "street",
                    FormAddressValidator::FORM_NAME_ZIPCODE => "zipCode",
                    FormAddressValidator::FORM_NAME_CITY => "city",
                )
            );
            if ($this->formAddressValidator->getStreet()) {
                $this->setStreet($this->formAddressValidator->getStreet());
            }
            if ($this->formAddressValidator->getZipCode()) {
                $this->setZipCode($this->formAddressValidator->getZipCode());
            }
            if ($this->formAddressValidator->getCity()) {
                $this->setCity($this->formAddressValidator->getCity());
            }
		}
	}

}

