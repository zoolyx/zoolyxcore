<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 26.09.18
 * Time: 18:57
 */

namespace Zoolyx\AccountBundle\Forms\Account;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Practice;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryPracticeRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Model\Address\Validation\FormAddressValidator;

/**
 * @DI\Service("zoolyx_account.mapper.practice")
 */
class PracticeMapper {

    /** @var VeterinaryRepository */
    private $veterinaryRepository;

    /** @var VeterinaryPracticeRepository */
    private $veterinaryPracticeRepository;

    /** @var FormAddressValidator $formAddressValidator */
    private $formAddressValidator;


    /**
     * Constructor.
     *
     * @param VeterinaryRepository $veterinaryRepository
     * @param VeterinaryPracticeRepository $veterinaryPracticeRepository
     * @param FormAddressValidator $formAddressValidator
     *
     * @DI\InjectParams({
     *  "veterinaryRepository" = @DI\Inject("zoolyx_core.repository.veterinary"),
     *  "veterinaryPracticeRepository" = @DI\Inject("zoolyx_core.repository.veterinary_practice"),
     *  "formAddressValidator" = @DI\Inject("zoolyx_core.address.form_validator")
     * })
     */
    public function __construct(VeterinaryRepository $veterinaryRepository, VeterinaryPracticeRepository $veterinaryPracticeRepository, FormAddressValidator $formAddressValidator) {
        $this->veterinaryRepository = $veterinaryRepository;
        $this->veterinaryPracticeRepository = $veterinaryPracticeRepository;
        $this->formAddressValidator = $formAddressValidator;
    }

    public function map(User $user, $id) {
        /** @var VeterinaryPractice $veterinaryPractice */
        $veterinaryPractice = $this->veterinaryPracticeRepository->find($id);
        /** @var Practice $practice */
        $practice = $veterinaryPractice->getPractice();

        $accountPractice = new AccountPractice();

        $accountPractice
            ->setFormAddressValidator($this->formAddressValidator)
            ->setPracticeId($practice->getId())
            ->setName($practice->getName())
            ->setStreet($practice->getStreet())
            ->setCity($practice->getCity())
            ->setZipCode($practice->getZipCode())
            ->setCountry($practice->getCountry())
            ->setTelephone($practice->getTelephone())
            ->setDescription($practice->getDescription())
            ->setVat(is_null($veterinaryPractice->getVat()) ? '' : $veterinaryPractice->getVat()->getVat())
            ->setInvoiceDefault($veterinaryPractice->getInvoiceDefault())
            ->setSendNotifications($practice->isSendNotifications())
            ->setNotificationEndpointId($practice->getNotificationEndpoint()? $practice->getNotificationEndpoint()->getId():0)
        ;
        /** @var VeterinaryPractice $vp */
        foreach ($practice->getVeterinaryPractices() as $vp) {
            if ($vp->getRole() == PracticeMember::PRACTICE_ROLE_REMOVED) continue;

            $member = new PracticeMember();
            $member
                ->setVeterinaryPracticeId($vp->getId())
                ->setFirstName($vp->getVeterinary()->getFirstName())
                ->setLastName($vp->getVeterinary()->getLastName())
                ->setEmail($vp->getVeterinary()->getAccount()->getEmail())
                ->setRole($vp->getRole())
                ->setIsMe($user->getId() == $vp->getVeterinary()->getAccount()->getId())
            ;
            $accountPractice->addMember($member);
        }


        return $accountPractice;
    }
} 