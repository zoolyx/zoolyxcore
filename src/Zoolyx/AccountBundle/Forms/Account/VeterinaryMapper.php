<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 26.09.18
 * Time: 18:57
 */

namespace Zoolyx\AccountBundle\Forms\Account;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\AccountBundle\Forms\Account\Veterinary as AccountVeterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;

/**
 * @DI\Service("zoolyx_account.mapper.veterinary")
 */
class VeterinaryMapper {

    public function map(VeterinaryPractice $veterinaryPractice) {
        $veterinary = $veterinaryPractice->getVeterinary();
        $accountVeterinary = new AccountVeterinary();
        $accountVeterinary
            ->setFirstName($veterinary->getFirstName())
            ->setLastName($veterinary->getLastName())
            ->setOrderNumber($veterinary->getOrderNumber())
            ->setGsm($veterinary->getTelephone())
            ->setNotificationId($veterinaryPractice->getNotificationId())
        ;

        return $accountVeterinary;
    }
} 