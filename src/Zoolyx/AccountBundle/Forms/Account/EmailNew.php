<?php

namespace Zoolyx\AccountBundle\Forms\Account;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Zoolyx\CoreBundle\Entity\Repository\UserRepository;

/**
 * @DI\Service("zoolyx_account.form.email_new")
 */
class EmailNew
{
    /** @var UserRepository */
    private $userRepository;

    /**
     * Constructor.
     *
     * @param UserRepository $userRepository
     *
     * @DI\InjectParams({
     *  "userRepository" = @DI\Inject("zoolyx_core.repository.user")
     * })
     */
    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        $user = $this->userRepository->findOneBy(array(
            'email' => $this->getEmail()
        ));

        if ($user) {
            $context->buildViolation('userRegistration.email.already_used')
                ->atPath('email')
                ->addViolation();
        }
    }


    /** @var string */
    private $email;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }


}
