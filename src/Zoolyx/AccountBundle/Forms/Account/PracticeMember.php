<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 26.09.18
 * Time: 20:21
 */

namespace Zoolyx\AccountBundle\Forms\Account;


class PracticeMember {
    const PRACTICE_ROLE_MEMBER = 0;
    const PRACTICE_ROLE_ADMIN = 1;
    const PRACTICE_ROLE_REMOVED = 2;

    /** @var int */
    private $veterinaryPracticeId;
    /** @var string */
    private $firstName="";
    /** @var string */
    private $lastName = "";
	/** @var string */
    private $email = "";
    /** @var int  */
    private $role = 0;
    /** @var bool */
    private $isMe = false;

    /**
     * @return int
     */
    public function getVeterinaryPracticeId()
    {
        return $this->veterinaryPracticeId;
    }

    /**
     * @param int $veterinaryPracticeId
     * @return $this
     */
    public function setVeterinaryPracticeId($veterinaryPracticeId)
    {
        $this->veterinaryPracticeId = $veterinaryPracticeId;
        return $this;
    }



    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this;
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }
    
 	/**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this;
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return int
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param int $role
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAdmin() {
        return $this->getRole() == self::PRACTICE_ROLE_ADMIN;
    }

    /**
     * @return boolean
     */
    public function isMe()
    {
        return $this->isMe;
    }

    /**
     * @param boolean $isMe
     * @return $this
     */
    public function setIsMe($isMe)
    {
        $this->isMe = $isMe;
        return $this;
    }




} 