<?php

namespace Zoolyx\AccountBundle\Forms\Account;

use Symfony\Component\Validator\Constraints as Assert;


class Veterinary
{
    /** @var string */
    private $firstName;
    /** @var string */
    private $lastName;
    /** @var string */
    private $orderNumber = '';
 	/** @var string */
    private $gsm = '';
    /** @var string */
    public $notificationId = '';

    /**
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return $this;
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getGsm() {
        return $this->gsm;
    }

    /**
     * @param string $gsm
     * @return $this
     */
    public function setGsm($gsm) {
        $this->gsm = $gsm;
        return $this;
    }


    /**
     * @return string
     */
    public function getNotificationId()
    {
    return $this->notificationId;
    }

    /**
     * @param string $notificationId
     * @return $this
     */
    public function setNotificationId($notificationId)
    {
    $this->notificationId = $notificationId;
    return $this;
    }
}
