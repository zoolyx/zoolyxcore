<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 9/7/14
 * Time: 4:34 PM
 */

namespace Zoolyx\AccountBundle\Forms\Account;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Zoolyx\CoreBundle\Entity\NotificationEndpoint;


class AccountPracticeType extends AbstractType
{


    /** @var Translator */
    private $translator;
    /** @var array */
    private $notificationEndpoints = array();

    public function __construct($translator, $notificationEndpoints) {
        $this->translator = $translator;
        /** @var NotificationEndpoint $notificationEndpoint */
        foreach ($notificationEndpoints as $notificationEndpoint) {
            if ($notificationEndpoint->veterinaryCanSelect()) {
                $this->notificationEndpoints[$notificationEndpoint->getName()] = $notificationEndpoint->getId();
            }
        }

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAttribute('class','form-horizontal zlx_form')
            ->add('name', 'text', array())
            ->add('street', 'text', array())
            ->add('zipCode', 'text', array())
            ->add('city', 'text', array())
            ->add('country' , 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'choices'  => array(
                    $this->translator->trans('country.be') => AccountPractice::COUNTRY_BE,
                    $this->translator->trans('country.lu') => AccountPractice::COUNTRY_LU,
                    $this->translator->trans('country.nl') => AccountPractice::COUNTRY_NL,
                    $this->translator->trans('country.fr') => AccountPractice::COUNTRY_FR
                ),
                'choices_as_values' => true,
            ))
            ->add('telephone', 'text', array('required' => false))
            ->add('vat', 'text', array('required' => false))
            ->add('invoiceDefault', 'choice', array(
                'label' => $this->translator->trans('request.invoice'),
                'choices' => array(
                    $this->translator->trans('request.da') =>1,
                    $this->translator->trans('request.ad') =>2
                ),
                'choices_as_values' => true,
                'expanded' => true,
                'multiple' => false,
                'attr' => array(
                    //'class' => 'form-control'
                )
            ))

            ->add('sendNotifications', 'Symfony\Component\Form\Extension\Core\Type\CheckboxType', array(
                'required' => false,
                'label' => 'settings.notifications',
            ))
            ->add('notificationEndpointId' , 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'placeholder' => '',
                'choices'  => $this->notificationEndpoints,
                'choices_as_values' => true,
                'required' => false,
            ))
        ;

    }
    
    /**
     * this gets called in the final stage before rendering the form
     *
     * @return void
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $fieldName = "street";
        /** @var FormErrorIterator $errorIterator */
        $errorIterator = $view->children[$fieldName]->vars["errors"];
        if ($errorIterator->count() > 0) {
            $view->children[$fieldName]->vars["value"] = $form->getData()->getStreet();
        }

        $fieldName = "zipCode";
        /** @var FormErrorIterator $errorIterator */
        $errorIterator = $view->children[$fieldName]->vars["errors"];
        if ($errorIterator->count() > 0) {
            $view->children[$fieldName]->vars["value"] = $form->getData()->getZipCode();
        }

        $fieldName = "city";
        /** @var FormErrorIterator $errorIterator */
        $errorIterator = $view->children[$fieldName]->vars["errors"];
        if ($errorIterator->count() > 0) {
            $view->children[$fieldName]->vars["value"] = $form->getData()->getCity();
        }

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zoolyx\AccountBundle\Forms\Account\AccountPractice',
        ));
    }

    public function getName()
    {
        return 'AccountPractice';
    }
}
