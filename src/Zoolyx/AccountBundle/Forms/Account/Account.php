<?php

namespace Zoolyx\AccountBundle\Forms\Account;

use Symfony\Component\Validator\Constraints as Assert;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;


class Account
{
    const FREQUENCY_EVERY_VERSION = 0;
    const FREQUENCY_FIRST_AND_COMPLETED = 1;
    const FREQUENCY_ONLY_COMPLETED = 2;
    const FREQUENCY_NEVER = 3;
    
    const LANGUAGE_NL = 'nl';
    const LANGUAGE_FR = 'fr';

    /** @var string */
    private $firstName;
    /** @var string */
    private $lastName;
    /** @var string */
    private $language;
    /** @var bool */
    private $isVeterinary = false;
    /** @var bool */
    private $isOrganisation = false;
    /** @var string */
    private $orderNumber = '';
    /** @var array */
    private $veterinaryPractices = array();
    /** @var int */
    public $reportFrequency = 1;
    /** @var bool */
    public $autoShareOwners = true;

    /**
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;
        return $this;
    }
	
	/**
     * @return string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * @param string $language
     * @return $this
     */
    public function setLanguage($language) {
        $this->language = $language;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isVeterinary()
    {
        return $this->isVeterinary;
    }

    /**
     * @param boolean $isVeterinary
     * @return $this;
     */
    public function setIsVeterinary($isVeterinary)
    {
        $this->isVeterinary = $isVeterinary;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isOrganisation()
    {
        return $this->isOrganisation;
    }

    /**
     * @param boolean $isOrganisation
     */
    public function setIsOrganisation($isOrganisation)
    {
        $this->isOrganisation = $isOrganisation;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return $this;
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }

    /**
     * @return array
     */
    public function getVeterinaryPractices()
    {
        return $this->veterinaryPractices;
    }

    /**
     * @param VeterinaryPractice $veterinaryPractice
     * @return $this
     */
    public function addVeterinaryPractice($veterinaryPractice)
    {
        $this->veterinaryPractices[] = $veterinaryPractice;
        return $this;
    }

    /**
     * @return int
     */
    public function getReportFrequency()
    {
        return $this->reportFrequency;
    }

    /**
     * @param int $reportFrequency
     * @return $this;
     */
    public function setReportFrequency($reportFrequency)
    {
        $this->reportFrequency = $reportFrequency;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isAutoShareOwners()
    {
        return $this->autoShareOwners;
    }

    /**
     * @param boolean $autoShareOwners
     * @return $this;
     */
    public function setAutoShareOwners($autoShareOwners)
    {
        $this->autoShareOwners = $autoShareOwners;
        return $this;
    }

}
