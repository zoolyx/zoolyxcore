<?php

namespace Zoolyx\AccountBundle\Forms\Account;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Zoolyx\CoreBundle\Entity\User;

class PasswordNew
{

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        $encoder = $this->encoderFactory->getEncoder($this->user);

        $passwordValid = $encoder->isPasswordValid($this->user->getPassword(),$this->getOldPassword(),$this->user->getSalt());

        if (!$passwordValid) {
            $context->buildViolation('changePassword.wrong_password')
                ->atPath('oldPassword')
                ->addViolation();
        }
    }

    /** @var EncoderFactory */
    private $encoderFactory;

    /** @var User */
    private $user;

    /** @var string */
    private $oldPassword;
    /** @var string */
    private $newPassword;

    /**
     * @return EncoderFactory
     */
    public function getEncoderFactory()
    {
        return $this->encoderFactory;
    }

    /**
     * @param EncoderFactory $encoderFactory
     * @return $this
     */
    public function setEncoderFactory($encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }



    /**
     * @return string
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * @param string $oldPassword
     * @return $this
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param string $newPassword
     * @return $this
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
        return $this;
    }


}
