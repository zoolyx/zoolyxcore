<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 26.09.18
 * Time: 18:57
 */

namespace Zoolyx\AccountBundle\Forms\Account;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;

/**
 * @DI\Service("zoolyx_account.mapper.account")
 */
class AccountMapper {

    /** @var VeterinaryRepository */
    private $veterinaryRepository;

    /** @var Veterinary */
    private $veterinary = null;

    /**
     * Constructor.
     *
     * @param VeterinaryRepository $veterinaryRepository
     *
     * @DI\InjectParams({
     *  "veterinaryRepository" = @DI\Inject("zoolyx_core.repository.veterinary")
     * })
     */
    public function __construct(VeterinaryRepository $veterinaryRepository) {
        $this->veterinaryRepository = $veterinaryRepository;
    }

    public function map(User $user) {
        $account = new Account();
        $account
            ->setFirstName($user->getFirstName())
            ->setLastName($user->getLastName())
            ->setLanguage($user->getLanguage())
            ->setReportFrequency($user->getReportFrequency())
        ;
        /** @var Veterinary $veterinary */
        $veterinary = ($user->hasRole('ROLE_VETERINARY') || $user->hasRole('ROLE_ORGANISATION')) ? $this->veterinaryRepository->searchForAccount($user) : null;
        $this->veterinary = $veterinary;
        if ($veterinary) {
            $account
                ->setFirstName($veterinary->getFirstName())
                ->setLastName($veterinary->getLastName())
                ->setIsVeterinary(!is_null($veterinary))
                ->setOrderNumber($veterinary->getOrderNumber())
                ->setAutoShareOwners($user->isAutoShareOwners())
            ;
            /** @var VeterinaryPractice $veterinaryPractice */
            foreach ($veterinary->getVeterinaryPractices() as $veterinaryPractice) {
                if ($veterinaryPractice->getRole() == PracticeMember::PRACTICE_ROLE_REMOVED) continue;
                $account->addVeterinaryPractice($veterinaryPractice);
            }
        }

        return $account;
    }

    /**
     * @return Veterinary
     */
    public function getVeterinary() {
        return $this->veterinary;
    }
} 