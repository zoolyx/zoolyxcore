<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 9/7/14
 * Time: 4:34 PM
 */

namespace Zoolyx\AccountBundle\Forms;


use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class RegistrationType extends AbstractType
{
    /** @var Translator */
    private $translator;

    public function __construct($translator)
    {
        $this->translator = $translator;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', 'text', array(
                'label' => $this->translator->trans('form.registration.first_name'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.registration.first_name.placeholder'),
                )
            ))
            ->add('lastName', 'text', array(
                'label' => $this->translator->trans('form.registration.last_name'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.registration.last_name.placeholder'),
                )
            ))
            ->add('email', 'email', array(
                'label' => $this->translator->trans('form.registration.email'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.registration.email.placeholder'),
                )
            ))
            ->add('language', 'choice', array(
                'label' => $this->translator->trans('form.registration.language'),
                'choices' => array('Nederlands'=> 'nl', 'Français' => 'fr'),
                'choices_as_values' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('agreeWithConditions', 'checkbox', array(
                'label' => $this->translator->trans('form.registration.agree_with_conditions'),
                'required' => true,
                'attr' => array(
                    'style' => 'margin-top: -7px;'
                )
            ))
            ->add('acceptMails', 'checkbox', array(
                'label' => $this->translator->trans('form.registration.accept_emails'),
                'required' => true,
                'attr' => array(
                    'style' => 'margin-top: -7px;'
                )
            ))
        ;


    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zoolyx\AccountBundle\Forms\Registration',
        ));
    }

    public function getName()
    {
        return 'Registration';
    }
}
