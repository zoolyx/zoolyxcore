<?php

namespace Zoolyx\AccountBundle\Forms;

use Symfony\Component\Validator\Constraints as Assert;


class PasswordNew
{

    /**
     * @var string
     */
    private $password;

    /** @var boolean */
    private $agreeWithConditions;

    /** @var boolean */
    private $acceptMails;


    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }
    /**
     * @return boolean
     */
    public function agreeWithConditions()
    {
        return $this->agreeWithConditions;
    }

    /**
     * @param boolean $agreeWithConditions
     * @return $this
     */
    public function setAgreeWithConditions($agreeWithConditions)
    {
        $this->agreeWithConditions = $agreeWithConditions;
        return $this;
    }

    /**
     * @return boolean
     */
    public function acceptMails()
    {
        return $this->acceptMails;
    }

    /**
     * @param boolean $acceptMails
     * @return $this
     */
    public function setAcceptMails($acceptMails)
    {
        $this->acceptMails = $acceptMails;
        return $this;
    }

}
