<?php

namespace Zoolyx\AccountBundle\Forms;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\User;

/**
 * @DI\Service("zoolyx_account.user_registration_mapper")
 */
class UserRegistrationMapper
{

    public function map(User $user) {
        $userRegistration = new UserRegistration();
        $userRegistration
            ->setFirstName($user->getFirstName())
            ->setLastName($user->getLastName())
            ->setEmail($user->getEmail())
            ->setLanguage($user->getLanguage())
        ;

        return $userRegistration;
    }

}
