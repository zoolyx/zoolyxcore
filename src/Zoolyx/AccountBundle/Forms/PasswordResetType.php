<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 9/7/14
 * Time: 4:34 PM
 */

namespace Zoolyx\AccountBundle\Forms;


use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class PasswordResetType extends AbstractType
{
    /** @var Translator */
    private $translator;

    public function __construct($translator)
    {
        $this->translator = $translator;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', array(
                'label' => $this->translator->trans('form.registration.email'),
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.registration.email.placeholder'),
                )
            ))
        ;


    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zoolyx\AccountBundle\Forms\PasswordReset',
        ));
    }

    public function getName()
    {
        return 'PasswordReset';
    }
}
