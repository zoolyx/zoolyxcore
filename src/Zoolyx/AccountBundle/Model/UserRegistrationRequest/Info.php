<?php

namespace Zoolyx\AccountBundle\Model\UserRegistrationRequest;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("zoolyx_account.user_registration_request.info")
 */
class Info
{
    const NAME_ORDER_NUMBER = 'OrderNr';

    /** @var array */
    private $info;

    public function addInfo($name, $value) {
        $this->info[$name] = $value;
        return $this;
    }

    public function getInfo() {
        return$this->info;
    }
}

