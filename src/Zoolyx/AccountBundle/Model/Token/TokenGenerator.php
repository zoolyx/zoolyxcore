<?php

namespace Zoolyx\AccountBundle\Model\Token;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("zoolyx_account.generator.token")
 */
class TokenGenerator
{
    public function get($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }
}

