<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 29.10.18
 * Time: 17:54
 */
namespace Zoolyx\AccountBundle\Model\Ftp;

use DOMDocument;

class VeterinaryXml {

    /** @var string */
    private $limsId;
    /** @var string */
    private $practiceId;
    /** @var string */
    private $practiceName;
    /** @var string */
    private $lastName;
    /** @var string */
    private $firstName;
    /** @var string */
    private $street;
    /** @var string */
    private $zipCode;
    /** @var string */
    private $city;
    /** @var string */
    private $country;
    /** @var string */
    private $language;
    /** @var string */
    private $email;
    /** @var string */
    private $tel;
    /** @var string */
    private $gsm;
    /** @var string */
    private $regId;
    /** @var string */
    private $fa;
    /** @var string */
    private $vatId;

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getFa()
    {
        return $this->fa;
    }

    /**
     * @param string $fa
     * @return $this
     */
    public function setFa($fa)
    {
        $this->fa = $fa;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLimsId()
    {
        return $this->limsId;
    }

    /**
     * @param string $limsId
     * @return $this
     */
    public function setLimsId($limsId)
    {
        $this->limsId = $limsId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPracticeId()
    {
        return $this->practiceId;
    }

    /**
     * @param string $practiceId
     * @return $this
     */
    public function setPracticeId($practiceId)
    {
        $this->practiceId = $practiceId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPracticeName()
    {
        return $this->practiceName;
    }

    /**
     * @param string $practiceName
     * @return $this
     */
    public function setPracticeName($practiceName)
    {
        $this->practiceName = $practiceName;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegId()
    {
        return $this->regId;
    }

    /**
     * @param string $regId
     * @return $this
     */
    public function setRegId($regId)
    {
        $this->regId = $regId;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return $this
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * @param string $tel
     * @return $this
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getGsm()
    {
        return $this->gsm;
    }

    /**
     * @param string $gsm
     * @return $this
     */
    public function setGsm($gsm)
    {
        $this->gsm = $gsm;
        return $this;
    }

    /**
     * @return string
     */
    public function getVatId()
    {
        return $this->vatId;
    }

    /**
     * @param string $vatId
     * @return $this
     */
    public function setVatId($vatId)
    {
        $this->vatId = $vatId;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     * @return $this
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
        return $this;
    }


    public function getXml() {
        $service = new DOMDocument("1.0","ISO-8859-1");
        $serviceContainer = $service->createElement('service');
        $serviceContainer->setAttribute('type','update_da');
        $service->appendChild($serviceContainer);

        $daContainer = $service->createElement('da');
        $daContainer->setAttribute('id',$this->getLimsId());
        $serviceContainer->appendChild($daContainer);

        $container = $service->createElement('practiceId');
        $container->nodeValue = $this->getPracticeId();
        $daContainer->appendChild($container);

        $container = $service->createElement('practiceName');
        $container->nodeValue = $this->getPracticeName();
        $daContainer->appendChild($container);

        $container = $service->createElement('lastName');
        $container->nodeValue = $this->getLastName();
        $daContainer->appendChild($container);

        $container = $service->createElement('firstName');
        $container->nodeValue = $this->getFirstName();
        $daContainer->appendChild($container);

        $container = $service->createElement('street');
        $container->nodeValue = $this->getStreet();
        $daContainer->appendChild($container);

        $container = $service->createElement('zipCode');
        $container->nodeValue = $this->getZipCode();
        $daContainer->appendChild($container);

        $container = $service->createElement('city');
        $container->nodeValue = $this->getCity();
        $daContainer->appendChild($container);

        $container = $service->createElement('country');
        $container->nodeValue = $this->getCountry();
        $daContainer->appendChild($container);

        $container = $service->createElement('language');
        $container->nodeValue = $this->getLanguage();
        $daContainer->appendChild($container);

        $container = $service->createElement('email');
        $container->nodeValue = $this->getEmail();
        $daContainer->appendChild($container);

        $container = $service->createElement('tel');
        $container->nodeValue = $this->getTel();
        $daContainer->appendChild($container);
        
        $container = $service->createElement('gsm');
        $container->nodeValue = $this->getGsm();
        $daContainer->appendChild($container);

        $container = $service->createElement('regId');
        $container->nodeValue = $this->getRegId();
        $daContainer->appendChild($container);

        $container = $service->createElement('fa');
        $container->nodeValue = $this->getFa();
        $daContainer->appendChild($container);

        $container = $service->createElement('vatId');
        $container->nodeValue = $this->getVatId();
        $daContainer->appendChild($container);

        return $service->saveXML();
    }

}