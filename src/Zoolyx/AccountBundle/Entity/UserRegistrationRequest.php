<?php

namespace Zoolyx\AccountBundle\Entity;
use DateTime;
use Zoolyx\CoreBundle\Entity\User;

/**
 * UserRegistrationRequest
 */
class UserRegistrationRequest
{
    const STATE_OPEN = 0;
    const STATE_APPROVED = 1;
    const STATE_DECLINED = 2;

    /** @var integer */
    private $id;

    /** @var DateTime */
    private $createdOn;

    /** @var boolean */
    private $requestedByUser;

    /** @var integer */
    private $state;

    /** @var string */
    private $info;

    /** @var DateTime */
    private $approvedOn;

    /** @var User */
    private $account;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * @param DateTime $createdOn
     * @return $this
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isRequestedByUser()
    {
        return $this->requestedByUser;
    }

    /**
     * @param boolean $requestedByUser
     * @return $this
     */
    public function setRequestedByUser($requestedByUser)
    {
        $this->requestedByUser = $requestedByUser;
        return $this;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return array
     */
    public function getInfo()
    {
        return unserialize($this->info);
    }

    /**
     * @param array $info
     * @return $this
     */
    public function setInfo($info)
    {
        $this->info = serialize($info);
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getApprovedOn()
    {
        return $this->approvedOn;
    }

    /**
     * @param DateTime $approvedOn
     * @return $this
     */
    public function setApprovedOn($approvedOn)
    {
        $this->approvedOn = $approvedOn;
        return $this;
    }

    /**
     * @return User
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param User $account
     * @return $this
     */
    public function setAccount($account)
    {
        $this->account = $account;
        return $this;
    }


}

