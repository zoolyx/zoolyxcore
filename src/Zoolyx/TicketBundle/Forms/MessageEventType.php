<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 9/7/14
 * Time: 4:34 PM
 */

namespace Zoolyx\TicketBundle\Forms;

use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MessageEventType extends AbstractType
{
    /** @var Translator */
    private $translator;

    /** @var bool  */
    private $internal = false;

    public function __construct($translator, $internal = false)
    {
        $this->translator = $translator;
        $this->internal = $internal;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction('#')
            ->add('content','textarea', array(
                'label' => 'Message',
                'attr' => array(
                    'rows' => 5,
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('ticket.type_message'),
                ),
                'label_attr'=> array('class'=>'sr-only'),
            ))
            ->add('attachments', FileType::class , array(
                'label' => 'Attachments',
                'multiple' => true,
                'required' => false,
                'attr'     => [
                    'accept' => '.pdf,image/*',
                    'multiple' => 'multiple'
                ]
            ))

            ->add('create','submit', array(
            	'label' => $this->translator->trans('ticket.send'),
            	'attr' => [
            		'class' => 'fusion-button button-flat fusion-button-square button-medium button-default button-1'
            	]
          	 ));

        if ($this->internal) {
            $builder
                ->add('internal','checkbox', array(
                    'label' => $this->translator->trans('ticket.internal'),
                    'required' => false,
                ));
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zoolyx\TicketBundle\Entity\MessageEvent',
        ));
    }

    public function getName()
    {
        return 'Ticket';
    }
}
