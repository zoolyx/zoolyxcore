<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 9/7/14
 * Time: 4:34 PM
 */

namespace Zoolyx\TicketBundle\Forms;

use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Zoolyx\TicketBundle\Model\QuestionTypes;

class QuestionEventType extends AbstractType
{
    /** @var Translator */
    private $translator;

    public function __construct($translator)
    {
        $this->translator = $translator;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction('#')
            ->add('content','textarea', array(
                'label' => '',
                'attr' => array(
                    'rows' => 10,
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('ticket.type_message'),
                ),
                'label_attr'=> array('class'=>'sr-only'),
            ))
            ->add('attachments', FileType::class , array(
                'label' => 'Attachments',
                'multiple' => true,
                'required' => false,
                'attr'     => [
                    'accept' => '.pdf,image/*',
                    'multiple' => 'multiple'
                ]
            ))
            /*
            ->add('type','choice', array(
                'label' => 'report_question_form.question_type',
                'expanded' => true,
                'multiple' => false,
                'choices' => QuestionTypes::types(),
                'required' => false,
            ))
            */
            ->add('create','submit', array(
            	'label' => $this->translator->trans('ticket.send'),
            	'attr' => [
            		'class' => 'fusion-button button-flat fusion-button-square button-medium button-default button-1'
            	]
          	 ));
    }

    public function getName()
    {
        return 'Ticket';
    }
}
