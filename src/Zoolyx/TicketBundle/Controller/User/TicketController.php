<?php

namespace Zoolyx\TicketBundle\Controller\User;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Zoolyx\CoreBundle\Events;
use Zoolyx\CoreBundle\Model\Exception\ObjectNotFoundException;
use Zoolyx\TicketBundle\Entity\MessageEvent;
use Zoolyx\TicketBundle\Entity\Repository\TicketRepository;
use Zoolyx\TicketBundle\Entity\Ticket;
use Zoolyx\TicketBundle\Event\UpdatedTicketEvent;
use Zoolyx\TicketBundle\Forms\MessageEventType;
use Zoolyx\TicketBundle\Model\Ticket\TicketContentManager;
use Zoolyx\TicketBundle\Model\Ticket\TicketFactory;
use Zoolyx\TicketBundle\Model\Ticket\TicketWatchManager;

class TicketController extends Controller
{
    /**
     * @var TicketRepository
     *
     * @DI\Inject("zoolyx_ticket.repository.ticket")
     */
    private $ticketRepository;

    /**
     * @var TicketFactory
     *
     * @DI\Inject("zoolyx_ticket.factory.ticket")
     */
    private $ticketFactory;

    /**
     * @var TicketContentManager
     *
     * @DI\Inject("zoolyx_ticket.manager.ticket_content")
     */
    private $ticketContentManager;

    /**
     * @var TicketWatchManager
     *
     * @DI\Inject("zoolyx_ticket.manager.ticket_watch")
     */
    private $ticketWatchManager;

    /**
     * @var Translator
     *
     * @DI\Inject("translator")
     */
    private $translator;

    /**
     * @var ObjectManager
     *
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    private $entityManager;

    /**
     * @var EventDispatcherInterface
     *
     * @DI\Inject("event_dispatcher")
     */
    private $eventDispatcher;

    public function indexAction(Request $request, $id)
    {
        /** @var Ticket $ticket */
        $ticket = $this->ticketRepository->find($id);
        if (!$ticket ) {
            throw new ObjectNotFoundException();
        }

        $messageEvent = new MessageEvent();
        $form = $this->createForm(new MessageEventType($this->translator), $messageEvent);

        if (!is_null($request)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $messageEvent->setInternal(false);
                $this->ticketContentManager->addMessage($ticket,$messageEvent, $this->getUser());
                $this->ticketContentManager->handleAttachments($messageEvent);
                $this->entityManager->persist($ticket);
                $this->entityManager->flush();

                $this->eventDispatcher->dispatch(Events::UPDATED_TICKET, new UpdatedTicketEvent($messageEvent));
            }
        };

        $this->ticketWatchManager->watchTicket($ticket, $this->getUser());
        $this->entityManager->persist($ticket);
        $this->entityManager->flush();

        $messageEvent = new MessageEvent();
        $form = $this->createForm(new MessageEventType($this->translator), $messageEvent);

        $tickets = array();
        if (!is_null($ticket->getReport())) {
            $tickets = $this->ticketRepository->findForVeterinary($ticket->getReport(), $this->getUser());
        }

        return $this->render('ZoolyxTicketBundle:User:ticket_container.html.twig', array(
            'ticket' => $ticket,
            'tickets' => $tickets,
            'form' => $form->createView(),
        ));
    }

    public function newAction() {
        /** @var Ticket $ticket */
        $ticket = $this->ticketFactory->create($this->getUser());
        $this->entityManager->persist($ticket);
        $this->entityManager->flush();

        return $this->redirect($this->generateUrl('zoolyx_ticket_ticket', array('id' => $ticket->getId())));
    }

}
