<?php

namespace Zoolyx\TicketBundle\Controller\User;

use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Exception\NotValidCurrentPageException;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zoolyx\TicketBundle\Entity\Repository\TicketRepository;
use Zoolyx\TicketBundle\Model\Ticket\TicketValidator;

class TicketsController extends Controller
{
    /**
     * @var TicketRepository
     *
     * @DI\Inject("zoolyx_ticket.repository.ticket")
     */
    private $ticketRepository;

    /**
     * @var TicketValidator
     *
     * @DI\Inject("zoolyx_ticket.validator.ticket")
     */
    private $ticketValidator;


    public function indexAction(Request $request)
    {
        list ($keyword, $filters) = $this->getValues($request);
        $user = $this->getUser();
        return $this->renderPage($request, $this->ticketRepository->findForUserQuery($user,$keyword, $filters),$keyword, $filters);
    }

    private function getValues(Request $request) {
        $keyword = $request->query->get('key','');
        $filterString = $request->query->get('filter','');
        $filters = $filterString=='' ? array() : explode(',',$filterString);

        if (count($filters) == 0) {
            $filters = array('New','Assigned','Open','Closed');
        }
        return array($keyword,$filters);
    }

    private function renderPage(Request $request, $query, $keyword='', $filters=array()) {
        $adapter = new DoctrineORMAdapter($query);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(10);
        $page = $request->query->get('page', 1);
        try  {
            $pager->setCurrentPage($page);
        }
        catch(NotValidCurrentPageException $e) {
            throw new NotFoundHttpException('Illegal page');
        }

        return $this->render('ZoolyxTicketBundle:User:tickets.html.twig', array(
            'pager' => $pager,
            'keyword' => $keyword,
            'filters' => $filters,
            'ticketValidator' => $this->ticketValidator
        ));
    }
}
