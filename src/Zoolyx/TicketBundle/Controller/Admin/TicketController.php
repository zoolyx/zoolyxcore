<?php

namespace Zoolyx\TicketBundle\Controller\Admin;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Zoolyx\CoreBundle\Entity\Owner;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\Repository\OwnerRepository;
use Zoolyx\CoreBundle\Entity\Repository\ReportRepository;
use Zoolyx\CoreBundle\Entity\Repository\UserRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Events;
use Zoolyx\CoreBundle\Model\Exception\NotAllowedException;
use Zoolyx\CoreBundle\Model\Exception\ObjectNotFoundException;
use Zoolyx\TicketBundle\Entity\AdministrationGroup;
use Zoolyx\TicketBundle\Entity\MessageEvent;
use Zoolyx\TicketBundle\Entity\Repository\AdministrationGroupRepository;
use Zoolyx\TicketBundle\Entity\Repository\TicketCopyRepository;
use Zoolyx\TicketBundle\Entity\Repository\TicketRepository;
use Zoolyx\TicketBundle\Entity\Ticket;
use Zoolyx\TicketBundle\Entity\TicketCopy;
use Zoolyx\TicketBundle\Event\AssignedTicketEvent;
use Zoolyx\TicketBundle\Event\UpdatedTicketEvent;
use Zoolyx\TicketBundle\Forms\MessageEventType;
use Zoolyx\TicketBundle\Model\Menu\MenuItem;
use Zoolyx\TicketBundle\Model\Ticket\TicketContentManager;
use Zoolyx\TicketBundle\Model\Ticket\TicketContextManager;
use Zoolyx\TicketBundle\Model\Ticket\TicketFactory;
use Zoolyx\TicketBundle\Model\Ticket\TicketManager;
use Zoolyx\TicketBundle\Model\Ticket\TicketStateManager;
use Zoolyx\TicketBundle\Model\Ticket\TicketWatchManager;

class TicketController extends Controller
{
    /**
     * @var TicketRepository
     *
     * @DI\Inject("zoolyx_ticket.repository.ticket")
     */
    private $ticketRepository;
    /**
     * @var TicketCopyRepository
     *
     * @DI\Inject("zoolyx_ticket.repository.ticket_copy")
     */
    private $ticketCopyRepository;

    /**
     * @var TicketManager
     *
     * @DI\Inject("zoolyx_ticket.manager.ticket")
     */
    private $ticketManager;

    /**
     * @var TicketFactory
     *
     * @DI\Inject("zoolyx_ticket.factory.ticket")
     */
    private $ticketFactory;

    /**
     * @var TicketContentManager
     *
     * @DI\Inject("zoolyx_ticket.manager.ticket_content")
     */
    private $ticketContentManager;

    /**
     * @var TicketContextManager
     *
     * @DI\Inject("zoolyx_ticket.manager.ticket_context")
     */
    private $ticketContextManager;

    /**
     * @var TicketStateManager
     *
     * @DI\Inject("zoolyx_ticket.manager.ticket_state")
     */
    private $ticketStateManager;

    /**
     * @var TicketWatchManager
     *
     * @DI\Inject("zoolyx_ticket.manager.ticket_watch")
     */
    private $ticketWatchManager;

    /**
     * @var AdministrationGroupRepository
     *
     * @DI\Inject("zoolyx_ticket.repository.administration_group")
     */
    private $administrationGroupRepository;

    /**
     * @var VeterinaryRepository
     *
     * @DI\Inject("zoolyx_core.repository.veterinary")
     */
    private $veterinaryRepository;

    /**
     * @var OwnerRepository
     *
     * @DI\Inject("zoolyx_core.repository.owner")
     */
    private $ownerRepository;

    /**
     * @var UserRepository
     *
     * @DI\Inject("zoolyx_core.repository.user")
     */
    private $userRepository;

    /**
     * @var ReportRepository
     *
     * @DI\Inject("zoolyx_core.repository.report")
     */
    private $reportRepository;

	/**
     * @var Translator
     *
     * @DI\Inject("translator")
     */
    private $translator;

    /**
     * @var ObjectManager
     *
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    private $entityManager;

    /**
     * @var EventDispatcherInterface
     *
     * @DI\Inject("event_dispatcher")
     */
    private $eventDispatcher;

    public function indexAction(Request $request, $id)
    {
        /** @var Ticket $ticket */
        $ticket = $this->ticketRepository->find($id);
        if (!$ticket ) {
            throw new ObjectNotFoundException();
        }

        $messageEvent = new MessageEvent();
        $form = $this->createForm(new MessageEventType($this->translator, true), $messageEvent);

        if (!is_null($request)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->ticketContentManager->addMessage($ticket,$messageEvent, $this->getUser());
                $this->ticketContentManager->handleAttachments($messageEvent);
                $this->entityManager->persist($ticket);
                $this->entityManager->flush();

                $this->eventDispatcher->dispatch(Events::UPDATED_TICKET, new UpdatedTicketEvent($messageEvent));
            }
        };

        $this->ticketWatchManager->watchTicket($ticket, $this->getUser());
        $this->entityManager->persist($ticket);
        $this->entityManager->flush();

        $messageEvent = new MessageEvent();
        $form = $this->createForm(new MessageEventType($this->translator, true), $messageEvent);

        return $this->render('ZoolyxTicketBundle:Default:ticket.html.twig', array(
            'ticket' => $ticket,
            'form' => $form->createView(),
            'administrationGroups' => $this->administrationGroupRepository->findAll()
        ));
    }

    public function newAction(Request $request) {
        /** @var Ticket $ticket */
        $ticket = $this->ticketFactory->create($this->getUser());

        if (!is_null($request)) {
            $messageEvent = new MessageEvent();
            $form = $this->createForm(new MessageEventType($this->translator, true), $messageEvent);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->entityManager->persist($ticket);
                $this->entityManager->flush(); //because we need the id

                $this->ticketContentManager->addMessage($ticket,$messageEvent, $this->getUser());
                $this->ticketContentManager->handleAttachments($messageEvent);

                $this->entityManager->persist($ticket);
                $this->entityManager->flush();

                $this->ticketWatchManager->watchTicket($ticket, $this->getUser());
                $this->entityManager->persist($ticket);
                $this->entityManager->flush();

                return $this->redirect($this->generateUrl('zoolyx_ticket_ticket', array('id' => $ticket->getId())));
            }
        }

        return $this->render('ZoolyxTicketBundle:Default:ticket_new.html.twig', array(
            'menuItem' => new MenuItem(MenuItem::MENU_NEW_TICKET),
            'ticket' => $ticket,
            'form' => $this->createForm(new MessageEventType($this->translator, true), new MessageEvent())->createView(),
            'administrationGroups' => $this->administrationGroupRepository->findAll()
        ));

    }

    public function assignGroupAction($id, $groupId)
    {
        /** @var Ticket $ticket */
        $ticket = $this->ticketRepository->find($id);

        /** @var AdministrationGroup $administrationGroup */
        $administrationGroup = $this->administrationGroupRepository->find($groupId);
        if (!$administrationGroup) {
            throw new ObjectNotFoundException();
        }

        $this->ticketManager->assignGroup($ticket, $administrationGroup, $this->getUser());
        $this->entityManager->persist($ticket);
        $this->entityManager->flush();

        return $this->redirect($this->generateUrl('zoolyx_ticket_ticket', array('id' => $id)));
    }

    public function assignAction($id, Request $request)
    {
        $assignmentId = $request->get('assignmentId');
        $assignmentParts = explode('_',$assignmentId);

        /** @var Ticket $ticket */
        $ticket = $this->ticketRepository->find($id);

        if ($assignmentParts[0] == 'p') {
            $administratorId = $assignmentParts[1];

            /** @var User $administrator */
            $administrator = $this->userRepository->find($administratorId);
            if (!$administrator) {
                throw new ObjectNotFoundException('administrator='.$administratorId);
            }
            if (!$administrator->hasRole("ROLE_ADMINISTRATOR")) {
                throw new NotAllowedException();
            }
            if ($this->ticketManager->isNewAdministrator($ticket, $administrator)) {
                $this->ticketManager->assignAdministrator($ticket, $administrator, $this->getUser());
                $this->entityManager->persist($ticket);
                $this->entityManager->flush();

                $this->eventDispatcher->dispatch(Events::ASSIGNED_TICKET, new AssignedTicketEvent($ticket, $administrator->getEmail()));
            }
        }
        if ($assignmentParts[0] == 'g') {
            $groupId = $assignmentParts[1];

            /** @var AdministrationGroup $group */
            $group = $this->administrationGroupRepository->find($groupId);
            if (!$group) {
                throw new ObjectNotFoundException('administrationGroup='.$groupId);
            }
            $this->ticketManager->assignGroup($ticket, $group, $this->getUser());
            $this->entityManager->persist($ticket);
        	$this->entityManager->flush();

            $this->eventDispatcher->dispatch(Events::ASSIGNED_TICKET, new AssignedTicketEvent($ticket, $group->getEmail()));
        }

        return $this->redirect($this->generateUrl('zoolyx_ticket_ticket', array('id' => $id)));
    }

    public function addCopyAction($id, Request $request)
    {
        $email = $request->get('email');

        /** @var Ticket $ticket */
        $ticket = $this->ticketRepository->find($id);

        $ticketCopy = new TicketCopy();
        $ticketCopy
            ->setEmail($email)
            ->setTicket($ticket);

        $this->ticketManager->addCopy($ticket, $ticketCopy, $this->getUser());
        $this->entityManager->persist($ticketCopy);
        $this->entityManager->persist($ticket);
        $this->entityManager->flush();

        return $this->redirect($this->generateUrl('zoolyx_ticket_ticket', array('id' => $id)));
    }


    public function removeCopyAction($id, $copyId)
    {
        /** @var Ticket $ticket */
        $ticket = $this->ticketRepository->find($id);

        /** @var TicketCopy $ticketCopy */
        $ticketCopy = $this->ticketCopyRepository->find($copyId);

        $this->ticketManager->removeCopy($ticket, $ticketCopy, $this->getUser());

        $this->entityManager->remove($ticketCopy);
        $this->entityManager->persist($ticket);
        $this->entityManager->flush();

        return $this->redirect($this->generateUrl('zoolyx_ticket_ticket', array('id' => $id)));
    }

    public function setVeterinaryAction($id, $veterinaryId) {
        /** @var Ticket $ticket */
        $ticket = $this->ticketRepository->find($id);

        /** @var Veterinary $veterinary */
        $veterinary = $this->veterinaryRepository->find($veterinaryId);
        $this->ticketContextManager->setVeterinary($ticket, $veterinary);

        $this->entityManager->persist($ticket);
        $this->entityManager->flush();

        return $this->redirect($this->generateUrl('zoolyx_ticket_ticket', array('id' => $id)));
    }

    public function setOwnerAction($id, $ownerId) {
        /** @var Ticket $ticket */
        $ticket = $this->ticketRepository->find($id);

        /** @var Owner $owner */
        $owner = $this->ownerRepository->find($ownerId);
        $this->ticketContextManager->setOwner($ticket, $owner);

        $this->entityManager->persist($ticket);
        $this->entityManager->flush();

        return $this->redirect($this->generateUrl('zoolyx_ticket_ticket', array('id' => $id)));
    }

    public function setUserAction($id, $userId) {
        /** @var Ticket $ticket */
        $ticket = $this->ticketRepository->find($id);

        /** @var User $user */
        $user = $this->userRepository->find($userId);
        $this->ticketContextManager->setUser($ticket, $user);

        $this->entityManager->persist($ticket);
        $this->entityManager->flush();

        return $this->redirect($this->generateUrl('zoolyx_ticket_ticket', array('id' => $id)));
    }

    public function setReportAction($id, $reportId) {
        /** @var Ticket $ticket */
        $ticket = $this->ticketRepository->find($id);

        /** @var Report $report */
        $report = $this->reportRepository->find($reportId);
        $this->ticketContextManager->setReport($ticket, $report);

        $this->entityManager->persist($ticket);
        $this->entityManager->flush();

        return $this->redirect($this->generateUrl('zoolyx_ticket_ticket', array('id' => $id)));
    }

    public function openAction($id) {
        /** @var Ticket $ticket */
        $ticket = $this->ticketRepository->find($id);

        $this->ticketStateManager->setOpen($ticket);

        $this->entityManager->persist($ticket);
        $this->entityManager->flush();

        return $this->redirect($this->generateUrl('zoolyx_ticket_ticket', array('id' => $id)));
    }

    public function closeAction($id) {
        /** @var Ticket $ticket */
        $ticket = $this->ticketRepository->find($id);

        $this->ticketStateManager->setClosed($ticket);

        $this->entityManager->persist($ticket);
        $this->entityManager->flush();

        return $this->redirect($this->generateUrl('zoolyx_ticket_ticket', array('id' => $id)));
    }

}
