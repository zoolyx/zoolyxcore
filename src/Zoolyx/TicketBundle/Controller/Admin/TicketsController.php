<?php

namespace Zoolyx\TicketBundle\Controller\Admin;

use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Exception\NotValidCurrentPageException;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zoolyx\TicketBundle\Entity\Repository\AdministrationGroupRepository;
use Zoolyx\TicketBundle\Entity\Repository\TicketRepository;
use Zoolyx\TicketBundle\Model\Menu\MenuItem;
use Zoolyx\TicketBundle\Model\Ticket\TicketValidator;

class TicketsController extends Controller
{
    /**
     * @var TicketRepository
     *
     * @DI\Inject("zoolyx_ticket.repository.ticket")
     */
    private $ticketRepository;

    /**
     * @var TicketValidator
     *
     * @DI\Inject("zoolyx_ticket.validator.ticket")
     */
    private $ticketValidator;

    /**
     * @var AdministrationGroupRepository
     *
     * @DI\Inject("zoolyx_ticket.repository.administration_group")
     */
    private $administrationGroupRepository;

    public function indexAction(Request $request)
    {
        list ($keyword, $filters) = $this->getValues($request);
        $user = $this->getUser();
        return $this->renderPage($request, new MenuItem(MenuItem::MENU_MY_TICKETS), $this->ticketRepository->findQuery($user,null,$keyword, $filters),$keyword, $filters);
    }
    public function groupAction(Request $request, $id)
    {
        list ($keyword, $filters) = $this->getValues($request);
        return $this->renderPage($request, new MenuItem(MenuItem::MENU_GROUP_TICKETS, $id), $this->ticketRepository->findQuery(null,$id,$keyword, $filters),$keyword, $filters);
    }


    public function allAction(Request $request) {
        list ($keyword, $filters) = $this->getValues($request);
        return $this->renderPage($request, new MenuItem(MenuItem::MENU_ALL_TICKETS), $this->ticketRepository->findQuery(null,null,$keyword, $filters),$keyword, $filters);
    }

    private function getValues(Request $request) {
        $keyword = $request->query->get('key','');
        $filterString = $request->query->get('filter','');
        $filters = $filterString=='' ? array() : explode(',',$filterString);

        if (count($filters) == 0) {
            $filters = array('New','Assigned','Open');
        }
        return array($keyword,$filters);
    }
    private function renderPage(Request $request, $menuItem, $query, $keyword='', $filters=array()) {
        $adapter = new DoctrineORMAdapter($query);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(10);
        $page = $request->query->get('page', 1);
        try  {
            $pager->setCurrentPage($page);
        }
        catch(NotValidCurrentPageException $e) {
            throw new NotFoundHttpException('Illegal page');
        }

        return $this->render('ZoolyxTicketBundle:Default:tickets.html.twig', array(
            'menuItem' => $menuItem,
            'pager' => $pager,
            'administrationGroups' => $this->administrationGroupRepository->findAll(),
            'keyword' => $keyword,
            'filters' => $filters,
            'ticketValidator' => $this->ticketValidator
        ));
    }
}
