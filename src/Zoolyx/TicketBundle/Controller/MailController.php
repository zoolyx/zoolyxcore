<?php

namespace Zoolyx\TicketBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use PhpMimeMailParser\Attachment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Zoolyx\TicketBundle\Model\Mail\MailParser;

class MailController extends Controller
{
    /**
     * @var MailParser
     *
     * @DI\Inject("zoolyx_ticket.mail_parser")
     */
    private $mailParser;



    public function getAttachmentAction($id, $attachmentId)
    {
        $this->mailParser->loadParser($id);
        $attachments = $this->mailParser->attachments();
        /** @var Attachment $attachment */
        $attachment = $attachments[$attachmentId];
        return new Response($attachment->getContent(), 200, array(
                'Content-Type' => $attachment->getContentType(),
                'Content-Disposition' => 'attachment; filename="'.$attachment->getFilename().'"'
            )
        );
    }

}
