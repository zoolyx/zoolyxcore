<?php

namespace Zoolyx\TicketBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Zoolyx\TicketBundle\Entity\TicketEvent;

class UpdatedTicketEvent extends Event
{
    /**
     * @var TicketEvent
     */
    private $ticketEvent;

    /**
     * Constructor.
     *
     * @param TicketEvent $ticketEvent
     */
    public function __construct(TicketEvent $ticketEvent)
    {
        $this->ticketEvent = $ticketEvent;
    }

    /**
     * @return TicketEvent
     */
    public function getTicketEvent()
    {
        return $this->ticketEvent;
    }
}
