<?php

namespace Zoolyx\TicketBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Zoolyx\TicketBundle\Entity\Ticket;

class AssignedTicketEvent extends Event
{
    /** @var Ticket */
    private $ticket;
    /** @var string */
    private $email;

    /**
     * Constructor.
     *
     * @param Ticket $ticket
     * @param string $email
     */
    public function __construct(Ticket $ticket, $email)
    {
        $this->ticket = $ticket;
        $this->email = $email;
    }

    /**
     * @return Ticket
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }
}
