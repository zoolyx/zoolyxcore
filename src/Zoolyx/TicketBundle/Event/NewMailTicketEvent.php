<?php

namespace Zoolyx\TicketBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\TicketBundle\Entity\Ticket;

class NewMailTicketEvent extends Event
{
    /** @var Ticket */
    private $ticket;
    /** @var User */
    private $creator;
    /** @var string */
    private $originalSubject;
    /** @var string */
    private $originalMessage;

    /**
     * Constructor.
     *
     * @param Ticket $ticket
     * @param User $creator
     * @param string $originalSubject
     * @param string $originalMessage
     */
    public function __construct(Ticket $ticket, User $creator, $originalSubject, $originalMessage)
    {
        $this->ticket = $ticket;
        $this->creator = $creator;
        $this->originalSubject = $originalSubject;
        $this->originalMessage = $originalMessage;
    }

    /**
     * @return Ticket
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @return User
     */
    public function getCreator() {
        return $this->creator;
    }

    /**
     * @return string
     */
    public function getOriginalSubject() {
        return $this->originalSubject;
    }

    /**
     * @return string
     */
    public function getOriginalMessage() {
        return $this->originalMessage;
    }
}
