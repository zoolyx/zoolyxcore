<?php

namespace Zoolyx\TicketBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\TicketBundle\Entity\Ticket;

class NewTicketEvent extends Event
{
    /** @var Ticket */
    private $ticket;
    /** @var User */
    private $creator;
    /** @var string */
    private $lastMessage;

    /**
     * Constructor.
     *
     * @param Ticket $ticket
     * @param User $creator
     * @param string $lastMessage
     */
    public function __construct(Ticket $ticket, User $creator, $lastMessage)
    {
        $this->ticket = $ticket;
        $this->creator = $creator;
        $this->lastMessage = $lastMessage;
    }

    /**
     * @return Ticket
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @return User
     */
    public function getCreator() {
        return $this->creator;
    }

    /**
     * @return string
     */
    public function getLastMessage() {
        return $this->lastMessage;
    }
}
