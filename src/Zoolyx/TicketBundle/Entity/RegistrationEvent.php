<?php

namespace Zoolyx\TicketBundle\Entity;
use Zoolyx\AccountBundle\Entity\UserRegistrationRequest;
use Zoolyx\TicketBundle\Visitor\RegistrationEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\VisitorInterface;

/**
 * Parameter
 */
class RegistrationEvent extends TicketEvent
{

    /** @var UserRegistrationRequest */
    private $request;

    /**
     * @return UserRegistrationRequest
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param UserRegistrationRequest $request
     * @return $this
     */
    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }

    public function accept(VisitorInterface $visitor)
    {
        if ($visitor instanceof RegistrationEventVisitorInterface) {
            return $visitor->visitRegistrationEvent($this);
        }

        return null;
    }


}

