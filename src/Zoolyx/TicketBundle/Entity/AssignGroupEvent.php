<?php

namespace Zoolyx\TicketBundle\Entity;
use Zoolyx\TicketBundle\Visitor\AssignGroupEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\VisitorInterface;

/**
 * Parameter
 */
class AssignGroupEvent extends TicketEvent
{
    /** @var AdministrationGroup */
    private $assignedTo;

    /**
     * @return AdministrationGroup
     */
    public function getAssignedTo() {
        return $this->assignedTo;
    }

    /**
     * @param AdministrationGroup $group
     * @return AssignGroupEvent
     */
    public function setAssignedTo(AdministrationGroup $group)  {
        $this->assignedTo = $group;
        return $this;
    }

    public function accept(VisitorInterface $visitor)
    {
        if ($visitor instanceof AssignGroupEventVisitorInterface) {
            return $visitor->visitAssignGroupEvent($this);
        }

        return null;
    }

}

