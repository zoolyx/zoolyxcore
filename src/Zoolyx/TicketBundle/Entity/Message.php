<?php

namespace Zoolyx\TicketBundle\Entity;

/**
 * Parameter
 */
abstract class Message
{
    /** @var int */
    private $id;

    /** @var \DateTime */
    private $createdOn;

    /** @var array */
    private $watchers;

    /** @var MessageEvent */
    private $ticketMessage;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn() {
        return $this->createdOn;
    }

    /**
     * @param \DateTime $createdOn
     * @return Ticket
     */
    public function setCreatedOn(\DateTime $createdOn) {
        $this->createdOn = $createdOn;
        return $this;
    }

    /**
     * @return array
     */
    public function getWatchers()
    {
        return $this->watchers;
    }

    /**
     * @param Watch $watch
     * @return $this
     */
    public function addWatcher($watch)
    {
        $this->watchers[] = $watch;
        return $this;
    }



    /**
     * @return MessageEvent
     */
    public function getTicketMessage()
    {
        return $this->ticketMessage;
    }

    /**
     * @param MessageEvent $ticketMessage
     * @return $this
     */
    public function setTicketMessage($ticketMessage)
    {
        $this->ticketMessage = $ticketMessage;
        return $this;
    }



}

