<?php

namespace Zoolyx\TicketBundle\Entity;
use Zoolyx\CoreBundle\Entity\User;

/**
 * Parameter
 */
class WatchTicket
{
    /** @var int */
    private $id;

    /** @var User  */
    private $user = '';

    /** @var Ticket  */
    private $ticket;

    /** @var TicketEvent */
    private $lastMessageEvent;

    /** @var bool */
    private $hasNew;



    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Ticket
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @param Ticket $ticket
     * @return $this
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;
        return $this;
    }


    /**
     * @return TicketEvent
     */
    public function getLastMessageEvent()
    {
        return $this->lastMessageEvent;
    }

    /**
     * @param TicketEvent $lastMessageEvent
     * @return $this
     */
    public function setLastMessageEvent($lastMessageEvent)
    {
        $this->lastMessageEvent = $lastMessageEvent;
        return $this;
    }

    /**
     * @return boolean
     */
    public function hasNew()
    {
        return $this->hasNew;
    }

    /**
     * @param boolean $hasNew
     * @return $this
     */
    public function setHasNew($hasNew)
    {
        $this->hasNew = $hasNew;
        return $this;
    }

}

