<?php

namespace Zoolyx\TicketBundle\Entity;
use Zoolyx\TicketBundle\Visitor\StateEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\VisitorInterface;

/**
 * Parameter
 */
class StateEvent extends TicketEvent
{
    /** @var TicketState */
    private $ticketState;

    /**
     * @return TicketState
     */
    public function getTicketState() {
        return $this->ticketState;
    }

    /**
     * @param TicketState $state
     * @return StateEvent
     */
    public function setTicketState($state) {
        $this->ticketState = $state;
        return $this;
    }

    public function accept(VisitorInterface $visitor)
    {
        if ($visitor instanceof StateEventVisitorInterface) {
            return $visitor->visitStateEvent($this);
        }

        return null;
    }
}

