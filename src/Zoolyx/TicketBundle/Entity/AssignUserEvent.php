<?php

namespace Zoolyx\TicketBundle\Entity;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\TicketBundle\Visitor\AssignUserEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\VisitorInterface;

/**
 * Parameter
 */
class AssignUserEvent extends TicketEvent
{
    /** @var  User */
    private $assignedTo;

    /**
     * @return User
     */
    public function getAssignedTo() {
        return $this->assignedTo;
    }

    /**
     * @param User $user
     * @return AssignUserEvent
     */
    public function setAssignedTo(User $user)  {
        $this->assignedTo = $user;
        return $this;
    }

    public function accept(VisitorInterface $visitor)
    {
        if ($visitor instanceof AssignUserEventVisitorInterface) {
            return $visitor->visitAssignUserEvent($this);
        }

        return null;
    }
}

