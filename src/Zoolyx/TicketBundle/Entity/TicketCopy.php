<?php

namespace Zoolyx\TicketBundle\Entity;

/**
 * Parameter
 */
class TicketCopy
{
    /** @var int */
    private $id;

    /** @var string  */
    private $email = '';

    /** @var Ticket  */
    private $ticket;


    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }



    /**
     * @return Ticket
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @param Ticket $ticket
     * @return $this
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;
        return $this;
    }

}

