<?php

namespace Zoolyx\TicketBundle\Entity;
use Zoolyx\CoreBundle\Entity\User;

/**
 * Parameter
 */
class Watch
{
    /** @var int */
    private $id;

    /** @var User  */
    private $user = '';

    /** @var Message  */
    private $message;

    /** @var bool  */
    private $seen = false;


    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param Message $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isSeen()
    {
        return $this->seen;
    }

    /**
     * @param boolean $seen
     * @return $this
     */
    public function setSeen($seen)
    {
        $this->seen = $seen;
        return $this;
    }


}

