<?php

namespace Zoolyx\TicketBundle\Entity;
use Zoolyx\TicketBundle\Visitor\MailEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\VisitorInterface;

/**
 * Parameter
 */
class MailEvent extends TicketEvent
{

    /** @var string  */
    private $fileId = '';

    /**
     * @return string
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * @param string $fileId
     * @return $this
     */
    public function setFileId($fileId)
    {
        $this->fileId = $fileId;
        return $this;
    }

    public function accept(VisitorInterface $visitor)
    {
        if ($visitor instanceof MailEventVisitorInterface) {
            return $visitor->visitMailEvent($this);
        }

        return null;
    }

}

