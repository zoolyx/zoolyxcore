<?php

namespace Zoolyx\TicketBundle\Entity;
use Zoolyx\TicketBundle\Visitor\AddCopyEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\VisitorInterface;

/**
 * Parameter
 */
class AddCopyEvent extends TicketEvent
{
    const STATE_ADDED = 0;
    const STATE_REMOVED = 1;

    /** @var string */
    private $copiedTo = "";

    /** @var int */
    private $state = 0;

    /**
     * @return string
     */
    public function getCopiedTo()
    {
        return $this->copiedTo;
    }

    /**
     * @param string $copiedTo
     * @return $this
     */
    public function setCopiedTo($copiedTo)
    {
        $this->copiedTo = $copiedTo;
        return $this;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }



    public function accept(VisitorInterface $visitor)
    {
        if ($visitor instanceof AddCopyEventVisitorInterface) {
            return $visitor->visitAddCopyEvent($this);
        }

        return null;
    }
}

