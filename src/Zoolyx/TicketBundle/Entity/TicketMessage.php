<?php

namespace Zoolyx\TicketBundle\Entity;

/**
 * Parameter
 */
class TicketMessage extends Message
{
    /** @var MessageEvent */
    private $messageEvent;

    /**
     * @return MessageEvent
     */
    public function getMessageEvent()
    {
        return $this->messageEvent;
    }

    /**
     * @param MessageEvent $messageEvent
     * @return $this
     */
    public function setMessageEvent($messageEvent)
    {
        $this->messageEvent = $messageEvent;
        return $this;
    }


}

