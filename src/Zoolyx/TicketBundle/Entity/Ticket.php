<?php

namespace Zoolyx\TicketBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User;
use Zoolyx\CoreBundle\Entity\Owner;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\Veterinary;

/**
 * Parameter
 */
class Ticket
{
    /** @var integer */
    private $id;

    /** @var TicketState */
    private $state;

    /** @var \DateTime */
    private $createdOn;

    /** @var \DateTime */
    private $lastUpdateOn;

    /** @var User */
    private $owner;

    /** @var AdministrationGroup */
    private $assignedGroup;

    /** @var User */
    private $assignedAdministrator;

    /** @var User */
    private $user;

    /** @var Veterinary */
    private $veterinary;

    /** @var Owner */
    private $contextOwner;

    /** @var Report */
    private $report;

    /** @var array */
    private $events = array();

    /** @var array */
    private $watchers = array();

    /** @var ArrayCollection */
    private $copied = array();

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param TicketState $state
     * @return Ticket
     */
    public function setState($state) {
        $this->state = $state;
        return $this;
    }

    /**
     * @return TicketState
     */
    public function getState() {
        return $this->state;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn() {
        return $this->createdOn;
    }

    /**
     * @param \DateTime $createdOn
     * @return Ticket
     */
    public function setCreatedOn(\DateTime $createdOn) {
        $this->createdOn = $createdOn;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdateOn()
    {
        return $this->lastUpdateOn;
    }

    /**
     * @param \DateTime $lastUpdateOn
     * @return $this
     */
    public function setLastUpdateOn($lastUpdateOn)
    {
        $this->lastUpdateOn = $lastUpdateOn;
        return $this;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     * @return $this
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }


    /**
     * @return AdministrationGroup
     */
    public function getAssignedGroup() {
        return $this->assignedGroup;
    }

    /**
     * @param AdministrationGroup$group
     * @return Ticket
     */
    public function assignGroup($group) {
        $this->assignedGroup = $group;
        return $this;
    }

    /**
     * @return User
     */
    public function getAssignedAdministrator() {
        return $this->assignedAdministrator;
    }

    /**
     * @param User $administrator
     * @return Ticket
     */
    public function assignAdministrator($administrator) {
        $this->assignedAdministrator = $administrator;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Owner
     */
    public function getContextOwner()
    {
        return $this->contextOwner;
    }

    /**
     * @param Owner $contextOwner
     * @return $this
     */
    public function setContextOwner($contextOwner)
    {
        $this->contextOwner = $contextOwner;
        return $this;
    }

    /**
     * @return Veterinary
     */
    public function getVeterinary()
    {
        return $this->veterinary;
    }

    /**
     * @param Veterinary $veterinary
     * @return $this;
     */
    public function setVeterinary($veterinary)
    {
        $this->veterinary = $veterinary;
        return $this;
    }

    /**
     * @return Report
     */
    public function getReport() {
        return $this->report;
    }

    /**
     * @param Report $report
     * @return Ticket
     */
    public function setReport(Report $report) {
        $this->report = $report;
        return $this;
    }

    /**
     * @param TicketEvent $event
     * @return Ticket
     */
    public function addEvent(TicketEvent $event) {
        $this->events[] = $event;
        return $this;
    }

    /**
     * @return array
     */
    public function getEvents() {
        return $this->events;
    }

    public function getPublicEvents() {
        $publicEvents = array();
        foreach ($this->events as $event) {
            if (is_a($event,'Zoolyx\TicketBundle\Entity\MessageEvent')) {
                /** @var MessageEvent $event */
                if (!$event->isInternal()) $publicEvents[] = $event;
            }
        }
        return $publicEvents;
    }

    /**
     * @return array
     */
    public function getWatchers()
    {
        return $this->watchers;
    }

    /**
     * @param WatchTicket $watchTicket
     * @return $this
     */
    public function addWatcher($watchTicket)
    {
        $this->watchers[] = $watchTicket;
        return $this;
    }

    /**
     * @return array
     */
    public function getCopied()
    {
        return $this->copied;
    }

    /**
     * @param TicketCopy $copy
     * @return $this
     */
    public function addCopy($copy)
    {
        $this->copied[] = $copy;
        return $this;
    }

    /**
     * @param TicketCopy $copy
     * @return $this
     */
    public function removeCopy($copy) {
        $this->copied->removeElement($copy);
        return $this;
    }


}

