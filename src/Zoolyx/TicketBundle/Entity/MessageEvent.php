<?php

namespace Zoolyx\TicketBundle\Entity;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Zoolyx\TicketBundle\Visitor\MessageEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\VisitorInterface;

/**
 * Parameter
 */
class MessageEvent extends TicketEvent
{
    /**
     * @param ExecutionContextInterface $context
     *
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        $files = $this->getAttachments();
        /** @var UploadedFile $file */
        foreach ($files as $index => $file) {
            $extension = strtolower($file->guessExtension());

            if (!in_array($extension, array('pdf','gif','jpeg', 'jpg','png'))) {
                $context->buildViolation('ticket.attachment.invalid %filename%',array('%filename%' => $file->getClientOriginalName()))
                    ->atPath('attachments')
                    ->addViolation();
            }
        }
    }

    /**
     * @var string
     */
    private $content = '';

    /** @var bool  */
    private $internal = true;

    /** @var string */
    private $attachment = ''; //comma separated list of filenames

    /** @var array */
    private $attachments = array();

    /**
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * @param string $content
     * @return MessageEvent
     */
    public function setContent($content) {
        $this->content = $content;
        return $this;
    }

    public function accept(VisitorInterface $visitor)
    {
        if ($visitor instanceof MessageEventVisitorInterface) {
            return $visitor->visitMessageEvent($this);
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isInternal() {
        return $this->internal;
    }

    /**
     * @param bool $internal
     * @return MessageEvent
     */
    public function setInternal($internal) {
        $this->internal = $internal;
        return $this;
    }

    /**
     * @return array
     */
    public function getAttachments() {
        return $this->attachments;
    }

    /**
     * @param $attachment
     */
    public function addAttachment($attachment) {
        $this->attachments[] = $attachment;
    }

    /**
     * @param $attachments
     */
    public function setAttachments($attachments) {
        $this->attachments = $attachments;
    }

    /**
     * @return string
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @param string $attachment
     */
    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;
    }


}

