<?php

namespace Zoolyx\TicketBundle\Entity;

/**
 * QuestionEvent
 */
class QuestionEvent extends MessageEvent
{
    /** @var string  */
    private $type = '';

    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Question
     */
    public function setType($type) {
        $this->type = $type;
        return $this;
    }
}

