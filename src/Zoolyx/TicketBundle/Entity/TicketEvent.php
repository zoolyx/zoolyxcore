<?php

namespace Zoolyx\TicketBundle\Entity;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\TicketBundle\Visitor\VisitableInterface;

/**
 * Parameter
 */
abstract class TicketEvent implements VisitableInterface
{
    /** @var integer */
    private $id;

    /** @var \DateTime */
    private $createdOn;

    /** @var Ticket */
    private $ticket;

    /** @var User */
    private $author;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn() {
        return $this->createdOn;
    }

    /**
     * @param \DateTime $createdOn
     * @return TicketEvent
     */
    public function setCreatedOn(\DateTime $createdOn) {
        $this->createdOn = $createdOn;
        return $this;
    }

    /**
     * @return Ticket
     */
    public function getTicket() {
        return $this->ticket;
    }

    /**
     * @param Ticket $ticket
     * @return TicketEvent
     */
    public function setTicket($ticket) {
        $this->ticket = $ticket;
        return $this;
    }

    /**
     * @return User
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * @param User $author
     * @return TicketEvent
     */
    public function setAuthor($author) {
        $this->author = $author;
        return $this;
    }

}

