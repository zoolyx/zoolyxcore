<?php

namespace Zoolyx\TicketBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Zoolyx\CoreBundle\Entity\Veterinary;

class MessageEventRepository extends EntityRepository
{

    public function findAllForUserQuery(Veterinary $user) {
        $query = $this->createQueryBuilder('m')
            ->innerJoin('m.ticket', 'ticket')
            ->where('ticket.veterinary = :userId')
            ->andWhere('m.internal = false')
            ->andWhere('m.author != :accountId')
            ->orderBy('m.createdOn','Desc')
            //->groupBy('ticket.id')
            ->getQuery()
            ->setParameters(array(
                'userId' => $user->getId(),
                'accountId' => $user->getAccount()->getId()
            ));
        return $query->getResult();
    }
}
