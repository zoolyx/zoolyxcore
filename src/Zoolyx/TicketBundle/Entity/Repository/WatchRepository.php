<?php

namespace Zoolyx\TicketBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Zoolyx\CoreBundle\Entity\User;

class WatchRepository extends EntityRepository
{

    public function findAllForUserQuery(User $user) {
        $query = $this->createQueryBuilder('watch')
            ->innerJoin('watch.message', 'message')
            ->where('watch.user = :userId')
            ->orderBy('message.createdOn','Desc')
            ->getQuery()
            ->setParameters(array(
                'userId' => $user->getId()
            ));
        return $query->getResult();
    }
}
