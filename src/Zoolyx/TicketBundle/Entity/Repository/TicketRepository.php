<?php

namespace Zoolyx\TicketBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\User;

class TicketRepository extends EntityRepository
{

    public function findQuery(User $user = null, $groupId = null, $keyword='', $filters=array()) {
        $innerJoin = array();
        $leftJoin = array();
        $where = array();
        $parameters = array();
        if ($user) {
            $where[] = 't.assignedAdministrator = :userId';
            $parameters['userId'] = $user->getId();
        }
        if ($groupId) {
            $where[] = 't.assignedGroup = :groupId';
            $parameters['groupId'] = $groupId;
        }
        if ($keyword != '') {
            $leftJoin[] = array('t.report', 'r');
            $leftJoin[] = array('t.owner', 'o');
            $leftJoin[] = array('t.assignedAdministrator', 'a');
            $where[] = 'r.requestId LIKE :keyword OR o.lastName LIKE :keyword OR o.email LIKE :keyword OR a.lastName LIKE :keyword OR a.email LIKE :keyword';
            $parameters['keyword'] = '%'.$keyword.'%';
        }
        if (count($filters)) {
            $innerJoin[] = array('t.state', 'ts');
            $subWhere = array();
            foreach ($filters as $index => $filter) {
                $parameters['filter'.$index] = $filter;
                $subWhere[] = 'ts.name = :filter'.$index;
            }
            $where[] = implode(' OR ',$subWhere);
        }

        $query = $this->createQueryBuilder('t');
        foreach ($innerJoin as $statement) {
            $query->innerJoin($statement[0],$statement[1]);
        }
        foreach ($leftJoin as $statement) {
            $query->leftJoin($statement[0],$statement[1]);
        }
        foreach ($where as $statement) {
            $query->andWhere($statement);
        }
        $query->orderBy('t.lastUpdateOn', 'DESC');
        $query->setParameters($parameters);

        //die($query->getDQL());
        return $query;
    }

    public function findAllQuery() {
        $query = $this->createQueryBuilder('t')
            ->orderBy('t.createdOn', 'DESC')
        ;
        return $query;

    }

    public function findAllForGroupQuery($groupId) {
        $query = $this->createQueryBuilder('t')
            ->where('t.assignedGroup = :groupId')
            ->getQuery()
            ->setParameters(array(
                'groupId' => $groupId
            ));
        return $query;
    }

    public function findAllForAdministratorQuery(User $user) {
        $query = $this->createQueryBuilder('t')
            ->where('t.assignedAdministrator = :userId')
            ->getQuery()
            ->setParameters(array(
                'userId' => $user->getId()
            ));
        return $query;
    }

    public function findForUserQuery(User $user, $keyword='', $filters=array()) {
        $innerJoin = array();
        $leftJoin = array();
        $where = array();
        $parameters = array();

        $where[] = 't.owner = :userId';
        $parameters['userId'] = $user->getId();

        if ($keyword != '') {
            $leftJoin[] = array('t.report', 'r');
            $where[] = 'r.requestId LIKE :keyword';
            $parameters['keyword'] = '%'.$keyword.'%';
        }
        if (count($filters)) {
            $innerJoin[] = array('t.state', 'ts');
            $subWhere = array();
            foreach ($filters as $index => $filter) {
                $parameters['filter'.$index] = $filter;
                $subWhere[] = 'ts.name = :filter'.$index;
            }
            $where[] = implode(' OR ',$subWhere);
        }

        $query = $this->createQueryBuilder('t');
        foreach ($innerJoin as $statement) {
            $query->innerJoin($statement[0],$statement[1]);
        }
        foreach ($leftJoin as $statement) {
            $query->leftJoin($statement[0],$statement[1]);
        }
        foreach ($where as $statement) {
            $query->andWhere($statement);
        }
        $query->orderBy('t.createdOn', 'DESC');
        $query->setParameters($parameters);

        //die($query->getDQL());
        return $query;
    }

    public function findForVeterinary(Report $report, User $user) {
        $parameters = [
            'userId' => $user->getId(),
            'reportId' => $report->getId()
        ];

        $query = $this->createQueryBuilder('t')
            ->leftJoin('ZoolyxCoreBundle:Veterinary','v', Join::WITH, 'v.account=t.owner')
            ->leftJoin('ZoolyxCoreBundle:VeterinaryPractice','vp', Join::WITH, 'vp.veterinary=v')
            ->leftJoin('vp.practice','p')
            ->leftJoin('ZoolyxCoreBundle:VeterinaryPractice','vp2', Join::WITH, 'vp2.practice=p')
            ->leftJoin('vp2.veterinary','v2')
            ->where('t.report = :reportId')
            //->andWhere('t.owner = :userId')
            ->andWhere('v2.account = :userId or t.owner = :userId')
            ->setParameters($parameters);

        return $query->getQuery()->getResult();
    }

}
