<?php

namespace Zoolyx\TicketBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Zoolyx\CoreBundle\Entity\User;

class MessageRepository extends EntityRepository
{

    public function findAllForUserQuery(User $user) {
        $query = $this->createQueryBuilder('m')
            ->innerJoin('m.watchers', 'watch')
            ->where('watch.user = :userId')
            ->orderBy('m.createdOn','Desc')
            ->getQuery()
            ->setParameters(array(
                'userId' => $user->getId()
            ));
        return $query->getResult();
    }
}
