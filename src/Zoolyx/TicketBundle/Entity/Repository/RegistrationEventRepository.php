<?php

namespace Zoolyx\TicketBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Zoolyx\AccountBundle\Entity\UserRegistrationRequest;

class RegistrationEventRepository extends EntityRepository
{
    public function findForUserRegistrationRequest(UserRegistrationRequest $request) {
        $query = $this->createQueryBuilder('e')
            ->where('e.request = :requestId')
            ->setParameters(array(
                'requestId' => $request->getId()
            ));
        return $query->getQuery()->getOneOrNullResult();
    }

}
