<?php

namespace Zoolyx\TicketBundle\Entity;

/**
 * Parameter
 */
class TicketState
{
    /** @var integer */
    private $id;

    /** @var string */
    private $name;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     * @return TicketState
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
}

