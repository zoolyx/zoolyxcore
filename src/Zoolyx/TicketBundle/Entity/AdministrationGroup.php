<?php

namespace Zoolyx\TicketBundle\Entity;
use FOS\UserBundle\Model\User;

/**
 * Parameter
 */
class AdministrationGroup
{
    /** @var integer */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $email;

    /** @var  array */
    private $administrators;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string$name
     * @return AdministrationGroup
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param User $administrator
     * @return AdministrationGroup
     */
    public function addAdministrator(User $administrator) {
        $this->administrators[] = $administrator;
        return $this;
    }

    /**
     * @return array
     */
    public function getAdministrators() {
        return $this->administrators;
    }

}

