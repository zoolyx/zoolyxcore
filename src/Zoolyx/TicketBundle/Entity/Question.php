<?php

namespace Zoolyx\TicketBundle\Entity;

/**
 * Parameter
 */
class Question extends Ticket
{
    /** @var string  */
    private $type = '';

    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Question
     */
    public function setType($type) {
        $this->type = $type;
        return $this;
    }
}

