<?php
namespace Zoolyx\TicketBundle\Model\Ticket;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\TicketBundle\Entity\Ticket;

/**
 * @DI\Service("zoolyx_ticket.factory.ticket")
 */
class TicketFactory {

    /** @var TicketStateManager  */
    private $ticketStateManager;

    /** @var TicketWatchManager  */
    private $ticketWatchManager;

    /**
     * Constructor.
     *
     * @param TicketStateManager $ticketStateManager
     * @param TicketWatchManager $ticketWatchManager
     *
     * @DI\InjectParams({
     *  "ticketStateManager" = @DI\Inject("zoolyx_ticket.manager.ticket_state"),
     *  "ticketWatchManager" = @DI\Inject("zoolyx_ticket.manager.ticket_watch")
     * })
     */
    public function __construct(
        TicketStateManager $ticketStateManager,
        TicketWatchManager $ticketWatchManager)
    {
        $this->ticketStateManager = $ticketStateManager;
        $this->ticketWatchManager = $ticketWatchManager;
    }


    public function create(User $owner) {
        $ticket = new Ticket();
        $ticket->setCreatedOn(new \DateTime())->setOwner($owner);
        $this->ticketStateManager->setNew($ticket);
        $this->ticketWatchManager->addWatcher($ticket, $owner);
        return $ticket;
    }


} 