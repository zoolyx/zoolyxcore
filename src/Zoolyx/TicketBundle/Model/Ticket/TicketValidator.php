<?php
namespace Zoolyx\TicketBundle\Model\Ticket;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\TicketBundle\Entity\MailEvent;
use Zoolyx\TicketBundle\Entity\MessageEvent;
use Zoolyx\TicketBundle\Entity\Repository\WatchTicketRepository;
use Zoolyx\TicketBundle\Entity\Ticket;
use Zoolyx\TicketBundle\Entity\TicketEvent;
use Zoolyx\TicketBundle\Entity\WatchTicket;

/**
 * @DI\Service("zoolyx_ticket.validator.ticket")
 */
class TicketValidator {


    /** @var WatchTicketRepository  */
    private $watchTicketRepository;

    /**
     * Constructor.
     *
     * @param WatchTicketRepository $watchTicketRepository
     *
     * @DI\InjectParams({
     *  "watchTicketRepository" = @DI\Inject("zoolyx_ticket.repository.watch_ticket")
     * })
     */
    public function __construct(WatchTicketRepository $watchTicketRepository)
    {
        $this->watchTicketRepository = $watchTicketRepository;
    }


    public function hasContent(Ticket $ticket) {
        /** @var TicketEvent $event */
        foreach ($ticket->getEvents() as $event) {
            if ($event instanceof MessageEvent || $event instanceof MailEvent ) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param Ticket $ticket
     * @param User $user
     * @return bool
     */
    public function hasNewContent(Ticket $ticket, User $user) {
        /** @var WatchTicket $watchTicket */
        $watchTicket = $this->watchTicketRepository->findOneBy(array(
            'user' => $user->getId(),
            'ticket' => $ticket->getId()
        ));

        return $watchTicket && $watchTicket->hasNew();
    }

} 