<?php
namespace Zoolyx\TicketBundle\Model\Ticket;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\User;

/**
 * @DI\Service("zoolyx_ticket.validator.ticket_role")
 */
class TicketRoleValidator {

    public function canSeeInternal(User $user) {
        return
            $user->hasRole('ROLE_ADMINISTRATOR') ||
            $user->hasRole('ROLE_ADMIN') ||
            $user->hasRole('ROLE_SUPER_ADMIN');
    }

    public function hasAdministrationRights(User $user) {
        return
            $user->hasRole('ROLE_ADMINISTRATOR') ||
            $user->hasRole('ROLE_ADMIN') ||
            $user->hasRole('ROLE_SUPER_ADMIN');
    }

} 