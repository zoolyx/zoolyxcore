<?php
namespace Zoolyx\TicketBundle\Model\Ticket;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\TicketBundle\Entity\AddCopyEvent;
use Zoolyx\TicketBundle\Entity\AdministrationGroup;
use Zoolyx\TicketBundle\Entity\AssignGroupEvent;
use Zoolyx\TicketBundle\Entity\AssignUserEvent;
use Zoolyx\TicketBundle\Entity\Ticket;
use Zoolyx\TicketBundle\Entity\TicketCopy;

/**
 * @DI\Service("zoolyx_ticket.manager.ticket")
 */
class TicketManager
{
    /** @var TicketStateManager  */
    private $ticketStateManager;
    /** @var TicketWatchManager  */
    private $ticketWatchManager;

    /**
     * Constructor.
     *
     * @param TicketStateManager $ticketStateManager
     * @param TicketWatchManager $ticketWatchManager
     *
     * @DI\InjectParams({
     *  "ticketStateManager" = @DI\Inject("zoolyx_ticket.manager.ticket_state"),
     *  "ticketWatchManager" = @DI\Inject("zoolyx_ticket.manager.ticket_watch")
     * })
     */
    public function __construct(
        TicketStateManager $ticketStateManager,
        TicketWatchManager $ticketWatchManager)
    {
        $this->ticketStateManager = $ticketStateManager;
        $this->ticketWatchManager = $ticketWatchManager;
    }

    /**
     * @param Ticket $ticket
     * @param AdministrationGroup $administrationGroup
     * @param User $author
     */
    public function assignGroup(Ticket $ticket, AdministrationGroup $administrationGroup, User $author) {
        $ticket->assignGroup($administrationGroup)->assignAdministrator(null);

        $event = new AssignGroupEvent();
        $event->setAssignedTo($administrationGroup)
            ->setAuthor($author)
            ->setCreatedOn(new \DateTime())
            ->setTicket($ticket);
        $ticket->addEvent($event);
    }


    /**
     * @param Ticket $ticket
     * @param User $administrator
     * @return bool
     */
    public function isNewAdministrator(Ticket $ticket, User $administrator) {
        if (is_null($ticket->getAssignedAdministrator())) {
            return true;
        }
        return $ticket->getAssignedAdministrator()->getId() != $administrator->getId();
    }

    /**
     * @param Ticket $ticket
     * @param User $administrator
     * @param User $author
     */
    public function assignAdministrator(Ticket $ticket, User $administrator, User $author) {
        $ticket->assignAdministrator($administrator)->assignGroup(null);
        $event = new AssignUserEvent();
        $event->setAssignedTo($administrator)
            ->setAuthor($author)
            ->setCreatedOn(new \DateTime())
            ->setTicket($ticket);

        $ticket->addEvent($event);

        $this->ticketStateManager->setAssigned($ticket);

        $this->ticketWatchManager->addWatcher($ticket,$administrator, true);
    }

    /**
     * @param Ticket $ticket
     * @param TicketCopy $ticketCopy
     * @param User $author
     */
    public function addCopy(Ticket $ticket, TicketCopy $ticketCopy, User $author) {
        $ticket->addCopy($ticketCopy);
        $event = new addCopyEvent();
        $event
            ->setCopiedTo($ticketCopy->getEmail())
            ->setState(AddCopyEvent::STATE_ADDED)
            ->setAuthor($author)
            ->setCreatedOn(new \DateTime())
            ->setTicket($ticket);

        $ticket->addEvent($event);
    }

    /**
     * @param Ticket $ticket
     * @param TicketCopy $ticketCopy
     * @param User $author
     */
    public function removeCopy(Ticket $ticket, TicketCopy $ticketCopy, User $author) {
        $ticket->removeCopy($ticketCopy);
        $event = new addCopyEvent();
        $event
            ->setCopiedTo($ticketCopy->getEmail())
            ->setState(AddCopyEvent::STATE_REMOVED)
            ->setAuthor($author)
            ->setCreatedOn(new \DateTime())
            ->setTicket($ticket);

        $ticket->addEvent($event);
    }

} 