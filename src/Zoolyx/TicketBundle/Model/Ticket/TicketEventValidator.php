<?php
namespace Zoolyx\TicketBundle\Model\Ticket;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\TicketBundle\Entity\MailEvent;
use Zoolyx\TicketBundle\Entity\MessageEvent;
use Zoolyx\TicketBundle\Entity\QuestionEvent;
use Zoolyx\TicketBundle\Entity\TicketEvent;

/**
 * @DI\Service("zoolyx_ticket.validator.ticket_event")
 */
class TicketEventValidator {

    /**
     * @param TicketEvent $event
     * @return bool
     */
    public function isInternal(TicketEvent $event) {
        return !$this->isPublic($event);
    }

    /**
     * @param TicketEvent $event
     * @return bool
     */
    public function isPublic(TicketEvent $event) {
        return
            ($event instanceof MessageEvent && !$event->isInternal()) ||
            $event instanceof QuestionEvent ||
            $event instanceof MailEvent
            ;
    }


} 