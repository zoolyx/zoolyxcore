<?php
namespace Zoolyx\TicketBundle\Model\Ticket;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\TicketBundle\Entity\Repository\TicketStateRepository;
use Zoolyx\TicketBundle\Entity\StateEvent;
use Zoolyx\TicketBundle\Entity\Ticket;
use Zoolyx\TicketBundle\Entity\TicketState;

/**
 * @DI\Service("zoolyx_ticket.manager.ticket_state")
 */
class TicketStateManager
{
    /** @var TicketStateRepository  */
    private $ticketStateRepository;
    /** @var User */
    private $author = null;

    /**
     * Constructor.
     *
     * @param TicketStateRepository $ticketStateRepository
     * @param TokenStorage $tokenStorage
     *
     * @DI\InjectParams({
     *  "ticketStateRepository" = @DI\Inject("zoolyx_ticket.repository.ticket_state"),
     *  "tokenStorage" = @DI\Inject("security.token_storage")
     * })
     */
    public function __construct(TicketStateRepository $ticketStateRepository,
                                TokenStorage $tokenStorage)
    {
        $this->ticketStateRepository = $ticketStateRepository;
        if ($tokenStorage->getToken() && $tokenStorage->getToken()->getUser() instanceof User) {
            $this->author = $tokenStorage->getToken()->getUser();
        } else {
            $this->author = null;
        }
    }

    /**
     * @param Ticket $ticket
     */
    public function setNew(Ticket $ticket) {
        $this->setState($ticket, 'New');
    }

    /**
     * @param Ticket $ticket
     */
    public function setAssigned(Ticket $ticket) {
        $this->setState($ticket, 'Assigned');
    }

    /**
     * @param Ticket $ticket
     */
    public function setOpen(Ticket $ticket) {
        $this->setState($ticket, 'Open');
    }

    /**
     * @param Ticket $ticket
     */
    public function setClosed(Ticket $ticket) {
        $this->setState($ticket, 'Closed');
    }

    /**
     * @param Ticket $ticket
     * @param string $state
     */
    private function setState(Ticket $ticket, $state) {
        /** @var TicketState $ticketState */
        $ticketState = $this->ticketStateRepository->findOneBy(array('name'=>$state));
        $ticket->setState($ticketState);

        $event = new StateEvent();
        $event->setAuthor($this->author);
        $event->setTicketState($ticketState);
        $event->setCreatedOn(new \DateTime());
        $event->setTicket($ticket);
        $ticket->addEvent($event);
    }



} 