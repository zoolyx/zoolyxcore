<?php
namespace Zoolyx\TicketBundle\Model\Ticket;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\Owner;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\TicketBundle\Entity\Ticket;

/**
 * @DI\Service("zoolyx_ticket.manager.ticket_context")
 */
class TicketContextManager {

    /**
     * @param Ticket $ticket
     * @param Veterinary $veterinary
     */
    public function setVeterinary(Ticket $ticket, Veterinary $veterinary) {
        $ticket->setVeterinary($veterinary);
    }

    /**
     * @param Ticket $ticket
     * @param Owner $owner
     */
    public function setOwner(Ticket $ticket, Owner $owner) {
        $ticket->setContextOwner($owner);
    }

    /**
     * @param Ticket $ticket
     * @param User $user
     */
    public function setUser(Ticket $ticket, User $user) {
        $ticket->setUser($user);
    }

    /**
     * @param Ticket $ticket
     * @param Report $report
     */
    public function setReport(Ticket $ticket, Report $report) {
        $ticket->setReport($report);
    }

} 