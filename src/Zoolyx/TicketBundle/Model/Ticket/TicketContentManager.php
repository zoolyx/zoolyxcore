<?php
namespace Zoolyx\TicketBundle\Model\Ticket;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Zoolyx\AccountBundle\Entity\UserRegistrationRequest;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\TicketBundle\Entity\MailEvent;
use Zoolyx\TicketBundle\Entity\MessageEvent;
use Zoolyx\TicketBundle\Entity\QuestionEvent;
use Zoolyx\TicketBundle\Entity\RegistrationEvent;
use Zoolyx\TicketBundle\Entity\Ticket;
use Zoolyx\TicketBundle\Entity\TicketEvent;
use Zoolyx\TicketBundle\Model\Content\HyperLinker;

/**
 * @DI\Service("zoolyx_ticket.manager.ticket_content")
 */
class TicketContentManager {

    /** @var TicketManager  */
    private $ticketManager;
    /** @var TicketStateManager  */
    private $ticketStateManager;
    /** @var TicketWatchManager  */
    private $ticketWatchManager;
    /** @var TicketValidator  */
    private $ticketValidator;
    /** @var TicketRoleValidator  */
    private $ticketRoleValidator;
    /** @var string */
    private $attachmentsDir;
    /** @var HyperLinker */
    private $hyperLinker;

    /**
     * Constructor.
     *
     * @param TicketManager $ticketManager
     * @param TicketStateManager $ticketStateManager
     * @param TicketWatchManager $ticketWatchManager
     * @param TicketValidator $ticketValidator
     * @param TicketRoleValidator $ticketRoleValidator
     * @param string $attachmentsDir
     * @param HyperLinker $hyperLinker
     *
     * @DI\InjectParams({
     *  "ticketManager" = @DI\Inject("zoolyx_ticket.manager.ticket"),
     *  "ticketStateManager" = @DI\Inject("zoolyx_ticket.manager.ticket_state"),
     *  "ticketWatchManager" = @DI\Inject("zoolyx_ticket.manager.ticket_watch"),
     *  "ticketValidator" = @DI\Inject("zoolyx_ticket.validator.ticket"),
     *  "ticketRoleValidator" = @DI\Inject("zoolyx_ticket.validator.ticket_role"),
     *  "attachmentsDir" = @DI\Inject("%attachments_dir%"),
     *  "hyperLinker" = @DI\Inject("zoolyx_ticket.content.hyper_linker")
     * })
     */
    public function __construct(
        TicketManager $ticketManager,
        TicketStateManager $ticketStateManager,
        TicketWatchManager $ticketWatchManager,
        TicketValidator $ticketValidator,
        TicketRoleValidator $ticketRoleValidator,
        $attachmentsDir,
        HyperLinker $hyperLinker)
    {
        $this->ticketManager = $ticketManager;
        $this->ticketStateManager = $ticketStateManager;
        $this->ticketWatchManager = $ticketWatchManager;
        $this->ticketValidator = $ticketValidator;
        $this->ticketRoleValidator = $ticketRoleValidator;
        $this->attachmentsDir = $attachmentsDir;
        $this->hyperLinker = $hyperLinker;
    }


    /**
     * @param Ticket $ticket
     * @param QuestionEvent $event
     * @param User $author
     * @return QuestionEvent
     */
    public function addQuestion(Ticket $ticket, QuestionEvent $event, User $author)
    {

        $event
            ->setCreatedOn(new \DateTime())
            ->setAuthor($author)
            ->setTicket($ticket);
        $this->addContentEvent($ticket, $event);
        return $event;
    }

    /**
     * @param Ticket $ticket
     * @param MessageEvent $event
     * @param User $author
     * @return MessageEvent
     */
    public function addMessage(Ticket $ticket, MessageEvent $event, User $author)
    {
        if (
            is_null($ticket->getAssignedAdministrator()) &&
            $this->ticketValidator->hasContent($ticket) &&
            $this->ticketRoleValidator->hasAdministrationRights($author))
        {
            //$this->assignAdministrator($author);
            $this->ticketStateManager->setOpen($ticket);
        }

        $this->hyperLinker->update($event);

        $event
            ->setCreatedOn(new \DateTime())
            ->setAuthor($author)
            ->setTicket($ticket);
        $this->addContentEvent($ticket, $event);
        return $event;
    }

    public function handleAttachments(TicketEvent $event) {
        /** @var MessageEvent $event */
        $files = $event->getAttachments();
        $baseFilename = 'ticket_'.$event->getTicket()->getId().'_'.date("YmdHis",time()).'_';
        $fileNames = array();

        /** @var UploadedFile $file */
        foreach ($files as $index => $file) {
            $filename = $baseFilename.$index.'.'.$file->guessExtension();

            try {
                $file->move(
                    $this->attachmentsDir,
                    $filename
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }
            $fileNames[] = $filename;
            $event->setAttachment(implode(",",$fileNames));
        }
    }

    /**
     * @param Ticket $ticket
     * @param string $fileId
     * @param User $author
     * @return MailEvent
     */
    public function addMail(Ticket $ticket, $fileId, User $author) {
        $event = new MailEvent();
        $event->setFileId($fileId)
            ->setCreatedOn(new \DateTime())
            ->setAuthor($author)
            ->setTicket($ticket);
        $this->addContentEvent($ticket, $event);
        return $event;
    }

    /**
     * @param Ticket $ticket
     * @param UserRegistrationRequest $userRegistrationRequest
     * @param User $author
     * @return RegistrationEvent
     */
    public function addUserRegistration(Ticket $ticket, UserRegistrationRequest $userRegistrationRequest, User $author) {
        $event = new RegistrationEvent();
        $event
            ->setRequest($userRegistrationRequest)
            ->setCreatedOn(new \DateTime())
            ->setAuthor($author)
            ->setTicket($ticket);
        $this->addContentEvent($ticket, $event);
        return $event;
    }

    /**
     * @param Ticket $ticket
     * @param TicketEvent $event
     * @return TicketEvent
     */
    private function addContentEvent(Ticket $ticket, TicketEvent $event)
    {
        $ticket
            ->addEvent($event)
            ->setLastUpdateOn($event->getCreatedOn())
        ;

        if ($ticket->getState()->getName() == "Closed") {
            $this->ticketStateManager->setOpen($ticket);
        }

        if (is_null($ticket->getAssignedAdministrator()) and $event->getAuthor()->hasRole('ROLE_ADMINISTRATOR')) {
            $this->ticketManager->assignAdministrator($ticket,$event->getAuthor(),$event->getAuthor());
        }

        $this->ticketWatchManager->addWatcher($ticket, $event->getAuthor());
        $this->ticketWatchManager->newEvent($ticket, $event);

        return $event;
    }

} 