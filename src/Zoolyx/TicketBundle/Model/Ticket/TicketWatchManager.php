<?php
namespace Zoolyx\TicketBundle\Model\Ticket;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\TicketBundle\Entity\MailEvent;
use Zoolyx\TicketBundle\Entity\MessageEvent;
use Zoolyx\TicketBundle\Entity\Ticket;
use Zoolyx\TicketBundle\Entity\TicketEvent;
use Zoolyx\TicketBundle\Entity\WatchTicket;

/**
 * @DI\Service("zoolyx_ticket.manager.ticket_watch")
 */
class TicketWatchManager {

    /** @var TicketRoleValidator  */
    private $ticketRoleValidator;
    /** @var TicketEventValidator  */
    private $ticketEventValidator;

    /**
     * Constructor.
     *
     * @param TicketRoleValidator $ticketRoleValidator
     * @param TicketEventValidator $ticketEventValidator
     *
     * @DI\InjectParams({
     *  "ticketRoleValidator" = @DI\Inject("zoolyx_ticket.validator.ticket_role"),
     *  "ticketEventValidator" = @DI\Inject("zoolyx_ticket.validator.ticket_event")
     * })
     */
    public function __construct(
        TicketRoleValidator $ticketRoleValidator,
        TicketEventValidator $ticketEventValidator)
    {
        $this->ticketRoleValidator = $ticketRoleValidator;
        $this->ticketEventValidator = $ticketEventValidator;
    }

    private function findWatcher(Ticket $ticket, User $user) {
        /** @var WatchTicket $watcher */
        foreach ($ticket->getWatchers() as $watcher) {
            if ($watcher->getUser()->getId() == $user->getId()) {
                return $watcher;
            }
        }
        return null;
    }
    public function addWatcher(Ticket $ticket, User $user, $hasNew = false) {
        $watcher = $this->findWatcher($ticket, $user);
        if (is_null($watcher)) {
            $watcher = new WatchTicket();
            $watcher->setUser($user)->setTicket($ticket)->setHasNew($hasNew);
            $ticket->addWatcher($watcher);
        }
        return $watcher;
    }

    public function watchTicket(Ticket $ticket, User $user) {
        /** @var WatchTicket $watcher */
        $watcher = $this->findWatcher($ticket, $user);
        if ($watcher) {
            $watcher->setHasNew(false);
            /** @var TicketEvent $event */
            foreach ($ticket->getEvents() as $event) {
                if ($event instanceof MessageEvent || $event instanceof MailEvent){
                    $watcher->setLastMessageEvent($event);
                }
            }
        }
    }

    public function newEvent(Ticket $ticket, TicketEvent $event) {
        /** @var WatchTicket $watcher */
        foreach ($ticket->getWatchers() as $watcher) {
            if ($watcher->getUser()->getId() == $event->getAuthor()->getId()) {
                $watcher->setLastMessageEvent($event)->setHasNew(false);
            } else {
                if ($this->ticketEventValidator->isPublic($event)) {
                    $watcher->setHasNew(true);
                    /** @todo launch event to inform this watcher via mail */
                    //$this->eventDispatcher->dispatch(Events::UPDATED_TICKET, new UpdatedTicketEvent($ticket, $watcher->getUser()));
                } else {
                    if ($this->ticketRoleValidator->canSeeInternal($watcher->getUser())) {
                        $watcher->setHasNew(true);
                        /** @todo launch event to inform this watcher via mail */
                        //$this->eventDispatcher->dispatch(Events::NEW_TICKET, new NewTicketEvent($ticket));
                    }
                }
            }
        }
    }

} 