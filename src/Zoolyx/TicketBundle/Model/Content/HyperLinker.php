<?php
namespace Zoolyx\TicketBundle\Model\Content;

use JMS\DiExtraBundle\Annotation as DI;
use Zoolyx\TicketBundle\Entity\MessageEvent;

/**
 * @DI\Service("zoolyx_ticket.content.hyper_linker")
 */
class HyperLinker {


    /**
     * @param MessageEvent $event
     */
    public function update(MessageEvent $event)
    {
        $content = $event->getContent();

        //$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        $reg_exUrl = "/((http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3})([\s.,;\?\!]|$)/";

        if (preg_match_all($reg_exUrl, $content, $matches)) {
            echo('found a match');
            foreach ($matches[0] as $i => $match) {
                $content = str_replace(
                    $match,
                    '<a href="'.$matches[1][$i].'" rel="nofollow" target="new">'.$matches[1][$i].'</a>'.$matches[3][$i],
                    $content
                );
            }

            $event->setContent($content);
        }
    }

} 