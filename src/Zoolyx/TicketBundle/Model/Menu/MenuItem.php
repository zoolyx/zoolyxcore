<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 16.01.19
 * Time: 15:56
 */

namespace Zoolyx\TicketBundle\Model\Menu;


class MenuItem {

    const MENU_MY_TICKETS = 1;
    const MENU_GROUP_TICKETS = 2;
    const MENU_ALL_TICKETS = 3;
    const MENU_NEW_TICKET = 4;

    public $selected = 0;
    public $groupId = 0;

    public function __construct($entry, $groupId = null) {
        $this->selected = $entry;
        $this->groupId = $groupId;
    }

} 