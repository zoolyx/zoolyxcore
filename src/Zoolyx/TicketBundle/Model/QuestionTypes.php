<?php
namespace Zoolyx\TicketBundle\Model;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("zoolyx_ticket.question_type")
 */
class QuestionTypes {

    static public function types() {
        return array(
            'wet' => 'Wetenschappelijk',
            'adm' => 'Administratief'
        );
    }

} 