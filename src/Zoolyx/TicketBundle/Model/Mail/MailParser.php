<?php
    namespace Zoolyx\TicketBundle\Model\Mail;

use JMS\DiExtraBundle\Annotation as DI;
use PhpMimeMailParser\Parser;

/**
 * @DI\Service("zoolyx_ticket.mail_parser")
 */
class MailParser {

    private $mailDir = '';

    /** @var Parser */
    private $parser;

    private $content = null;
    private $isHtml = false;


    /**
     * @param string $mailDir
     *
     * @DI\InjectParams({
     *     "mailDir" = @DI\Inject("%mail_dir%")
     * })
     */
    public function __construct($mailDir) {
        $this->mailDir = $mailDir;
        $this->parser = new Parser();
    }

    public function loadParser($fileId) {
        $this->parser = new Parser();
        $filename = $this->mailDir . '/' . $fileId . ".txt";
        $this->parser->setText(file_get_contents($filename));
        $this->loadContent();
        return $this;
    }

    public function from() {
        return $this->parser->getHeader('from');
    }
    public function subject() {
        return $this->parser->getHeader('subject');
    }

    public function loadContent() {
        $this->content = $this->parser->getMessageBody('htmlEmbedded');
        $this->isHtml = true;
        if ($this->content == '') {
            $this->content = $this->parser->getMessageBody('text');
            $this->isHtml = false;
        }
    }
    public function isHtml() {
        return $this->isHtml;
    }
    public function content() {
        return is_null($this->content) ? $this->parser->getMessageBody('htmlEmbedded') : $this->content;
    }

    public function attachments() {
        return $this->parser->getAttachments();
    }
} 