<?php
namespace Zoolyx\TicketBundle\Model;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("zoolyx_ticket.ticket_states")
 */
class TicketStates {

    private $states = array(
        1 => 'New',
        2 => 'Assigned',
        3 => 'Open',
        4 => 'closed'
    );

    public function states() {
        return $this->states;
    }
    public function getState($stateName) {
        foreach ($this->states as $index=>$state) {
            if ($state == $stateName) {
                return $index;
            }
        }
        return 0;
    }

} 