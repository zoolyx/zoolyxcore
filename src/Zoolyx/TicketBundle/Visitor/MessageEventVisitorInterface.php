<?php
/**
 * Created by PhpStorm.
 * User: Mihai
 * Date: 07/09/14
 * Time: 12:57
 */

namespace Zoolyx\TicketBundle\Visitor;


use Zoolyx\TicketBundle\Entity\MessageEvent;

interface MessageEventVisitorInterface
{
    /**
     * @param MessageEvent $messageEvent
     **/
    public function visitMessageEvent(MessageEvent $messageEvent);
}
