<?php
/**
 * Created by PhpStorm.
 * User: Mihai
 * Date: 07/09/14
 * Time: 12:57
 */

namespace Zoolyx\TicketBundle\Visitor;


use Zoolyx\TicketBundle\Entity\AssignUserEvent;

interface AssignUserEventVisitorInterface
{
    /**
     * @param AssignUserEvent $assignUserEvent
     **/
    public function visitAssignUserEvent(AssignUserEvent $assignUserEvent);
}
