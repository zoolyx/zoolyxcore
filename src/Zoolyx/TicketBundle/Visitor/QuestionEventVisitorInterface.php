<?php
/**
 * Created by PhpStorm.
 * User: Mihai
 * Date: 07/09/14
 * Time: 12:57
 */

namespace Zoolyx\TicketBundle\Visitor;

use Zoolyx\TicketBundle\Entity\QuestionEvent;

interface QuestionEventVisitorInterface
{
    /**
     * @param QuestionEvent $questionEvent
     **/
    public function visitQuestionEvent(QuestionEvent $questionEvent);
}
