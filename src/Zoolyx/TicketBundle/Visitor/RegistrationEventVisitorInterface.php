<?php
/**
 * Created by PhpStorm.
 * User: Mihai
 * Date: 07/09/14
 * Time: 12:57
 */

namespace Zoolyx\TicketBundle\Visitor;

use Zoolyx\TicketBundle\Entity\RegistrationEvent;

interface RegistrationEventVisitorInterface
{
    /**
     * @param RegistrationEvent $registrationEvent
     **/
    public function visitRegistrationEvent(RegistrationEvent $registrationEvent);
}
