<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 9/2/14
 * Time: 7:02 PM
 */

namespace Zoolyx\TicketBundle\Visitor\TicketEvent;


use Zoolyx\TicketBundle\Entity\AddCopyEvent;
use Zoolyx\TicketBundle\Entity\AssignGroupEvent;
use Zoolyx\TicketBundle\Entity\AssignUserEvent;
use Zoolyx\TicketBundle\Entity\MailEvent;
use Zoolyx\TicketBundle\Entity\MessageEvent;
use Zoolyx\TicketBundle\Entity\QuestionEvent;
use Zoolyx\TicketBundle\Entity\RegistrationEvent;
use Zoolyx\TicketBundle\Entity\StateEvent;
use Zoolyx\TicketBundle\Visitor\AddCopyEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\AssignGroupEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\AssignUserEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\MailEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\MessageEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\QuestionEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\RegistrationEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\StateEventVisitorInterface;
use Zoolyx\TicketBundle\Visitor\VisitorInterface;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("zoolyx_ticket.visitor.show_to_user")
 */
class ShowToUserVisitor implements
    VisitorInterface,
    StateEventVisitorInterface,
    QuestionEventVisitorInterface,
    MessageEventVisitorInterface,
    MailEventVisitorInterface,
    RegistrationEventVisitorInterface,
    AssignUserEventVisitorInterface,
    AssignGroupEventVisitorInterface,
    AddCopyEventVisitorInterface
{


    /**
     * {@inheritdoc}
     */
    public function visitStateEvent(StateEvent $stateEvent){
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function visitQuestionEvent(QuestionEvent $questionEvent){
        return !$questionEvent->isInternal();
    }

    /**
     * {@inheritdoc}
     */
    public function visitMessageEvent(MessageEvent $messageEvent){
        return !$messageEvent->isInternal();
    }

    /**
     * {@inheritdoc}
     */
    public function visitMailEvent(MailEvent $mailEvent){
        return true;
    }

    public function visitRegistrationEvent(RegistrationEvent $registrationEvent) {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function visitAssignUserEvent(AssignUserEvent $assignUserEvent) {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function visitAssignGroupEvent(AssignGroupEvent $assignGroupEvent) {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function visitAddCopyEvent(AddCopyEvent $addCopyEvent) {
        return false;
    }


}
