<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 13.03.17
 * Time: 16:28
 */

namespace Zoolyx\TicketBundle\Command;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zoolyx\TicketBundle\Entity\MailEvent;
use Zoolyx\TicketBundle\Entity\MessageEvent;
use Zoolyx\TicketBundle\Entity\QuestionEvent;
use Zoolyx\TicketBundle\Entity\Repository\TicketRepository;
use Zoolyx\TicketBundle\Entity\Ticket;

class SetLastUpdatedCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:ticket:last-update')

            // the short description shown while running "php bin/console list"
            ->setDescription('Update tickets.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Update tickets.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var TicketRepository $ticketRepository */
        $ticketRepository = $this->getContainer()->get('zoolyx_ticket.repository.ticket');
        /** @var ObjectManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');

        $tickets = $ticketRepository->findAll();

        /** @var Ticket $ticket */
        foreach ($tickets as $ticket)
        {
            $lastUpdate = $ticket->getCreatedOn();
            $events = $ticket->getEvents();
            foreach ($events as $event)
            {
                if ($event instanceof MessageEvent || $event instanceof QuestionEvent || $event instanceof MailEvent)
                {
                    if ($event->getCreatedOn()->format("Y/m/d H:i:s") > $lastUpdate->format("Y/m/d H:i:s"))
                    {
                        $lastUpdate = $event->getCreatedOn();
                    }
                }
            }
            $ticket->setLastUpdateOn($lastUpdate);
            $entityManager->persist($ticket);
        }

        $entityManager->flush();

    }

}