<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 13.03.17
 * Time: 16:28
 */

namespace Zoolyx\TicketBundle\Command;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zoolyx\CoreBundle\Entity\Owner;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\Repository\OwnerRepository;
use Zoolyx\CoreBundle\Entity\Repository\ReportRepository;
use Zoolyx\CoreBundle\Entity\Repository\UserRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Event\NewAccountEvent;
use Zoolyx\CoreBundle\Events;
use Zoolyx\TicketBundle\Entity\Repository\TicketRepository;
use Zoolyx\TicketBundle\Entity\Ticket;
use PhpMimeMailParser\Parser;
use Zoolyx\TicketBundle\Event\NewMailTicketEvent;
use Zoolyx\TicketBundle\Event\UpdatedTicketEvent;
use Zoolyx\TicketBundle\Model\Ticket\TicketContentManager;
use Zoolyx\TicketBundle\Model\Ticket\TicketContextManager;
use Zoolyx\TicketBundle\Model\Ticket\TicketFactory;

class ImportMailCommand extends ContainerAwareCommand {

    /*
     * configuration for postfix:
     * https://itknowledgeexchange.techtarget.com/security-admin/parsing-e-mails-via-postfix-and-php/
     */

    /*
     * installation of mime mail parser
     * https://github.com/php-mime-mail-parser/php-mime-mail-parser
     */

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:import-mail')

            // the short description shown while running "php bin/console list"
            ->setDescription('Loads new mails.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command will load new mails...')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $fd = fopen("php://stdin", "r");
        $email = "";
        while(!feof($fd))
        {
            $email .= fread($fd, 1024);
        }
        fclose($fd);

        //$email = file_get_contents('/opt/repositories/zoolyx/ftp/mail/rfc2822complianceException.txt');
        //$email = file_get_contents('/opt/repositories/zoolyx_orig/ftp/mail/from_bart.txt');

        $tokenGenerator = $this->getContainer()->get('zoolyx_account.generator.token');
        $fileId = time() . "_" . $tokenGenerator->get(5);
        $path = $this->getContainer()->getParameter('mail_dir');
        $filename = $path . "/" . $fileId . ".txt";
        file_put_contents($filename,$email);
        chmod($filename, 0664);

        $parser = new Parser();
        $parser->setText($email);

        $addressesFrom = $parser->getAddresses('from');
        $fromAddress = isset($addressesFrom[0]["address"]) ? $addressesFrom[0]["address"] : null;

        if ( $parser->getHeader('reply-to') ) {
          $fromAddress = $parser->getHeader('reply-to');
        }
        
        //$addressesTo = $parser->getAddresses('to');
        //$toAddress = isset($addressesTo[0]["address"]) ? $addressesTo[0]["address"] : null;
 
        /** @var ObjectManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var UserRepository $userRepository */
        $userRepository = $this->getContainer()->get('zoolyx_core.repository.user');
        /** @var User $user */
        $user = $fromAddress ? $userRepository->findOneBy(array('email'=>$fromAddress)) : null;
        if (!$user) {
            $user = new User();
            $user
                ->setUsername($fromAddress)
                ->setUsernameCanonical($fromAddress)
                ->setEmail($fromAddress)
                ->setEmailCanonical($fromAddress)
                ->setEnabled(false)
                ->setPassword('temp')
                ->setConfirmationToken($tokenGenerator->get(32))
                ->setPasswordRequestedAt(new \DateTime())
                ->addRole('ROLE_USER');
            ;
            $entityManager->persist($user);
            $entityManager->flush();

            /** @var EventDispatcherInterface */
            $eventDispatcher = $this->getContainer()->get('event_dispatcher');
            $eventDispatcher->dispatch(Events::NEW_ACCOUNT, new NewAccountEvent($user, NewAccountEvent::TRIGGER_MAIL_RECEIVED));
        }

        $ticket = null;
        /** @var Report $report */
        $report = null;

        /** @var TicketRepository $ticketRepository */
        $ticketRepository =$this->getContainer()->get('zoolyx_ticket.repository.ticket');

        // find ticket via explicit ticket number in mail subject
        $subject = $parser->getHeader('subject');
        preg_match('/\[ticket #([0-9]*)\]/', $subject, $matches);
        $ticketId = isset($matches[1]) ? $matches[1] : null;
        if ($ticketId) {
            $ticket = $ticketRepository->find($ticketId);
        }

        // find ticket via report requestId in mail subject
        if (!$ticket) {
            preg_match('/\[request #(.*)\]/', $subject, $matches);
            $reportRequestId = isset($matches[1]) ? $matches[1] : null;
            /** @var ReportRepository $reportRepository */
            $reportRepository = $this->getContainer()->get('zoolyx_core.repository.report');
            $report = $reportRepository->findOneBy(array('requestId'=>$reportRequestId));
            if ($report) {
                /** @var Ticket $ticket */
                $ticket = $ticketRepository->findOneBy(array('report'=>$report, 'user'=>$user));
            }
        }

        $ticketIsNew = false;
        //no pointer to an existing ticket, let's create a new one
        if (!$ticket) {
            /** @var TicketFactory $ticketFactory */
            $ticketFactory = $this->getContainer()->get('zoolyx_ticket.factory.ticket');
            $ticket = $ticketFactory->create($user);
            $ticketIsNew = true;
        }

        /** @var TicketContentManager $ticketContentManager */
        $ticketContentManager = $this->getContainer()->get('zoolyx_ticket.manager.ticket_content');
        $mailEvent = $ticketContentManager->addMail($ticket, $fileId, $user);

        /** @var TicketContextManager $ticketContextManager */
        $ticketContextManager = $this->getContainer()->get('zoolyx_ticket.manager.ticket_context');

        if ($report) {
            $ticketContextManager->setReport($ticket, $report);
        }
        if ($user->hasRole('ROLE_VETERINARY')) {
            /** @var VeterinaryRepository $veterinaryRepository */
            $veterinaryRepository = $this->getContainer()->get('zoolyx_core.repository.veterinary');
            /** @var Veterinary $veterinary */
            $veterinary = $veterinaryRepository->findOneBy(array('account'=>$user->getId()));
            if ($veterinary) {
                $ticketContextManager->setVeterinary($ticket, $veterinary);
            }
        }
        if ($user->hasRole('ROLE_OWNER')) {
            /** @var OwnerRepository $ownerRepository */
            $ownerRepository = $this->getContainer()->get('zoolyx_core.repository.owner');
            /** @var Owner $owner */
            $owner = $ownerRepository->findOneBy(array('account'=>$user->getId()));
            if ($owner) {
                $ticketContextManager->setOwner($ticket, $owner);
            }
        }

        $entityManager->persist($ticket);
        $entityManager->flush();

        /** @var EventDispatcherInterface $eventDispatcher */
        $eventDispatcher = $this->getContainer()->get('event_dispatcher');
        if ($ticketIsNew) {
            $eventDispatcher->dispatch(Events::NEW_MAIL_TICKET, new NewMailTicketEvent($ticket, $user, $parser->getHeader('subject'), $parser->getMessageBody('text')));
        } else {
            $eventDispatcher->dispatch(Events::UPDATED_TICKET, new UpdatedTicketEvent($mailEvent));

        }
    }

}