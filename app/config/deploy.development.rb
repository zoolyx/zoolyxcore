# development on mdclab
domain = dev.mdclab.be
#155.120.1.25

set :application, "zoolyxcore"
set :domain,      domain
set :deploy_to,   "/opt/repositories/zoolyx.be"
set :branch, "new_xsl"
set :app_path,    "app"

set :repository,  "git@bitbucket.org:zoolyx/zoolyxcore.git"
set :scm,         :git

set :user, "root"
set :shared_files, ["app/config/parameters.yml", "app/bootstrap.php.cache", "web/report", "web/wordpress"]
set :shared_children, [app_path + "/logs", app_path + "/jwt", "vendor"]
set :writable_dirs, ["app/cache", "app/logs"]
set :webserver_user, "www-data"
set :permission_method, :acl
set :use_set_permissions, true
set :use_composer, true
set :composer_options, "--verbose --optimize-autoloader"
set :dump_assetic_assets, false
set :model_manager, "doctrine"
set :use_sudo, false
set :clear_controllers, true

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain, :primary => true       # This may be the same as your `Web` server

set  :keep_releases,  3

# Be more verbose by uncommenting the following line
logger.level = Logger::MAX_LEVEL