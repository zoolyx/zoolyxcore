<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180228143145 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FCB816BAE');
        $this->addSql('DROP INDEX UNIQ_B6BD307FCB816BAE ON message');
        $this->addSql('ALTER TABLE message ADD discr VARCHAR(16) NOT NULL, CHANGE ticketmessage_id messageEvent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F6C4151AC FOREIGN KEY (messageEvent_id) REFERENCES ticket_event (id)');
        $this->addSql('CREATE INDEX IDX_B6BD307F6C4151AC ON message (messageEvent_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F6C4151AC');
        $this->addSql('DROP INDEX IDX_B6BD307F6C4151AC ON message');
        $this->addSql('ALTER TABLE message DROP discr, CHANGE messageevent_id ticketMessage_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FCB816BAE FOREIGN KEY (ticketMessage_id) REFERENCES ticket_event (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B6BD307FCB816BAE ON message (ticketMessage_id)');
    }
}
