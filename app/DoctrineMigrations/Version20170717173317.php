<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170717173317 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE owner_detail ADD account_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE owner_detail ADD CONSTRAINT FK_8BFC53CD9B6B5FBA FOREIGN KEY (account_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_8BFC53CD9B6B5FBA ON owner_detail (account_id)');
        $this->addSql('ALTER TABLE veterinary DROP FOREIGN KEY FK_C414EC879B6B5FBA');
        $this->addSql('DROP INDEX idx_c414ec879b6b5fba ON veterinary');
        $this->addSql('CREATE INDEX IDX_8B49EF579B6B5FBA ON veterinary (account_id)');
        $this->addSql('ALTER TABLE veterinary ADD CONSTRAINT FK_C414EC879B6B5FBA FOREIGN KEY (account_id) REFERENCES fos_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE owner_detail DROP FOREIGN KEY FK_8BFC53CD9B6B5FBA');
        $this->addSql('DROP INDEX IDX_8BFC53CD9B6B5FBA ON owner_detail');
        $this->addSql('ALTER TABLE owner_detail DROP account_id');
        $this->addSql('ALTER TABLE veterinary DROP FOREIGN KEY FK_8B49EF579B6B5FBA');
        $this->addSql('DROP INDEX idx_8b49ef579b6b5fba ON veterinary');
        $this->addSql('CREATE INDEX IDX_C414EC879B6B5FBA ON veterinary (account_id)');
        $this->addSql('ALTER TABLE veterinary ADD CONSTRAINT FK_8B49EF579B6B5FBA FOREIGN KEY (account_id) REFERENCES fos_user (id)');
    }
}
