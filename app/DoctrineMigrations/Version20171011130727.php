<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171011130727 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE parameter_profile_price DROP FOREIGN KEY FK_417127C13B025C87');
        $this->addSql('DROP INDEX IDX_417127C13B025C87 ON parameter_profile_price');
        $this->addSql('ALTER TABLE parameter_profile_price ADD billing VARCHAR(16) NOT NULL, DROP billing_id');
        $this->addSql('ALTER TABLE owner DROP FOREIGN KEY FK_8BFC53CD7E3C61F9');
        $this->addSql('ALTER TABLE owner DROP FOREIGN KEY FK_8BFC53CD9B6B5FBA');
        $this->addSql('DROP INDEX idx_8bfc53cd7e3c61f9 ON owner');
        $this->addSql('CREATE INDEX IDX_CF60E67C7E3C61F9 ON owner (owner_id)');
        $this->addSql('DROP INDEX idx_8bfc53cd9b6b5fba ON owner');
        $this->addSql('CREATE INDEX IDX_CF60E67C9B6B5FBA ON owner (account_id)');
        $this->addSql('ALTER TABLE owner ADD CONSTRAINT FK_8BFC53CD7E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE owner ADD CONSTRAINT FK_8BFC53CD9B6B5FBA FOREIGN KEY (account_id) REFERENCES fos_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE owner DROP FOREIGN KEY FK_CF60E67C7E3C61F9');
        $this->addSql('ALTER TABLE owner DROP FOREIGN KEY FK_CF60E67C9B6B5FBA');
        $this->addSql('DROP INDEX idx_cf60e67c7e3c61f9 ON owner');
        $this->addSql('CREATE INDEX IDX_8BFC53CD7E3C61F9 ON owner (owner_id)');
        $this->addSql('DROP INDEX idx_cf60e67c9b6b5fba ON owner');
        $this->addSql('CREATE INDEX IDX_8BFC53CD9B6B5FBA ON owner (account_id)');
        $this->addSql('ALTER TABLE owner ADD CONSTRAINT FK_CF60E67C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE owner ADD CONSTRAINT FK_CF60E67C9B6B5FBA FOREIGN KEY (account_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE parameter_profile_price ADD billing_id INT DEFAULT NULL, DROP billing');
        $this->addSql('ALTER TABLE parameter_profile_price ADD CONSTRAINT FK_417127C13B025C87 FOREIGN KEY (billing_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_417127C13B025C87 ON parameter_profile_price (billing_id)');
    }
}
