<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180705124238 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ticket_event ADD account_id INT DEFAULT NULL, ADD order_number VARCHAR(8) DEFAULT NULL');
        $this->addSql('ALTER TABLE ticket_event ADD CONSTRAINT FK_139E692C9B6B5FBA FOREIGN KEY (account_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_139E692C9B6B5FBA ON ticket_event (account_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ticket_event DROP FOREIGN KEY FK_139E692C9B6B5FBA');
        $this->addSql('DROP INDEX IDX_139E692C9B6B5FBA ON ticket_event');
        $this->addSql('ALTER TABLE ticket_event DROP account_id, DROP order_number');
    }
}
