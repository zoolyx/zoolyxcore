<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181212144540 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE watch_ticket DROP INDEX UNIQ_D0E0513F700047D2, ADD INDEX IDX_D0E0513F700047D2 (ticket_id)');
        $this->addSql('ALTER TABLE watch_ticket DROP INDEX UNIQ_D0E0513FF49E140A, ADD INDEX IDX_D0E0513FF49E140A (ticket_event_id)');
        $this->addSql('ALTER TABLE watch_ticket DROP FOREIGN KEY FK_D0E0513FF49E140A');
        $this->addSql('ALTER TABLE watch_ticket DROP FOREIGN KEY FK_D0E0513F700047D2');
        $this->addSql('ALTER TABLE watch_ticket ADD CONSTRAINT FK_D0E0513FF49E140A FOREIGN KEY (ticket_event_id) REFERENCES ticket_event (id)');
        $this->addSql('ALTER TABLE watch_ticket ADD CONSTRAINT FK_D0E0513F700047D2 FOREIGN KEY (ticket_id) REFERENCES ticket (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE watch_ticket DROP INDEX IDX_D0E0513F700047D2, ADD UNIQUE INDEX UNIQ_D0E0513F700047D2 (ticket_id)');
        $this->addSql('ALTER TABLE watch_ticket DROP INDEX IDX_D0E0513FF49E140A, ADD UNIQUE INDEX UNIQ_D0E0513FF49E140A (ticket_event_id)');
        $this->addSql('ALTER TABLE watch_ticket DROP FOREIGN KEY FK_D0E0513F700047D2');
        $this->addSql('ALTER TABLE watch_ticket DROP FOREIGN KEY FK_D0E0513FF49E140A');
        $this->addSql('ALTER TABLE watch_ticket ADD CONSTRAINT FK_D0E0513F700047D2 FOREIGN KEY (ticket_id) REFERENCES ticket (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE watch_ticket ADD CONSTRAINT FK_D0E0513FF49E140A FOREIGN KEY (ticket_event_id) REFERENCES ticket_event (id) ON DELETE CASCADE');
    }
}
