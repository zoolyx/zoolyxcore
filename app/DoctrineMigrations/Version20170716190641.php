<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170716190641 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE reference_range (id INT AUTO_INCREMENT NOT NULL, parameter_base_id INT DEFAULT NULL, species_code VARCHAR(2) NOT NULL, to_age VARCHAR(16) DEFAULT NULL, low DOUBLE PRECISION DEFAULT NULL, high DOUBLE PRECISION DEFAULT NULL, INDEX IDX_45263A59A33963C1 (parameter_base_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parameter_profile_parameter (parameter_profile_id INT NOT NULL, parameter_base_id INT NOT NULL, INDEX IDX_F69C63D2E96C94B5 (parameter_profile_id), INDEX IDX_F69C63D2A33963C1 (parameter_base_id), PRIMARY KEY(parameter_profile_id, parameter_base_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parameter_base (id INT AUTO_INCREMENT NOT NULL, parameter_id VARCHAR(64) NOT NULL, unit VARCHAR(16) DEFAULT NULL, alt_unit VARCHAR(16) DEFAULT NULL, conversion_factor VARCHAR(16) DEFAULT NULL, decimals VARCHAR(4) DEFAULT NULL, delay VARCHAR(16) DEFAULT NULL, is_title SMALLINT DEFAULT NULL, is_cumulative SMALLINT NOT NULL, loinc VARCHAR(16) DEFAULT NULL, link VARCHAR(256) DEFAULT NULL, UNIQUE INDEX UNIQ_6FBAFD647C56DBD6 (parameter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reference_range ADD CONSTRAINT FK_45263A59A33963C1 FOREIGN KEY (parameter_base_id) REFERENCES parameter_base (id)');
        $this->addSql('ALTER TABLE parameter_profile_parameter ADD CONSTRAINT FK_F69C63D2E96C94B5 FOREIGN KEY (parameter_profile_id) REFERENCES parameter_profile (id)');
        $this->addSql('ALTER TABLE parameter_profile_parameter ADD CONSTRAINT FK_F69C63D2A33963C1 FOREIGN KEY (parameter_base_id) REFERENCES parameter_base (id)');
        $this->addSql('ALTER TABLE description ADD parameter_base_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE description ADD CONSTRAINT FK_6DE44026A33963C1 FOREIGN KEY (parameter_base_id) REFERENCES parameter_base (id)');
        $this->addSql('CREATE INDEX IDX_6DE44026A33963C1 ON description (parameter_base_id)');
        $this->addSql('ALTER TABLE parameter ADD parameter_base_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE parameter ADD CONSTRAINT FK_2A979110A33963C1 FOREIGN KEY (parameter_base_id) REFERENCES parameter_base (id)');
        $this->addSql('CREATE INDEX IDX_2A979110A33963C1 ON parameter (parameter_base_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE description DROP FOREIGN KEY FK_6DE44026A33963C1');
        $this->addSql('ALTER TABLE reference_range DROP FOREIGN KEY FK_45263A59A33963C1');
        $this->addSql('ALTER TABLE parameter_profile_parameter DROP FOREIGN KEY FK_F69C63D2A33963C1');
        $this->addSql('ALTER TABLE parameter DROP FOREIGN KEY FK_2A979110A33963C1');
        $this->addSql('DROP TABLE reference_range');
        $this->addSql('DROP TABLE parameter_profile_parameter');
        $this->addSql('DROP TABLE parameter_base');
        $this->addSql('DROP INDEX IDX_6DE44026A33963C1 ON description');
        $this->addSql('ALTER TABLE description DROP parameter_base_id');
        $this->addSql('DROP INDEX IDX_2A979110A33963C1 ON parameter');
        $this->addSql('ALTER TABLE parameter DROP parameter_base_id');
    }
}
