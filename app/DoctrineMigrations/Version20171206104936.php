<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171206104936 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user ADD notification_endpoint INT DEFAULT NULL, ADD send_notifications TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A6479915237DE FOREIGN KEY (notification_endpoint) REFERENCES notification_endpoint (id)');
        $this->addSql('CREATE INDEX IDX_957A6479915237DE ON fos_user (notification_endpoint)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A6479915237DE');
        $this->addSql('DROP INDEX IDX_957A6479915237DE ON fos_user');
        $this->addSql('ALTER TABLE fos_user DROP notification_endpoint, DROP send_notifications');
    }
}
