<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200714134517 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE geo_op_log ADD veterinary_id INT DEFAULT NULL, DROP first_name, DROP last_name');
        $this->addSql('ALTER TABLE geo_op_log ADD CONSTRAINT FK_F9736BBED954EB99 FOREIGN KEY (veterinary_id) REFERENCES veterinary (id)');
        $this->addSql('CREATE INDEX IDX_F9736BBED954EB99 ON geo_op_log (veterinary_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE geo_op_log DROP FOREIGN KEY FK_F9736BBED954EB99');
        $this->addSql('DROP INDEX IDX_F9736BBED954EB99 ON geo_op_log');
        $this->addSql('ALTER TABLE geo_op_log ADD first_name VARCHAR(32) NOT NULL COLLATE utf8_unicode_ci, ADD last_name VARCHAR(64) NOT NULL COLLATE utf8_unicode_ci, DROP veterinary_id');
    }
}
