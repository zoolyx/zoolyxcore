<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180116135258 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE reference_range CHANGE low low VARCHAR(10) DEFAULT NULL, CHANGE high high VARCHAR(10) DEFAULT NULL, CHANGE very_low very_low VARCHAR(10) DEFAULT NULL, CHANGE very_high very_high VARCHAR(10) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE reference_range CHANGE low low DOUBLE PRECISION DEFAULT NULL, CHANGE high high DOUBLE PRECISION DEFAULT NULL, CHANGE very_low very_low DOUBLE PRECISION DEFAULT NULL, CHANGE very_high very_high DOUBLE PRECISION DEFAULT NULL');
    }
}
