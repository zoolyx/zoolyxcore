<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181128185141 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA3FF515203');
        $this->addSql('ALTER TABLE ticket ADD veterinary_id INT DEFAULT NULL, ADD context_owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA3D954EB99 FOREIGN KEY (veterinary_id) REFERENCES veterinary (id)');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA36AC8859E FOREIGN KEY (context_owner_id) REFERENCES owner (id)');
        $this->addSql('CREATE INDEX IDX_97A0ADA3D954EB99 ON ticket (veterinary_id)');
        $this->addSql('CREATE INDEX IDX_97A0ADA36AC8859E ON ticket (context_owner_id)');
        $this->addSql('DROP INDEX idx_97a0ada3ff515203 ON ticket');
        $this->addSql('CREATE INDEX IDX_97A0ADA37E3C61F9 ON ticket (owner_id)');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA3FF515203 FOREIGN KEY (owner_id) REFERENCES fos_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA3D954EB99');
        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA36AC8859E');
        $this->addSql('DROP INDEX IDX_97A0ADA3D954EB99 ON ticket');
        $this->addSql('DROP INDEX IDX_97A0ADA36AC8859E ON ticket');
        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA37E3C61F9');
        $this->addSql('ALTER TABLE ticket DROP veterinary_id, DROP context_owner_id');
        $this->addSql('DROP INDEX idx_97a0ada37e3c61f9 ON ticket');
        $this->addSql('CREATE INDEX IDX_97A0ADA3FF515203 ON ticket (owner_id)');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA37E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user (id)');
    }
}
