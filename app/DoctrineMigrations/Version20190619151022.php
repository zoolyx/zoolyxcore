<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190619151022 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE history (id INT AUTO_INCREMENT NOT NULL, account_id INT DEFAULT NULL, report_id INT DEFAULT NULL, action VARCHAR(64) NOT NULL, INDEX IDX_27BA704B9B6B5FBA (account_id), INDEX IDX_27BA704B4BD2A4C0 (report_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE history ADD CONSTRAINT FK_27BA704B9B6B5FBA FOREIGN KEY (account_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE history ADD CONSTRAINT FK_27BA704B4BD2A4C0 FOREIGN KEY (report_id) REFERENCES report (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ContractorSubmission (id INT AUTO_INCREMENT NOT NULL, contractor_id INT DEFAULT NULL, submission_id VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, status SMALLINT NOT NULL, created DATETIME NOT NULL, confirmed DATETIME DEFAULT NULL, INDEX IDX_F665D449B0265DC7 (contractor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ContractorSubmission ADD CONSTRAINT FK_F665D449B0265DC7 FOREIGN KEY (contractor_id) REFERENCES fos_user (id)');
        $this->addSql('DROP TABLE history');
    }
}
