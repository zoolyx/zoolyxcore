<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20201014124740 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE geo_op_log ADD veterinary_practice_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE geo_op_log ADD CONSTRAINT FK_F9736BBE2AEFDCC5 FOREIGN KEY (veterinary_practice_id) REFERENCES veterinary_practice (id)');
        $this->addSql('CREATE INDEX IDX_F9736BBE2AEFDCC5 ON geo_op_log (veterinary_practice_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE geo_op_log DROP FOREIGN KEY FK_F9736BBE2AEFDCC5');
        $this->addSql('DROP INDEX IDX_F9736BBE2AEFDCC5 ON geo_op_log');
        $this->addSql('ALTER TABLE geo_op_log DROP veterinary_practice_id');
    }
}
