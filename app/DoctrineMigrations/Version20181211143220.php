<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181211143220 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE watch_ticket ADD ticket_event_id INT DEFAULT NULL, ADD has_new TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE watch_ticket ADD CONSTRAINT FK_D0E0513FF49E140A FOREIGN KEY (ticket_event_id) REFERENCES ticket_event (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D0E0513FF49E140A ON watch_ticket (ticket_event_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE watch_ticket DROP FOREIGN KEY FK_D0E0513FF49E140A');
        $this->addSql('DROP INDEX UNIQ_D0E0513FF49E140A ON watch_ticket');
        $this->addSql('ALTER TABLE watch_ticket DROP ticket_event_id, DROP has_new');
    }
}
