<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170927140114 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE parameter_profile_breed (parameter_profile_id INT NOT NULL, breed_id INT NOT NULL, INDEX IDX_73168D57E96C94B5 (parameter_profile_id), INDEX IDX_73168D57A8B4A30F (breed_id), PRIMARY KEY(parameter_profile_id, breed_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE parameter_profile_breed ADD CONSTRAINT FK_73168D57E96C94B5 FOREIGN KEY (parameter_profile_id) REFERENCES parameter_profile (id)');
        $this->addSql('ALTER TABLE parameter_profile_breed ADD CONSTRAINT FK_73168D57A8B4A30F FOREIGN KEY (breed_id) REFERENCES breed (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE parameter_profile_breed');
    }
}
