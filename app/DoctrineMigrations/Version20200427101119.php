<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200427101119 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cart_line ADD sample_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cart_line ADD CONSTRAINT FK_3EF1B4CF1B1FEA20 FOREIGN KEY (sample_id) REFERENCES sample (id)');
        $this->addSql('CREATE INDEX IDX_3EF1B4CF1B1FEA20 ON cart_line (sample_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cart_line DROP FOREIGN KEY FK_3EF1B4CF1B1FEA20');
        $this->addSql('DROP INDEX IDX_3EF1B4CF1B1FEA20 ON cart_line');
        $this->addSql('ALTER TABLE cart_line DROP sample_id');
    }
}
