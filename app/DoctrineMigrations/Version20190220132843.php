<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190220132843 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A6479F4344D77');
        $this->addSql('DROP INDEX IDX_957A6479F4344D77 ON fos_user');
        $this->addSql('ALTER TABLE fos_user DROP notification_endpoint_id, DROP send_notifications, DROP notification_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user ADD notification_endpoint_id INT DEFAULT NULL, ADD send_notifications TINYINT(1) NOT NULL, ADD notification_id VARCHAR(16) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A6479F4344D77 FOREIGN KEY (notification_endpoint_id) REFERENCES notification_endpoint (id)');
        $this->addSql('CREATE INDEX IDX_957A6479F4344D77 ON fos_user (notification_endpoint_id)');
    }
}
