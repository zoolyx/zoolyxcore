<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170927115528 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE breed (id INT AUTO_INCREMENT NOT NULL, species_id INT DEFAULT NULL, breed_id VARCHAR(8) NOT NULL, UNIQUE INDEX UNIQ_F8AF884FA8B4A30F (breed_id), INDEX IDX_F8AF884FB2A1D860 (species_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE species (id INT AUTO_INCREMENT NOT NULL, species_id VARCHAR(2) NOT NULL, UNIQUE INDEX UNIQ_A50FF712B2A1D860 (species_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE breed ADD CONSTRAINT FK_F8AF884FB2A1D860 FOREIGN KEY (species_id) REFERENCES species (id)');
        $this->addSql('ALTER TABLE description ADD species_id INT DEFAULT NULL, ADD breed_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE description ADD CONSTRAINT FK_6DE44026B2A1D860 FOREIGN KEY (species_id) REFERENCES species (id)');
        $this->addSql('ALTER TABLE description ADD CONSTRAINT FK_6DE44026A8B4A30F FOREIGN KEY (breed_id) REFERENCES breed (id)');
        $this->addSql('CREATE INDEX IDX_6DE44026B2A1D860 ON description (species_id)');
        $this->addSql('CREATE INDEX IDX_6DE44026A8B4A30F ON description (breed_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE description DROP FOREIGN KEY FK_6DE44026A8B4A30F');
        $this->addSql('ALTER TABLE description DROP FOREIGN KEY FK_6DE44026B2A1D860');
        $this->addSql('ALTER TABLE breed DROP FOREIGN KEY FK_F8AF884FB2A1D860');
        $this->addSql('DROP TABLE breed');
        $this->addSql('DROP TABLE species');
        $this->addSql('DROP INDEX IDX_6DE44026B2A1D860 ON description');
        $this->addSql('DROP INDEX IDX_6DE44026A8B4A30F ON description');
        $this->addSql('ALTER TABLE description DROP species_id, DROP breed_id');
    }
}
