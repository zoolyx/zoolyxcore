<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170716195418 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE veterinary_practice DROP FOREIGN KEY FK_B80BBEEDD954EB99');
        $this->addSql('DROP INDEX IDX_B80BBEEDD954EB99 ON veterinary_practice');
        $this->addSql('ALTER TABLE veterinary_practice CHANGE veterinary_id veterinary_old_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE veterinary_practice ADD CONSTRAINT FK_B80BBEEDAE0E68F9 FOREIGN KEY (veterinary_old_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_B80BBEEDAE0E68F9 ON veterinary_practice (veterinary_old_id)');
        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA3D954EB99');
        $this->addSql('DROP INDEX IDX_97A0ADA3D954EB99 ON ticket');
        $this->addSql('ALTER TABLE ticket CHANGE veterinary_id veterinary_old_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA3AE0E68F9 FOREIGN KEY (veterinary_old_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_97A0ADA3AE0E68F9 ON ticket (veterinary_old_id)');

        $this->addSql('CREATE TABLE veterinary (id INT AUTO_INCREMENT NOT NULL, account_id INT DEFAULT NULL, title VARCHAR(32) DEFAULT NULL, first_name VARCHAR(128) NOT NULL, last_name VARCHAR(128) NOT NULL, language VARCHAR(16) NOT NULL, INDEX IDX_C414EC879B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE veterinary ADD CONSTRAINT FK_C414EC879B6B5FBA FOREIGN KEY (account_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE veterinary_practice ADD veterinary_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE veterinary_practice ADD CONSTRAINT FK_B80BBEEDD954EB99 FOREIGN KEY (veterinary_id) REFERENCES veterinary (id)');
        $this->addSql('CREATE INDEX IDX_B80BBEEDD954EB99 ON veterinary_practice (veterinary_id)');
        $this->addSql('ALTER TABLE ticket ADD veterinary_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA3D954EB99 FOREIGN KEY (veterinary_id) REFERENCES veterinary (id)');
        $this->addSql('CREATE INDEX IDX_97A0ADA3D954EB99 ON ticket (veterinary_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA3AE0E68F9');
        $this->addSql('DROP INDEX IDX_97A0ADA3AE0E68F9 ON ticket');
        $this->addSql('ALTER TABLE ticket CHANGE veterinary_old_id veterinary_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA3D954EB99 FOREIGN KEY (veterinary_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_97A0ADA3D954EB99 ON ticket (veterinary_id)');
        $this->addSql('ALTER TABLE veterinary_practice DROP FOREIGN KEY FK_B80BBEEDAE0E68F9');
        $this->addSql('DROP INDEX IDX_B80BBEEDAE0E68F9 ON veterinary_practice');
        $this->addSql('ALTER TABLE veterinary_practice CHANGE veterinary_old_id veterinary_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE veterinary_practice ADD CONSTRAINT FK_B80BBEEDD954EB99 FOREIGN KEY (veterinary_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_B80BBEEDD954EB99 ON veterinary_practice (veterinary_id)');

        $this->addSql('ALTER TABLE veterinary_practice DROP FOREIGN KEY FK_B80BBEEDD954EB99');
        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA3D954EB99');
        $this->addSql('DROP TABLE veterinary');
        $this->addSql('DROP INDEX IDX_97A0ADA3D954EB99 ON ticket');
        $this->addSql('ALTER TABLE ticket DROP veterinary_id');
        $this->addSql('DROP INDEX IDX_B80BBEEDD954EB99 ON veterinary_practice');
        $this->addSql('ALTER TABLE veterinary_practice DROP veterinary_id');
    }
}
