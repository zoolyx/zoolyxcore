<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181212161830 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE watch_ticket DROP FOREIGN KEY FK_D0E0513FF49E140A');
        $this->addSql('ALTER TABLE watch_ticket ADD CONSTRAINT FK_D0E0513FF49E140A FOREIGN KEY (ticket_event_id) REFERENCES ticket_event (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE watch_ticket DROP FOREIGN KEY FK_D0E0513FF49E140A');
        $this->addSql('ALTER TABLE watch_ticket ADD CONSTRAINT FK_D0E0513FF49E140A FOREIGN KEY (ticket_event_id) REFERENCES ticket_event (id)');
    }
}
