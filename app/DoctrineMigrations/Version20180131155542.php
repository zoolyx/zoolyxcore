<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180131155542 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE parameter_profile_form (id INT AUTO_INCREMENT NOT NULL, reference VARCHAR(16) NOT NULL, content LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_55350F15AEA34913 (reference), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE parameter_profile ADD parameter_profile_form_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE parameter_profile ADD CONSTRAINT FK_4B09168FAD877C12 FOREIGN KEY (parameter_profile_form_id) REFERENCES parameter_profile_form (id)');
        $this->addSql('CREATE INDEX IDX_4B09168FAD877C12 ON parameter_profile (parameter_profile_form_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE parameter_profile DROP FOREIGN KEY FK_4B09168FAD877C12');
        $this->addSql('DROP TABLE parameter_profile_form');
        $this->addSql('DROP INDEX IDX_4B09168FAD877C12 ON parameter_profile');
        $this->addSql('ALTER TABLE parameter_profile DROP parameter_profile_form_id');
    }
}
