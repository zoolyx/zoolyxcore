<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180403103711 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE parameter_base_sample_type (id INT AUTO_INCREMENT NOT NULL, parameter_base_id INT DEFAULT NULL, comment_id INT DEFAULT NULL, sample_type_id INT DEFAULT NULL, min_volume VARCHAR(16) DEFAULT NULL, pref VARCHAR(16) DEFAULT NULL, INDEX IDX_1D58EF03A33963C1 (parameter_base_id), INDEX IDX_1D58EF03F8697D13 (comment_id), INDEX IDX_1D58EF03D5064105 (sample_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE parameter_base_sample_type ADD CONSTRAINT FK_1D58EF03A33963C1 FOREIGN KEY (parameter_base_id) REFERENCES parameter_base (id)');
        $this->addSql('ALTER TABLE parameter_base_sample_type ADD CONSTRAINT FK_1D58EF03F8697D13 FOREIGN KEY (comment_id) REFERENCES parameter_profile_comment (id)');
        $this->addSql('ALTER TABLE parameter_base_sample_type ADD CONSTRAINT FK_1D58EF03D5064105 FOREIGN KEY (sample_type_id) REFERENCES parameter_profile_sample_type_definition (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE parameter_base_sample_type');
    }
}
