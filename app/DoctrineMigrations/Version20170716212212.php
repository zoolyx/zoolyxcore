<?php

namespace Application\Migrations;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zoolyx\CoreBundle\Entity\Practice;
use Zoolyx\CoreBundle\Entity\Repository\UserRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryPracticeRepository;
use Zoolyx\CoreBundle\Entity\Repository\VeterinaryRepository;
use Zoolyx\CoreBundle\Entity\User;
use Zoolyx\CoreBundle\Entity\Veterinary;
use Zoolyx\CoreBundle\Entity\VeterinaryPractice;
use Zoolyx\CoreBundle\Entity\VeterinaryPracticeReport;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170716212212 extends AbstractMigration implements ContainerAwareInterface
{
    /** @var  ObjectManager */
    private $entityManager;
    /** @var VeterinaryPracticeRepository */
    private $veterinaryPracticeRepository;
    /** @var VeterinaryRepository */
    private $veterinaryRepository;
    /** @var UserRepository */
    private $userRepository;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->entityManager = $container->get('doctrine.orm.entity_manager');
        $this->veterinaryPracticeRepository = $container->get('zoolyx_core.repository.veterinary_practice');
        $this->veterinaryRepository = $container->get('zoolyx_core.repository.veterinary');
        $this->userRepository = $container->get('zoolyx_core.repository.user');
    }

    public function preUp(Schema $schema) {
        $veterinaryPractices = $this->veterinaryPracticeRepository->findAll();
        $limsIdList = array();
        /** @var VeterinaryPractice $veterinaryPractice */
        foreach($veterinaryPractices as $veterinaryPractice) {
            $limsId = $veterinaryPractice->getLimsId();
            if (isset($limsIdList[$limsId])) {
                /** @var VeterinaryPractice $originalVeterinaryPractice */
                $originalVeterinaryPractice = $limsIdList[$limsId];
                /** @var VeterinaryPracticeReport $vpr */
                foreach ($veterinaryPractice->getVeterinaryPracticeReports() as $vpr) {
                    $veterinaryPractice->removeVeterinaryPracticeReport($vpr);
                    $originalVeterinaryPractice->addVeterinaryPracticeReport($vpr);
                    $vpr->setVeterinaryPractice($originalVeterinaryPractice);
                    $this->entityManager->persist($vpr);
                }
                /** @var Practice $practice */
                $practice = $veterinaryPractice->getPractice();
                if (count($practice->getVeterinaryPractices()) < 2) {
                    $this->entityManager->remove($practice);
                }
                $this->entityManager->remove($veterinaryPractice);
            } else {
                $limsIdList[$limsId] = $veterinaryPractice;
            }
        }
        $this->entityManager->flush();
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_B80BBEED7FA907D1 ON veterinary_practice (lims_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_B80BBEED7FA907D1 ON veterinary_practice');
    }

    public function postUp(Schema $schema) {
        $veterinaryPractices = $this->veterinaryPracticeRepository->findAll();
        /** @var VeterinaryPractice $veterinaryPractice */
        foreach ($veterinaryPractices as $veterinaryPractice) {
            $userId = $veterinaryPractice->getId();
            $veterinary = $this->veterinaryRepository->findOneBy(array(
                'account'=>$userId
            ));
            if (!$veterinary) {
                $veterinary = new Veterinary();
                /** @var User $user */
                $user = $this->userRepository->find($userId);
                $veterinary->setAccount($user);
                $this->entityManager->persist($veterinary);
            }
            $veterinaryPractice->setVeterinary($veterinary);
            $this->entityManager->persist($veterinaryPractice);
        }
        $this->entityManager->flush();
    }

}
