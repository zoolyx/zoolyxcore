<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180703115326 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE genius_code DROP FOREIGN KEY FK_A1572BB8E96C94B5');
        $this->addSql('ALTER TABLE genius_code ADD CONSTRAINT FK_A1572BB8E96C94B5 FOREIGN KEY (parameter_profile_id) REFERENCES parameter_profile (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE genius_code DROP FOREIGN KEY FK_A1572BB8E96C94B5');
        $this->addSql('ALTER TABLE genius_code ADD CONSTRAINT FK_A1572BB8E96C94B5 FOREIGN KEY (parameter_profile_id) REFERENCES parameter_profile (id)');
    }
}
