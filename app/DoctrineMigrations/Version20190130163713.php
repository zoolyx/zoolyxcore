<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190130163713 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE veterinary_practice ADD notification_id VARCHAR(64) DEFAULT NULL');
        $this->addSql('ALTER TABLE practice ADD notification_endpoint_id INT DEFAULT NULL, ADD send_notifications TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE practice ADD CONSTRAINT FK_7FEC344EF4344D77 FOREIGN KEY (notification_endpoint_id) REFERENCES notification_endpoint (id)');
        $this->addSql('CREATE INDEX IDX_7FEC344EF4344D77 ON practice (notification_endpoint_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE practice DROP FOREIGN KEY FK_7FEC344EF4344D77');
        $this->addSql('DROP INDEX IDX_7FEC344EF4344D77 ON practice');
        $this->addSql('ALTER TABLE practice DROP notification_endpoint_id, DROP send_notifications');
        $this->addSql('ALTER TABLE veterinary_practice DROP notification_id');
    }
}
