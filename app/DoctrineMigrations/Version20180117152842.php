<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180117152842 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE parameter_group_base (id INT AUTO_INCREMENT NOT NULL, parameter_group_id VARCHAR(8) NOT NULL, UNIQUE INDEX UNIQ_CC78231C132604DB (parameter_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE description ADD parameter_group_base_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE description ADD CONSTRAINT FK_6DE44026930A1187 FOREIGN KEY (parameter_group_base_id) REFERENCES parameter_group_base (id)');
        $this->addSql('CREATE INDEX IDX_6DE44026930A1187 ON description (parameter_group_base_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE description DROP FOREIGN KEY FK_6DE44026930A1187');
        $this->addSql('DROP TABLE parameter_group_base');
        $this->addSql('DROP INDEX IDX_6DE44026930A1187 ON description');
        $this->addSql('ALTER TABLE description DROP parameter_group_base_id');
    }
}
