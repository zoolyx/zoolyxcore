<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181017151743 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE vat (id INT AUTO_INCREMENT NOT NULL, fa_id VARCHAR(32) NOT NULL, vat VARCHAR(128) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE veterinary_practice DROP FOREIGN KEY FK_B80BBEEDD954EB99');
        $this->addSql('ALTER TABLE veterinary_practice ADD vat_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE veterinary_practice ADD CONSTRAINT FK_B80BBEEDB5B63A6B FOREIGN KEY (vat_id) REFERENCES vat (id)');
        $this->addSql('ALTER TABLE veterinary_practice ADD CONSTRAINT FK_B80BBEEDD954EB99 FOREIGN KEY (veterinary_id) REFERENCES veterinary (id)');
        $this->addSql('CREATE INDEX IDX_B80BBEEDB5B63A6B ON veterinary_practice (vat_id)');
        $this->addSql('ALTER TABLE practice DROP fa_id');
        $this->addSql('ALTER TABLE veterinary DROP FOREIGN KEY FK_8B49EF579B6B5FBA');
        $this->addSql('ALTER TABLE veterinary DROP fa');
        $this->addSql('ALTER TABLE veterinary ADD CONSTRAINT FK_8B49EF579B6B5FBA FOREIGN KEY (account_id) REFERENCES fos_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE veterinary_practice DROP FOREIGN KEY FK_B80BBEEDB5B63A6B');
        $this->addSql('DROP TABLE vat');
        $this->addSql('ALTER TABLE practice ADD fa_id VARCHAR(16) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE veterinary DROP FOREIGN KEY FK_8B49EF579B6B5FBA');
        $this->addSql('ALTER TABLE veterinary ADD fa VARCHAR(8) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE veterinary ADD CONSTRAINT FK_8B49EF579B6B5FBA FOREIGN KEY (account_id) REFERENCES fos_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE veterinary_practice DROP FOREIGN KEY FK_B80BBEEDD954EB99');
        $this->addSql('DROP INDEX IDX_B80BBEEDB5B63A6B ON veterinary_practice');
        $this->addSql('ALTER TABLE veterinary_practice DROP vat_id');
        $this->addSql('ALTER TABLE veterinary_practice ADD CONSTRAINT FK_B80BBEEDD954EB99 FOREIGN KEY (veterinary_id) REFERENCES veterinary (id) ON DELETE SET NULL');
    }
}
