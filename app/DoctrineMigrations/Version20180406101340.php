<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180406101340 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE genius_code (id INT AUTO_INCREMENT NOT NULL, species_id INT DEFAULT NULL, parameter_profile_id INT DEFAULT NULL, reference VARCHAR(16) NOT NULL, rule VARCHAR(64) NOT NULL, expires INT DEFAULT NULL, INDEX IDX_A1572BB8B2A1D860 (species_id), INDEX IDX_A1572BB8E96C94B5 (parameter_profile_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE genius_code ADD CONSTRAINT FK_A1572BB8B2A1D860 FOREIGN KEY (species_id) REFERENCES species (id)');
        $this->addSql('ALTER TABLE genius_code ADD CONSTRAINT FK_A1572BB8E96C94B5 FOREIGN KEY (parameter_profile_id) REFERENCES parameter_profile (id)');
        $this->addSql('ALTER TABLE description ADD genius_code_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE description ADD CONSTRAINT FK_6DE440262E735307 FOREIGN KEY (genius_code_id) REFERENCES genius_code (id)');
        $this->addSql('CREATE INDEX IDX_6DE440262E735307 ON description (genius_code_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE description DROP FOREIGN KEY FK_6DE440262E735307');
        $this->addSql('DROP TABLE genius_code');
        $this->addSql('DROP INDEX IDX_6DE440262E735307 ON description');
        $this->addSql('ALTER TABLE description DROP genius_code_id');
    }
}
