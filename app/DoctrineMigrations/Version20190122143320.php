<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190122143320 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ContractorReport DROP FOREIGN KEY FK_E7998A51BF8D8CE');
        $this->addSql('ALTER TABLE ContractorReport ADD CONSTRAINT FK_E7998A51BF8D8CE FOREIGN KEY (contractor_request_id) REFERENCES ContractorRequest (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ContractorReport DROP FOREIGN KEY FK_E7998A51BF8D8CE');
        $this->addSql('ALTER TABLE ContractorReport ADD CONSTRAINT FK_E7998A51BF8D8CE FOREIGN KEY (contractor_request_id) REFERENCES ContractorRequest (id)');
    }
}
