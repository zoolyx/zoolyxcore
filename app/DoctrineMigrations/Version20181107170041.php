<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181107170041 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE owner CHANGE first_name first_name VARCHAR(128) DEFAULT NULL, CHANGE last_name last_name VARCHAR(128) DEFAULT NULL, CHANGE street street VARCHAR(128) DEFAULT NULL, CHANGE zip_code zip_code VARCHAR(10) DEFAULT NULL, CHANGE city city VARCHAR(64) DEFAULT NULL, CHANGE country country VARCHAR(16) DEFAULT NULL, CHANGE language language VARCHAR(16) DEFAULT NULL, CHANGE telephone telephone VARCHAR(16) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE owner CHANGE first_name first_name VARCHAR(128) NOT NULL COLLATE utf8_unicode_ci, CHANGE last_name last_name VARCHAR(128) NOT NULL COLLATE utf8_unicode_ci, CHANGE street street VARCHAR(128) NOT NULL COLLATE utf8_unicode_ci, CHANGE zip_code zip_code VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, CHANGE city city VARCHAR(64) NOT NULL COLLATE utf8_unicode_ci, CHANGE country country VARCHAR(16) NOT NULL COLLATE utf8_unicode_ci, CHANGE telephone telephone VARCHAR(16) NOT NULL COLLATE utf8_unicode_ci, CHANGE language language VARCHAR(16) NOT NULL COLLATE utf8_unicode_ci');

    }
}
