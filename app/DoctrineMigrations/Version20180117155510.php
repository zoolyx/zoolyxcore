<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180117155510 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE parameter_group ADD parameter_group_base_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE parameter_group ADD CONSTRAINT FK_1D05BE49930A1187 FOREIGN KEY (parameter_group_base_id) REFERENCES parameter_group_base (id)');
        $this->addSql('CREATE INDEX IDX_1D05BE49930A1187 ON parameter_group (parameter_group_base_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE parameter_group DROP FOREIGN KEY FK_1D05BE49930A1187');
        $this->addSql('DROP INDEX IDX_1D05BE49930A1187 ON parameter_group');
        $this->addSql('ALTER TABLE parameter_group DROP parameter_group_base_id');
    }
}
