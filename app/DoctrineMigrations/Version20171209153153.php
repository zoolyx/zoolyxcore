<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171209153153 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE parameter_profile_sample_type ADD comment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE parameter_profile_sample_type ADD CONSTRAINT FK_9A978F93F8697D13 FOREIGN KEY (comment_id) REFERENCES parameter_profile_comment (id)');
        $this->addSql('CREATE INDEX IDX_9A978F93F8697D13 ON parameter_profile_sample_type (comment_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE parameter_profile_sample_type DROP FOREIGN KEY FK_9A978F93F8697D13');
        $this->addSql('DROP INDEX IDX_9A978F93F8697D13 ON parameter_profile_sample_type');
        $this->addSql('ALTER TABLE parameter_profile_sample_type DROP comment_id');
    }
}
