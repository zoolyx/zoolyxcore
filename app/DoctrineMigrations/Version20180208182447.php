<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180208182447 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE veterinary_practice DROP FOREIGN KEY FK_B80BBEEDAE0E68F9');
        $this->addSql('DROP INDEX IDX_B80BBEEDAE0E68F9 ON veterinary_practice');
        $this->addSql('ALTER TABLE veterinary_practice DROP veterinary_old_id');
        $this->addSql('ALTER TABLE fos_user DROP title');
        $this->addSql('ALTER TABLE owner DROP FOREIGN KEY FK_8BFC53CD7E3C61F9');
        $this->addSql('DROP INDEX IDX_CF60E67C7E3C61F9 ON owner');
        $this->addSql('ALTER TABLE owner DROP owner_id');
        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA3AE0E68F9');
        $this->addSql('DROP INDEX IDX_97A0ADA3AE0E68F9 ON ticket');
        $this->addSql('ALTER TABLE ticket DROP veterinary_old_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ContractorOrder (id INT AUTO_INCREMENT NOT NULL, contractor_request_id INT DEFAULT NULL, pet_id INT DEFAULT NULL, contractor_id INT DEFAULT NULL, status SMALLINT NOT NULL, created DATETIME NOT NULL, confirmed DATETIME DEFAULT NULL, sample_id VARCHAR(16) NOT NULL COLLATE utf8_unicode_ci, container VARCHAR(16) NOT NULL COLLATE utf8_unicode_ci, position VARCHAR(16) NOT NULL COLLATE utf8_unicode_ci, profiles VARCHAR(256) NOT NULL COLLATE utf8_unicode_ci, INDEX IDX_19043869966F7FB6 (pet_id), INDEX IDX_19043869B0265DC7 (contractor_id), INDEX IDX_190438691BF8D8CE (contractor_request_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ContractorOrder ADD CONSTRAINT FK_190438691BF8D8CE FOREIGN KEY (contractor_request_id) REFERENCES ContractorRequest (id)');
        $this->addSql('ALTER TABLE ContractorOrder ADD CONSTRAINT FK_19043869966F7FB6 FOREIGN KEY (pet_id) REFERENCES pet (id)');
        $this->addSql('ALTER TABLE ContractorOrder ADD CONSTRAINT FK_19043869B0265DC7 FOREIGN KEY (contractor_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE fos_user ADD title VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE owner ADD owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE owner ADD CONSTRAINT FK_CF60E67C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_CF60E67C7E3C61F9 ON owner (owner_id)');
        $this->addSql('ALTER TABLE ticket ADD veterinary_old_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA3AE0E68F9 FOREIGN KEY (veterinary_old_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_97A0ADA3AE0E68F9 ON ticket (veterinary_old_id)');
        $this->addSql('ALTER TABLE veterinary_practice ADD veterinary_old_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE veterinary_practice ADD CONSTRAINT FK_B80BBEEDAE0E68F9 FOREIGN KEY (veterinary_old_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_B80BBEEDAE0E68F9 ON veterinary_practice (veterinary_old_id)');
    }
}
