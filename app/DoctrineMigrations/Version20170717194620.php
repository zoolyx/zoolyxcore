<?php

namespace Application\Migrations;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zoolyx\CoreBundle\Entity\Owner;
use Zoolyx\CoreBundle\Entity\Report;
use Zoolyx\CoreBundle\Entity\Repository\OwnerOldRepository;
use Zoolyx\CoreBundle\Entity\Repository\OwnerRepository;
use Zoolyx\CoreBundle\Entity\Repository\ReportRepository;
use Zoolyx\CoreBundle\Entity\Repository\UserRepository;
use Zoolyx\CoreBundle\Entity\User;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170717194620 extends AbstractMigration implements ContainerAwareInterface
{
    /** @var ObjectManager */
    private $entityManager;
    /** @var OwnerRepository */
    private $ownerRepository;
    /** @var ReportRepository */
    private $reportRepository;
    /** @var UserRepository */
    private $userRepository;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->entityManager = $container->get('doctrine.orm.entity_manager');
        $this->ownerRepository = $container->get('zoolyx_core.repository.owner');
        //$this->ownerOldRepository = $container->get('zoolyx_core.repository.owner_old');
        $this->reportRepository = $container->get('zoolyx_core.repository.report');
        $this->userRepository = $container->get('zoolyx_core.repository.user');
    }

    public function preUp(Schema $schema) {
        $owners = $this->ownerRepository->findAll();
        $limsIdList = array();
        /** @var Owner $owner */
        foreach($owners as $owner) {
            $limsId = $owner->getLimsId();
            if (isset($limsIdList[$limsId])) {
                /** @var Owner $originalOwner */
                $originalOwner = $limsIdList[$limsId];
                $reports = $this->reportRepository->findBy(array('owner'=>$originalOwner));
                /** @var Report $report */
                foreach ($reports as $report) {
                    $report->setOwner($owner);
                    $this->entityManager->persist($report);
                }
                $this->entityManager->remove($originalOwner);
            } else {
                $limsIdList[$limsId] = $owner;
            }
        }
        $this->entityManager->flush();
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_CF60E67C7FA907D1 ON owner (lims_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_CF60E67C7FA907D1 ON owner');
    }

    public function postUp(Schema $schema) {



    }
}
