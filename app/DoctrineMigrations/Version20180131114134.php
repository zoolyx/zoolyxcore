<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180131114134 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A6479915237DE');
        $this->addSql('DROP INDEX IDX_957A6479915237DE ON fos_user');
        $this->addSql('ALTER TABLE fos_user CHANGE notification_endpoint notification_endpoint_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A6479F4344D77 FOREIGN KEY (notification_endpoint_id) REFERENCES notification_endpoint (id)');
        $this->addSql('CREATE INDEX IDX_957A6479F4344D77 ON fos_user (notification_endpoint_id)');
        $this->addSql('ALTER TABLE notification_endpoint ADD xslt LONGTEXT NOT NULL, CHANGE url url VARCHAR(256) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A6479F4344D77');
        $this->addSql('DROP INDEX IDX_957A6479F4344D77 ON fos_user');
        $this->addSql('ALTER TABLE fos_user CHANGE notification_endpoint_id notification_endpoint INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A6479915237DE FOREIGN KEY (notification_endpoint) REFERENCES notification_endpoint (id)');
        $this->addSql('CREATE INDEX IDX_957A6479915237DE ON fos_user (notification_endpoint)');
        $this->addSql('ALTER TABLE notification_endpoint DROP xslt, CHANGE url url VARCHAR(64) NOT NULL COLLATE utf8_unicode_ci');
    }
}
