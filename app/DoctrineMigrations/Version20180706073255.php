<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180706073255 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fos_user_registration_request (id INT AUTO_INCREMENT NOT NULL, account_id INT DEFAULT NULL, created_on DATETIME NOT NULL, requested_by_user TINYINT(1) NOT NULL, state SMALLINT NOT NULL, info LONGTEXT NOT NULL, approved_on DATETIME DEFAULT NULL, INDEX IDX_54C2DE599B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fos_user_registration_request ADD CONSTRAINT FK_54C2DE599B6B5FBA FOREIGN KEY (account_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE ticket_event DROP FOREIGN KEY FK_139E692C9B6B5FBA');
        $this->addSql('DROP INDEX IDX_139E692C9B6B5FBA ON ticket_event');
        $this->addSql('ALTER TABLE ticket_event DROP order_number, account_id');
        $this->addSql('ALTER TABLE ticket_event ADD request_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ticket_event ADD CONSTRAINT FK_139E692C427EB8A5 FOREIGN KEY (request_id) REFERENCES fos_user_registration_request (id)');
        $this->addSql('CREATE INDEX IDX_139E692C427EB8A5 ON ticket_event (request_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ticket_event DROP FOREIGN KEY FK_139E692C427EB8A5');
        $this->addSql('DROP TABLE fos_user_registration_request');
        $this->addSql('DROP INDEX IDX_139E692C427EB8A5 ON ticket_event');
        $this->addSql('ALTER TABLE ticket_event ADD order_number VARCHAR(8) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE request_id account_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ticket_event ADD CONSTRAINT FK_139E692C9B6B5FBA FOREIGN KEY (account_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_139E692C9B6B5FBA ON ticket_event (account_id)');
    }
}
