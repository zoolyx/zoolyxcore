<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200712095123 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE geo_op_log (id INT AUTO_INCREMENT NOT NULL, client_id VARCHAR(16) NOT NULL, job_id VARCHAR(16) NOT NULL, first_name VARCHAR(32) NOT NULL, last_name VARCHAR(64) NOT NULL, title VARCHAR(64) NOT NULL, description VARCHAR(64) NOT NULL, priority INT NOT NULL, line1 VARCHAR(64) NOT NULL, line2 VARCHAR(64) NOT NULL, city VARCHAR(32) NOT NULL, state VARCHAR(32) NOT NULL, post_code VARCHAR(8) NOT NULL, latitude VARCHAR(32) NOT NULL, longitude VARCHAR(16) NOT NULL, response VARCHAR(16) NOT NULL, created_on DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE geo_op_log');
    }
}
